﻿<?php
session_start();

include 'lang.php';
//include "./backoffice/DBAccess.php";
include "./backoffice/DBAccess2.php";

$dbInst = new DBAccess2();
$dbInst->connOpen();

//$lang = (isset($_GET["lang"]) ? $_GET["lang"] : "IT");

if(isset($_GET["lang"])){
	$lang = $_GET["lang"];
	$_SESSION["lang"] = $lang;
}else{
	if(isset($_SESSION["lang"])){
		$lang = $_SESSION["lang"];
	}else{
		$lang = "EN";
		$_SESSION["lang"] = $lang;
	}
}

if (($lang!="IT") && ($lang!="EN")) $lang="IT";
$idPage = $_GET["idPage"];

$extD = $dbInst->getDetailLangPagina($idPage, $lang);
$row = mysql_fetch_array($extD);
$title = $row['TITLE'];
$description = $row['DESCRIPTION'];
$keywords = $row['KEYWORDS'];

$menuRows = $dbInst->getVociMenuTop($lang,$idPage);
//per ogni menu' devo vedere se ci sono sottomenu
	//$subMenuItem = $dbInst->getVociMenuTopByPadre($lang,$positionRow['id']);	
	//$subMenuRows[$positionRow['id']] = $dbInst->getVociMenuTopByPadre($lang,$positionRow['id']);
 
$dbInst->connClose();


$cat = (isset($_GET["cat"]) ? $_GET["cat"] : "");
$tag = (isset($_GET["tag"]) ? $_GET["tag"] : "");
?>


<!DOCTYPE html>
<html>
  <head>
    <title><?=$title?></title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="HandheldFriendly" content="true" />

    <meta name="description" content="<?=$description?>"/>
    <meta name="keywords" content="<?=$keywords?>"/>

    <link href='https://fonts.googleapis.com/css?family=Maven+Pro:900,500' rel='stylesheet' type='text/css'>
    <link href="bootstrap/css/bootstrap.css" rel="stylesheet" media="screen">
    <link href="bootstrap.customization.css" rel="stylesheet" media="screen">
    <link href="navbar.customization.css" rel="stylesheet" media="screen">
    <link href="carousel.customization.css" rel="stylesheet" media="screen">
    <link href="content.customization.css" rel="stylesheet" media="screen">
	<link href="ThumbnailGridExpandingPreview/css/default.css" rel="stylesheet" type="text/css"  />
	<link href="ThumbnailGridExpandingPreview/css/component.css" rel="stylesheet" type="text/css"  />
	<!-- <link href="jQueryTimeliner/css/style.css" rel="stylesheet"  media="screen" /> -->

	<link href="timeline/css/flat.css" rel="stylesheet" type="text/css" />
	<link href="timeline/css/style.css" rel="stylesheet" type="text/css" />
	<link href="timeline/css/lightbox.css" rel="stylesheet" type="text/css" />
	<link href="timeline/css/jquery.mCustomScrollbar.css" rel="stylesheet" type="text/css" />

	<link rel="shortcut icon" href="/images/faviconBW.ico" type="image/x-icon" />

	<!-- <link rel="stylesheet" href="arbor-v0.92/docs/sample-project/style.css" type="text/css"> -->
	
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
    
    <style>

	    body {
	    	padding: 0px;
	    	margin: 0px;
	    	font-family: 'Maven Pro';
	    	font-size: 13px;
	    	font-weight:500;
	    }

		q{
	    	font-size: 18px;
			font-weight:500;
	    	line-height: 30px;	 
		}	
		
	    #rowCollection h2 {
	    	font-size: 55px;
	    	line-height: 55px;	 
	    	margin:0px;
	    	padding: 0px;   	
	    	color: #b1c903;
	    }

	    #rowCollection h3 {
	    	font-size: 18px;
	    	line-height: 18px;	    	
	    	margin:0px;
	    	padding: 0px;
	    }

	    #rowCollection h4 {
	    	font-size: 17px;
	    	line-height: 17px;
	    	color: #b1c903;
	    }

	    #rowCollection h5 {
	    	text-align: justify;
	    }

	    a:link, a:hover, a:visited {
	    	text-decoration: none;
	    	color: #58585a;
			<!--color: #b1c903;-->
			
	    }
		
		b {
			font-weight:900;
		}		

    </style>

    <!-- fancybox stylesheet -->
    <link rel="stylesheet" type="text/css" href="./source/jquery.fancybox.css?v=2.1.5" media="screen" />
    <link rel="stylesheet" type="text/css" href="./source/helpers/jquery.fancybox-thumbs.css?v=1.0.7" />

    <!-- LayerSlider stylesheet -->
	<link rel="stylesheet" href="layerslider/css/layerslider.css" type="text/css">

  </head>

  <body>
  	<input type="hidden" id="langHidden" value="<?=$lang?>" />

	<div id="fb-root"></div>
  
  
  	<div class="navbar-wrapper" style="position:fixed;background-color: #FFFFFF">
		<div class="container" id="navContainer" style="width:100%;">

			<nav class="navbar navbar-default transparent_navbar" role="navigation">

			  <!-- Brand and toggle get grouped for better mobile display -->
			  <div class="navbar-header">
			    <button type="button" class="navbar-toggle" data-toggle="collapse" id="buttonMobile" data-target="#bs-example-navbar-collapse-1">
			      <span class="sr-only">Toggle navigation</span>
			       <img id="logoButtonMobile" src="images/logo_mediasoft_notext_white.png">
			    </button>
			  </div>

			  <!-- Collect the nav links, forms, and other content for toggling -->
			  <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			    <ul class="nav navbar-nav" id="navBarMainUl" style="width:100%">
					<li id="linkHomeMobile"><a href="index.php?lang='<?=$lang?>'">HOME</a></li>
					<?php
						$indice=0;
						$dbInst = new DBAccess2();
						$dbInst->connOpen();
						
						while($positionRow =  mysql_fetch_assoc($menuRows))
						{
							$indice++;
							$url = $positionRow['url'];
							$SxOrDx = "";
							if ($positionRow['SxDx'] == 1) $SxOrDx="left";
							else $SxOrDx="right";
							if($positionRow['valore'] != "LOGO_SEGNAPOSTO")
							{
								//vedo se ci sono sottomenu
								$subMenuItems = $dbInst->getVociMenuTopByPadre($lang,$positionRow['id']);	
								$countTotal = mysql_num_rows($subMenuItems);
								
								
								if($countTotal == 0)
								//if(0 == 0)
								{
									$subString = strstr($url, 'idPage'); 
									$trovato=false;
									if ($subString==("idPage=".$idPage))  $trovato = true;
									//alert(idPage + " - " + url + " - " + trovato);
									
									//alert(Valore + " - No SubItems");
									if ($trovato)
									{
									?>
									<li menu-position="<?=$SxOrDx?>" class="menu-<?=$SxOrDx?>-item" style="width:14.2%;"><a href="<?=$positionRow['url']?>&lang=<?=$lang?>"><b><?=$positionRow['valore']?></b></a><div class="underliner" style="top:20px;"/></li>
									<?php
									}
									else
									{
									?>
									<li menu-position="<?=$SxOrDx?>" class="menu-<?=$SxOrDx?>-item" style="width:14.2%;"><a href="<?=$positionRow['url']?>&lang=<?=$lang?>"><?=$positionRow['valore']?></a><div class="underliner" style="top:20px;"/></li>
									<?php
									}
								}
								else
								{
									$subString = strstr($url, 'idPage'); 
									$trovato=false;
									if ($subString==("idPage=".$idPage))  $trovato = true;

									if ($trovato)
									{
									?>
									<li menu-position="<?=$SxOrDx?>" class="dropdown menu-<?=$SxOrDx?>-item" style="width:14.2%;" id="father_internet"><a href="<?=$positionRow['url']?>&lang=<?=$lang?>"><b><?=$positionRow['valore']?></b></a><div class="underliner" style="top:20px;"></div> 
									<?php
									}
									else
									{
									?>
									<li menu-position="<?=$SxOrDx?>" class="dropdown menu-<?=$SxOrDx?>-item" style="width:14.2%;" id="father_internet"><a href="<?=$positionRow['url']?>&lang=<?=$lang?>"><?=$positionRow['valore']?></a><div class="underliner" style="top:20px;"></div> 
									<?php
									}
									?>
										<ul id="submenu_<?=$positionRow['id']?>" class="dropdown-menu second-level-submenu-custom">
											<ul class="list-inline">
												<?php		
												while($subMenuRow =  mysql_fetch_assoc($subMenuItems))
												{
													?>
													<li><a href="<?=$subMenuRow['url']?>" id="link<?=$subMenuRow['id']?>" menu-level="secondo"><?=$subMenuRow['valore']?></a><div class="underliner" style="top:35px;"/></li>
													<?php
												}
												?>
											</ul>
										</ul>
									</li>
									<?php
								
								}
							
							}
							else
							{
								// quando Valore == 'LOGO_SEGNAPOSTO' carico l'immagine centrale
								?>
								<li id="navLogo" style="width:14.2%;"><a href="index.php?lang=<?=$lang?>"><img id="imageLogo" src="images/logo_mediasoft_green.png"></a></li>
								<?php
							}
						}
						$dbInst->connClose();
					?>										


					
				</ul>

			  </div><!-- /.navbar-collapse -->
			</nav>

		</div>
		<hr style="margin-left:6%;margin-right:6%;color: #B1C903; background-color: #B1C903; height: 2px;margin-top: 0px;margin-bottom: 0px;">
	</div>
	

	<div id="mainContainerDiv" class="container fill" style="width:90%; margin: 0px; padding: 0px;float:left;position:absolute;top:100px;">
	<!--
	<div id="mainContainerDiv" class="container fill" style="width:80%; margin: 0px; padding: 0px;float:left;">
	-->


	</div>


	
	
	<div id="navbar-laterale" style="position:fixed;width:14%;margin-top: 10px;  right:-5px; top: 120px;">
		<ul id="lateralMenu" class="nav nav-pills nav-stacked" style="list-style-type: circle;">
		</ul>
	</div>

	<div id="social" style="position:fixed; bottom:0px; margin: 10px;  right:20px; height:20px; z-index:999999;">
		<a href="https://www.facebook.com/Mediasoftsrl" target="_blank"><img src="images/social-fb.png" alt="FB" height="20px"></a>
		<a href="https://twitter.com/Mediasoftonline" target="_blank"><img src="images/social-tw.png" alt="TW" height="20px"></a>
		<a href="https://www.linkedin.com/company/1194297" target="_blank"><img src="images/social-lin.png" alt="LinkedIn" height="20px"></a>
	</div>



	<script src="jquery-1.10.2.min.js"></script>
	<script src="jquery.jscroll.min.js"></script>
	<script src="bootstrap/js/bootstrap.js"></script>
	<script src="functions.js?v=2"></script>
	
<!--	<script src="js/jquery.timelinr-0.9.54.js"></script> -->
	<script src="timeline/js/jquery.mCustomScrollbar.js" type="text/javascript"></script>
	<script src="timeline/js/jquery.easing.1.3.js" type="text/javascript"></script>
	<script src="timeline/js/jquery.timeline.js" type="text/javascript"></script>
	<script src="freewall-master/freewall.js"></script>
	<script src="ThumbnailGridExpandingPreview/js/modernizr.custom.js"></script>
	<script src="ThumbnailGridExpandingPreview/js/grid.js"></script>
	<script src="arbor-v0.92/lib/arbor.js"></script> 
	
	<!-- <script src="ahttp://platform.twitter.com/widgets.js"></script> -->
	
	
<!--	<script src="http://d3js.org/d3.v3.min.js" charset="utf-8"></script> 
		
	<script type="text/javascript" src="http://mbostock.github.com/d3/d3.js?2.6.0"></script>
	<script type="text/javascript" src="http://mbostock.github.com/d3/d3.layout.js?2.6.0"></script>
	<script type="text/javascript" src="http://mbostock.github.com/d3/d3.geom.js?2.6.0"></script>
-->	

	<!-- Add fancyBox main JS and CSS files -->
	<script type="text/javascript" src="./source/jquery.fancybox.js?v=2.1.5"></script>
	<script type="text/javascript" src="./source/helpers/jquery.fancybox-thumbs.js?v=1.0.7"></script>

	<!-- LayerSlider script files -->
	<script src="layerslider/js/greensock.js" type="text/javascript"></script>
	<script src="layerslider/js/layerslider.transitions.js" type="text/javascript"></script>
	<script src="layerslider/js/layerslider.kreaturamedia.jquery.js" type="text/javascript"></script>
	<?php
	if($idPage == 1){
		?>
		<script src="lib/frontone.js" type="text/javascript"></script>
		<?php
	}
	if($idPage == 6){
		?>
		<script src="lib/frontone_vision.js" type="text/javascript"></script>
		<?php
	}	?>

	<script type="text/javascript">

		function openScreenshot(idSezione){
			var data = $('a[data-fancybox-group="thumb'+idSezione+'"]');
			data[0].click();
		}

		function addSlide(item){
	  	
	  		//creo i layer per ogni item di slide
	  		var layers = '';
	  		$.each(item.layers, function(i,layer) {
			   var layerConfig = ''
			   $.each( layer.dataLs, function( key, value ) {
				  layerConfig += key + ":" + value +";";
			   });
			   if(layer.tag == "img"){
			   		if(layer.class == 'ls-bg'){
			   			layers += '<img src="'+layer.path+'" class="'+layer.class+'" alt="" onerror="this.src=\'images/evoluzione.jpg\'" />';
			   		}else{
			   			layers += '<img src="'+layer.path+'" class="'+layer.class+'" style="'+layer.style+'" onerror="this.src=\'images/evoluzione.jpg\'" data-ls="'+layerConfig+'" alt="" />';
			   		}
			   }else{
			   		if(layer.multimedia != ''){
			   			layers += '<'+layer.tag+' class="'+layer.class+'" style="'+layer.style+'" data-ls="'+layerConfig+'" >'+layer.multimedia+'</'+layer.tag+'>';
			   		}else{
			   			layers += '<'+layer.tag+' class="'+layer.class+'" style="'+layer.style+'" data-ls="'+layerConfig+'" >'+layer.text+'</'+layer.tag+'>';
			   		}
			   }
			});

	  		//verifico se ogni singola slide deve essere linkata
	  		var linkSlide = '';
			if(item.href != ''){
				var linkSlide = '<a href="'+item.href+'" target="'+item.target+'" class="ls-link"></a>';
			}

	  		var html =  '<div class="ls-slide" data-ls="slidedelay:'+item.slidedelay+';'+item.transitionType+':'+item.transitionNumber+';timeshift:'+item.timeshift+'">' +layers+linkSlide+
		                '</div>';

			$("#layerslider").append(html);
			
	 	}	

		var subTotalAnnoTimeline = new Array();
		var subAnnoTimeline = new Array();
		var startTimeline = "";

		var clientiTxt = new Array();
		var clientiImg1 = new Array();
		var clientiImg2 = new Array();
		var clientiImg3 = new Array();

		var pages_config = new Array();

		pages_config[$.urlParam('idPage')] =
		{
			'idPage': $.urlParam('idPage'),
			'backColor': 'white',

			'renderer': function(row_data)
			{
				var idSezione = row_data['idSezione'];
				var type = row_data['type'];
				var url = row_data['url'];
				var titolo = row_data['titolo'];
				var testo = row_data["testo"];
				var media = row_data['subItems'];
				var allegati = row_data['subItemsAllegati'];
				var icons = row_data['iconItems'];

				//alert("type: " + type);
				//in base al tipo ho una composizione differente
				var html;
				//tipo 1: Titolo, Immagine e descrizione
				if (type=="1")
				{
					//mi aspetto un solo subitem, una immagine
					var img=media[0]['url'];
					html =	
					'<div id="section'+idSezione+'" class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-left" style="padding: 0px 10px 0px 10px; margin: 0px 0px 0px 0px;">' + 		
						'<div>' +
							'<div class="titleSection" style="margin-left:8%; margin-right:8%; text-align:center; color:#b1c903;">' + titolo + '</div>'	+				
							'<div style="margin: 0% 0% 0% 0%;padding-bottom: 5px;"><img src="images/' + img + '" alt="" style="width:100%;" ></div>' + 
							'<div style="margin: 0% 0% 0% 0%; font-size: 15px; overflow-y:auto; text-align: justify; padding-bottom: 20px;">' + testo + '</div>' +
						'</div>' +						
						'<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center" style="padding: 0px; margin: 0px;">' +
						'<hr style="color: #B1C903; background-color: #B1C903; height: 2px; width:100%">' +
						'</div>' +
					'</div>' 
				}
				//tipo 2: Immagine, Titolo e descrizione
				else if (type=="2")
				{
					
					//mi aspetto un solo subitem, una immagine (o anche no.. solo il titolo...)
					var img="";
					if (media.length>0) img=media[0]['url'];

					//SE SONO IN PRODOTTI STAMPO LO SLIDER DEL FRONTONE
					if(idSezione == 2 && typeof slider !== "undefined" && slider.slides.length > 0){
						html =	
						'<div id="section'+idSezione+'" class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-left" style="padding: 0px 10px 0px 10px; margin: 0px 0px 0px 0px;">' + 		
							'<div>' 
								//if (img!="") html += '<div style="margin: 0% 0% 0% 0%;padding-bottom: 5px;padding-top: 40px"><img src="images/' + img + '" alt="" style="width:100%;"></div>' 
								html += '<div id="layerslider" style="width:1300px;height:456px;margin-top:40px;"></div>'
								if (titolo!="")	html += '<div class="titleSection" style="margin-left:0%; margin-right:0%; text-align:center;  color:#b1c903;">' + titolo + '</div>'			
								html += '<div style="margin: 0% 0% 0% 0%; font-size: 15px; overflow-y:auto; text-align: justify; padding-bottom: 20px;font-weight: 500">' + testo + '</div>' +

								'<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center" style="padding: 0px; margin: 0px;">' +
								'<hr style="color: #B1C903; background-color: #B1C903; height: 2px; width:100%">' +
								'</div>' +
							'</div>' +						
						'</div>'
					}else if(idSezione == 10 && typeof slider !== "undefined" && slider.slides.length > 0) //SE SONO IN VISION STAMPO LO SLIDER DEL FRONTONE
					{
						html =	
						'<div id="section'+idSezione+'" class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-left" style="padding: 0px 10px 0px 10px; margin: 0px 0px 0px 0px;">' + 		
							'<div>' 
								//if (img!="") html += '<div style="margin: 0% 0% 0% 0%;padding-bottom: 5px;padding-top: 40px"><img src="images/' + img + '" alt="" style="width:100%;"></div>' 
								html += '<div id="layerslider" style="width:1300px;height:456px;margin-top:40px;"></div>'
								if (titolo!="")	html += '<div class="titleSection" style="margin-left:0%; margin-right:0%; text-align:center;  color:#b1c903;">' + titolo + '</div>'			
								html += '<div style="margin: 0% 0% 0% 0%; font-size: 15px; overflow-y:auto; text-align: justify; padding-bottom: 20px;font-weight: 500">' + testo + '</div>' +

								'<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center" style="padding: 0px; margin: 0px;">' +
								'<hr style="color: #B1C903; background-color: #B1C903; height: 2px; width:100%">' +
								'</div>' +
							'</div>' +						
						'</div>'
					}else{
						html =	
						'<div id="section'+idSezione+'" class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-left" style="padding: 0px 10px 0px 10px; margin: 0px 0px 0px 0px;">' + 		
							'<div>' 
								if (img!="") html += '<div style="margin: 0% 0% 0% 0%;padding-bottom: 5px;padding-top: 40px"><img src="images/' + img + '" alt="" style="width:100%;"></div>' 
								if (titolo!="")	html += '<div class="titleSection" style="margin-left:0%; margin-right:0%; text-align:center;  color:#b1c903;">' + titolo + '</div>'			
								html += '<div style="margin: 0% 0% 0% 0%; font-size: 15px; overflow-y:auto; text-align: justify; padding-bottom: 20px;font-weight: 500">' + testo + '</div>' +

								'<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center" style="padding: 0px; margin: 0px;">' +
								'<hr style="color: #B1C903; background-color: #B1C903; height: 2px; width:100%">' +
								'</div>' +
							'</div>' +						
						'</div>'
					}
					 
				}
				//tipo 3: Titolo e lista item testuali cliccabili (in 2 colonne)... tipo Allegati e Prodotti correlati
				else if (type=="3")
				{
					//alert("media length: " + media.length);
					html = '<div id="section'+idSezione+'" class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-left" style="padding: 0px 10px 0px 10px; margin: 0px 0px 0px 0px;">' 		
						//devo dividere la sezione in 2 parti (piu' la parte centrale dove metto la linea verticale)
						html += '<table style="width:100%"><tr>';		
						for(sez=1; sez<=2; sez++)
						{ 
							//html += '<div style="width=100%;float:left;">'
							html += '<td style="width:50%">'
							//per ogni item nell'array dei subitem, vedo vedere il tipo e il link
							for(i=0; i<media.length; i++)
							{ 
								if (media[i]['sotto_sezione']==sez)
								{
									//divido tra titolo e testi linkabili
									if (media[i]['tipo']=="2") //Titolo
									{
										html += '<div class="titleSection" style="margin-left:8%; margin-right:8%; text-align:center;  color:#b1c903;">' + media[i]['titolo'] + '</div>'				
									}
									else if (media[i]['tipo']=="3") //Testo con Link
									{
										html += '<div class="allegati_correlati" style="margin-left:8%; margin-right:8%; text-align:center; margin-top:5px; margin-bottom:5px;"><a href="'+ media[i]['url_link'] +'">' + media[i]['titolo'] + '</a></div>'				
									}
								}
							}
							html += '</td>';
							//html += '</div>'
							if (sez!=2) //metto la riga di separazione verticale, ma solo se non è l'ultima sotto sezione
							{
								html +='<td style="width:2px;background-color:#B1C903"></td>'
							}
						}
						html += '</td></table>';
					html += '</div>' + 
					'<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center" style="padding: 0px; margin: 0px;">' +
					'<hr style="color: #B1C903; background-color: #B1C903; height: 2px; width:100%">' +
					'</div>'
				}
				//tipo 5: Titolo e n media (foto, titolo e testo), tipo Media Blog
				else if (type=="5")
				{
					//alert("media length: " + media.length);
					html = '<div id="section'+idSezione+'" class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-left" style="padding: 0px 10px 0px 10px; margin: 0px 0px 0px 0px;">' +	
					'<div>' +
						'<div class="titleSection" style="margin-left:8%; margin-right:8%; text-align:center; color:#b1c903;">' + titolo + '</div>'	+				
						'<div>'
							//quanto media abbiamo?
							if (media.length==3) // li metto in linea
							{
								for(type5Index=0; type5Index<media.length; type5Index++)
								{ 
									//alert(media[i]['url']);
									//alert(media[i]['titolo']);
									//alert(media[i]['descrizione']);
									var testoArticolo = media[type5Index]['descrizione'];
									//alert(testoArticolo);
									//alert(testoArticolo.length);
									html += '<div style="float: left; width:33%;padding: 5px 10px 5px 10px;">' + 
										'<div style="margin: 0% 0% 0% 0%;padding-bottom: 0px;"><img src="images/' + media[type5Index]['url'] + '" alt="" style="width:100%;border: solid 1px #b1c903;"></div>' + 
										'<div class="mediaBlogTitle" style="margin-left:0%; margin-right:0%; color:#b1c903;">' + media[type5Index]['titolo'] + '</div>'				
										//se è molto lungo, vedo di dividerlo...
										//se il testo è molto lungo, vedo di dividerlo...
										if (testoArticolo.length>750)
										{
											//prendo i primi 750 caratteri
											var arrTestoDiviso = $dividiTesto(testoArticolo,750);
											var testoArticoloParte1=arrTestoDiviso[0];
											var testoArticoloParte2=arrTestoDiviso[1];
											
											
											html += '<div style="margin: 0% 0% 0% 0%; font-size: 15px; overflow-y:auto; text-align: justify; padding-bottom: 20px;">' + testoArticoloParte1 + 
											'<div id="collapseSection'+idSezione+''+type5Index+'" class="collapse">' +
												'<div style="margin: 0% 0% 0% 0%; font-size: 15px; overflow-y:auto; text-align: justify; padding-bottom: 20px;">' + testoArticoloParte2 + '</div>' +
											'</div>' +
											'<p style="margin-top:10px;"><a id="readMoreSection'+idSezione+'" data-toggle="collapse" data-target="#collapseSection'+idSezione+''+type5Index+'" class="pulsanteReadMore">MORE</a></p></div>'
										}
										else
										{
											html += '<div style="margin: 0% 0% 0% 0%; font-size: 15px; overflow-y:auto; text-align: justify; padding-bottom: 20px;">' + media[type5Index]['descrizione'] + '</div>'
										}
									html += '</div>'
								}
							}
							else if (media.length==4) // 2 sopra e 2 sotto
							{
								html += '<div style="width:100%;">'
								for(type5Index=0; type5Index<2; type5Index++)
								{ 
									//alert(media[i]['url']);
									//alert(media[i]['titolo']);
									//alert(media[i]['descrizione']);
									html += '<div style="float: left; width:50%;padding: 5px 10px 5px 10px;">' + 
										'<div style="margin: 0% 0% 0% 0%;padding-bottom: 0px;"><img src="images/' + media[type5Index]['url'] + '" alt="" style="width:100%;border: solid 1px #b1c903;"></div>' + 
										'<div class="mediaBlogTitle" style="margin-left:0%; margin-right:0%; color:#b1c903;">' + media[type5Index]['titolo'] + '</div>'	+				
										'<div style="margin: 0% 0% 0% 0%; font-size: 15px; overflow-y:auto; text-align: justify; padding-bottom: 20px;">' + media[type5Index]['descrizione'] + '</div>' +
									'</div>'
								}
								html += '</div><br clear="all"/><div style="width:100%;">'
								for(type5Index=2; type5Index<4; type5Index++)
								{ 
									//alert(media[i]['url']);
									//alert(media[i]['titolo']);
									//alert(media[i]['descrizione']);
									html += '<div style="float: left; width:50%;padding: 15px 15px 15px 15px;">' + 
										'<div style="margin: 0% 0% 0% 0%;padding-bottom: 0px;"><img src="images/' + media[type5Index]['url'] + '" alt="" style="width:100%;border: solid 1px #b1c903;"></div>' + 
										'<div class="mediaBlogTitle" style="margin-left:0%; margin-right:0%; color:#b1c903;">' + media[type5Index]['titolo'] + '</div>'	+				
										'<div style="margin: 0% 0% 0% 0%; font-size: 15px; overflow-y:auto; text-align: justify; padding-bottom: 20px;">' + media[type5Index]['descrizione'] + '</div>' +
									'</div>'
								}
								html += '</div><br clear="all"/>'
							}
						html += '</div>' + 
						'<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center" style="padding: 0px; margin: 0px;">' +
						'<hr style="color: #B1C903; background-color: #B1C903; height: 2px; width:100%">' +
						'</div>' +
					'</div>'
				}
				//tipo 6: Timeline
				else if (type=="6")
				{
					var language=$.urlParam('lang');
					//alert(language);

					var itemTl = row_data['subItemsTimeline'];

					var dataIdTimeline = new Array();
					var titoloTimeline = new Array();
					var descrizioneTimeline = new Array();
					var immagineTimeline = new Array();
					var link = new Array();

					for(c=0;c<itemTl.length;c++){
						dataIdTimeline[c+1]= itemTl[c].progressivo+"/"+itemTl[c].anno;
						titoloTimeline[c+1]=itemTl[c].titolo;
						descrizioneTimeline[c+1]=itemTl[c].descrizione;
						immagineTimeline[c+1]=itemTl[c].immagine;
						link[c+1]=itemTl[c].link;

						if(c == itemTl.length - 2){
							startTimeline = itemTl[c].progressivo+"/"+itemTl[c].anno;
						}
						
					}

					subTotalAnnoTimeline = row_data['subTotalAnnoTimeline'];
					subAnnoTimeline = row_data['subAnnoTimeline'];

					html =	
					'<div id="section'+idSezione+'" class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-left" style="padding: 0px 10px 0px 10px; margin: 0px 0px 0px 0px;">' + 		
						'<div>' +
							'<div class="titleSection" style="margin-left:0%; margin-right:0%; text-align:center; color:#b1c903;">' + titolo + '</div>'	+				

							'<div class="timelineLoader">' +
								'<img src="timeline/images/timeline/loadingAnimation.gif" />' +
							'</div>' +					 
							
							'<div class="timelineFlat tl1">'
								for(timelineIndex=1; timelineIndex<dataIdTimeline.length; timelineIndex++)
								{ 
									html +='<div class="item" data-id="'+dataIdTimeline[timelineIndex]+'" data-description="'+titoloTimeline[timelineIndex]+'">'
									if (link[timelineIndex]!="")
									{
										//è un link verso la pagina prodotti o verso altre pagine?
										if (link[timelineIndex].length<=4)
										{
											html +='<h2><a style="color:#ffffff" href="fixed.content.php?idPage=1&lang='+language+'&prod='+link[timelineIndex]+'">'+titoloTimeline[timelineIndex]+'</a></h2>'
										}
										else
										{
											html +='<h2><a target="_blank" style="color:#ffffff" href="'+link[timelineIndex]+'">'+titoloTimeline[timelineIndex]+'</a></h2>'
										}
									}
									else
									{
										//console.log(titoloTimeline[timelineIndex]);
										html +='<h2>'+titoloTimeline[timelineIndex]+'</h2>'
									}
										
									html += '<img src="images/'+immagineTimeline[timelineIndex]+'" alt="" style="max-width:100%;border: solid 1px #b1c903;"/>' +
									'<span>'+descrizioneTimeline[timelineIndex]+'</span>' +
									'</div>'
								}

							html += '</div>' +

						
						'</div>' +						
						'<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center" style="padding: 15px 0px 0px 0px; margin: 0px;">' +
						'<hr style="color: #B1C903; background-color: #B1C903; height: 2px; width:100%">' +
						'</div>' +
						
					'</div>' 
				}
				//tipo 7: Titolo, Testo e griglia di item (immagini, immagini con caption, ecc
				else if (type=="7")
				{

					clientiTxt = row_data['clientiTxt'];
					clientiImg1 = row_data['clientiImg1'];
					clientiImg2 = row_data['clientiImg2'];
					clientiImg3 = row_data['clientiImg3'];

					//console.log(clientiTxt);
					//console.log(clientiImg1);
					//console.log(clientiImg2);
					//console.log(clientiImg3);

					//verranno disposti 8 elementi per riga.. vediamo quante righe...
					var righe=3;
					var element_width = ($('#mainContainerDiv').width()*0.80)/8;
					var altezza_sezione = (element_width*righe*0.82)+90;
					//element_width=140;
					//alert(altezza_sezione);

					var htmlImgClienti1 = "";
					for(var i=0;i<clientiImg1.length;i++){
						htmlImgClienti1+='<div class="wall_item" style="background: #b1c903; width:'+element_width+'px; height:'+element_width+'px;"><img src="images/clienti/'+clientiImg1[i]["path"]+'" alt="" title="'+clientiImg1[i]["nome"]+'" style="max-width:100%"></div>';
					}

					var htmlImgClienti2 = "";
					for(var i=0;i<clientiImg2.length;i++){
						htmlImgClienti2+='<div class="wall_item" style="background: #b1c903; width:'+element_width+'px; height:'+element_width+'px;"><img src="images/clienti/'+clientiImg2[i]["path"]+'" alt="" title="'+clientiImg2[i]["nome"]+'" style="max-width:100%"></div>';
					}

					var htmlImgClienti3 = "";
					for(var i=0;i<clientiImg3.length;i++){
						htmlImgClienti3+='<div class="wall_item" style="background: #b1c903; width:'+element_width+'px; height:'+element_width+'px;"><img src="images/clienti/'+clientiImg3[i]["path"]+'" alt="" title="'+clientiImg3[i]["nome"]+'" style="max-width:100%"></div>';
					}

					html =	
					'<div id="section'+idSezione+'" class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-left" style="padding: 0px 10px 0px 10px; margin: 0px 0px 0px 0px">' + 		
						'<div id="test">' +
							'<div id="titolo" class="titleSection" style="margin-left:0%; margin-right:0%; text-align:center; color:#b1c903;">' + titolo + '</div>'	+				
							'<div id="testo" style="margin: 0% 0% 0% 0%; font-size: 15px; overflow-y:auto; text-align: justify; padding-bottom: 20px;">' + testo + '</div>' +

							'<div id="wall_container" style="margin-bottom: 50px;">' + htmlImgClienti1 +
							'</div>' +
							'<div id="wall_container2" style="margin-bottom: 50px;">' + htmlImgClienti2 +
							'</div>' +
							'<div id="wall_container3">' + htmlImgClienti3 +
							'</div>' +

							'<div style="float: left; width:33%;padding: 25px 10px 5px 10px;font-size: 16px;line-height: 16px;"><p>' + clientiTxt[0]["testo_colonna"].replace(/\r?\n/g, '</p><p>') + '</p>' +
								//'<p>Cartotecnica Vitellio</p><p>Villa Conca Marco</p><p>Residence Le Palme</p><p>Tenuta Flora Maria</p><p>Residenza Gemma</p><p>Agenzia Viaggi Sasinae</p><p>Hotel Aloisi</p><p>I Teatri -Reggio Emilia</p><p>Area Marina Protetta - Porto Cesareo</p><p>Parco dei Nebrodi</p><p>Area Marina Protetta - Capo Rizzuto</p><p>Parco Nazionale del Pollino</p><p>Parco delle Orobie</p><p>Parco Oglio Sud</p><p>Parco Naturale Veglia</p><p>Fiera Bolzano</p><p>Area Marina Protetta Portofino</p><p>Parco Regionale Abbazia di Monte Veglio</p><p>Parco La Mandria</p><p>Parco Regionale dei Colli di Bergamo</p>'+
							'</div>' +
							'<div style="float: left; width:33%;padding: 25px 10px 5px 10px;font-size: 16px;line-height: 16px;"><p>' + clientiTxt[1]["testo_colonna"].replace(/\r?\n/g, '</p><p>') + '</p>' +
								//'<p>Area Marina Protetta Punta Capannella</p><p>ScopriMiniera</p><p>www.informazionecampania.it</p><p>Il Piccolo d&apos;Abruzzo</p><p>Gran Fondo della Pace</p><p>Fiera del Cioccolato Artigianale</p><p>Bike Expo</p><p>Blu Nautilus</p><p>AmaTour World Cup 2008</p><p>KunStart08</p><p>Tutti Sapori</p><p>Parco Regionale di Montemarcello-Magra</p><p>Riserva Naturale dello Zingaro</p><p>Parco Regionale delle Orobie Valtellinesi</p><p>Parco Nazionale dell&apos;Alta Murgia</p><p>Parco Regionale dei Nebrodi</p><p>Area Marina Protetta Capo Rizzuto</p><p>Case ad Oriente</p><p>Area Marina Protetta di Porto Cesareo</p>'+
							'</div>' +
							'<div style="float: left; width:33%;padding: 25px 10px 5px 10px;font-size: 16px;line-height: 16px;"><p>' + clientiTxt[2]["testo_colonna"].replace(/\r?\n/g, '</p><p>') + '</p>' +
								//'<p>Parco Fluviale dell&apos;Alcantara</p><p>Area Marina Protetta di Porto Cesareo</p><p>Parco Nazionale Appennino Tosco-Emiliano</p><p>Parco del Po Cuneese</p><p>Parco Regionale del Partenio</p><p>Area Marina Protetta Penisola del Sinis - Isola di Mal di Ventre</p><p>Parco della Valle del Lambro</p><p>Parco Regionale Migliarino San Rossore Massaciuccoli</p><p>Parco Fluviale Oglio Sud</p><p>Parco Regionale La Mandria</p><p>Parco Regionale dell&apos;Abbazia di Monteveglio</p><p>Parco Naturale dell&apos;Alta Valsesia</p><p>Parco Regionale di Veglia - Devero</p><p>Parco Regionale Sasso Simone e Simoncello</p><p>Parco Fluviale Regionale del Taro</p>'+
							'</div>' +
							
						'</div>' +						
					'</div>' +
					'<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center" style="padding-top: 10px; margin: 0px;">' +
					'<hr style="color: #B1C903; background-color: #B1C903; height: 2px; width:100%">' +
					'</div>'
					
				}
				//tipo 8: Titolo, Immagine + 3 thumb, Testo di fianco
				else if (type=="8")
				{
					//alert("media length: " + media.length);
					//mi aspetto 4 immagini, la prima la metto nel box centrale e le altre in quelli laterali
					var img1=media[0]['url'];

					var img2="";
					if (typeof media[1] !== "undefined") {
					    var img2=media[1]['url'];
					}
					var img3="";
					if (typeof media[2] !== "undefined") {
					    var img3=media[2]['url'];
					}
					var img4="";
					if (typeof media[3] !== "undefined") {
					    var img4=media[3]['url'];
					}

					/*var img2=media[1]['url'];
					var img3=media[2]['url'];
					var img4=media[3]['url'];*/
					
					html =	
					'<div id="section'+idSezione+'" class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-left" style="padding: 0px 10px 0px 10px; margin: 0px 0px 0px 0px;">' + 		
						'<div id="test">' +
							'<div id="titolo" class="titleSection" style="margin-left:0%; margin-right:0%; text-align:center;  color:#b1c903;">' + titolo + '</div>'	+				
							'<div id="testo" style="display:block;  margin: 0% 0% 0% 0%; font-size: 15px; overflow-y:auto; text-align: justify; padding-bottom: 20px;">' + 
								'<div style="margin: 0% 0% 0% 0%;padding-bottom: 5px;width:45%;float: left;">' + 
									'<div style="float: left; width:76%;padding: 10px 0px 10px 10px;"><img id="myGridImg_section'+idSezione+'" src="images/'+ img1 +'" alt="" style="max-width:100%;border: solid 2px #b1c903"></div>' +
									'<div style="float: left; width:24%;display:block;">' + 
										'<div style="padding: 10px 10px 10px 10px;"><img id="myGridThumb1_section'+idSezione+'" src="images/'+ img2 +'" alt="" style="max-width:100%;border: solid 2px #b1c903;"></div>' +
										'<div style="padding: 5px 10px 5px 10px;"><img id="myGridThumb2_section'+idSezione+'" src="images/'+ img3 +'" alt="" style="max-width:100%;border: solid 2px #b1c903;"></div>' +
										'<div style="padding: 10px 10px 10px 10px;"><img id="myGridThumb3_section'+idSezione+'" src="images/'+ img4 +'" alt="" style="max-width:100%;border: solid 2px #b1c903;"></div>' +
									'</div>' +
								'</div>'
							
								//se il testo è molto lungo, vedo di dividerlo...
								if (testo.length>400)
								{
									var arrTestoDiviso = $dividiTesto(testo,400);
									//prendo i primi 100 caratteri
									var testoArticoloParte1=arrTestoDiviso[0];
									var testoArticoloParte2=arrTestoDiviso[1];
									html += testoArticoloParte1 + 
									'<div id="collapseSection'+idSezione+'" class="collapse">' +
										'<div style="display:block;  margin: 0% 0% 0% 0%; font-size: 15px; text-align: justify; padding-bottom: 20px;">' + testoArticoloParte2 + '</div>' +
									'</div>'
									//c'e' un url associato a questa sezione?
									if (url=="" || url==null)
									{
										html +='<p style="margin-top:10px;"><a id="readMoreSection'+idSezione+'" data-toggle="collapse" data-target="#collapseSection'+idSezione+'" class="pulsanteReadMore">MORE</a>'
										'</p></div>'
									}
									else
									{
										html +='<p style="margin-top:10px;"><a id="readMoreSection'+idSezione+'" data-toggle="collapse" data-target="#collapseSection'+idSezione+'" class="pulsanteReadMore">MORE</a><a class="pulsanteReadMore" href="'+url+'" style="margin-left:10px;" target="_blank">WEBSITE</a>'
										'</p></div>'
									}
								}
								else
								{
									html += testo
								}
								
								//testo +
						
							
							html += '</div>'
							
							
							
							//eventuali allegati
							if (allegati.length>0)
							{
								// ci sono allegati... metto inizialmente una riga di separazione
								html +='<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center" style="padding: 0px; margin: 0px;">' +
								'<hr style="color: #B1C903; background-color: #B1C903; height: 1px; width:90%;margin-left:5%">' +
								'</div>'
								
								var allegatoTipo1 = new Array();
								var allegatoTipo2 = new Array();
								var allegatoTipo3 = new Array();
								//alert("trovati allegati: " + allegati.length);
								//li devo dividere per tipo
								for(allegatiIndex=0; allegatiIndex<allegati.length; allegatiIndex++)
								{ 
									if (allegati[allegatiIndex]['tipo']==1) //allegato
									{
										allegatoTipo1.push(allegati[allegatiIndex]);
									}
									else if (allegati[allegatiIndex]['tipo']==2) //collegamento
									{
										allegatoTipo2.push(allegati[allegatiIndex]);
									}
									else if (allegati[allegatiIndex]['tipo']==3) //rassegna stampa
									{
										allegatoTipo3.push(allegati[allegatiIndex]);
									}
								}								
								//alert("Allegato tipo 1: " + allegatoTipo1.length);
								//alert("Allegato tipo 2: " + allegatoTipo2.length);
								//alert("Allegato tipo 3: " + allegatoTipo3.length);
								var numSezioni=0;
								if (allegatoTipo1.length>0) numSezioni++;
								if (allegatoTipo2.length>0) numSezioni++;
								if (allegatoTipo3.length>0) numSezioni++;
								var larghezzaSez = (100/numSezioni)-0.02;
								
								var icona="";

								var language=$("#langHidden").val();
								if(language == "IT"){
									var lblAllegati = "ALLEGATI";
									var lblLink = "COLLEGAMENTI";
									var lblRassegnaStampa = "RASSEGNA STAMPA";
								}else{
									var lblAllegati = "ATTACHMENTS";
									var lblLink = "LINK";
									var lblRassegnaStampa = "PRESS AREA";
								}
								
								html += '<div style="display: table;padding: 5px 10px 5px 10px;width:100%"><div style="display: table-row;padding: 5px 10px 5px 10px;">'

								if (allegatoTipo1.length>0)
								{
									html += '<div style="display: table-cell;width:'+larghezzaSez+'%;">' + 
										'<div class="titleAllegati" style="margin-left:8%; margin-right:8%; text-align:center;  color:#b1c903;">'+lblAllegati+'</div>'
									for(allegatiIndex=0; allegatiIndex<allegatoTipo1.length; allegatiIndex++)
									{
										//icona
										icona = $.findIconByType(allegatoTipo1[allegatiIndex]['icona']);
										var link="";
										if (allegatoTipo1[allegatiIndex]['isUrl']==0) link="allegati/" + allegatoTipo1[allegatiIndex]['url'];
										else link=allegatoTipo1[allegatiIndex]['url'];
										html += '<div class="itemAllegati" style="margin-left:8%; margin-right:8%; text-align:left;  color:#58585a;"><a target="_blank" href="'+ link +'"><img src="images/'+icona+'" height="20px" style="margin-right:10px;"\>'+allegatoTipo1[allegatiIndex]['testo']+'</a></div>'
									}
									html += '</div>'								
								}
								if (allegatoTipo2.length>0)
								{
									//ci sono allegati di tipo 1(per mettere una riga verticale)
									if (allegatoTipo1.length>0) //metto un'altra cella con una riga verticale
									{
										html += '<div style="display: table-cell;width:2px;background-color:#B1C903"></div>'
									}
									html += '<div style="display: table-cell;width:'+larghezzaSez+'%;">' + 
										'<div class="titleAllegati" style="margin-left:8%; margin-right:8%; text-align:center;  color:#b1c903;">'+lblLink+'</div>'
									for(allegatiIndex=0; allegatiIndex<allegatoTipo2.length; allegatiIndex++)
									{
										//icona
										icona = $.findIconByType(allegatoTipo2[allegatiIndex]['icona']);
										var link="";
										if (allegatoTipo2[allegatiIndex]['isUrl']==0) link="allegati/" + allegatoTipo2[allegatiIndex]['url'];
										else link=allegatoTipo2[allegatiIndex]['url'];
										html += '<div class="itemAllegati" style="margin-left:8%; margin-right:8%; text-align:left;  color:#58585a;"><a target="_blank" href="'+ link +'"><img src="images/'+icona+'" height="20px" style="margin-right:10px;"\>'+allegatoTipo2[allegatiIndex]['testo']+'</a></div>'
									}									
									html += '</div>'								
								}
								if (allegatoTipo3.length>0)
								{
									//ci sono allegati di tipo 1 o 2(per mettere una riga verticale)
									if ((allegatoTipo1.length>0) || (allegatoTipo2.length>0))
									{
										html += '<div style="display: table-cell;width:1px;background-color:#B1C903"></div>'
									}
									html += '<div style="display: table-cell;width:'+larghezzaSez+'%;">' + 
										'<div class="titleAllegati" style="margin-left:8%; margin-right:8%; text-align:center;  color:#b1c903;">'+lblRassegnaStampa+'</div>'
									for(allegatiIndex=0; allegatiIndex<allegatoTipo3.length; allegatiIndex++)
									{
										//icona
										icona = $.findIconByType(allegatoTipo3[allegatiIndex]['icona']);
										var link="";
										if (allegatoTipo3[allegatiIndex]['isUrl']==0) link="allegati/" + allegatoTipo3[allegatiIndex]['url'];
										else link=allegatoTipo3[allegatiIndex]['url'];
										html += '<div class="itemAllegati" style="margin-left:8%; margin-right:8%; text-align:left;  color:#58585a;"><a target="_blank" href="'+ link +'"><img src="images/'+icona+'" height="20px" style="margin-right:10px;"\>'+allegatoTipo3[allegatiIndex]['testo']+'</a></div>'
									}									
									html += '</div>'								
								}
							}
						
						html += '</div></div>' +						
						'<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center" style="padding: 0px; margin: 0px;">' +
						'<hr style="color: #B1C903; background-color: #B1C903; height: 2px; width:100%">' +
						'</div>' +
					'</div>' 
				}
				//tipo 10: Titolo, Immagine + 3 thumb, Testo sotto
				else if (type=="10")
				{
					//alert("media length: " + media.length);
					//mi aspetto 4 immagini, la prima la metto nel box centrale e le altre in quelli laterali
					var img1=media[0]['url'];
					var img2=media[1]['url'];
					var img3=media[2]['url'];
					var img4=media[3]['url'];
					
					html =	
					'<div id="section'+idSezione+'" class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-left" style="padding: 10px 10px 0px 10px; margin: 0px 0px 0px 0px;">' + 		
						'<div id="test">' +
							'<div id="titolo" class="titleSection" style="margin-left:0%; margin-right:0%; text-align:center;  color:#b1c903;">' + titolo + '</div>'	+				
							'<div style="margin: 0% 0% 0% 0%;padding-bottom: 20px;">' + 
								'<div style="float: left; width:76%;padding: 10px 10px 10px 10px;"><img id="myGridImg_section'+idSezione+'" src="images/'+ img1 +'" alt="" style="max-width:100%;border: solid 2px #b1c903"></div>' +
								'<div style="float: left; width:24%;display:block;">' + 
									'<div style="padding: 10px 10px 10px 10px;"><img id="myGridThumb1_section'+idSezione+'" src="images/'+ img2 +'" alt="" style="max-width:100%;border: solid 2px #b1c903;"></div>' +
									'<div style="padding: 9px 10px 9px 10px;"><img id="myGridThumb2_section'+idSezione+'" src="images/'+ img3 +'" alt="" style="max-width:100%;border: solid 2px #b1c903;"></div>' +
									'<div style="padding: 10px 10px 10px 10px;"><img id="myGridThumb3_section'+idSezione+'" src="images/'+ img4 +'" alt="" style="max-width:100%;border: solid 2px #b1c903;"></div>' +
								'</div>' +
							'</div><br clear="all"/>' +
							'<div id="testo" style="display:block;  margin: 0% 0% 0% 0%; font-size: 15px; overflow-y:auto; text-align: justify; padding-bottom: 20px;">' + testo + '</div>'

							//eventuali allegati
							if (allegati.length>0)
							{
								// ci sono allegati... metto inizialmente una riga di separazione
								html +='<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center" style="padding: 0px; margin: 0px;">' +
								'<hr style="color: #B1C903; background-color: #B1C903; height: 1px; width:90%;margin-left:5%">' +
								'</div>'
								
								var allegatoTipo1 = new Array();
								var allegatoTipo2 = new Array();
								var allegatoTipo3 = new Array();
								//alert("trovati allegati: " + allegati.length);
								//li devo dividere per tipo
								for(allegatiIndex=0; allegatiIndex<allegati.length; allegatiIndex++)
								{ 
									if (allegati[allegatiIndex]['tipo']==1) //allegato
									{
										allegatoTipo1.push(allegati[allegatiIndex]);
									}
									else if (allegati[allegatiIndex]['tipo']==2) //collegamento
									{
										allegatoTipo2.push(allegati[allegatiIndex]);
									}
									else if (allegati[allegatiIndex]['tipo']==3) //rassegna stampa
									{
										allegatoTipo3.push(allegati[allegatiIndex]);
									}
								}								
								//alert("Allegato tipo 1: " + allegatoTipo1.length);
								//alert("Allegato tipo 2: " + allegatoTipo2.length);
								//alert("Allegato tipo 3: " + allegatoTipo3.length);
								var numSezioni=0;
								if (allegatoTipo1.length>0) numSezioni++;
								if (allegatoTipo2.length>0) numSezioni++;
								if (allegatoTipo3.length>0) numSezioni++;
								var larghezzaSez = (100/numSezioni)-0.02;
								
								var icona="";

								var language=$("#langHidden").val();
								if(language == "IT"){
									var lblAllegati = "ALLEGATI";
									var lblLink = "COLLEGAMENTI";
									var lblRassegnaStampa = "RASSEGNA STAMPA";
								}else{
									var lblAllegati = "ATTACHMENTS";
									var lblLink = "LINK";
									var lblRassegnaStampa = "PRESS AREA";
								}
								
								html += '<div style="display: table;padding: 5px 10px 5px 10px;width:100%"><div style="display: table-row;padding: 5px 10px 5px 10px;">'
								
								if (allegatoTipo1.length>0)
								{
									html += '<div style="display: table-cell;width:'+larghezzaSez+'%;">' + 
										'<div class="titleAllegati" style="margin-left:8%; margin-right:8%; text-align:center;  color:#b1c903;">'+lblAllegati+'</div>'
									for(allegatiIndex=0; allegatiIndex<allegatoTipo1.length; allegatiIndex++)
									{
										//icona
										icona = $.findIconByType(allegatoTipo1[allegatiIndex]['icona']);
										var link="";
										if (allegatoTipo1[allegatiIndex]['isUrl']==0) link="allegati/" + allegatoTipo1[allegatiIndex]['url'];
										else link=allegatoTipo1[allegatiIndex]['url'];
										html += '<div class="itemAllegati" style="margin-left:8%; margin-right:8%; text-align:left;  color:#58585a;"><a target="_blank" href="'+ link +'"><img src="images/'+icona+'" height="20px" style="margin-right:10px;"\>'+allegatoTipo1[allegatiIndex]['testo']+'</a></div>'
									}
									html += '</div>'								
								}
								if (allegatoTipo2.length>0)
								{
									//ci sono allegati di tipo 1(per mettere una riga verticale)
									if (allegatoTipo1.length>0) //metto un'altra cella con una riga verticale
									{
										html += '<div style="display: table-cell;width:2px;background-color:#B1C903"></div>'
									}
									html += '<div style="display: table-cell;width:'+larghezzaSez+'%;">' + 
										'<div class="titleAllegati" style="margin-left:8%; margin-right:8%; text-align:center;  color:#b1c903;">'+lblLink+'</div>'
									for(allegatiIndex=0; allegatiIndex<allegatoTipo2.length; allegatiIndex++)
									{
										//icona
										icona = $.findIconByType(allegatoTipo2[allegatiIndex]['icona']);
										var link="";
										if (allegatoTipo2[allegatiIndex]['isUrl']==0) link="allegati/" + allegatoTipo2[allegatiIndex]['url'];
										else link=allegatoTipo2[allegatiIndex]['url'];
										html += '<div class="itemAllegati" style="margin-left:8%; margin-right:8%; text-align:left;  color:#58585a;"><a target="_blank" href="'+ link +'"><img src="images/'+icona+'" height="20px" style="margin-right:10px;"\>'+allegatoTipo2[allegatiIndex]['testo']+'</a></div>'
									}									
									html += '</div>'								
								}
								if (allegatoTipo3.length>0)
								{
									//ci sono allegati di tipo 1 o 2(per mettere una riga verticale)
									if ((allegatoTipo1.length>0) || (allegatoTipo2.length>0))
									{
										html += '<div style="display: table-cell;width:1px;background-color:#B1C903"></div>'
									}
									html += '<div style="display: table-cell;width:'+larghezzaSez+'%;">' + 
										'<div class="titleAllegati" style="margin-left:8%; margin-right:8%; text-align:center;  color:#b1c903;">'+lblRassegnaStampa+'</div>'
									for(allegatiIndex=0; allegatiIndex<allegatoTipo3.length; allegatiIndex++)
									{
										//icona
										icona = $.findIconByType(allegatoTipo3[allegatiIndex]['icona']);
										var link="";
										if (allegatoTipo3[allegatiIndex]['isUrl']==0) link="allegati/" + allegatoTipo3[allegatiIndex]['url'];
										else link=allegatoTipo3[allegatiIndex]['url'];
										html += '<div class="itemAllegati" style="margin-left:8%; margin-right:8%; text-align:left;  color:#58585a;"><a target="_blank" href="'+ link +'"><img src="images/'+icona+'" height="20px" style="margin-right:10px;"\>'+allegatoTipo3[allegatiIndex]['testo']+'</a></div>'
									}									
									html += '</div>'								
								}
							}
							
							
							
						html += '</div>' +						
						'<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center" style="padding: 0px; margin: 0px;">' +
						'<hr style="color: #B1C903; background-color: #B1C903; height: 2px; width:100%">' +
						'</div>' +
					'</div>' 
				}
				//tipo 9: Titolo, descrizione e griglia di immagini (competenze)
				else if (type=="9")
				{
					//creo la sezione
					html =	
					'<div id="section'+idSezione+'" class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-left" style="padding: 10px 10px 0px 10px; margin: 0px 0px 0px 0px;">' + 		
						'<div id="competenzeTitle" class="titleSection" style="margin-left:8%; margin-right:8%; text-align:center; color:#b1c903;">' + titolo + '</div>'	+				
						'<div style="margin: 0% 0% 0% 0%; font-size: 15px; overflow-y:auto; text-align: justify; padding-bottom: 20px;">' + testo + '</div>' +

						'<div id="competenzeDivTest1" style="padding-left:5%;padding-right:5%">' +
							'<div style="float: left; width:20%;padding: 5px 5px 5px 5px;margin:auto;text-align:center;"><img id="competenzeItem1" src="<?=MStranslate("vision_img1")?>" alt="" style="max-width:100%;"></div>' +
							'<div style="float: left; width:20%;padding: 5px 5px 5px 5px;margin:auto;text-align:center;"><img id="competenzeItem2" src="<?=MStranslate("vision_img2")?>" alt="" style="max-width:100%;"></div>' +
							'<div style="float: left; width:20%;padding: 5px 5px 5px 5px;margin:auto;text-align:center;"><img id="competenzeItem3" src="<?=MStranslate("vision_img3")?>" alt="" style="max-width:100%;"></div>' +
							'<div style="float: left; width:20%;padding: 5px 5px 5px 5px;margin:auto;text-align:center;"><img id="competenzeItem4" src="<?=MStranslate("vision_img4")?>" alt="" style="max-width:100%;"></div>' +
							'<div style="float: left; width:20%;padding: 5px 5px 5px 5px;margin:auto;text-align:center;"><img id="competenzeItem5" src="<?=MStranslate("vision_img5")?>" alt="" style="max-width:100%;"></div>' +
							'<div id="collapseCompetenze1" class="collapse" style="padding-left:20px;padding-right:20px;clear:both;">' +
								'<div style="font-size:1px;">&nbsp;</div>' +
								'<div style="background-color:#dddddd;">' +
									'<div style="font-weight: 900; color: #58585a; font-size: 52px;padding: 0px 0 10px;margin-bottom: 10px;padding-top: 15px; line-height: 40px;text-align: center;" id="collapseTitoloCompetenze1">ciao ciao</div>' +
									'<div style="padding-left:8%;padding-right:8%">' +
										'<div style="display:table-row">' +
											'<div style="display:table-cell;width:10%">' +
												'<img src="images/virgolette-inizio.png" width="60px" style="margin-right:5px;"/>' +
											'</div>' +
											'<p id="collapseVirgolettatoCompetenze1" style="font-style:italic;text-align: justify;display:table-cell;width:80%;padding-top:20px;padding-bottom:20px;font-weight: 400;font-size: 16px;line-height: 22px;color: #999;"></p>' +
											'<div style="display:table-cell;width:10%;height:100%;position:relative;">' +
												'<img src="images/virgolette-fine.png" width="60px" style="margin-left:5px;position: absolute;bottom: 0px;"/>' +
											'</div>' +
											'<br clear="all"/>' +
										'</div>' +
									'</div>' +
									'<div style="font-weight: 400;font-size: 16px;line-height: 22px;color: #999;padding-left:3%;padding-right:3%" id="collapseTestoCompetenze1">ciao ciao</div>' +
									'<div style="padding:20px;text-align: center;">' +
										'<a id="collapseLinkCompetenze1" class="pulsanteReadMore" href="" style="font-weight: 900;font-size: 14px;background-color: transparent;margin-left:10px;padding:10px"><?=MStranslate("read_more")?></a>' +
									'</div>' +
								'</div>' +
							'</div>' +
						'</div>' + 
						'<div id="competenzeDivTest2" style="padding-left:5%;padding-right:5%">' +
							'<div style="float: left; width:20%;padding: 5px 5px 5px 5px;margin:auto;text-align:center;"><img id="competenzeItem6" src="<?=MStranslate("vision_img6")?>" alt="" style="max-width:100%;"></div>' +
							'<div style="float: left; width:20%;padding: 5px 5px 5px 5px;margin:auto;text-align:center;"><img id="competenzeItem7" src="<?=MStranslate("vision_img7")?>" alt="" style="max-width:100%;"></div>' +
							'<div style="float: left; width:20%;padding: 5px 5px 5px 5px;margin:auto;text-align:center;"><img id="competenzeItem8" src="<?=MStranslate("vision_img8")?>" alt="" style="max-width:100%;"></div>' +
							'<div style="float: left; width:20%;padding: 5px 5px 5px 5px;margin:auto;text-align:center;"><img id="competenzeItem9" src="<?=MStranslate("vision_img9")?>" alt="" style="max-width:100%;"></div>' +
							'<div style="float: left; width:20%;padding: 5px 5px 5px 5px;margin:auto;text-align:center;"><img id="competenzeItem10" src="<?=MStranslate("vision_img10")?>" alt="" style="max-width:100%;"></div>' +
							'<div id="collapseCompetenze2" class="collapse" style="padding-left:20px;padding-right:20px;clear:both;">' +
									'<div style="font-size:1px;">&nbsp;</div>' +
									'<div style="background-color:#dddddd;">' +
										'<div style="font-weight: 900; color: #58585a; font-size: 52px;padding: 0px 0 10px;margin-bottom: 10px;padding-top: 15px; line-height: 40px;text-align: center;" id="collapseTitoloCompetenze2">ciao ciao</div>' +
										'<div style="padding-left:8%;padding-right:8%">' +
											'<div style="display:table-row">' +
												'<div style="display:table-cell;width:10%">' +
													'<img src="images/virgolette-inizio.png" width="60px" style="margin-right:5px;"/>' +
												'</div>' +
												'<p id="collapseVirgolettatoCompetenze2" style="font-style:italic;text-align: justify;display:table-cell;width:80%;padding-top:20px;padding-bottom:20px;font-weight: 400;font-size: 16px;line-height: 22px;color: #999;"></p>' +
												'<div style="display:table-cell;width:10%;height:100%;position:relative;">' +
													'<img src="images/virgolette-fine.png" width="60px" style="margin-left:5px;position: absolute;bottom: 0px;"/>' +
												'</div>' +
												'<br clear="all"/>' +
											'</div>' +
										'</div>' +
										'<div style="font-weight: 400;font-size: 16px;line-height: 22px;color: #999;padding-left:3%;padding-right:3%" id="collapseTestoCompetenze2">ciao ciao</div>' +
										'<div style="padding:20px;text-align: center;"><a id="collapseLinkCompetenze2" class="pulsanteReadMore" href="" style="font-weight: 900;font-size: 14px;background-color: transparent;margin-left:10px;padding:10px"><?=MStranslate("read_more")?></a></div>' +
									'</div>' +
							'</div>' +
						'</div>' + 
					'</div>' + 
					
					'<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center" style="padding-top: 25px; margin: 0px;">' +
					'<hr style="color: #B1C903; background-color: #B1C903; height: 2px; width:100%">' +
					'</div>'
				}
				//tipo 11: grafico competenze
				else if (type=="11")
				{
					//alert("arrivato");
					html =	
					'<div id="sectionCompetenze" class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-left" style="padding: 10px 10px 0px 10px; margin: 0px 0px 0px 0px;">' +
					'<canvas id="viewport" width="800" height="600" ></canvas></div>'				
				}
				//tipo 12: Titolo, Immagine, Testo di fianco
				else if (type=="12")
				{
					//alert("media length: " + media.length);
					//mi aspetto 1 immagine
					var img1=media[0]['url'];

					$('.fancybox-thumbs').fancybox({
						prevEffect : 'none',
						nextEffect : 'none',
						closeBtn  : true,
						arrows    : true,
						nextClick : true,
						helpers : {
							thumbs : {
								width  : 50,
								height : 50
							}
						}
					});

					var widthImg = "width:45%";
					var bordImgDefault = "border: solid 2px #b1c903;";
					if($.urlParam('idPage') == 1){
						widthImg = "width:25%";
						bordImgDefault = "";
					}
					
					html =	
					'<div id="section'+idSezione+'" class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-left" style="padding: 0px 10px 0px 10px; margin: 0px 0px 0px 0px;">' + 		
						'<div id="test">' +
							'<div id="titolo" class="titleSection" style="margin-left:0%; margin-right:0%; text-align:center;  color:#b1c903;">' + titolo + '</div>'	+				
							'<div id="testo" style="display:block;  margin: 0% 0% 0% 0%; font-size: 15px; overflow-y:auto; text-align: justify; padding-bottom: 20px;">' + 
								'<div style="margin: 0% 0% 0% 0%;padding-bottom: 5px;'+widthImg+';float: left;">' + 
									'<div style="float: left; width:95%;padding: 10px 0px 10px 10px;"><img src="images/'+ img1 +'" alt="" style="max-width:100%;'+bordImgDefault+'"></div>' +
								'</div>'
							
								//se il testo è molto lungo, vedo di dividerlo...
								if (testo.length>450)
								{
									//console.log(testo);
									var arrTestoDiviso = $dividiTesto(testo,450);
									//prendo i primi x caratteri
									var testoArticoloParte1=arrTestoDiviso[0];
									var testoArticoloParte2=arrTestoDiviso[1];
									//puo' darsi che la divisione habbia generato una parte 2 vuota (erano poco più di x caratteri e ha messo tutto nella prima parte...)
									if (testoArticoloParte2=="")
									{
										html += testoArticoloParte1;
									}
									else
									{	
										html += testoArticoloParte1 + 
										'<div id="collapseSection'+idSezione+'" class="collapse">' +
											'<div style="display:block;  margin: 0% 0% 0% 0%; font-size: 15px; text-align: justify; padding-bottom: 20px;">' + testoArticoloParte2 + '</div>' +
										'</div>';

										//ci sono screenshot associate a questa sezione?
										var scren = '';
										if(media.length > 1){
											scren ='<a class="pulsanteReadMore" href="javascript:void(0);" onclick="openScreenshot('+idSezione+')" style="margin-left:10px;" target="_blank">SCREENSHOT</a>';
										}
								
										//c'e' un url associato a questa sezione?
										if (url=="" || url==null)
										{
											html +='<p style="margin-top:10px;"><a id="readMoreSection'+idSezione+'" data-toggle="collapse" data-target="#collapseSection'+idSezione+'" class="pulsanteReadMore">MORE</a>'+
											scren+'</p></div>'
										}
										else
										{
											html +='<p style="margin-top:10px;"><a id="readMoreSection'+idSezione+'" data-toggle="collapse" data-target="#collapseSection'+idSezione+'" class="pulsanteReadMore">MORE</a><a class="pulsanteReadMore" href="'+url+'" style="margin-left:10px;" target="_blank">WEBSITE</a>'+
											scren+'</p></div>'
										}
									}
								}
								else
								{
									html += testo
								}
								
								//testo +
						
							
							html += '</div>'

							//ci sono icone? stampo le selezionate
							if(icons.length != 0){
								html +='<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center" style="padding: 0px; margin: 0px;margin-bottom: 30px;">';
								for(icn=0; icn<icons.length; icn++)
								{ 
									html += '<div class="col-xs-12 col-sm-6 col-md-4" style="margin-bottom:10px">'+    
						                        '<img src="./icon/'+icons[icn].path+'" style="width:100%;max-width:350px;" />'+
						                    '</div>';
								}
								html +='</div>';
							}

							//ci sono screenshot? stampo i path nascosti
							if(media.length > 1){
								html += '<div style="display:none">';
								for(imm=1; imm<media.length; imm++){ 
									html += '<a class="fancybox-thumbs" data-fancybox-group="thumb'+idSezione+'" href="./images/'+media[imm]['url']+'"><img src="./images/'+media[imm]['url']+'" alt="" /></a>';
								}
								html += '</div>';
							}

							//eventuali allegati
							if (allegati.length>0)
							{
								// ci sono allegati... metto inizialmente una riga di separazione
								html +='<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center" style="padding: 0px; margin: 0px;">' +
								'<hr style="color: #B1C903; background-color: #B1C903; height: 1px; width:90%;margin-left:5%">' +
								'</div>'
								
								var allegatoTipo1 = new Array();
								var allegatoTipo2 = new Array();
								var allegatoTipo3 = new Array();
								//alert("trovati allegati: " + allegati.length);
								//li devo dividere per tipo
								for(allegatiIndex=0; allegatiIndex<allegati.length; allegatiIndex++)
								{ 
									if (allegati[allegatiIndex]['tipo']==1) //allegato
									{
										allegatoTipo1.push(allegati[allegatiIndex]);
									}
									else if (allegati[allegatiIndex]['tipo']==2) //collegamento
									{
										allegatoTipo2.push(allegati[allegatiIndex]);
									}
									else if (allegati[allegatiIndex]['tipo']==3) //rassegna stampa
									{
										allegatoTipo3.push(allegati[allegatiIndex]);
									}
								}								
								//alert("Allegato tipo 1: " + allegatoTipo1.length);
								//alert("Allegato tipo 2: " + allegatoTipo2.length);
								//alert("Allegato tipo 3: " + allegatoTipo3.length);
								var numSezioni=0;
								if (allegatoTipo1.length>0) numSezioni++;
								if (allegatoTipo2.length>0) numSezioni++;
								if (allegatoTipo3.length>0) numSezioni++;
								var larghezzaSez = 100/numSezioni;

								var language=$("#langHidden").val();
								if(language == "IT"){
									var lblAllegati = "ALLEGATI";
									var lblLink = "COLLEGAMENTI";
									var lblRassegnaStampa = "RASSEGNA STAMPA";
								}else{
									var lblAllegati = "ATTACHMENTS";
									var lblLink = "LINK";
									var lblRassegnaStampa = "PRESS AREA";
								}
								
								var icona="";
								
								html += '<div style="display: table;padding: 5px 10px 5px 10px;width:100%"><div style="display: table-row;padding: 5px 10px 5px 10px;">'
								
								if (allegatoTipo1.length>0)
								{
									html += '<div style="display: table-cell;width:'+larghezzaSez+'%;">' + 
										'<div class="titleAllegati" style="margin-left:8%; margin-right:8%; text-align:center;  color:#b1c903;">'+lblAllegati+'</div>'
									for(allegatiIndex=0; allegatiIndex<allegatoTipo1.length; allegatiIndex++)
									{
										//icona
										icona = $.findIconByType(allegatoTipo1[allegatiIndex]['icona']);
										var link="";
										if (allegatoTipo1[allegatiIndex]['isUrl']==0) link="allegati/" + allegatoTipo1[allegatiIndex]['url'];
										else link=allegatoTipo1[allegatiIndex]['url'];
										html += '<div class="itemAllegati" style="margin-left:8%; margin-right:8%; text-align:left;  color:#58585a;"><a target="_blank" href="'+ link +'"><img src="images/'+icona+'" height="20px" style="margin-right:10px;"\>'+allegatoTipo1[allegatiIndex]['testo']+'</a></div>'
									}
									html += '</div>'								
								}
								if (allegatoTipo2.length>0)
								{
									//ci sono allegati di tipo 1(per mettere una riga verticale)
									if (allegatoTipo1.length>0) //metto un'altra cella con una riga verticale
									{
										html += '<div style="display: table-cell;width:2px;background-color:#B1C903"></div>'
									}
									html += '<div style="display: table-cell;width:'+larghezzaSez+'%;">' + 
										'<div class="titleAllegati" style="margin-left:8%; margin-right:8%; text-align:center;  color:#b1c903;">'+lblLink+'</div>'
									for(allegatiIndex=0; allegatiIndex<allegatoTipo2.length; allegatiIndex++)
									{
										//icona
										icona = $.findIconByType(allegatoTipo2[allegatiIndex]['icona']);
										var link="";
										if (allegatoTipo2[allegatiIndex]['isUrl']==0) link="allegati/" + allegatoTipo2[allegatiIndex]['url'];
										else link=allegatoTipo2[allegatiIndex]['url'];
										html += '<div class="itemAllegati" style="margin-left:8%; margin-right:8%; text-align:left;  color:#58585a;"><a target="_blank" href="'+ link +'"><img src="images/'+icona+'" height="20px" style="margin-right:10px;"\>'+allegatoTipo2[allegatiIndex]['testo']+'</a></div>'
									}									
									html += '</div>'								
								}
								if (allegatoTipo3.length>0)
								{
									//ci sono allegati di tipo 1 o 2(per mettere una riga verticale)
									if ((allegatoTipo1.length>0) || (allegatoTipo2.length>0))
									{
										html += '<div style="display: table-cell;width:1px;background-color:#B1C903"></div>'
									}
									html += '<div style="display: table-cell;width:'+larghezzaSez+'%;">' + 
										'<div class="titleAllegati" style="margin-left:8%; margin-right:8%; text-align:center;  color:#b1c903;">'+lblRassegnaStampa+'</div>'
									for(allegatiIndex=0; allegatiIndex<allegatoTipo3.length; allegatiIndex++)
									{
										//icona
										icona = $.findIconByType(allegatoTipo3[allegatiIndex]['icona']);
										var link="";
										if (allegatoTipo3[allegatiIndex]['isUrl']==0) link="allegati/" + allegatoTipo3[allegatiIndex]['url'];
										else link=allegatoTipo3[allegatiIndex]['url'];
										html += '<div class="itemAllegati" style="margin-left:8%; margin-right:8%; text-align:left;  color:#58585a;"><a target="_blank" href="'+ link +'"><img src="images/'+icona+'" height="20px" style="margin-right:10px;"\>'+allegatoTipo3[allegatiIndex]['testo']+'</a></div>'
									}									
									html += '</div>'								
								}
							}
						
						html += '</div></div>' +						
						'<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center" style="padding: 0px; margin: 0px;">' +
						'<hr style="color: #B1C903; background-color: #B1C903; height: 2px; width:100%">' +
						'</div>' +
					'</div>' 
				}
				//tipo 13: footer
				else if (type=="13")
				{
					html =	
					'<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-left" style="padding: 10px 10px 0px 10px; margin: 0px 0px 0px 0px;">' + 		
						'<div style="margin: 0% 0% 0% 0%; font-size: 15px; overflow-y:auto; text-align: center; padding-bottom: 20px;">&copy; <?=date("Y", time());?>  MediaSoft srl</div>' +
					'</div>' 
				}
				//tipo 14: social feed
				else if (type=="14")
				{
					//alert("arrivato");
					html =	
					'<div id="sectionSocialFeed" class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-left" style="padding: 10px 10px 0px 10px; margin: 0px 0px 0px 0px;">' + 		
						'<div style="display:table;width:100%">' +
							'<div style="display:table-row">' +
								'<div style="padding:5px;display:table-cell;margin: auto;text-align: center;"><img src="images/logo_facebook.png" width="30%"></div>' +
								'<div style="display:table-cell"></div>' +
								'<div style="padding:5px;display:table-cell;margin: auto;text-align: center;"><img src="images/logo_twitter.png" width="30%"></div>' +
							'</div>' +
							'<div style="display:table-row">' +
								'<div class="container-like" style="overflow: hidden;padding:5px;display:table-cell">' +
									'<div class="fb-like-box" style="margin-top:-70px;border:none!important" data-href="https://www.facebook.com/Mediasoftsrl"  data-colorscheme="light" data-show-faces="false" data-header="false" data-stream="true" data-show-border="false"></div>' +
								'</div>' +

								'<div style="display:table-cell;width=3px;background-color:#B1C903"></div>' +
								
								'<div class="container-like2" style="overflow: hidden;padding:5px;display:table-cell">' +
									'<a class="twitter-timeline"  href="https://twitter.com/Mediasoftonline"  data-chrome="nofooter noheader"  data-widget-id="461507894010060800">Tweets di @Mediasoftonline</a>' +
								'</div>' +
							'</div>' +
						'</div>' +
					'</div>'
				}
				//tipo 15: blog
				else if (type=="15")
				{
					
					<?php
					include "MSWP_Posts.php";
					$MyBlog = new MSWP();

					//devo prendere solo i post di una categoria?
					if (($cat=="") && ($tag=="")) $posts=$MyBlog->GetAllPosts();
					else
					{
						if ($cat!="") $posts=$MyBlog->GetPostsByCat($cat);
						else $posts=$MyBlog->GetPostsByTag($tag);
					}
					//print_r($posts);
					
					?>
					html = 
					'<div id="sectionBlog" class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-left" style="padding: 10px 10px 0px 10px; margin: 0px 0px 0px 0px;">' +  		
						'<div id="titolo" class="titleSection" style="margin-left:0%; margin-right:0%; text-align:center;  color:#b1c903;">THE INTERNET OF <b>IDEAS</b></div>'				
						

						//per ognuno dei post del blog, creo un div
						<?php					
						for($i=0;$i<count($posts);$i++)
						{
							//devo controllare il titolo
							$title = str_replace("'", "’", $posts[$i]->post_title);
							$title = sanitize_text_field($title);

							//check linguaggio
							$lingua = substr($title, 0, 4);

							//il default è l'inglese
							$check=false;
							if (strtoupper($lingua) == strtoupper("_".$lang."_"))
							{
								$check=true;
							}
							else
							{
								if ((strtoupper($lingua) != strtoupper("_IT_")) && (strtoupper($lingua) != strtoupper("_EN_")) && (strtoupper($lang)=="EN"))
								{
									$check=true;
									$title = "____".$title; //perchè dopo toglie i primi 4 caratteri
								}
							}

							if ($check==false) continue;

							$title = substr($title, 4);

							//devo parsare la data
							$anno = substr($posts[$i]->post_date, 0,4);
							$mese = substr($posts[$i]->post_date, 5,2);
							$giorno = substr($posts[$i]->post_date, 8,2);
							
							//devo controllare il post
							$content = str_replace("'", "’", $posts[$i]->post_content);
							//$content = str_replace('"', "'", $content);
							//$content = sanitize_text_field($content);
							$content = str_replace("\n\r",  '<br>', $content);
							$content = str_replace("\r\n",  '<br>', $content);
							$content = str_replace("\n",  '<br>', $content);
							$content = str_replace(array("\n", "\t", "\r"),  '', $content);
							

							
							//id del post
							$idPost = $posts[$i]->ID;
							
							?>
							html +=
							'<div style="display: table;padding: 20px 0px 0px 0px;width:100%;">' +
								'<div style="display: table-row;padding: 0px 0px 0px 0px;">' + 		
								
									'<div style="display: table-cell;width:90px;border:0px solid red">' +
										'<div style="width:80px;height:80px;background:#b1c903">' +
											'<p style="font-size:13px;font-weight: normal;color: #FFFFFF;text-align: center;padding-top: 5px;margin:0;"><?=MStranslate($mese)?></p>' +
											'<p style="font-size:40px;font-weight: bold;color: #FFFFFF;text-align: center;padding-top: 0px;margin:0;line-height: 32px;"><?=$giorno?></p>' +
											'<p style="font-size:16px;color: #FFFFFF;text-align: center;padding-top: 0px;margin:0;line-height: 24px;"><?=$anno?></p>' +
										'</div>' +
									'</div>' +
									
									'<div style="display: table-cell;border:0px solid green;">' +
										'<p style="margin-top:10px;"><a id="blogPost<?=$i?>" data-toggle="collapse" data-target="#collapseBlogPost<?=$i?>" class="blogPostTitle"><?=$title?></a></p>'
										
										<?php
											if ($posts[$i]->ImageDetail!="")
											$imageDetail=$posts[$i]->ImageDetail;
											$imageDetail = str_replace("http:","https:",$imageDetail);
											{?>
											html += '<div style="width:100%;float: left; margin: 0% 0% 0% 0%;padding-bottom: 0px;padding-top: 0px; margin-left:10px; text-align:justify; font-size:16px;"><img src="<?=$imageDetail?>" alt="" style="width:400px; float: left; margin:20px; border:1px #eeeeee solid;"><?=$content?></div>' 
											<?php
											}
										?>	
										html += "" + //'<div class="blogPostPublishedBy" style="margin-left:0%; margin-right:0%; text-align:left;">Posted by <?=$posts[$i]->display_name?> (<?=$posts[$i]->user_email?>)</div>'
										//inserisco i tag
										'<div>'
										<?php
											$numeroTag=count($posts[$i]->TagDetails);
											for($j=0;$j<$numeroTag;$j++)
											{
												?>
												html += '<div style="float:left;padding-right:5px"><p class="blogTag"><?=$posts[$i]->TagDetails[$j][name]?></p></div>'
												<?php
											}
										?>	
										html += '</div>' +		
										
										//'<div style="clear:both;" id="collapseBlogPost<?=$i?>" class="collapse">' +
										//	'<div class="blogPostContent" style="display:block;  margin: 0% 0% 0% 0%; padding-bottom: 20px;"><?=$content?></div>' +
										//'</div>' +
										'<hr style="color: #B1C903; background-color: #B1C903; height: 2px; width:100%;clear:both;">'
										//commenti, eventuali
										<?php
											$numeroCommenti = count($posts[$i]->CommentDetails);
											if ($numeroCommenti==0)
											{?>
												html += '<div>' +
															'<div style="float:left"><img style="padding-top:5px;padding-right:5px" src="images/icon_comments.png" alt="" style="width:20px"/></div>' +
															'<div class="blogPostPublishedBy" style="float:left;margin-left:0%; margin-right:0%; text-align:left;">Nessun Commento</div>' +
														'</div>'	
											<?php
											}
											else
											{
												if ($numeroCommenti==1) $testoCommenti="1 Commento";
												else $testoCommenti=$numeroCommenti." Commenti";
												?>
													html += '<div>' +
															'<a id="blogComments<?=$i?>" data-toggle="collapse" data-target="#collapseBlogComments<?=$i?>" style="cursor: pointer;">' +
																'<div style="float:left"><img style="padding-top:5px;padding-right:5px" src="images/icon_comments.png" alt="" style="width:20px"/></div>' +
																'<div class="blogPostPublishedBy" style="float:left;margin-left:0%; margin-right:0%; text-align:left;"><?=$testoCommenti?></div>' +
															'</a>' +
														'</div>' +
													'<div id="collapseBlogComments<?=$i?>" class="collapse" style="clear:both;">'
												<?php
													//ciclo e metto tutti i commenti
													for($j=0;$j<$numeroCommenti;$j++)
													{
														if ($posts[$i]->CommentDetails[$j][comment_author_email]!="") $nome=$posts[$i]->CommentDetails[$j][comment_author].' ('.$posts[$i]->CommentDetails[$j][comment_author_email].')';
														else $nome=$posts[$i]->CommentDetails[$j][comment_author];
														$testoCommento=sanitize_text_field($posts[$i]->CommentDetails[$j][comment_content]);
														$testoCommento = str_replace("'", "’", $testoCommento);
														?>	
														html += '<div style="width:50%">' +
															'<div style="display: table;width:100%">' +
																'<div style="display: table-row;padding: 0px 0px 0px 0px;">' + 		
																	'<div style="display: table-cell;width:50px;border:0px solid red">' +
																		'<img style="padding-top:15px" src="images/icona_wp_user.png" alt="" style="width:40px"/>' +
																	'</div>' +
																	'<div style="display: table-cell;border:0px solid green">' +
																		'<p class="blogCommentPublishedBy" style=""><?=$nome?></p>' +
																		'<p class="blogCommentText" style=""><?=$testoCommento?></p>' +
																	'</div>' +
																'</div>' +
															'</div>' +
															'<hr style="color: #B1C903; background-color: #B1C903; height: 1px; width:100%;margin-bottom:5px">' +
														'</div>'
														<?php
													}
												?>
													html += '</div>'
												<?php
											}	
										?>
										html += '<p style="margin-top:10px;clear:both;"><a data-toggle="collapse" data-target="#collapseBlogWriteComment<?=$i?>" style="cursor: pointer;">Lascia un Commento</a></p><p id="commentoInserito<?=$idPost?>" style="color:red;display:none">Commento inserito correttamente. In attesa di approvazione</p>' +
													'<div id="collapseBlogWriteComment<?=$i?>" class="collapse" style="clear:both;">' +
														'<div style="float:left;margin-right:10px;">Nome</div>' +
														'<input style="float:left;margin-right:10px;border: 1px solid #cccccc;border-radius:4px;width:200px;" type="text" id="blogCommentName<?=$idPost?>">' + 
														'<div style="float:left;margin-right:10px;">Email</div>' +
														'<input style="float:left;border: 1px solid #cccccc;border-radius:4px;width:200px;" type="text" id="blogCommentEmail<?=$idPost?>">' + 
														'<div style="clear:both;padding-top:5px;">' + 
															'<div style="float:left;margin-right:10px;">Commento</div>' +
															'<textarea style="float:left;resize:none;border: 1px solid #cccccc;border-radius:4px;width:420px;" id="blogCommentText<?=$idPost?>" rows="3" ></textarea>' + 
														'</div>' +
														'<p style="clear:both;padding-top:10px;"><a id="submitBlogInsertComment<?=$idPost?>" name="<?=$idPost?>" class="pulsanteReadMore">INSERISCI IL COMMENTO</a></p>' +
													'</div>'
									html += '</div>' +
								'</div>' +
							'</div>'
							<?php
						}
						?>						
						
					html +=	'<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center" style="padding-top: 20px; margin: 0px;">' +
						'<hr style="color: #B1C903; background-color: #B1C903; height: 2px; width:100%">' +
						'</div>' +
					'</div>'
				

				}
				//tipo 16: R&D
				else if (type=="16")
				{

					var language=$("#langHidden").val();
					if(language == "IT"){
						var lblPubblicazioni = "PUBBLICAZIONI SCIENTIFICHE";
					}else{
						var lblPubblicazioni = "SCIENTIFIC PUBBLICATIONS";
					}				

					var itemRD = row_data['subItemsRD'];

					console.log("subItemsRD");
					console.log(itemRD);

					/*var dataIdTimeline = new Array();
					var titoloTimeline = new Array();
					var descrizioneTimeline = new Array();
					var immagineTimeline = new Array();
					var link = new Array();

					for(c=0;c<itemTl.length;c++){
						dataIdTimeline[c+1]= itemTl[c].progressivo+"/"+itemTl[c].anno;
						titoloTimeline[c+1]=itemTl[c].titolo;
						descrizioneTimeline[c+1]=itemTl[c].descrizione;
						immagineTimeline[c+1]=itemTl[c].immagine;
						link[c+1]=itemTl[c].link;

						if(c == itemTl.length - 2){
							startTimeline = itemTl[c].progressivo+"/"+itemTl[c].anno;
						}
						
					}*/

					var elRd = "";
					for(c=0;c<itemRD.length;c++){

						if(itemRD[c].link == "" || itemRD[c].link == null){
							var linkRD = "javascript:void(0);";
						}else{
							var linkRD = itemRD[c].link;
						}

						elRd += '<a href="'+linkRD+'" target="_blank" class="blogPostTitle" style="margin-top: 40px;color: #B1C903">'+itemRD[c].titolo+'</a>';
						elRd += '<div style="width:100%; margin: 20px 0% 0% 0%;padding-bottom: 0px;padding-top: 0px; margin-left:10px; text-align:left; font-size:16px;">'+itemRD[c].autore.replace(/\r?\n/g, '</br>')+'</div>';
						elRd += '<div style="width:100%; margin: 30px 0% 0% 0%;padding-bottom: 0px;padding-top: 0px; margin-left:10px; text-align:left; font-size:14px;">Abstract:</div>';
						elRd += '<div style="width:100%; margin: 0% 0% 0% 0%;padding-bottom: 0px;padding-top: 0px; margin-left:10px; text-align:justify; font-size:14px;font-style: italic;">'+itemRD[c].testo.replace(/\r?\n/g, '</br>')+'</div>';
						elRd += '<hr style="color: #B1C903; background-color: #B1C903; height: 2px; width:100%;margin-top:30px;margin-bottom:40px;">';

					}


					html =	
					'<div id="titolo" class="titleSection" style="margin-left:0%; margin-right:0%; text-align:center;  color:#b1c903;">'+lblPubblicazioni+'</div>' +elRd+
					
					/*'<a href="#"  class="blogPostTitle" style="margin-top: 40px;color: #B1C903">Social Software and Online Collaboration Tool: A comparison based on user expectations</a>'+
					'<div style="width:100%; margin: 20px 0% 0% 0%;padding-bottom: 0px;padding-top: 0px; margin-left:10px; text-align:left; font-size:16px;">Stefano Santo Sabato, Federico Pici, Anna Lisa Guido "Social Software and Online Collaboration Tool: A comparison based on user expectations" in Proceeding of the 27th IBIMA (International Business Information Management Association) May 4-5 2016, Milan - Accepted - in press</div>' +
					'<div style="width:100%; margin: 30px 0% 0% 0%;padding-bottom: 0px;padding-top: 0px; margin-left:10px; text-align:left; font-size:14px;">Abstract:</div>' +
					'<div style="width:100%; margin: 0% 0% 0% 0%;padding-bottom: 0px;padding-top: 0px; margin-left:10px; text-align:left; font-size:14px;font-style: italic;">Collaboration systems are a key point in modern Information Systems. They are very useful when companies base their work on “knowledge”. Collaboration systems help knowledge workers to share knowledge and to work together regardless of where they are located. There are several solutions from turnkey solutions, to ad-hoc, as well as customizable solutions. Collaboration systems can be classified as social software and online collaboration tools, and they are useful both for big and small to medium sized companies. The collaboration systems promise to increase knowledge worker efficiency, and this is an important goal for all companies; especially small and medium sized companies. The real problem is that, very often, a knowledge worker does not use the tool. In this paper, starting from an empirical study, the expectations of the knowledge workers and of the companies are defined and a survey of several collaboration based on them will be presented.</div>' +

					'<hr style="color: #B1C903; background-color: #B1C903; height: 2px; width:100%;margin-top:30px;margin-bottom:40px;">' +					

					'<a href="http://ieeexplore.ieee.org/xpl/mostRecentIssue.jsp?punumber=6853543" target="_blank" class="blogPostTitle" style="margin-top: 40px;color: #B1C903">A two-stage approach to bring the postural assessment to masses: The KISS-Health Project</a>'+
					'<div style="width:100%; margin: 20px 0% 0% 0%;padding-bottom: 0px;padding-top: 0px; margin-left:10px; text-align:left; font-size:16px;">Bortone, I.; Argentiero, A.; Agnello, N.; Sabato, S.S.; Bucciero, A., "A two-stage approach to bring the postural assessment to masses: The KISS-Health Project", Biomedical and Health Informatics (BHI), 2014 IEEE-EMBS International Conference on , vol., no., pp.371,374, 1-4 June 2014</div>' +
					'<div style="width:100%; margin: 15px 0% 0% 0%;padding-bottom: 0px;padding-top: 0px; margin-left:10px; text-align:left; font-size:16px;">doi: 10.1109/BHI.2014.6864380</div>' +
					'<div style="width:100%; margin: 30px 0% 0% 0%;padding-bottom: 0px;padding-top: 0px; margin-left:10px; text-align:left; font-size:14px;">Abstract:</div>' +
					'<div style="width:100%; margin: 0% 0% 0% 0%;padding-bottom: 0px;padding-top: 0px; margin-left:10px; text-align:left; font-size:14px;font-style: italic;">The human body is in a posture every minute of every day: it can be static, such as when resting, or dynamic, such as when walking. It is quite frequent that people assume poor postures during the day and they make their bodies more susceptible to injuries. Considering this, prevention assumes a central role as it can avoid damage and pain in day-to-day life, but it is very poorly practiced in the majority of countries, even in the Western world. Moreover, diagnosis and assessments for postural abnormalities are demanded to few specialized centers, which usually perfom anedotical or subjective evaluations. In this paper, we propose an innovative model to face up to postural abnormalities starting from a two-stage approach that have been conceived and are being implemented by the KISS- Health Project. The idea consists of two units: a) the Mobile Diagnostic System, which is composed of both hardware and software components and it will permit a first assessment of human posture in order to identify people at risk; b) the Laboratory Diagnostic System, which will investigate more accurately the complete postural configuration of the subject going under exam. It is our goal to promote an integrated approach that, based on scientific and economical evidences, will reduce the costs related to postural abnormalities.</div>' +

					'<hr style="color: #B1C903; background-color: #B1C903; height: 2px; width:100%;margin-top:30px;margin-bottom:40px;">' +					

					'<a href="http://eudl.eu/doi/10.4108/icst.pervasivehealth.2014.254944" target="_blank" class="blogPostTitle" style="margin-top: 40px;color: #B1C903">A Biomechanical Analysis System of Posture</a>'+
					'<div style="width:100%; margin: 20px 0% 0% 0%;padding-bottom: 0px;padding-top: 0px; margin-left:10px; text-align:left; font-size:16px;">Bucciero, A.; Santo Sabato, S.; Zappatore, M., "A Biomechanical Analysis System of Posture", PERVASIVE HEALTH International Conference (PH2014) Year: 2014, ICST Editor, DOI: 10.4108/icst.pervasivehealth.2014.254944</div>' +
					'<div style="width:100%; margin: 30px 0% 0% 0%;padding-bottom: 0px;padding-top: 0px; margin-left:10px; text-align:left; font-size:14px;">Abstract:</div>' +
					'<div style="width:100%; margin: 0% 0% 0% 0%;padding-bottom: 0px;padding-top: 0px; margin-left:10px; text-align:left; font-size:14px;font-style: italic;">The number of scenarios that may gain benefits from postural analysis performed with sustainable economic costs is virtually limitless. They range from clinical and outpatient studies to sport and dance practicing, from posture learning and replication for humanoid robots to body control techniques. Therefore the authors firmly believe that the Microsoft Kinect, a novel optical markerless, model-free motion/capture commercial system can pave the way towards this direction due to its ease of usage and its low costs. In this paper, a Model-View-Controller based framework for Kinect-based postural analysis is presented in terms of both system architecture and selected test environment. A first prototypal version of the system has been employed to perform a preliminary stabilometric analysis in a real test case, in order to illustrate the adopted natural user interface and the overall system behavior.</div>' +

					'<hr style="color: #B1C903; background-color: #B1C903; height: 2px; width:100%;margin-top:30px;margin-bottom:40px;">' +					

					'<a href="http://eleot.org/2014/show/home" target="_blank" class="blogPostTitle" style="margin-top: 40px;color: #B1C903">From the First Generation of Distance Learning to Personal Learning Environments: an Overall Look</a>'+
					'<div style="width:100%; margin: 20px 0% 0% 0%;padding-bottom: 0px;padding-top: 0px; margin-left:10px; text-align:left; font-size:16px;">Santo Sabato, A.; Vernaleone, M.; "From the First Generation of Distance Learning to Personal Learning Environments: an Overall Look", 1st International Conference on e-Learning e-Education and Online Training  (eLEOT2014)</div>' +
					'<div style="width:100%; margin: 30px 0% 0% 0%;padding-bottom: 0px;padding-top: 0px; margin-left:10px; text-align:left; font-size:14px;">Abstract:</div>' +
					'<div style="width:100%; margin: 0% 0% 0% 0%;padding-bottom: 0px;padding-top: 0px; margin-left:10px; text-align:left; font-size:14px;font-style: italic;">This article aims to confront how e-learning models have evolved over time from the characteristics of the web 1.0 to those of 2.0 looking briefly at the CMS, LMS, LCMS and PLE platforms and how it is essential to carefully first consider the constructivist pedagogical theories so as to comprehend the present situation, as well as that of the forthcoming future.</div>' +

					'<hr style="color: #B1C903; background-color: #B1C903; height: 2px; width:100%;margin-top:30px;margin-bottom:40px;">' +					

					'<a href="http://www.ncbi.nlm.nih.gov/pubmed/25266937" target="_blank" class="blogPostTitle" style="margin-top: 40px;color: #B1C903">Networking and data sharing reduces hospitalization cost of heart failure: the experience of GISC study</a>'+
					'<div style="width:100%; margin: 20px 0% 0% 0%;padding-bottom: 0px;padding-top: 0px; margin-left:10px; text-align:left; font-size:16px;">Pisanò, F.; Lorenzoni, G.; Santo Sabato, S.; Soriani, N.; Narraci, O.; Accogli, M.; Rosato, C.; De Paolis, P.; Folino, F.; Buja, G.; Tona, F.; Baldi, I.; Iliceto, S.; Gregori, D.; "Networking and data sharing reduces hospitalization cost of heart failure: the experience of GISC study",  Journal of Evaluation in Clinical Practice ISSN 1365-2753</div>' +
					'<div style="width:100%; margin: 30px 0% 0% 0%;padding-bottom: 0px;padding-top: 0px; margin-left:10px; text-align:left; font-size:14px;">Abstract:</div>' +
					'<div style="width:100%; margin: 0% 0% 0% 0%;padding-bottom: 0px;padding-top: 0px; margin-left:10px; text-align:left; font-size:14px;font-style: italic;">Heart failure (HF) is a concerning public health burden in Western society because, despite the improvement of medical treatments, it is still associated with adverse outcomes (high morbidity and mortality), resulting in one of the most expensive chronic disease in Western countries. Hospital admission particularly is the most expensive cost driver among the several resources involved in the management of HF. The aim of our study was to investigate the cost of hospitalization before and after the enrolment to a new strategy (GISC) in the management of patients with HF.</div>' +
					
					'<hr style="color: #B1C903; background-color: #B1C903; height: 2px; width:100%;margin-top:30px;">' +	*/			

					'';
				}
				
				return html;
			},
			'renderer2': function(row_data)
			{
				var idSezione = row_data['idSezione'];
				var titolo = row_data['titolo_menu_lat'];
				
				if (titolo!=null) var html = '<li><a href="#section'+idSezione+'"><img src="images/pallino_verde_20.png" style="width:8px;height:8px;margin-right:6px;margin-bottom:2px;"\>'+titolo+'</a></li>'
				return html;
			}
		}
		


		$(window).load(function() {

			//$.buildTopMenuNew($.urlParam('idPage'));
			//$.tweakIpadLandscape();
			if($.detectMobile() == true) {
				//tolgo il menu' a destra e metto il resto al 100%
				$('#mainContainerDiv').css("width","100%");
				$('#navbar-laterale').css("visibility","hidden");
			}
			
			
			(function(d, s, id) {
			  var js, fjs = d.getElementsByTagName(s)[0];
			  if (d.getElementById(id)) return;
			  js = d.createElement(s); js.id = id;
			  js.src = "//connect.facebook.net/it_IT/all.js#xfbml=1&appId=1436514966597507";
			  fjs.parentNode.insertBefore(js, fjs);
			}(document, 'script', 'facebook-jssdk'));  
			
		});


		$(document).ready(function()
		{

			$.setupNavbar();			
			$.centerNavbarByLogo();
			$.setThemeBlack();

			
			


			setTimeout(function()
			{


			!function(d,s,id){
				var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';
				if(!d.getElementById(id)){
					js=d.createElement(s);js.id=id;
					js.src=p+"://platform.twitter.com/widgets.js";
					fjs.parentNode.insertBefore(js,fjs);
					}
			}(document,"script","twitter-wjs");

			
			
				var mainPageConfig = pages_config[$.urlParam('idPage')];
				var next_content = mainPageConfig.next_content;
				var index = 0;
				
				$.setDivContent2( mainPageConfig, true, false);

				
				$('a[href*=#]:not([href=#])').click(function()
				{
					
					if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
					  var target = $(this.hash);
					  
					  target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
					  
					  var offsetTimeline=0;
					  
					  var name=($(target).attr('id'));
					  //alert(name.charAt(0));
					  //alert(name);
					  
					  if (name.charAt(0)=="t") offsetTimeline=120; //timeline
					  else offsetTimeline=0;

					  if (target.length) {
						$('html,body').animate({
						  scrollTop: target.offset().top-100-offsetTimeline
						}, 500);
						return false;
					  }
					}
				});	
				
				
				
				$('[id^=submitBlogInsertComment]').click(function() {
					var $this = $(this);
					var idPost = $this.attr('name');
					//alert($this.attr('name'));
					//controllo se tutti i campi sono ok
					if($('#blogCommentName'+idPost).val() == "")
					{
						alert("Attenzione! Inserire Nome e Cognome");
						return false;
					}
					var email = $('#blogCommentEmail'+idPost).val();
					if($.validateEmail(email) == false)
					{
						alert("Attenzione! Inserire una Email valida");
						return false;
					}
					if($('#blogCommentText'+idPost).val() == "")
					{
						alert("Attenzione! Inserire il Commento");
						return false;
					}

					//recupero i campi che mi servono
					var nome = $('#blogCommentName'+idPost).val();
					var commento = $('#blogCommentText'+idPost).val();
					//alert(nome + " - " + email + " - " + commento);
					
					var objParam = new Object();

					objParam.action = "WriteCommentOnPost";
					objParam.IdPost = idPost;
					objParam.nome=nome;
					objParam.commento=commento;
					objParam.email =email;
					$.ajax({
					  url: "MSWP_Wrapper.php",
					  type:"GET",
					  data:objParam
					}).done(function ( data ) {
						if (data==1)
						{
							//tolgo i dati dai campi
							$('#blogCommentName'+idPost).val("");
							$('#blogCommentText'+idPost).val("");
							$('#blogCommentEmail'+idPost).val("");
							$('#commentoInserito'+idPost).text("Commento inserito correttamente. In attesa di approvazione");
							$('#commentoInserito'+idPost).show();
						}
						else
						{
							//tolgo i dati dai campi
							$('#blogCommentName'+idPost).val("");
							$('#blogCommentText'+idPost).val("");
							$('#blogCommentEmail'+idPost).val("");
							$('#commentoInserito'+idPost).text("Errore nell'inserimento del commento. Riprovare più tardi, grazie");
							$('#commentoInserito'+idPost).show();
						}
					});
					
					
				});

				//esiste la sezione social feed?
				if ($('#sectionBlog').length != 0)
				{
					//prendo tutte le categorie dei blog
					var objParam = new Object();

					objParam.action = "getCategoriesAndTag";
					
					$.ajax({
					  url: "MSWP_Wrapper.php",
					  type:"GET",
					  data:objParam

					}).done(function ( data ) {
						//alert(data);
						var str="";
						var objCat = eval(data);
						str+="<li><a href=\"?idPage=19\"><img src=\"images/pallino_verde_20.png\" style=\"width:8px;height:8px;margin-right:6px;margin-bottom:2px;\"\>Tutte le Categorie</a></li>";
						for (var i=0;i<objCat.length;i++)
						{
							if (objCat[i].taxonomy=="category") str+="<li><a href=\"?idPage=19&cat="+objCat[i].name+"\"><img src=\"images/pallino_verde_20.png\" style=\"width:8px;height:8px;margin-right:6px;margin-bottom:2px;\"\>"+objCat[i].name+"</a></li>";
							
						}
						str+="<hr style=\"padding: 5px;margin: 10px;\">";
						//alert(str);
						for (var i=0;i<objCat.length;i++)
						{
							if (objCat[i].taxonomy=="post_tag") str+="<a href=\"?idPage=19&tag="+objCat[i].name+"\"><div style=\"float:left;padding-right:5px;padding-top:5px;\"><p class=\"blogTag\">"+objCat[i].name+"</p></div></a>";
							
						}

						document.getElementById("lateralMenu").innerHTML =str;
					
					});


				}

				
				/*
				//esiste la sezione competenze?
				if ($('#competenzeTitle').length != 0)
				{
					$(function() {
						//prima di far partire il js della griglia, ridimensiono le immagini
						var element_width = ($('#mainContainerDiv').width()*0.70)/5;
						//alert(element_width);
						$('#competenze1').width(element_width);
						$('#competenze2').width(element_width);
						$('#competenze3').width(element_width);
						$('#competenze4').width(element_width);
						$('#competenze5').width(element_width);
						$('#competenze6').width(element_width);
						$('#competenze7').width(element_width);
						$('#competenze8').width(element_width);
						$('#competenze9').width(element_width);
						$('#competenze10').width(element_width);
						//var $grid = $( '#og-grid' );
						//alert($grid.attr('id'));
						//alert($( '#TitoloCompetenze' ).attr('id'));
						//alert($( '#TitoloCompetenze' ).text());
						//sposto la griglia nel div appropriato
						$("#og-grid").appendTo('#competenzeDiv');
						//rimetto la griglia a visibile
						$( '#og-grid').css("visibility", "visible");
						Grid.init();
					});
				}
				else
				{
					//elimino la og-grid che avevo messo statica
					$( '#og-grid').remove();
				}
				*/

				$('img').click(function(event)
				{
					//è un thumb della categoria myGrid, quello che sto cliccando?
					var name=($(event.target).attr('id'));
					
					if (name.substr(0,11)=="myGridThumb")
					{
						//di che sezione fa parte?
						var sezione = name.substr(12);
						//alert(sezione);
						//devo fare lo swap delle 2 immagini
						var src_thumb=($(event.target).attr('src'));
						var src_image=($("#myGridImg"+sezione).attr('src'));
						//faccio lo swap
						$("#myGridImg"+sezione).fadeOut(function(){
						  $(this).attr("src",src_thumb).fadeIn();
						});					  

	//					$("#myGridImg"+sezione).attr('src',src_thumb);
						
						$(event.target).attr('src',src_image);
					
					}
				});

				
				$('[id^=readMoreSection]').click(function(event)
				{
					var $this = $(this);
					var testo = $this.text();
					if (testo=="MORE") $this.text("LESS"); 
					else $this.text("MORE");
				});
				
				
				$('[id^=competenzeItem]').click(function(event)
				{
					var $this = $(this);
					//alert($this.attr('id'));
					var titolo="";
					var virgolettato="";
					var descrizione="";
					var link="";
					var collapseSection=0;

					var language=$("#langHidden").val();
					if(language == "IT"){
						if ($this.attr('id')=="competenzeItem1")
						{
							collapseSection=1;
							titolo="APPLICAZIONI WEB";
							virgolettato="Sviluppiamo le vostre soluzioni sotto forma di applicazioni web quali sistemi di monitoraggio, gestionali per il web, CRM, gestione progetti, automazione della rete vendita, sistemi di prenotazione alberghiera ed altre ancora.";
							descrizione="Ogni qualvolta si usi un social network come facebook, si guardi un video su Youtube, o si legga la propria casella di posta elettronica on-line, si sta utilizzando un'applicazione web, ovvero un software che non risiede fisicamente sul proprio computer ma sul web, e che diviene quindi raggiungibile da qualsiasi terminale connesso ad internet. Mediasoft ha da tempo sposato la filosofia del SaaS, ovvero del Software as Service, sviluppando le proprie soluzioni (dai sistemi gestionali arrivando addirittura a software di diagnostica clinica) sotto forma di applicazioni web.";
							link="fixed.content.php?idPage=9";
						}
						else if ($this.attr('id')=="competenzeItem2")
						{
							collapseSection=1;
							titolo="APP MOBILE e MULTICANALE";
							virgolettato="Attiviamo diversi canali tra cui app o portali mobile per dispositivi quali palmari e smartphone, portali vocali fruibili tramite riconoscimento vocale o tastiera telefonica, newsletter tramite mail, invio di SMS, RSS e podcasting dei contenuti multimediali.";
							descrizione="Le applicazioni multicanale permettono di distribuire contenuti formattati in maniera adeguata, a seconda della tipologia di dispositivo impiegato dall’utente finale per la visualizzazione. MediaSoft è in grado di attivare molteplici canali tra cui app (iOS o Android) o portali mobile per dispositivi quali palmari e smartphone, portali vocali fruibili tramite riconoscimento vocale o tastiera telefonica, totem multimediali, Web TV, newsletter tramite mail, invio di SMS, RSS e podcasting dei contenuti multimediali.";
							link="fixed.content.php?idPage=10";
						}
						else if ($this.attr('id')=="competenzeItem3")
						{
							collapseSection=1;
							titolo="WEB MARKETING";
							virgolettato="Annoveriamo certificazioni SEO & SEM in grado di garantire professionalità e capacità riconosciute, indirizzate verso l'ottenimento di una migliore rilevazione, analisi di web marketing e lettura dei siti web da parte dei motori di ricerca.";
							descrizione="Per aggirare l’ostacolo del costante incremento di informazioni di qualità presenti in rete, l'azienda deve usufruire di alcuni accorgimenti e strumenti di web marketing per ottenere una visibilità soddisfacente del proprio sito.  E’ fondamentale strutturare un sito e le sue pagine affinché possano essere facilmente raggiungibili dagli spider dei motori di ricerca e ottimizzate sulle parole chiave per le quali si vuole essere trovati. Il personale MediaSoft vanta certificazioni SEO & SEM che garantiscono professionalità e capacità comprovate, finalizzate ad ottenere la migliore rilevazione, analisi e lettura del sito web da parte dei motori di ricerca.";
							link="fixed.content.php?idPage=11";
						}
						else if ($this.attr('id')=="competenzeItem4")
						{
							collapseSection=1;
							titolo="REALTA' AUMENTATA";
							virgolettato="Utilizziamo la Realtà Aumentata, la sovrapposizione di livelli informativi (immagini, video, oggetti 3D, animazioni) all’ambiente reale; alla normale realtà percepita attraverso i propri sensi, vengono fatte coincidere informazioni sensoriali artificiali/virtuali.";
							descrizione="Gli elementi si aggiungono alla realtà tramite dispositivi dotati di fotocamera, telecamera, webcam o particolari visori, i quali riprendono l’ambiente circostante. Il software rielabora il flusso video in tempo reale, aggiungendo contenuti multimediali in grado di integrarsi al contesto ed arricchirlo tramite geolocalizzazione e tracciamento. Tra gli esempi di Augmented Reality: la confezione di un prodotto che fornisce informazioni aggiuntive sul contenuto; gli oggetti di un catalogo si mostrano in un display video tridimensionalmente; un supporto cartaceo attiva un concorso a premi o un divertente gioco.";
							link="fixed.content.php?idPage=8";
						}
						else if ($this.attr('id')=="competenzeItem5")
						{
							collapseSection=1;
							titolo="APPLICAZIONI 3D";
							virgolettato="Sviluppiamo applicazioni 3d di diverse tipologie (fondate sull’utilizzo di motori grafici 3D) a seconda delle esigenze richieste dai nostri clienti, spaziando da semplici visualizzazioni a simulazioni articolate fino ad applicazioni di realtà aumentata.";
							descrizione="Fondate sull’utilizzo di motori grafici 3D, consentono di navigare interattivamente in un ambiente virtuale o di visualizzare un oggetto mutandone in tempo reale la configurazione, le finiture, i materiali e le forme, di riprodurne le cinematiche, e di programmare logiche complesse a fini simulativi. Le applicazioni sviluppate possono essere di diverso tipo a seconda delle necessità, spaziando da semplici visualizzazioni a simulazioni articolate fino ad applicazioni di realtà aumentata.";
							link="fixed.content.php?idPage=12";
						}
						else if ($this.attr('id')=="competenzeItem6")
						{
							collapseSection=2;
							titolo="INTERNET DELLE COSE";
							virgolettato="Ci accostiamo al mondo dell’Internet delle Cose, che integra il mondo fisico con quello virtuale della rete attraverso tecnologie incorporate agli oggetti quotidiani che rendono le informazioni su questi ultimi, e sul loro ambiente, accessibili e gestibili nel mondo digitale.";
							descrizione="Le piante avvisano l'innaffiatoio quando è il momento di essere innaffiate, le sveglie suonano in anticipo in caso di traffico, le tapparelle si regolano in automatico a seconda del livello di luce presente all’esterno, le scarpe da ginnastica trasmettono tempi, distanza e velocità per gareggiare in tempo reale con persone dall'altra parte del mondo. Fantascienza? Assolutamente no! E’ la nuova rivoluzione dell’Internet delle Cose che consente agli oggetti di potersi conquistare un ruolo attivo attraverso il collegamento alla Rete. Gli oggetti si rendono identificabili e acquisiscono intelligenza grazie alla nuova capacità di poter comunicare dati su se stessi e accedere a informazioni aggregate da parte di altri.";
							link="fixed.content.php?idPage=7";
						}
						else if ($this.attr('id')=="competenzeItem7")
						{
							collapseSection=2;
							titolo="TECNOLOGIE EMERGENTI";
							virgolettato="Esaminiamo le Tecnologie Emergenti degli strumenti abilitanti per Creatività e Innovazione ed eseguiamo su di esse attività di scouting e test. Tale approccio ci consente di realizzare prototipi sperimentali su casi reali anticipando in tal modo le necessità del mercato.";
							descrizione="Le Tecnologie Emergenti rappresentano l’innovazione per eccellenza presente nel mondo della tecnologia e della convergenza tecnologica pertinente a numerosi campi di ricerca. Quelle che attualmente vengono considerate emergenti sono, ad esempio: Natural User Interfaces: la possibilità di dominare sistemi informativi articolati con il solo movimento corporeo consente nuove modalità di impiego di sistemi informativi complessi. Wearable Devices: esistono molteplici sperimentazioni di dispositivi indossabili, ognuna con peculiarità e caratteristiche interessanti dal punto di vista di un’ipotetica collocazione sul mercato.";
							link="fixed.content.php?idPage=13";
						}
						else if ($this.attr('id')=="competenzeItem8")
						{
							collapseSection=2;
							titolo="SISTEMI DI COLLABORAZIONE AZIENDALE";
							virgolettato="Garantiamo strumenti e soluzioni di collaborazione efficienti, in grado di creare l'equivalente virtuale della presenza di tutti i partecipanti in una stessa stanza, alla stessa ora, per esaminare gli stessi dati.";
							descrizione="In un ambiente in cui i team di prodotto sono sempre più distribuiti e i partner e i fornitori sono dislocati in tutto il mondo, una collaborazione efficiente necessita sia della memorizzazione centralizzata dei dati di prodotto sia di mezzi per la condivisione plurilaterale. Le funzionalità di social networking e Web 2.0 custodiscono molteplici potenzialità di collaborazione per gli utenti aziendali, oltre ad avere già trasformato le modalità di interazione dei consumatori.";
							link="fixed.content.php?idPage=14";
						}
						else if ($this.attr('id')=="competenzeItem9")
						{
							collapseSection=2;
							titolo="IDENTITA' DIGITALE";
							virgolettato="Creiamo e integriamo sistemi per la gestione del ciclo di vita delle identità digitali, sviluppando processi di assegnazione delle stesse che possono riguardare l'accesso all’intranet aziendale o l'account di posta.";
							descrizione="Ogni singola persona, che sia il dipendente di un’azienda o un semplice utente esterno di un servizio, è oggetto di un processo di assegnazione di numerose identità digitali che possono riguardare l'accesso all’intranet aziendale o l'account di posta, in maniera coerente con il suo particolare ruolo aziendale. Risulta quindi indispensabile essere in grado di progettare e realizzare o integrare sistemi per la gestione del ciclo di vita delle identità digitali.";
							link="fixed.content.php?idPage=15";
						}
						else if ($this.attr('id')=="competenzeItem10")
						{
							collapseSection=2;
							titolo="GESTIONE DELLE INFORMAZIONI E DELLA CONOSCENZA";
							virgolettato="Sviluppiamo e gestiamo siti web fondati su sistemi di content e knowledge management. Attraverso l'impiego di soluzioni fondate sulle ontologie si ha la possibilità di visualizzare percorsi logici di navigazione e usufruire di motori di ricerca concettuali.";
							descrizione="Abbiamo sviluppato negli anni tecniche e metodi per il reperimento, la gestione e l'analisi dei dati, dell'informazione e della conoscenza, posando l'attenzione sull’esigenza di gestione di ampi flussi informativi tipici di organizzazioni complesse, e dell'evoluzione verso una società dell'informazione. Attraverso l'utilizzo di soluzioni fondate sulle ontologie diviene possibile visualizzare percorsi logici di navigazione e usufruire di motori di ricerca concettuali.";
							link="fixed.content.php?idPage=16";
						}
					}else{
						if ($this.attr('id')=="competenzeItem1")
						{
							collapseSection=1;
							titolo="WEB APPLICATION";
							virgolettato="We develop our solutions in the form of web applications such as monitoring systems, management systems for the web, CRM, project management, automation of the sales network, reservation systems for hotels, and more.";
							descrizione="Whenever you use a social network like Facebook, watch a video on Youtube, or read email online, you are using a web application or software that does not reside physically on your computer, rather on the web. It then becomes accessible from any terminal connected to the Internet. Mediasoft has long espoused the philosophy of SaaS, or Software as a Service, by developing solutions (from system management software to clinical diagnostics) in the form of web applications.";
							link="fixed.content.php?idPage=9";
						}
						else if ($this.attr('id')=="competenzeItem2")
						{
							collapseSection=1;
							titolo="MULTI-CHANNEL AND MOBILE APPS";
							virgolettato="Activate various channels for app or mobile portals for devices such as PDAs and smartphones. Voice portals accessible via voice recognition or telephone keypad, newsletters via email, sending SMS, RSS and podcasting multimedia content.";
							descrizione="The multi-channel applications allow the distribution content formatted in an appropriate manner, depending on the type of device used by the end user for display. MediaSoft can activate multiple channels including apps (iOS or Android) or portals, mobile devices such as PDAs and smartphones, voice portals accessible via voice recognition or telephone keypad, multimedia kiosks, Web TV, newsletters via email, sending SMS, RSS, and podcasting multimedia content.";
							link="fixed.content.php?idPage=10";
						}
						else if ($this.attr('id')=="competenzeItem3")
						{
							collapseSection=1;
							titolo="WEB MARKETING";
							virgolettato="We have SEO & SEM certificates, and can guarantee professionalism and recognized skills ranging from views articulated in simple simulations up to augmented reality applications..";
							descrizione="To overcome the obstacle of the constant increase of quality information on the network, a company has to take advantage of some tricks and web marketing tools to get satisfactory visibility of the website. A fundamental structure of a site and its pages is needed so that they can be easily reached by search engine spiders to optimize on the keywords for which you want to be found. MediaSoft staff boasts SEO & SEM certifications that guarantee professionalism and proven skills that will achieve the best detection, analysis and reading of the website by the search engines.";
							link="fixed.content.php?idPage=11";
						}
						else if ($this.attr('id')=="competenzeItem4")
						{
							collapseSection=1;
							titolo="AUGMENTED REALITY";
							virgolettato="We use Augmented Reality by overlapping levels of information (images, video, 3D objects, animations) of the real environment; the normally perceived reality through our senses is made to coincide with artificial/virtual  sensory information.";
							descrizione="The elements are added to the reality through devices equipped with cameras, webcams or special viewers which record their surroundings. The software revises the video stream in real time, adding multimedia content that can be integrated into the context and enrich it through geolocation and tracking. Examples of Augmented Reality: the packaging of a product that provides additional information on the content; objects in a catalog shown in a three-dimensional video display; a paper activates a sweepstakes or a fun game.";
							link="fixed.content.php?idPage=8";
						}
						else if ($this.attr('id')=="competenzeItem5")
						{
							collapseSection=1;
							titolo="3D APPLICATION";
							virgolettato="We develop different types of 3D applications (based on the use of 3D graphics engines) according to the requirements of our customers; ranging from views articulated in simple simulations to augmented reality applications.";
							descrizione="The use of 3D graphics engines allows you to interactively navigate in a virtual environment or to view an object, changing real-time configuration, finishes, materials and forms to reproduce its kinematics, and to program complex logic for simulation purposes. Different types of applications can be developed depending on the need, ranging from simple visualizations to augmented reality applications .";
							link="fixed.content.php?idPage=12";
						}
						else if ($this.attr('id')=="competenzeItem6")
						{
							collapseSection=2;
							titolo="THE INTERNET OF THINGS";
							virgolettato="The world of the Internet of Things, which integrates the physical world with the virtual network through technologies incorporated into everyday objects. This provides information about the object and its enviroment, making it possible to access and manipulate it in the digital world.";
							descrizione="Plants indicating when it's time to be watered, an alarm sounds in advance incase of traffic, blinds that automatically adjust depending on the amount of light outside, or sneakers that transmit time, distance and speed to compete in real time with people halfway around the world. Science fiction? Absolutely not! The new revolution of the Internet of Things allows objects to be able to play an active role through the connection to the network. The objects become identifiable and acquire intelligence with the new ability to communicate information about themselves and access to aggregate information from others.";
							link="fixed.content.php?idPage=7";
						}
						else if ($this.attr('id')=="competenzeItem7")
						{
							collapseSection=2;
							titolo="EMERGING TECHNOLOGIES";
							virgolettato="We examine the Emerging Technologies tools for enabling creativity and innovation, and run scouting and testing with them. This approach allows us to create experimental prototypes of real cases, thus anticipating market needs.";
							descrizione="Emerging Technologies represent innovation for excellence in this world of technology and the convergence of technologies applicable to many fields of research. Those that are currently considered to be emerging, for example: Natural User Interfaces: the ability to dominate articulated information systems with only body movement allows new methods of use of complex information systems. Wearable Devices: there are many experiments with wearable devices, each with peculiarities and characteristics interesting from the point of view of a hypothetical placement on the market.";
							link="fixed.content.php?idPage=13";
						}
						else if ($this.attr('id')=="competenzeItem8")
						{
							collapseSection=2;
							titolo="BUSINESS COLLABORATION SYSTEMS";
							virgolettato="We guarantee efficient instruments and collaboration solutions able to create the virtual equivalent of the presence of all participants in the same room, at the same time, to examine the same data.";
							descrizione="In an environment where product teams are increasingly spread out and partners and suppliers are located all over the world, effective collaboration requires both the centralized storage of data produced and a means of plurilateral sharing. The social networking and Web 2.0 hold vast possibilities of collaboration for users, in addition to having already transformed the way of consumer interaction.";
							link="fixed.content.php?idPage=14";
						}
						else if ($this.attr('id')=="competenzeItem9")
						{
							collapseSection=2;
							titolo="DIGITAL IDENTITY";
							virgolettato="We create and integrate systems to manage the lifecycle of digital identities, developing processes which may include access to the corporate intranet or the mail account.";
							descrizione="Every single company employee or a simple external user of a service is subject to a process of assigning a number of digital identities that can affect access to the corporate intranet or the mail accounts in a manner consistent with their particular business role. It is therefore essential to be able to design and implement or integrate systems to manage the lifecycle of digital identities.";
							link="fixed.content.php?idPage=15";
						}
						else if ($this.attr('id')=="competenzeItem10")
						{
							collapseSection=2;
							titolo="KNOWLADGE AND INFORMATION MANAGEMENT";
							virgolettato="Develop and maintain websites based on content and knowledge management systems. Through the use of solutions based on ontologies you have the ability to view logical paths of navigation and use conceptual search engines.";
							descrizione="Over the years, we have developed methods for the retrieval, management and analysis of data, information and knowledge. Placing attention on the management of information flows typical of large complex organizations, and the shift towards an information society. Through the use of solutions based on ontologies, it becomes possible to display logical paths of navigation and use conceptual search engines.";
							link="fixed.content.php?idPage=16";
						}
					}



					if (collapseSection==1)
					{
						//ho cliccato lo stesso?
						if ($('#collapseTitoloCompetenze1').text()==titolo)
						{
							//è lo stesso... lo chiudo
							$('#collapseTitoloCompetenze1').text("");
							$('#collapseCompetenze1').collapse('hide');
						}
						else
						{
							//è un altro... lo apro
							$('#collapseTitoloCompetenze1').text(titolo);
							$('#collapseVirgolettatoCompetenze1').text(virgolettato);
							$('#collapseTestoCompetenze1').text(descrizione);
							$('#collapseLinkCompetenze1').attr('href',link);
							$('#collapseCompetenze1').collapse('show');
						}
						//il collapse 2 è aperto?
						if ($('#collapseCompetenze2').hasClass( "in" )) $('#collapseCompetenze2').collapse('hide');
					}
					else if (collapseSection==2)
					{
						//ho cliccato lo stesso?
						if ($('#collapseTitoloCompetenze2').text()==titolo)
						{
							//è lo stesso... lo chiudo
							$('#collapseTitoloCompetenze2').text("");
							$('#collapseCompetenze2').collapse('hide');
						}
						else
						{
							//è un altro... lo apro
							$('#collapseTitoloCompetenze2').text(titolo);
							$('#collapseVirgolettatoCompetenze2').text(virgolettato);
							$('#collapseTestoCompetenze2').text(descrizione);
							$('#collapseLinkCompetenze2').attr('href',link);
							$('#collapseCompetenze2').collapse('show');
						}
						//il collapse 1 è aperto?
						if ($('#collapseCompetenze1').hasClass( "in" )) $('#collapseCompetenze1').collapse('hide');
					}
					
				});


				
				
				//$('body').scrollspy({ target: '#navbar-laterale' })

				//esiste una timeline?
				if ($('.tl1').length != 0)
				{
					//console.log("parto");
					//console.log(subTotalAnnoTimeline[0]);
					//console.log(subAnnoTimeline[0]);
					//console.log(startTimeline);

					$('.tl1').timeline({
						openTriggerClass : '.read_more',
						startItem : '3/2015',
						startItem : startTimeline,
						closeText : 'x',
						//categories : ['2007', '2008', '2009', '2010', '2011', '2012', '2013', '2014', '2015'],
						//nuberOfSegments        : [3, 4, 3, 3, 3, 5, 4, 9, 4], // number of elements per category (number of days)
						categories : subAnnoTimeline[0],
						nuberOfSegments        : subTotalAnnoTimeline[0], // number of elements per category (number of days)
						yearsOn                : false           // show years (can be any number you use in data-id (elementNumber/category/yearOrSomeOtherNumber))
						});
					$('.tl1').on('ajaxLoaded.timeline', function(e){
						var height = e.element.height()-60-e.element.find('h2').height();
						e.element.find('.timeline_open_content span').css('max-height', height).mCustomScrollbar({
							autoHideScrollbar:true,
							theme:"light-thin"
						});	
					});
				}

				//esiste la sezione socialFeed (siamo quindi nella pagina dei feed social)?
				if ($('#sectionSocialFeed').length != 0)
				{
					//tolgo il menu a dx e metto a tutta larghezza
					$('#mainContainerDiv').css("width","100%");
					$('#navbar-laterale').css("visibility","hidden");
					
					var largh=Math.round(($('#sectionSocialFeed').width()*0.95)/2);
					//alert(largh);
					$('.fb-like-box').attr('data-width',largh);
					//massima larghezza twitter = 520
					if (largh>520) largh=520;
					$('.twitter-timeline').attr('width',largh);
					
					$('.fb-like-box').attr('data-height',470);
					$('.twitter-timeline').attr('height',400);
				}
				
				$(function() {
					var element_width = ($('#mainContainerDiv').width()*0.80)/8;
					var width=$('#mainContainerDiv').width()*0.80;
					var height=$('#mainContainerDiv').width()*0.20;

					var wall = new freewall("#wall_container");
					//wall.fitWidth();
					//wall.fitHeigth();
					wall.reset({
						selector: '.wall_item',
						animate: true,
						cellW: element_width,
						cellH: element_width,
						onResize: function() {
							wall.refresh();
						}
					});
					wall.fitWidth();

					var wall2 = new freewall("#wall_container2");
					wall2.reset({
						selector: '.wall_item',
						animate: true,
						cellW: element_width,
						cellH: element_width,
						onResize: function() {
							wall2.refresh();
						}
					});
					wall2.fitWidth();

					var wall3 = new freewall("#wall_container3");
					wall3.reset({
						selector: '.wall_item',
						animate: true,
						cellW: element_width,
						cellH: element_width,
						onResize: function() {
							wall3.refresh();
						}
					});
					wall3.fitWidth();

					// for scroll bar appear;
					//$(window).trigger("resize");
					//rimetto a posto l'altezza del div
					//var wallHeight = $('#wall_container').height();
					//var titleHeight = $('#wall_container').parent().children('titolo').height();
					//var testoHeight = $('#wall_container').parent().children('testo').height();
					//alert(wallHeight + " - " + titleHeight + " - " + testoHeight);
					//var oldHeight = $('#wall_container').parent().parent().height();
					//alert(oldHeight);
					//$('#wall_container').parent().parent().height(oldHeight*1.3);
				});

				//esiste la sezione dove mettere il grafico a pallini?
				if ($('#sectionCompetenze').length != 0)
				{
					//var language=$.urlParam('lang');
					var language=$("#langHidden").val();

					var w = $(window).width()*0.95, h = $(window).height()*0.70
					//devo fare la larghezza in proporzione all'altezza
					w=h*2.2;
					var marginLeft = -(w/2)*0.8;
					$('#viewport').attr('width', w);
					$('#viewport').attr('height', h);

					//riposiziono il canvas al centro
					$('#viewport').css("position","absolute");
					$('#viewport').css("position","absolute");
					$('#viewport').css("top","10px");
					$('#viewport').css("left","50%");
					$('#viewport').css("margin-left",marginLeft);

					if(language == "EN"){
						$.getScript( "arbor-v0.92/competenze_grigio_EN.js" );
					}else{
						$.getScript( "arbor-v0.92/competenze_grigio.js" );
					}
				
					/*
					//alert("eccolo");
					setTimeout(function() {
						var w = $('#sectionCompetenze').width()*0.95, h = 500;

						//alert(w);
						//alert(h);
						var labelDistance = 0;

						//alert("ciao");
						
		//				var vis = d3.select(".container").append("svg:svg").attr("width", w).attr("height", h).style("position", "absolute").style("top", "80px"); 
						var vis = d3.select("#sectionCompetenze").append("svg:svg").attr("width", w).attr("height", h).attr("id", "grafico").style("position", "absolute").style("top", "0px").style("background", "transparent").style("display", "none");
						
						//alert(vis);
						
						var nodes = [];
						var labelAnchors = [];
						var labelAnchorLinks = [];
						var links = [];
						var labelText="";
						
						for(var i = 0; i < 28; i++) {
							
							if (i==2) labelText="APPLICAZIONI WEB";
							else if (i==6) labelText="APP MOBILE E MULTICANALE";
							else if (i==10) labelText="WEB MARKETING";
							else if (i==14) labelText="REALTA' AUMENTATA";
							else if (i==16) labelText="APPLICAZIONI 3D";
							else if (i==18) labelText="INTERNET DELLE COSE";
							else if (i==20) labelText="TECNOLOGIE EMERGENTI";
							else if (i==21) labelText="SISTEMI DI COLLABORAZIONE AZIENDALE";
							else if (i==25) labelText="IDENTITA' DIGITALE";
							else if (i==27) labelText="GESTIONE DELLE INFORMAZIONI";
							else labelText="";
							
							
							var node = {label : labelText};
							nodes.push(node);
							labelAnchors.push({	node : node	});
							labelAnchors.push({	node : node	});
						
						};

						var linkWeight=0.6;

						links.push({source : 25,	target : 13,	weight : linkWeight});
						links.push({source : 1,	target : 26,	weight : linkWeight});
						links.push({source : 19,	target : 27,	weight : linkWeight});
						links.push({source : 16,	target : 0,	weight : linkWeight});
						
						links.push({source : 17,	target : 24,	weight : linkWeight});
						//links.push({source : 19,	target : 25,	weight : linkWeight}); --
						links.push({source : 21,	target : 26,	weight : linkWeight});
						//links.push({source : 23,	target : 27,	weight : linkWeight}); --


						//links.push({source : 0,	target : 16,	weight : linkWeight});
						links.push({source : 2,	target : 17,	weight : linkWeight});
						links.push({source : 4,	target : 18,	weight : linkWeight});
						links.push({source : 6,	target : 19,	weight : linkWeight});
						links.push({source : 8,	target : 20,	weight : linkWeight});
						links.push({source : 10,	target : 21,	weight : linkWeight});
						links.push({source : 12,	target : 22,	weight : linkWeight});
						links.push({source : 14,	target : 23,	weight : linkWeight});

						//links.push({source : 24,	target : 25,	weight : linkWeight});
						//links.push({source : 25,	target : 26,	weight : linkWeight});
						//links.push({source : 26,	target : 27,	weight : linkWeight});
						links.push({source : 27,	target : 24,	weight : linkWeight});

						//links.push({source : 16,	target : 17,	weight : linkWeight});
						links.push({source : 17,	target : 18,	weight : linkWeight});
						links.push({source : 18,	target : 19,	weight : linkWeight});
						links.push({source : 19,	target : 20,	weight : linkWeight});
						links.push({source : 20,	target : 21,	weight : linkWeight});
						links.push({source : 21,	target : 22,	weight : linkWeight});
						links.push({source : 22,	target : 23,	weight : linkWeight});
						links.push({source : 23,	target : 16,	weight : linkWeight});

						links.push({source : 0,	target : 1,	weight : linkWeight});
						//links.push({source : 1,	target : 2,	weight : linkWeight});
						links.push({source : 2,	target : 3,	weight : linkWeight});
						links.push({source : 3,	target : 4,	weight : linkWeight});
						links.push({source : 4,	target : 5,	weight : linkWeight});
						links.push({source : 5,	target : 6,	weight : linkWeight});
						links.push({source : 6,	target : 7,	weight : linkWeight});
						links.push({source : 7,	target : 8,	weight : linkWeight});
						links.push({source : 8,	target : 9,	weight : linkWeight});
						links.push({source : 9,	target : 10,	weight : linkWeight});
						links.push({source : 10,	target : 11,	weight : linkWeight});
						links.push({source : 11,	target : 12,	weight : linkWeight});
						links.push({source : 12,	target : 13,	weight : linkWeight});
						links.push({source : 13,	target : 14,	weight : linkWeight});
						links.push({source : 14,	target : 15,	weight : linkWeight});
						links.push({source : 15,	target : 0,	weight : linkWeight});

						
						for(var i = 0; i < nodes.length; i++)
						{
							labelAnchorLinks.push({	source : i * 2,
													target : i * 2 + 1,
													weight : 0.5
													});
						};

						var force = d3.layout.force().size([w, h]).nodes(nodes).links(links).gravity(1).linkDistance(10).charge(-2000).linkStrength(function(x) {
							return x.weight * 10
						});


						force.start();

						
						$("#grafico" ).fadeIn(3000);
						
						var force2 = d3.layout.force().nodes(labelAnchors).links(labelAnchorLinks).gravity(0).linkDistance(0).linkStrength(8).charge(-100).size([w, h]);
						force2.start();

						
		//				for (var i=1;i<10;i++)
		//				{
							setInterval(function() {	force.alpha(0.03);}, 3000);
		//				}
						
						var link = vis.selectAll("line.link").data(links).enter().append("svg:line").attr("class", "link").style("stroke", "#58585a").style("stroke-width", "2px").style("stroke-opacity", "0.5");

						var node = vis.selectAll("g.node").data(force.nodes()).enter().append("svg:g").attr("class", "node").on("click", function(d,i)
						{
							//alert(d.label);
							if (d.label=="WEB MARKETING") window.location.href="fixed.content.php?idPage=11";
							else if (d.label=="APP MOBILE E MULTICANALE") window.location.href="fixed.content.php?idPage=10";
							else if (d.label=="APPLICAZIONI WEB") window.location.href="fixed.content.php?idPage=9";
							else if (d.label=="REALTA' AUMENTATA") window.location.href="fixed.content.php?idPage=8";
							else if (d.label=="APPLICAZIONI 3D") window.location.href="fixed.content.php?idPage=12";
							else if (d.label=="INTERNET DELLE COSE") window.location.href="fixed.content.php?idPage=7";
							else if (d.label=="TECNOLOGIE EMERGENTI") window.location.href="fixed.content.php?idPage=13";
							else if (d.label=="SISTEMI DI COLLABORAZIONE AZIENDALE") window.location.href="fixed.content.php?idPage=14";
							else if (d.label=="IDENTITA' DIGITALE") window.location.href="fixed.content.php?idPage=15";
							else if (d.label=="GESTIONE DELLE INFORMAZIONI") window.location.href="fixed.content.php?idPage=16";
							
						}).on("mouseover", function ()
						{
							document.body.style.cursor = "pointer";
						}).on("mouseout", function ()
						{
							document.body.style.cursor = "auto";
						});
			//			node.append("svg:circle").attr("r", 10).style("fill", "#EEE").style("stroke", "#F00").style("stroke-width", 1);
						node.append("svg:circle").attr("r", function(d,i)	{return (d.label=="") ? 4 : 6}).style("fill", function(d,i)	{return (d.label=="") ? "#58585a" : "#b1c903"}).style("stroke", "#58585a").style("stroke-width", function(d,i)	{return (d.label=="") ? 0 : 0}).style("fill-opacity", function(d,i)	{return (d.label=="") ? "0.3" : "1.0"});
						node.call(force.drag);
						

						var anchorLink = vis.selectAll("line.anchorLink").data(labelAnchorLinks)//.enter().append("svg:line").attr("class", "anchorLink").style("stroke", "#999");

						var anchorNode = vis.selectAll("g.anchorNode").data(force2.nodes()).enter().append("svg:g").attr("class", "anchorNode");
						anchorNode.append("svg:circle").attr("r", 0).style("fill", "#FFF");
						anchorNode.append("svg:text").text(function(d, i)
						{
							return i % 2 == 0 ? "" : d.node.label
						}).style("fill", "#58585a").style("font-family", "Maven Pro").style("font-size", 16).on("mouseover", function ()
						{
							document.body.style.cursor = "pointer";
						}).on("mouseout", function ()
						{
							document.body.style.cursor = "auto";
						}).on("click", function(d,i)
						{
							//alert(d.node.label);
							if (d.node.label=="WEB MARKETING") window.location.href="fixed.content.php?idPage=11";
							else if (d.node.label=="APP MOBILE E MULTICANALE") window.location.href="fixed.content.php?idPage=10";
							else if (d.node.label=="APPLICAZIONI WEB") window.location.href="fixed.content.php?idPage=9";
							else if (d.node.label=="REALTA' AUMENTATA") window.location.href="fixed.content.php?idPage=8";
							else if (d.node.label=="APPLICAZIONI 3D") window.location.href="fixed.content.php?idPage=12";
							else if (d.node.label=="INTERNET DELLE COSE") window.location.href="fixed.content.php?idPage=7";
							else if (d.node.label=="TECNOLOGIE EMERGENTI") window.location.href="fixed.content.php?idPage=13";
							else if (d.node.label=="SISTEMI DI COLLABORAZIONE AZIENDALE") window.location.href="fixed.content.php?idPage=14";
							else if (d.node.label=="IDENTITA' DIGITALE") window.location.href="fixed.content.php?idPage=15";
							else if (d.node.label=="GESTIONE DELLE INFORMAZIONI") window.location.href="fixed.content.php?idPage=16";
							
						});

						var updateLink = function() {
							this.attr("x1", function(d) {
								return d.source.x;
							}).attr("y1", function(d) {
								return d.source.y;
							}).attr("x2", function(d) {
								return d.target.x;
							}).attr("y2", function(d) {
								return d.target.y;
							});

						}

						var updateNode = function() {
							this.attr("transform", function(d) {
								return "translate(" + d.x + "," + d.y + ")";
							});

						}


						force.on("tick", function() {

							
							force2.start();

							node.call(updateNode);

							anchorNode.each(function(d, i) {
								if(i % 2 == 0) {
									d.x = d.node.x;
									d.y = d.node.y;
								} else {
									var b = this.childNodes[1].getBBox();

									var diffX = d.x - d.node.x;
									var diffY = d.y - d.node.y;

									var dist = Math.sqrt(diffX * diffX + diffY * diffY);

									var shiftX = b.width * (diffX - dist) / (dist * 2);
									shiftX = Math.max(-b.width, Math.min(0, shiftX));
									var shiftY = 5;
									this.childNodes[1].setAttribute("transform", "translate(" + shiftX + "," + shiftY + ")");
								}
							});


							anchorNode.call(updateNode);
							link.call(updateLink);
							anchorLink.call(updateLink);

						});
					}, 10);
					*/
				}
				
				
				setTimeout(function()
				{
					$('body').scrollspy({ target: '#navbar-laterale' })

					//nella url ci sta un parametro col nome del prodotto su cui andare?
					var prodotto=$.urlParam('prod');
					//alert(prodotto);
					if (prodotto!="")
					{
						 var target = $('#section'+prodotto);
							var name=(target.attr('id'));
						  //alert(name.charAt(0));
						  //alert(name);
						  
						  if (name.charAt(0)=="t") offsetTimeline=120; //timeline
						  else offsetTimeline=0;

						  if (target.length) {
							$('html,body').animate({
							  scrollTop: target.offset().top-100-offsetTimeline
							}, 500);
							return false;
						  }

					}
				}, 1100);

				if(typeof slider !== "undefined" && $.urlParam('idPage') == 1){
					$.each(slider.slides, function(i,item) {
					  addSlide(item);
					});
					jQuery("#layerslider").layerSlider(slider.options);
				}
				if(typeof slider !== "undefined" && $.urlParam('idPage') == 6){
					$.each(slider.slides, function(i,item) {
					  addSlide(item);
					});
					jQuery("#layerslider").layerSlider(slider.options);
				}

			}, 10);

		});





	</script>

  </body>

</html>
