<?php

include 'lang.php';
 
 ?>

<!DOCTYPE html>
<html>
  <head>
    <title></title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="HandheldFriendly" content="true" />

    <style>

        .scroll-down-link:link,
        .scroll-down-link:visited,
        .scroll-down-link:hover {
            font-size: 15px;
            color: #b1c903;
            text-decoration:none;
            background-image: url('images/line_green.png');
            background-repeat: no-repeat;
            background-position: center bottom;
            background-size:50% 1px;
        }

        body {
          background-color: white !important;
        }

    </style>

  </head>

  <body>

    <!-- il valore 'min-height' serve come tweak per evitare che il browser consideri il div ad altezza pari a zero e conseguentemente faccia il caricamento di tutti i div in sequenza -->
    <div role="dynamic-div" style="display:block; background-color: white; min-height:1px; padding-bottom:20px;"></div>

  </body>

</html>
