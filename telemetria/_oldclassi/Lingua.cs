﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;

namespace _oldclassi
{
    public class Lingua
    {
        private string l;
        private XmlDocument xd;
        private string pathBase = "";
        public Lingua(string l, string pathBase)
        {
            this.l = l;
            this.pathBase = pathBase;
            xd = new XmlDocument();
        }

        public void load()
        {

            xd.Load(pathBase + "/language/" + l + ".xml");

        }

        public string getString(string name)
        {
            XmlNode xn = xd.SelectSingleNode("//label[@id='" + name + "']");
            if (xn != null)
            {
                if (xn.HasChildNodes)
                {
                    return xn.FirstChild.Value;
                }
            }
            return name;
        }

       
    }
}
