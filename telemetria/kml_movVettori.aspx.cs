﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

using System.Data;
using System.Data.SqlClient;
using Newtonsoft.Json;

public partial class kml_movVettori : System.Web.UI.Page
{
    string pointLines = "";

    string oraDb;
    private SqlConnection oc;


    private string checkNameParameter(string name)
    {
        if (name.Substring(0, 1) != "@")
        {
            return "@" + name;
        }
        else
        {
            return name;
        }
    }


    private string checkStoredNameParameter(string name)
    {
        //toglie il nome del package. Nel caso locale non serve.

        int idxPoint = name.IndexOf(".");
        if (idxPoint == -1)
        {
            return name;
        }
        else
        {
            return name.Substring(idxPoint + 1);
        }
    }

    private SqlDbType obtainType(string type)
    {

        SqlDbType typeToRet = SqlDbType.VarChar;
        switch (type)
        {
            case "Number":
                typeToRet = SqlDbType.Int;
                break;
            case "Cursor":
                //null lo ingnora in quanto non ce necessita di definire parametri di output per le connesioni sql server
                break;
            case "String":
                typeToRet = SqlDbType.VarChar;
                break;
            case "VarChar":
                typeToRet = SqlDbType.VarChar;
                break;
            case "Bool":
                typeToRet = SqlDbType.Bit;
                break;
            case "DateTime":
                typeToRet = SqlDbType.DateTime;
                break;
            case "float":
                typeToRet = SqlDbType.Float;
                break;
            case "money":
                typeToRet = SqlDbType.Money;
                break;
        }
        return typeToRet;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        Response.ContentType = "text/xml";
        Response.Write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
        Response.Write("<kml xmlns=\"http://www.opengis.net/kml/2.2\">");
        Response.Write("<Document>");
        Response.Write("<Name>Posizione Squadre</Name>");
        //DBAccessAlba db = new DBAccessAlba();
        
        oraDb = ConfigurationSettings.AppSettings["LocalConnection"].ToString();
        oc = new System.Data.SqlClient.SqlConnection(oraDb);
        oc.Open();

        try
        {
            //appendStyleSquadre();

            SqlCommand objCmd = new SqlCommand();
            objCmd.Connection = oc;
            objCmd.CommandTimeout = 120;
            objCmd.CommandText = checkStoredNameParameter("SP_getKmlVettori"); //NomePackage.NomeProcedure 
            objCmd.CommandType = CommandType.StoredProcedure;

            if (Int32.Parse(Request.Params["idVettore"].ToString()) == -1)
            {
                objCmd.Parameters.Add(new SqlParameter(checkNameParameter("@idVettore"), obtainType("Number"))).Value = DBNull.Value;
            }
            else
            {
                objCmd.Parameters.Add(new SqlParameter(checkNameParameter("@idVettore"), obtainType("Number"))).Value = Request.Params["idVettore"];
            }


            //if ((Request.Params["codSq"] != null) && (Request.Params["day"] != null) && (Request.Params["start"] != null) && (Request.Params["stop"] != null))
            //{
            //    DateTime day = DateTime.Parse(Request.Params["day"].ToString());
            //    string codSq = Request.Params["codSq"].ToString();
            //    string[] start = Request.Params["start"].ToString().Split("_".ToCharArray());
            //    string[] stop = Request.Params["stop"].ToString().Split("_".ToCharArray());

            //    DataTable dtLoc = db.getMoveSquadra(day, codSq, int.Parse(start[0]), int.Parse(start[1]), int.Parse(stop[1]), int.Parse(stop[0])).Tables[0];

            SqlDataAdapter adapter = new SqlDataAdapter(objCmd);
            System.Data.DataSet dsReturn = new DataSet();
            adapter.Fill(dsReturn);
            DataTable dtLoc = dsReturn.Tables[0];

            string[] v1 = new string[0];
            string[] v2 = new string[0];
            string[] v3 = new string[0];
            string[] v4 = new string[0];
            if (Request.Params["v1"] != null)
            {
                v1 = Request.Params["v1"].ToString().Split("_".ToCharArray());
            }
            if (Request.Params["v2"] != null)
            {
                v2 = Request.Params["v2"].ToString().Split("_".ToCharArray());
            }
            if (Request.Params["v3"] != null)
            {
                v3 = Request.Params["v3"].ToString().Split("_".ToCharArray());
            }
            if (Request.Params["v4"] != null)
            {
                v4 = Request.Params["v4"].ToString().Split("_".ToCharArray());
            }

            if (v1.Length > 0)
            {

                appendPlacemark(dtLoc, dtLoc.Rows.Count, v1, v2, v3, v4);
            }
            else
            {
                appendPlacemark(dtLoc, dtLoc.Rows.Count);
            }
            drawConnector();
            //}


        }
        catch (Exception ex)
        {
            Response.Write(ex.Message.ToString() + " - " + ex.StackTrace.ToString());
        }
        finally
        {
            oc.Close(); 
        }
        Response.Write("</Document></kml>");
    }


    private void drawConnector()
    {
        Response.Write("<Placemark>");
        Response.Write("<name>Path</name>");
        Response.Write("<styleUrl>root://styles#default+icon=0x307</styleUrl>");
        Response.Write("<Style id=\"khStyle578\">");
        Response.Write("<LineStyle id=\"khLineStyle582\">");
        Response.Write("<color>ff0000ff</color>");
        Response.Write("<width>3</width>");
        Response.Write("</LineStyle>");
        Response.Write("</Style>");
        Response.Write("<LineString id=\"khLineString585\">");
        Response.Write("<tessellate>1</tessellate>");
        Response.Write("<coordinates>");
        Response.Write( pointLines);
        Response.Write("</coordinates>");
        Response.Write("</LineString>");
        Response.Write("</Placemark>");
    }
    private void appendPlacemark(DataTable dt,int numSquadre)
    {
       
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            string idSquadra = dt.Rows[i]["idVettore"].ToString();
           
                DateTime when = (DateTime)(dt.Rows[i]["DateTime"]);

                
                Response.Write("<Placemark>");
                 Response.Write("<name>" + dt.Rows[i]["Nome"] + "</name>");
                 Response.Write("<description>" + when.ToLongDateString() +" " + when.ToLongTimeString() + "</description>");
                 Response.Write("<TimeStamp>");
               Response.Write("<when>" + when.Year + "-" + when.Day +"-" + when.Month +"T" + when.Hour +":" + when.Minute+":" + when.Second+"Z</when>");
                Response.Write("</TimeStamp>");
                string strIcon = "<styleUrl>#icon_" + idSquadra + "</styleUrl>";    
                if (i == 0)
                {
                    strIcon ="<styleUrl>#icon_start</styleUrl>";
                }
                else
                {
                    if (i == dt.Rows.Count - 1)
                    {
                        strIcon = "<styleUrl>#icon_stop</styleUrl>";
                    }
                }
                Response.Write(strIcon);
                Response.Write("<Point>");
                Response.Write("<coordinates>" + dt.Rows[i]["Lon"] + "," + dt.Rows[i]["Lat"] + ",0</coordinates>");
                pointLines += dt.Rows[i]["Lon"] + "," + dt.Rows[i]["Lat"] + ",0 ";
                Response.Write("</Point>");
               
                Response.Write("</Placemark>");
            
        }
    }
    private object[] scambia(float[] v, float[] vv)
    {
        float[] appoggio = new float[2];
        appoggio[0] = v[0];
        appoggio[1] = v[1];
        v[0] = vv[0];
        v[1] = vv[1];
        vv[0] = appoggio[0];
        vv[1] = appoggio[1];
        object[] obj = new object[2];
        obj[0] = v;
        obj[1] = vv;
        return obj;
    }
    private int segno(float[] a, float[] b, float[] p)
    {
        float c, t;
        t = 10^(-6);
        c = p[0] * (a[1] - b[1]) - p[1] * (a[0] - b[0]) + a[0] * b[1] - a[1] * b[0];
        if (Math.Abs(c) < t)
            return 0;
        else
        {
            if (c > 0) { return 1; }
            else { return -1; }
        }
    }
    private void appendPlacemark(DataTable dt, int numSquadre,string[] v1,string[] v2,string[] v3,string[] v4)
    {
         ///
        //ordinamento punti
        ///
        float[] v1f = new float[2];
        v1f[0] = float.Parse(v1[0].Replace(".",","));
        v1f[1] = float.Parse(v1[1].Replace(".", ","));
        float[] v2f = new float[2];
        v2f[0] = float.Parse(v2[0].Replace(".", ","));
        v2f[1] = float.Parse(v2[1].Replace(".", ","));
        float[] v3f = new float[2];
        v3f[0] = float.Parse(v3[0].Replace(".", ","));
        v3f[1] = float.Parse(v3[1].Replace(".", ","));
       /* float[] v4f = new float[2];
        v4f[0] = float.Parse(v4[0].Replace(".", ","));
        v4f[1] = float.Parse(v4[1].Replace(".", ","));*/

        int s1, s2, s3;
       
       /* float[] appoggio = new float[2];
        //controllo delle latitudini
        if (v1f[1] > v2f[1])
        {
            appoggio[0] = v1f[0];
            appoggio[1] = v1f[1];
            v1f[0] = v2f[0];
            v1f[1] = v2f[1];
            v2f[0] = appoggio[0];
            v2f[1] = appoggio[1];
        }
        if (v4f[1] > v3f[1])
        {
            appoggio[0] = v4f[0];
            appoggio[1] = v4f[1];
            v4f[0] = v3f[0];
            v4f[1] = v3f[1];
            v3f[0] = appoggio[0];
            v3f[1] = appoggio[1];
        }
        //controllo delle longitudini
        if (v1f[0] < v4f[0])
        {
            appoggio[0] = v1f[0];
            appoggio[1] = v1f[1];
            v1f[0] = v4f[0];
            v1f[1] = v4f[1];
            v4f[0] = appoggio[0];
            v4f[1] = appoggio[1];
        }
        if (v2f[0] < v3f[0])
        {
            appoggio[0] = v2f[0];
            appoggio[1] = v2f[1];
            v2f[0] = v3f[0];
            v2f[1] = v3f[1];
            v3f[0] = appoggio[0];
            v3f[1] = appoggio[1];
        }
        if ((v2f[0] > v1f[0]) && (v3f[0] > v4f[0]) && (v2f[1] < v3f[1]) && (v1f[1] < v4f[1]))
        {
            object[] o = scambia(v1f, v2f);
            v1f = (float[])(o[0]);
            v2f = (float[])(o[1]);

            o = scambia(v2f, v3f);
            v2f = (float[])(o[0]);
            v3f = (float[])(o[1]);

            o = scambia(v3f, v4f);
            v3f = (float[])(o[0]);
            v4f = (float[])(o[1]);
        }
        if ((v2f[0] > v1f[0]) && (v3f[0] > v4f[0]) && (v2f[1] > v3f[1]) && (v1f[1] > v4f[1]))
        {
            object[] o = scambia(v1f, v3f);
            v1f = (float[])(o[0]);
            v3f = (float[])(o[1]);
          
        }
        * */
        /*float bigX = calcolaBig(v1f[0], v2f[0], v3f[0], v4f[0]);
        float smallX = calcolaSmall(v1f[0], v2f[0], v3f[0], v4f[0]);

        float bigY = calcolaBig(v1f[1], v2f[1], v3f[1], v4f[1]);
        float smallY = calcolaSmall(v1f[1], v2f[1], v3f[1], v4f[1]);*/
        ///
        int valid = 0;
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            string idSquadra = dt.Rows[i]["idVettore"].ToString();

            DateTime when = (DateTime)(dt.Rows[i]["DateTime"]);

            float lonY = float.Parse(dt.Rows[i]["Lon"].ToString().Replace(".", ","));
            float latX = float.Parse(dt.Rows[i]["Lat"].ToString().Replace(".", ","));
            float[] p = new float[2];
            p[0] = latX; 
            p[1] = lonY;
             s1 = segno(v1f,v2f,v3f)*segno(v1f,v2f,p);
            s2 = segno(v2f,v3f,v1f)*segno(v2f,v3f,p);
            s3= segno(v3f,v1f,v2f)*segno(v3f,v1f,p);
            
            //if (((lonY >= v1f[1]) && (lonY <= v2f[1]) && (lonY >= v4f[1]) && (lonY <= v3f[1])) && ((latX >= v4f[0]) && (latX <= v1f[0]) && (latX >= v3f[0]) && (latX <= v2f[0])))
            //if ((lonY >= smallY)&&(lonY <= bigY)&&(latX>=smallX)&&(latX <= bigX))
            if ((s1<0)||(s2<0)||(s3<0))
            {//il punto è fuori
            }
            else
            {
            Response.Write("<Placemark>");
            Response.Write("<name>" + dt.Rows[i]["Nome"] + "</name>");
            Response.Write("<description>Ultimo aggiornamento: " + when.ToLongDateString() + " " + when.ToLongTimeString() + "</description>");
            Response.Write("<TimeStamp>");
            Response.Write("<when>" + when.Year + "-" + when.Day + "-" + when.Month + "T" + when.Hour + ":" + when.Minute + ":" + when.Second + "Z</when>");
            Response.Write("</TimeStamp>");
            string strIcon = "<styleUrl>#icon_" + idSquadra + "</styleUrl>";
            if (valid == 0)
            {
                strIcon = "<styleUrl>#icon_start</styleUrl>";
            }
            /*else
            {
                if (i == dt.Rows.Count - 1)
                {
                    strIcon = "<styleUrl>#icon_stop</styleUrl>";
                }
            }*/
            Response.Write(strIcon);

            Response.Write("<Point>");
            Response.Write("<coordinates>" + dt.Rows[i]["Lon"] + "," + dt.Rows[i]["Lat"] + ",0</coordinates>");
            pointLines += dt.Rows[i]["Lon"] + "," + dt.Rows[i]["Lat"] + ",0 ";
            Response.Write("</Point>");

            Response.Write("</Placemark>");
            valid++;
            }

        }
    }
    private float calcolaBig(float a,float b,float c, float d)
    {

        float big = a;
        if (b > big)
        {
            big = b;
        }
        if (c > big)
        {
            big = c;
        }
        if (d > big)
        {
            big = d;
        }
        return big;
    }
    private float calcolaSmall(float a, float b, float c, float d)
    {

        float small = a;
        if (b < small)
        {
            small = b;
        }
        if (c < small)
        {
            small = c;
        }
        if (d < small)
        {
            small = d;
        }
        return small;
    }


    //private void appendStyleSquadre()
    //{
    //    string urlImg = System.Configuration.ConfigurationManager.AppSettings["urlImgKml"];
    //    DBAccessAlba db = new DBAccessAlba();
    //    try
    //    {
    //        db.connOpen();
    //        DataTable dt = db.searchSquadra("%").Tables[0];
    //        for (int i = 0; i < dt.Rows.Count; i++)
    //        {
    //            Response.Write("<Style id=\"icon_" + dt.Rows[i]["IDSquadra"] + "\">");
    //        Response.Write("<IconStyle>");
    //        Response.Write("<scale>1</scale>");
    //        Response.Write("<Icon>");
          
    //        //Response.Write("<href>http://maps.google.com/mapfiles/kml/paddle/A.png</href>");
    //        Response.Write("<href>" + System.Configuration.ConfigurationManager.AppSettings["pathImgWeb"] + dt.Rows[i]["PathImg"] + "</href>");
    //        Response.Write("</Icon>");
    //        //Response.Write("<hotSpot x=\"0\" y=\"0\" xunits=\"pixels\" yunits=\"pixels\" />");
    //        Response.Write("</IconStyle>");
    //        Response.Write("</Style>");
    //        }

    //        Response.Write("<Style id=\"icon_start\">");
    //        Response.Write("<IconStyle>");
    //        Response.Write("<scale>1</scale>");
    //        Response.Write("<Icon>");
    //        Response.Write("<href>" + System.Configuration.ConfigurationManager.AppSettings["cursorStart"] + "</href>");
    //        Response.Write("</Icon>");
    //        Response.Write("</IconStyle>");
    //        Response.Write("</Style>");

    //        Response.Write("<Style id=\"icon_stop\">");
    //        Response.Write("<IconStyle>");
    //        Response.Write("<scale>1</scale>");
    //        Response.Write("<Icon>");
    //        Response.Write("<href>" + System.Configuration.ConfigurationManager.AppSettings["cursorStop"] + "</href>");
    //        Response.Write("</Icon>");
    //        Response.Write("</IconStyle>");
    //        Response.Write("</Style>");
          
    //    }
    //    catch (Exception ex)
    //    {
    //    }
    //    finally
    //    {
    //        db.connClose();
    //    }

    //}

}
