﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Drawing;
using System.Text;
using System.IO;
using System.Data;
using System.Net;
using System.Data.SqlClient;
using Newtonsoft.Json;
using System.Net.Mail;
using classi;
using System.Xml;

namespace classi
{
    public class Lingua
    {
        private string l;
        private XmlDocument xd;
        private string pathBase = "";
        public Lingua(string l, string pathBase)
        {
            this.l = l;
            this.pathBase = pathBase;
            xd = new XmlDocument();
        }

        public void load()
        {

            xd.Load(pathBase + "/telemetria/language/" + l + ".xml");

        }

        public string getString(string name)
        {
            XmlNode xn = xd.SelectSingleNode("//label[@id='" + name + "']");
            if (xn != null)
            {
                if (xn.HasChildNodes)
                {
                    return xn.FirstChild.Value;
                }
            }
            return name;
        }


    }
}


public partial class UtilsTelemetria : System.Web.UI.Page
{


    string oraDb;
    private SqlConnection oc;
    private String RES;
    protected Lingua l;

    string pointLines = "";

    private string checkNameParameter(string name)
    {
        if (name.Substring(0, 1) != "@")
        {
            return "@" + name;
        }
        else
        {
            return name;
        }
    }

    private string checkStoredNameParameter(string name)
    {
        //toglie il nome del package. Nel caso locale non serve.

        int idxPoint = name.IndexOf(".");
        if (idxPoint == -1)
        {
            return name;
        }
        else
        {
            return name.Substring(idxPoint + 1);
        }
    }



    private SqlDbType obtainType(string type)
    {

        SqlDbType typeToRet = SqlDbType.VarChar;
        switch (type)
        {
            case "Number":
                typeToRet = SqlDbType.Int;
                break;
            case "Cursor":
                //null lo ingnora in quanto non ce necessita di definire parametri di output per le connesioni sql server
                break;
            case "String":
                typeToRet = SqlDbType.VarChar;
                break;
            case "VarChar":
                typeToRet = SqlDbType.VarChar;
                break;
            case "Bool":
                typeToRet = SqlDbType.Bit;
                break;
            case "DateTime":
                typeToRet = SqlDbType.DateTime;
                break;
            case "float":
                typeToRet = SqlDbType.Float;
                break;
            case "money":
                typeToRet = SqlDbType.Money;
                break;
        }
        return typeToRet;
    }
 
   

    protected void Page_Load(object sender, EventArgs e)
    {

        l = new Lingua("IT".ToString(), Server.MapPath("/"));
        l.load();

        string action = Request.Params["action"];
            if (action == null)
                return;
            switch (action)
            {
                case "getAnomalia":
                    getAnomalia();
                    break;
            }
    }

	 private void addGinguAlert(string codVettore, string type, string altro)
        {
            WebClient client = new WebClient();

            // set the user agent to IE6
            client.Headers.Add("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; .NET CLR 1.0.3705;)");
            try
            {
                // actually execute the GET request
                string ret = client.DownloadString("http://smoconline.unionkey.com/PackageGovernance/GestioneAlert/addExtAlert.aspx?codVettore=" + codVettore + "&type=" + type + "&altro=" + altro);

                // ret now contains the contents of the webpage
                //Console.WriteLine("First 256 bytes of response: " + ret.Substring(0, 265));
            }
            catch (WebException we)
            {
                // WebException.Status holds useful information
                //Console.WriteLine(we.Message + "\n" + we.Status.ToString());
            }
            catch (NotSupportedException ne)
            {
                // other errors
                //Console.WriteLine(ne.Message);
            }
        
        }

    private void getAnomalia()
    {
        try
        {
            oraDb = ConfigurationSettings.AppSettings["LocalConnection"].ToString();
            oc = new System.Data.SqlClient.SqlConnection(oraDb);

            oc.Open();

            SqlCommand objCmd = new SqlCommand();
            objCmd.Connection = oc;
            objCmd.CommandTimeout = 120;
            objCmd.CommandText = checkStoredNameParameter("[SP_getAnomalia]"); //NomePackage.NomeProcedure 
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add(new SqlParameter(checkNameParameter("@rangeMin"), obtainType("Number"))).Value = ConfigurationSettings.AppSettings["rangeMin"].ToString();// DBNull.Value;
            

            SqlDataAdapter adapter = new SqlDataAdapter(objCmd);
            System.Data.DataSet dsReturn = new DataSet();
            adapter.Fill(dsReturn);
            DataTable cercati = dsReturn.Tables[0];
            string elencoVettori = "";
            

            //################## INVIO MAIL
            if (cercati.Rows.Count > 0)
            {
                String testoMail = "";
                elencoVettori += "<ul>";
                for (int i = 0; i < cercati.Rows.Count; i++)
                {
                    elencoVettori += "<li><b>" + cercati.Rows[i]["targa"].ToString().Trim() + "</b> " + l.getString("lblInattivoDal") + " "+ cercati.Rows[i]["ultimaAttivita"].ToString().Trim() + "</li>";
                    //CODICE NUOVO ALERT
		    addGinguAlert(cercati.Rows[i]["idVettore"].ToString(),"MANOMISSIONE","");
		}

                testoMail = l.getString("msgReportAnomalie"); 
                testoMail = testoMail.Replace("$$$dataGen$$$", DateTime.Now.Day.ToString() + "/" + DateTime.Now.Month.ToString() + "/" + DateTime.Now.Year.ToString());
                testoMail = testoMail.Replace("$$$vettori$$$", elencoVettori);




                MailMessage mm = new MailMessage();
                mm.Body = testoMail;
                mm.Sender = new MailAddress("ferrantes@unionkey.com");
                mm.From = new MailAddress("ferrantes@unionkey.com");
                mm.Subject = l.getString("lblHeaderServizioAlerting");
                mm.IsBodyHtml = true;

                mm.To.Add(new MailAddress(ConfigurationSettings.AppSettings["destAlertMail"].ToString()));

                //############# INVIO MULTIPLO
                //string[] destEmail = elencoMail.Rows[0][0].ToString().Split(';');
                ////Response.Write("<br><br>" + destEmail.Length);
                //for (int iMail = 0; iMail < destEmail.Length; iMail++)
                //{
                //    if (destEmail[iMail] != "")
                //    {
                //        mm.To.Add(new MailAddress(destEmail[iMail]));
                //        Response.Write("INVIO MAIL: " + destEmail[iMail] + "<br/>");
                //    }
                //}

                SmtpClient sc = new SmtpClient(System.Configuration.ConfigurationManager.AppSettings["smtpServer"].ToString());
                //sc.EnableSsl = true;
                //sc.Credentials = new System.Net.NetworkCredential(System.Configuration.ConfigurationManager.AppSettings["smtpUser"].ToString(), System.Configuration.ConfigurationManager.AppSettings["smtpPwd"].ToString());
                sc.Send(mm);

                //#########################################

                Response.Write("INVIO MAIL: " + ConfigurationSettings.AppSettings["destAlertMail"].ToString() + "<br/>");
                Response.Write(testoMail);
            }
            

            
            oc.Close();
        }
        catch (Exception ex)
        {
            Response.Write(ex.Message.ToString() + " - " + ex.StackTrace.ToString());
        }
        finally
        {
            oc.Close();
        }
    }



}
