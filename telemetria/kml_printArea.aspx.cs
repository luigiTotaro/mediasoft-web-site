﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Collections;

public partial class kml_printArea : System.Web.UI.Page
{
    string pointLines = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        Response.ContentType = "text/xml";
        Response.Write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
        Response.Write("<kml xmlns=\"http://www.opengis.net/kml/2.2\">");
        Response.Write("<Document>");
        Response.Write("<Name>Area Squadrre</Name>");
       
        try
        {
           
            appendStyleSquadre();
            //if ((Request.Params["v1"] != null) && (Request.Params["v2"] != null) && (Request.Params["v3"] != null) && (Request.Params["v4"] != null))
            //{
            ArrayList arrList = new ArrayList();
            try
            {
                int v = 1;
                while (true)
                {
                    string[] vert = new string[0];
                    vert = Request.Params["v" + v].ToString().Split("_".ToCharArray());
                    arrList.Add(vert);
                    v++;
                   // Response.Write(Request.Params["v" + v].ToString());
                }
            }
            catch (Exception ex)
            {
                //Response.Write(ex.Message);
            
            }
            appendPlacemark(arrList);
             /* string[] v1 = new string[0];
              string[] v2 = new string[0];
              string[] v3 = new string[0];
              string[] v4 = new string[0];
                if (Request.Params["v1"] != null)
                {
                    v1 = Request.Params["v1"].ToString().Split("_".ToCharArray());
                }
                if (Request.Params["v2"] != null)
                {
                    v2 = Request.Params["v2"].ToString().Split("_".ToCharArray());
                }
                if (Request.Params["v3"] != null)
                {
                    v3 = Request.Params["v3"].ToString().Split("_".ToCharArray());
                }
                if (Request.Params["v4"] != null)
                {
                    v4 = Request.Params["v4"].ToString().Split("_".ToCharArray());
                }*/
                                           

                
                //appendPlacemark(v1,v2,v3,v4);
          
                    
            //drawConnector();
            //}
            

        }
        catch (Exception ex)
        {
            Response.Write(ex.Message);
        
        }
        finally
        { 
        
        }
        Response.Write("</Document></kml>");
    }
    private void drawConnector()
    {
        Response.Write("<Placemark>");
        Response.Write("<name>Path</name>");
        Response.Write("<styleUrl>root://styles#default+icon=0x307</styleUrl>");
        Response.Write("<Style id=\"khStyle578\">");
        Response.Write("<LineStyle id=\"khLineStyle582\">");
        Response.Write("<color>ff0000ff</color>");
        Response.Write("<width></width>");
        Response.Write("</LineStyle>");
        Response.Write("</Style>");
        Response.Write("<LineString id=\"khLineString585\">");
        Response.Write("<tessellate>1</tessellate>");
        Response.Write("<coordinates>");
        Response.Write( pointLines);
        Response.Write("</coordinates>");
        Response.Write("</LineString>");
        Response.Write("</Placemark>");
    }
   // private void appendPlacemark(string[] v1,string[] v2,string[] v3,string[] v4)
    private void appendPlacemark(ArrayList arrL)
      
{
        Response.Write("<Placemark>");
        Response.Write(" <name>Absolute</name>");
         Response.Write("<visibility>1</visibility>");
         Response.Write("<styleUrl>#transBluePoly</styleUrl>");
          Response.Write("<Polygon>");
            Response.Write("<tessellate>1</tessellate>");
            Response.Write("<altitudeMode>clampToGround</altitudeMode>");
            Response.Write("<outerBoundaryIs>");
            Response.Write("<LinearRing>");
            Response.Write("<coordinates>");
           // -112.3372510731295,36.14888505105317,1784
            //-112.3356128688403,36.14781540589019,1784
            //-112.3368169371048,36.14658677734382,1784
            //-112.3384408457543,36.14762778914076,1784
            //-112.3372510731295,36.14888505105317,1784
          

        DateTime when = DateTime.Now;
        for (int i = 0; i < arrL.Count; i++)
        {
            string[] arrTemp = (string[])(arrL[i]);
            Response.Write(arrTemp[1] + "," + arrTemp[0] + ",1784 ");
        }
        if (arrL.Count > 0)
        {
            string[] arrTemp = (string[])(arrL[0]);
            Response.Write(arrTemp[1] + "," + arrTemp[0] + ",1784 ");
        }
        //if (v1.Length == 2)
        //{
        //    //vertice1
        //  /*  Response.Write("<Placemark>");
        //    Response.Write("<name>Vertice 1</name>");
        //    Response.Write("<description>" + v1[0] + " " + v1[1] + "</description>");
        //    Response.Write("<TimeStamp>");
        //    Response.Write("<when>" + when.Year + "-" + when.Day + "-" + when.Month + "T" + when.Hour + ":" + when.Minute + ":" + when.Second + "Z</when>");
        //    Response.Write("</TimeStamp>");
        //    Response.Write("<styleUrl>#icon_Vertice</styleUrl>");
        //    Response.Write("<Point>");
        //    Response.Write("<coordinates>" + v1[1] + "," + v1[0] + ",0</coordinates>");
        //    pointLines += v1[1] + "," + v1[0] + ",0 ";
        //    Response.Write("</Point>");
        //    Response.Write("</Placemark>");
        //    */
        //    Response.Write(v1[1] + "," + v1[0] + ",1784 ");
          
        //}
        //if (v2.Length == 2)
        //{
        //    //vertice2
        //   /* Response.Write("<Placemark>");
        //    Response.Write("<name>Vertice 2</name>");
        //    Response.Write("<description>" + v2[0] + " " + v2[1] + "</description>");
        //    Response.Write("<TimeStamp>");
        //    Response.Write("<when>" + (when.Year + 1) + "-" + when.Day + "-" + when.Month + "T" + when.Hour + ":" + when.Minute + ":" + when.Second + "Z</when>");
        //    Response.Write("</TimeStamp>");
        //    Response.Write("<styleUrl>#icon_Vertice</styleUrl>");
        //    Response.Write("<Point>");
        //    Response.Write("<coordinates>" + v2[1] + "," + v2[0] + ",0</coordinates>");
        //    pointLines += v2[1] + "," + v2[0] + ",0 ";
        //    Response.Write("</Point>");
        //    Response.Write("</Placemark>");*/
        //    Response.Write(v2[1] + "," + v2[0] + ",1784 ");
        //}
        //if (v3.Length == 2)
        //{
        //    //vertice3
        //    /*Response.Write("<Placemark>");
        //    Response.Write("<name>Vertice 3</name>");
        //    Response.Write("<description>" + v3[0] + " " + v3[1] + "</description>");
        //    Response.Write("<TimeStamp>");
        //    Response.Write("<when>" + (when.Year + 2) + "-" + when.Day + "-" + when.Month + "T" + when.Hour + ":" + when.Minute + ":" + when.Second + "Z</when>");
        //    Response.Write("</TimeStamp>");
        //    Response.Write("<styleUrl>#icon_Vertice</styleUrl>");
        //    Response.Write("<Point>");
        //    Response.Write("<coordinates>" + v3[1] + "," + v3[0] + ",0</coordinates>");
        //    pointLines += v3[1] + "," + v3[0] + ",0 ";
        //    Response.Write("</Point>");
        //    Response.Write("</Placemark>");*/
        //    Response.Write(v3[1] + "," + v3[0] + ",1784 ");
        //}
        //if (v4.Length == 2)
        //{
        //    //vertice4
        //    /*Response.Write("<Placemark>");
        //    Response.Write("<name>Vertice 4</name>");
        //    Response.Write("<description>" + v4[0] + " " + v4[1] + "</description>");
        //    Response.Write("<TimeStamp>");
        //    Response.Write("<when>" + (when.Year + 3) + "-" + when.Day + "-" + when.Month + "T" + when.Hour + ":" + when.Minute + ":" + when.Second + "Z</when>");
        //    Response.Write("</TimeStamp>");
        //    Response.Write("<styleUrl>#icon_Vertice</styleUrl>");
        //    Response.Write("<Point>");
        //    Response.Write("<coordinates>" + v4[1] + "," + v4[0] + ",0</coordinates>");
        //    pointLines += v4[1] + "," + v4[0] + ",0 ";
        //    Response.Write("</Point>");
        //    Response.Write("</Placemark>");

        //    //vertice1
        //    Response.Write("<Placemark>");
        //    Response.Write("<name>Vertice 1</name>");
        //    Response.Write("<description>" + v1[0] + " " + v1[1] + "</description>");
        //    Response.Write("<TimeStamp>");
        //    Response.Write("<when>" + (when.Year + 4) + "-" + when.Day + "-" + when.Month + "T" + when.Hour + ":" + when.Minute + ":" + when.Second + "Z</when>");
        //    Response.Write("</TimeStamp>");
        //    Response.Write("<styleUrl>#icon_Vertice</styleUrl>");
        //    Response.Write("<Point>");
        //    Response.Write("<coordinates>" + v1[1] + "," + v1[0] + ",0</coordinates>");
        //    pointLines += v1[1] + "," + v1[0] + ",0 ";
        //    Response.Write("</Point>");
        //    Response.Write("</Placemark>");*/
        //    Response.Write(v4[1] + "," + v4[0] + ",1784 ");
        //    Response.Write(v1[1] + "," + v1[0] + ",1784 ");
        //}
          Response.Write("</coordinates>");
            Response.Write("</LinearRing>");
            Response.Write("</outerBoundaryIs>");
            Response.Write("</Polygon>");
            Response.Write("</Placemark>");
    }
    
    private void appendStyleSquadre()
    {
        //string urlImg = System.Configuration.ConfigurationManager.AppSettings["urlImgKml"];
        //DBAccessAlba db = new DBAccessAlba();
        try
        {
         
           /* Response.Write("<Style id=\"icon_Vertice\">");
            Response.Write("<IconStyle>");
            Response.Write("<scale>1</scale>");
            Response.Write("<Icon>");
            Response.Write("<href></href>");
            Response.Write("</Icon>");
            Response.Write("</IconStyle>");
            Response.Write("</Style>");*/
         
             Response.Write("<Style id=\"transBluePoly\">");
 Response.Write("<LineStyle>");
 Response.Write("<color>ff00ff00</color>");
 Response.Write("<width>5</width>");
 Response.Write("</LineStyle>");
 Response.Write("<PolyStyle>");
 Response.Write("<color>77fffff0</color>");
 Response.Write("</PolyStyle>");
 Response.Write("</Style>");
        }
        catch (Exception ex)
        {
        }
        finally
        {
            //db.connClose();
        }

    }

}
