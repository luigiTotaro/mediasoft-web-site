﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using System.IO;

using System.Data;
using System.Data.SqlClient;

public partial class GPS: System.Web.UI.Page
{


    string oraDb;
    private SqlConnection oc;
    private String RES;

    private string checkNameParameter(string name)
    {
        if (name.Substring(0, 1) != "@")
        {
            return "@" + name;
        }
        else
        {
            return name;
        }
    }

    private string checkStoredNameParameter(string name)
    {
        //toglie il nome del package. Nel caso locale non serve.

        int idxPoint = name.IndexOf(".");
        if (idxPoint == -1)
        {
            return name;
        }
        else
        {
            return name.Substring(idxPoint + 1);
        }
    }



    private SqlDbType obtainType(string type)
    {

        SqlDbType typeToRet = SqlDbType.VarChar;
        switch (type)
        {
            case "Number":
                typeToRet = SqlDbType.Int;
                break;
            case "Cursor":
                //null lo ingnora in quanto non ce necessita di definire parametri di output per le connesioni sql server
                break;
            case "String":
                typeToRet = SqlDbType.VarChar;
                break;
            case "VarChar":
                typeToRet = SqlDbType.VarChar;
                break;
            case "Bool":
                typeToRet = SqlDbType.Bit;
                break;
            case "DateTime":
                typeToRet = SqlDbType.DateTime;
                break;
            case "float":
                typeToRet = SqlDbType.Float;
                break;
            case "money":
                typeToRet = SqlDbType.Money;
                break;
        }
        return typeToRet;
    }


    private string formatData(string data)
    {
        return "20" + data.Substring(4, 2) + "-" + data.Substring(2, 2) + "-" + data.Substring(0, 2);
    }

    private string formatOra(string ora)
    {
        return ora.Substring(0, 2) + ":" + ora.Substring(2, 2) + ":" + ora.Substring(4, 2);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        //Response.Write(Request.Params["dati"]+"");
        TextWriter sw = File.AppendText("D:/inetpub/portale_mediasoft/telemetria/DATI_SENSORE.HTML");
        sw.WriteLine(DateTime.Now + " - Dati: " + Request.Params["dati"] + " - MAC:" + Request.Params["MAC"] + "<BR/>");
        sw.Close();

            try
            {
                oraDb = ConfigurationSettings.AppSettings["LocalConnection"].ToString();
                oc = new System.Data.SqlClient.SqlConnection(oraDb);
                String RES;

                string macAdd = Request.Params["MAC"].ToString();
                string lat = Request.Params["dati"].ToString().Split(',')[0];
                string lon = Request.Params["dati"].ToString().Split(',')[1];
                
                //string dataOraGPS = formatData(Request.Params["dati"].ToString().Split(',')[2])+" "+formatOra(Request.Params["dati"].ToString().Split(',')[3]);
                string dataOraGPS = Request.Params["dati"].ToString().Split(',')[2] + " " + Request.Params["dati"].ToString().Split(',')[3];
                string altitudine = Request.Params["dati"].ToString().Split(',')[4];
                string kmh = Request.Params["dati"].ToString().Split(',')[5];
	            string motore = Request.Params["dati"].ToString().Split(',')[6];


                oc.Open();

                SqlCommand objCmd = new SqlCommand();
                objCmd.Connection = oc;
                objCmd.CommandTimeout = 120;

                objCmd.CommandText = checkStoredNameParameter("SP_insertLocalizzazioneNEW"); 

                objCmd.CommandType = CommandType.StoredProcedure;

                if (macAdd == null)
                {
                    objCmd.Parameters.Add(new SqlParameter(checkNameParameter("@macAdd"), obtainType("String"))).Value = "";// DBNull.Value;
                }
                else
                {
                    objCmd.Parameters.Add(new SqlParameter(checkNameParameter("@macAdd"), obtainType("String"))).Value = macAdd;
                }

                if (lat == null)
                {
                    objCmd.Parameters.Add(new SqlParameter(checkNameParameter("@lat"), obtainType("String"))).Value = "";// DBNull.Value;
                }
                else
                {
                    objCmd.Parameters.Add(new SqlParameter(checkNameParameter("@lat"), obtainType("String"))).Value = lat;
                }

                if (lon == null)
                {
                    objCmd.Parameters.Add(new SqlParameter(checkNameParameter("@lon"), obtainType("String"))).Value = "";// DBNull.Value;
                }
                else
                {
                    objCmd.Parameters.Add(new SqlParameter(checkNameParameter("@lon"), obtainType("String"))).Value = lon;
                }


                if (dataOraGPS == null)
                {
                    objCmd.Parameters.Add(new SqlParameter(checkNameParameter("@dataOraGPS"), obtainType("Datetime"))).Value = DBNull.Value;
                }
                else
                {
                    objCmd.Parameters.Add(new SqlParameter(checkNameParameter("@dataOraGPS"), obtainType("Datetime"))).Value = dataOraGPS;
                }

                if (altitudine == null)
                {
                    objCmd.Parameters.Add(new SqlParameter(checkNameParameter("@altitudine"), obtainType("Number"))).Value =  DBNull.Value;
                }
                else
                {
                    objCmd.Parameters.Add(new SqlParameter(checkNameParameter("@altitudine"), obtainType("Number"))).Value = altitudine;
                }
                if (kmh == null)
                {
                    objCmd.Parameters.Add(new SqlParameter(checkNameParameter("@kmh"), obtainType("float"))).Value =  DBNull.Value;
                }
                else
                {
                    objCmd.Parameters.Add(new SqlParameter(checkNameParameter("@kmh"), obtainType("float"))).Value = kmh;
                }
                if (motore == null)
                {
                    objCmd.Parameters.Add(new SqlParameter(checkNameParameter("@motore"), obtainType("Number"))).Value =  DBNull.Value;
                }
                else
                {
                    objCmd.Parameters.Add(new SqlParameter(checkNameParameter("@motore"), obtainType("Number"))).Value = motore;
                }

                SqlDataAdapter adapter = new SqlDataAdapter(objCmd);
                System.Data.DataSet dsReturn = new DataSet();
                adapter.Fill(dsReturn);
                RES = "OK";
                Response.Write(RES);
                oc.Close();
				
				//################################### INIZIO VERIFICA AREA DI COMPETENZA

                if ((macAdd != null) && (lat != null) && (lon != null))
                {

                    System.Net.WebRequest wr = System.Net.WebRequest.Create("http://62.149.205.246/telemetria/graphUtils.aspx?action=isInArea&mac="+macAdd+"&Lat="+lat+"&Lon="+lon);
                    wr.Timeout = 30000;

                    //try
                    //{
                        System.Net.HttpWebResponse response = (System.Net.HttpWebResponse)wr.GetResponse();
                    //}
                    //catch (Exception ex)
                    //{
                        //Response.Write(ex.Message.ToString() + " - " + ex.StackTrace.ToString());
                    //}
                }

                //################################### FINE VERIFICA AREA DI COMPETENZA
            }
            catch (Exception ex)
            {
                Response.Write(ex.Message.ToString() + " - " + ex.StackTrace.ToString());
            }










    }
}
