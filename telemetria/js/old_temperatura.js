/*

This file is part of Ext JS 4

Copyright (c) 2011 Sencha Inc

Contact:  http://www.sencha.com/contact

GNU General Public License Usage
This file may be used under the terms of the GNU General Public License version 3.0 as published by the Free Software Foundation and appearing in the file LICENSE included in the packaging of this file.  Please review the following information to ensure the GNU General Public License version 3.0 requirements will be met: http://www.gnu.org/copyleft/gpl.html.

If you are unsure which license is appropriate for your use, please contact the sales department at http://www.sencha.com/contact.

*/
Ext.require([
    'Ext.window.Window',
    'Ext.chart.*',
    'Ext.fx.target.Sprite', 'Ext.layout.container.Fit'
]);


var codice = 'rollog';
var velocita = -1;
var dataOdierna = new Date();
var dataInizio = dataOdierna.getFullYear() + '-' + (dataOdierna.getMonth() + 1) + '-' + dataOdierna.getDate() + ' 00:00:00';
var dataFine = dataOdierna.getFullYear() + '-' + (dataOdierna.getMonth() + 1) + '-' + dataOdierna.getDate() + ' 23:59:00';

function getUrlVars() {
    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function (m, key, value) {
        vars[key] = value;
    });
    return vars;
}

Ext.onReady(function () {
    var chart;

    Ext.regModel('clsTemp', {
        fields: [{ type: 'string', name: 'dataOra' },
                    { type: 'float', name: 'temperatura' },
                    { type: 'float', name: 'permettivita' },
                    { type: 'float', name: 'livello' },
                    { type: 'string', name: 'macAdd'}]
    });


    Ext.regModel('clsPerm', {
        fields: [{ type: 'string', name: 'name' },
                    { type: 'float', name: 'data'}]
    });


    var statoMotore = [
           { "iso2": "all", "code": -1, "name": "Tutti gli stati" },
           { "iso2": "on", "code": 1, "name": "Acceso" },
           { "iso2": "off", "code": 0, "name": "Spento" }
        ];


    Ext.define("clsMotore", { extend: "Ext.data.Model",
        fields: [
        { type: 'iso2', name: 'iso2' },
        { type: 'int', name: 'code' },
        { type: 'string', name: 'name' }
    ]
    });





    function searchDati() {
        storeTemp.proxy.extraParams.action = 'getGraphTemp';
        storeTemp.proxy.extraParams.dataD = Ext.ComponentMgr.get('edtData').getValue().getDate();
        storeTemp.proxy.extraParams.dataM = Ext.ComponentMgr.get('edtData').getValue().getMonth() + 1;
        storeTemp.proxy.extraParams.dataY = Ext.ComponentMgr.get('edtData').getValue().getFullYear();
        storeTemp.proxy.extraParams.motore = Ext.ComponentMgr.get('cbStatoMotore').getValue();
        if (getUrlVars()['mac'] != undefined) {
            storeTemp.proxy.extraParams.mac = getUrlVars()['mac'];
        }
        if (Ext.ComponentMgr.get('btnApplicaFiltro').pressed) {
            storeTemp.proxy.extraParams.kmh = Ext.ComponentMgr.get('edtKmh').getValue();
            velocita = Ext.ComponentMgr.get('edtKmh').getValue();
        } else {
            storeTemp.proxy.extraParams.kmh = -1;
            velocita = -1;
        }
        storeTemp.load();
    }


    // The data store
    var storeStatoMotore = Ext.create('Ext.data.Store', {
        model: 'clsMotore',
        data: statoMotore
    });


    // ComboBox with a custom item template
    var cbStatoMotore = Ext.create('Ext.form.field.ComboBox', {
        id: 'cbStatoMotore',
        displayField: 'name',
        hidden: false,
        fieldLabel: 'Motore',
        labelWidth: 50,
        valueField: 'code',
        value: -1,
        grow: true,
        store: storeStatoMotore,
        queryMode: 'local',
        editable: false,
        listeners: {
            'select': function () {
                searchDati();
            }
        }//,
        //        listConfig: {
        //            getInnerTpl: function () {
        //                var tpl = '<div>' +
        //                      '<img src="images/{iso2}.png" align="left">&nbsp;&nbsp;' +
        //                      '{name}</div>';
        //                return tpl;
        //            }
        //        }
    });



    var storeTemp = Ext.create('Ext.data.JsonStore', {
        autoLoad: false,
        autoSync: true,
        model: 'clsTemp',
        proxy: {
            type: 'ajax',
            url: 'graphUtils.aspx',
            timeout: 900000,
            reader: {
                type: 'json'
            },
            extraParams:
            {
                action: 'getGraphTemp',
                dataD: null,
                dataM: null,
                dataY: null
            }
        },
        listeners: {
            beforeload: function () {
            },
            load: function () {

                if (this.getTotalCount() > 0) {
                    Ext.ComponentMgr.get('chartTemp').show();
                    Ext.ComponentMgr.get('chartPerm').show();
                    Ext.ComponentMgr.get('chartTemp').redraw(true);
                    Ext.ComponentMgr.get('chartPerm').redraw(true);
                    Ext.ComponentMgr.get('pnlGraphs').enable();

                } else {
                    Ext.ComponentMgr.get('chartTemp').hide();
                    Ext.ComponentMgr.get('chartPerm').hide();

                    Ext.ComponentMgr.get('pnlGraphs').disable();
                    Ext.ComponentMgr.get('tabs').setActiveTab(1);
                }

                updateView();

            },
            exception: function (dp, type, action, options, response, arg) {
                if (console) {
                    console.log('storeTemp - errore: ' + response.responseText);
                }
            }
        }
    });



    var pieStore = Ext.create('Ext.data.JsonStore', {
        autoLoad: false,
        autoSync: true,
        model: 'clsPerm',
        proxy: {
            type: 'ajax',
            url: 'graphUtils.aspx',
            timeout: 900000,
            reader: {
                type: 'json'
            },
            extraParams:
            {
                action: 'getGraphPerm',
                perm: null
            }
        },
        listeners: {
            beforeload: function () {
            },
            load: function () {

            },
            exception: function (dp, type, action, options, response, arg) {
                if (console) {
                    console.log('pieStore - errore: ' + response.responseText);
                }
            }
        }
    });


    var pieChart = Ext.create('Ext.chart.Chart', {
        width: 200,
        height: 200,
        animate: false,
        store: pieStore,
        shadow: false,
        insetPadding: 0,
        theme: 'Base:gradients',
        series: [{
            type: 'pie',
            field: 'data',
            showInLegend: true,
            label: {
                field: 'name',
                style: 'font-weight:bold;',
                display: 'rotate',
                contrast: true,
                font: '9px Arial'
            }
        }, ]
    });








    Ext.create('Ext.Panel', {
        id: 'crtTemp',
        height: 300,
        title: 'Temperatura (&deg;C)',
        layout: 'fit',
        items: [{
            xtype: 'chart',
            id: 'chartTemp',
            width: 800,
            style: 'background:#fff',
            animate: true,
            store: storeTemp,
            shadow: true,
            theme: 'Category1',
            legend: {
                position: 'right'
            },
            axes: [{
                type: 'Numeric',
//                minimum: 0,
//                maximum:80,
//                minimum: 50,
//                maximum: 150,
                position: 'left',
                fields: ['temperatura'],
                title: 'Temperatura',
                minorTickSteps: 10,
                grid: {
                    odd: {
                        opacity: 1,
                        fill: '#ddd',
                        stroke: '#bbb',
                        'stroke-width': 0.5
                    }
                }
            }, {
                type: 'Category',
                position: 'bottom',
                fields: ['dataOra'],
                title: 'Orario'
            }],
            series: [{
                type: 'line',
                highlight: {
                    size: 7,
                    radius: 7
                },
                smooth: true,
                fill: true,
                axis: 'left',
                xField: 'dataOra',
                yField: 'temperatura',
                markerConfig: {
                    type: 'circle',
                    size: 1,
                    radius: 1,
                    'stroke-width': 0
                }
            }]
        }]
    });



    Ext.create('Ext.Panel', {
        id: 'crtPerm',
        height: 300,
        title: 'Costante Dielettrica',
        layout: 'fit',
        items: [{
            xtype: 'chart',
            id: 'chartPerm',
            width: 800,
            style: 'background:#fff',
            animate: true,
            store: storeTemp,
            shadow: true,
            theme: 'Category2',
            legend: {
                position: 'right'
            },
            axes: [{
                type: 'Numeric',
//                minimum: 2.34,
//                maximum: 2.40,
                position: 'left',
                fields: ['permettivita'],
                title: 'Costante Dielettrica',
                minorTickSteps: 10,
                grid: {
                    odd: {
                        opacity: 1,
                        fill: '#ddd',
                        stroke: '#bbb',
                        'stroke-width': 0.5
                    }
                }
            }, {
                type: 'Category',
                position: 'bottom',
                fields: ['dataOra'],
                title: 'Orario'
            }],
            series: [{
                type: 'line',
                highlight: {
                    size: 7,
                    radius: 7
                },
                smooth: true,
                fill: true,
                axis: 'left',
                xField: 'dataOra',
                yField: 'permettivita',
                markerConfig: {
                    type: 'circle',
                    size: 2,
                    radius: 2,
                    'stroke-width': 0
                },

                tips: {
                    trackMouse: true,
                    width: 220,
                    height: 220,
                    layout: 'fit',
                    items: {
                        xtype: 'container',
                        layout: 'hbox',
                        items: [pieChart]
                    },
                    renderer: function (klass, item) {
                        pieStore.proxy.extraParams.action = 'getGraphPerm';
                        pieStore.proxy.extraParams.perm = item.value[1];
                        pieStore.load();
                    }
                }
            }]
        }]
    });


    Ext.create('Ext.Panel', {
        id: 'pnlPercorsi',
        title: 'Percorsi',
        height: 620,
        items: [{
            html: '<br/>&nbsp;&nbsp;&nbsp;Percorso effettuato in giornata:<br/><br/><div id="map" class="map" style="width: 100%; height: 540px; margin-top:1px;"></div>'
        }]

    });



    var pnlGraphs = Ext.create('Ext.Panel', {
        id: 'pnlGraphs',
        title: 'Grafici',
        layout: 'anchor',
        items: [Ext.ComponentMgr.get('crtTemp'), Ext.ComponentMgr.get('crtPerm')]
    });

    // Ext.create('Ext.Panel', {
    var tabs = Ext.createWidget('tabpanel', {
        id: 'tabs',
        activeTab: 0,
        defaults: {
            bodyPadding: 10
        },
        //renderTo: 'pnlTemp',
        layout: 'fit',
        items: [Ext.ComponentMgr.get('pnlGraphs'), Ext.ComponentMgr.get('pnlPercorsi')],
        listeners: {
            'tabchange': function () {
                storeTemp.load();
                if (this.getActiveTab().id != 'pnlPercorsi') {
                    Ext.ComponentMgr.get('edtKmh').disable();
                    Ext.ComponentMgr.get('btnSearchKmh').disable();
                    Ext.ComponentMgr.get('btnApplicaFiltro').disable();
                    Ext.ComponentMgr.get('cbStatoMotore').disable();
                    Ext.ComponentMgr.get('edtData').show();
                    Ext.ComponentMgr.get('btnDaData').hide();
                    Ext.ComponentMgr.get('btnAData').hide();
                } else {
                    Ext.ComponentMgr.get('btnApplicaFiltro').enable();
                    Ext.ComponentMgr.get('cbStatoMotore').enable();
                    Ext.ComponentMgr.get('edtData').hide();
                    Ext.ComponentMgr.get('btnDaData').show();
                    Ext.ComponentMgr.get('btnAData').show();
                    if (Ext.ComponentMgr.get('btnApplicaFiltro').pressed) {
                        Ext.ComponentMgr.get('edtKmh').enable();
                        Ext.ComponentMgr.get('btnSearchKmh').enable();
                        searchDati();
                    } else {
                        Ext.ComponentMgr.get('edtKmh').disable();
                        Ext.ComponentMgr.get('btnSearchKmh').disable();
                        searchDati();
                    }
                }
            }
        }

    });


    var menuDataDaFilter = Ext.create('Ext.menu.Menu', {
        id: 'menuDataDaFilter',
        showSeparator: false,
        style: {
            overflow: 'visible'     // For the Combo popup
        },
        items: [{
            xtype: 'datepicker',
            id: 'edtDaData',
            //fieldLabel: 'Da Data',
            format: 'd/m/Y',
            maxValue: new Date(),
            value: new Date(),
            //labelWidth: 50,
            listeners: {
                'select': function () {
                    Ext.ComponentMgr.get('btnDaData').setText('Dal:' + this.getValue().getDate() + '/' + (this.getValue().getMonth() + 1) + '/' + this.getValue().getFullYear() + ' ' + Ext.ComponentMgr.get('edtDaOre').getValue().toString() + ':' + Ext.ComponentMgr.get('edtDaMinuti').getValue().toString());
                    dataInizio = this.getValue().getFullYear() + '-' + (this.getValue().getMonth() + 1) + '-' + this.getValue().getDate() + ' ' + Ext.ComponentMgr.get('edtDaOre').getValue().toString() + ':' + Ext.ComponentMgr.get('edtDaMinuti').getValue().toString() + ':00';
                    Ext.ComponentMgr.get('edtAData').setMinDate(this.getValue());
                    Ext.ComponentMgr.get('edtAData').setValue(this.getValue());

                    Ext.ComponentMgr.get('btnAData').setText('Al:' + this.getValue().getDate() + '/' + (this.getValue().getMonth() + 1) + '/' + this.getValue().getFullYear() + ' ' + Ext.ComponentMgr.get('edtDaOre').getValue().toString() + ':' + Ext.ComponentMgr.get('edtDaMinuti').getValue().toString());
                    dataFine = this.getValue().getFullYear() + '-' + (this.getValue().getMonth() + 1) + '-' + this.getValue().getDate() + ' ' + Ext.ComponentMgr.get('edtDaOre').getValue().toString() + ':' + Ext.ComponentMgr.get('edtDaMinuti').getValue().toString() + ':00'
                }
            }
        }, {
            xtype: 'fieldcontainer',
            layout: 'hbox',
            icon: 'images/speed.png',
            fieldLabel: 'Da Ore',
            labelWidth: 50,
            items: [{
                xtype: 'numberfield',
                id: 'edtDaOre',
                width: 40,
                minValue: 00,
                maxValue: 23,
                value: 00,
                allowDecimals: false,
                hideTrigger: true,
                listeners: {
                    'render': function (c) {
                        c.getEl().on('keyup', function () {
                            Ext.ComponentMgr.get('btnDaData').setText('Dal:' + Ext.ComponentMgr.get('edtDaData').getValue().getDate() + '/' + (Ext.ComponentMgr.get('edtDaData').getValue().getMonth() + 1) + '/' + Ext.ComponentMgr.get('edtDaData').getValue().getFullYear() + ' ' + this.getValue().toString() + ':' + Ext.ComponentMgr.get('edtDaMinuti').getValue().toString());
                            dataInizio = Ext.ComponentMgr.get('edtDaData').getValue().getFullYear() + '-' + (Ext.ComponentMgr.get('edtDaData').getValue().getMonth() + 1) + '-' + Ext.ComponentMgr.get('edtDaData').getValue().getDate() + ' ' + this.getValue().toString() + ':' + Ext.ComponentMgr.get('edtDaMinuti').getValue().toString() + ':00';
                            Ext.ComponentMgr.get('edtAOre').setMinValue(this.getValue());
                            Ext.ComponentMgr.get('edtAOre').setValue(this.getValue());

                            Ext.ComponentMgr.get('btnAData').setText('Al:' + Ext.ComponentMgr.get('edtDaData').getValue().getDate() + '/' + (Ext.ComponentMgr.get('edtDaData').getValue().getMonth() + 1) + '/' + Ext.ComponentMgr.get('edtDaData').getValue().getFullYear() + ' ' + this.getValue().toString() + ':' + Ext.ComponentMgr.get('edtDaMinuti').getValue().toString());
                            dataFine = Ext.ComponentMgr.get('edtDaData').getValue().getFullYear() + '-' + (Ext.ComponentMgr.get('edtDaData').getValue().getMonth() + 1) + '-' + Ext.ComponentMgr.get('edtDaData').getValue().getDate() + ' ' + this.getValue().toString() + ':' + Ext.ComponentMgr.get('edtDaMinuti').getValue().toString() + ':00';
                        }, c);
                    }
                }
            }, {
                xtype: 'label',
                text: ' : '
            }, {
                xtype: 'numberfield',
                id: 'edtDaMinuti',
                width: 40,
                minValue: 00,
                maxValue: 59,
                value: 00,
                allowDecimals: false,
                hideTrigger: true,
                listeners: {
                    'render': function (c) {
                        c.getEl().on('keyup', function () {
                            Ext.ComponentMgr.get('btnDaData').setText('Dal:' + Ext.ComponentMgr.get('edtDaData').getValue().getDate() + '/' + (Ext.ComponentMgr.get('edtDaData').getValue().getMonth() + 1) + '/' + Ext.ComponentMgr.get('edtDaData').getValue().getFullYear() + ' ' + Ext.ComponentMgr.get('edtDaOre').getValue().toString() + ':' + this.getValue().toString());
                            dataInizio = Ext.ComponentMgr.get('edtDaData').getValue().getFullYear() + '-' + (Ext.ComponentMgr.get('edtDaData').getValue().getMonth() + 1) + '-' + Ext.ComponentMgr.get('edtDaData').getValue().getDate() + ' ' + Ext.ComponentMgr.get('edtDaOre').getValue().toString() + ':' + this.getValue().toString() + ':00';
                            Ext.ComponentMgr.get('edtAMinuti').setMinValue(this.getValue());
                            Ext.ComponentMgr.get('edtAMinuti').setValue(this.getValue());

                            Ext.ComponentMgr.get('btnAData').setText('Al:' + Ext.ComponentMgr.get('edtDaData').getValue().getDate() + '/' + (Ext.ComponentMgr.get('edtDaData').getValue().getMonth() + 1) + '/' + Ext.ComponentMgr.get('edtDaData').getValue().getFullYear() + ' ' + Ext.ComponentMgr.get('edtDaOre').getValue().toString() + ':' + this.getValue().toString());
                            dataFine = Ext.ComponentMgr.get('edtDaData').getValue().getFullYear() + '-' + (Ext.ComponentMgr.get('edtDaData').getValue().getMonth() + 1) + '-' + Ext.ComponentMgr.get('edtDaData').getValue().getDate() + ' ' + Ext.ComponentMgr.get('edtDaOre').getValue().toString() + ':' + this.getValue().toString() + ':00';
                        }, c);
                    }
                }
            }, {
                xtype: 'button',
                id: 'btnSearchDaOre',
                iconAlign: 'left',
                icon: 'images/search.png',
                handler: function () {
                    Ext.ComponentMgr.get('btnDaData').setText('Dal:' + Ext.ComponentMgr.get('edtDaData').getValue().getDate() + '/' + (Ext.ComponentMgr.get('edtDaData').getValue().getMonth() + 1) + '/' + Ext.ComponentMgr.get('edtDaData').getValue().getFullYear() + ' ' + Ext.ComponentMgr.get('edtDaOre').getValue().toString() + ':' + Ext.ComponentMgr.get('edtDaMinuti').getValue().toString());
                    dataInizio = Ext.ComponentMgr.get('edtDaData').getValue().getFullYear() + '-' + (Ext.ComponentMgr.get('edtDaData').getValue().getMonth() + 1) + '-' + Ext.ComponentMgr.get('edtDaData').getValue().getDate() + ' ' + Ext.ComponentMgr.get('edtDaOre').getValue().toString() + ':' + Ext.ComponentMgr.get('edtDaMinuti').getValue().toString() + ':00'
                    searchDati();
                }
            }]
        }]
    });


    var menuDataAFilter = Ext.create('Ext.menu.Menu', {
        id: 'menuDataAFilter',
        showSeparator: false,
        style: {
            overflow: 'visible'     // For the Combo popup
        },
        items: [{
            xtype: 'datepicker',
            id: 'edtAData',
            //fieldLabel: 'Da Data',
            format: 'd/m/Y',
            maxValue: new Date(),
            value: new Date(),
            //labelWidth: 50,
            listeners: {
                'select': function () {
                    Ext.ComponentMgr.get('btnAData').setText('Al:' + this.getValue().getDate() + '/' + (this.getValue().getMonth() + 1) + '/' + this.getValue().getFullYear() + ' ' + Ext.ComponentMgr.get('edtAOre').getValue().toString() + ':' + Ext.ComponentMgr.get('edtAMinuti').getValue().toString());
                    dataFine = this.getValue().getFullYear() + '-' + (this.getValue().getMonth() + 1) + '-' + this.getValue().getDate() + ' ' + Ext.ComponentMgr.get('edtAOre').getValue().toString() + ':' + Ext.ComponentMgr.get('edtAMinuti').getValue().toString() + ':00'
                }
            }
        }, {
            xtype: 'fieldcontainer',
            layout: 'hbox',
            icon: 'images/speed.png',
            fieldLabel: 'A Ore',
            labelWidth: 50,
            items: [{
                xtype: 'numberfield',
                id: 'edtAOre',
                width: 40,
                minValue: 00,
                maxValue: 23,
                value: 00,
                allowDecimals: false,
                hideTrigger: true,
                listeners: {
                    'render': function (c) {
                        c.getEl().on('keyup', function () {
                            Ext.ComponentMgr.get('btnAData').setText('Al:' + Ext.ComponentMgr.get('edtAData').getValue().getDate() + '/' + (Ext.ComponentMgr.get('edtAData').getValue().getMonth() + 1) + '/' + Ext.ComponentMgr.get('edtAData').getValue().getFullYear() + ' ' + this.getValue().toString() + ':' + Ext.ComponentMgr.get('edtAMinuti').getValue().toString());
                            dataFine = Ext.ComponentMgr.get('edtAData').getValue().getFullYear() + '-' + (Ext.ComponentMgr.get('edtAData').getValue().getMonth() + 1) + '-' + Ext.ComponentMgr.get('edtAData').getValue().getDate() + ' ' + this.getValue().toString() + ':' + Ext.ComponentMgr.get('edtAMinuti').getValue().toString() + ':00'
                        }, c);
                    }
                }
            }, {
                xtype: 'label',
                text: ' : '
            }, {
                xtype: 'numberfield',
                id: 'edtAMinuti',
                width: 40,
                minValue: 00,
                maxValue: 59,
                value: 00,
                allowDecimals: false,
                hideTrigger: true,
                listeners: {
                    'render': function (c) {
                        c.getEl().on('keyup', function () {
                            Ext.ComponentMgr.get('btnAData').setText('Al:' + Ext.ComponentMgr.get('edtAData').getValue().getDate() + '/' + (Ext.ComponentMgr.get('edtAData').getValue().getMonth() + 1) + '/' + Ext.ComponentMgr.get('edtAData').getValue().getFullYear() + ' ' + Ext.ComponentMgr.get('edtAOre').getValue().toString() + ':' + this.getValue().toString());
                            dataFine = Ext.ComponentMgr.get('edtAData').getValue().getFullYear() + '-' + (Ext.ComponentMgr.get('edtAData').getValue().getMonth() + 1) + '-' + Ext.ComponentMgr.get('edtAData').getValue().getDate() + ' ' + Ext.ComponentMgr.get('edtAOre').getValue().toString() + ':' + this.getValue().toString() + ':00'
                        }, c);
                    }
                }
            }, {
                xtype: 'button',
                id: 'btnSearchAOre',
                iconAlign: 'left',
                icon: 'images/search.png',
                handler: function () {
                    Ext.ComponentMgr.get('btnAData').setText('Al:' + Ext.ComponentMgr.get('edtAData').getValue().getDate() + '/' + (Ext.ComponentMgr.get('edtAData').getValue().getMonth() + 1) + '/' + Ext.ComponentMgr.get('edtAData').getValue().getFullYear() + ' ' + Ext.ComponentMgr.get('edtAOre').getValue().toString() + ':' + Ext.ComponentMgr.get('edtAMinuti').getValue().toString());
                    dataFine = Ext.ComponentMgr.get('edtAData').getValue().getFullYear() + '-' + (Ext.ComponentMgr.get('edtAData').getValue().getMonth() + 1) + '-' + Ext.ComponentMgr.get('edtAData').getValue().getDate() + ' ' + Ext.ComponentMgr.get('edtAOre').getValue().toString() + ':' + Ext.ComponentMgr.get('edtAMinuti').getValue().toString() + ':00'
                    searchDati();
                }
            }]
        }]
    });




    Ext.create('Ext.Viewport', {
        id: 'vpMain',
        layout: 'border',
        items: [{
            collapsible: false,
            id: 'pnlCenter',
            region: 'center',
            layout: 'fit',
            items: [Ext.ComponentMgr.get('tabs')],
            tbar: Ext.create('Ext.toolbar.Toolbar', {
                defaults: {
                    iconAlign: 'left'
                },
                id: 'tbar_Temp',
                items: [{
                    id: 'btnDaData',
                    text: 'Dal: ' + dataOdierna.getDate() + '/' + (dataOdierna.getMonth() + 1) + '/' + dataOdierna.getFullYear() + ' 00:00',
                    icon: 'images/calendar.png',  // <-- icon
                    menu: menuDataDaFilter  // assign menu by instance
                }, {
                    id: 'btnAData',
                    text: 'Al: ' + dataOdierna.getDate() + '/' + (dataOdierna.getMonth() + 1) + '/' + dataOdierna.getFullYear() + ' 23:59',
                    icon: 'images/calendar.png',  // <-- icon
                    menu: menuDataAFilter  // assign menu by instance
                }, {
                    xtype: 'datefield',
                    id: 'edtData',
                    format: 'd/m/Y',
                    hidden: true,
                    maxValue: new Date(),
                    value: new Date(),
                    listeners: {
                        'select': function () {
                            searchDati();
                        }
                    }
                }, Ext.ComponentMgr.get('cbStatoMotore'), '-',
                    {
                        xtype: 'numberfield',
                        fieldLabel: 'Km/h (>=)',
                        width: 150,
                        labelWidth: 70,
                        id: 'edtKmh',
                        minValue: 0,
                        value: 0,
                        disabled: true,
                        allowDecimals: false
                    }, {
                        xtype: 'button',
                        id: 'btnSearchKmh',
                        iconAlign: 'left',
                        disabled: true,
                        icon: 'images/search.png',
                        handler: function () {
                            searchDati();
                        }
                    }, '-', {
                        xtype: 'button',
                        id: 'btnApplicaFiltro',
                        text: 'Applica Filtro per Km/h',
                        iconAlign: 'left',
                        icon: 'images/speed.png',
                        enableToggle: true,
                        handler: function () {
                            if (this.pressed) {
                                Ext.ComponentMgr.get('edtKmh').enable();
                                Ext.ComponentMgr.get('btnSearchKmh').enable();
                                searchDati();
                            } else {
                                Ext.ComponentMgr.get('edtKmh').disable();
                                Ext.ComponentMgr.get('btnSearchKmh').disable();
                                searchDati();
                            }
                        }
                    }, '->', {
                        xtype: 'button',
                        icon: './reload16.png',
                        text: 'Aggiorna i grafici',
                        handler: function () {
                            storeTemp.load();
                        }
                    }]
            })
        }]

    });

    storeTemp.proxy.extraParams.action = 'getGraphTemp';
    storeTemp.proxy.extraParams.dataD = new Date().getDate();
    storeTemp.proxy.extraParams.dataM = new Date().getMonth() + 1;
    storeTemp.proxy.extraParams.dataY = new Date().getFullYear();
    storeTemp.proxy.extraParams.motore = null;
    if (getUrlVars()['mac'] != undefined) {
        storeTemp.proxy.extraParams.mac = getUrlVars()['mac'];
    }
    storeTemp.load();


    if (Ext.ComponentMgr.get('tabs').getActiveTab().id != 'pnlPercorsi') {
        Ext.ComponentMgr.get('edtKmh').disable();
        Ext.ComponentMgr.get('btnSearchKmh').disable();
        Ext.ComponentMgr.get('btnApplicaFiltro').disable();
        Ext.ComponentMgr.get('cbStatoMotore').disable();
        Ext.ComponentMgr.get('edtData').show();
        Ext.ComponentMgr.get('btnDaData').hide();
        Ext.ComponentMgr.get('btnAData').hide();

    } else {
        Ext.ComponentMgr.get('btnApplicaFiltro').enable();
        Ext.ComponentMgr.get('cbStatoMotore').enable();
        Ext.ComponentMgr.get('edtData').hide();
        Ext.ComponentMgr.get('btnDaData').show();
        Ext.ComponentMgr.get('btnAData').show();
        if (Ext.ComponentMgr.get('btnApplicaFiltro').pressed) {
            Ext.ComponentMgr.get('edtKmh').enable();
            Ext.ComponentMgr.get('btnSearchKmh').enable();
            searchDati();
        } else {
            Ext.ComponentMgr.get('edtKmh').disable();
            Ext.ComponentMgr.get('btnSearchKmh').disable();
            searchDati();
        }
    }

});

