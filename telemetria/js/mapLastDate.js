/*

This file is part of Ext JS 4

Copyright (c) 2011 Sencha Inc

Contact:  http://www.sencha.com/contact

GNU General Public License Usage
This file may be used under the terms of the GNU General Public License version 3.0 as published by the Free Software Foundation and appearing in the file LICENSE included in the packaging of this file.  Please review the following information to ensure the GNU General Public License version 3.0 requirements will be met: http://www.gnu.org/copyleft/gpl.html.

If you are unsure which license is appropriate for your use, please contact the sales department at http://www.sencha.com/contact.

*/
Ext.require([
    'Ext.window.Window',
    'Ext.chart.*',
    'Ext.fx.target.Sprite', 'Ext.layout.container.Fit'
]);


var codice = 'rollog';
var velocita = -1;
var dataOdierna = new Date();
var dataInizio = dataOdierna.getFullYear() + '-' + (dataOdierna.getMonth() + 1) + '-' + dataOdierna.getDate() + ' 00:00:00';
var dataFine = dataOdierna.getFullYear() + '-' + (dataOdierna.getMonth() + 1) + '-' + dataOdierna.getDate() + ' 23:59:00';

function getUrlVars() {
    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function (m, key, value) {
        vars[key] = value;
    });
    return vars;
}

Ext.onReady(function () {

    Ext.create('Ext.Panel', {
        id: 'pnlPercorsi',
        title: '',
        height: 620,
        items: [{
            html: '<div id="map" class="map" style="width: 100%; height:310px; margin-top:1px;"></div>'
        }]

    });


    Ext.create('Ext.Viewport', {
        id: 'vpMain',
        layout: 'border',
        items: [{
            collapsible: false,
            id: 'pnlCenter',
            region: 'center',
            layout: 'fit',
            items: [Ext.ComponentMgr.get('pnlPercorsi')],
            
        }]

    });


    updateView();

});

