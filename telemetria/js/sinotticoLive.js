/*

This file is part of Ext JS 4

Copyright (c) 2011 Sencha Inc

Contact:  http://www.sencha.com/contact

GNU General Public License Usage
This file may be used under the terms of the GNU General Public License version 3.0 as published by the Free Software Foundation and appearing in the file LICENSE included in the packaging of this file.  Please review the following information to ensure the GNU General Public License version 3.0 requirements will be met: http://www.gnu.org/copyleft/gpl.html.

If you are unsure which license is appropriate for your use, please contact the sales department at http://www.sencha.com/contact.

*/
Ext.require([
    'Ext.window.Window',
    'Ext.chart.*',
    'Ext.fx.target.Sprite', 'Ext.layout.container.Fit'
]);


var codice = 'rollog';
var velocita = -1;
var dataOdierna = new Date(); //new Date(2012, 0, 26, 00, 00, 00, 000);  //
var dataInizio = dataOdierna.getFullYear() + '-' + (dataOdierna.getMonth() + 1) + '-' + dataOdierna.getDate() + ' 00:00:00';
var dataFine = dataOdierna.getFullYear() + '-' + (dataOdierna.getMonth() + 1) + '-' + dataOdierna.getDate() + ' 23:59:00';

var urlStore_PackageMainDash = 'http://smoconline.unionkey.com/PackageNewLookDash/PackageMain/PackageMainUtils.aspx';
//var urlStore_PackageMainDash = 'http://localhost:52739/PackageNewLookDash/PackageMain/PackageMainUtils.aspx';

function valueGET(name) {
    name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    var regexS = "[\\?&]" + name + "=([^&#]*)";
    var regex = new RegExp(regexS);
    var results = regex.exec(window.location.href);
    if (results == null)
        return "";
    else
        return results[1];
}

Ext.onReady(function () {
    var chart;

    var statoMotore = [
           { "iso2": "all", "code": -1, "name": "Tutti gli stati" },
           { "iso2": "on", "code": 1, "name": "Acceso" },
           { "iso2": "off", "code": 0, "name": "Spento" }
        ];


    Ext.define("clsMotore", { extend: "Ext.data.Model",
        fields: [
        { type: 'iso2', name: 'iso2' },
        { type: 'int', name: 'code' },
        { type: 'string', name: 'name' }
    ]
    });

    Ext.regModel('clsVettore', {
        fields: [{ type: 'int', name: 'codVettore' },
        { type: 'string', name: 'descrValuta' },
        { type: 'string', name: 'mac' },
        { type: 'string', name: 'targa'}]
    });


    var storeCbVettore = Ext.create('Ext.data.JsonStore', {
        id: 'storeCbVettore',
        autoLoad: true,
        autoSync: true,
        model: 'clsVettore',
        proxy: {
            // type: 'ajax',
            type: 'jsonp',
            url: urlStore_PackageMainDash,
            timeout: 900000,
            reader: {
                type: 'json'
            },
            extraParams:
            {
                action: 'getVettoriMac',
                codCliente: valueGET('cdCl'),
                codSettore: valueGET('cdSett'),
                codSottoSettore: valueGET('cdSottoSett')
            }
        },
        listeners: {
            beforeload: function () {
            },
            load: function () {

            },
            exception: function (dp, type, action, options, response, arg) {
                if (console) {
                    console.log('storeCbVettore - errore: ' + response.responseText);
                }
            }
        }
    });


    var cbVettore = Ext.create('Ext.form.field.ComboBox', {
        id: 'cbVettore',
        emptyText: 'Vettore',
        displayField: 'targa',
        valueField: 'mac',
        width: 200,
        listWidth: 400,
        store: storeCbVettore,
        queryMode: 'local',
        listeners: {
            'select': function () {
                //console.log(this.findRecordByValue(this.getValue()));
                updateView();
            }
        },
        typeAhead: true,
        listConfig: {
            getInnerTpl: function () {
                var tpl = '<div><span style="color : #000000;font-weight:bold;" >{targa}</span>';
                tpl += '<br><span style="color : #999999;" >{descrValuta}</span></div>';
                return tpl;
            }
        }
    });


    // The data store
    var storeStatoMotore = Ext.create('Ext.data.Store', {
        model: 'clsMotore',
        data: statoMotore
    });


    // ComboBox with a custom item template
    var cbStatoMotore = Ext.create('Ext.form.field.ComboBox', {
        id: 'cbStatoMotore',
        displayField: 'name',
        hidden: false,
        fieldLabel: 'Motore',
        labelWidth: 50,
        valueField: 'code',
        value: -1,
        grow: true,
        store: storeStatoMotore,
        queryMode: 'local',
        editable: false,
        listeners: {
            'select': function () {
                updateView();
            }
        }
    });



    Ext.create('Ext.Panel', {
        id: 'pnlPercorsi',
        title: '',
        height: 620,
        items: [{
            html: '<br/>&nbsp;&nbsp;&nbsp;Percorso effettuato in giornata:<br/><br/><div id="map" class="map" style="width: 100%; height: 540px; margin-top:1px;"></div>'
        }]

    });


    var menuDataDaFilter = Ext.create('Ext.menu.Menu', {
        id: 'menuDataDaFilter',
        showSeparator: false,
        style: {
            overflow: 'visible'     // For the Combo popup
        },
        items: [{
            xtype: 'datepicker',
            id: 'edtDaData',
            //fieldLabel: 'Da Data',
            format: 'd/m/Y',
            maxValue: new Date(),
            value: new Date(),
            //labelWidth: 50,
            listeners: {
                'select': function () {
                    Ext.ComponentMgr.get('btnDaData').setText('Dal:' + this.getValue().getDate() + '/' + (this.getValue().getMonth() + 1) + '/' + this.getValue().getFullYear() + ' ' + Ext.ComponentMgr.get('edtDaOre').getValue().toString() + ':' + Ext.ComponentMgr.get('edtDaMinuti').getValue().toString());
                    dataInizio = this.getValue().getFullYear() + '-' + (this.getValue().getMonth() + 1) + '-' + this.getValue().getDate() + ' ' + Ext.ComponentMgr.get('edtDaOre').getValue().toString() + ':' + Ext.ComponentMgr.get('edtDaMinuti').getValue().toString() + ':00';
                    Ext.ComponentMgr.get('edtAData').setMinDate(this.getValue());
                    Ext.ComponentMgr.get('edtAData').setValue(this.getValue());

                    Ext.ComponentMgr.get('btnAData').setText('Al:' + this.getValue().getDate() + '/' + (this.getValue().getMonth() + 1) + '/' + this.getValue().getFullYear() + ' ' + Ext.ComponentMgr.get('edtDaOre').getValue().toString() + ':' + Ext.ComponentMgr.get('edtDaMinuti').getValue().toString());
                    dataFine = this.getValue().getFullYear() + '-' + (this.getValue().getMonth() + 1) + '-' + this.getValue().getDate() + ' ' + Ext.ComponentMgr.get('edtDaOre').getValue().toString() + ':' + Ext.ComponentMgr.get('edtDaMinuti').getValue().toString() + ':00'
                }
            }
        }, {
            xtype: 'fieldcontainer',
            layout: 'hbox',
            icon: 'images/speed.png',
            fieldLabel: 'Da Ore',
            labelWidth: 50,
            items: [{
                xtype: 'numberfield',
                id: 'edtDaOre',
                width: 40,
                minValue: 00,
                maxValue: 23,
                value: 00,
                allowDecimals: false,
                hideTrigger: true,
                listeners: {
                    'render': function (c) {
                        c.getEl().on('keyup', function () {
                            Ext.ComponentMgr.get('btnDaData').setText('Dal:' + Ext.ComponentMgr.get('edtDaData').getValue().getDate() + '/' + (Ext.ComponentMgr.get('edtDaData').getValue().getMonth() + 1) + '/' + Ext.ComponentMgr.get('edtDaData').getValue().getFullYear() + ' ' + this.getValue().toString() + ':' + Ext.ComponentMgr.get('edtDaMinuti').getValue().toString());
                            dataInizio = Ext.ComponentMgr.get('edtDaData').getValue().getFullYear() + '-' + (Ext.ComponentMgr.get('edtDaData').getValue().getMonth() + 1) + '-' + Ext.ComponentMgr.get('edtDaData').getValue().getDate() + ' ' + this.getValue().toString() + ':' + Ext.ComponentMgr.get('edtDaMinuti').getValue().toString() + ':00';
                            Ext.ComponentMgr.get('edtAOre').setMinValue(this.getValue());
                            Ext.ComponentMgr.get('edtAOre').setValue(this.getValue());

                            Ext.ComponentMgr.get('btnAData').setText('Al:' + Ext.ComponentMgr.get('edtDaData').getValue().getDate() + '/' + (Ext.ComponentMgr.get('edtDaData').getValue().getMonth() + 1) + '/' + Ext.ComponentMgr.get('edtDaData').getValue().getFullYear() + ' ' + this.getValue().toString() + ':' + Ext.ComponentMgr.get('edtDaMinuti').getValue().toString());
                            dataFine = Ext.ComponentMgr.get('edtDaData').getValue().getFullYear() + '-' + (Ext.ComponentMgr.get('edtDaData').getValue().getMonth() + 1) + '-' + Ext.ComponentMgr.get('edtDaData').getValue().getDate() + ' ' + this.getValue().toString() + ':' + Ext.ComponentMgr.get('edtDaMinuti').getValue().toString() + ':00';
                        }, c);
                    }
                }
            }, {
                xtype: 'label',
                text: ' : '
            }, {
                xtype: 'numberfield',
                id: 'edtDaMinuti',
                width: 40,
                minValue: 00,
                maxValue: 59,
                value: 00,
                allowDecimals: false,
                hideTrigger: true,
                listeners: {
                    'render': function (c) {
                        c.getEl().on('keyup', function () {
                            Ext.ComponentMgr.get('btnDaData').setText('Dal:' + Ext.ComponentMgr.get('edtDaData').getValue().getDate() + '/' + (Ext.ComponentMgr.get('edtDaData').getValue().getMonth() + 1) + '/' + Ext.ComponentMgr.get('edtDaData').getValue().getFullYear() + ' ' + Ext.ComponentMgr.get('edtDaOre').getValue().toString() + ':' + this.getValue().toString());
                            dataInizio = Ext.ComponentMgr.get('edtDaData').getValue().getFullYear() + '-' + (Ext.ComponentMgr.get('edtDaData').getValue().getMonth() + 1) + '-' + Ext.ComponentMgr.get('edtDaData').getValue().getDate() + ' ' + Ext.ComponentMgr.get('edtDaOre').getValue().toString() + ':' + this.getValue().toString() + ':00';
                            Ext.ComponentMgr.get('edtAMinuti').setMinValue(this.getValue());
                            Ext.ComponentMgr.get('edtAMinuti').setValue(this.getValue());

                            Ext.ComponentMgr.get('btnAData').setText('Al:' + Ext.ComponentMgr.get('edtDaData').getValue().getDate() + '/' + (Ext.ComponentMgr.get('edtDaData').getValue().getMonth() + 1) + '/' + Ext.ComponentMgr.get('edtDaData').getValue().getFullYear() + ' ' + Ext.ComponentMgr.get('edtDaOre').getValue().toString() + ':' + this.getValue().toString());
                            dataFine = Ext.ComponentMgr.get('edtDaData').getValue().getFullYear() + '-' + (Ext.ComponentMgr.get('edtDaData').getValue().getMonth() + 1) + '-' + Ext.ComponentMgr.get('edtDaData').getValue().getDate() + ' ' + Ext.ComponentMgr.get('edtDaOre').getValue().toString() + ':' + this.getValue().toString() + ':00';
                        }, c);
                    }
                }
            }, {
                xtype: 'button',
                id: 'btnSearchDaOre',
                iconAlign: 'left',
                icon: 'images/search.png',
                handler: function () {
                    Ext.ComponentMgr.get('btnDaData').setText('Dal:' + Ext.ComponentMgr.get('edtDaData').getValue().getDate() + '/' + (Ext.ComponentMgr.get('edtDaData').getValue().getMonth() + 1) + '/' + Ext.ComponentMgr.get('edtDaData').getValue().getFullYear() + ' ' + Ext.ComponentMgr.get('edtDaOre').getValue().toString() + ':' + Ext.ComponentMgr.get('edtDaMinuti').getValue().toString());
                    dataInizio = Ext.ComponentMgr.get('edtDaData').getValue().getFullYear() + '-' + (Ext.ComponentMgr.get('edtDaData').getValue().getMonth() + 1) + '-' + Ext.ComponentMgr.get('edtDaData').getValue().getDate() + ' ' + Ext.ComponentMgr.get('edtDaOre').getValue().toString() + ':' + Ext.ComponentMgr.get('edtDaMinuti').getValue().toString() + ':00'
                    updateView();
                }
            }]
        }]
    });


    var menuDataAFilter = Ext.create('Ext.menu.Menu', {
        id: 'menuDataAFilter',
        showSeparator: false,
        style: {
            overflow: 'visible'     // For the Combo popup
        },
        items: [{
            xtype: 'datepicker',
            id: 'edtAData',
            //fieldLabel: 'Da Data',
            format: 'd/m/Y',
            maxValue: new Date(),
            value: new Date(),
            //labelWidth: 50,
            listeners: {
                'select': function () {
                    Ext.ComponentMgr.get('btnAData').setText('Al:' + this.getValue().getDate() + '/' + (this.getValue().getMonth() + 1) + '/' + this.getValue().getFullYear() + ' ' + Ext.ComponentMgr.get('edtAOre').getValue().toString() + ':' + Ext.ComponentMgr.get('edtAMinuti').getValue().toString());
                    dataFine = this.getValue().getFullYear() + '-' + (this.getValue().getMonth() + 1) + '-' + this.getValue().getDate() + ' ' + Ext.ComponentMgr.get('edtAOre').getValue().toString() + ':' + Ext.ComponentMgr.get('edtAMinuti').getValue().toString() + ':00'
                }
            }
        }, {
            xtype: 'fieldcontainer',
            layout: 'hbox',
            icon: 'images/speed.png',
            fieldLabel: 'A Ore',
            labelWidth: 50,
            items: [{
                xtype: 'numberfield',
                id: 'edtAOre',
                width: 40,
                minValue: 00,
                maxValue: 23,
                value: 00,
                allowDecimals: false,
                hideTrigger: true,
                listeners: {
                    'render': function (c) {
                        c.getEl().on('keyup', function () {
                            Ext.ComponentMgr.get('btnAData').setText('Al:' + Ext.ComponentMgr.get('edtAData').getValue().getDate() + '/' + (Ext.ComponentMgr.get('edtAData').getValue().getMonth() + 1) + '/' + Ext.ComponentMgr.get('edtAData').getValue().getFullYear() + ' ' + this.getValue().toString() + ':' + Ext.ComponentMgr.get('edtAMinuti').getValue().toString());
                            dataFine = Ext.ComponentMgr.get('edtAData').getValue().getFullYear() + '-' + (Ext.ComponentMgr.get('edtAData').getValue().getMonth() + 1) + '-' + Ext.ComponentMgr.get('edtAData').getValue().getDate() + ' ' + this.getValue().toString() + ':' + Ext.ComponentMgr.get('edtAMinuti').getValue().toString() + ':00'
                        }, c);
                    }
                }
            }, {
                xtype: 'label',
                text: ' : '
            }, {
                xtype: 'numberfield',
                id: 'edtAMinuti',
                width: 40,
                minValue: 00,
                maxValue: 59,
                value: 00,
                allowDecimals: false,
                hideTrigger: true,
                listeners: {
                    'render': function (c) {
                        c.getEl().on('keyup', function () {
                            Ext.ComponentMgr.get('btnAData').setText('Al:' + Ext.ComponentMgr.get('edtAData').getValue().getDate() + '/' + (Ext.ComponentMgr.get('edtAData').getValue().getMonth() + 1) + '/' + Ext.ComponentMgr.get('edtAData').getValue().getFullYear() + ' ' + Ext.ComponentMgr.get('edtAOre').getValue().toString() + ':' + this.getValue().toString());
                            dataFine = Ext.ComponentMgr.get('edtAData').getValue().getFullYear() + '-' + (Ext.ComponentMgr.get('edtAData').getValue().getMonth() + 1) + '-' + Ext.ComponentMgr.get('edtAData').getValue().getDate() + ' ' + Ext.ComponentMgr.get('edtAOre').getValue().toString() + ':' + this.getValue().toString() + ':00'
                        }, c);
                    }
                }
            }, {
                xtype: 'button',
                id: 'btnSearchAOre',
                iconAlign: 'left',
                icon: 'images/search.png',
                handler: function () {
                    Ext.ComponentMgr.get('btnAData').setText('Al:' + Ext.ComponentMgr.get('edtAData').getValue().getDate() + '/' + (Ext.ComponentMgr.get('edtAData').getValue().getMonth() + 1) + '/' + Ext.ComponentMgr.get('edtAData').getValue().getFullYear() + ' ' + Ext.ComponentMgr.get('edtAOre').getValue().toString() + ':' + Ext.ComponentMgr.get('edtAMinuti').getValue().toString());
                    dataFine = Ext.ComponentMgr.get('edtAData').getValue().getFullYear() + '-' + (Ext.ComponentMgr.get('edtAData').getValue().getMonth() + 1) + '-' + Ext.ComponentMgr.get('edtAData').getValue().getDate() + ' ' + Ext.ComponentMgr.get('edtAOre').getValue().toString() + ':' + Ext.ComponentMgr.get('edtAMinuti').getValue().toString() + ':00'
                    updateView();
                }
            }]
        }]
    });




    Ext.create('Ext.Viewport', {
        id: 'vpMain',
        layout: 'border',
        items: [{
            collapsible: false,
            id: 'pnlCenter',
            region: 'center',
            layout: 'fit',
            items: [Ext.ComponentMgr.get('pnlPercorsi')],
            tbar: Ext.create('Ext.toolbar.Toolbar', {
                defaults: {
                    iconAlign: 'left'
                },
                id: 'tbar_Temp',
                items: [Ext.ComponentMgr.get('cbVettore'), {
                    xtype: 'button',
                    icon: 'images/all.png',
                    handler: function () {
                        Ext.ComponentMgr.get('cbVettore').setRawValue('');
                        Ext.ComponentMgr.get('cbVettore').setValue('');
                        updateView();
                    }
                }, {
                    id: 'btnDaData',
                    hidden:true,
                    text: 'Dal: ' + dataOdierna.getDate() + '/' + (dataOdierna.getMonth() + 1) + '/' + dataOdierna.getFullYear() + ' 00:00',
                    icon: 'images/calendar.png',  // <-- icon
                    menu: menuDataDaFilter  // assign menu by instance
                }, {
                    id: 'btnAData',
                    hidden: true,
                    text: 'Al: ' + dataOdierna.getDate() + '/' + (dataOdierna.getMonth() + 1) + '/' + dataOdierna.getFullYear() + ' 23:59',
                    icon: 'images/calendar.png',  // <-- icon
                    menu: menuDataAFilter  // assign menu by instance
                }, Ext.ComponentMgr.get('cbStatoMotore'), '-',
                    {
                        xtype: 'numberfield',
                        fieldLabel: 'Km/h (>=)',
                        width: 150,
                        labelWidth: 70,
                        id: 'edtKmh',
                        minValue: 0,
                        value: 0,
                        disabled: true,
                        allowDecimals: false
                    }, {
                        xtype: 'button',
                        id: 'btnSearchKmh',
                        iconAlign: 'left',
                        disabled: true,
                        icon: 'images/search.png',
                        handler: function () {
                            velocita = Ext.ComponentMgr.get('edtKmh').getValue();
                            updateView();
                        }
                    }, '-', {
                        xtype: 'button',
                        id: 'btnApplicaFiltro',
                        text: 'Applica Filtro per Km/h',
                        iconAlign: 'left',
                        icon: 'images/speed.png',
                        enableToggle: true,
                        handler: function () {
                            if (this.pressed) {
                                Ext.ComponentMgr.get('edtKmh').enable();
                                Ext.ComponentMgr.get('btnSearchKmh').enable();
                                updateView();
                            } else {
                                Ext.ComponentMgr.get('edtKmh').disable();
                                Ext.ComponentMgr.get('btnSearchKmh').disable();
                                updateView();
                            }
                        }
                    }]
            })
        }]

    });


    Ext.ComponentMgr.get('btnApplicaFiltro').enable();
    Ext.ComponentMgr.get('cbStatoMotore').enable();
//    Ext.ComponentMgr.get('btnDaData').show();
//    Ext.ComponentMgr.get('btnAData').show();
    if (Ext.ComponentMgr.get('btnApplicaFiltro').pressed) {
        Ext.ComponentMgr.get('edtKmh').enable();
        Ext.ComponentMgr.get('btnSearchKmh').enable();
        updateView();
    } else {
        Ext.ComponentMgr.get('edtKmh').disable();
        Ext.ComponentMgr.get('btnSearchKmh').disable();
        updateView();
    }

});

