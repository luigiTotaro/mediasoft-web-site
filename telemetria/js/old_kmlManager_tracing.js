﻿google.load('maps', '2');
var geoXml = null;
var map = null;
var centerCoords = null;
var idSquadra = null;
var day = null;
var geo;
var reasons = [];
var geoXml2 = null;
var area = null;
var geoXmlMarker = null;
var standards = [["road", "rd"],
                        ["street", "st"],
                        ["avenue", "ave"],
                        ["av", "ave"],
                        ["drive", "dr"],
                        ["saint", "st"],
                        ["north", "n"],
                        ["south", "s"],
                        ["east", "e"],
                        ["west", "w"],
                        ["expressway", "expy"],
                        ["parkway", "pkwy"],
                        ["terrace", "ter"],
                        ["turnpike", "tpke"],
                        ["highway", "hwy"],
                        ["lane", "ln"]
                     ];


function getUrlVariabili() {
    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function (m, key, value) {
        vars[key] = value;
    });
    return vars;
}

function first() {
    //alert(document.getElementById("map"));
    map = new GMap2(document.getElementById("map"));
    //map = new GMap(document.getElementById("map"));
    //map.addControl(new GLargeMapControl());
    //map.addControl(new GMapTypeControl());
    //map.setCenter(new GLatLng(20, 0), 2);

    // ====== Create a Client Geocoder ======
    geo = new GClientGeocoder();
    //GEvent.addListener(map, "singlerightclick", mapSingleRightClick);
    GEvent.addListener(map, "moveend", moveend);
    GEvent.addListener(map, "zoomend", zoomend);
    GEvent.addListener(map, "dragend", dragend);
    
    // ====== Array for decoding the failure codes ======
    reasons[G_GEO_SUCCESS] = "Success";
    reasons[G_GEO_MISSING_ADDRESS] = "Missing Address: The address was either missing or had no value.";
    reasons[G_GEO_UNKNOWN_ADDRESS] = "Unknown Address:  No corresponding geographic location could be found for the specified address.";
    reasons[G_GEO_UNAVAILABLE_ADDRESS] = "Unavailable Address:  The geocode for the given address cannot be returned due to legal or contractual reasons.";
    reasons[G_GEO_BAD_KEY] = "Bad Key: The API key is either invalid or does not match the domain for which it was given";
    reasons[G_GEO_TOO_MANY_QUERIES] = "Too Many Queries: The daily geocoding quota for this site has been exceeded.";
    reasons[G_GEO_SERVER_ERROR] = "Server error: The geocoding request could not be successfully processed.";
    centerCoords = new GLatLng(40.165232, 18.263397);
    initialize(0);
}
function zoomend(oldLevel, newLevel) {

    aggiornaParametri();
}
function moveend() {
    aggiornaParametri();
}
function dragend() {
    aggiornaParametri();
}
function aggiornaParametri() {
    document.getElementById("center").value = map.getCenter();
    document.getElementById("zoom").value = map.getZoom();

}
function visualizza() {
   /* var dropdownIndex = document.getElementById('cmbSquadre').selectedIndex;
    var dropdownValue = document.getElementById('cmbSquadre')[dropdownIndex].value;
    document.getElementById("idSquadra").value = dropdownValue;

    var ggIdx = document.getElementById('cmbGG').selectedIndex;
    var ggValue = document.getElementById('cmbGG')[ggIdx].value;
    var mmIdx = document.getElementById('cmbMM').selectedIndex;
    var mmValue = document.getElementById('cmbMM')[mmIdx].value;
    var yyIdx = document.getElementById('cmbYY').selectedIndex;
    var yyValue = document.getElementById('cmbYY')[yyIdx].value;
    document.getElementById("day").value = ggValue + "/" + mmValue + "/" + yyValue;

    var oraIdx1 = document.getElementById('cmbStartOra').selectedIndex;
    var oraValue1 = document.getElementById('cmbStartOra')[oraIdx1].value;
    var minIdx1 = document.getElementById('cmbStartMinuti').selectedIndex;
    var minValue1 = document.getElementById('cmbStartMinuti')[minIdx1].value;

    var oraIdx2 = document.getElementById('cmbStopOra').selectedIndex;
    var oraValue2 = document.getElementById('cmbStopOra')[oraIdx2].value;
    var minIdx2 = document.getElementById('cmbStopMinuti').selectedIndex;
    var minValue2 = document.getElementById('cmbStopMinuti')[minIdx2].value;

    document.getElementById("start").value = oraValue1 + "_" + minValue1;
    document.getElementById("stop").value = oraValue2 + "_" + minValue2;*/
    updateView();
}
function initializeArea() {
    area = "area";
    updateView();
}
var exml;
function updateView() {
    first();
    idSquadra = document.getElementById("idSquadra").value;
    day = document.getElementById("day").value;
    start = document.getElementById("start").value;
    stop = document.getElementById("stop").value;
    var pathToKml;


    pathToKml = 'http://localhost:49227/graphUtils.aspx?action=getPercorsi&idUtente=rollog&giorno=6&mese=10&anno=2011'; //'http://localhost:52334/PackageIOT/ModuloServiceMobile/kml_movSquadra.aspx?idUtente=1&giorno=25&mese=8&anno=2011';

    map.clearOverlays();
    //map.addControl(new GLargeMapControl());
    //map.addControl(new GMapTypeControl());
   // geoXml = new GGeoXml(pathToKml);
    //geoXml = new GGeoXml("http://192.170.5.71/PackageIOT/ModuloServiceMobile/cta.kml");
   // geoXml = new GGeoXml("http://gmaps-samples.googlecode.com/svn/trunk/ggeoxml/cta.kml");
    //alert("AGGIUNGO IL LIVELLO");
    
    //exml = new EGeoXml("exml", map, "cta.kml");
    //alert(Ext.ComponentMgr.get('edtData').getValue().getFullYear());
    var m = new Date().getMonth() + 1;
    var a = new Date().getFullYear();
    var g = new Date().getDate();
    var motore = Ext.ComponentMgr.get('cbStatoMotore').getValue();
    var mac = '';
    var idVettore = -1;
    if ((Ext.ComponentMgr.get('cbVettore').getRawValue() != '') && (Ext.ComponentMgr.get('cbVettore').getRawValue() != null)) {
        idVettore = Ext.ComponentMgr.get('cbVettore').findRecordByDisplay(Ext.ComponentMgr.get('cbVettore').getRawValue()).get('codVettore')
    } else {
        idVettore = -1
    }
    //alert(idVettore);
    if ((Ext.ComponentMgr.get('cbVettore').getValue() != '') && (Ext.ComponentMgr.get('cbVettore').getValue() != null)){
        mac = Ext.ComponentMgr.get('cbVettore').getValue();
    }else{
        mac = "undefined";
    };
    var dataDa = dataInizio;
    var dataA = dataFine;

    var kmh = velocita;

    //alert("exml" + a + "_" + m + "_" + g);
    //var ute = Ext.ComponentMgr.get('txtCodAutista').getValue();    
    
    //exml = new EGeoXml("exml" + a+"_"+m+"_"+g, map, "../ModuloServiceMobile/kml_movSquadra.aspx?idUtente=" + ute + "&giorno=" + g + "&mese=" + m + "&anno=" + a);
    // alert("/telemetria/graphUtils.aspx?action=getPercorsi&idUtente=" + ute + "&giorno=" + g + "&mese=" + m + "&anno=" + a + "&motore=" + motore + "&mac=" + mac + "&kmh=" + kmh + "&da=" + dataDa + "&a=" + dataA+"&idVettore="+idVettore);

    var ute = getUrlVariabili()['idUser'];
    var codCliente = getUrlVariabili()['cdCl'];
    var codSettore = getUrlVariabili()['cdSett'];
    var codSottoSett = getUrlVariabili()['codSottoSett'];

    var urlTracing = "/telemetria/graphUtils.aspx?action=getPercorsiByCliente&idUtente=" + ute + "&giorno=" + g + "&mese=" + m + "&anno=" + a + "&motore=" + motore + "&mac=" + mac + "&kmh=" + kmh + "&da=" + dataDa + "&a=" + dataA + "&idVettore=" + idVettore + "&codCliente=" + codCliente + "&codSettore=" + codSettore + "&codSottoSett=" + codSottoSett
    //alert(urlTracing);
    exml = new EGeoXml("exml", map, urlTracing, //"http://smoconline.unionkey.com:8088/PackageBusiness/ModuloVettori/ModuloServiceMobile/kml_movSquadra.aspx?idUtente=76309&giorno=27&mese=9&anno=2011"
      { sidebarid: "the_side_bar",
          iwwidth: 300,
          directions: true,
          titlestyle: 'style = "font-family:Tahoma,serif;color:#FF0000;font-size: 16pt;background-color:#FFFF00;text-align:center;"',
          descstyle: 'class = "description"',
          directionstyle: 'class = "directions"'
      });
    exml.parse();
   
    //map.addOverlay(geoXml);
    document.getElementById("pathKml").value = pathToKml;
}
function initialize(ref) {

    if (ref == 0) {
        map.setCenter(centerCoords, 10);
        map.addMapType(G_SATELLITE_3D_MAP);
        map.addControl(new GHierarchicalMapTypeControl());
        map.addControl(new GLargeMapControl());
    }

    //updateView();
    //setTimeout("initialize(1)", 60000);






}

function refresh() {
}
function fullScreen() {
    window.open("fullscreen_roadSquadre.aspx?idSquadra=" + idSquadra + "&day=" + day, '', 'fullscreen=yes, scrollbars=auto');
}


function disegnaArea() {
    var strParam = "";
    if (document.getElementById("lblVertice1").value != "") {
        var v1 = document.getElementById("lblVertice1").value.replace(" ", "_");
        strParam += "&v1=" + v1;
    }
    if (document.getElementById("lblVertice2").value != "") {
        var v2 = document.getElementById("lblVertice2").value.replace(" ", "_");
        strParam += "&v2=" + v2;
    }
    if (document.getElementById("lblVertice3").value != "") {
        var v3 = document.getElementById("lblVertice3").value.replace(" ", "_");
        strParam += "&v3=" + v3;
    }
    if (document.getElementById("lblVertice4").value != "") {
        var v4 = document.getElementById("lblVertice4").value.replace(" ", "_");
        strParam += "&v4=" + v4;
    }

    var pathToKml = 'http://www.oneclicksite.org/kml_printArea.aspx?refresh=' + Math.floor(Math.random() * 2342343) + strParam;
    if (geoXml2 != null) {
        map.removeOverlay(geoXml2);
    }
    geoXml2 = new GGeoXml(pathToKml);

    map.addOverlay(geoXml2);
}

function mapSingleRightClick(p, src, overlay) {

    var controllo = false;
    var vertice = document.getElementById("lblVertice1").value;
    if (vertice != "") {
        vertice = document.getElementById("lblVertice2").value;
        if (vertice != "") {
            vertice = document.getElementById("lblVertice3").value;
            if (vertice != "") {

                document.getElementById("lblVertice1").value = "";
                vertice = document.getElementById("lblVertice2").value = "";
                document.getElementById("lblVertice3").value = "";

            }
            else {
                document.getElementById("lblVertice3").value = map.fromContainerPixelToLatLng(p).lat() + " " + map.fromContainerPixelToLatLng(p).lng();
                controllo = true;
            }
        }
        else {
            document.getElementById("lblVertice2").value = map.fromContainerPixelToLatLng(p).lat() + " " + map.fromContainerPixelToLatLng(p).lng();
        }
    }
    else {
        document.getElementById("lblVertice1").value = map.fromContainerPixelToLatLng(p).lat() + " " + map.fromContainerPixelToLatLng(p).lng();
    }
    disegnaArea();
    if (geoXmlMarker != null) {
        map.removeOverlay(geoXmlMarker);
    }
    if (document.getElementById("lblVertice1").value != "") {
        var icon = new GIcon();
        icon.image = "http://labs.google.com/ridefinder/images/mm_20_green.png";
        icon.iconSize = new GSize(12, 20);
        icon.iconAnchor = new GPoint(0, 22);
        icon.infoWindowAnchor = new GPoint(10, 1);

        geoXmlMarker = new GMarker(map.fromContainerPixelToLatLng(p), icon);
        map.addOverlay(geoXmlMarker);

    }
    else {
        controllo = true;
    }
    if (controllo) {
        initializeArea();
    }
}

function resetArea() {
    if (geoXml2 != null) {
        map.removeOverlay(geoXml2);
    }
    document.getElementById("lblVertice1").value = "";
    document.getElementById("lblVertice2").value = "";
    document.getElementById("lblVertice3").value = "";
    document.getElementById("lblVertice4").value = "";
    area = null;
}


//<![CDATA[


    // var map;





    // ===== list of words to be standardized =====


    // ===== convert words to standard versions =====
    function standardize(a) {
        for (var i = 0; i < standards.length; i++) {
            if (a == standards[i][0]) { a = standards[i][1]; }
        }
        return a;
    }

    // ===== check if two addresses are sufficiently different =====
    function different(a, b) {
        // only interested in the bit before the first comma in the reply
        var c = b.split(",");
        b = c[0];
        // convert to lower case
        a = a.toLowerCase();
        b = b.toLowerCase();
        // remove apostrophies
        a = a.replace(/'/g, "");
        b = b.replace(/'/g, "");
        // replace all other punctuation with spaces
        a = a.replace(/\W/g, " ");
        b = b.replace(/\W/g, " ");
        // replace all multiple spaces with a single space
        a = a.replace(/\s+/g, " ");
        b = b.replace(/\s+/g, " ");
        // split into words
        awords = a.split(" ");
        bwords = b.split(" ");
        // perform the comparison
        var reply = false;
        for (var i = 0; i < bwords.length; i++) {
            //GLog.write (standardize(awords[i])+"  "+standardize(bwords[i]))
            if (standardize(awords[i]) != standardize(bwords[i])) { reply = true }
        }
        //GLog.write(reply);
        return (reply);
    }


    // ====== Plot a marker after positive reponse to "did you mean" ======
    function place(lat, lng) {
        var point = new GLatLng(lat, lng);
        map.setCenter(point, 14);

        map.addOverlay(new GMarker(point));
        map.addOverlay(geoXml);
        document.getElementById("message").innerHTML = "";
    }

    // ====== Geocoding ======
    function showAddress() {
        var search = document.getElementById("search").value;
        // ====== Perform the Geocoding ======        
        geo.getLocations(search, function(result) {
            map.clearOverlays();
            if (result.Status.code == G_GEO_SUCCESS) {
                // ===== If there was more than one result, "ask did you mean" on them all =====
                if (result.Placemark.length > 1) {
                    document.getElementById("message").innerHTML = "<b>Cosa stai cercando?:</b>";
                    // Loop through the results
                    for (var i = 0; i < result.Placemark.length; i++) {
                        var p = result.Placemark[i].Point.coordinates;
                        document.getElementById("message").innerHTML += "<br>" + (i + 1) + ": <a href='javascript:place(" + p[1] + "," + p[0] + ")'>" + result.Placemark[i].address + "<\/a>";
                    }
                }
                // ===== If there was a single marker, is the returned address significantly different =====
                else {
                    document.getElementById("message").innerHTML = "";
                    if (different(search, result.Placemark[0].address)) {
                        document.getElementById("message").innerHTML = "Did you mean: ";
                        var p = result.Placemark[0].Point.coordinates;
                        document.getElementById("message").innerHTML += "<a href='javascript:place(" + p[1] + "," + p[0] + ")'>" + result.Placemark[0].address + "<\/a>";
                    } else {
                        var p = result.Placemark[0].Point.coordinates;
                        place(p[1], p[0]);
                        document.getElementById("message").innerHTML = "<b>Trovato</b>: " + result.Placemark[0].address;
                    }
                }
            }
            // ====== Decode the error status ======
            else {
                var reason = "Code " + result.Status.code;
                if (reasons[result.Status.code]) {
                    reason = reasons[result.Status.code]
                }
                alert('Non è stato possibile trovare "' + search + '" ');
            }
        }
        );
    }




//]]>