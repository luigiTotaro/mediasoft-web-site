﻿//ATTENZIONE. QUESTO FILE VA COPIATO NELLA CARTELLA REMOTA DEL SERVER DELLA TELEMETRIA. NON IN QUELLO DI PRODUZIONE DI UK.
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data;
using System.Data.SqlClient;
using Newtonsoft.Json;

public partial class remoteUtils : System.Web.UI.Page
{

    string oraDb;
    private SqlConnection oc;
    private String RES;
    private string action;


    private string checkNameParameter(string name)
    {
        if (name.Substring(0, 1) != "@")
        {
            return "@" + name;
        }
        else
        {
            return name;
        }
    }

    private string checkStoredNameParameter(string name)
    {
        //toglie il nome del package. Nel caso locale non serve.

        int idxPoint = name.IndexOf(".");
        if (idxPoint == -1)
        {
            return name;
        }
        else
        {
            return name.Substring(idxPoint + 1);
        }
    }



    private SqlDbType obtainType(string type)
    {

        SqlDbType typeToRet = SqlDbType.VarChar;
        switch (type)
        {
            case "Number":
                typeToRet = SqlDbType.Int;
                break;
            case "Cursor":
                //null lo ingnora in quanto non ce necessita di definire parametri di output per le connesioni sql server
                break;
            case "String":
                typeToRet = SqlDbType.VarChar;
                break;
            case "VarChar":
                typeToRet = SqlDbType.VarChar;
                break;
            case "Bool":
                typeToRet = SqlDbType.Bit;
                break;
            case "DateTime":
                typeToRet = SqlDbType.DateTime;
                break;
            case "float":
                typeToRet = SqlDbType.Float;
                break;
            case "money":
                typeToRet = SqlDbType.Money;
                break;
        }
        return typeToRet;
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        Response.AppendHeader("Access-Control-Allow-Origin", "*");


        string action = Request.Params["action"];
        if (action == null)
            return;
        switch (action)
        {

            case "salvaAreaVettore":
                salvaAreaVettore();
                break;
            case "getCurrentAreaVettore":
                getCurrentAreaVettore();
                break;
            case "clearAreaVettore":
                clearAreaVettore();
                break;
        }
    }
    
    private void clearAreaVettore()
    {
        try
        {
            oraDb = ConfigurationSettings.AppSettings["LocalConnection"].ToString();
            oc = new System.Data.SqlClient.SqlConnection(oraDb);
            oc.Open();

            SqlCommand objCmd = new SqlCommand();
            objCmd.Connection = oc;
            objCmd.CommandTimeout = 120;
            objCmd.CommandText = "Delete from T_CONFIG_KML where idVettore=" + Request.Params["codVettore"] + "";
            objCmd.CommandType = CommandType.Text;


            SqlDataAdapter adapter = new SqlDataAdapter(objCmd);
            System.Data.DataSet dsReturn = new DataSet();
            adapter.Fill(dsReturn);
            oc.Close();
        }
        catch (Exception ex)
        {
            Response.Write(ex.Message.ToString() + " - " + ex.StackTrace.ToString());
        }
    }
    private void salvaAreaVettore()
    {
        try
        {
            oraDb = ConfigurationSettings.AppSettings["LocalConnection"].ToString();
            oc = new System.Data.SqlClient.SqlConnection(oraDb);
            oc.Open();
            SqlCommand objCmd = new SqlCommand();
            objCmd.Connection = oc;
            objCmd.CommandTimeout = 120;
            objCmd.CommandText = "Insert into T_CONFIG_KML(idVettore,targa,modelloVettore,Lon,Lat,mac,lastUpdate) values(" + Request.Params["codVettore"] + ",'" + Request.Params["targa"].ToString().Replace("'", "") + "',''," + Request.Params["Lon"].ToString().Replace(",", ".") + "," + Request.Params["Lat"].ToString().Replace(",", ".") + ",'',GETDATE());";
            objCmd.CommandType = CommandType.Text;
            SqlDataAdapter adapter = new SqlDataAdapter(objCmd);
            System.Data.DataSet dsReturn = new DataSet();
            adapter.Fill(dsReturn);
            //Response.Write(res);
            oc.Close();
        }
        catch (Exception ex)
        {
            Response.Write(ex.Message.ToString() + " - " + ex.StackTrace.ToString());
        }
    }
    private void getCurrentAreaVettore()
    {
        try
        {
            oraDb = ConfigurationSettings.AppSettings["LocalConnection"].ToString();
            oc = new System.Data.SqlClient.SqlConnection(oraDb);
            oc.Open();

            SqlCommand objCmd = new SqlCommand();
            objCmd.Connection = oc;
            objCmd.CommandTimeout = 120;
            objCmd.CommandText = "Select idVettore,targa,LTRIM(RTRIM(Lon)) as Lon, LTRIM(RTRIM(Lat)) as Lat from T_CONFIG_KML where idVettore=" + Request.Params["codVettore"] + " order by id";
            objCmd.CommandType = CommandType.Text;


            SqlDataAdapter adapter = new SqlDataAdapter(objCmd);
            System.Data.DataSet dsReturn = new DataSet();
            adapter.Fill(dsReturn);
            string res = "{";

            res += JsonConvert.SerializeObject(dsReturn.Tables[0]);

            res += " }";

            Response.Write(res);
            oc.Close();
        }
        catch (Exception ex)
        {
            Response.Write(ex.Message.ToString() + " - " + ex.StackTrace.ToString());
        }
    }
   
}
