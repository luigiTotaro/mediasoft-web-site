﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Newtonsoft.Json;
using System.Data.SqlClient;

using System;
using System.Collections.Generic;
using System.Web;
using System.Xml;
using System.IO;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;



    public  partial class test : System.Web.UI.Page
    {
        
       string oraDb;
        private SqlConnection oc;
        private String RES;


        
        private string checkNameParameter(string name)
        {
            if (name.Substring(0, 1) != "@")
            {
                return "@" + name;
            }
            else
            {
                return name;
            }
        }

        private string checkStoredNameParameter(string name)
        {
            //toglie il nome del package. Nel caso locale non serve.

            int idxPoint = name.IndexOf(".");
            if (idxPoint == -1)
            {
                return name;
            }
            else
            {
                return name.Substring(idxPoint+1);
            }
        }
       
        

        private SqlDbType obtainType(string type)
        {

            SqlDbType typeToRet = SqlDbType.VarChar;
            switch (type)
            {
                case "Number":
                    typeToRet = SqlDbType.Int;
                    break;
                case "Cursor":
                    //null lo ingnora in quanto non ce necessita di definire parametri di output per le connesioni sql server
                    break;
                case "String":
                    typeToRet = SqlDbType.VarChar;
                    break;
                case "VarChar":
                    typeToRet = SqlDbType.VarChar;
                    break;
                case "Bool":
                    typeToRet = SqlDbType.Bit;
                    break;
                case "DateTime":
                    typeToRet = SqlDbType.DateTime;
                    break;
                case "float":
                    typeToRet = SqlDbType.Float;
                    break;
                case "money":
                    typeToRet = SqlDbType.Money;
                    break;
            }
            return typeToRet;
        }
    
           


        


        protected void Page_Load(object sender, EventArgs e)
        {
            try
             {
                 oraDb = ConfigurationSettings.AppSettings["LocalConnection"].ToString();
                 oc = new System.Data.SqlClient.SqlConnection(oraDb);
                 String RES;

                 oc.Open();

                 SqlCommand objCmd = new SqlCommand();
                 objCmd.Connection = oc;
                 objCmd.CommandTimeout = 120;
                 objCmd.CommandText = checkStoredNameParameter("SP_insertTelemetria"); //NomePackage.NomeProcedure 
                 objCmd.CommandType = CommandType.StoredProcedure;
                 if (Request.Params["temp"] == null)
                {
                    objCmd.Parameters.Add(new SqlParameter(checkNameParameter("@temperatura"), obtainType("String"))).Value = "";// DBNull.Value;
                }
                else
                {
                    double tempAppoggio = (333.3333 * ((float.Parse(Request.Params["temp"].ToString()) / 120000) - 0.2)) - 40;
                    objCmd.Parameters.Add(new SqlParameter(checkNameParameter("@temperatura"), obtainType("String"))).Value = tempAppoggio.ToString();
                }

                 if (Request.Params["perm"] == null)
                 {
                     objCmd.Parameters.Add(new SqlParameter(checkNameParameter("@permettivita"), obtainType("String"))).Value = "";// DBNull.Value;
                 }
                 else
                 {
                     double permAppoggio = ((float.Parse(Request.Params["perm"].ToString()) / 120000) - 0.08) / 0.12;
                     objCmd.Parameters.Add(new SqlParameter(checkNameParameter("@permettivita"), obtainType("String"))).Value = permAppoggio.ToString();
                 }

                 if (Request.Params["level"] == null)
                 {
                     objCmd.Parameters.Add(new SqlParameter(checkNameParameter("@livello"), obtainType("String"))).Value = "";// DBNull.Value;
                 }
                 else
                 {
                     double levelAppoggio = (0.7 * (float.Parse(Request.Params["level"].ToString()) / 120000.0) - 0.2) / ((float.Parse(Request.Params["perm"].ToString()) / 120000.0) - 0.2);
                     objCmd.Parameters.Add(new SqlParameter(checkNameParameter("@livello"), obtainType("String"))).Value = levelAppoggio.ToString();
                 }

                 if (Request.Params["mac"] == null)
                 {
                     objCmd.Parameters.Add(new SqlParameter(checkNameParameter("@macAdd"), obtainType("String"))).Value = "";// DBNull.Value;
                 }
                 else
                 {
                     objCmd.Parameters.Add(new SqlParameter(checkNameParameter("@macAdd"), obtainType("String"))).Value = Request.Params["mac"];
                 }

                 SqlDataAdapter adapter = new SqlDataAdapter(objCmd);
                 System.Data.DataSet dsReturn = new DataSet();
                 adapter.Fill(dsReturn);
                 RES = "OK";
                 Response.Write(RES);
                 oc.Close();
             }
             catch (Exception ex)
             {
                 Response.Write(ex.Message.ToString() + " - " + ex.StackTrace.ToString());
             }

        }
    }
