﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using System.IO;

using System.Data;
using System.Data.SqlClient;

public partial class GPS_imei : System.Web.UI.Page
{


    string oraDb;
    private SqlConnection oc;
    private String RES;

    private string checkNameParameter(string name)
    {
        if (name.Substring(0, 1) != "@")
        {
            return "@" + name;
        }
        else
        {
            return name;
        }
    }

    private string checkStoredNameParameter(string name)
    {
        //toglie il nome del package. Nel caso locale non serve.

        int idxPoint = name.IndexOf(".");
        if (idxPoint == -1)
        {
            return name;
        }
        else
        {
            return name.Substring(idxPoint + 1);
        }
    }



    private SqlDbType obtainType(string type)
    {

        SqlDbType typeToRet = SqlDbType.VarChar;
        switch (type)
        {
            case "Number":
                typeToRet = SqlDbType.Int;
                break;
            case "Cursor":
                //null lo ingnora in quanto non ce necessita di definire parametri di output per le connesioni sql server
                break;
            case "String":
                typeToRet = SqlDbType.VarChar;
                break;
            case "VarChar":
                typeToRet = SqlDbType.VarChar;
                break;
            case "Bool":
                typeToRet = SqlDbType.Bit;
                break;
            case "DateTime":
                typeToRet = SqlDbType.DateTime;
                break;
            case "float":
                typeToRet = SqlDbType.Float;
                break;
            case "money":
                typeToRet = SqlDbType.Money;
                break;
        }
        return typeToRet;
    }


    private string formatData(string data)
    {
        string timestamp = data.Split('.')[0];
        string anno = timestamp.Substring(0, 4);
        string mese = timestamp.Substring(4, 2);
        string giorno = timestamp.Substring(6, 2);
        string ore = timestamp.Substring(7, 2);
        string min = timestamp.Substring(10, 2);
        string sec = timestamp.Substring(12, 2);

        return anno + "-" + mese + "-" + giorno + " " + ore + ":" + min + ":" + sec + "." + data.Split('.')[1];
    }

    private string formatOra(string ora)
    {
        return ora.Substring(0, 2) + ":" + ora.Substring(2, 2) + ":" + ora.Substring(4, 2);
    }


    private string ritornaLaStringa(string byteStr)
    {
        string strToRet = "";
        string imei = byteStr;
        char[] thechars = imei.ToCharArray();

        for (int i = 0; i < thechars.Length; i++)
        {
            string c = thechars[i] + "" + thechars[i + 1];
            strToRet += char.ConvertFromUtf32(int.Parse(c));
            i++;
        }
        return strToRet;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        //Response.Write(Request.Params["dati"]+"");
        //TextWriter sw = File.AppendText("D:/inetpub/portale_mediasoft/telemetria/DATI_SENSORE.HTML");
        //sw.WriteLine(DateTime.Now + " - Dati: " + Request.Params["dati"] + " - MAC:" + Request.Params["MAC"] + "<BR/>");
        //sw.Close();

        try
        {
            oraDb = ConfigurationSettings.AppSettings["LocalConnection"].ToString();
            oc = new System.Data.SqlClient.SqlConnection(oraDb);
            String RES;

            oc.Open();

            SqlCommand objCmd = new SqlCommand();
            objCmd.Connection = oc;
            objCmd.CommandTimeout = 120;

            //data GPS:  ?data=565449484849484848484851555057,GPS131048444956494846495653514951445248494846545149544848444948484648484848484844504849504850494849555253484846484852444844484448464848484848484448464848484848481310797513
            //data SEN: ?data=565449484849484848484851555057,SEN-22432,22952,22160,MOT0 

            string dt = Request.Params["data"] + "";
            string imeiRif = ritornaLaStringa(dt.Split(',')[0]);
            string[] campi = dt.Split(',')[1].Split(',');


            for (int i = 0; i < campi.Length; i++)
            {
                switch (campi[i].Substring(0, 3))
                {
                    case "GPS":
                        //Response.Write("<br/>GPS:" + ritornaLaStringa(campi[i].Replace("GPS", "")));
                        //sw.WriteLine("\n" + DateTime.Now + "GPS:" + ritornaLaStringa(campi[i].Replace("GPS", "")));
                        //sw.WriteLine("IMEI:" + ritornaLaStringa(dt.Split(',')[0]));
 
                        
                        string datiGPS = ritornaLaStringa(campi[i].Replace("GPS", ""));

                        string macAdd = imeiRif.ToString();
                        //string lat = datiGPS.ToString().Split(',')[2];
                        //string lon = datiGPS.ToString().Split(',')[1];

                        //###################### parser lat e lon inizio

                        string strLat = datiGPS.ToString().Split(',')[2];
                        string strLon = "0"+datiGPS.ToString().Split(',')[1];

                        //calcoliamo la latitudine
                        string degLat = strLat.Substring(0, 2);
                        string minLat = strLat.Substring(2, 5);
                        double latNum = double.Parse(degLat.Replace(".", ",")) + (double.Parse(minLat.Replace(".", ",")) / 60);
                        latNum = Math.Round(latNum * 1000000.0) / 1000000.0;

                        //calcoliamo la longitudine
                        string degLon = strLon.Substring(0, 3);
                        string minLon = strLon.Substring(3, 5);
                        double LonNum = double.Parse(degLon.Replace(".", ",")) + (double.Parse(minLon.Replace(".", ",")) / 60);
                        LonNum = Math.Round(LonNum * 1000000.0) / 1000000.0;


                        string lat = latNum.ToString();
                        string lon = LonNum.ToString();

                        //###################### parser lat e lon fine



                        //string dataOraGPS = formatData(datiGPS.ToString().Split(',')[2])+" "+formatOra(datiGPS.ToString().Split(',')[3]);
                        string dataOraGPS = formatData(datiGPS.ToString().Split(',')[4]);  //datiGPS.ToString().Split(',')[2] + " " + datiGPS.ToString().Split(',')[3];
                        Int64 altitudine = Int64.Parse(datiGPS.ToString().Split(',')[3].Split('.')[0]);
                        double kmh = double.Parse(datiGPS.ToString().Split(',')[7]);
                        int motore = -1; //datiGPS.ToString().Split(',')[6];



                        objCmd.CommandText = checkStoredNameParameter("SP_insertLocalizzazioneNEW");

                        objCmd.CommandType = CommandType.StoredProcedure;

                        if (macAdd == null)
                        {
                            objCmd.Parameters.Add(new SqlParameter(checkNameParameter("@macAdd"), obtainType("String"))).Value = "";// DBNull.Value;
                        }
                        else
                        {
                            objCmd.Parameters.Add(new SqlParameter(checkNameParameter("@macAdd"), obtainType("String"))).Value = macAdd;
                        }

                        if (lat == null)
                        {
                            objCmd.Parameters.Add(new SqlParameter(checkNameParameter("@lat"), obtainType("String"))).Value = "";// DBNull.Value;
                        }
                        else
                        {
                            objCmd.Parameters.Add(new SqlParameter(checkNameParameter("@lat"), obtainType("String"))).Value = lat;
                        }

                        if (lon == null)
                        {
                            objCmd.Parameters.Add(new SqlParameter(checkNameParameter("@lon"), obtainType("String"))).Value = "";// DBNull.Value;
                        }
                        else
                        {
                            objCmd.Parameters.Add(new SqlParameter(checkNameParameter("@lon"), obtainType("String"))).Value = lon;
                        }


                        if (dataOraGPS == null)
                        {
                            objCmd.Parameters.Add(new SqlParameter(checkNameParameter("@dataOraGPS"), obtainType("Datetime"))).Value = DBNull.Value;
                        }
                        else
                        {
                            objCmd.Parameters.Add(new SqlParameter(checkNameParameter("@dataOraGPS"), obtainType("Datetime"))).Value = dataOraGPS;
                        }

                        if (altitudine == null)
                        {
                            objCmd.Parameters.Add(new SqlParameter(checkNameParameter("@altitudine"), obtainType("Number"))).Value = DBNull.Value;
                        }
                        else
                        {
                            objCmd.Parameters.Add(new SqlParameter(checkNameParameter("@altitudine"), obtainType("Number"))).Value = altitudine;
                        }
                        if (kmh == null)
                        {
                            objCmd.Parameters.Add(new SqlParameter(checkNameParameter("@kmh"), obtainType("float"))).Value = DBNull.Value;
                        }
                        else
                        {
                            objCmd.Parameters.Add(new SqlParameter(checkNameParameter("@kmh"), obtainType("float"))).Value = kmh;
                        }
                        if (motore == null)
                        {
                            objCmd.Parameters.Add(new SqlParameter(checkNameParameter("@motore"), obtainType("Number"))).Value = DBNull.Value;
                        }
                        else
                        {
                            objCmd.Parameters.Add(new SqlParameter(checkNameParameter("@motore"), obtainType("Number"))).Value = motore;
                        }

                        SqlDataAdapter adapter = new SqlDataAdapter(objCmd);
                        System.Data.DataSet dsReturn = new DataSet();
                        adapter.Fill(dsReturn);
                        RES = "OK";
                        Response.Write(RES);
                        oc.Close();
                        //################################### INIZIO VERIFICA AREA DI COMPETENZA
                        /*
                        if ((macAdd != null) && (lat != null) && (lon != null))
                        {

                            // System.Net.WebRequest wr = System.Net.WebRequest.Create("http://62.149.205.246/telemetria/graphUtils.aspx?action=isInArea&mac=" + macAdd + "&Lat=" + lat + "&Lon=" + lon);
                            System.Net.WebRequest wr = System.Net.WebRequest.Create("http://localhost:49227/graphUtils.aspx?action=isInArea&mac=" + macAdd + "&Lat=" + lat + "&Lon=" + lon);
                            wr.Timeout = 30000;

                            //try
                            //{
                            System.Net.HttpWebResponse response = (System.Net.HttpWebResponse)wr.GetResponse();
                            //}
                            //catch (Exception ex)
                            //{
                            //Response.Write(ex.Message.ToString() + " - " + ex.StackTrace.ToString());
                            //}
                        }*/

                        //################################### FINE VERIFICA AREA DI COMPETENZA

                        break;
                    case "SEN":
                        //int primo = 32767 + 32767 + int.Parse(campi[i].Replace("SEN", ""));
                        //Response.Write("<br/>SEN:" + primo + " ; " + dt.Split(',')[2].Split(',')[0] + " ; " + dt.Split(',')[3].Split(',')[0]);
                        //sw.WriteLine("\n" + DateTime.Now + "SEN:" + primo + " ; " + dt.Split(',')[2].Split(',')[0] + " ; " + dt.Split(',')[3].Split(',')[0]);

                        //Response.Write("<br/>MOT:" + dt.Split(',')[4].Split(',')[0].Replace("MOT", ""));
                        //sw.WriteLine("MOT:" + dt.Split(',')[4].Split(',')[0].Replace("MOT", ""));
                        //sw.WriteLine("IMEI:" + ritornaLaStringa(dt.Split(',')[0]));                            

                        int primo = 32767 + 32767 + int.Parse(campi[i].Replace("SEN", ""));
                        string temp = primo.ToString();
                        string perm = dt.Split(',')[3].Split(',')[0];
                        string livello = dt.Split(',')[2].Split(',')[0];
                        string motoreSen = dt.Split(',')[4].Split(',')[0].Replace("MOT", "");


                        objCmd.CommandText = checkStoredNameParameter("SP_insertTelemetria_imei"); //NomePackage.NomeProcedure 
                        
                        objCmd.CommandType = CommandType.StoredProcedure;
                        if (temp == null)
                        {
                            objCmd.Parameters.Add(new SqlParameter(checkNameParameter("@temperatura"), obtainType("String"))).Value = "";// DBNull.Value;
                        }
                        else
                        {
                            double tempAppoggio = (333.3333 * ((float.Parse(temp.ToString()) / 120000) - 0.2)) - 40;
                            objCmd.Parameters.Add(new SqlParameter(checkNameParameter("@temperatura"), obtainType("String"))).Value = tempAppoggio.ToString();
                        }

                        if (perm == null)
                        {
                            objCmd.Parameters.Add(new SqlParameter(checkNameParameter("@permettivita"), obtainType("String"))).Value = "";// DBNull.Value;
                        }
                        else
                        {
                            double permAppoggio = ((float.Parse(perm.ToString()) / 120000) - 0.08) / 0.12;
                            objCmd.Parameters.Add(new SqlParameter(checkNameParameter("@permettivita"), obtainType("String"))).Value = permAppoggio.ToString();
                        }

                        if (livello == null)
                        {
                            objCmd.Parameters.Add(new SqlParameter(checkNameParameter("@livello"), obtainType("String"))).Value = "";// DBNull.Value;
                        }
                        else
                        {
                            double levelAppoggio = (0.7 * (float.Parse(livello.ToString()) / 120000.0) - 0.2) / ((float.Parse(perm.ToString()) / 120000.0) - 0.2);
                            objCmd.Parameters.Add(new SqlParameter(checkNameParameter("@livello"), obtainType("String"))).Value = levelAppoggio.ToString();
                        }

                        if (imeiRif == null)
                        {
                            objCmd.Parameters.Add(new SqlParameter(checkNameParameter("@macAdd"), obtainType("String"))).Value = "";// DBNull.Value;
                        }
                        else
                        {
                            objCmd.Parameters.Add(new SqlParameter(checkNameParameter("@macAdd"), obtainType("String"))).Value = imeiRif;
                        }

                        if (motoreSen == null)
                        {
                            objCmd.Parameters.Add(new SqlParameter(checkNameParameter("@motore"), obtainType("Number"))).Value = DBNull.Value;
                        }
                        else
                        {
                            objCmd.Parameters.Add(new SqlParameter(checkNameParameter("@motore"), obtainType("Number"))).Value = Int32.Parse(motoreSen);
                        }
                        
                        SqlDataAdapter adapterSen = new SqlDataAdapter(objCmd);
                        System.Data.DataSet dsReturnSen = new DataSet();
                        adapterSen.Fill(dsReturnSen);
                        RES = "OK";
                        Response.Write(RES);
                        oc.Close();

                        break;

                }
            }

            oc.Close();

        }
        catch (Exception ex)
        {
            Response.Write(ex.Message.ToString() + " - " + ex.StackTrace.ToString());
            oc.Close();
        }
        finally
        {
            oc.Close();
        }

    }
}
