<?php
// definiamo una costante per la pagina corrente
define("SELF", $_SERVER['PHP_SELF']);

// definiamo la classe
class Paging
{
  // definiamo la pagina di partenza
  function paginaIniziale($max_row)
  {
    if ((!isset($_GET['p'])) || ($_GET['p'] == "1"))
    {
      $parti_da = 0;
      $_GET['p'] = 1;
    }else{
      $parti_da = ($_GET['p']-1) * $max_row;
    }
    return $parti_da;
  }

  // contiamo le pagine e stabiliamo quanti records devono essere impaginati 
  function contaPagine($conta, $max_row)
  {
    $pgg = (($conta % $max_row) == 0) ? $conta / $max_row : floor($conta / $max_row) + 1;
    return $pgg;
  }

  // mostriamo l'elenco delle pagine
  function listaPagine($p_corrente, $pgg,$SELF,$categoria)
  {
	if($categoria == ''){
		$listapgg = "";
		if (($p_corrente != 1) && ($p_corrente))
		{
		  //$listapgg .= " <a href=\"".$SELF."-pagina-1.html\">Prima pag.</a> ";
		}
		if (($p_corrente-1) > 0)
		{
		  //$listapgg .= "<a href=\"".$SELF."-pagina-".($p_corrente-1).".html\"><</a> ";
		}
		for ($i=1; $i<=$pgg; $i++)
		{
		  if ($i == $p_corrente)
		  {
			$listapgg .= "<button class=\"btn\" style=\"background-image:url('../images_belvedere/azzurro.png');\" ><b>".$i."</b></button>";
		  }else{
			$listapgg .= "<a class=\"btn\" href=\"".$SELF."-pagina-".$i.".html\">".$i."</a>";
		  }
		  $listapgg .= " ";
		}
		if (($p_corrente+1) <= $pgg)
		{
		  //$listapgg .= "<a  href=\"".$SELF."-pagina-".($p_corrente+1).".html\">></a> ";
		}
		if (($p_corrente != $pgg) && ($pgg != 0))
		{
		 // $listapgg .= "<a href=\"".$SELF."-pagina-".$pgg.".html\">Ultima pag.</a> ";
		}
		$listapgg .= "</td>\n";
		return $listapgg;
	}else{
		$listapgg = "";
		if (($p_corrente != 1) && ($p_corrente))
		{
		  $listapgg .= " <a href=\"javascript:post_to_url('".$SELF."-pagina-1.html','".$categoria."')\">Prima pag.</a> ";
		}
		if (($p_corrente-1) > 0)
		{
		  $listapgg .= "<a href=\"javascript:post_to_url('".$SELF."-pagina-".($p_corrente-1).".html','".$categoria."')\"><</a> ";
		}
		for ($i=1; $i<=$pgg; $i++)
		{
		  if ($i == $p_corrente)
		  {
			$listapgg .= "<b>".$i."</b>";
		  }else{
			$listapgg .= "<a href=\"javascript:post_to_url('".$SELF."-pagina-".$i.".html','".$categoria."')\">".$i."</a>";
		  }
		  $listapgg .= " ";
		}
		if (($p_corrente+1) <= $pgg)
		{
		  $listapgg .= "<a href=\"javascript:post_to_url('".$SELF."-pagina-".($p_corrente+1).".html','".$categoria."')\">></a> ";
		}
		if (($p_corrente != $pgg) && ($pgg != 0))
		{
		  $listapgg .= "<a href=\"javascript:post_to_url('".$SELF."-pagina-".$pgg.".html','".$categoria."')\">Ultima pag.</a> ";
		}
		$listapgg .= "</td>\n";
		return $listapgg;
	}
  }

  // permettiamo la navigazione per pagine precedenti e successive
  function precedenteSuccessiva($p_corrente, $pgg,$SELF,$categoria)
  {
	if($categoria == ''){
		$impaginazione = "";
		if (($p_corrente-1) <= 0)
		{
		  //$impaginazione .= "Precedente";
		}else{
		  //$impaginazione .= "<a href=\"".$SELF."-pagina-".($p_corrente-1).".html\">Pag. precedente</a>";
		}
		//$impaginazione .= " | ";
		if (($p_corrente+1) > $pgg)
		{
		  //$impaginazione .= "Prossima";
		}else{
		  //$impaginazione .= "<a href=\"".$SELF."-pagina-".($p_corrente+1).".html\">Prossima pag.</a>";
		}
		return $impaginazione;
	}else{
		$impaginazione = "";
		if (($p_corrente-1) <= 0)
		{
		  $impaginazione .= "Precedente";
		}else{
		  $impaginazione .= "<a href=\"javascript:post_to_url('".$SELF."-pagina-".($p_corrente-1).".html,'".$categoria."')\">Pag. precedente</a>";
		}
		$impaginazione .= " | ";
		if (($p_corrente+1) > $pgg)
		{
		  $impaginazione .= "Prossima";
		}else{
		  $impaginazione .= "<a href=\"javascript:post_to_url('".$SELF."-pagina-".($p_corrente+1).".html,'".$categoria."')\">Prossima pag.</a>";
		}
		return $impaginazione;
	}
  }
}
?>