<?php
class DataManipulation {
	
	
	public $startSettimana1;
	public $stopSettimana1;
	public $startSettimana2;
	public $stopSettimana2;
	public $startSettimana3;
	public $stopSettimana3;
	public $startSettimana4;
	public $startSettimana5;
	public $stopSettimana4;
	public $stopSettimana5;
	
	public $giorno1;
	public $giorno2;
	public $giorno3;
	public $giorno4;
	public $giorno5;
	public $giorno6;
	public $giorno7;
	
	function DataManipulation()
	{
		
		
	}
	
	function getSettimaneMese($m,$y)
	{
		$firstDay  = mktime(0, 0, 0, $m  ,'1',$y);
		$decremento=31;
		while(date("D",$firstDay) != "Mon")
		{
			$mTmp = $m - 1;
			$firstDay  = mktime(0, 0, 0, $mTmp  ,$decremento,$y);
			$decremento=$decremento-1;
		}
		return $firstDay;
		
	}
	
	function getDettaglioSettimanaMese($inizio,$y)
	{
		$arrDate = explode('_',$inizio);
		$firstDay  = mktime(0, 0, 0, $arrDate[1]  ,$arrDate[0],$y);
		return $firstDay;
		
	}
	
	function getOffset($firstDay,$offset,$nSett)
	{
		switch($nSett)
		{
			case 1:
				 $this->startSettimana1 = $firstDay;
				 $this->stopSettimana1 = mktime(0, 0, 0, date("m",$firstDay),date("d",$firstDay)+$offset,date("Y",$firstDay));
			
			break;
			case 2:
				 $this->startSettimana2 = mktime(0, 0, 0, date("m",$firstDay),date("d",$firstDay)+$offset-6,date("Y",$firstDay));
				 $this->stopSettimana2 = mktime(0, 0, 0, date("m",$firstDay),date("d",$firstDay)+$offset,date("Y",$firstDay));
				 
			break;
			case 3:
				 $this->startSettimana3 = mktime(0, 0, 0, date("m",$firstDay),date("d",$firstDay)+$offset-7,date("Y",$firstDay));
				 $this->stopSettimana3 = mktime(0, 0, 0, date("m",$firstDay),date("d",$firstDay)+$offset,date("Y",$firstDay));
				
			break;
			case 4:
				 $this->startSettimana4 = mktime(0, 0, 0, date("m",$firstDay),date("d",$firstDay)+$offset-7,date("Y",$firstDay));
				 $this->stopSettimana4 = mktime(0, 0, 0, date("m",$firstDay),date("d",$firstDay)+$offset,date("Y",$firstDay));
			
			break;
			case 5:
				 $this->startSettimana5 = mktime(0, 0, 0, date("m",$firstDay),date("d",$firstDay)+$offset-7,date("Y",$firstDay));
				 $this->stopSettimana5 = mktime(0, 0, 0, date("m",$firstDay),date("d",$firstDay)+$offset,date("Y",$firstDay));
				 break;
		}
		return mktime(0, 0, 0, date("m",$firstDay),date("d",$firstDay)+$offset,date("Y",$firstDay));
		
	}
	
	function getOffsetGiorno($firstDay,$offset)
	{
		switch($offset)
		{
			case 0:
				 $this->giorno1 = $firstDay;
			break;
			case 1:
				 $this->giorno2 = mktime(0, 0, 0, date("m",$firstDay),date("d",$firstDay)+$offset,date("Y",$firstDay));
			break;
			case 2:
			 	$this->giorno3 = mktime(0, 0, 0, date("m",$firstDay),date("d",$firstDay)+$offset,date("Y",$firstDay));
			break;
			case 3:
				 $this->giorno4 = mktime(0, 0, 0, date("m",$firstDay),date("d",$firstDay)+$offset,date("Y",$firstDay));
			break;
			case 4:
				 $this->giorno5 = mktime(0, 0, 0, date("m",$firstDay),date("d",$firstDay)+$offset,date("Y",$firstDay));
			break;
			case 5:
				 $this->giorno6 = mktime(0, 0, 0, date("m",$firstDay),date("d",$firstDay)+$offset,date("Y",$firstDay));
			break;
			case 6:
				 $this->giorno7 = mktime(0, 0, 0, date("m",$firstDay),date("d",$firstDay)+$offset,date("Y",$firstDay));
			break;
		}
		return mktime(0, 0, 0, date("m",$firstDay),date("d",$firstDay)+$offset,date("Y",$firstDay));
		
	}
}
?>