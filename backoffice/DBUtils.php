<?php
include "../DBAccess.php";
require "../JSON.php"; 

require "../ClassObject/Pagina.php"; 

function getLingue()
{
	$dbInst = new DBAccess();
	
	$dbInst->connOpen();
	$ext = $dbInst->getAllLingue();
	$res = "{ Lingue: [";
	$i=0;
	$countTotal = mysql_num_rows($ext);
	
		while($row =  mysql_fetch_array($ext)) 
		{
			$res = $res."{\"lingua\":\"".$row['nome']."\", \"abbr\":\"".$row['etichetta']."\"}";
			if($i < $countTotal - 1)
				$res =$res.",";
		}	
		
		if ($countTotal > 1)
		{
		$res =  substr ( $res , 0 , strlen($res)-1 );
		}
		
	$res =$res."]}";
	$dbInst->connClose();
	echo $res;
}

function getFlags()
{
	$dbInst = new DBAccess();
	
	$dbInst->connOpen();
	$ext = $ext = $dbInst->getFlags($_POST["idvetrina"]);
	
	$res = "{ Flags: [";
	
	//$res = "{ Flags: [{\"flag\":\"".$_POST["idvetrina"]."\", \"idflag\":\"".$_POST["idvetrina"]."\"}";
	
	$i=0;
	$countTotal = mysql_num_rows($ext);
	
	while($row =  mysql_fetch_array($ext)) 
	{
		$res = $res."{\"flag\":\"".$row['nomeFlag']."\", \"idflag\":\"".$row['idFlag']."\"}";
			
			if($i < $countTotal - 1)
			{
				$res =$res.",";
			}
	}
	
	if ($countTotal > 1)
	{
		$res =  substr ( $res , 0 , strlen($res)-1 );
	}
	
	$res =$res."]}";
	$dbInst->connClose();
	echo $res;
}

function do_upload($tmpFile,$fileToCreate)
{
		$cartella_upload="./Repository/";
		$cartella_upload2="./RepositoryVetrina/";
		
		if(move_uploaded_file($tmpFile, $cartella_upload.$fileToCreate))
		{
						
			list($width, $height, $type, $attr) = getimagesize($cartella_upload.$fileToCreate);

			$thumb = imagecreatetruecolor(200, 120);
			$thumb2 = imagecreatetruecolor(320, 240);
			
			$path_parts = pathinfo($fileToCreate); 			
			if($path_parts['extension'] == "png"){
				imagealphablending($thumb, false); 
				imagesavealpha($thumb, true);
				$source = imagecreatefrompng($cartella_upload.$fileToCreate);
				imagealphablending($source, true);
				imagecopyresampled($thumb, $source, 0, 0, 0, 0, 200, 120, $width, $height);
				imagepng($thumb,$cartella_upload.$fileToCreate);
			}else{
				$source = imagecreatefromjpeg($cartella_upload.$fileToCreate);
				imagecopyresized($thumb, $source, 0, 0, 0, 0, 200, 120, $width, $height);
				imagejpeg($thumb, $cartella_upload.$fileToCreate, 75);
			}
			
			return  "OK";
		}
		else
		{
			return "KO";
		}
}

function insertNuovaRisorsa()
{
	$dbInst = new DBAccess();
	$dbInst->connOpen();
	
	$fileIt="";
	$fileEn="";
	$fileFr="";
	$fileDe="";
	
	if (isset($_FILES['fileaudioIT']['name']))
	{
		$nameFile =  rand(0 , 10000 ).$_FILES['fileaudioIT']['name'];
		$nameFile = str_replace(" ", "_", $nameFile);
		$nameFile = str_replace("(", "_", $nameFile);
		$nameFile = str_replace(")", "_", $nameFile);
		
		if (do_upload($_FILES['fileaudioIT']['tmp_name'],$nameFile) == "OK")
		{
			$fileIt = $nameFile;
		}
	}
	if (isset($_FILES['fileaudioEN']['name']))
	{	
		$nameFile =  rand(0 , 10000 ).$_FILES['fileaudioEN']['name'];
		$nameFile = str_replace(" ", "_", $nameFile);
		$nameFile = str_replace("(", "_", $nameFile);
		$nameFile = str_replace(")", "_", $nameFile);
		
		if (do_upload($_FILES['fileaudioEN']['tmp_name'],$nameFile) == "OK")
		{
			$fileEn = $nameFile;
		}
	}
	if (isset($_FILES['fileaudioFRA']['name']))
	{	
		$nameFile =  rand(0 , 10000 ).$_FILES['fileaudioFRA']['name'];
		$nameFile = str_replace(" ", "_", $nameFile);
		$nameFile = str_replace("(", "_", $nameFile);
		$nameFile = str_replace(")", "_", $nameFile);
		
		if (do_upload($_FILES['fileaudioFRA']['tmp_name'],$nameFile) == "OK")
		{
			$fileFr =$nameFile;
		}
	}
	
	if (isset($_FILES['fileaudioDEU']['name']))
	{	
		$nameFile =  rand(0 , 10000 ).$_FILES['fileaudioDEU']['name'];
		$nameFile = str_replace(" ", "_", $nameFile);
		$nameFile = str_replace("(", "_", $nameFile);
		$nameFile = str_replace(")", "_", $nameFile);
		
		if (do_upload($_FILES['fileaudioDEU']['tmp_name'],$nameFile) == "OK")
		{
			$fileDe =$nameFile;
		}
	}	
	
	if($_POST["categoria1"] == "true"){
		$categoria1= 1;
	}else{
		$categoria1= 0;
	}
	if($_POST["categoria2"] == "true"){
		$categoria2= 1;
	}else{
		$categoria2= 0;
	}
	if($_POST["categoria3"] == "true"){
		$categoria3= 1;
	}else{
		$categoria3= 0;
	}
	if($_POST["categoria4"] == "true"){
		$categoria4= 1;
	}else{
		$categoria4= 0;
	}
	if($_POST["categoria5"] == "true"){
		$categoria5= 1;
	}else{
		$categoria5= 0;
	}
	if($_POST["categoria6"] == "true"){
		$categoria6= 1;
	}else{
		$categoria6= 0;
	}
	
	if($_POST["txtAcquisto"] == "true"){
		$acquisto= 1;
	}else{
		$acquisto= 0;
	}
	if($_POST["txtInfo"] == "true"){
		$info= 1;
	}else{
		$info= 0;
	}
	if($_POST["txtNascondiDurata"] == "true"){
		$nascondiDurata= 1;
	}else{
		$nascondiDurata= 0;
	}
	
	$idRisMax = $dbInst->insertOffertaGirosalento($_POST["idvetrina"],$_POST["pulsante"],
	$_POST["txtTitoloIT"],$_POST["txtTitoloEN"],$_POST["txtTitoloFR"],$_POST["txtTitoloDE"],
	$_POST["txtTitoloES"],$_POST["txtTitoloRU"],$_POST["txtTitoloCE"],$_POST["txtTitoloCH"],
	$_POST["txtTitle"],$_POST["txtDescription"],$_POST["txtKeywords"],
	$_POST["txtDescrizioneIT"],$_POST["txtDescrizioneEN"],$_POST["txtDescrizioneFR"],$_POST["txtDescrizioneDE"],
	$_POST["txtDescrizioneES"],$_POST["txtDescrizioneRU"],$_POST["txtDescrizioneCE"],$_POST["txtDescrizioneCH"],
	$_POST["txtPresentazioneIT"],$_POST["txtPresentazioneEN"],$_POST["txtPresentazioneFR"],$_POST["txtPresentazioneDE"],
	$_POST["txtPresentazioneES"],$_POST["txtPresentazioneRU"],$_POST["txtPresentazioneCE"],$_POST["txtPresentazioneCH"],
	$_POST["txtTrascrizioneIT"],$_POST["txtTrascrizioneEN"],$_POST["txtTrascrizioneFR"],$_POST["txtTrascrizioneDE"],
	$_POST["txtTrascrizioneES"],$_POST["txtTrascrizioneRU"],$_POST["txtTrascrizioneCE"],$_POST["txtTrascrizioneCH"],
	$_POST["txtInizioVisibilita"],$_POST["txtFineVisibilita"],$_POST["txtInizioValidita"],$_POST["txtFineValidita"],$_POST["txtGiorni"],
	$_POST["txtLinkAudioIT"],$_POST["txtLinkAudioEN"],$_POST["txtLinkAudioFR"],$_POST["txtLinkAudioDE"],
	$fileIt,$fileEn,$fileFr,$fileFr,$_POST["tipo"],$_POST["flags"],$_POST["totalFlags"],$_POST["txtTrascrizioneIT2"],$_POST["txtTrascrizioneIT3"],
	$categoria1,$categoria2,$categoria3,$categoria4,$categoria5,$categoria6,$acquisto,$info,$_POST["txtNumeroNotti"],
	$_POST["titoloExtra1IT"],$_POST["titoloExtra2IT"],$_POST["titoloExtra3IT"],
	$_POST["titoloExtra1EN"],$_POST["titoloExtra2EN"],$_POST["titoloExtra3EN"],$_POST["destinazioneStatica"],
	$_POST["descrizionePhoneIT"],$_POST["descrizionePhoneEN"],$nascondiDurata,$_POST["fotoMappa"],
	$_POST["txtDescrizioneMobileIT"],$_POST["txtDescrizioneMobileEN"],$_POST["txtDescrizioneMobileFR"],$_POST["txtDescrizioneMobileDE"],
	$_POST["txtDescrizioneMobileES"],$_POST["txtDescrizioneMobileRU"],$_POST["txtDescrizioneMobileCE"],$_POST["txtDescrizioneMobileCH"]);
	
	
	//contenuti
	$pieces = explode("|", $_POST["totPathFoto"]);
	$piecesNome = explode("|", $_POST["totNomeFoto"]);
	$piecesDescrizione = explode("|", $_POST["totDescrizioneFoto"]);
	for($i=0;$i<count($pieces);$i++){
		if($pieces[$i] != ""){
			$ext2 = $dbInst->insertContenutoRisorsa($idRisMax,$pieces[$i],$piecesNome[$i],$piecesDescrizione[$i],'foto');
		}
	}
	
	$pieces = explode("|", $_POST["totPathVideo"]);
	$piecesNome = explode("|", $_POST["totNomeVideo"]);
	$piecesDescrizione = explode("|", $_POST["totDescrizioneVideo"]);
	for($i=0;$i<count($pieces);$i++){
		if($pieces[$i] != ""){
			$ext2 = $dbInst->insertContenutoRisorsa($idRisMax,$pieces[$i],$piecesNome[$i],$piecesDescrizione[$i],'video');
		}
	}
	
	$pieces = explode("|", $_POST["totPathAllegato"]);
	$piecesNome = explode("|", $_POST["totNomeAllegato"]);
	$piecesDescrizione = explode("|", $_POST["totDescrizioneAllegato"]);
	for($i=0;$i<count($pieces);$i++){
		if($pieces[$i] != ""){
			$ext2 = $dbInst->insertContenutoRisorsa($idRisMax,$pieces[$i],$piecesNome[$i],$piecesDescrizione[$i],'allegato');
		}
	}
	//fine contenuti
	
	
	$fotoAssociate = $_POST["totPathRisorse"];	
	$pieces = explode("|", $fotoAssociate);
	for($i=0;$i<count($pieces);$i++){
		if($pieces[$i] != ""){
			$ext2 = $dbInst->insertFotoProdotto($pieces[$i]);
		}
	}
	
	$videoAssociati = $_POST["totPathRisorseVideo"];	
	$piecesVid = explode("|", $videoAssociati);
	for($i=0;$i<count($piecesVid);$i++){
		if($piecesVid[$i] != ""){
			$ext2 = $dbInst->insertVideoDestinazione($piecesVid[$i]);
		}
	}
	
	$vetrina = $_POST["idvetrina"];
	$destinazioniAssociate = $_POST["totIdDestinazioni"];	
	$sigleAssociate = $_POST["totSigle"];	
	$pieces = explode("|", $destinazioniAssociate);
	$piecesSigle = explode("|", $sigleAssociate);
	for($i=0;$i<count($pieces);$i++){
		if($pieces[$i] != ""){
			$ext2 = $dbInst->insertRelDestinazioneRisorsa($pieces[$i],$piecesSigle[$i],$vetrina);
		}
	}
	
	$serviziAssociati = $_POST["totIdServizi"];	
	$pieces = explode("|", $serviziAssociati);
	for($i=0;$i<count($pieces);$i++){
		if($pieces[$i] != ""){
			$ext2 = $dbInst->insertRelServizioRisorsa($pieces[$i],$vetrina);
		}
	}
	
	$totDataPartenza = $_POST["totDataPartenza"];	
	$totDataRitorno = $_POST["totDataRitorno"];	
	$totPrezzoAdulti = $_POST["totPrezzoAdulti"];	
	$totPrezzoBambini = $_POST["totPrezzoBambini"];	
	$totPrezzoInfanti = $_POST["totPrezzoInfanti"];	
	$pieces = explode("|", $totDataPartenza);
	$pieces2 = explode("|", $totDataRitorno);
	$pieces3 = explode("|", $totPrezzoAdulti);
	$pieces4 = explode("|", $totPrezzoBambini);
	$pieces5 = explode("|", $totPrezzoInfanti);
	for($i=0;$i<count($pieces);$i++){
		if($pieces[$i] != ""){
			$ext2 = $dbInst->insertRelDataRisorsa($pieces[$i],$pieces2[$i],$pieces3[$i],$pieces4[$i],$pieces5[$i]);
		}
	}
	
	$dbInst->connClose();
	
	echo "{success:true, result:\"".$ext."\"}";
}

function updateRisorsa()
{
	$dbInst = new DBAccess();
	$dbInst->connOpen();
	
	$fileIt=$_POST["lblFileITValue"];
	$fileEn=$_POST["lblFileENValue"];
	$fileFr=$_POST["lblFileFRValue"];
	$fileFr=$_POST["lblFileDEValue"];
	
	if (isset($_FILES['fileaudioIT']['name']))
	{
		if (do_upload($_FILES['fileaudioIT']['tmp_name'],$_FILES['fileaudioIT']['name']) == "OK")
		{
			$fileIt = $_FILES['fileaudioIT']['name'];
		}
	}
	if (isset($_FILES['fileaudioEN']['name']))
	{	
		if (do_upload($_FILES['fileaudioEN']['tmp_name'],$_FILES['fileaudioEN']['name']) == "OK")
		{
			$fileEn = $_FILES['fileaudioEN']['name'];
		}
	}
	if (isset($_FILES['fileaudioFRA']['name']))
	{	
		if (do_upload($_FILES['fileaudioFRA']['tmp_name'],$_FILES['fileaudioFRA']['name']) == "OK")
		{
			$fileFr = $_FILES['fileaudioFRA']['name'];
		}
	}
	
	if (isset($_FILES['fileaudioDEU']['name']))
	{	
		if (do_upload($_FILES['fileaudioDE']['tmp_name'],$_FILES['fileaudioDE']['name']) == "OK")
		{
			$fileFr = $_FILES['fileaudioDE']['name'];
		}
	}
	
	if($_POST["categoria1"] == "true"){
		$categoria1= 1;
	}else{
		$categoria1= 0;
	}
	if($_POST["categoria2"] == "true"){
		$categoria2= 1;
	}else{
		$categoria2= 0;
	}
	if($_POST["categoria3"] == "true"){
		$categoria3= 1;
	}else{
		$categoria3= 0;
	}
	if($_POST["categoria4"] == "true"){
		$categoria4= 1;
	}else{
		$categoria4= 0;
	}
	if($_POST["categoria5"] == "true"){
		$categoria5= 1;
	}else{
		$categoria5= 0;
	}
	if($_POST["categoria6"] == "true"){
		$categoria6= 1;
	}else{
		$categoria6= 0;
	}
	
	if($_POST["txtAcquisto"] == "true"){
		$acquisto= 1;
	}else{
		$acquisto= 0;
	}
	if($_POST["txtInfo"] == "true"){
		$info= 1;
	}else{
		$info= 0;
	}
	if($_POST["txtNascondiDurata"] == "true"){
		$nascondiDurata= 1;
	}else{
		$nascondiDurata= 0;
	}
	
	$ext = $dbInst->updateOffertaGirosalento($_POST["idrisorsa"],$_POST["pulsante"],$_POST["txtTitoloIT"],$_POST["txtTitoloEN"],$_POST["txtTitoloFR"],$_POST["txtTitoloDE"],
	$_POST["txtTitoloES"],$_POST["txtTitoloRU"],$_POST["txtTitoloCE"],$_POST["txtTitoloCH"],
	$_POST["txtTitle"],$_POST["txtDescription"],$_POST["txtKeywords"],
	$_POST["txtDescrizioneIT"],$_POST["txtDescrizioneEN"],$_POST["txtDescrizioneFR"],$_POST["txtDescrizioneDE"],
	$_POST["txtDescrizioneES"],$_POST["txtDescrizioneRU"],$_POST["txtDescrizioneCE"],$_POST["txtDescrizioneCH"],
	$_POST["txtPresentazioneIT"],$_POST["txtPresentazioneEN"],$_POST["txtPresentazioneFR"],$_POST["txtPresentazioneDE"],
	$_POST["txtPresentazioneES"],$_POST["txtPresentazioneRU"],$_POST["txtPresentazioneCE"],$_POST["txtPresentazioneCH"],
	$_POST["txtTrascrizioneIT"],$_POST["txtTrascrizioneEN"],$_POST["txtTrascrizioneFR"],$_POST["txtTrascrizioneDE"],
	$_POST["txtTrascrizioneES"],$_POST["txtTrascrizioneRU"],$_POST["txtTrascrizioneCE"],$_POST["txtTrascrizioneCH"],
	$_POST["txtInizioVisibilita"],$_POST["txtFineVisibilita"],$_POST["txtInizioValidita"],$_POST["txtFineValidita"],$_POST["txtGiorni"],
	$_POST["txtLinkAudioIT"],$_POST["txtLinkAudioEN"],$_POST["txtLinkAudioFR"],$_POST["txtLinkAudioDE"],
	$fileIt,$fileEn,$fileFr,$fileFr,$_POST["tipo"],$_POST["flags"],$_POST["totalFlags"],$_POST["txtTrascrizioneIT2"],$_POST["txtTrascrizioneIT3"],
	$categoria1,$categoria2,$categoria3,$categoria4,$categoria5,$categoria6,$acquisto,$info,$_POST["txtNumeroNotti"],
	$_POST["titoloExtra1IT"],$_POST["titoloExtra2IT"],$_POST["titoloExtra3IT"],
	$_POST["titoloExtra1EN"],$_POST["titoloExtra2EN"],$_POST["titoloExtra3EN"],$_POST["destinazioneStatica"],
	$_POST["descrizionePhoneIT"],$_POST["descrizionePhoneEN"],$nascondiDurata
	,$_POST["fotoMappa"],
	$_POST["txtDescrizioneMobileIT"],$_POST["txtDescrizioneMobileEN"],$_POST["txtDescrizioneMobileFR"],$_POST["txtDescrizioneMobileDE"],
	$_POST["txtDescrizioneMobileES"],$_POST["txtDescrizioneMobileRU"],$_POST["txtDescrizioneMobileCE"],$_POST["txtDescrizioneMobileCH"]);
	
	$extDel = $dbInst->deleteFoto($_POST["idrisorsa"]);	
	$fotoAssociate = $_POST["totPathRisorse"];	
	$pieces = explode("|", $fotoAssociate);
	for($i=0;$i<count($pieces);$i++){	
		if($pieces[$i] != ""){
			$ext2 = $dbInst->updateFotoProdotto($_POST["idrisorsa"],$pieces[$i]);
		}
	}
	
	$extDel = $dbInst->deleteVideoDestinazione($_POST["idRisorsa"]);	
	$videoAssociati = $_POST["totPathRisorseVideo"];	
	$piecesVid = explode("|", $videoAssociati);	
	for($i=0;$i<count($piecesVid);$i++){	
		if($piecesVid[$i] != ""){
			$ext2 = $dbInst->updateVideoDestinazione($_POST["idRisorsa"],$piecesVid[$i]);
		}
	}
	
	
	$vetrina = $_POST["idvetrina"];
	
	$extDel = $dbInst->deleteAssDestinazioni($_POST["idrisorsa"]);	
	$destinazioniAssociate = $_POST["totIdDestinazioni"];	
	$sigleAssociate = $_POST["totSigle"];	
	$piecesSigle = explode("|", $sigleAssociate);
	$pieces = explode("|", $destinazioniAssociate);
	for($i=0;$i<count($pieces);$i++){
		if($pieces[$i] != ""){
			$ext2 = $dbInst->updateRelDestinazioneRisorsa($pieces[$i],$piecesSigle[$i],$vetrina,$_POST["idrisorsa"]);
		}
	}
	
	$extDel = $dbInst->deleteAssServizi($_POST["idrisorsa"]);	
	$serviziAssociati = $_POST["totIdServizi"];	
	$pieces = explode("|", $serviziAssociati);
	for($i=0;$i<count($pieces);$i++){
		if($pieces[$i] != ""){
			$ext2 = $dbInst->updateRelServizioRisorsa($pieces[$i],$vetrina,$_POST["idrisorsa"]);
		}
	}
	
	$extDel = $dbInst->deleteAssDate($_POST["idrisorsa"]);	
	$totDataPartenza = $_POST["totDataPartenza"];	
	$totDataRitorno = $_POST["totDataRitorno"];	
	$totPrezzoAdulti = $_POST["totPrezzoAdulti"];	
	$totPrezzoBambini = $_POST["totPrezzoBambini"];	
	$totPrezzoInfanti = $_POST["totPrezzoInfanti"];	
	$pieces = explode("|", $totDataPartenza);
	$pieces2 = explode("|", $totDataRitorno);
	$pieces3 = explode("|", $totPrezzoAdulti);
	$pieces4 = explode("|", $totPrezzoBambini);
	$pieces5 = explode("|", $totPrezzoInfanti);
	for($i=0;$i<count($pieces);$i++){
		if($pieces[$i] != ""){
			$ext2 = $dbInst->updateRelDataRisorsa($_POST["idrisorsa"],$pieces[$i],$pieces2[$i],$pieces3[$i],$pieces4[$i],$pieces5[$i]);
		}
	}
	
	$dbInst->connClose();
	
	echo "{success:true, result:\"".$_POST["idrisorsa"]."\"}";	
}

function deleteRisorsa()
{
	$dbInst = new DBAccess();
	
	$dbInst->connOpen();
	
	$ext = $dbInst->deleteRisorsa($_POST["idrisorsa"]);
	$dbInst->connClose();
	echo "{success:true, result:\"".$_POST["idrisorsa"]."\"}";
}

function getRisorsaVetrina()
{
	$dbInst = new DBAccess();
	
	$dbInst->connOpen();
	
	$ext = $dbInst->getRisorsaVetrina($_POST["idvetrina"],$_POST["tiporisorsa"]);
	
	$res = "{ Risorsa: [";
	$i=0;
	$countTotal = mysql_num_rows($ext);
	
		while($row =  mysql_fetch_array($ext)) 
		{ 
			$res = $res."{\"idrisorsa\":\"".$row['idRisorsa']."\", \"path\":\"".$row['Path']."\", \"titoloit\":\"".addslashes($row['titoloit'])."\", \"flags\":[";
			
			$extFlags = $dbInst->getFlagsRisorsa($row['idRisorsa']);
			$countFlags = mysql_num_rows($extFlags);
			
			while($rowFlags =  mysql_fetch_array($extFlags )) 
			{
				$res = $res."{\"idflag\":\"".$rowFlags['flag_idFlag']."\", \"valore\":\"".$rowFlags['valore']."\"}";
				
				if($i < $countFlags - 1) $res =$res.",";
			}
			
			$res =$res."]}";
			
			if($i < $countTotal - 1) $res =$res.",";
		}	
		
		if ($countTotal > 1)
		{
			$res =  substr ( $res , 0 , strlen($res)-1 );
		}
		
	$res =$res."]}";
	
	$dbInst->connClose();
	
	echo $res;
}

function getRisorsaDetails()
{
	$dbInst = new DBAccess();
	
	$dbInst->connOpen();
	
	$ext = $dbInst->getOffertaGirosalentoDettaglio($_POST["idrisorsa"]);
	
	$i=0;
	$countTotal = mysql_num_rows($ext[0]);
	
	$res = "{ Risorsa: [";
	
	while($row =  mysql_fetch_array($ext[0])) 
	{ 
		$res = $res."{\"idrisorsa\":\"".$_POST["idrisorsa"]."\", \"path\":\"".$row['Path']."\", \"tipo\":\"".$row['Tipo']."\"
		
		, \"pulsante\":\"".$row['pagamento']."\"
		
		, \"titoloIT\":\"".addslashes($row['titoloIT'])."\"
		, \"titoloEN\":\"".$row['titoloEN']."\"
		, \"titoloFR\":\"".$row['titoloFR']."\"
		, \"titoloDE\":\"".$row['titoloDE']."\"
		, \"titoloES\":\"".$row['titoloES']."\"
		, \"titoloRU\":\"".$row['titoloRU']."\"
		, \"titoloCE\":\"".$row['titoloCE']."\"
		, \"titoloCH\":\"".$row['titoloCH']."\"
		, \"descrIT\":\"".strtr($row['descrIT'],   array("\r\n" => ' ', "\r" => ' ', "\n" => ' '))."\"
		, \"descrEN\":\"".strtr($row['descrEN'],   array("\r\n" => ' ', "\r" => ' ', "\n" => ' '))."\"
		, \"descrFR\":\"".strtr($row['descrFR'],   array("\r\n" => ' ', "\r" => ' ', "\n" => ' '))."\"
		, \"descrDE\":\"".strtr($row['descrDE'],   array("\r\n" => ' ', "\r" => ' ', "\n" => ' '))."\"
		, \"descrES\":\"".strtr($row['descrES'],   array("\r\n" => ' ', "\r" => ' ', "\n" => ' '))."\"
		, \"descrRU\":\"".strtr($row['descrRU'],   array("\r\n" => ' ', "\r" => ' ', "\n" => ' '))."\"
		, \"descrCE\":\"".strtr($row['descrCE'],   array("\r\n" => ' ', "\r" => ' ', "\n" => ' '))."\"
		, \"descrCH\":\"".strtr($row['descrCH'],   array("\r\n" => ' ', "\r" => ' ', "\n" => ' '))."\"		
		
		, \"descrMobIT\":\"".strtr($row['descrMobIT'],   array("\r\n" => ' ', "\r" => ' ', "\n" => ' '))."\"
		, \"descrMobEN\":\"".strtr($row['descrMobEN'],   array("\r\n" => ' ', "\r" => ' ', "\n" => ' '))."\"
		, \"descrMobFR\":\"".strtr($row['descrMobFR'],   array("\r\n" => ' ', "\r" => ' ', "\n" => ' '))."\"
		, \"descrMobDE\":\"".strtr($row['descrMobDE'],   array("\r\n" => ' ', "\r" => ' ', "\n" => ' '))."\"
		, \"descrMobES\":\"".strtr($row['descrMobES'],   array("\r\n" => ' ', "\r" => ' ', "\n" => ' '))."\"
		, \"descrMobRU\":\"".strtr($row['descrMobRU'],   array("\r\n" => ' ', "\r" => ' ', "\n" => ' '))."\"
		, \"descrMobCE\":\"".strtr($row['descrMobCE'],   array("\r\n" => ' ', "\r" => ' ', "\n" => ' '))."\"
		, \"descrMobCH\":\"".strtr($row['descrMobCH'],   array("\r\n" => ' ', "\r" => ' ', "\n" => ' '))."\"	
		
		, \"presIT\":\"".strtr(addslashes($row['presIT']), array("\r\n" => ' ', "\r" => ' ', "\n" => ' '))."\"
		, \"presEN\":\"".strtr($row['presEN'], array("\r\n" => ' ', "\r" => ' ', "\n" => ' '))."\"
		, \"presFR\":\"".strtr($row['presFR'], array("\r\n" => ' ', "\r" => ' ', "\n" => ' '))."\"
		, \"presDE\":\"".strtr($row['presDE'], array("\r\n" => ' ', "\r" => ' ', "\n" => ' '))."\"
		, \"presES\":\"".strtr($row['presES'], array("\r\n" => ' ', "\r" => ' ', "\n" => ' '))."\"
		, \"presRU\":\"".strtr($row['presRU'], array("\r\n" => ' ', "\r" => ' ', "\n" => ' '))."\"
		, \"presCE\":\"".strtr($row['presCE'], array("\r\n" => ' ', "\r" => ' ', "\n" => ' '))."\"
		, \"presCH\":\"".strtr($row['presCH'], array("\r\n" => ' ', "\r" => ' ', "\n" => ' '))."\"		
		, \"trascrIT\":\"".strtr($row['trascrIT'], array("\r\n" => ' ', "\r" => ' ', "\n" => ' '))."\"
		, \"trascrEN\":\"".strtr($row['trascrEN'], array("\r\n" => ' ', "\r" => ' ', "\n" => ' '))."\"
		, \"trascrFR\":\"".strtr($row['trascrFR'], array("\r\n" => ' ', "\r" => ' ', "\n" => ' '))."\"
		, \"trascrDE\":\"".strtr($row['trascrDE'], array("\r\n" => ' ', "\r" => ' ', "\n" => ' '))."\"
		, \"trascrES\":\"".strtr($row['trascrES'], array("\r\n" => ' ', "\r" => ' ', "\n" => ' '))."\"
		, \"trascrRU\":\"".strtr($row['trascrRU'], array("\r\n" => ' ', "\r" => ' ', "\n" => ' '))."\"
		, \"trascrCE\":\"".strtr($row['trascrCE'], array("\r\n" => ' ', "\r" => ' ', "\n" => ' '))."\"
		, \"trascrCH\":\"".strtr($row['trascrCH'], array("\r\n" => ' ', "\r" => ' ', "\n" => ' '))."\"
			
		, \"titoloExtra1IT\":\"".strtr(addslashes($row['titoloExtra1IT']), array("\r\n" => ' ', "\r" => ' ', "\n" => ' '))."\"
		, \"titoloExtra2IT\":\"".strtr(addslashes($row['titoloExtra2IT']), array("\r\n" => ' ', "\r" => ' ', "\n" => ' '))."\"
		, \"titoloExtra3IT\":\"".strtr(addslashes($row['titoloExtra3IT']), array("\r\n" => ' ', "\r" => ' ', "\n" => ' '))."\"
		, \"titoloExtra1EN\":\"".strtr(addslashes($row['titoloExtra1EN']), array("\r\n" => ' ', "\r" => ' ', "\n" => ' '))."\"
		, \"titoloExtra2EN\":\"".strtr(addslashes($row['titoloExtra2EN']), array("\r\n" => ' ', "\r" => ' ', "\n" => ' '))."\"
		, \"titoloExtra3EN\":\"".strtr(addslashes($row['titoloExtra3EN']), array("\r\n" => ' ', "\r" => ' ', "\n" => ' '))."\"
		, \"descrizionePhoneIT\":\"".strtr(addslashes($row['descrizionePhoneIT']), array("\r\n" => ' ', "\r" => ' ', "\n" => ' '))."\"
		, \"descrizionePhoneEN\":\"".strtr(addslashes($row['descrizionePhoneEN']), array("\r\n" => ' ', "\r" => ' ', "\n" => ' '))."\"
		, \"destinazioneStatica\":\"".strtr(addslashes($row['destinazioneStatica']), array("\r\n" => ' ', "\r" => ' ', "\n" => ' '))."\"	
			
		, \"bambino\":\"".$row['bambino']."\"
		, \"infante\":\"".$row['infante']."\"	
		, \"acquisto\":\"".$row['acquisto']."\"
		, \"info\":\"".$row['info']."\"
		, \"notti\":\"".$row['notti']."\"
		, \"nascondiDurata\":\"".$row['nascondiDurata']."\"
		
		, \"immagineMappa\":\"".$row['immagineMappa']."\"
			
		, \"categoria1\":\"".$row['categoria1']."\"
		, \"categoria2\":\"".$row['categoria2']."\"
		, \"categoria3\":\"".$row['categoria3']."\"
		, \"categoria4\":\"".$row['categoria4']."\"
		, \"categoria5\":\"".$row['categoria5']."\"	
		, \"categoria6\":\"".$row['categoria6']."\"	
		
		, \"inizioValidita\":\"".strtr($row['inizioValidita'], array("\r\n" => ' ', "\r" => ' ', "\n" => ' '))."\"
		, \"fineValidita\":\"".strtr($row['fineValidita'], array("\r\n" => ' ', "\r" => ' ', "\n" => ' '))."\"
		, \"inizioVisibilita\":\"".strtr($row['inizioVisibilita'], array("\r\n" => ' ', "\r" => ' ', "\n" => ' '))."\"
		, \"fineVisibilita\":\"".strtr($row['fineVisibilita'], array("\r\n" => ' ', "\r" => ' ', "\n" => ' '))."\"
		, \"giorni\":\"".strtr($row['giorni'], array("\r\n" => ' ', "\r" => ' ', "\n" => ' '))."\"

		, \"Title\":\"".$row['title']."\"
		, \"Description\":\"".$row['description']."\"
		, \"Keywords\":\"".$row['keywords']."\"		
		
		, \"fileIT\":\"".$row['fileIT']."\"
		, \"fileEN\":\"".$row['fileEN']."\"
		, \"fileFR\":\"".$row['fileFR']."\"
		, \"fileDE\":\"".$row['fileDE']."\"
		, \"linkIT\":\"".$row['linkIT']."\"
		, \"linkEN\":\"".$row['linkEN']."\"
		, \"linkFR\":\"".$row['linkFR']."\"";
		//, \"linkFR\":\"".$row['linkFR']."\"}";
			
		if($i < $countTotal - 1) $res=$res.",";
	}
	
	$res=$res.", \"flags\":[";
	
	//$res=$res.", \"flag1\":\"0\"";
	
	$countFlags = mysql_num_rows($ext[1]);
	
	$i = 0;
			
	while($rowFlags =  mysql_fetch_array($ext[1])) 
	{
		$res = $res."{\"idflag\":\"".$rowFlags['flag_idFlag']."\", \"valore\":\"".$rowFlags['valore']."\"}";
		
		$i = $i + 1;
				
		if($i < $countFlags) $res =$res.",";
	}
			
	$res =$res."]}";
			
	$res=$res."]}";
	
	$dbInst->connClose();
	
	echo $res;
}

function getAllRisorse()
{
	$dbInst = new DBAccess();
	
	$dbInst->connOpen();
	
	$ext = $dbInst->getAllRisorse($_POST["idvetrina"],$_POST["lang"]);

	$res = "{ Risorsa: [";
			
	$i=0;
	
	$numTot = mysql_num_rows($ext);
	
	$arrayPath[0] = '';
					
	if ($numTot > 0)
	{
		$title = '';
		$descr = '';
			
		while($row =  mysql_fetch_array($ext)) 
		{
			
			$path = '';
			
			if ($row['campo'] =='Titolo')
			{
				if ($row['valore'] !='') $title = $row['valore'];
			}
			
			if ($row['campo'] =='Descrizione')
			{
				if ($row['valore'] !='') $descr = $row['valore'];
			}
					
			if ($row['campo'] =='Link')
			{
				if ($row['valore'] !='') $path = $row['valore'];
			}
			if ($row['campo'] =='File')
			{
				if ($row['valore'] !='') 
				{
					$path = $row['valore'];
				}
				else
				{
					$path = $row['absPath'];
				}
			}
					
			if ($path != '')
			{
				$res = $res."{\"path\":\"".$path."\", \"titolo\":\"".$title."\", \"descr\":\"".$descr."\"}";
				
				$title = '';
				$descr = '';
			
				if($i < $numTot - 1)
				{
					$res =$res.",";
				}
			}
		}
		
		if ($numTot > 1)
		{
			$res =  substr ( $res , 0 , strlen($res)-1 );
		}
	}
	
	$res =$res."]}";
	
	$dbInst->connClose();
	
	echo $res;
}

function uploadFile()
{
	if (isset($_FILES['fileFotogallery']['name']))
	{
		$nameFile =  rand(0 , 10000 ).str_replace(" ", "_", $_FILES['fileFotogallery']['name']);	
		if (do_upload($_FILES['fileFotogallery']['tmp_name'],$nameFile) == "OK")
		{
			$fileIt = $nameFile;
		}
	}
	echo "{success:true, result:\"".$nameFile."\"}";
}

function uploadFileContenuto()
{
	if (isset($_FILES['fileUploadedExtra']['name']))
	{
		$nameFile =  rand(0 , 10000 ).str_replace(" ", "_", $_FILES['fileUploadedExtra']['name']);	
		if (do_uploadFotoContenuto($_FILES['fileUploadedExtra']['tmp_name'],$nameFile) == "OK")
		{
			$fileIt = $nameFile;
		}
	}
	echo "{success:true, result:\"".$nameFile."\"}";
}

function getFotogallery()
{
	$dbInst = new DBAccess();
	$dbInst->connOpen();	
	$idRisorsa = $_POST["idRisorsa"];
	$ext = $dbInst->getFotogallery($idRisorsa);
	$countTotal = mysql_num_rows($ext);	
	if($countTotal==0){
		echo '{ Fotogallery: []}';
	}else{	
		$res = "{ Fotogallery: [";
		$i=0;	
			while($row =  mysql_fetch_array($ext)) 
			{
				$res = $res."{\"idFoto\":\"".$row['risorsa_idRisorsa']."\", \"nomeFoto\":\"".$row['path']."\"}";
				if($i < $countTotal - 1)
					$res =$res.",";
			}			
			if ($countTotal > 1)
			{
			$res =  substr ( $res , 0 , strlen($res)-1 );
			}				
		$res =$res."]}";
		$dbInst->connClose();
		echo $res;
	}
}

//servizi

function getAllServizi()
{
	$dbInst = new DBAccess();
	
	$dbInst->connOpen();
	$ext = $dbInst->getAllServizi();
	$res = "{ Servizi: [";
	$i=0;
	$countTotal = mysql_num_rows($ext);
	while($row =  mysql_fetch_array($ext)) 
		{

			$res = $res."{\"idServizio\":\"".$row['idServizio']."\", \"nome\":\"".$row['nome']."\", \"tipo\":\"".$row['tipologia']."\", \"costo\":\"".$row['costo']."\"}";

			if($i < $countTotal - 1)
				$res =$res.",";
		}		
		if ($countTotal > 1)
		{
		$res =  substr ( $res , 0 , strlen($res)-1 );
		}
		$res =$res."]}";
	$dbInst->connClose();
	echo $res;
}

function getServiziAssociati()
{
	$dbInst = new DBAccess();
	
	$idRisorsa = $_POST["idRisorsa"];
	$idVetrina = $_POST["idvetrina"];
	
	$dbInst->connOpen();
	$ext = $dbInst->getServiziAssociati($idRisorsa,$idVetrina,"IT");
	$res = "{ Servizi: [";
	$i=0;
	$countTotal = mysql_num_rows($ext);
	while($row =  mysql_fetch_array($ext)) 
		{

			$res = $res."{\"idServizio\":\"".$row['idServizio']."\", \"nome\":\"".$row['nomeLingua']."\", \"tipo\":\"".$row['tipologia']."\", \"costo\":\"".$row['costo']."\"}";

			if($i < $countTotal - 1)
				$res =$res.",";
		}		
		if ($countTotal > 1)
		{
		$res =  substr ( $res , 0 , strlen($res)-1 );
		}
		$res =$res."]}";
	$dbInst->connClose();
	echo $res;
}

function getServiziDeassociati()
{
	$dbInst = new DBAccess();
	
	$idRisorsa = $_POST["idRisorsa"];
	$idVetrina = $_POST["idvetrina"];
	
	$dbInst->connOpen();
	$ext = $dbInst->getServiziDeassociati($idRisorsa,$idVetrina);
	$res = "{ Servizi: [";
	$i=0;
	$countTotal = mysql_num_rows($ext);
	while($row =  mysql_fetch_array($ext)) 
		{

			$res = $res."{\"idServizio\":\"".$row['idServizio']."\", \"nome\":\"".$row['nome']."\", \"tipo\":\"".$row['tipologia']."\", \"costo\":\"".$row['costo']."\"}";

			if($i < $countTotal - 1)
				$res =$res.",";
		}		
		if ($countTotal > 1)
		{
		$res =  substr ( $res , 0 , strlen($res)-1 );
		}
		$res =$res."]}";
	$dbInst->connClose();
	echo $res;
}

//destinazioni
function getAllDestinazioni()
{
	$dbInst = new DBAccess();
	$nazione = $_POST["nazione"];
	$dbInst->connOpen();
	$ext = $dbInst->getAllDestinazioniAssociateNazione($nazione);
	$res = "{ Destinazioni: [";
	$i=0;
	$countTotal = mysql_num_rows($ext);
	while($row =  mysql_fetch_array($ext)) 
		{

			$res = $res."{\"idDestinazione\":\"".$row['idDestinazione']."\", \"nome\":\"".$row['Nome']."\", \"nomeNazione\":\"".$row['nomeNazione']."\", \"sigla\":\"".$row['nazione']."\"}";

			if($i < $countTotal - 1)
				$res =$res.",";
		}		
		if ($countTotal > 1)
		{
		$res =  substr ( $res , 0 , strlen($res)-1 );
		}
		$res =$res."]}";
	$dbInst->connClose();
	echo $res;
}

function getAllDestinazioniSearch()
{
	$dbInst = new DBAccess();	
	$nazione = $_POST["nazione"];
	$idRisorsa = $_POST["idrisorsa"];
	$dbInst->connOpen();
	$ext = $dbInst->getAllDestinazioniAssociateNazioneSearch($nazione,$idRisorsa);
	$res = "{ Destinazioni: [";
	$i=0;
	$countTotal = mysql_num_rows($ext);
	while($row =  mysql_fetch_array($ext)) 
		{

			$res = $res."{\"idDestinazione\":\"".$row['idDestinazione']."\", \"nome\":\"".$row['Nome']."\", \"nomeNazione\":\"".$row['nomeNazione']."\", \"sigla\":\"".$row['nazione']."\"}";

			if($i < $countTotal - 1)
				$res =$res.",";
		}		
		if ($countTotal > 1)
		{
		$res =  substr ( $res , 0 , strlen($res)-1 );
		}
		$res =$res."]}";
	$dbInst->connClose();
	echo $res;
}


function getDestinazioniAssociate()
{
	$dbInst = new DBAccess();
	
	$idRisorsa = $_POST["idRisorsa"];
	$idVetrina = $_POST["idvetrina"];
	
	$dbInst->connOpen();
	$ext = $dbInst->getDestinazioniAssociate($idRisorsa,$idVetrina);
	$res = "{ Destinazioni: [";
	$i=0;
	$countTotal = mysql_num_rows($ext);
	while($row =  mysql_fetch_array($ext)) 
		{

			$res = $res."{\"idDestinazione\":\"".$row['idDestinazione']."\", \"nome\":\"".$row['Nome']."\", \"nomeNazione\":\"".$row['nomeNazione']."\", \"sigla\":\"".$row['nazione']."\"}";

			if($i < $countTotal - 1)
				$res =$res.",";
		}		
		if ($countTotal > 1)
		{
		$res =  substr ( $res , 0 , strlen($res)-1 );
		}
		$res =$res."]}";
	$dbInst->connClose();
	echo $res;
}

function getDestinazioniDeasssociate()
{
	$dbInst = new DBAccess();
	
	$idRisorsa = $_POST["idRisorsa"];
	$idVetrina = $_POST["idvetrina"];
	
	$dbInst->connOpen();
	$ext = $dbInst->getDestinazioniDessociate($idRisorsa,$idVetrina);
	$res = "{ Destinazioni: [";
	$i=0;
	$countTotal = mysql_num_rows($ext);
	while($row =  mysql_fetch_array($ext)) 
		{

			$res = $res."{\"idDestinazione\":\"".$row['idDestinazione']."\", \"nome\":\"".$row['Nome']."\", \"nomeNazione\":\"".$row['nomeNazione']."\", \"sigla\":\"".$row['nazione']."\"}";

			if($i < $countTotal - 1)
				$res =$res.",";
		}		
		if ($countTotal > 1)
		{
		$res =  substr ( $res , 0 , strlen($res)-1 );
		}
		$res =$res."]}";
	$dbInst->connClose();
	echo $res;
}

function getAllNazioni()
{
	$dbInst = new DBAccess();
	$dbInst->connOpen();
	$ext = $dbInst->getAllNazioniAssociateDestinazione();
	$res = "{ Nazioni: [";
	$i=0;
	$countTotal = mysql_num_rows($ext);
	while($row =  mysql_fetch_array($ext)) 
		{

			$res = $res."{\"sigla\":\"".$row['sigla']."\", \"nomeit\":\"".$row['nomeit']."\"}";

			if($i < $countTotal - 1)
				$res =$res.",";
		}		
		if ($countTotal > 1)
		{
		$res =  substr ( $res , 0 , strlen($res)-1 );
		}
		$res =$res."]}";
	$dbInst->connClose();
	echo $res;
}

function getYoutube()
{
	$dbInst = new DBAccess();
	$dbInst->connOpen();	
	$idRisorsa = $_POST["idRisorsa"];
	$ext = $dbInst->getYoutube($idRisorsa);
	$countTotal = mysql_num_rows($ext);	
	if($countTotal==0){
		echo '{ Youtube: []}';
	}else{	
		$res = "{ Youtube: [";
		$i=0;	
			while($row =  mysql_fetch_array($ext)) 
			{
				$res = $res."{\"idYoutube\":\"".$row['destinazione_idDestinazione']."\", \"nomeVideo\":\"".$row['path']."\"}";
				if($i < $countTotal - 1)
					$res =$res.",";
			}			
			if ($countTotal > 1)
			{
			$res =  substr ( $res , 0 , strlen($res)-1 );
			}				
		$res =$res."]}";
		$dbInst->connClose();
		echo $res;
	}
}

function getDateAssociate()
{
	$dbInst = new DBAccess();
	$dbInst->connOpen();	
	$idRisorsa = $_POST["idRisorsa"];
	$ext = $dbInst->getDateAssociate($idRisorsa);
	$countTotal = mysql_num_rows($ext);	
	if($countTotal==0){
		echo '{ Date: []}';
	}else{	
		$res = "{ Date: [";
		$i=0;	
			while($row =  mysql_fetch_array($ext)) 
			{
				$res = $res."{\"idData\":\"".$row['idData']."\", \"idRisorsa\":\"".$row['idRisorsa']."\", \"relPartenza\":\"".$row['relPartenza']."\"
				, \"relRitorno\":\"".$row['relRitorno']."\", \"relAdulti\":\"".$row['relAdulti']."\", \"relBambini\":\"".$row['relBambini']."\"
				, \"relInfanti\":\"".$row['relInfanti']."\"
				}";
				if($i < $countTotal - 1)
					$res =$res.",";
			}			
			if ($countTotal > 1)
			{
			$res =  substr ( $res , 0 , strlen($res)-1 );
			}				
		$res =$res."]}";
		$dbInst->connClose();
		echo $res;
	}
}

function do_uploadAllegato($tmpFile,$fileToCreate)
{
		$cartella_upload="./Repository/";
		if(move_uploaded_file($tmpFile, $cartella_upload.$fileToCreate))
		{					
			return  "OK";
		}
		else
		{
			return "KO";
		}
}

function uploadAllegato()
{
	if (isset($_FILES['fileUploadedExtraAllegato']['name']))
	{
		$nameFile =  rand(0 , 10000 ).str_replace(" ", "_", $_FILES['fileUploadedExtraAllegato']['name']);	
		if (do_uploadAllegato($_FILES['fileUploadedExtraAllegato']['tmp_name'],$nameFile) == "OK")
		{
			$fileIt = $nameFile;
		}
	}
	echo "{success:true, result:\"".$nameFile."\"}";
} 

function deleteContenuto()
{
	$dbInst = new DBAccess();
	$path = $_POST["path"];
	$dbInst->connOpen();
	$ext = $dbInst->deleteContenutoRisorsaByPath($path);
	echo 'OK';
}

function insertContenuto()
{
	$dbInst = new DBAccess();
	$dbInst->connOpen();
	$ext2 = $dbInst->insertContenutoRisorsa($_POST["idRisorsa"],$_POST["path"],$_POST["nome"],$_POST["descrizione"],$_POST["tipologia"]);
	$dbInst->connClose();
	echo 'OK';
}

function contenutoPagine()
{
	$dbInst = new DBAccess();
	$dbInst->connOpen();

	$lingua = $_POST["lingua"];
	$idRisorsa = $_POST["idRisorsa"];
	$tipologia = $_POST["tipologia"];
	
	$ext = $dbInst->getContenutoByRisorsaLingua($idRisorsa,$lingua,$tipologia);
	$res = "{ Contenuto: [";
	$i=0;
	$countTotal = mysql_num_rows($ext);
	while($row =  mysql_fetch_array($ext)) 
		{
			$res = $res."{
			\"idContenuto\":\"".$row['idContenuto']."\", \"path\":\"".addslashes($row['path'])."\", \"nome\":\"".addslashes($row['nome'])."\", \"descrizione\":\"".addslashes($row['descrizione'])."\"
			}";
			if($i < $countTotal - 1)
				$res =$res.",";
		}		
		if ($countTotal > 1)
		{
			$res =  substr ( $res , 0 , strlen($res)-1 );
		}
		$res =$res."]}";
	$dbInst->connClose();
	echo $res;
}

function do_uploadFotoContenuto($tmpFile,$fileToCreate)
{
		$cartella_upload="./Repository/";		
		$cartella_upload2="./RepositoryVetrina/";
		if(move_uploaded_file($tmpFile, $cartella_upload.$fileToCreate))
		{
						
			list($width, $height, $type, $attr) = getimagesize($cartella_upload.$fileToCreate);
			$thumb = imagecreatetruecolor(640, 480);
			$thumb2 = imagecreatetruecolor(320, 240);
			
			$path_parts = pathinfo($fileToCreate); 			
			if($path_parts['extension'] == "png"){
				imagealphablending($thumb, false); 
				imagesavealpha($thumb, true);
				$source = imagecreatefrompng($cartella_upload.$fileToCreate);
				imagealphablending($source, true);
				imagecopyresampled($thumb, $source, 0, 0, 0, 0, 640, 480, $width, $height);
				imagepng($thumb,$cartella_upload.$fileToCreate);
			}else{
				$source = imagecreatefromjpeg($cartella_upload.$fileToCreate);
				imagecopyresized($thumb, $source, 0, 0, 0, 0, 640, 480, $width, $height);
				imagejpeg($thumb, $cartella_upload.$fileToCreate, 75);
			}
			return  "OK";
		}
		else
		{
			return "KO";
		}
}

function do_uploadMiniatura($tmpFile,$fileToCreate)
{
		$cartella_upload="./Repository/";		
		$cartella_upload2="./RepositoryVetrina/";
		if(move_uploaded_file($tmpFile, $cartella_upload.$fileToCreate))
		{
						
			list($width, $height, $type, $attr) = getimagesize($cartella_upload.$fileToCreate);
			$thumb = imagecreatetruecolor(980, 545);
			$thumb2 = imagecreatetruecolor(320, 240);
			
			$path_parts = pathinfo($fileToCreate); 			
			if($path_parts['extension'] == "png"){
				imagealphablending($thumb, false); 
				imagesavealpha($thumb, true);
				$source = imagecreatefrompng($cartella_upload.$fileToCreate);
				imagealphablending($source, true);
				imagecopyresampled($thumb, $source, 0, 0, 0, 0, 980, 545, $width, $height);
				imagepng($thumb,$cartella_upload.$fileToCreate);
			}else{
				$source = imagecreatefromjpeg($cartella_upload.$fileToCreate);
				imagecopyresized($thumb, $source, 0, 0, 0, 0, 980, 545, $width, $height);
				imagejpeg($thumb, $cartella_upload.$fileToCreate, 75);
			}
			return  "OK";
		}
		else
		{
			return "KO";
		}
}

function uploadFileMappa()
{
	if (isset($_FILES['fileFotogalleryMappa']['name']))
	{
		$nameFile =  rand(0 , 10000 ).str_replace(" ", "_", $_FILES['fileFotogalleryMappa']['name']);	
		if (do_uploadMiniatura($_FILES['fileFotogalleryMappa']['tmp_name'],$nameFile) == "OK")
		{
			$fileIt = $nameFile;
		}
	}
	echo "{success:true, result:\"".$nameFile."\"}";
}

//retrieve dell'azione da effettuare
$param = $_POST["action"];

switch ($param ) 
{
   case 'getRisorsaVetrina':
   		getRisorsaVetrina();
   		break;
   case 'insertRisorsa':
	   	insertNuovaRisorsa();
	   	break;
   case 'updateRisorsa':
   		updateRisorsa();
   		break;
	case 'deleteRisorsa':
		deleteRisorsa();
		break;
	case 'getRisorsaDetails':
		getRisorsaDetails();
		break;
	case 'lingue':
        getLingue();
        break;
	case 'flags':
        getFlags();
        break;	
	case 'getAllRisorse':
        getAllRisorse();
        break;
	case 'uploadFile':
        uploadFile();
        break;
	case 'getFotogallery':
        getFotogallery();
        break;
	
	//associazioni Youtube
	case 'getYoutube':
        getYoutube();
        break;	
		
	//servizi
	case 'servizi':
       getAllServizi();
       break;	
	case 'serviziAssociati':
       getServiziAssociati();
       break;
	case 'serviziDessociati':
       getServiziDeassociati();
       break;
	   
	// destinazioni
	case 'destinazioni':
       getAllDestinazioni();
       break;
	case 'destinazioniAssociate':
       getDestinazioniAssociate();
       break;
	case 'destinazioniDeassociate':
       getDestinazioniDeasssociate();
       break;
	   
	//nazioni
	case 'nazioni':
       getAllNazioni();
       break;
	   
	//nazioni
	case 'nazioniSearch':
       getAllDestinazioniSearch();
       break;
		
	//date multiple
	case 'getDateAssociate':
        getDateAssociate();
        break;
		
	//contenuto
	case 'insertContenuto':
        insertContenuto();
        break;	
	case 'contenutoPagine':
        contenutoPagine();
        break;
	case 'deleteContenuto':
        deleteContenuto();
        break;
	case 'uploadFileContenuto':
        uploadFileContenuto();
        break;	
	case 'uploadAllegato':
        uploadAllegato();
        break;	
		
	case 'uploadFileMappa':
        uploadFileMappa();
        break;
	
		
}

?>