<!DOCTYPE html>
<html>
  <head>
    <title>Mediasoft Srl</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="HandheldFriendly" content="true" />

    <link href='http://fonts.googleapis.com/css?family=Maven+Pro' rel='stylesheet' type='text/css'>
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
    <link href="navbar.customization.css" rel="stylesheet" media="screen">
    <link href="carousel.customization.css" rel="stylesheet" media="screen">
    <link href="content.customization.css" rel="stylesheet" media="screen">
    <link href="modal.customization.css" rel="stylesheet" media="screen">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

    <style>

	    body {
	    	padding: 0px;
	    	margin: 0px;
	    	font-family: 'Maven Pro';
	    	font-size: 13px;
	    	font-weight:500;
	    }


		#inner-footer {

			padding-left:10px; 
			padding-right:10px; 
			border-color: white; 
			border-left:solid 3px; 
			display: none;

		}

		@media (max-width: 767px) {

			#inner-footer {

				display: block !important;

			}

		}

		


    </style>
  </head>

  <body>



  	<div class="modal fade" id="modalContentWindow" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" tabindex="-1">
	  <div class="modal-dialog">
	    <div class="modal-content">
	    	
	      <div class="modal-header">
	       
	        <span class="glyphicon glyphicon-remove-circle close" onclick="$('#modalContentWindow').modal('hide');" style="margin:30px 10px 0px 0px;"></span>
	        <h4 class="modal-title" id="modalContentWindowTitle">HIGHTLIGHT</h4>
	      </div>
	
	      <div class="modal-body">
	      	
	      		<div class="row" id="modalContentWindowRow">

<!--
					<div class="col-sm-12 col-md-4 col-lg-4 col-full-height">
						<div class="thumbnail">
							<div class="modal-image-container" style="background-image:url('images/IT.jpg');"></div>

							<div class="caption">     
								<h5>PRODOTTO 2</h5>
								<h6>Lorem ipsum dolor sit amet, consectetur adipisici elit, sed eiusmod tempor incidunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquid ex ea commodi consequat. Quis aute iure reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint obcaecat cupiditat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</h6>
								<h7><a href="#">read more</a></h7>
							</div>

						</div>
					</div>
		  

					<div class="col-sm-12 col-md-4 col-lg-4 col-full-height">
						<div class="thumbnail">
							<div class="modal-image-container" style="background-image:url('images/IT.jpg');"></div>

							<div class="caption">     
								<h5>PRODOTTO 2</h5>
								<h6>Lorem ipsum dolor sit amet, consectetur adipisici elit, sed eiusmod tempor incidunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquid ex ea commodi consequat. Quis aute iure reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint obcaecat cupiditat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</h6>
								<h7><a href="#">read more</a></h7>
							</div>

						</div>
					</div>


					<div class="col-sm-12 col-md-4 col-lg-4 col-full-height">
						<div class="thumbnail">
							<div class="modal-image-container" style="background-image:url('images/IT.jpg');"></div>

							<div class="caption">     
								<h5>PRODOTTO 2</h5>
								<h6>Lorem ipsum dolor sit amet, consectetur adipisici elit, sed eiusmod tempor incidunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquid ex ea commodi consequat. Quis aute iure reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint obcaecat cupiditat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</h6>
								<h7><a href="#">read more</a></h7>
							</div>

						</div>
					</div>
				-->
			  
				</div>



	      </div>

	    </div><!-- /.modal-content -->
	  </div><!-- /.modal-dialog -->
	</div><!-- /.modal -->


	<div class="navbar-wrapper">
		<div class="container" id="navContainer" style="width:100%;">

				<nav class="navbar navbar-default transparent_navbar" role="navigation">

				  <!-- Brand and toggle get grouped for better mobile display -->
				  <div class="navbar-header">
				    <button type="button" class="navbar-toggle text-left" data-toggle="collapse" id="buttonMobile" data-target="#bs-example-navbar-collapse-1">
				      <span class="sr-only">Toggle navigation</span>
				      <img id="logoButtonMobile" src="images/logo_mediasoft_notext_white.png">
				    </button>
				  </div>

				  <!-- Collect the nav links, forms, and other content for toggling -->
				  <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				    <ul class="nav navbar-nav" id="navBarMainUl">

				    	<!--
				      	<li id="nav1" menu-position="left" class="menu-left-item"><a href="#">SOLUZIONI</a></li>
				    
						<li id="nav2" menu-position="left" class="dropdown menu-left-item">

							<a href="#" class="dropdown-toggle" data-toggle="dropdown">INTERNET DELLE COSE</a>

							<ul class="dropdown-menu second-level-submenu-custom">

								<ul class='list-inline'>
									<li><a href="#" id="linkProgetti" menu-level="secondo">PROGETTI</a></li>
									<li><a href="#" id="linkNews" menu-level="secondo">NEWS</a></li>
									<li><a href="#" id="linkCaseStudy" menu-level="secondo">CASE STUDY</a></li>					         
								</ul>

								
								<div id="progettiChild" father-element="linkProgetti" menu-level="terzo">
									<div class="row">
										<div class="col-sm-4 col-md-4 col-lg-4"><a href="#">pinco pallino 1</a></div>
										<div class="col-sm-4 col-md-4 col-lg-4"><a href="#">pinco pallino 2</a></div>
										<div class="col-sm-4 col-md-4 col-lg-4"><a href="#">pinco pallino 3</a></div>
										<div class="col-sm-4 col-md-4 col-lg-4"><a href="#">pinco pallino 4</a></div>
										<div class="col-sm-4 col-md-4 col-lg-4"><a href="#">pinco pallino 5</a></div>
									</div>

							
								</div>
								
							</ul>

						</li>


				      	<li id="nav3" menu-position="left" class="menu-left-item"><a href="#">APPLICAZIONI</a></li>

				    	<li id="navLogo"><a href="#"><img id="img" src="images/logo_mediasoft_white.png"></a></li>

				    	<li id="nav4" menu-position="right" class="menu-right-item"><a href="#">ABOUT</a></li>
				    	<li id="nav5" menu-position="right" class="menu-right-item"><a href="#">LAB</a></li>
				    	<li id="nav6" menu-position="right" class="menu-right-item"><a href="#">SUPPORTO</a></li>
				    	<li id="nav7" menu-position="right" class="menu-right-item"><a href="#">CONTATTI</a></li>
				    	-->

				    </ul>

				  </div><!-- /.navbar-collapse -->
				</nav>

		</div>
	</div>



  	<div id="mainCarousel" class="carousel slide">

	  <!-- Indicators -->
	  	<ol class="carousel-indicators" id="carouselIndicators">
			<!--<li data-target="#mainCarousel" data-slide-to="0" class="active"></li>
			<li data-target="#mainCarousel" data-slide-to="1"></li>
			<li data-target="#mainCarousel" data-slide-to="2"></li>
			<li data-target="#mainCarousel" data-slide-to="3"></li>-->
		</ol>

	  <!-- Wrapper for slides -->
	  <div class="carousel-inner" id="carouselInnerContainer">
		
		<!--	
			<div class="active item">
				<div class="fill" style="background-image:url('images/home.jpg');">
					<div class="container">
						<div class="carousel-caption">
							<h1>ENGINEERING IMAGINATION.</h1>
							<button type="button" class="mediasoft-btn">HIGHLIGHT</button>
						</div>
					</div>
				</div>
			</div>

			<div class="item">
				<div class="fill" style="background-image:url('images/internet_delle_cose.jpg');">
					<div class="container">
						<div class="carousel-caption">
							<h1>INTERNET DELLE COSE</h1>
							<button type="button" class="mediasoft-btn">VIEW MORE</button>
						</div>
					</div>
				</div>
			</div>


			<div class="item">
				<div class="fill" style="background-image:url('images/applicazioni.jpg');">
					<div class="container">
						<div class="carousel-caption">
							<h1>APPLICAZIONI</h1>
							<button type="button" class="mediasoft-btn">VIEW MORE</button>
						</div>
					</div>
				</div>
			</div>
	 

			<div class="item">
				<div class="fill" style="background-image:url('images/soluzioni.jpg');">
					<div class="container">
						<div class="carousel-caption">
							<h1>SOLUZIONI</h1>
							<button type="button" class="mediasoft-btn">VIEW MORE</button>
						</div>
					</div>
				</div>
			</div>
		-->

	  </div>

	
	</div>



    <div id="footer">
      	
      	<div id="footer-custom-content">
			
      		<div id="inner-footer">
				
				&nbsp;

				<span style="float:left">&copy; 2013 MediaSoft srl P.IVA 04033260755</span>
				<span style="float:right">
					<a href="index.php?lang=IT">IT</a> 
					<a href="index.php?lang=EN">EN</a>
				</span>
			
			</div>
			
      	</div>
        
    </div>

	<script src="jquery-1.10.2.min.js"></script>
	<script src="bootstrap/js/bootstrap.min.js"></script>
	<script src="functions.js"></script>
	<script src="modernizr.custom.js"></script>

	<script type="text/javascript">	

		/* il cambio carousel sul keypress deve essere disabilitato se viene visualizzata la finestra modale dei contenuti */
		var switchCarouselOnKeyPress = true;


		$.setThemeByCarouselIndex = function(index) {

			var style = carousel_style_by_index[index]; // 0 bianco - 1 nero

			if(style == 0) {

				$.setThemeWhite();	
				$.setCarouselColor(index, 'white');
				
			} else {

				$.setThemeBlack();					
				$.setCarouselColor(index, 'black');

			}	
		}

		$(window).load(function() {
			 
			$.buildTopMenu();
			$.tweakIpadLandscape();
			loadCarousel();
			$.setThemeByCarouselIndex(0);	

		});

		

		$(document).ready(function() {



			$('#modalContentWindow').on('show.bs.modal', function () {
				
				switchCarouselOnKeyPress = false;				

				$("[name='carousel-content']").each(function(index) {   
					$(this).hide();
				});

			});



			$('#modalContentWindow').on('hide.bs.modal', function () {

				switchCarouselOnKeyPress = true;
				
				$("[name='carousel-content']").each(function(index) {   
					$(this).show();
				});
			});




			$('.carousel').carousel({
			    interval: false
			});

		
			var style = carousel_style_by_index[0];



			$("#mainCarousel").on('slide.bs.carousel', function(evt) {	
				$.setThemeByCarouselIndex( $(evt.relatedTarget).index() );				
			});




			$(document).keydown(function(e) {

				if(switchCarouselOnKeyPress == true) {	

					if (e.keyCode == 37) { 
					   $('.carousel').carousel('prev');
					   return false;
					}

					if (e.keyCode == 39) { 
					   $('.carousel').carousel('next');
					   return false;
					}

				}

			});



			$('#footer-custom-content').hover( 

				function() {

					if($.detectMobile() == false) {
						$('#inner-footer').fadeIn(250);
					}

				}, function() {

					if($.detectMobile() == false) {
						$('#inner-footer').fadeOut(250);
					}
				}

			);


		});






	</script>

  </body>

</html>
