<?PHP

  // Implementazione dei metodi per l'estrazione dei contenuti del blog
  // © 2014, MediaSoft S.r.l.
  // AUTORE: Luigi Bruno
  // VERSIONE: 1.0

  error_reporting(E_ALL);
  ini_set('display_errors',1);

  //include_once('http://192.170.5.20/wordpress/wp-config.php');
  include_once($_SERVER["DOCUMENT_ROOT"].'/wordpress/wp-load.php');
  include_once($_SERVER["DOCUMENT_ROOT"].'/wordpress/wp-includes/wp-db.php');  

  // Classe per l'estrazione dei contenuti del blog
  class MSWP
  {
  	// Restituisce tutte le informazioni su un post
  	// $PostNumber: numero del post
  	function GetPostDetail($PostNumber)
  	{
      global $wpdb;
      $SQLQuery="SELECT * FROM $wpdb->posts WHERE $wpdb->posts.ID='".$PostNumber."'";

      $PostDetails=$wpdb->get_results($SQLQuery,ARRAY_A);
      
      return $PostDetails;
  	}

  	function GetAllPosts()
  	{
		global $wpdb;
		$SQLQuery="SELECT u.display_name, u.user_email, $wpdb->posts.* FROM $wpdb->posts
				INNER JOIN wp_users u ON $wpdb->posts.post_author = u.ID
				WHERE $wpdb->posts.post_status='publish' AND $wpdb->posts.post_type='post'
				order by $wpdb->posts.post_date DESC";

		//echo "<b>Query:</b> ".$SQLQuery."<br />";			
		$PostDetails=$wpdb->get_results($SQLQuery,OBJECT);
      
	  
		for($i=0;$i<count($PostDetails);$i++)
		{
			//vedo se ci sta un'immagine allegata a questo post
			//$SQLImageQuery="SELECT guid FROM $wpdb->posts WHERE $wpdb->posts.post_parent='".$PostDetails[$i]->ID."' and $wpdb->posts.post_type='attachment'";
			$SQLImageQuery="SELECT guid FROM $wpdb->posts WHERE $wpdb->posts.ID = (SELECT meta_value from wp_postmeta WHERE wp_postmeta.post_id='".$PostDetails[$i]->ID."' and wp_postmeta.meta_key='_thumbnail_id' )";
			
			//echo "<b>Query:</b> ".$SQLImageQuery."<br />";		
			$ImageDetails=$wpdb->get_var($SQLImageQuery);          
			$PostDetails[$i]->ImageDetail = $ImageDetails;
			
			//prendo tutti i commenti
			$SQLCommentQuery="SELECT * FROM {$wpdb->comments} WHERE comment_approved='1' AND comment_post_ID='".$PostDetails[$i]->ID."'";
			//echo "<b>i:</b> ".$PostDetails[$i]."<br />";
			//echo "<b>i:</b> ".$i."<br />";
			//echo "<b>Query:</b> ".$SQLCommentQuery."<br />";
			$CommentDetails=$wpdb->get_results($SQLCommentQuery,ARRAY_A);          
			$PostDetails[$i]->CommentDetails = $CommentDetails;
			
			//prendo tutti i tag
			$SQLTagQuery="select name from wp_terms t
							inner join wp_term_taxonomy tt on tt.term_id = t.term_id
							inner join wp_term_relationships tr on tt.term_taxonomy_id = tr.term_taxonomy_id
							where tt.taxonomy='post_tag'
							and tr.object_id='".$PostDetails[$i]->ID."'";
			//echo "<b>Query:</b> ".$SQLTagQuery."<br />";
			$TagDetails=$wpdb->get_results($SQLTagQuery,ARRAY_A);          
			$PostDetails[$i]->TagDetails = $TagDetails;
							
		}	  
		  
		return $PostDetails;
  	}

	
    function GetPostsByCat($TermTags)
    {
      global $wpdb;

      // Selezione della tipologia di termine (categoria o tag) da cercare
      $TaxonomyType=($TermType=='CAT')?"category":"post_tag";

      // Composizione query SQL
      $SQLQuery="SELECT u.display_name, post.*
                 FROM wp_terms t
                 INNER JOIN wp_term_taxonomy tax ON tax.term_id=t.term_id
                 INNER JOIN wp_term_relationships rel ON rel.term_taxonomy_id=tax.term_taxonomy_id
                 INNER JOIN wp_posts post ON post.ID=rel.object_id
                 INNER JOIN wp_users u ON u.ID=post.post_author
                 WHERE tax.taxonomy='category' AND post.post_type='post'";

      // Elencazione dei termini da cercare
      $Terms=explode(",",$TermTags);
      $TermsString="";

      for($i=0;$i<count($Terms);$i++)
      {
        if($TermsString!="")
        {
          $TermsString.=" OR t.name='".$Terms[$i]."'";
        }
        else
        {
          $TermsString=" AND t.name='".$Terms[$i]."'";
        }
      }
      $SQLQuery.=$TermsString;

      
      // Ordinamento risultati
      $SQLQuery.=" ORDER BY post.post_date DESC";

	
	$PostDetails=$wpdb->get_results($SQLQuery,OBJECT);
      
		for($i=0;$i<count($PostDetails);$i++)
		{
			//vedo se ci sta un'immagine allegata a questo post
			//$SQLImageQuery="SELECT guid FROM $wpdb->posts WHERE $wpdb->posts.post_parent='".$PostDetails[$i]->ID."' and $wpdb->posts.post_type='attachment'";
			$SQLImageQuery="SELECT guid FROM $wpdb->posts WHERE $wpdb->posts.ID = (SELECT meta_value from wp_postmeta WHERE wp_postmeta.post_id='".$PostDetails[$i]->ID."' and wp_postmeta.meta_key='_thumbnail_id' )";
			
			//echo "<b>Query:</b> ".$SQLImageQuery."<br />";		
			$ImageDetails=$wpdb->get_var($SQLImageQuery);          
			$PostDetails[$i]->ImageDetail = $ImageDetails;
			
			//prendo tutti i commenti
			$SQLCommentQuery="SELECT * FROM {$wpdb->comments} WHERE comment_approved='1' AND comment_post_ID='".$PostDetails[$i]->ID."'";
			//echo "<b>i:</b> ".$PostDetails[$i]."<br />";
			//echo "<b>i:</b> ".$i."<br />";
			//echo "<b>Query:</b> ".$SQLCommentQuery."<br />";
			$CommentDetails=$wpdb->get_results($SQLCommentQuery,ARRAY_A);          
			$PostDetails[$i]->CommentDetails = $CommentDetails;
			
			//prendo tutti i tag
			$SQLTagQuery="select name from wp_terms t
							inner join wp_term_taxonomy tt on tt.term_id = t.term_id
							inner join wp_term_relationships tr on tt.term_taxonomy_id = tr.term_taxonomy_id
							where tt.taxonomy='post_tag'
							and tr.object_id='".$PostDetails[$i]->ID."'";
			//echo "<b>Query:</b> ".$SQLTagQuery."<br />";
			$TagDetails=$wpdb->get_results($SQLTagQuery,ARRAY_A);          
			$PostDetails[$i]->TagDetails = $TagDetails;
							
		}	  
		  
		return $PostDetails;


      return $PostDetails;
    }
	
    function GetPostsByTag($TermTags)
    {
      global $wpdb;

      // Selezione della tipologia di termine (categoria o tag) da cercare
      $TaxonomyType=($TermType=='CAT')?"category":"post_tag";

      // Composizione query SQL
      $SQLQuery="SELECT u.display_name, post.*
                 FROM wp_terms t
                 INNER JOIN wp_term_taxonomy tax ON tax.term_id=t.term_id
                 INNER JOIN wp_term_relationships rel ON rel.term_taxonomy_id=tax.term_taxonomy_id
                 INNER JOIN wp_posts post ON post.ID=rel.object_id
                 INNER JOIN wp_users u ON u.ID=post.post_author
                 WHERE tax.taxonomy='post_tag' AND post.post_type='post'";

      // Elencazione dei termini da cercare
      $Terms=explode(",",$TermTags);
      $TermsString="";

      for($i=0;$i<count($Terms);$i++)
      {
        if($TermsString!="")
        {
          $TermsString.=" OR t.name='".$Terms[$i]."'";
        }
        else
        {
          $TermsString=" AND t.name='".$Terms[$i]."'";
        }
      }
      $SQLQuery.=$TermsString;

      
      // Ordinamento risultati
      $SQLQuery.=" ORDER BY post.post_date DESC";

	
	$PostDetails=$wpdb->get_results($SQLQuery,OBJECT);
      
		for($i=0;$i<count($PostDetails);$i++)
		{
			//vedo se ci sta un'immagine allegata a questo post
			//$SQLImageQuery="SELECT guid FROM $wpdb->posts WHERE $wpdb->posts.post_parent='".$PostDetails[$i]->ID."' and $wpdb->posts.post_type='attachment'";
			$SQLImageQuery="SELECT guid FROM $wpdb->posts WHERE $wpdb->posts.ID = (SELECT meta_value from wp_postmeta WHERE wp_postmeta.post_id='".$PostDetails[$i]->ID."' and wp_postmeta.meta_key='_thumbnail_id' )";
			
			//echo "<b>Query:</b> ".$SQLImageQuery."<br />";		
			$ImageDetails=$wpdb->get_var($SQLImageQuery);          
			$PostDetails[$i]->ImageDetail = $ImageDetails;
			
			//prendo tutti i commenti
			$SQLCommentQuery="SELECT * FROM {$wpdb->comments} WHERE comment_approved='1' AND comment_post_ID='".$PostDetails[$i]->ID."'";
			//echo "<b>i:</b> ".$PostDetails[$i]."<br />";
			//echo "<b>i:</b> ".$i."<br />";
			//echo "<b>Query:</b> ".$SQLCommentQuery."<br />";
			$CommentDetails=$wpdb->get_results($SQLCommentQuery,ARRAY_A);          
			$PostDetails[$i]->CommentDetails = $CommentDetails;
			
			//prendo tutti i tag
			$SQLTagQuery="select name from wp_terms t
							inner join wp_term_taxonomy tt on tt.term_id = t.term_id
							inner join wp_term_relationships tr on tt.term_taxonomy_id = tr.term_taxonomy_id
							where tt.taxonomy='post_tag'
							and tr.object_id='".$PostDetails[$i]->ID."'";
			//echo "<b>Query:</b> ".$SQLTagQuery."<br />";
			$TagDetails=$wpdb->get_results($SQLTagQuery,ARRAY_A);          
			$PostDetails[$i]->TagDetails = $TagDetails;
							
		}	  
		  
		return $PostDetails;


      return $PostDetails;
    }
	
    // Restituisce il numero di post presenti nel blog
    function GetPostsCount()
    {
      global $wpdb;
      $SQLQuery="SELECT COUNT(*) FROM $wpdb->posts";

      $NumberOfPosts=$wpdb->get_var($SQLQuery);
      
      return $NumberOfPosts;
    }

    // Restituisce il numero di post per un tag o per una categoria
    // $TermType: tipologia di termine. Valori possibili: CAT = categoria, TAG = tag
    // $TermTags: termine da cercare
    // $ContentText: (opzionale) testo presente nel post
    // $StartNumberOfResults: (opzionale) inizio numero di risultati da restituire
    // $EndNumberOfResults: (opzionale) fine numero di risultati da restituire
    function GetPostsByTerm($TermType,$TermTags,$ContentText=NULL,$StartNumberOfResults=NULL,$EndNumberOfResults=NULL)
    {
      global $wpdb;

      // Selezione della tipologia di termine (categoria o tag) da cercare
      $TaxonomyType=($TermType=='CAT')?"category":"post_tag";

      // Composizione query SQL
      $SQLQuery="SELECT u.display_name, post.*
                 FROM wp_terms t
                 INNER JOIN wp_term_taxonomy tax ON tax.term_id=t.term_id
                 INNER JOIN wp_term_relationships rel ON rel.term_taxonomy_id=tax.term_taxonomy_id
                 INNER JOIN wp_posts post ON post.ID=rel.object_id
                 INNER JOIN wp_users u ON u.ID=post.post_author
                 WHERE tax.taxonomy='".$TaxonomyType."' AND post.post_type='post'";

      // Elencazione dei termini da cercare
      $Terms=explode(",",$TermTags);
      $TermsString="";

      for($i=0;$i<count($Terms);$i++)
      {
        if($TermsString!="")
        {
          $TermsString.=" OR t.name='".$Terms[$i]."'";
        }
        else
        {
          $TermsString=" AND t.name='".$Terms[$i]."'";
        }
      }
      $SQLQuery.=$TermsString;

      // Ricerca di testo all'interno del contenuto
      if($ContentText)
      {
        $SQLQuery.=" AND post.post_content LIKE '".$ContentText."'";
      }
      
      // Ordinamento risultati
      $SQLQuery.=" ORDER BY post.post_date DESC";

      // Limitazione dei risultati restituiti
      if ($StartNumberOfResults!=NULL && $EndNumberOfResults!=NULL)
      {
        $SQLQuery.=" LIMIT ".$StartNumberOfResults.",".$EndNumberOfResults."";
      }

      echo "<b>Query:</b> ".$SQLQuery."<br />";
      $PostsPerTag=$wpdb->get_results($SQLQuery,OBJECT);
      
      echo "<b>Numero post:</b> ".count($PostsPerTag)."<hr/>";

      return $PostsPerTag;
    }

    // Restituisce l'elenco dei tag utilizzati
    function GetTagsList()
    {
      global $wpdb;
      $SQLQuery="SELECT $wpdb->terms.name FROM $wpdb->terms";

      $TagsList=$wpdb->get_results($SQLQuery,ARRAY_A);
      
      return $TagsList;
    }

    // Restituisce il numero di utenti registrati nel blog
    function GetUsersCount()
    {
      global $wpdb;
      $SQLQuery="SELECT COUNT(*) FROM $wpdb->users";

      $NumberOfUsers=$wpdb->get_var($SQLQuery);
      
      return $NumberOfUsers;
    } 
	
    // Scrive un commento
    function WriteCommentOnPost($idPost,$nome,$commento,$email)
    {
		
		global $wpdb;

		
		$SQLQuery = $wpdb->prepare("insert into $wpdb->comments (comment_post_ID, comment_author, comment_author_email, comment_date, comment_date_gmt, comment_content, comment_approved) values (%d,'%s','%s', NOW(), NOW(),'%s', 0)",$idPost, $nome, $email, $commento);
	  
		$ret = $wpdb->query($SQLQuery);

		return $ret;
    } 

    // prende le categorie
    function getCategories()
    {
		
		global $wpdb;
		
		$SQLQuery="SELECT name FROM wp_terms t join wp_term_taxonomy tt on tt.term_id=t.term_id and tt.taxonomy = 'category'";

		$CatList=$wpdb->get_results($SQLQuery,ARRAY_A);

		return $CatList;

    } 
	
    // prende i tags
    function getTags()
    {
		
		global $wpdb;
		
		$SQLQuery="SELECT name FROM wp_terms t join wp_term_taxonomy tt on tt.term_id=t.term_id and tt.taxonomy = 'post_tag'";

		$TagList=$wpdb->get_results($SQLQuery,ARRAY_A);

		return $TagList;

    } 

    // prende i tags
    function getCategoriesAndTag()
    {
		
		global $wpdb;
		
		$SQLQuery="SELECT t.name, tt.taxonomy FROM wp_terms t join wp_term_taxonomy tt on tt.term_id=t.term_id and (tt.taxonomy = 'post_tag' OR tt.taxonomy = 'category') ";

		$TagCatList=$wpdb->get_results($SQLQuery,ARRAY_A);

		return $TagCatList;

    } 
	
	
  }
  
  
  

?>