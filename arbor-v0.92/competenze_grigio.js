//
//  main.js
//
//  A project template for using arbor.js
//

(function($){

  var Renderer = function(canvas){
    var canvas = $(canvas).get(0)
    var ctx = canvas.getContext("2d");
    var particleSystem

    var that = {
      init:function(system){
        //
        // the particle system will call the init function once, right before the
        // first frame is to be drawn. it's a good place to set up the canvas and
        // to pass the canvas size to the particle system
        //
        // save a reference to the particle system for use in the .redraw() loop
        particleSystem = system

        // inform the system of the screen dimensions so it can map coords for us.
        // if the canvas is ever resized, screenSize should be called again with
        // the new dimensions
        particleSystem.screenSize(canvas.width, canvas.height) 
        particleSystem.screenPadding(50,180,50,180) // leave an extra 80px of whitespace per side
        
        // set up some event handlers to allow for node-dragging
        that.initMouseHandling()
      },
      
      redraw:function(){
        // 
        // redraw will be called repeatedly during the run whenever the node positions
        // change. the new positions for the nodes can be accessed by looking at the
        // .p attribute of a given node. however the p.x & p.y values are in the coordinates
        // of the particle system rather than the screen. you can either map them to
        // the screen yourself, or use the convenience iterators .eachNode (and .eachEdge)
        // which allow you to step through the actual node objects but also pass an
        // x,y point in the screen's coordinate system
        // 
        //ctx.fillStyle = "yellow"
		//ctx.fillRect(0,0, canvas.width, canvas.height)
        ctx.clearRect(0,0, canvas.width, canvas.height)
		
		
		
        particleSystem.eachEdge(function(edge, pt1, pt2){
          // edge: {source:Node, target:Node, length:#, data:{}}
          // pt1:  {x:#, y:#}  source position in screen coords
          // pt2:  {x:#, y:#}  target position in screen coords

          // draw a line from pt1 to pt2
          ctx.strokeStyle = "rgba(100, 100, 100, 0.5)";
          ctx.lineWidth = 1
          ctx.beginPath()
          ctx.moveTo(pt1.x, pt1.y)
          ctx.lineTo(pt2.x, pt2.y)
          ctx.stroke()
        })

        particleSystem.eachNode(function(node, pt){
          // node: {mass:#, p:{x,y}, name:"", data:{}}
          // pt:   {x:#, y:#}  node position in screen coords

          var w = ctx.measureText(node.data.label||"").width + 6
          var label = node.data.label
          if (!(label||"").match(/^[ \t]*$/))
		  {
            pt.x = Math.floor(pt.x)
            pt.y = Math.floor(pt.y)
          }
		  else
		  {
            label = null
          }

          //ctx.clearRect(pt.x-w/2, pt.y-7, w,14)

          

          // draw the text
          if (label.length>2){
            ctx.font = "bold 14px Maven Pro"
            ctx.textAlign = "center"
            
            // if (node.data.region) ctx.fillStyle = palette[node.data.region]
            // else ctx.fillStyle = "#888888"
            ctx.fillStyle = "#58585a"

			//ci sta per caso una label2?
			var label2 = node.data.label2
			if (!(label2||"").match(/^[ \t]*$/))
			{
				ctx.fillText(label||"", pt.x, pt.y-3)
				ctx.fillText(label2||"", pt.x, pt.y+11)
			}
			else
			{
				// ctx.fillText(label||"", pt.x, pt.y+4)
				ctx.fillText(label||"", pt.x, pt.y+4)
			}
		  }
		  else
		{ 			
			  ctx.beginPath();
			  ctx.arc(pt.x, pt.y, 4, 0, 2 * Math.PI, false);
			  ctx.fillStyle = "rgba(177, 201, 3, 0.8)";
			  ctx.fill();
			  //ctx.lineWidth = 5;
			  //ctx.strokeStyle = '#003300';
			  //ctx.stroke();

			//ctx.fillStyle = "#888888"
			//ctx.fillRect(pt.x-w/2, pt.y-w/2, w,w) 
		  }

         
		 // draw a rectangle centered at pt
         /* var w = 10
          ctx.fillStyle = (node.data.alone) ? "orange" : "black"
          ctx.fillRect(pt.x-w/2, pt.y-w/2, w,w) */
        })    			
      },
      
      initMouseHandling:function(){
        // no-nonsense drag and drop (thanks springy.js)
        var dragged = null;

        // set up a handler object that will initially listen for mousedowns then
        // for moves and mouseups while dragging
        var handler = {
          clicked:function(e){
            var pos = $(canvas).offset();
            _mouseP = arbor.Point(e.pageX-pos.left, e.pageY-pos.top)
            dragged = particleSystem.nearest(_mouseP);

            if (dragged && dragged.node !== null){
              // while we're dragging, don't let physics move the node
              dragged.node.fixed = true
            }

            $(canvas).bind('mousemove', handler.dragged)
            $(window).bind('mouseup', handler.dropped)

            return false
          },
          dragged:function(e){
            var pos = $(canvas).offset();
            var s = arbor.Point(e.pageX-pos.left, e.pageY-pos.top)

            if (dragged && dragged.node !== null){
              var p = particleSystem.fromScreen(s)
              dragged.node.p = p
            }

            return false
          },

          dropped:function(e){
			//alert(dragged.node.data.link);
 			var link = dragged.node.data.link
			if (!(link||"").match(/^[ \t]*$/))
			{
				window.location.href=link;
			}
			
			if (dragged===null || dragged.node===undefined) return
            if (dragged.node !== null) dragged.node.fixed = false
            dragged.node.tempMass = 1000
            dragged = null
            $(canvas).unbind('mousemove', handler.dragged)
            $(window).unbind('mouseup', handler.dropped)
            _mouseP = null
 			return false
          }
        }
        
        // start listening
        $(canvas).mousedown(handler.clicked);

      },
      
    }
    return that
  }    

  $(document).ready(function(){
    var sys = arbor.ParticleSystem(1000, 600, 0.5) // create the system with sensible repulsion/stiffness/friction
    sys.parameters({gravity:true}) // use center-gravity to make the graph settle nicely (ymmv)
    sys.renderer = Renderer("#viewport") // our newly created renderer will have its .init() method called shortly by sys...

    // add some nodes to the graph and watch it go...
	node1 = sys.addNode("1", {mass:1, label:"1"}) 
	node2 = sys.addNode("2", {mass:1, label:"2"}) 
	node3 = sys.addNode("3", {mass:1, label:"3"}) 
	node4 = sys.addNode("4", {mass:1, label:"4"}) 
	node5 = sys.addNode("5", {mass:1, label:"5"}) 
	node6 = sys.addNode("6", {mass:1, label:"6"}) 
	node7 = sys.addNode("7", {mass:1, label:"7"}) 
	node8 = sys.addNode("8", {mass:1, label:"8"}) 
	nodec1 = sys.addNode("c1", {mass:1, label:"APPLICAZIONI WEB", link:"fixed.content.php?idPage=9"}) 
	nodec2 = sys.addNode("c2", {mass:1, label:"WEB MARKETING", link:"fixed.content.php?idPage=11"}) 
	nodec3 = sys.addNode("c3", {mass:1, label:"REALTA' AUMENTATA", link:"fixed.content.php?idPage=8"}) 
	nodec4 = sys.addNode("c4", {mass:1, label:"APPLICAZIONI 3D", link:"fixed.content.php?idPage=12"}) 
	nodec5 = sys.addNode("c5", {mass:1, label:"INTERNET DELLE COSE", link:"fixed.content.php?idPage=7"}) 
	nodec6 = sys.addNode("c6", {mass:1, label:"TECNOLOGIE EMERGENTI", link:"fixed.content.php?idPage=13"}) 
	nodec7 = sys.addNode("c7", {mass:1, label:"SISTEMI DI", label2:"COLLABORAZIONE AZIENDALE", link:"fixed.content.php?idPage=14"}) 
	nodec8 = sys.addNode("c8", {mass:1, label:"IDENTITA' DIGITALE", link:"fixed.content.php?idPage=15"}) 
	nodec9 = sys.addNode("c9", {mass:1, label:"GESTIONE DELLE", label2:"INFORMAZIONI", link:"fixed.content.php?idPage=16"}) 
	nodec10 = sys.addNode("c10", {mass:1, label:"APP MOBILE E", label2:"MULTICANALE", link:"fixed.content.php?idPage=10"}) 

	
	sys.addEdge(node1, node2, {length:1.0})
	sys.addEdge(node2, node3, {length:1.0})
	sys.addEdge(node3, node4, {length:1.0})
	sys.addEdge(node4, nodec10, {length:1.0})
	sys.addEdge(nodec9, nodec10, {length:1.0})
	sys.addEdge(nodec5, nodec9, {length:1.0})
	sys.addEdge(node8, nodec5, {length:1.0})
	sys.addEdge(nodec2, node8, {length:1.0})
	sys.addEdge(nodec1, nodec2, {length:1.0})
	sys.addEdge(nodec1, node1, {length:1.0})
	sys.addEdge(nodec2, node5, {length:1.0})
	sys.addEdge(node1, node5, {length:1.0})
	sys.addEdge(node5, nodec4, {length:1.0})
	sys.addEdge(nodec4, node2, {length:1.0})
	sys.addEdge(node2, node6, {length:1.0})
	sys.addEdge(node5, nodec6, {length:1.0})
	sys.addEdge(nodec3, nodec10, {length:1.0})
	sys.addEdge(nodec3, nodec5, {length:1.0})
	sys.addEdge(nodec5, node7, {length:1.0})
	sys.addEdge(nodec6, node7, {length:1.0})
	sys.addEdge(node6, nodec7, {length:1.0})
	sys.addEdge(nodec7, nodec8, {length:1.0})
	sys.addEdge(nodec8, nodec9, {length:1.0})
	sys.addEdge(nodec8, node4, {length:1.0})
	sys.addEdge(nodec7, node3, {length:1.0})
	sys.addEdge(node6, node7, {length:1.0})
	sys.addEdge(node6, nodec4, {length:1.0})

	
	setInterval(function()
	{
		//alert("ciao");
		sys.parameters({ stiffness:300})
		setTimeout(function()
		{
			sys.parameters({ stiffness:800})
		}, 300);
		
//		var stif = Math.floor((Math.random()*1000)+300);
	
	}, 2000);
	
    // or, equivalently:
    //
    // sys.graft({
    //   nodes:{
    //     f:{alone:true, mass:.25}
    //   }, 
    //   edges:{
    //     a:{ b:{},
    //         c:{},
    //         d:{},
    //         e:{}
    //     }
    //   }
    // })
    
  })

})(this.jQuery)