﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _Default : System.Web.UI.Page 
{
    protected void Page_Load(object sender, EventArgs e)
    {
        
        
    }


    private void conv(string strLat, string strLon)
    {
       

        //calcoliamo la latitudine
        string degLat = strLat.Substring(0, 2);
        string minLat = strLat.Substring(2, 5);
        double lat = double.Parse(degLat.Replace(".", ",")) + (double.Parse(minLat.Replace(".", ",")) / 60);
        lat = Math.Round(lat * 1000000.0) / 1000000.0;

        //calcoliamo la longitudine
        string degLon = strLon.Substring(0, 2);
        string minLon = strLon.Substring(2, 5);
        double Lon = double.Parse(degLon.Replace(".", ",")) + (double.Parse(minLon.Replace(".", ",")) / 60);
        Lon = Math.Round(Lon * 1000000.0) / 1000000.0;
        Response.Write("COORDINATE Lat: " + lat.ToString().Replace(",", ".") + " Lon:" + Lon.ToString().Replace(",", "."));
        lblMappa.Text = "<iframe width=\"425\" height=\"350\" frameborder= \"0\" scrolling=\"no\" marginheight=\"0\" marginwidth=\"0\" src=\"http://maps.google.it/maps?f=q&amp;source=s_q&amp;hl=it&amp;geocode=&amp;q=+" + lat.ToString().Replace(",", ".") + "+" + Lon.ToString().Replace(",", ".") + "+&amp;aq=&amp;sll=" + lat.ToString().Replace(",", ".") + "," + Lon.ToString().Replace(",", ".") + "&amp;sspn=25.643016,39.067383&amp;ie=UTF8&amp;z=14&amp;ll=" + lat.ToString().Replace(",", ".") + "," + Lon.ToString().Replace(",", ".") + "&amp;output=embed\"></iframe><br /><small><a href=\"http://maps.google.it/maps?f=q&amp;source=embed&amp;hl=it&amp;geocode=&amp;q=+40.261833+18.044667+&amp;aq=&amp;sll=41.442726,12.392578&amp;sspn=25.643016,39.067383&amp;ie=UTF8&amp;z=15&amp;ll=" + lat.ToString().Replace(",", ".") + "," + Lon.ToString().Replace(",", ".") + "\" style=\"color:#0000FF;text-align:left\">Visualizzazione ingrandita della mappa</a></small>";
    }
    protected void bttOttieni_Click(object sender, EventArgs e)
    {
        //string strLat = "4016.27976";
        //string strLon = "1803.46147";
        conv(txtLat.Text, txtLon.Text);
    }
}
