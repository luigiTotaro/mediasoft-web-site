/**
 * NoobHub node.js server
 * Opensource multiplayer and network messaging for CoronaSDK, Moai, Gideros & LÖVE
 *
 * @usage node node.js
 * configure port number and buffer size
 * according to your needs
 *
 * @authors
 * Igor Korsakov
 * Sergii Tsegelnyk
 *
 * @license WTFPL
 * https://github.com/Overtorment/NoobHub
 */

console.log('Hello World');

var receivedMessage = [];
var clientConnected = new Array();
var ranking = new Object(); //il ranking starà in un json che mi spedisce il master
var metafore = new Object(); //memorizzo le metafore in modod da avere sempre uno stato aggiornato nel server, in termini di metafore eseguite e non

 
var server = require('net').createServer()
    , sockets = {}  // this is where we store all current client socket connections
    , cfg = {
        port: 1337,
        buffer_size: 1024*8, // buffer is allocated per each socket client
        verbose: true
    }
    , _log = function(){
        if (cfg.verbose) console.log.apply(console, arguments);
    };

// black magic
process.on('uncaughtException', function(err){
    _log('Exception: ' + err);
});

server.on('connection', function(socket) {
    socket.setNoDelay(true);
    socket.connection_id = require('crypto').createHash('sha1').update( 'noobhub'  + Date.now() + Math.random() ).digest('hex') ; // unique sha1 hash generation
    socket.channel = '';
    socket.buffer = new Buffer(cfg.buffer_size);
    socket.buffer.len = 0; // due to Buffer's nature we have to keep track of buffer contents ourself

    _log('New client: ' + socket.remoteAddress +':'+ socket.remotePort);
	// lo memorizzo nella tabella
	var newClient = new Array();
	newClient[0]=socket.remoteAddress;
	newClient[1]=socket.remotePort;
	newClient[2]=""; //il nome
	newClient[3]=socket.connection_id; 
	
	clientConnected.push(newClient);
	
    socket.on('data', function(data_raw) { // data_raw is an instance of Buffer as well
        if (data_raw.length > (cfg.buffer_size - socket.buffer.len)) {
            _log("Message doesn't fit the buffer. Adjust the buffer size in configuration");
            socket.buffer.len = 0; // trimming buffer
            return false;
        }
		
		
        socket.buffer.len +=  data_raw.copy(socket.buffer, socket.buffer.len); // keeping track of how much data we have in buffer

        var str, start, end
            , conn_id = socket.connection_id;
        str = socket.buffer.slice(0,socket.buffer.len).toString();

        if ( (start = str.indexOf("__SUBSCRIBE__")) !=  -1   &&   (end = str.indexOf("__ENDSUBSCRIBE__"))  !=  -1) {
            socket.channel = "ciao"; //str.substr( start+13,  end-(start+13) );
            socket.write('Hello. Noobhub online. \r\n\0');
            _log("Client subscribes for channel: " + socket.channel);
            str = str.substr(end + 16);  // cut the message and remove the precedant part of the buffer since it can't be processed
            socket.buffer.len = socket.buffer.write(str, 0);
            sockets[socket.channel] = sockets[socket.channel] || {}; // hashmap of sockets  subscribed to the same channel
            sockets[socket.channel][conn_id] = socket;
        }

        var time_to_exit = true;
        do{  // this is for a case when several messages arrived in buffer
            if ( (start = str.indexOf("__JSON__START__")) !=  -1   &&  (end = str.indexOf("__JSON__END__"))  !=  -1 ) {
                var json = str.substr( start+15,  end-(start+15) );
                _log("Client posts json:  " + json);
                str = str.substr(end + 13);  // cut the message and remove the precedant part of the buffer since it can't be processed
                //str = str.substr(end + 0);  // cut the message and remove the precedant part of the buffer since it can't be processed
                socket.buffer.len = socket.buffer.write(str, 0);
                for (var prop in sockets[socket.channel]) {
					if (sockets[socket.channel].hasOwnProperty(prop)) {
						//_log("prop: **"+ prop + "**");
						//_log("socket.channel]: **"+ socket.channel + "**");
						sockets[socket.channel][prop].write("__JSON__START__" + json + "__JSON__END__\0");
                    }
                } // writing this message to all sockets with the same channel
				
				receivedMessage[receivedMessage.length]=json;
				//controllo il json per vedere se devo fare qualche operazione
				var obj = JSON.parse(json);
				if (obj.action=="identity")
				{
					_log("Messaggio di Identity: " + obj.param + " - Addr: " + socket.remoteAddress+ " - Port: " + socket.remotePort);
					// e aggiorno la tabella dei client connessi
					for (var i=0;i<clientConnected.length;i++)
					{
						if ((clientConnected[i][0]==socket.remoteAddress) && (clientConnected[i][1]==socket.remotePort))
						{
							clientConnected[i][2]=obj.param;
						}
					}
					//scrivo tutti i client connessi e creo il json da spedire
					var jsonObj = new Object();
					var jsonArr = [];
					for (var i=0;i<clientConnected.length;i++)
					{
						_log("Client Connesso: " + clientConnected[i][2] + " - Addr: " + clientConnected[i][0]+ " - Port: " + clientConnected[i][1]);
						jsonArr.push({
							id: clientConnected[i][2],
							addr: clientConnected[i][0],
							port: clientConnected[i][1]
						});
					}
					jsonObj.action="clientConnected";
					jsonObj.clients=jsonArr;
					//creo un messaggio e lo spedisco
					for (var prop in sockets[socket.channel]) {
						if (sockets[socket.channel].hasOwnProperty(prop)) {
							sockets[socket.channel][prop].write("__JSON__START__" + JSON.stringify(jsonObj) + "__JSON__END__\0");
						}
					} // writing this message to all sockets with the same channel
					
				}
				if (obj.action=="metaforeSet")
				{
					_log("Messaggio di set Metafore");
					metafore = obj.param;
				}
				if (obj.action=="metaforeRequest")
				{
					_log("Messaggio di richiesta Metafore:");
					var jsonObj = new Object();
					jsonObj.action="metaforeRequested";
					jsonObj.param=metafore;
					
					for (var prop in sockets[socket.channel]) {
						if (sockets[socket.channel].hasOwnProperty(prop)) {
							sockets[socket.channel][prop].write("__JSON__START__" + JSON.stringify(jsonObj) + "__JSON__END__\0");
						}
					} // writing this message to all sockets with the same channel
					
				}
				if (obj.action=="rankingSet")
				{
					_log("Messaggio di set Ranking:");
					//il ranking sta nel parametro
					ranking = obj.param;
				}
				if (obj.action=="rankingRequest")
				{
					_log("Messaggio di richiesta Ranking:");
					var jsonObj = new Object();
					jsonObj.action="rankingRequested";
					jsonObj.param=ranking;
					
					for (var prop in sockets[socket.channel]) {
						if (sockets[socket.channel].hasOwnProperty(prop)) {
							sockets[socket.channel][prop].write("__JSON__START__" + JSON.stringify(jsonObj) + "__JSON__END__\0");
						}
					} // writing this message to all sockets with the same channel
					
				}
				
                time_to_exit = false;
            } else {  time_to_exit = true; } // if no json data found in buffer - then it is time to exit this loop
        } while ( !time_to_exit );
    }); // end of  socket.on 'data'

    socket.on('close', function(){  // we need to cut out closed socket from array of client socket connections
        
		//lupix .... scrivo quello che ho in receivedMessage
		_log("Messaggi ricevuti: "+receivedMessage.length);
		for (var i=0;i<receivedMessage.length;i++)
		{
			_log(receivedMessage[i])
		}
		if  (!socket.channel   ||   !sockets[socket.channel])  return;
        delete sockets[socket.channel][socket.connection_id];
        _log(socket.connection_id + " has been disconnected from channel " + socket.channel);
		//lo tolgo dall'array dei connessi
		var idToDelete=-1;
		for (var i=0;i<clientConnected.length;i++)
		{
			if (clientConnected[i][3]==socket.connection_id)
			{
				idToDelete=i;
			}
		}
		if (idToDelete!=-1) clientConnected.splice(idToDelete, 1);
		// e rimando la lista aggiornata
		var jsonObj = new Object();
		var jsonArr = [];
		for (var i=0;i<clientConnected.length;i++)
		{
			_log("Client Connesso: " + clientConnected[i][2] + " - Addr: " + clientConnected[i][0]+ " - Port: " + clientConnected[i][1]);
			jsonArr.push({
				id: clientConnected[i][2],
				addr: clientConnected[i][0],
				port: clientConnected[i][1]
			});
		}
		jsonObj.action="clientConnected";
		jsonObj.clients=jsonArr;
		//creo un messaggio e lo spedisco
		for (var prop in sockets[socket.channel]) {
			if (sockets[socket.channel].hasOwnProperty(prop)) {
				sockets[socket.channel][prop].write("__JSON__START__" + JSON.stringify(jsonObj) + "__JSON__END__\0");
			}
		} // writing this message to all sockets with the same channel
		
    }); // end of socket.on 'close'

}); //  end of server.on 'connection'

server.on('listening', function(){ console.log('NoobHub on ' + server.address().address +':'+ server.address().port); });
server.listen(cfg.port);