<?php


if(empty($_SERVER['HTTPS']) || $_SERVER['HTTPS'] == "off"){
	$redirect = 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
	header('HTTP/1.1 301 Moved Permanently');
	header('Location: ' . $redirect);
	exit();
}

session_start();
include 'lang.php';
//include "./backoffice/DBAccess.php";
include "./backoffice/DBAccess2.php";

$dbInst = new DBAccess2();
$dbInst->connOpen();

//$lang = (isset($_GET["lang"]) ? $_GET["lang"] : "IT");

if(isset($_GET["lang"])){
	$lang = $_GET["lang"];
	$_SESSION["lang"] = $lang;
}else{
	if(isset($_SESSION["lang"])){
		$lang = $_SESSION["lang"];
	}else{
		$lang = "EN";
		$_SESSION["lang"] = $lang;
	}
}


if (($lang!="IT") && ($lang!="EN")) $lang="IT";
$idPage = 3; //landing page

$extD = $dbInst->getDetailLangPagina($idPage, $lang);
$row = mysql_fetch_array($extD);
$title = $row['TITLE'];
$description = $row['DESCRIPTION'];
$keywords = $row['KEYWORDS'];

$menuRows = $dbInst->getVociMenuTop($lang,$idPage);
//per ogni menu' devo vedere se ci sono sottomenu
	//$subMenuItem = $dbInst->getVociMenuTopByPadre($lang,$positionRow['id']);	
	//$subMenuRows[$positionRow['id']] = $dbInst->getVociMenuTopByPadre($lang,$positionRow['id']);
 
$dbInst->connClose();

?>

<!DOCTYPE html>
<html>
  <head>
    <title>Mediasoft Srl</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="HandheldFriendly" content="true" />

    <link href='https://fonts.googleapis.com/css?family=Maven+Pro' rel='stylesheet' type='text/css'>
    <link href="bootstrap/css/bootstrap.css" rel="stylesheet" media="screen">
    <link href="navbar.customization.css" rel="stylesheet" media="screen">
    <link href="carousel.customization.css" rel="stylesheet" media="screen">
    <link href="content.customization.css" rel="stylesheet" media="screen">
    <link href="modal.customization.css" rel="stylesheet" media="screen">
    <link rel="shortcut icon" href="/images/faviconBW.ico" type="image/x-icon" />

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

    <style>

	    body {
	    	padding: 0px;
	    	margin: 0px;
	    	font-family: 'Maven Pro';
	    	font-size: 13px;
	    	font-weight:500;
	    }


		#inner-footer {

			padding-left:10px; 
			padding-right:10px; 
			border-color: white; 
			border-left:solid 3px; 
			display: none;

		}

		@media (max-width: 767px) {

			#inner-footer {

				display: block !important;

			}

		}

		


    </style>
  </head>

  <body>

  	<input type="hidden" id="langHidden" value="<?=$lang?>" />

  	<div class="modal fade" id="modalContentWindow" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" tabindex="-1">
	  <div class="modal-dialog">
	    <div class="modal-content">
	    	
	      <div class="modal-header">
	       
	        <span class="glyphicon glyphicon-remove-circle close" onclick="$('#modalContentWindow').modal('hide');" style="margin:20px 10px 0px 0px;"></span>
	        <h4 class="modal-title" id="modalContentWindowTitle">HIGHTLIGHT</h4>
	      </div>
	
	      <div class="modal-body">
	      	
	      		<div class="row" id="modalContentWindowRow">
			  
				</div>

	      </div>

	    </div><!-- /.modal-content -->
	  </div><!-- /.modal-dialog -->
	</div><!-- /.modal -->


	<div class="navbar-wrapper">
		<div class="container" id="navContainer" style="width:100%;">

				<nav class="navbar navbar-default transparent_navbar" role="navigation">

				  <!-- Brand and toggle get grouped for better mobile display -->
				  <div class="navbar-header">
				    <button type="button" class="navbar-toggle text-left" data-toggle="collapse" id="buttonMobile" data-target="#bs-example-navbar-collapse-1">
				      <span class="sr-only">Toggle navigation</span>
				      <img id="logoButtonMobile" src="images/logo_mediasoft_notext_white.png">
				    </button>
				  </div>

				  <!-- Collect the nav links, forms, and other content for toggling -->
				  <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				    <ul class="nav navbar-nav" id="navBarMainUl" style="width:100%;">

					<li id="linkHomeMobile"><a href="index.php?lang='<?=$lang?>'">HOME</a></li>
					<?php
						$indice=0;
						$dbInst = new DBAccess2();
						$dbInst->connOpen();
						
						while($positionRow =  mysql_fetch_assoc($menuRows))
						{
							$indice++;
							$url = $positionRow['url'];
							$SxOrDx = "";
							if ($positionRow['SxDx'] == 1) $SxOrDx="left";
							else $SxOrDx="right";
							if($positionRow['valore'] != "LOGO_SEGNAPOSTO")
							{
								//vedo se ci sono sottomenu
								$subMenuItems = $dbInst->getVociMenuTopByPadre($lang,$positionRow['id']);	
								$countTotal = mysql_num_rows($subMenuItems);
								
								
								if($countTotal == 0)
								//if(0 == 0)
								{
									$subString = strstr($url, 'idPage'); 
									$trovato=false;
									if ($subString==("idPage=".$idPage))  $trovato = true;
									//alert(idPage + " - " + url + " - " + trovato);
									
									//alert(Valore + " - No SubItems");
									if ($trovato)
									{
									?>
									<li menu-position="<?=$SxOrDx?>" class="menu-<?=$SxOrDx?>-item" style="width:14.2%;text-shadow:0px 0px 5px rgba(0, 0, 0, 1);"><a href="<?=$positionRow['url']?>&lang=<?=$lang?>"><b><?=$positionRow['valore']?></b></a><div class="underliner" style="top:20px;"/></li>
									<?php
									}
									else
									{
									?>
									<li menu-position="<?=$SxOrDx?>" class="menu-<?=$SxOrDx?>-item" style="width:14.2%;text-shadow:0px 0px 5px rgba(0, 0, 0, 1);"><a href="<?=$positionRow['url']?>&lang=<?=$lang?>"><?=$positionRow['valore']?></a><div class="underliner" style="top:20px;"/></li>
									<?php
									}
								}
								else
								{
									?>
									<li menu-position="<?=$SxOrDx?>" class="dropdown menu-'<?=$SxOrDx?>'-item" style="width:14.2%;text-shadow:0px 0px 6px rgba(0, 0, 0, 1);" id="father_internet"><a href="<?=$positionRow['url']?>" ><?=$positionRow['valore']?></a><div class="underliner" style="top:20px;"></div> 
										<ul id="submenu_<?=$positionRow['id']?>" class="dropdown-menu second-level-submenu-custom">
											<ul class="list-inline">
												<?php		
												while($subMenuRow =  mysql_fetch_assoc($subMenuItems))
												{
													?>
													<li style="text-shadow:0px 0px 6px rgba(0, 0, 0, 1);"><a href="<?=$subMenuRow['url']?>" id="link<?=$subMenuRow['id']?>" menu-level="secondo"><?=$subMenuRow['valore']?></a><div class="underliner" style="top:35px;"/></li>
													<?php
												}
												?>
											</ul>
										</ul>
									</li>
									<?php
								
								}
							
							}
							else
							{
								// quando Valore == 'LOGO_SEGNAPOSTO' carico l'immagine centrale
								?>
								<li id="navLogo" style="width:14.2%;"><a href="index.php?lang=<?=$lang?>"><img id="imageLogo" src="images/logo_mediasoft_white.png"></a></li>
								<?php
							}
						}
						$dbInst->connClose();
					?>										
					

				    </ul>

				  </div><!-- /.navbar-collapse -->
				</nav>

		</div>
		<canvas id="viewport" width="100" height="100" ></canvas>
	</div>


  	<div id="mainCarousel" class="carousel slide">

	  <!-- Indicators -->
	  	<ol class="carousel-indicators" id="carouselIndicators">
			<!--<li data-target="#mainCarousel" data-slide-to="0" class="active"></li>
			<li data-target="#mainCarousel" data-slide-to="1"></li>
			<li data-target="#mainCarousel" data-slide-to="2"></li>
			<li data-target="#mainCarousel" data-slide-to="3"></li>-->
		</ol>

	  <!-- Wrapper for slides -->
	  <div class="carousel-inner" id="carouselInnerContainer">
		

	  </div>

	
	</div>

	<div id="social" style="position:fixed; bottom:0px; margin: 10px;  right:20px; height:20px; z-index:999999;">
		<a href="index.php?lang=IT"><img src="images/l_it.jpg"/></a>&nbsp;
		<a href="index.php?lang=EN"><img src="images/l_en.jpg"/></a>
	</div>

    <div id="footer">
      	
      	<div id="footer-custom-content">
			
      		<div id="inner-footer">
				
				&nbsp;

				<span style="float:left;text-shadow:0px 0px 5px rgba(0, 0, 0, 1);">&copy; <?=date("Y", time());?>  MediaSoft srl P.IVA 04033260755</span>
				<!--<span style="float:right">
					<a href="index.php?lang=IT">IT</a> 
					<a href="index.php?lang=EN">EN</a>
				</span>-->

				
			
			</div>
			
      	</div>
       
    </div>
	<div id="cookieChoiceInfo" style="position: fixed; width: 100%; margin: 0px; left: 0px; bottom: 0px; padding: 4px; z-index: 1000; text-align: center; background-color: rgb(238, 238, 238);"></div>
	
	<script src="jquery-1.10.2.min.js"></script>
	<script src="bootstrap/js/bootstrap.js"></script>
	<script src="functions.js?v=5"></script>
	<script src="modernizr.custom.js"></script>
	<script src="arbor-v0.92/lib/arbor.js"></script> 
	<script type='text/javascript' src="//wurfl.io/wurfl.js"></script> 
	
	<!--<script src="http://d3js.org/d3.v3.min.js" charset="utf-8"></script>
	<script type="text/javascript" src="http://mbostock.github.com/d3/d3.js?2.6.0"></script>
		<script type="text/javascript" src="http://mbostock.github.com/d3/d3.layout.js?2.6.0"></script>
		<script type="text/javascript" src="http://mbostock.github.com/d3/d3.geom.js?2.6.0"></script>
-->
	<script type="text/javascript">	

		/* il cambio carousel sul keypress deve essere disabilitato se viene visualizzata la finestra modale dei contenuti */
		var switchCarouselOnKeyPress = true;


		$.setThemeByCarouselIndex = function(index) {

			var style = carousel_style_by_index[index]; // 0 bianco - 1 nero

			if(style == 0) {

				//$.setThemeWhite();	
				$.setCarouselColor(index, 'white');
				
			} else {

				//$.setThemeBlack();					
				$.setCarouselColor(index, 'black');

			}	
		}

		$(window).load(function() {
			//$.buildTopMenuNew(3); //idPage=3
			//alert(WURFL.is_mobile);
			//alert(WURFL.form_factor);
			
			
			//alert(navigator.appCodeName);
			//alert(navigator.appName);
			//alert(navigator.appVersion);
			//alert(navigator.cookieEnabled);
			//alert(navigator.language);
			//alert(navigator.onLine);
			//alert(navigator.platform);
			//alert(navigator.userAgent);
			//alert(navigator.systemLanguage);

			$.tweakIpadLandscape();
			loadCarousel(3); //idPage=3
			//$.setThemeByCarouselIndex(0);	
			if($.detectMobile() == true) {
				//alert("detect mobile");
				$('.carousel-caption').css("bottom","40%");
				$('.carousel h1').css("font-size","30px");
				$('.carousel h3').css("font-size","20px");
				$('.mediasoft-btn').css("font-size","15px");
				$('.carousel-indicators').css("bottom","10px");
				$('.carousel-indicators li').css("margin-left","5px");
				$('.carousel-indicators li').css("margin-right","5px");
				$('.carousel-indicators li').css("width","35px");
				$('.carousel-indicators li').css("height","35px");
				$('.carousel-indicators active').css("width","40px");
				$('.carousel-indicators active').css("height","40px");
			}
			if ($.detectTablet() == true){
				//alert("detect tablet");
				$('.carousel-caption').css("bottom","10%");
				$('.carousel-indicators').css("bottom","15px");
				$('.carousel-indicators li').css("width","35px");
				$('.carousel-indicators li').css("height","35px");
				$('.carousel-indicators active').css("width","40px");
				$('.carousel-indicators active').css("height","40px");
			}

		});

		

		$(document).ready(function() {

			$.setupNavbar();			
			////$.alignLeftAndRightMenuItems();
			$.centerNavbarByLogo();
			$.setThemeWhite();

			console.log("QUI!!!!!")

			
			$('#modalContentWindow').on('show.bs.modal', function () {
				
				switchCarouselOnKeyPress = false;				

				$("[name='carousel-content']").each(function(index) {   
					$(this).hide();
				});

			});



			$('#modalContentWindow').on('hide.bs.modal', function () {

				switchCarouselOnKeyPress = true;
				
				$("[name='carousel-content']").each(function(index) {   
					$(this).show();
				});
				//faccio (ri)partire il carousel
				$('.carousel').carousel('cycle');
			});




			$('.carousel').carousel({
			    interval: 100000,
				pause: ''
			});

		
			var style = carousel_style_by_index[0];



			if (($.detectMobile() == false) || ($.detectTablet() == true)) {
				$("#mainCarousel").on('slide.bs.carousel', function(evt) {	
					if ($(evt.relatedTarget).index()==0)
					{
						$("#viewport" ).fadeIn(1000);
						if ($.detectTablet() == true)
							$(".carousel-caption").fadeOut("fast", function() { $(this).css("bottom","10%");	}).fadeIn("fast");
						else
							$(".carousel-caption").fadeOut("fast", function() { $(this).css("bottom","2%");	}).fadeIn("fast");
					}
					else
					{
						$("#viewport" ).fadeOut(500);
						$(".carousel-caption").fadeOut("fast", function() {
							$(this).css("bottom","40%"); 
						}).fadeIn("fast");
					}
					
					$.setThemeByCarouselIndex( $(evt.relatedTarget).index() );				
				});
			}
			
			
			
			
			var localStorageOkCookie = localStorage.getItem('okCookie'); 
			var setupTime = localStorage.getItem('setupTime');
			var hours = 7200; // Reset when storage is more than 24hours
			var now = new Date().getTime();	
			var languageCk=$("#langHidden").val();
			if(localStorageOkCookie == null){
				document.getElementById("cookieChoiceInfo").style.display = '';
				if(languageCk == "IT"){
					document.getElementById("cookieChoiceInfo").innerHTML = '<span>I cookie ci permettono di migliorare la tua esperienza utente. Continuando a navigare su questo sito accetti il loro impiego</span><a id="cookieChoiceDismiss" href="javascript:void(0);goToOkCookie();" style="margin-left: 24px;border:solid 1px #cccccc;padding:2px 2px 2px 2px">Ok</a>';
				}else{
					document.getElementById("cookieChoiceInfo").innerHTML = '<span>Cookies allow us to improve your user experience. By continuing to browse this website you agree to their use.</span><a id="cookieChoiceDismiss" href="javascript:void(0);goToOkCookie();" style="margin-left: 24px;border:solid 1px #cccccc;padding:2px 2px 2px 2px">Ok</a>';
				}
			}else{
				document.getElementById("cookieChoiceInfo").style.display = 'none';
				if (setupTime == null) {
					localStorage.setItem('setupTime', now)
				} else {
					if(now-setupTime > hours*60*60*1000) {
						localStorage.clear();
					}
				}
			}			
			
			/*
			$("#mainCarousel").on('slid.bs.carousel', function(evt) {	
				if ($(evt.relatedTarget).index()==0)
				{
					$(".carousel-caption" ).css("bottom","2%");
				}
				else
				{
					//$(".carousel-caption").fadeOut("fast", function() {
					//	$(this).css("bottom","20%"); //Use your CSS here, I did this as an example.
					//}).fadeIn("fast");
					//$(".carousel-caption" ).css("bottom","20%");
				}
			});
			*/

			$(document).keydown(function(e) {

				if(switchCarouselOnKeyPress == true) {	

					if (e.keyCode == 37) { 
					   $('.carousel').carousel('prev');
					   return false;
					}

					if (e.keyCode == 39) { 
					   $('.carousel').carousel('next');
					   return false;
					}

				}

			});



			$('#footer-custom-content').hover( 

				function() {

					if($.detectMobile() == false) {
						$('#inner-footer').fadeIn(250);
					}

				}, function() {

					if($.detectMobile() == false) {
						$('#inner-footer').fadeOut(250);
					}
				}

			);
			
			
			if (($.detectMobile() == false) || ($.detectTablet() == true)) {
				setTimeout(function() {
					var w = $(window).width()*0.95, h = $(window).height()*0.60
					//devo fare la larghezza in proporzione all'altezza
					w=h*2.2;
					var marginLeft = -(w/2);
					$('#viewport').attr('width', w);
					$('#viewport').attr('height', h);


					//riposiziono il canvas al centro
					$('#viewport').css("position","absolute");
					$('#viewport').css("position","absolute");
					$('#viewport').css("top","100px");
					$('#viewport').css("left","50%");
					$('#viewport').css("margin-left",marginLeft);
					
					//var language=$.urlParam('lang');
					var language=$("#langHidden").val();
					
					//$.getScript( "arbor-v0.92/competenze_bianco.js" );

					if(language == "EN"){
						$.getScript( "arbor-v0.92/competenze_bianco_EN.js" );
					}else{
						$.getScript( "arbor-v0.92/competenze_bianco.js" );
					}

				}, 10);
			}	
			


		});

		function goToOkCookie(){
			var now = new Date().getTime();
			localStorage.setItem("okCookie", "ok");  
			localStorage.setItem("setupTime", now); 
			var element = document.getElementById("cookieChoiceInfo");
			element.parentNode.removeChild(element);
		}




	</script>

  </body>

</html>
