<?php
session_start();

$translation = array("IT", "EN");


$translation["IT"]["01"] = "GENNAIO";
$translation["IT"]["02"] = "FEBBRAIO";
$translation["IT"]["03"] = "MARZO";
$translation["IT"]["04"] = "APRILE";
$translation["IT"]["05"] = "MAGGIO";
$translation["IT"]["06"] = "GIUGNO";
$translation["IT"]["07"] = "LUGLIO";
$translation["IT"]["08"] = "AGOSTO";
$translation["IT"]["09"] = "SETTEMBRE";
$translation["IT"]["10"] = "OTTOBRE";
$translation["IT"]["11"] = "NOVEMBRE";
$translation["IT"]["12"] = "DICEMBRE";
$translation["IT"]["scroll_down"] = "CONTINUA";
$translation["IT"]["dettagli"] = "DETTAGLI";
$translation["IT"]["allegati"] = "ALLEGATI";
$translation["IT"]["correlati"] = "PRODOTTI CORRELATI";

$translation["IT"]["vision_img1"] = "images/vision-applicazioniWeb.png";
$translation["IT"]["vision_img2"] = "images/vision-appMobileMulticanale.png";
$translation["IT"]["vision_img3"] = "images/vision-webMarketing.png";
$translation["IT"]["vision_img4"] = "images/vision-realtaAumentata.png";
$translation["IT"]["vision_img5"] = "images/vision-app3D.png";
$translation["IT"]["vision_img6"] = "images/vision-IOT.png";
$translation["IT"]["vision_img7"] = "images/vision-tecnologieEmergenti.png";
$translation["IT"]["vision_img8"] = "images/vision-sistemiCollaborazioneAziendale.png";
$translation["IT"]["vision_img9"] = "images/vision-identitaDigitale.png";
$translation["IT"]["vision_img10"] = "images/vision-gestioneInformazioni.png";

$translation["IT"]["read_more"] = "SCOPRI DI PIU";
$translation["IT"]["contact_copy_cut"] = "Copia e incolla il codice seguente nelle pagine del tuo sito per inserire il banner di Mediasoftonline.com";
$translation["IT"]["contact_copy_cut1"] = "Logo Black - Sfondo Trasparente - 330 x 212 px";
$translation["IT"]["contact_copy_cut2"] = "Logo Black - Sfondo Trasparente - 330 x 212 px";
$translation["IT"]["contact_copy_cut3"] = "Logo Green - Sfondo Trasparente - 330 x 212 px";
$translation["IT"]["contact_copy_cut4"] = "Logo Black Small - Sfondo Trasparente - 72 x 70 px";
$translation["IT"]["contact_copy_cut5"] = "Logo White Small - Sfondo Trasparente - 72 x 70 px";
$translation["IT"]["sidebar_contatti_sedi"] = "SEDI";
$translation["IT"]["sidebar_contatti_contatti"] = "CONTATTI";
$translation["IT"]["sidebar_contatti_lavora"] = "SUPPORTO";
$translation["IT"]["sidebar_contatti_as"] = "AREA STAMPA";
$translation["IT"]["dove_siamo"] = "DOVE SIAMO";
$translation["IT"]["lavora_con_noi"] = "LAVORA CON NOI";
$translation["IT"]["posizioni_aperte"] = "POSIZIONI APERTE";
$translation["IT"]["vieni_a_trovarci"] = "VIENI A TROVARCI";
$translation["IT"]["oppure_scrivici"] = "OPPURE SCRIVICI";
$translation["IT"]["supporto"] = "SUPPORTO";
$translation["IT"]["nome"] = "NOME";
$translation["IT"]["cognome"] = "COGNOME";
$translation["IT"]["nome-cognome"] = "NOME E COGNOME";
$translation["IT"]["prodotto"] = "PRODOTTO";
$translation["IT"]["titolo"] = "TITOLO";
$translation["IT"]["telefono"] = "TELEFONO";
$translation["IT"]["open-ticket"] = "APRI TICKET";
$translation["IT"]["area-stampa"] = "AREA STAMPA";
$translation["IT"]["messaggio"] = "MESSAGGIO";
$translation["IT"]["invia"] = "INVIA";
$translation["IT"]["livello_importanza"] = "GRAVITA'";
$translation["IT"]["messaggio_condizioni_privacy"] = "Ho letto ed accettato le condizioni di privacy.";
$translation["IT"]["errore_privacy_non_selezionata"] = "E' necessario leggere ed accettare le condizioni di privacy";
$translation["IT"]["errore_inserire_il_nome"] = "Inserire il campo 'NOME' correttamente";
$translation["IT"]["errore_inserire_il_cognome"] = "Inserire il campo 'COGNOME' correttamente";
$translation["IT"]["errore_inserire_il_telefono"] = "Inserire il campo 'TELEFONO' correttamente";
$translation["IT"]["errore_inserire_il_prodotto"] = "Inserire il campo 'PRODOTTO' correttamente";
$translation["IT"]["errore_inserire_il_titolo"] = "Inserire il campo 'TITOLO' correttamente";
$translation["IT"]["errore_mail"] = "L'email inserita non è valida";
$translation["IT"]["errore_messaggio"] = "Inserire un testo per il messaggio";
$translation["IT"]["messaggio_inviato"] = "Il messaggio e' stato inviato correttamente. Avremo cura di rispondere il prima possibile. Grazie.";

$translation["IT"]["disclaimer"] = <<<EOF
Esiste un insieme di regole denominato Netiquette che si potrebbe tradurre in "Galateo (Etiquette) della Rete (Net)" che consiste nel rispettare e conservare le risorse di rete e nel rispettare e collaborare con gli altri utenti.
Entrando in Internet si accede ad una massa enorme di dati messi a disposizione il più spesso gratuitamente da altri utenti. Pertanto bisogna portare rispetto verso quanti, spesso in maniera volontaria, hanno prestato e prestano opera per consentire a tutti di accedere a dati ed informazioni che altrimenti sarebbero patrimonio di pochi o addirittura di singoli.
In Internet regna un'anarchia ordinata, intendendo con questo il fatto che non esiste una autorità centrale che regolamenti cosa si può fare e cosa no, né esistono organi di vigilanza. È infatti demandato alla responsabilità individuale il buon funzionamento delle cose.
Si può pertanto decidere di entrare in Internet come persone civili, o al contrario, si può utilizzare la rete comportandosi da predatori o vandali saccheggiando le risorse presenti in essa. Sta a ciascuno decidere come comportarsi.
Risulta comunque chiaro che le cose potranno continuare a funzionare solo in presenza di una autodisciplina dei singoli.
Questo documento fornisce informazioni alla comunità di Internet, ma non fa riferimento ad alcun tipo di standard Internet e può essere redistribuito senza alcuna limitazione.
La nota è stata curata dal Gruppo di Lavoro dell'IETF "Responsable Use of the Network (RUN)".
1.0 Introduzione

Nel passato la popolazione degli utenti telematici "cresciuta" con Internet aveva profonde conoscenze tecniche e comprendeva la natura del mezzo di comunicazione e dei vari protocolli. Oggi la comunità di Internet è costituita da persone del tutto nuove all'ambiente. Questi neofiti non hanno familiarità con la cultura di Internet e non hanno bisogno di sapere cosa siano i protocolli e le reti. Per introdurre rapidamente questi nuovi utenti alla cultura di Internet, questa Guida presenta una serie di comportamenti di base che organizzazioni ed individui possono seguire ed adattare ai propri usi. I singoli dovrebbero essere a conoscenza del fatto che, qualunque sia il fornitore di servizi Internet usato, (acconto privato oppure tramite l'Universita' o l'azienda), ognuna di queste organizzazioni ha delle regole sulla proprietà dei messaggi di posta e dei file, sulle modalità d'inserimento o invio dei testi, e sulle presentazioni personali. Ogni comportamento specifico va verificato con le autorità locali.
Il seguente materiale è stato organizzato in tre sezioni: Comunicazioni interpersonali (One-to-One), comprendente posta e talk; Comunicazioni verso più persone (One-to-Many), comprendente mailing list e NetNews; e Servizi d'informazione, comprendenti ftp, WWW, Wais, Gopher, MUD e MOO. Abbiamo infine incluso una Bibliografia Selezionata da poter usare per ulteriori referenze.
2.0 Comunicazioni interpersonali (posta elettronica, talk)

Con queste termine vengono definite quelle comunicazioni in cui una persona comunica con un'altra persona come se fosse un dialogo faccia a faccia. In genere, conviene seguire le comuni regole di cortesia in ogni situazione, e ciò è doppiamente importante su Internet, dove, ad esempio, linguaggio gestuale e tono di voce vengono a mancare. Per maggiori informazioni sulla Netiquette nelle comunicazioni di posta elettronica e talk vedere le referenze n. 1, 23, 25, 27 nella Bibliografia Selezionata. 
2.1 Linee-guida per gli utenti
2.1.1 Per i messaggi di posta (e-mail)
" A meno che non si abbia accesso a Internet attraverso un fornitore di servizi, conviene verificare con il datore di lavoro la questione della proprietà dei messaggi di posta elettronica. Le leggi al riguardo variano da luogo a luogo.
" A meno che non si usi uno strumento di crittografia (hardware o software), conviene assumere che la posta su Internet non sia sicura. Non inserire mai in un messaggio elettronico quel che non si scriverebbe su una comune cartolina postale.
" Rispettare il copyright sui materiali riprodotti. Quasi ogni paese ha una propria legislazione sul copyright .
" Nel caso di inoltro o re-invio di un messaggio ricevuto, non modificarne il testo. Se si tratta di un messaggio personale e lo si vuole re-inviare ad un gruppo, è il caso di chiedere preventiva autorizzazione. È possibile abbreviare il testo e riportare solo le parti più rilevanti, ma ci si assicuri di fornire le appropriate credenziali.
" Non inviare mai lettere a catena via posta elettronica. Su Internet le lettere a catena sono vietate, pena la revoca dell'acconto. Nel caso ne riceviate una, fatelo immediatamente presente all'amministratore locale.
" Una buona regola generale: conviene esser risparmiatori in quel che si spedisce e liberali in quel che si riceve. Non è il caso di inviare messaggi d'ira ("flames") anche se si dovesse esser provocati. D'altra parte, non c'è da sorprendersi se si diventa vittime di qualche "flame", e in tal caso è prudente non rispondere.
" In generale, è una buona idea controllare tutti i Subject dei messaggi ricevuti prima di rispondere. Altrimenti potrebbe succedere che, ad esempio, una persona che chiede indicazioni d'aiuto o chiarimenti invii un nuovo messaggio per dire "Non importa". Ci si assicuri inoltre che il messaggio a cui si sta per rispondere sia diretto proprio a voi: potreste riceverlo per cc: anziché come destinatario principale.
" È bene rendere le cose facili per chi riceve. Molti programmi di posta (mailer) eliminano i dati inclusi nell'intestazione (header), compreso l'indirizzo del mittente. Per far in modo che la gente sappia chi sia a scrivere, assicurarsi di includere una riga con tali dati a fine messaggio. Conviene creare tale file a parte ed aggiungerlo alla fine di ogni testo inviato (alcuni mailer lo fanno automaticamente). Nell'idioma di Internet, quest'ultimo è noto come il "file di firma" (".sig" o "signature" file). In pratica si tratta dell'equivalente del biglietto da visita (e a seconda della circostanze, è possibile averne più d'uno).
" Fare attenzione quando s'inserisce l'indirizzo del ricevente. Ci sono indirizzi che fanno parte di un gruppo ma sembra appartengano ad una persona soltanto. Ci si assicuri di conoscere la persona a cui s'invia il messaggio.
" Verificare gli indirizzi di cc: quando si risponde. È inutile continuare ad includere questi ultimi se il messaggio è diventato una conversazione tra due persone soltanto.
" In generale, la maggior parte della gente che usa Internet non ha il tempo per rispondere a domande generali su Internet e il suo funzionamento. Non inviare messaggi non voluti per richieste d'informazioni a persone il cui nome è stato visto su elenchi o mailing list.
" Ricordarsi che la gente con cui si comunica è sparsa in tutto il mondo. Se si invia un messaggio che richiede immediata risposta, può darsi che arrivi quando il destinatario stia dormendo. Aspettate che si svegli, vada al lavoro e si colleghi prima di ritenere che la posta non sia arrivata o che non si curi di rispondere.
" Verificare tutti gli indirizzi prima di iniziare discussioni lunghe o personali. È altresì buona norma includere la parola "lungo" alla voce "Subject" in modo che il ricevente possa sapere in precedenza che ci vorrà tempo per leggere e rispondere a quel messaggio. Oltre le 100 righe è considerato "lungo".
" Occorre sapere chi contattare in caso di richiesta d'aiuto. In genere le risorse dovrebbero essere a portata di mano, soprattutto riguardo gente locale in grado di risolvere eventuali problemi di software e di sistema. Ci si assicuri anche di sapere a chi rivolgersi in caso si riceva qualcosa di equivoco o illegale. Quasi tutti i siti hanno un indirizzo di "Postmaster", dove un esperto è in grado di fornire aiuto in caso di problemi con la posta.
" Tenere in mente che il destinatario è un essere umano la cui cultura, lingua ed umore hanno riferimenti diversi dal vostro. Ricordarsi anche che i formati riguardanti date, misure, idiomi possono essere trasmessi in modo incorretto. Va prestata particolare attenzione alle forme di sarcasmo e ironia.
" Usare maiuscole e minuscole. SOLTANTO CON LE MAIUSCOLE È COME SE SI STESSE URLANDO.
" Usare i simboli per dare enfasi. Ecco *cosa* intendo dire. Usare i trattini in basso per le sottolineature. _Guerra e Pace_ è il mio libro preferito.
" Usare le faccine sorridenti (smileys) in sostituzione del tono di voce, ma fatelo con parsimonia.:-)è un esempio di faccina sorridente (guardare lateralmente). Non si ritenga che la semplice inclusione di una tale simbolo renda contento il destinatario su quel che si sta scrivendo o che faccia dimenticare eventuali contenuti offensivi.
" Prima d'inviare risposte d'impulso conviene dormirci su. Se si vogliono esprimere opinioni forti su un certo testo, è il caso di segnalarlo inserendo un FLAME ON/OFF. Ad esempio: FLAME ON: Quest'argomento occupa inutilmente banda di trasmissione. È illogico e senza costrutto. E il resto del mondo è d'accordo con me. FLAME OFF
" Non si includano caratteri per istruzioni particolari o allegati non-ASCII, a meno che questi non siano in MIME o a meno che il vostro mailer non sia in grado di decodificarli. Assicuratevi comunque che il destinatario sia in grado di decodificare tali messaggi.
" Ci si preoccupi di essere brevi senza essere eccessivamente concisi. Includere nella risposta sufficiente materiale originale per esser compresi ma non oltre. È piuttosto brutto rispondere ad un messaggio allegando tutto il testo precedente: tagliar via il materiale irrilevante.
" Limitare la lunghezza delle righe a meno di 65 caratteri e inserire un ritorno del carrello (premendo Invio) alla fine di ogni riga
" Il "Subject" del messaggio dovrebbe riflettere il contenuto del testo.
" Se s'include la firma, la si tenga corta. In genere si sta entro le 4 righe. Tenere in mente che molta gente paga per collegarsi, e più il messaggio è lungo più costa.
" Così come oggi la posta tradizionale può non rimanere privata, allo stesso modo (oggi) posta (e notizie) elettroniche possono essere soggette a vari livelli di intercettazione e falsificazione. Il senso comune aiuta a verificare l'autenticità dei messaggi.
" Se l'importanza di un messaggio sembra giustificarlo, conviene inviare immediatamente al mittente una breve replica di ricevimento, rimandando a più tardi una risposta più esauriente.
" I comportamenti "ragionevoli" via e-mail dipendono dalle relazioni tra le due persone e dal contesto della comunicazione. Consuetudini imparate in un particolare ambiente telematico potrebbero risultare inapplicabili nelle comunicazioni via e-mail con persone su Internet. Si faccia attenzione ad espressioni gergali o acronimi.
" Generalmente i costi relativi alla trasmissione di un messaggio di posta elettronica sono equamente divisi tra mittente e destinatario (o tra i rispettivi sistemi/organizzazioni), al contrario di quanto avviene in altri media quali posta tradizionale, telefono, radio e televisione. Inviare posta elettronica può anche comportare costi aggiuntivi per uso di banda di trasmissione del network, spazio su disco o uso di memoria del sistema. Questa è la ragione economica fondamentale del perché le e-mail pubblicitarie non richieste non sono benvenute (e proibite in molti contesti).
" Ci si assicuri della grandezza del testo inviato. Allegare ampi file come un Postscript o un programma potrebbe impedire l'arrivo del messaggio stesso o un eccessivo utilizzo di risorse. Buona regola è non inviare mai file superiori a 50 Kilobyte. In tal caso, considerare come alternativa il trasferimento di file o la riduzione a diversi file più piccoli da inviare come messaggi separati.
" Non inviare eccessive informazioni quando non richieste.
" Qualora il sistema di posta del vostro sistema lo consenta, si presti attenzione all'inoltro (forward) dei messaggi. Conviene assicurarsi che la configurazione prescelta non preveda l'inoltro ad una serie di host diversi, evitando Così che un testo passi da un computer altro e ad un altro ancora all'infinito.

2.1.2 Per il talk
Il "talk" è una serie di protocolli che consentono a due persone di avere un dialogo interattivo via computer.
" Usare maiuscole e minuscole e la necessaria punteggiatura, come se si stesse scrivendo una lettera o inviando una e-mail.
" Non arrivare in fondo alla riga facendo spezzare le parole al terminale, ma usare il ritorno del carrello (premendo Invio) a fine riga. E non si ritenga che il proprio schermo sia della stessa grandezza degli altri. Buona regola è scrivere non oltre 70 caratteri, e non più di 12 righe (che dovrebbero entrare in un'unica schermata).
" Lasciare un po' di margine; non scrivere immediatamente in cima alla schermata.
" Inserire due ritorni di carrello, ovvero una riga bianca (premendo Invio), per segnalare che avete finito e che l'altra persona può iniziare a scrivere.
" Concludere sempre con una qualche forma di saluto, ed aspettare di vedere il saluto dell'altra persona prima di chiudere la sessione. Ciò è di particolare importanza quando si comunica con qualcuno molto lontano. Tenere in mente che una buona comunicazione si basa sull'ampiezza di banda e sulla velocità della luce.
" Ricordarsi che il talk è un'interruzione delle attività dell'altra persona. Va quindi usato in modo appropriato, e mai con gli sconosciuti.
" Se non arrivano risposte, le ragioni potrebbero essere molte. Non si ritenga che tutto funzioni perfettamente. Non tutte le versioni di talk sono compatibili.
" In automatico il talk chiama due volte il destinatario. Far squillare una o due volte, poi chiudere.
" Se la persona non risponde, provare un'altra volta. Usare finger per verificare la correttezza della chiamata. Se la persona non risponde ancora, inutile insistere.
" Con talk si misura l'abilità nella battitura. Se si digitano le parole lentamente facendo molti errori, in genere non vale la pena di correggerli, poiché l'altra persona riesce a capire cosa s'intende dire.
" Si faccia attenzione quando si stanno seguendo più d'una sessione di talk!
2.2 Note per gli amministratori

" Assicurarsi di aver distribuito informazioni scritte sulle linee-guida relative ad eventuali situazioni su traffici illegali o impropri e falsificazioni.
" Gestire ogni richiesta in tempi corretti - entro il successivo giorno lavorativo.
" Replicare prontamente a chi segnala d'aver ricevuto messaggi impropri o illegali. Questioni riguardanti lettere a catena vanno affrontate immediatamente.
" Illustrare agli utenti le regole del sistema, incluso lo spazio disponibile su disco. Ci si accerti che ogni utente comprenda bene il significato delle richieste di file via posta, tra cui riempimento dei dischi, aumento delle bollette telefoniche, ritardi postali, ecc.
" Assicurarsi di avere acconti riservati sia per il "Postmaster" che per il "Root" e che qualcuno legga regolarmente la posta ivi indirizzata.
" Analizzare le lamentele degli utenti a mente aperta. Ricordarsi che gli indirizzi potrebbero essere stati intercettati e falsificati.
3.0 Comunicazioni verso più persone (Mailing List, NetNews)

Ogni volta che avviene una comunicazione rivolta a più persone (One-to-Many) dovrebbero applicarsi le norme già viste per la posta. In fondo comunicare con molte persone attraverso un messaggio postale o un testo equivale a comunicare con una sola persona, con in più la possibilità di offendere molta più gente che nella comunicazione uno-a-uno. È quindi molto importante conoscere quanto meglio possibile il pubblico a cui vengono diretti i messaggi.

3.1 Linee-guida per gli utenti

3.1.1 Linee-guida generali per mailing list e NetNews
" Seguire regolarmente mailing list e newsgroup per uno-due mesi prima d'inserire testi. Ciò permette di comprendere la cultura del gruppo.
" Non considerare responsabile l'amministratore del sistema per i comportamenti degli utenti.
" Tenere in mente che ogni testo verrà letto da un'ampio pubblico. Compreso forse il vostro attuale o futuro datore di lavoro. Fare attenzione a ciò che si scrive. Ricordarsi anche sia le mailing lists che i newsgroup vengono archiviati con una certa frequenza, e che le vostre parole possono essere conservate per molto tempo in un'area accessibile da molte persone.
" Occorre ritenere che ogni singolo individuo esprima le proprie opinioni, e non rappresenta la propria organizzazione (a meno che ciò non venga esplicitamente detto).
" Ricordarsi che sia la posta che le news riguardano in qualche modo le risorse del sistema. Prestare attenzione a qualunque regola specifica relativa all'uso di tali risorse esista nell'ambito del vostro sistema/organizzazione.
" Messaggi ed articoli dovrebbero essere brevi e centrati sull'argomento. Non scrivere su temi off-topic, non ripetersi e non inviare posta né inserire testi giusto per sottolineare gli errori di battitura o i refusi degli altri. Sono questi i comportamenti che, ben più di altri, testimoniano dell'immaturità dei principianti.
" La riga del "Subject" dovrebbe seguire le consuetudini del gruppo.
" Falsificazioni e imbrogli non rientrano tra i comportamenti approvati.
" Su alcune liste e newsgroup la pubblicità è benvenuta, mentre su altri è aborrita! Questo è un'altro esempio di come sia importante avere piena conoscenza del gruppo dove s'inseriscono i testi. Fare pubblicità non richiesta quando è chiaramente off-topic garantisce il ricevimento assicurato di messaggi particolarmente duri ed arrabbiati.
" Quando si replica ad un messaggio o ad un testo, ci si assicuri di riassumere il messaggio originale ad inizio testo, o di includere appena il testo sufficiente per spiegare il contesto. Ciò consentirà a chi legge di capire perfettamente la vostra replica. Poiché soprattutto nelle NetNews i testi vengono distribuiti da un host all'altro, può succedere di vedere apparire la risposta ancor prima dell'originale. Fornire quindi il contesto giusto aiuta la comprensione generale, ma senza inserire l'intero testo originale!
" Come già detto per la posta, ci si assicuri d'inserire la firma a fine testo. Ciò garantisce che, anche in presenza di programmi di posta (mailer) o per la lettura delle news (newsreader) che tagliano via i dati dell'intestazione, resteranno visibili le informazioni necessarie per chi volesse contattarvi.
" Si faccia attenzione nel replicare ad testo. Spesso le risposte tornano al mittente che ha originato il testo, e in molti casi si tratta dell'indirizzo della lista o del gruppo! Potrebbe accadere d'inviare una risposta personale ad una gran numero di persone, nell'imbarazzo generale. È meglio digitare l'indirizzo esatto anziché usare l'opzione "replica" (reply).
" Avvisi di ricevimento, di non-ricevimento e/o di temporanea assenza non sono né pienamente standardizzati né del tutto affidabili, stante la notevole diversità dei sistemi di gestione posta su Internet. Risultano invasivi se inviati alle mailing list, ed alcuni considerano gli avvisi di ricevimento un'invasione della privacy. In breve, non usare mai tali avvisi.
" Se si scopre che un vostro messaggio personale è stato erroneamente recapitato ad una lista o gruppo, inviate un messaggio di scuse.
" Nel caso ci si trovi in disaccordo con una persona, meglio proseguire la discussione attraverso e-mail personali piuttosto che nella lista o gruppo. Se la discussione verte su un punto d'interesse generale, è sempre possibile riassumerla in seguito per gli altri.
" Non ci si faccia coinvolgere in nessuna "flame war". Non rispondere a materiali incendiari.
" Si eviti di inviare messaggi o inserire testi che non sono altro che gratuite repliche ad altre repliche.
" Si faccia attenzione ai caratteri mono-spazio e ai diagrammi, che possono apparire in modi diversi sia su sistemi differenti che con differenti mailer dello stesso sistema.
" Esistono newsgroup e Mailing List che discutono di argomenti ed interessi anche molto diversi tra loro. I vari gruppi rappresentano l'ampia diversità di stili di vita, religioni e culture. Inserire testi o inviare messaggi ad un gruppo il cui punto di vista risulta offensivo per voi soltanto per dire quanto siano offensivi è un comportamento inaccettabile. Messaggi che procurano molestie sessuali o razziali possono anche avere conseguenze legali. Oggi esistono diversi programmi in grado di filtrare quei testi che risultano equivoci.

3.1.2 Linee-guida per le Mailing List

Ci sono vari modi per trovare informazioni su quali mailing list esistono su Internet e su come iscriversi. Ci si assicuri di aver chiara la politica del vostro sistema/organizzazione riguardo iscrizione e inserimento di testi in tali liste. In generale è sempre meglio dare un'occhiata alle risorse locali prima di cercare informazioni su Internet. Esistono comunque una serie di file inseriti periodicamente in news.answersche forniscono l'elenco delle mailing list disponibili su Internet e relative modalità d'iscrizione. Si tratta di un'incalcolabile fonte d'informazioni per trovare liste su qualsiasi argomento. Vedere anche le voci n. 9, 13, 15 della Bibliografia Selezionata.
" Inviare i messaggi d'iscrizione e/o di cancellazione (subscribe, unsubscribe) all'indirizzo appropriato. Anche se il software di alcune mailing list è abbastanza intelligente da isolarli e smistarli, non con tutte è possibile farlo. Fa parte della responsabilità di ciascuno comprendere come funziona ogni lista, compreso l'invio dei vari messaggi agli indirizzi giusti. Nonostante molte liste abbiano per convenzione un indirizzo "-request" per l'invio dei messaggi iscrizione e/o cancellazione (subscribe,unsubscribe), non tutte lo prevedono. Ci si assicuri di essere a conoscenza delle convenzioni in vigore nella lista a cui ci si vuole iscrivere.
" Salvare e conservare i messaggi d'iscrizione di ogni lista a cui si partecipa. In genere vi si trovano anche le istruzioni per cancellare l'iscrizione.
" In genere non è possibile recuperare un messaggio una volta inviato. Neppure l'amministratore del sistema sarà in grado di farlo. Ciò significa che occorre essere davvero sicuri che si vuole inviare il messaggio così com'è.
" L'opzione di replica automatica (auto-reply) di molti programmi di gestione posta (mailer) è utile per comunicazioni interne, ma piuttosto disturbante quando arriva all'intera lista. Date un'occhiata agli indirizzi nel campo "Reply-To" quando si replica ai messaggi della lista: molti di questi andranno a tutti i membri della lista stessa.
" Non inviare alla mailing list ampi file raggiungibili via Uniform Resource Locator (URL) o ftp. È possibile inviarlo diviso in vari file, purché ciò rientri nella cultura del gruppo. Se non si è certi su cosa fare, chiedere prima.
" Considerare la cancellazione dell'iscrizione o l'opzione "nomail" (se disponibile) quando si sa che non sarà possibile controllare la posta per un certo periodo.
" Quando s'invia un messaggio più d'una mailing list, specialmente se queste sono strettamente collegate, scusarsi per il cross-posting.
" Quando si chiede qualcosa, ci si assicuri di inserire un breve riassunto. E lo si faccia seriamente, anziché inviare una serie di stralci dai messaggi ricevuti.
" Alcune mailing list sono private. Non inviare messaggi a tali liste se non invitati. Non riportare in pubblico i testi di tali liste.
" Se si interviene in un'accesa discussione, ci si concentri sugli argomenti piuttosto che sulle personalità coinvolte.

3.1.3 Linee-guida per le NetNews

NetNews è un sistema distribuito globalmente che consente alla gente di comunicare su argomenti d'interesse specifico. È diviso in gerarchie, con le seguenti divisioni primarie: sci - discussioni connesse con la scienza; comp - discussioni intorno ai computer; news - discussioni relative alle NetNews stesse; rec - attività ricreative; soc - questioni sociali; talk - interminabili discussioni ad ampio raggio; biz - per faccende genericamente commerciali; alt - la gerarchia "alternativa". Alt è chiamata così anche perché per creare un gruppo alt non occorre seguire la medesima procedura necessaria per i newsgroup delle altre gerarchie. Esistono poi gerarchie regionali, quelle ampiamente distribuite come bionet, ed è anche possibile avere un gruppo per specifiche attività imprenditoriali. Recentemente è stata aggiunta la gerarchia "humanities", e col passare del tempo è probabile altre ancora se ne aggiungeranno. Per informazioni e discussioni sulle News vedere le voci n. 2, 8, 22, 23 della Bibliografia Selezionata.
" Nel linguaggio delle NetNews, "Posting" indica l'inserimento di un nuovo articolo in un gruppo, o la risposta ad un testo o articolo (post) inserito da qualcun altro. "Cross-Posting" indica l'inserimento di un testo in più d'un gruppo. Avvisare preventivamente i lettori nel caso si effettui un Cross-Posting in un gruppo, oppure se si usa il "Followup-To:" nell'intestazione del messaggio! Chi legge ritiene generalmente che il messaggio e/o i seguiti (followups) siano per quel gruppo. È bene segnalare ogni modifica a tale consuetudine.
" Leggere per intero l'andamento della discussione (il cosiddetto "thread") prima di fare interventi. Evitare di inserire messaggi tipo "Anch'io", il cui contenuto si limita ad esprimere accordo con il testo precedente. Il contenuto di un seguito dovrebbe andare oltre la semplice ripresa del testo originale.
" Inviare un e-mail quando la risposta ad una domanda riguarda una sola persona. Ricordarsi che le News vengono distribuite globalmente ed è probabile che non tutto il mondo sia interessato ad una replica personale. Non è tuttavia il caso di esitare ad inserire qualcosa che sia d'interesse generale.
" Controllare la sezione "Distribution" dell'intestazione, senza però farvi troppo affidamento. A causa del complesso metodo con cui vengono distribuite le News il campo "Distribution" non è del tutto affidabile. Ma se ci appresta ad inserire un testo che interessa un numero limitato di lettori, conviene usare la riga per la distribuzione cercando di limitarla a tali persone. Ad esempio, impostare la riga "Distribution" su "nj" nel caso l'articolo da inserire interessi soltanto i lettori del New Jersey.
" Se si ritiene che un articolo sia d'interesse per più di un newsgroup, ci si assicuri di effettuare il CROSSPOST anziché inserirlo singolarmente in tali gruppi. È probabile che saranno non oltre cinque o sei i gruppi con interessi simili.
" Prima d'inserire una domanda, è bene cercare in altre referenze (manuali tecnici, giornali, file d'aiuto). Avanzare richieste in un newsgroup quando le risposte sono facilmente reperibili altrove può generare il brusco messaggio di "RTFM" (read the fine manual - leggiti quel fantastico manuale, anche se in genere la lettera "f" implica un termine ben più volgare, [sia in inglese che in italiano]).
" Nonostante esistano newsgroup che accettano pubblicità, in genere è considerato poco meno che criminale fare pubblicità a prodotti off-topic. Inserire un annuncio pubblicitario in tutti i gruppi garantisce la pressoché certa cancellazione all'accesso.
" Nel caso si scoprano errori in un proprio testo, conviene cancellarlo appena possibile.
" NON si cerchi di cancellare altri articoli diversi dai propri. Contattare l'amministratore per sapere come fare a rimuovere i propri testi, o nel caso occorra cancellare testi quali lettere a catena.
" Se un testo inserito non appare immediatamente, non significa che ci sia qualche intoppo e che bisogna inviarlo di nuovo; conviene aspettare.
" Alcuni gruppi permettono (ed alcuni gradiscono) l'inserimento di testi che in altre circostanze sarebbero considerati quantomeno equivoci. Nuovamente, non esiste alcuna garanzia che tutte le persone del gruppo apprezzeranno quel materiale. Nell'inserire simili testi, usare la utility Rotate (che sposta in rotazione i caratteri del testo di 13 posti nell'alfabeto) così da evitare d'offendere qualcuno. Si può usare, ad esempio, la utility Rot13 per Unix.
" Nei gruppi in cui si discute di film o libri è considerato vitale segnalare quei testi dal contenuto particolare con il termine "Spoilers", da inserire nella riga del Subject:. Per nasconderlo in qualche modo si possono poi aggiungere delle righe bianche ad inizio testo oppure usare la utility Rotate.
" La falsificazione di articoli è generalmente censurata. Ci si può proteggere dalle falsificazioni usando del software che genera sicure firme digitali, come PGP (negli USA).
" L'inserimento di testi da un server anonimo è accettato in alcuni newsgroup e rifiutato in altri. Materiali inappropriati se inseriti a proprio nome restano tali anche se inseriti anonimamente.
" Ci si aspetti qualche ritardo nel veder apparire testi inseriti in un gruppo moderato. Potrebbe accadere che il moderatore decida di cambiarne il Subject per renderlo conforme ad un particolare thread.
" Si eviti di farsi coinvolgere in "flame wars". Non inserire né replicare a critiche infiammate.

3.2 Linee-guida per gli amministratori

3.2.1 Questioni generali

" Chiarire ogni questione sulla politica del sito o del sistema riguardo l'iscrizione ai newsgroup e alle mailing list.
" Chiarire ogni questione sulla politica del sito o del sistema riguardo l'inserimento di testi nei newsgroup e nelle mailing list, compreso l'uso dei "disclaimer" nelle firme.
" Chiarire e pubblicizzare la politica sull'archiviazione. (Per quanto tempo vengono conservati gli articoli?)
" Avviare indagini immediate ed a mente aperta riguardo accuse da parte di utenti.
" Ci si assicuri di monitorare regolarmente lo stato di salute del sistema.
" Decidere per quanto tempo archiviare i "log" del sistema, e diffonderne la politica.
3.2.2 Mailing List
" Mantenere aggiornate le mailing list per evitare problemi di posta respinta ("bouncing mail").
" In caso di problemi, dare aiuto ai proprietari delle liste.
" Informare i proprietari delle liste riguardo i periodi di chiusura per manutenzione o altro.
" Ci si assicuri di avere acconti "-request" per le iscrizioni alla lista e per l'amministrazione.
" Ci si assicuri che tutti i gateway della posta funzionino correttamente.

3.2.3. NetNews

" Pubblicizzare la natura dei gruppi che si ricevono; se non tutti risultano disponibili, la gente vorrà conoscerne le ragioni.
" Ci si ricordi che avere vari clienti News Reader può causare problemi di cui verranno poi accusati i Server delle News.
" Si sbrighino immediatamente le richieste degli utenti, soprattutto quando richiedano la cancellazione di propri testi o di testi non validi quali lettere a catena.
" Si abbiano acconti diversi per "Usenet", "Netnews" e "News", e ci si assicuri che qualcuno ne legga regolarmente la posta.
3.3 Linee-guida per i moderatori
3.3.1 Linee-guida generali
" Ci si assicuri che il file delle Frequestly Asked Questions (FAQ) venga inserito a intervalli regolari. Includere (o far includere, se il file delle FAQ è curato da altri) le linee-guida per l'inserimento di articoli/messaggi.
" Ci si assicuri di avere un buon messaggio di benvenuto, con incluse le informazioni per l'iscrizione e la relativa cancellazione.
" Inserire regolarmente le linee-guida relative ai newsgroup.
" Tenere aggiornati mailing list e newsgroup. Inserire tempestivamente i messaggi. Designare un sostituto in caso di assenza per vacanza o altro.
4.0 Servizi d'informazione (Gopher, Wais, WWW, ftp, telnet)

Nella recente storia di Internet, c'è stata l'esplosione di nuovi e diversi servizi d'informazione. Gopher, Wais, World Wide Web (WWW), Multi-User Dimensions (MUDs), Multi-User Dimensions che sono Object Oriented (MOOs) sono alcune di tali nuove aree. Nonostante la capacità di trovare informazioni stia diventando enorme, rimane la costante del "Caveat Emptor" Per maggiori informazioni in merito ai suddetti servizi, vedere le referenze 14 e 23 della Bibliografia Selezionata.

4.1 Linee-guida per gli utenti

4.1.1. Linee-guida generali

" Ricordarsi che tali servizi appartengono a qualcun altro. Le persone che pagano bollette e fatture stabiliranno le regole per il loro uso. L'informazione può essere libera e gratuita - oppure no! Ci si assicuri di verificarlo.
" Nel caso di problemi con un qualsiasi servizio d'informazioni, iniziare i controlli a livello locale: verificare le configurazioni dei file, le impostazioni del software, i collegamenti del network, etc. Ciò va fatto prima di ritenere che il problema riguardi il fornitore o che sia colpa dello stesso.
" Nonostante si usino delle convenzioni per stabilire i nomi dei file, è bene non farvi troppo affidamento. Ad esempio, non sempre un file ".doc" è un file in Word.
" Anche i servizi d'informazione usano nomi convenzionali, quali www.xyz.com. Pur essendo utile esserne a conoscenza, di nuovo, non è il caso di affidarsi eccessivamente a tali convenzioni.
" È importante conoscere quei nomi di file che funzionano bene sul proprio sistema.
" Occorre conoscere le convenzioni usate per fornire informazioni durante le sessioni. I siti FTP hanno in genere dei file chiamati README ad inizio directory contenenti informazioni sui file disponibili. Ma non si dia per scontato che tali file siano necessariamente aggiornati e/o accurati.
" In generale, non si ritenga che qualunque informazione sia accurata e/o aggiornata. Ricordarsi che le nuove tecnologie consentono praticamente a chiunque di diventare editori, ma non tutti hanno ancora scoperto le responsabilità che accompagnano la pubblicazione dei materiali.
" Ricordarsi che, a meno che non si sia certi sull'utilizzo di tecnologie per la sicurezza e l'autenticazione, ogni informazione trasmessa attraverso Internet passa a cielo aperto, senza alcuna protezione nei confronti di "curiosi" e falsari.
" Poiché Internet è sparsa in tutto il globo, tenere a mente che i servizi d'informazione possono riflettere cultura e stili di vita piuttosto diversi dai propri. Quei materiali che vi sembrano offensivi potrebbero nascere in un'area geografica che li ritiene accettabili. Occorre avere una mente aperta.
" Quando si vuol prelevare un dato da un server molto noto, conviene utilizzare un server "mirror" vicino, qualora ne venga fornito l'elenco.
" Non usare il sito FTP di qualcuno per depositarvi materiale che dovrebbe esser prelevato da altre persone. Ciò è definito "dumping" e in genere viene considerato un comportamento inaccettabile.
" Nel caso di richiesta di aiuto per problemi con un sito, ci si assicuri di fornire quante più informazioni possibili per far sì che il problema possa esser risolto.
" Quando si organizza il proprio sistema d'informazioni, come una homepage, ci si assicuri di verificare con il proprio amministratore quali siano le linee-guida locali in vigore.
" Si cerchi di rendere meno pesante il traffico sui siti più noti, evitando di collegandosi nelle ore di punta.

4.1.2 Linee-guida per servizi interattivi in tempo reale (MUDs MOOs IRC)

" Come per gli altri ambienti, è saggio innanzitutto "ascoltare" per conoscere la cultura del gruppo.
" Non è necessario salutare personalmente chiunque sia presente su un canale o in una stanza. In genere basta un solo "Salve" o un saluto equivalente. Non è un comportamento accettabile utilizzare la funzione automatica del client per salutare le persone.
" Avvisare i partecipanti nel caso s'intenda inviare grandi quantità di dati. Se tutti acconsentono a riceverli, si può procedere, ma inviare informazioni non desiderate senza avvisare preventivamente è considerata azione negativa, proprio come con la posta.
" Non dare per scontato che persone sconosciute abbiano voglia di parlare con voi. Se sorge la necessità d'inviare messaggi privati a degli sconosciuti, bisogna allora accettare serenamente il fatto che questi possano essere occupati o che semplicemente non abbiano voglia di chiacchierare con voi.
" Rispettare le linee-guida del gruppo. Cercare il materiale di presentazione del gruppo, che potrebbe anche trovarsi in un sito ftp connesso.
" Non fare domande su questioni personali quali età, sesso, dimora. Dopo aver stabilito una certa confidenza con qualcuno, tali domande potranno essere più appropriate, ma molte persone esitano a dare tali dati a gente che non conoscono.
" Se un utente usa uno pseudonimo, alias o soprannome, tale desiderio d'anonimità va rispettato. Anche se si è amici di tale persona, è più gentile usare il soprannome. Non usare il nome vero senza permesso.

4.2 Linee-guida per gli amministratori

4.2.1 Linee-guida generali

" Stabilire con chiarezza cosa è disponibile per esser copiato e cosa no.
" Definire con chiarezza cosa è disponibile sul vostro sito e nella vostra organizzazione. Ci si assicuri che le politiche generali siano ben chiare.
" Mantenere aggiornate le informazioni, soprattutto i file README, da tenere soltanto in formato ASCII, solo testo.
" Diffondere l'eventuale elenco dei siti "mirror", allegando una dichiarazione sul copyright ad essi relativo. Se possibile, indicarne le previsioni sugli aggiornamenti.
" Ci si assicuri che le informazioni note (e massicce) abbiano un'adeguata ampiezza di banda.
" Usare i nomi convenzionali per le estensioni dei file - .txt per testi in ASCII; .html o .htm per HTML;.ps per Postscript; .pdf per Portable Document Format;.sgml o .sgm per SGML; .exe per eseguibili non-Unix, etc.
" Per i file da prelevare, si cerchi di comporne nomi unici con i primi otto caratteri.
" Nel fornire informazioni, ci si assicuri che il vostro sito abbia qualcosa di unico da offrire. Evitare di metter su un servizio d'informazioni che semplicemente rimandi ad altri servizi su Internet.
" Non si rimandi ad altri siti senza prima aver chiesto.
" Ricordarsi che mettere su un servizio d'informazioni significa molto più che progettare e realizzare. C'è anche la manutenzione.
" Assicurarsi che il materiale inserito sia appropriato nei riguardi della vostra organizzazione.
" Provare le applicazioni con strumenti diversi. Non si ritenga che tutto funzioni bene se avete fatto prove con un client soltanto. Ci si basi inoltre sulle tecnologie più modeste e non creare applicazioni da usare esclusivamente con un interfaccia grafico.
" Cercare di avere una visione complessiva delle informazioni offerte. Assicurarsi che forma e contenuto rimangano inalterate nelle varie applicazioni.
" Fare attenzione all'invecchiamento delle informazioni disponibili. Ci si assicuri di datare le informazioni particolari, e si vigili affinché siano mantenute bene.
" Le restrizioni sull'esportazione variano da paese a paese. Assicurarsi di conoscere le implicazioni sull'esportazione quando si inseriscono dei testi.
" Informare gli utenti su quel che s'intende fare con ogni tipo d'informazione raccolta, come commenti sul WWW. Occorre avvisare le persone nel caso s'intenda usare qualunque loro affermazione, anche se la si rende passivamente disponibile ad altri utenti.
" Ci si assicuri che sia di conoscenza generale la politica in vigore sui servizi d'informazione per gli utenti, tipo le homepage. 
EOF;



$translation["EN"]["01"] = "JANUARY";
$translation["EN"]["02"] = "FEBRUARY";
$translation["EN"]["03"] = "MARCH";
$translation["EN"]["04"] = "APRIL";
$translation["EN"]["05"] = "MAY";
$translation["EN"]["06"] = "JUNE";
$translation["EN"]["07"] = "JULY";
$translation["EN"]["08"] = "AUGUST";
$translation["EN"]["09"] = "SEPTEMBER";
$translation["EN"]["10"] = "OCTOBER";
$translation["EN"]["11"] = "NOVEMBER";
$translation["EN"]["12"] = "DECEMBER";
$translation["EN"]["scroll_down"] = "SCROLL DOWN";
$translation["EN"]["dettagli"] = "VIEW MORE";
$translation["EN"]["allegati"] = "ATTACHMENTS";
$translation["EN"]["correlati"] = "RELATED PRODUCTS";

$translation["EN"]["vision_img1"] = "images/vision-applicazioniWeb_EN.png";
$translation["EN"]["vision_img2"] = "images/vision-appMobileMulticanale_EN.png";
$translation["EN"]["vision_img3"] = "images/vision-webMarketing_EN.png";
$translation["EN"]["vision_img4"] = "images/vision-realtaAumentata_EN.png";
$translation["EN"]["vision_img5"] = "images/vision-app3D_EN.png";
$translation["EN"]["vision_img6"] = "images/vision-IOT_EN.png";
$translation["EN"]["vision_img7"] = "images/vision-tecnologieEmergenti_EN.png";
$translation["EN"]["vision_img8"] = "images/vision-sistemiCollaborazioneAziendale_EN.png";
$translation["EN"]["vision_img9"] = "images/vision-identitaDigitale_EN.png";
$translation["EN"]["vision_img10"] = "images/vision-gestioneInformazioni_EN.png";

$translation["EN"]["read_more"] = "READ MORE";
$translation["EN"]["contact_copy_cut"] = "Copy and paste the following code in the pages of your website to insert the banner of Mediasoftonline.com";
$translation["EN"]["contact_copy_cut1"] = "Logo Black - Transparent Background - 330 x 212 px";
$translation["EN"]["contact_copy_cut2"] = "Logo Black - Transparent Background - 330 x 212 px";
$translation["EN"]["contact_copy_cut3"] = "Logo Green - Transparent Background - 330 x 212 px";
$translation["EN"]["contact_copy_cut4"] = "Logo Black Small - Transparent Background - 72 x 70 px";
$translation["EN"]["contact_copy_cut5"] = "Logo White Small - Transparent Background - 72 x 70 px";
$translation["EN"]["sidebar_contatti_sedi"] = "WHERE WE ARE";
$translation["EN"]["sidebar_contatti_contatti"] = "CONTATS";
$translation["EN"]["sidebar_contatti_lavora"] = "WORK WITH US";
$translation["EN"]["sidebar_contatti_as"] = "PRESS AREA";
$translation["EN"]["dove_siamo"] = "WHERE WE ARE";
$translation["EN"]["lavora_con_noi"] = "WORK WITH US";
$translation["EN"]["posizioni_aperte"] = "OPEN POSITIONS";
$translation["EN"]["vieni_a_trovarci"] = "FIND US";
$translation["EN"]["oppure_scrivici"] = "OR WRITE US";
$translation["EN"]["supporto"] = "SUPPORT";
$translation["EN"]["nome"] = "FIRST NAME";
$translation["EN"]["cognome"] = "LAST NAME";
$translation["EN"]["nome-cognome"] = "FIRST AND LAST NAME";
$translation["EN"]["area-stampa"] = "PRESS AREA";
$translation["EN"]["open-ticket"] = "OPEN TICKET";
$translation["EN"]["prodotto"] = "PRODUCT";
$translation["EN"]["titolo"] = "TITLE";
$translation["EN"]["telefono"] = "PHONE NUMBER";
$translation["EN"]["messaggio"] = "MESSAGE";
$translation["EN"]["invia"] = "SUBMIT";
$translation["EN"]["livello_importanza"] = "SEVERITY";
$translation["EN"]["messaggio_condizioni_privacy"] = "I have read and accepted the terms and conditions.";
$translation["EN"]["errore_privacy_non_selezionata"] = "Is mandatory to read and accept the terms and conditions";
$translation["EN"]["errore_inserire_il_nome"] = "'FIRST NAME' is mandatory";
$translation["EN"]["errore_inserire_il_cognome"] = "'LAST NAME' is mandatory";
$translation["EN"]["errore_inserire_il_telefono"] = "'PHONE NUMBER' is mandatory";
$translation["EN"]["errore_inserire_il_titolo"] = "'TITLE' is mandatory";
$translation["EN"]["errore_inserire_il_prodotto"] = "'PRODUCT' is mandatory";
$translation["EN"]["errore_mail"] = "Email is uncorrect";
$translation["EN"]["errore_messaggio"] = "Write something in 'MESSAGE' field";
$translation["EN"]["messaggio_inviato"] = "Your message has been sent succesfully. We will reply as soon as possible. Thank you.";

$translation["EN"]["disclaimer"] = <<<EOF
Esiste un insieme di regole denominato Netiquette che si potrebbe tradurre in "Galateo (Etiquette) della Rete (Net)" che consiste nel rispettare e conservare le risorse di rete e nel rispettare e collaborare con gli altri utenti.
Entrando in Internet si accede ad una massa enorme di dati messi a disposizione il più spesso gratuitamente da altri utenti. Pertanto bisogna portare rispetto verso quanti, spesso in maniera volontaria, hanno prestato e prestano opera per consentire a tutti di accedere a dati ed informazioni che altrimenti sarebbero patrimonio di pochi o addirittura di singoli.
In Internet regna un'anarchia ordinata, intendendo con questo il fatto che non esiste una autorità centrale che regolamenti cosa si può fare e cosa no, né esistono organi di vigilanza. È infatti demandato alla responsabilità individuale il buon funzionamento delle cose.
Si può pertanto decidere di entrare in Internet come persone civili, o al contrario, si può utilizzare la rete comportandosi da predatori o vandali saccheggiando le risorse presenti in essa. Sta a ciascuno decidere come comportarsi.
Risulta comunque chiaro che le cose potranno continuare a funzionare solo in presenza di una autodisciplina dei singoli.
Questo documento fornisce informazioni alla comunità di Internet, ma non fa riferimento ad alcun tipo di standard Internet e può essere redistribuito senza alcuna limitazione.
La nota è stata curata dal Gruppo di Lavoro dell'IETF "Responsable Use of the Network (RUN)".
1.0 Introduzione

Nel passato la popolazione degli utenti telematici "cresciuta" con Internet aveva profonde conoscenze tecniche e comprendeva la natura del mezzo di comunicazione e dei vari protocolli. Oggi la comunità di Internet è costituita da persone del tutto nuove all'ambiente. Questi neofiti non hanno familiarità con la cultura di Internet e non hanno bisogno di sapere cosa siano i protocolli e le reti. Per introdurre rapidamente questi nuovi utenti alla cultura di Internet, questa Guida presenta una serie di comportamenti di base che organizzazioni ed individui possono seguire ed adattare ai propri usi. I singoli dovrebbero essere a conoscenza del fatto che, qualunque sia il fornitore di servizi Internet usato, (acconto privato oppure tramite l'Universita' o l'azienda), ognuna di queste organizzazioni ha delle regole sulla proprietà dei messaggi di posta e dei file, sulle modalità d'inserimento o invio dei testi, e sulle presentazioni personali. Ogni comportamento specifico va verificato con le autorità locali.
Il seguente materiale è stato organizzato in tre sezioni: Comunicazioni interpersonali (One-to-One), comprendente posta e talk; Comunicazioni verso più persone (One-to-Many), comprendente mailing list e NetNews; e Servizi d'informazione, comprendenti ftp, WWW, Wais, Gopher, MUD e MOO. Abbiamo infine incluso una Bibliografia Selezionata da poter usare per ulteriori referenze.
2.0 Comunicazioni interpersonali (posta elettronica, talk)

Con queste termine vengono definite quelle comunicazioni in cui una persona comunica con un'altra persona come se fosse un dialogo faccia a faccia. In genere, conviene seguire le comuni regole di cortesia in ogni situazione, e ciò è doppiamente importante su Internet, dove, ad esempio, linguaggio gestuale e tono di voce vengono a mancare. Per maggiori informazioni sulla Netiquette nelle comunicazioni di posta elettronica e talk vedere le referenze n. 1, 23, 25, 27 nella Bibliografia Selezionata. 
2.1 Linee-guida per gli utenti
2.1.1 Per i messaggi di posta (e-mail)
" A meno che non si abbia accesso a Internet attraverso un fornitore di servizi, conviene verificare con il datore di lavoro la questione della proprietà dei messaggi di posta elettronica. Le leggi al riguardo variano da luogo a luogo.
" A meno che non si usi uno strumento di crittografia (hardware o software), conviene assumere che la posta su Internet non sia sicura. Non inserire mai in un messaggio elettronico quel che non si scriverebbe su una comune cartolina postale.
" Rispettare il copyright sui materiali riprodotti. Quasi ogni paese ha una propria legislazione sul copyright .
" Nel caso di inoltro o re-invio di un messaggio ricevuto, non modificarne il testo. Se si tratta di un messaggio personale e lo si vuole re-inviare ad un gruppo, è il caso di chiedere preventiva autorizzazione. È possibile abbreviare il testo e riportare solo le parti più rilevanti, ma ci si assicuri di fornire le appropriate credenziali.
" Non inviare mai lettere a catena via posta elettronica. Su Internet le lettere a catena sono vietate, pena la revoca dell'acconto. Nel caso ne riceviate una, fatelo immediatamente presente all'amministratore locale.
" Una buona regola generale: conviene esser risparmiatori in quel che si spedisce e liberali in quel che si riceve. Non è il caso di inviare messaggi d'ira ("flames") anche se si dovesse esser provocati. D'altra parte, non c'è da sorprendersi se si diventa vittime di qualche "flame", e in tal caso è prudente non rispondere.
" In generale, è una buona idea controllare tutti i Subject dei messaggi ricevuti prima di rispondere. Altrimenti potrebbe succedere che, ad esempio, una persona che chiede indicazioni d'aiuto o chiarimenti invii un nuovo messaggio per dire "Non importa". Ci si assicuri inoltre che il messaggio a cui si sta per rispondere sia diretto proprio a voi: potreste riceverlo per cc: anziché come destinatario principale.
" È bene rendere le cose facili per chi riceve. Molti programmi di posta (mailer) eliminano i dati inclusi nell'intestazione (header), compreso l'indirizzo del mittente. Per far in modo che la gente sappia chi sia a scrivere, assicurarsi di includere una riga con tali dati a fine messaggio. Conviene creare tale file a parte ed aggiungerlo alla fine di ogni testo inviato (alcuni mailer lo fanno automaticamente). Nell'idioma di Internet, quest'ultimo è noto come il "file di firma" (".sig" o "signature" file). In pratica si tratta dell'equivalente del biglietto da visita (e a seconda della circostanze, è possibile averne più d'uno).
" Fare attenzione quando s'inserisce l'indirizzo del ricevente. Ci sono indirizzi che fanno parte di un gruppo ma sembra appartengano ad una persona soltanto. Ci si assicuri di conoscere la persona a cui s'invia il messaggio.
" Verificare gli indirizzi di cc: quando si risponde. È inutile continuare ad includere questi ultimi se il messaggio è diventato una conversazione tra due persone soltanto.
" In generale, la maggior parte della gente che usa Internet non ha il tempo per rispondere a domande generali su Internet e il suo funzionamento. Non inviare messaggi non voluti per richieste d'informazioni a persone il cui nome è stato visto su elenchi o mailing list.
" Ricordarsi che la gente con cui si comunica è sparsa in tutto il mondo. Se si invia un messaggio che richiede immediata risposta, può darsi che arrivi quando il destinatario stia dormendo. Aspettate che si svegli, vada al lavoro e si colleghi prima di ritenere che la posta non sia arrivata o che non si curi di rispondere.
" Verificare tutti gli indirizzi prima di iniziare discussioni lunghe o personali. È altresì buona norma includere la parola "lungo" alla voce "Subject" in modo che il ricevente possa sapere in precedenza che ci vorrà tempo per leggere e rispondere a quel messaggio. Oltre le 100 righe è considerato "lungo".
" Occorre sapere chi contattare in caso di richiesta d'aiuto. In genere le risorse dovrebbero essere a portata di mano, soprattutto riguardo gente locale in grado di risolvere eventuali problemi di software e di sistema. Ci si assicuri anche di sapere a chi rivolgersi in caso si riceva qualcosa di equivoco o illegale. Quasi tutti i siti hanno un indirizzo di "Postmaster", dove un esperto è in grado di fornire aiuto in caso di problemi con la posta.
" Tenere in mente che il destinatario è un essere umano la cui cultura, lingua ed umore hanno riferimenti diversi dal vostro. Ricordarsi anche che i formati riguardanti date, misure, idiomi possono essere trasmessi in modo incorretto. Va prestata particolare attenzione alle forme di sarcasmo e ironia.
" Usare maiuscole e minuscole. SOLTANTO CON LE MAIUSCOLE È COME SE SI STESSE URLANDO.
" Usare i simboli per dare enfasi. Ecco *cosa* intendo dire. Usare i trattini in basso per le sottolineature. _Guerra e Pace_ è il mio libro preferito.
" Usare le faccine sorridenti (smileys) in sostituzione del tono di voce, ma fatelo con parsimonia.:-)è un esempio di faccina sorridente (guardare lateralmente). Non si ritenga che la semplice inclusione di una tale simbolo renda contento il destinatario su quel che si sta scrivendo o che faccia dimenticare eventuali contenuti offensivi.
" Prima d'inviare risposte d'impulso conviene dormirci su. Se si vogliono esprimere opinioni forti su un certo testo, è il caso di segnalarlo inserendo un FLAME ON/OFF. Ad esempio: FLAME ON: Quest'argomento occupa inutilmente banda di trasmissione. È illogico e senza costrutto. E il resto del mondo è d'accordo con me. FLAME OFF
" Non si includano caratteri per istruzioni particolari o allegati non-ASCII, a meno che questi non siano in MIME o a meno che il vostro mailer non sia in grado di decodificarli. Assicuratevi comunque che il destinatario sia in grado di decodificare tali messaggi.
" Ci si preoccupi di essere brevi senza essere eccessivamente concisi. Includere nella risposta sufficiente materiale originale per esser compresi ma non oltre. È piuttosto brutto rispondere ad un messaggio allegando tutto il testo precedente: tagliar via il materiale irrilevante.
" Limitare la lunghezza delle righe a meno di 65 caratteri e inserire un ritorno del carrello (premendo Invio) alla fine di ogni riga
" Il "Subject" del messaggio dovrebbe riflettere il contenuto del testo.
" Se s'include la firma, la si tenga corta. In genere si sta entro le 4 righe. Tenere in mente che molta gente paga per collegarsi, e più il messaggio è lungo più costa.
" Così come oggi la posta tradizionale può non rimanere privata, allo stesso modo (oggi) posta (e notizie) elettroniche possono essere soggette a vari livelli di intercettazione e falsificazione. Il senso comune aiuta a verificare l'autenticità dei messaggi.
" Se l'importanza di un messaggio sembra giustificarlo, conviene inviare immediatamente al mittente una breve replica di ricevimento, rimandando a più tardi una risposta più esauriente.
" I comportamenti "ragionevoli" via e-mail dipendono dalle relazioni tra le due persone e dal contesto della comunicazione. Consuetudini imparate in un particolare ambiente telematico potrebbero risultare inapplicabili nelle comunicazioni via e-mail con persone su Internet. Si faccia attenzione ad espressioni gergali o acronimi.
" Generalmente i costi relativi alla trasmissione di un messaggio di posta elettronica sono equamente divisi tra mittente e destinatario (o tra i rispettivi sistemi/organizzazioni), al contrario di quanto avviene in altri media quali posta tradizionale, telefono, radio e televisione. Inviare posta elettronica può anche comportare costi aggiuntivi per uso di banda di trasmissione del network, spazio su disco o uso di memoria del sistema. Questa è la ragione economica fondamentale del perché le e-mail pubblicitarie non richieste non sono benvenute (e proibite in molti contesti).
" Ci si assicuri della grandezza del testo inviato. Allegare ampi file come un Postscript o un programma potrebbe impedire l'arrivo del messaggio stesso o un eccessivo utilizzo di risorse. Buona regola è non inviare mai file superiori a 50 Kilobyte. In tal caso, considerare come alternativa il trasferimento di file o la riduzione a diversi file più piccoli da inviare come messaggi separati.
" Non inviare eccessive informazioni quando non richieste.
" Qualora il sistema di posta del vostro sistema lo consenta, si presti attenzione all'inoltro (forward) dei messaggi. Conviene assicurarsi che la configurazione prescelta non preveda l'inoltro ad una serie di host diversi, evitando Così che un testo passi da un computer altro e ad un altro ancora all'infinito.

2.1.2 Per il talk
Il "talk" è una serie di protocolli che consentono a due persone di avere un dialogo interattivo via computer.
" Usare maiuscole e minuscole e la necessaria punteggiatura, come se si stesse scrivendo una lettera o inviando una e-mail.
" Non arrivare in fondo alla riga facendo spezzare le parole al terminale, ma usare il ritorno del carrello (premendo Invio) a fine riga. E non si ritenga che il proprio schermo sia della stessa grandezza degli altri. Buona regola è scrivere non oltre 70 caratteri, e non più di 12 righe (che dovrebbero entrare in un'unica schermata).
" Lasciare un po' di margine; non scrivere immediatamente in cima alla schermata.
" Inserire due ritorni di carrello, ovvero una riga bianca (premendo Invio), per segnalare che avete finito e che l'altra persona può iniziare a scrivere.
" Concludere sempre con una qualche forma di saluto, ed aspettare di vedere il saluto dell'altra persona prima di chiudere la sessione. Ciò è di particolare importanza quando si comunica con qualcuno molto lontano. Tenere in mente che una buona comunicazione si basa sull'ampiezza di banda e sulla velocità della luce.
" Ricordarsi che il talk è un'interruzione delle attività dell'altra persona. Va quindi usato in modo appropriato, e mai con gli sconosciuti.
" Se non arrivano risposte, le ragioni potrebbero essere molte. Non si ritenga che tutto funzioni perfettamente. Non tutte le versioni di talk sono compatibili.
" In automatico il talk chiama due volte il destinatario. Far squillare una o due volte, poi chiudere.
" Se la persona non risponde, provare un'altra volta. Usare finger per verificare la correttezza della chiamata. Se la persona non risponde ancora, inutile insistere.
" Con talk si misura l'abilità nella battitura. Se si digitano le parole lentamente facendo molti errori, in genere non vale la pena di correggerli, poiché l'altra persona riesce a capire cosa s'intende dire.
" Si faccia attenzione quando si stanno seguendo più d'una sessione di talk!
2.2 Note per gli amministratori

" Assicurarsi di aver distribuito informazioni scritte sulle linee-guida relative ad eventuali situazioni su traffici illegali o impropri e falsificazioni.
" Gestire ogni richiesta in tempi corretti - entro il successivo giorno lavorativo.
" Replicare prontamente a chi segnala d'aver ricevuto messaggi impropri o illegali. Questioni riguardanti lettere a catena vanno affrontate immediatamente.
" Illustrare agli utenti le regole del sistema, incluso lo spazio disponibile su disco. Ci si accerti che ogni utente comprenda bene il significato delle richieste di file via posta, tra cui riempimento dei dischi, aumento delle bollette telefoniche, ritardi postali, ecc.
" Assicurarsi di avere acconti riservati sia per il "Postmaster" che per il "Root" e che qualcuno legga regolarmente la posta ivi indirizzata.
" Analizzare le lamentele degli utenti a mente aperta. Ricordarsi che gli indirizzi potrebbero essere stati intercettati e falsificati.
3.0 Comunicazioni verso più persone (Mailing List, NetNews)

Ogni volta che avviene una comunicazione rivolta a più persone (One-to-Many) dovrebbero applicarsi le norme già viste per la posta. In fondo comunicare con molte persone attraverso un messaggio postale o un testo equivale a comunicare con una sola persona, con in più la possibilità di offendere molta più gente che nella comunicazione uno-a-uno. È quindi molto importante conoscere quanto meglio possibile il pubblico a cui vengono diretti i messaggi.

3.1 Linee-guida per gli utenti

3.1.1 Linee-guida generali per mailing list e NetNews
" Seguire regolarmente mailing list e newsgroup per uno-due mesi prima d'inserire testi. Ciò permette di comprendere la cultura del gruppo.
" Non considerare responsabile l'amministratore del sistema per i comportamenti degli utenti.
" Tenere in mente che ogni testo verrà letto da un'ampio pubblico. Compreso forse il vostro attuale o futuro datore di lavoro. Fare attenzione a ciò che si scrive. Ricordarsi anche sia le mailing lists che i newsgroup vengono archiviati con una certa frequenza, e che le vostre parole possono essere conservate per molto tempo in un'area accessibile da molte persone.
" Occorre ritenere che ogni singolo individuo esprima le proprie opinioni, e non rappresenta la propria organizzazione (a meno che ciò non venga esplicitamente detto).
" Ricordarsi che sia la posta che le news riguardano in qualche modo le risorse del sistema. Prestare attenzione a qualunque regola specifica relativa all'uso di tali risorse esista nell'ambito del vostro sistema/organizzazione.
" Messaggi ed articoli dovrebbero essere brevi e centrati sull'argomento. Non scrivere su temi off-topic, non ripetersi e non inviare posta né inserire testi giusto per sottolineare gli errori di battitura o i refusi degli altri. Sono questi i comportamenti che, ben più di altri, testimoniano dell'immaturità dei principianti.
" La riga del "Subject" dovrebbe seguire le consuetudini del gruppo.
" Falsificazioni e imbrogli non rientrano tra i comportamenti approvati.
" Su alcune liste e newsgroup la pubblicità è benvenuta, mentre su altri è aborrita! Questo è un'altro esempio di come sia importante avere piena conoscenza del gruppo dove s'inseriscono i testi. Fare pubblicità non richiesta quando è chiaramente off-topic garantisce il ricevimento assicurato di messaggi particolarmente duri ed arrabbiati.
" Quando si replica ad un messaggio o ad un testo, ci si assicuri di riassumere il messaggio originale ad inizio testo, o di includere appena il testo sufficiente per spiegare il contesto. Ciò consentirà a chi legge di capire perfettamente la vostra replica. Poiché soprattutto nelle NetNews i testi vengono distribuiti da un host all'altro, può succedere di vedere apparire la risposta ancor prima dell'originale. Fornire quindi il contesto giusto aiuta la comprensione generale, ma senza inserire l'intero testo originale!
" Come già detto per la posta, ci si assicuri d'inserire la firma a fine testo. Ciò garantisce che, anche in presenza di programmi di posta (mailer) o per la lettura delle news (newsreader) che tagliano via i dati dell'intestazione, resteranno visibili le informazioni necessarie per chi volesse contattarvi.
" Si faccia attenzione nel replicare ad testo. Spesso le risposte tornano al mittente che ha originato il testo, e in molti casi si tratta dell'indirizzo della lista o del gruppo! Potrebbe accadere d'inviare una risposta personale ad una gran numero di persone, nell'imbarazzo generale. È meglio digitare l'indirizzo esatto anziché usare l'opzione "replica" (reply).
" Avvisi di ricevimento, di non-ricevimento e/o di temporanea assenza non sono né pienamente standardizzati né del tutto affidabili, stante la notevole diversità dei sistemi di gestione posta su Internet. Risultano invasivi se inviati alle mailing list, ed alcuni considerano gli avvisi di ricevimento un'invasione della privacy. In breve, non usare mai tali avvisi.
" Se si scopre che un vostro messaggio personale è stato erroneamente recapitato ad una lista o gruppo, inviate un messaggio di scuse.
" Nel caso ci si trovi in disaccordo con una persona, meglio proseguire la discussione attraverso e-mail personali piuttosto che nella lista o gruppo. Se la discussione verte su un punto d'interesse generale, è sempre possibile riassumerla in seguito per gli altri.
" Non ci si faccia coinvolgere in nessuna "flame war". Non rispondere a materiali incendiari.
" Si eviti di inviare messaggi o inserire testi che non sono altro che gratuite repliche ad altre repliche.
" Si faccia attenzione ai caratteri mono-spazio e ai diagrammi, che possono apparire in modi diversi sia su sistemi differenti che con differenti mailer dello stesso sistema.
" Esistono newsgroup e Mailing List che discutono di argomenti ed interessi anche molto diversi tra loro. I vari gruppi rappresentano l'ampia diversità di stili di vita, religioni e culture. Inserire testi o inviare messaggi ad un gruppo il cui punto di vista risulta offensivo per voi soltanto per dire quanto siano offensivi è un comportamento inaccettabile. Messaggi che procurano molestie sessuali o razziali possono anche avere conseguenze legali. Oggi esistono diversi programmi in grado di filtrare quei testi che risultano equivoci.

3.1.2 Linee-guida per le Mailing List

Ci sono vari modi per trovare informazioni su quali mailing list esistono su Internet e su come iscriversi. Ci si assicuri di aver chiara la politica del vostro sistema/organizzazione riguardo iscrizione e inserimento di testi in tali liste. In generale è sempre meglio dare un'occhiata alle risorse locali prima di cercare informazioni su Internet. Esistono comunque una serie di file inseriti periodicamente in news.answersche forniscono l'elenco delle mailing list disponibili su Internet e relative modalità d'iscrizione. Si tratta di un'incalcolabile fonte d'informazioni per trovare liste su qualsiasi argomento. Vedere anche le voci n. 9, 13, 15 della Bibliografia Selezionata.
" Inviare i messaggi d'iscrizione e/o di cancellazione (subscribe, unsubscribe) all'indirizzo appropriato. Anche se il software di alcune mailing list è abbastanza intelligente da isolarli e smistarli, non con tutte è possibile farlo. Fa parte della responsabilità di ciascuno comprendere come funziona ogni lista, compreso l'invio dei vari messaggi agli indirizzi giusti. Nonostante molte liste abbiano per convenzione un indirizzo "-request" per l'invio dei messaggi iscrizione e/o cancellazione (subscribe,unsubscribe), non tutte lo prevedono. Ci si assicuri di essere a conoscenza delle convenzioni in vigore nella lista a cui ci si vuole iscrivere.
" Salvare e conservare i messaggi d'iscrizione di ogni lista a cui si partecipa. In genere vi si trovano anche le istruzioni per cancellare l'iscrizione.
" In genere non è possibile recuperare un messaggio una volta inviato. Neppure l'amministratore del sistema sarà in grado di farlo. Ciò significa che occorre essere davvero sicuri che si vuole inviare il messaggio così com'è.
" L'opzione di replica automatica (auto-reply) di molti programmi di gestione posta (mailer) è utile per comunicazioni interne, ma piuttosto disturbante quando arriva all'intera lista. Date un'occhiata agli indirizzi nel campo "Reply-To" quando si replica ai messaggi della lista: molti di questi andranno a tutti i membri della lista stessa.
" Non inviare alla mailing list ampi file raggiungibili via Uniform Resource Locator (URL) o ftp. È possibile inviarlo diviso in vari file, purché ciò rientri nella cultura del gruppo. Se non si è certi su cosa fare, chiedere prima.
" Considerare la cancellazione dell'iscrizione o l'opzione "nomail" (se disponibile) quando si sa che non sarà possibile controllare la posta per un certo periodo.
" Quando s'invia un messaggio più d'una mailing list, specialmente se queste sono strettamente collegate, scusarsi per il cross-posting.
" Quando si chiede qualcosa, ci si assicuri di inserire un breve riassunto. E lo si faccia seriamente, anziché inviare una serie di stralci dai messaggi ricevuti.
" Alcune mailing list sono private. Non inviare messaggi a tali liste se non invitati. Non riportare in pubblico i testi di tali liste.
" Se si interviene in un'accesa discussione, ci si concentri sugli argomenti piuttosto che sulle personalità coinvolte.

3.1.3 Linee-guida per le NetNews

NetNews è un sistema distribuito globalmente che consente alla gente di comunicare su argomenti d'interesse specifico. È diviso in gerarchie, con le seguenti divisioni primarie: sci - discussioni connesse con la scienza; comp - discussioni intorno ai computer; news - discussioni relative alle NetNews stesse; rec - attività ricreative; soc - questioni sociali; talk - interminabili discussioni ad ampio raggio; biz - per faccende genericamente commerciali; alt - la gerarchia "alternativa". Alt è chiamata così anche perché per creare un gruppo alt non occorre seguire la medesima procedura necessaria per i newsgroup delle altre gerarchie. Esistono poi gerarchie regionali, quelle ampiamente distribuite come bionet, ed è anche possibile avere un gruppo per specifiche attività imprenditoriali. Recentemente è stata aggiunta la gerarchia "humanities", e col passare del tempo è probabile altre ancora se ne aggiungeranno. Per informazioni e discussioni sulle News vedere le voci n. 2, 8, 22, 23 della Bibliografia Selezionata.
" Nel linguaggio delle NetNews, "Posting" indica l'inserimento di un nuovo articolo in un gruppo, o la risposta ad un testo o articolo (post) inserito da qualcun altro. "Cross-Posting" indica l'inserimento di un testo in più d'un gruppo. Avvisare preventivamente i lettori nel caso si effettui un Cross-Posting in un gruppo, oppure se si usa il "Followup-To:" nell'intestazione del messaggio! Chi legge ritiene generalmente che il messaggio e/o i seguiti (followups) siano per quel gruppo. È bene segnalare ogni modifica a tale consuetudine.
" Leggere per intero l'andamento della discussione (il cosiddetto "thread") prima di fare interventi. Evitare di inserire messaggi tipo "Anch'io", il cui contenuto si limita ad esprimere accordo con il testo precedente. Il contenuto di un seguito dovrebbe andare oltre la semplice ripresa del testo originale.
" Inviare un e-mail quando la risposta ad una domanda riguarda una sola persona. Ricordarsi che le News vengono distribuite globalmente ed è probabile che non tutto il mondo sia interessato ad una replica personale. Non è tuttavia il caso di esitare ad inserire qualcosa che sia d'interesse generale.
" Controllare la sezione "Distribution" dell'intestazione, senza però farvi troppo affidamento. A causa del complesso metodo con cui vengono distribuite le News il campo "Distribution" non è del tutto affidabile. Ma se ci appresta ad inserire un testo che interessa un numero limitato di lettori, conviene usare la riga per la distribuzione cercando di limitarla a tali persone. Ad esempio, impostare la riga "Distribution" su "nj" nel caso l'articolo da inserire interessi soltanto i lettori del New Jersey.
" Se si ritiene che un articolo sia d'interesse per più di un newsgroup, ci si assicuri di effettuare il CROSSPOST anziché inserirlo singolarmente in tali gruppi. È probabile che saranno non oltre cinque o sei i gruppi con interessi simili.
" Prima d'inserire una domanda, è bene cercare in altre referenze (manuali tecnici, giornali, file d'aiuto). Avanzare richieste in un newsgroup quando le risposte sono facilmente reperibili altrove può generare il brusco messaggio di "RTFM" (read the fine manual - leggiti quel fantastico manuale, anche se in genere la lettera "f" implica un termine ben più volgare, [sia in inglese che in italiano]).
" Nonostante esistano newsgroup che accettano pubblicità, in genere è considerato poco meno che criminale fare pubblicità a prodotti off-topic. Inserire un annuncio pubblicitario in tutti i gruppi garantisce la pressoché certa cancellazione all'accesso.
" Nel caso si scoprano errori in un proprio testo, conviene cancellarlo appena possibile.
" NON si cerchi di cancellare altri articoli diversi dai propri. Contattare l'amministratore per sapere come fare a rimuovere i propri testi, o nel caso occorra cancellare testi quali lettere a catena.
" Se un testo inserito non appare immediatamente, non significa che ci sia qualche intoppo e che bisogna inviarlo di nuovo; conviene aspettare.
" Alcuni gruppi permettono (ed alcuni gradiscono) l'inserimento di testi che in altre circostanze sarebbero considerati quantomeno equivoci. Nuovamente, non esiste alcuna garanzia che tutte le persone del gruppo apprezzeranno quel materiale. Nell'inserire simili testi, usare la utility Rotate (che sposta in rotazione i caratteri del testo di 13 posti nell'alfabeto) così da evitare d'offendere qualcuno. Si può usare, ad esempio, la utility Rot13 per Unix.
" Nei gruppi in cui si discute di film o libri è considerato vitale segnalare quei testi dal contenuto particolare con il termine "Spoilers", da inserire nella riga del Subject:. Per nasconderlo in qualche modo si possono poi aggiungere delle righe bianche ad inizio testo oppure usare la utility Rotate.
" La falsificazione di articoli è generalmente censurata. Ci si può proteggere dalle falsificazioni usando del software che genera sicure firme digitali, come PGP (negli USA).
" L'inserimento di testi da un server anonimo è accettato in alcuni newsgroup e rifiutato in altri. Materiali inappropriati se inseriti a proprio nome restano tali anche se inseriti anonimamente.
" Ci si aspetti qualche ritardo nel veder apparire testi inseriti in un gruppo moderato. Potrebbe accadere che il moderatore decida di cambiarne il Subject per renderlo conforme ad un particolare thread.
" Si eviti di farsi coinvolgere in "flame wars". Non inserire né replicare a critiche infiammate.

3.2 Linee-guida per gli amministratori

3.2.1 Questioni generali

" Chiarire ogni questione sulla politica del sito o del sistema riguardo l'iscrizione ai newsgroup e alle mailing list.
" Chiarire ogni questione sulla politica del sito o del sistema riguardo l'inserimento di testi nei newsgroup e nelle mailing list, compreso l'uso dei "disclaimer" nelle firme.
" Chiarire e pubblicizzare la politica sull'archiviazione. (Per quanto tempo vengono conservati gli articoli?)
" Avviare indagini immediate ed a mente aperta riguardo accuse da parte di utenti.
" Ci si assicuri di monitorare regolarmente lo stato di salute del sistema.
" Decidere per quanto tempo archiviare i "log" del sistema, e diffonderne la politica.
3.2.2 Mailing List
" Mantenere aggiornate le mailing list per evitare problemi di posta respinta ("bouncing mail").
" In caso di problemi, dare aiuto ai proprietari delle liste.
" Informare i proprietari delle liste riguardo i periodi di chiusura per manutenzione o altro.
" Ci si assicuri di avere acconti "-request" per le iscrizioni alla lista e per l'amministrazione.
" Ci si assicuri che tutti i gateway della posta funzionino correttamente.

3.2.3. NetNews

" Pubblicizzare la natura dei gruppi che si ricevono; se non tutti risultano disponibili, la gente vorrà conoscerne le ragioni.
" Ci si ricordi che avere vari clienti News Reader può causare problemi di cui verranno poi accusati i Server delle News.
" Si sbrighino immediatamente le richieste degli utenti, soprattutto quando richiedano la cancellazione di propri testi o di testi non validi quali lettere a catena.
" Si abbiano acconti diversi per "Usenet", "Netnews" e "News", e ci si assicuri che qualcuno ne legga regolarmente la posta.
3.3 Linee-guida per i moderatori
3.3.1 Linee-guida generali
" Ci si assicuri che il file delle Frequestly Asked Questions (FAQ) venga inserito a intervalli regolari. Includere (o far includere, se il file delle FAQ è curato da altri) le linee-guida per l'inserimento di articoli/messaggi.
" Ci si assicuri di avere un buon messaggio di benvenuto, con incluse le informazioni per l'iscrizione e la relativa cancellazione.
" Inserire regolarmente le linee-guida relative ai newsgroup.
" Tenere aggiornati mailing list e newsgroup. Inserire tempestivamente i messaggi. Designare un sostituto in caso di assenza per vacanza o altro.
4.0 Servizi d'informazione (Gopher, Wais, WWW, ftp, telnet)

Nella recente storia di Internet, c'è stata l'esplosione di nuovi e diversi servizi d'informazione. Gopher, Wais, World Wide Web (WWW), Multi-User Dimensions (MUDs), Multi-User Dimensions che sono Object Oriented (MOOs) sono alcune di tali nuove aree. Nonostante la capacità di trovare informazioni stia diventando enorme, rimane la costante del "Caveat Emptor" Per maggiori informazioni in merito ai suddetti servizi, vedere le referenze 14 e 23 della Bibliografia Selezionata.

4.1 Linee-guida per gli utenti

4.1.1. Linee-guida generali

" Ricordarsi che tali servizi appartengono a qualcun altro. Le persone che pagano bollette e fatture stabiliranno le regole per il loro uso. L'informazione può essere libera e gratuita - oppure no! Ci si assicuri di verificarlo.
" Nel caso di problemi con un qualsiasi servizio d'informazioni, iniziare i controlli a livello locale: verificare le configurazioni dei file, le impostazioni del software, i collegamenti del network, etc. Ciò va fatto prima di ritenere che il problema riguardi il fornitore o che sia colpa dello stesso.
" Nonostante si usino delle convenzioni per stabilire i nomi dei file, è bene non farvi troppo affidamento. Ad esempio, non sempre un file ".doc" è un file in Word.
" Anche i servizi d'informazione usano nomi convenzionali, quali www.xyz.com. Pur essendo utile esserne a conoscenza, di nuovo, non è il caso di affidarsi eccessivamente a tali convenzioni.
" È importante conoscere quei nomi di file che funzionano bene sul proprio sistema.
" Occorre conoscere le convenzioni usate per fornire informazioni durante le sessioni. I siti FTP hanno in genere dei file chiamati README ad inizio directory contenenti informazioni sui file disponibili. Ma non si dia per scontato che tali file siano necessariamente aggiornati e/o accurati.
" In generale, non si ritenga che qualunque informazione sia accurata e/o aggiornata. Ricordarsi che le nuove tecnologie consentono praticamente a chiunque di diventare editori, ma non tutti hanno ancora scoperto le responsabilità che accompagnano la pubblicazione dei materiali.
" Ricordarsi che, a meno che non si sia certi sull'utilizzo di tecnologie per la sicurezza e l'autenticazione, ogni informazione trasmessa attraverso Internet passa a cielo aperto, senza alcuna protezione nei confronti di "curiosi" e falsari.
" Poiché Internet è sparsa in tutto il globo, tenere a mente che i servizi d'informazione possono riflettere cultura e stili di vita piuttosto diversi dai propri. Quei materiali che vi sembrano offensivi potrebbero nascere in un'area geografica che li ritiene accettabili. Occorre avere una mente aperta.
" Quando si vuol prelevare un dato da un server molto noto, conviene utilizzare un server "mirror" vicino, qualora ne venga fornito l'elenco.
" Non usare il sito FTP di qualcuno per depositarvi materiale che dovrebbe esser prelevato da altre persone. Ciò è definito "dumping" e in genere viene considerato un comportamento inaccettabile.
" Nel caso di richiesta di aiuto per problemi con un sito, ci si assicuri di fornire quante più informazioni possibili per far sì che il problema possa esser risolto.
" Quando si organizza il proprio sistema d'informazioni, come una homepage, ci si assicuri di verificare con il proprio amministratore quali siano le linee-guida locali in vigore.
" Si cerchi di rendere meno pesante il traffico sui siti più noti, evitando di collegandosi nelle ore di punta.

4.1.2 Linee-guida per servizi interattivi in tempo reale (MUDs MOOs IRC)

" Come per gli altri ambienti, è saggio innanzitutto "ascoltare" per conoscere la cultura del gruppo.
" Non è necessario salutare personalmente chiunque sia presente su un canale o in una stanza. In genere basta un solo "Salve" o un saluto equivalente. Non è un comportamento accettabile utilizzare la funzione automatica del client per salutare le persone.
" Avvisare i partecipanti nel caso s'intenda inviare grandi quantità di dati. Se tutti acconsentono a riceverli, si può procedere, ma inviare informazioni non desiderate senza avvisare preventivamente è considerata azione negativa, proprio come con la posta.
" Non dare per scontato che persone sconosciute abbiano voglia di parlare con voi. Se sorge la necessità d'inviare messaggi privati a degli sconosciuti, bisogna allora accettare serenamente il fatto che questi possano essere occupati o che semplicemente non abbiano voglia di chiacchierare con voi.
" Rispettare le linee-guida del gruppo. Cercare il materiale di presentazione del gruppo, che potrebbe anche trovarsi in un sito ftp connesso.
" Non fare domande su questioni personali quali età, sesso, dimora. Dopo aver stabilito una certa confidenza con qualcuno, tali domande potranno essere più appropriate, ma molte persone esitano a dare tali dati a gente che non conoscono.
" Se un utente usa uno pseudonimo, alias o soprannome, tale desiderio d'anonimità va rispettato. Anche se si è amici di tale persona, è più gentile usare il soprannome. Non usare il nome vero senza permesso.

4.2 Linee-guida per gli amministratori

4.2.1 Linee-guida generali

" Stabilire con chiarezza cosa è disponibile per esser copiato e cosa no.
" Definire con chiarezza cosa è disponibile sul vostro sito e nella vostra organizzazione. Ci si assicuri che le politiche generali siano ben chiare.
" Mantenere aggiornate le informazioni, soprattutto i file README, da tenere soltanto in formato ASCII, solo testo.
" Diffondere l'eventuale elenco dei siti "mirror", allegando una dichiarazione sul copyright ad essi relativo. Se possibile, indicarne le previsioni sugli aggiornamenti.
" Ci si assicuri che le informazioni note (e massicce) abbiano un'adeguata ampiezza di banda.
" Usare i nomi convenzionali per le estensioni dei file - .txt per testi in ASCII; .html o .htm per HTML;.ps per Postscript; .pdf per Portable Document Format;.sgml o .sgm per SGML; .exe per eseguibili non-Unix, etc.
" Per i file da prelevare, si cerchi di comporne nomi unici con i primi otto caratteri.
" Nel fornire informazioni, ci si assicuri che il vostro sito abbia qualcosa di unico da offrire. Evitare di metter su un servizio d'informazioni che semplicemente rimandi ad altri servizi su Internet.
" Non si rimandi ad altri siti senza prima aver chiesto.
" Ricordarsi che mettere su un servizio d'informazioni significa molto più che progettare e realizzare. C'è anche la manutenzione.
" Assicurarsi che il materiale inserito sia appropriato nei riguardi della vostra organizzazione.
" Provare le applicazioni con strumenti diversi. Non si ritenga che tutto funzioni bene se avete fatto prove con un client soltanto. Ci si basi inoltre sulle tecnologie più modeste e non creare applicazioni da usare esclusivamente con un interfaccia grafico.
" Cercare di avere una visione complessiva delle informazioni offerte. Assicurarsi che forma e contenuto rimangano inalterate nelle varie applicazioni.
" Fare attenzione all'invecchiamento delle informazioni disponibili. Ci si assicuri di datare le informazioni particolari, e si vigili affinché siano mantenute bene.
" Le restrizioni sull'esportazione variano da paese a paese. Assicurarsi di conoscere le implicazioni sull'esportazione quando si inseriscono dei testi.
" Informare gli utenti su quel che s'intende fare con ogni tipo d'informazione raccolta, come commenti sul WWW. Occorre avvisare le persone nel caso s'intenda usare qualunque loro affermazione, anche se la si rende passivamente disponibile ad altri utenti.
" Ci si assicuri che sia di conoscenza generale la politica in vigore sui servizi d'informazione per gli utenti, tipo le homepage. 
EOF;

function MStranslate($id) {


	//$lang = isset($_GET['lang']) ? $_GET['lang'] : "IT";

	if(isset($_GET["lang"])){
		$lang = $_GET["lang"];
		$_SESSION["lang"] = $lang;
	}else{
		if(isset($_SESSION["lang"])){
			$lang = $_SESSION["lang"];
		}else{
			$lang = "IT";
			$_SESSION["lang"] = $lang;
		}
	}

	global $translation;

	echo $translation[$lang][$id];
	
}


?>