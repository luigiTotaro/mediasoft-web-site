<?php

include 'lang.php';
 
 ?>

<!DOCTYPE html>
<html>
  <head>
    <title>Mediasoft Srl</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="HandheldFriendly" content="true" />

    <link href='http://fonts.googleapis.com/css?family=Maven+Pro' rel='stylesheet' type='text/css'>
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
    <link href="navbar.customization.css" rel="stylesheet" media="screen">
    <link href="carousel.customization.css" rel="stylesheet" media="screen">
    <link href="content.customization.css" rel="stylesheet" media="screen">
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
    <style>

	    body {
	    	padding: 0px;
	    	margin: 0px;
	    	font-family: 'Maven Pro';
	    	font-size: 13px;
	    	font-weight:500;
	    }

	    h1 {
	    	color: #b1c903;
	    	font-size: 50px;
	    	font-weight: normal;
	    }

	    h5 {
	    	color: #b1c903;
	    	font-size: 19px;
	    }

	    h5 {	    
	    	font-size: 16px;
	    }

	    a:link, a:hover, a:visited, a:focus {
	    	color: black;
	    	text-decoration: none;
	    }

	    #submitButton {

	    	border:solid 1px #4d4d4d;
	    	color: #b1c903;
	    	width:100px;
	    	padding: 0px;
	    	margin:0px;
	    }

	    .map-frame {
	    	width:100%; 
	    	min-height: 200px; 
	    	height:40%; 
	    	border:solid 1px #4d4d4d; 
	    }

	    .form-control {
	    	border-color: #4d4d4d;
	    }

	    .form-group {
	    	margin: 30px 10px 30px 10px;
	    }


    </style>
  </head>

  <body>



  	<div class="navbar-wrapper">
		<div class="container" id="navContainer" style="width:100%;">

			<nav class="navbar navbar-default transparent_navbar" role="navigation">

			  <!-- Brand and toggle get grouped for better mobile display -->
			  <div class="navbar-header">
			    <button type="button" class="navbar-toggle" data-toggle="collapse" id="buttonMobile" data-target="#bs-example-navbar-collapse-1">
			      <span class="sr-only">Toggle navigation</span>
			       <img id="logoButtonMobile" src="images/logo_mediasoft_notext_white.png">
			    </button>
			  </div>

			  <!-- Collect the nav links, forms, and other content for toggling -->
			  <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			    <ul class="nav navbar-nav" id="navBarMainUl">
			    </ul>

			  </div><!-- /.navbar-collapse -->
			</nav>

		</div>
	</div>



	<div class="container fill" style="margin: 0px; padding-top:10%; padding-left:8%; padding-right:8%; width:100%;">

		<div class="row" style="margin:0px; padding: 0px;">

			<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 text-center">

				<h1><?=translate('vieni_a_trovarci')?></h1>

				
				<div>
			
					<iframe class="map-frame" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" 
					src="https://maps.google.it/maps?f=q&amp;source=s_q&amp;hl=it&amp;geocode=&amp;q=Via+Sonzini,+25,+Galatina,+LE&amp;aq=0&amp;oq=via+sonzini+25&amp;sll=41.008099,16.727239&amp;sspn=3.150242,4.954834&amp;t=m&amp;ie=UTF8&amp;hq=&amp;hnear=Via+Sonzini,+25,+Galatina,+Lecce,+Puglia&amp;z=14&amp;ll=40.17726,18.169935&amp;output=embed&iwloc=near">
					</iframe>

					<div style="text-align:left; margin-bottom: 3%;">
						<h5>GALATINA</h5>
						<h6>Via Sonzini, 25 73013 Galatina (Le) &#8226; Tel 0836564849 &#8226; Fax 0836843111</h6>
					</div>

					

					<iframe class="map-frame" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" 
					src="https://maps.google.it/maps?f=q&amp;source=s_q&amp;hl=it&amp;geocode=&amp;q=Piazza+del+Popolo,+18+00187+Roma&amp;aq=&amp;sll=40.177004,18.171773&amp;sspn=0.012968,0.023046&amp;ie=UTF8&amp;hq=&amp;hnear=Piazza+del+Popolo,+18,+00187+Roma,+Lazio&amp;t=m&amp;z=14&amp;ll=41.910246,12.477104&amp;output=embed&iwloc=near"></iframe>

					<div style="text-align:left; margin-bottom: 3%;">
						<h5>ROMA</h5>
						<h6>Piazza del Popolo, 18 00187 Roma &#8226; Tel 0636712397 &#8226; Fax 0636712400</h6>
					</div>

				</div>
				

			</div>

			<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 text-center">
			
				<h1><?=translate('oppure_scrivici')?></h1>

				<div style="background-color: #ededee; padding: 20px 20px 20px 20px;">
					
					<form id="submitForm" class="form-horizontal" role="form">

						<div class="form-group">
							<label for="txtNome" class="col-sm-3 control-label"><?=translate('nome')?></label>
							<div class="col-sm-9">
								<input type="text" class="form-control" id="txtNome">
							</div>
						</div>

						<div class="form-group">
							<label for="txtCognome" class="col-sm-3 control-label"><?=translate('cognome')?></label>
							<div class="col-sm-9">
								<input type="text" class="form-control" id="txtCognome">
							</div>
						</div>

						<div class="form-group">
							<label for="txtEmail" class="col-sm-3 control-label">EMAIL</label>
							<div class="col-sm-9">
								<input type="email" class="form-control" id="txtEmail">
							</div>
						</div>

						<div class="form-group">
							<label for="txtMessaggio" class="col-sm-3 control-label"><?=translate('messaggio')?></label>
							<div class="col-sm-9">
								<textarea class="form-control" id="txtMessaggio" rows="5" style="resize:none;"></textarea>
							</div>
						</div>

					  <div class="form-group">
					    <div class="col-sm-offset-2 col-sm-10 text-left">
					      <div class="checkbox">
					        <label>
					          <input type="checkbox" id="chkPrivacy">
					        </label>
					        <a href="#" id="disclaimerHref"><?=translate('messaggio_condizioni_privacy')?></a>
					      </div>
					    </div>
					  </div>

					  <div class="form-group">
					    <div class="col-sm-offset-2 col-sm-10">
					      <button id="submitButton" type="button" class="btn btn-default"><?=translate('invia')?></button>
					    </div>
					  </div>
					</form>

				</div>

			</div>

		</div>
		

	</div>

	<div class="modal" id="modalDisclaimer" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	  <div class="modal-dialog">
	    <div class="modal-content">
	      <div class="modal-header" style="border:none;">
	        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	      </div>
	      <div class="modal-body">
	      		<?=translate('disclaimer')?>
	      </div>
	    </div><!-- /.modal-content -->
	  </div><!-- /.modal-dialog -->
	</div><!-- /.modal -->

	<div class="modal" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	  <div class="modal-dialog">
	    <div class="modal-content">
	      <div class="modal-header" style="border:none;">
	        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	      </div>
	      <div class="modal-body" id="alertBody"></div>

	    </div><!-- /.modal-content -->
	  </div><!-- /.modal-dialog -->
	</div><!-- /.modal -->

	<script src="jquery-1.10.2.min.js"></script>
	<script src="bootstrap/js/bootstrap.min.js"></script>
	<script src="functions.js"></script>
	<script src="modernizr.custom.js"></script>

	<script type="text/javascript">

		$(window).load(function() {

			$.buildTopMenu();			
			$.tweakIpadLandscape();
			$.setThemeBlack();

			$('body').css("background-color", "white");

			
			
		});

		$(document).ready(function() {

			$('#submitButton').click(function() {

				$.onSubmit();

			});

			$('#disclaimerHref').click(function() {

				$('#modalDisclaimer').modal();
			});

		});


		$.onSubmit = function() {
			
			if($('#chkPrivacy').is(':checked') == false) {

				$('#alertBody').text("<?=translate('errore_privacy_non_selezionata')?>");
				$('#myModal').modal();
				return false;
			}

			if($('#txtNome').val() == "") {

				$('#alertBody').text("<?=translate('errore_inserire_il_nome')?>");
				$('#myModal').modal();
				return false;
			}

			if($('#txtCognome').val() == "") {

				$('#alertBody').text("<?=translate('errore_inserire_il_cognome')?>");
				$('#myModal').modal();
				return false;
			}

			if( $.validateEmail($('#txtEmail').val()) == false) {

				$('#alertBody').text("<?=translate('errore_mail')?>");
				$('#myModal').modal();
				return false;
			}

			if($('#txtMessaggio').val() == "") {

				$('#alertBody').text("<?=translate('errore_messaggio')?>");
				$('#myModal').modal();
				return false;
			}

			$.ajax({

				/*dataType: 'json',*/
				type: 'POST',
			  	url: "mailsend.php",			  	
			  	data: {
			  		'txtNome': $('#txtNome').val(),
			  		'txtCognome': $('#txtCognome').val(),
			  		'txtEmail': $('#txtEmail').val(),
			  		'txtMessaggio': $('#txtMessaggio').val()
			  	}
			  	
			}).done(function(data) {

				$('#alertBody').text("<?=translate('messaggio_inviato')?>");
				$('#myModal').modal();
				$("#submitForm").trigger('reset'); 

			}).fail(function(jqXHR, textStatus, errorThrown ) {
				console.log(textStatus);
				console.log(errorThrown);
			    alert( "error" );
			});

			
		}



	</script>

  </body>

</html>
