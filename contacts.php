<?php
session_start();
include 'lang.php';
//include "./backoffice/DBAccess.php";
include "./backoffice/DBAccess2.php";

$dbInst = new DBAccess2();
$dbInst->connOpen();

//$lang = (isset($_GET["lang"]) ? $_GET["lang"] : "IT");

if(isset($_GET["lang"])){
	$lang = $_GET["lang"];
	$_SESSION["lang"] = $lang;
}else{
	if(isset($_SESSION["lang"])){
		$lang = $_SESSION["lang"];
	}else{
		$lang = "EN";
		$_SESSION["lang"] = $lang;
	}
}


if (($lang!="IT") && ($lang!="EN")) $lang="IT";
$idPage = $_GET["idPage"];

$extD = $dbInst->getDetailLangPagina($idPage, $lang);
$row = mysql_fetch_array($extD);
$title = $row['TITLE'];
$description = $row['DESCRIPTION'];
$keywords = $row['KEYWORDS'];

$OpenPositionsRows = $dbInst->getPosizioniAperte($lang);

$menuRows = $dbInst->getVociMenuTop($lang,$idPage);

$rassegnaStampaRows = $dbInst->getRassegnaStampa($lang);
 
$dbInst->connClose();

?>


<!DOCTYPE html>
<html>
  <head>
    <title><?=$title?></title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="HandheldFriendly" content="true" />

    <meta name="description" content="<?=$description?>"/>
    <meta name="keywords" content="<?=$keywords?>"/>

    <link href='https://fonts.googleapis.com/css?family=Maven+Pro:900,500' rel='stylesheet' type='text/css'>
    <link href="bootstrap/css/bootstrap.css" rel="stylesheet" media="screen">
    <link href="bootstrap.customization.css" rel="stylesheet" media="screen">
    <link href="navbar.customization.css" rel="stylesheet" media="screen">
    <link href="carousel.customization.css" rel="stylesheet" media="screen">
    <link href="content.customization.css" rel="stylesheet" media="screen">
	<link href="ThumbnailGridExpandingPreview/css/default.css" rel="stylesheet" type="text/css"  />
	<link href="ThumbnailGridExpandingPreview/css/component.css" rel="stylesheet" type="text/css"  />

	<link rel="shortcut icon" href="/images/faviconBW.ico" type="image/x-icon" />
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
    
    <style>

	    body {
			padding: 0px;
	    	margin: 0px;
	    	font-family: 'Maven Pro';
	    	font-size: 13px;
	    	font-weight:500;
	    }

	    #rowCollection h2 {
	    	font-size: 55px;
	    	line-height: 55px;	 
	    	margin:0px;
	    	padding: 0px;   	
	    	color: #b1c903;
	    }

	    #rowCollection h3 {
	    	font-size: 18px;
	    	line-height: 18px;	    	
	    	margin:0px;
	    	padding: 0px;
	    }

	    #rowCollection h4 {
	    	font-size: 17px;
	    	line-height: 17px;
	    	color: #b1c903;
	    }

	    #rowCollection h5 {
	    	text-align: justify;
	    }

	    a:link, a:hover, a:visited {
	    	text-decoration: none;

		}
		
	    h1 {
	    	color: #b1c903;
	    	font-size: 50px;
	    	font-weight: normal;
	    }

	    h5 {
	    	color: #b1c903;
	    	font-size: 19px;
	    }

	    h5 {	    
	    	font-size: 16px;
	    }
		
		#submitButton {

	    	border:solid 1px #4d4d4d;
	    	color: #b1c903;
	    	width:100px;
	    	padding: 0px;
	    	margin:0px;
	    }

	    .map-frame {
	    	width:100%; 
	    	min-height: 200px; 
	    	height:40%; 
	    	border:solid 1px #4d4d4d; 
	    }

	    .form-control {
	    	border-color: #4d4d4d;
	    }

	    .form-group {
	    	margin: 30px 10px 30px 10px;
	    }
		

    </style>
  </head>

  <body>

  	<input type="hidden" id="langHidden" value="<?=$lang?>" />

	<div id="fb-root"></div>
	<script>(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = "//connect.facebook.net/it_IT/all.js#xfbml=1&appId=528759423900786";
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));</script>  

	<script>!function(d,s,id){
		var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';
		if(!d.getElementById(id)){
			js=d.createElement(s);js.id=id;
			js.src=p+"://platform.twitter.com/widgets.js";
			fjs.parentNode.insertBefore(js,fjs);
			}
		}(document,"script","twitter-wjs");
	</script>

	
  	<div class="navbar-wrapper" style="position:fixed;background-color: #FFFFFF">
		<div class="container" id="navContainer" style="width:100%;">

			<nav class="navbar navbar-default transparent_navbar" role="navigation">

			  <!-- Brand and toggle get grouped for better mobile display -->
			  <div class="navbar-header">
			    <button type="button" class="navbar-toggle" data-toggle="collapse" id="buttonMobile" data-target="#bs-example-navbar-collapse-1">
			      <span class="sr-only">Toggle navigation</span>
			       <img id="logoButtonMobile" src="images/logo_mediasoft_notext_white.png">
			    </button>
			  </div>

			  <!-- Collect the nav links, forms, and other content for toggling -->
			  <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			    <ul class="nav navbar-nav" id="navBarMainUl" style="width:100%;">
					<li id="linkHomeMobile"><a href="index.php?lang='<?=$lang?>'">HOME</a></li>
					<?php

						$indice=0;
						$dbInst = new DBAccess2();
						$dbInst->connOpen();
						
						while($positionRow =  mysql_fetch_assoc($menuRows))
						{
							$indice++;
							$url = $positionRow['url'];
							$SxOrDx = "";
							if ($positionRow['SxDx'] == 1) $SxOrDx="left";
							else $SxOrDx="right";
							if($positionRow['valore'] != "LOGO_SEGNAPOSTO")
							{
								//vedo se ci sono sottomenu
								$subMenuItems = $dbInst->getVociMenuTopByPadre($lang,$positionRow['id']);	
								$countTotal = mysql_num_rows($subMenuItems);
								
								
								if($countTotal == 0)
								//if(0 == 0)
								{
									$subString = strstr($url, 'idPage'); 
									$trovato=false;
									if ($subString==("idPage=".$idPage))  $trovato = true;
									//alert(idPage + " - " + url + " - " + trovato);
									
									//alert(Valore + " - No SubItems");
									if ($trovato)
									{
									?>
									<li menu-position="<?=$SxOrDx?>" class="menu-<?=$SxOrDx?>-item" style="width:14.2%;"><a href="<?=$positionRow['url']?>&lang=<?=$lang?>"><b><?=$positionRow['valore']?></b></a><div class="underliner" style="top:20px;"/></li>
									<?php
									}
									else
									{
									?>
									<li menu-position="<?=$SxOrDx?>" class="menu-<?=$SxOrDx?>-item" style="width:14.2%;"><a href="<?=$positionRow['url']?>&lang=<?=$lang?>"><?=$positionRow['valore']?></a><div class="underliner" style="top:20px;"/></li>
									<?php
									}
								}
								else
								{
									$subString = strstr($url, 'idPage'); 
									$trovato=false;
									if ($subString==("idPage=".$idPage))  $trovato = true;

									if ($trovato)
									{
									?>
									<li menu-position="<?=$SxOrDx?>" class="dropdown menu-<?=$SxOrDx?>-item" style="width:14.2%;" id="father_internet"><a href="<?=$positionRow['url']?>&lang=<?=$lang?>"><b><?=$positionRow['valore']?></b></a><div class="underliner" style="top:20px;"></div> 
									<?php
									}
									else
									{
									?>
									<li menu-position="<?=$SxOrDx?>" class="dropdown menu-<?=$SxOrDx?>-item" style="width:14.2%;" id="father_internet"><a href="<?=$positionRow['url']?>&lang=<?=$lang?>"><?=$positionRow['valore']?></a><div class="underliner" style="top:20px;"></div> 
									<?php
									}
									?>
										<ul id="submenu_<?=$positionRow['id']?>" class="dropdown-menu second-level-submenu-custom">
											<ul class="list-inline">
												<?php		
												while($subMenuRow =  mysql_fetch_assoc($subMenuItems))
												{
													?>
													<li><a href="<?=$subMenuRow['url']?>" id="link<?=$subMenuRow['id']?>" menu-level="secondo"><?=$subMenuRow['valore']?></a><div class="underliner" style="top:35px;"/></li>
													<?php
												}
												?>
											</ul>
										</ul>
									</li>
									<?php
								
								}
							
							}
							else
							{
								// quando Valore == 'LOGO_SEGNAPOSTO' carico l'immagine centrale
								?>
								<li id="navLogo" style="width:14.2%;"><a href="index.php?lang=<?=$lang?>"><img id="imageLogo" src="images/logo_mediasoft_green.png"></a></li>
								<?php
							}
						}
						$dbInst->connClose();
					?>										

			    </ul>

			  </div><!-- /.navbar-collapse -->
			</nav>

		</div>
		<hr style="margin-left:6%;margin-right:6%;color: #B1C903; background-color: #B1C903; height: 2px;margin-top: 0px;margin-bottom: 0px;">
	</div>

	<div id="mainContainerDiv" class="container fill" style="width:90%; margin: 0px; padding: 0px;float:left;position:absolute;top:120px;">
		<div style="margin-left:8%; margin-right:8%;">
		
			<div id="section1" class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-left" style="padding: 10px 20px 25px 20px; margin: 0px 0px 0px 0px;"> 		
				<div>
					<div class="titleSection" style="margin-left:8%; margin-right:8%; text-align:center; margin-top:0px; margin-bottom:20px;"><?=MStranslate('dove_siamo')?></div>				
					<div style="margin: 0% 0% 0% 0%;padding-bottom: 20px;"><img src="images/mappa_europa_usa.png" style="max-width:100%;" alt="Smiley face"></div> 
				</div>						
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center" style="padding: 0px; margin: 0px; ">
				<hr style="color: #B1C903; background-color: #B1C903; height: 2px; width:100%">
				</div>
			

			</div>
		
			<div id="section2" class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-left" style="padding: 10px 20px 25px 20px; margin: 0px 0px 0px 0px;"> 		
				<div class="container fill" style="margin: 0px; padding-top:0%; padding-left:0%; padding-right:0%; width:100%;">
					<div class="row" style="margin:0px; padding: 0px;">
						<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 text-center">
							<div class="titleSection"><?=MStranslate('vieni_a_trovarci')?></div>
							<div>
								<iframe class="map-frame" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" 
									src="https://maps.google.it/maps?f=q&amp;source=s_q&amp;hl=it&amp;geocode=&amp;q=Via+Sonzini,+25,+Galatina,+LE&amp;aq=0&amp;oq=via+sonzini+25&amp;sll=41.008099,16.727239&amp;sspn=3.150242,4.954834&amp;t=m&amp;ie=UTF8&amp;hq=&amp;hnear=Via+Sonzini,+25,+Galatina,+Lecce,+Puglia&amp;z=14&amp;ll=40.17726,18.169935&amp;output=embed&iwloc=near">
								</iframe>

								<div style="text-align:left; margin-bottom: 3%;">
									<h5>GALATINA</h5>
									<h6>Via Sonzini, 25 73013 Galatina (Le) &#8226;<img src="images/icon-phone.png" height="16px" style="margin-left:10px;margin-right:5px">0836564849 &#8226; <img src="images/icon-fax.png" height="16px" style="margin-left:10px;margin-right:5px"> 0836843111</h6>
								</div>

								<iframe class="map-frame" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" 
									src="https://maps.google.it/maps?f=q&amp;source=s_q&amp;hl=it&amp;geocode=&amp;q=Piazza+del+Popolo,+18+00187+Roma&amp;aq=&amp;sll=40.177004,18.171773&amp;sspn=0.012968,0.023046&amp;ie=UTF8&amp;hq=&amp;hnear=Piazza+del+Popolo,+18,+00187+Roma,+Lazio&amp;t=m&amp;z=14&amp;ll=41.910246,12.477104&amp;output=embed&iwloc=near">
								</iframe>

								<div style="text-align:left; margin-bottom: 3%;">
									<h5>ROMA</h5>
									<h6>Piazza del Popolo, 18 00187 Roma <!-- &#8226; <img src="images/icon-phone.png" height="16px" style="margin-left:10px;margin-right:5px"> 0636712397 &#8226; <img src="images/icon-fax.png" height="16px" style="margin-left:10px;margin-right:5px"> 0636712400 --></h6>
								</div>

								<iframe class="map-frame" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" 
									src="https://maps.google.it/maps?f=q&amp;source=s_q&amp;hl=it&amp;geocode=&amp;q=Vicolo+Valle,+9,+37122+Verona+VR&amp;aq=0&amp;oq=Vicolo+Valle,+9&amp;sll=45.437828,10.986429&amp;sspn=3.150242,4.954834&amp;t=m&amp;ie=UTF8&amp;hq=&amp;hnear=Vicolo+Valle,+9,+37122+Verona+VR&amp;z=14&amp;ll=45.437828,10.986429&amp;output=embed&iwloc=near">
								</iframe>

								<div style="text-align:left; margin-bottom: 3%;">
									<h5>VERONA</h5>
									<h6> Vicolo Valle 9, 37122 Verona</h6>
								</div>

								<iframe class="map-frame" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" 
									src="https://maps.google.it/maps?f=q&amp;source=s_q&amp;hl=it&amp;geocode=&amp;q=95+3rd+St,+San+Francisco,+CA+94103&amp;aq=0&amp;oq=95+3rd+St,+San+Francisco,+CA+94103&amp;sll=37.7865714,-122.4021233&amp;sspn=3.150242,4.954834&amp;t=m&amp;ie=UTF8&amp;hq=&amp;hnear=95+3rd+St,+San+Francisco,+CA+94103&amp;z=10&amp;ll=37.7865714,-122.4021233&amp;output=embed&iwloc=near">
								</iframe>
								
								<div style="text-align:left; margin-bottom: 3%;">
									<h5>SAN FRANCISCO</h5>
									<h6>MediaSoft US c/o Fyberloom Inc.</h6>
									<h6>95 Third Street 2nd Floor - San Francisco - California 94103 - United States of America</h6>
								</div>

							</div>
						</div>

						<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 text-center">
							<div class="titleSection"><?=MStranslate('oppure_scrivici')?></div>
							<div style="background-color: #ffffff; padding: 20px 20px 20px 20px;">
								<form id="submitFormScrivici" class="form-horizontal" role="form">

									<div class="form-group">
										<label for="txtNomeScrivici" class="col-sm-3 control-label"><?=MStranslate('nome')?></label>
										<div class="col-sm-9">
											<input type="text" class="form-control" id="txtNomeScrivici">
										</div>
									</div>

									<div class="form-group">
										<label for="txtCognomeScrivici" class="col-sm-3 control-label"><?=MStranslate('cognome')?></label>
										<div class="col-sm-9">
											<input type="text" class="form-control" id="txtCognomeScrivici">
										</div>
									</div>

									<div class="form-group">
										<label for="txtEmailScrivici" class="col-sm-3 control-label">EMAIL</label>
										<div class="col-sm-9">
											<input type="email" class="form-control" id="txtEmailScrivici">
										</div>
									</div>

									<div class="form-group">
										<label for="txtMessaggioScrivici" class="col-sm-3 control-label"><?=MStranslate('messaggio')?></label>
										<div class="col-sm-9">
											<textarea class="form-control" id="txtMessaggioScrivici" rows="5" style="resize:none;"></textarea>
										</div>
									</div>

								  <div class="form-group">
									<div class="col-sm-offset-2 col-sm-10 text-left">
									  <div class="checkbox">
										<label>
										  <input type="checkbox" id="chkPrivacyScrivici">
										</label>
										<a href="#" id="disclaimerHref"><?=MStranslate('messaggio_condizioni_privacy')?></a>
									  </div>
									</div>
								  </div>

								  <div class="form-group">
									<div class="col-sm-offset-2 col-sm-10">
									  <p style="margin-top:10px;"><a id="submitButtonScrivici" class="pulsanteReadMore"><?=MStranslate('invia')?></a></p>
									  <!-- <button id="submitButtonScrivici" type="button" class="btn btn-default"><?=MStranslate('invia')?></button>-->
									
									</div>
								  </div>
								</form>

							</div>
						</div>
					</div>
				</div>

				<div class="modal" id="modalDisclaimer" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				  <div class="modal-dialog">
					<div class="modal-content">
					  <div class="modal-header" style="border:none;padding:10px 10px 30px 10px;">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					  </div>
					  <div class="modal-body" >
							<?=MStranslate('disclaimer')?>
					  </div>
					</div><!-- /.modal-content -->
				  </div><!-- /.modal-dialog -->
				</div><!-- /.modal -->

				<div class="modal" id="myModalScrivici" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				  <div class="modal-dialog">
					<div class="modal-content">
					  <div class="modal-header" style="border:none;">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					  </div>
					  <div class="modal-body" id="alertBodyScrivici"></div>

					</div><!-- /.modal-content -->
				  </div><!-- /.modal-dialog -->
				</div><!-- /.modal -->

				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center" style="padding: 0px; margin: 0px; ">
				<hr style="color: #B1C903; background-color: #B1C903; height: 2px; width:100%">
				</div>
			
			</div> <!-- sezione 2 -->

			<div id="section3" class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-left" style="padding: 10px 20px 25px 20px; margin: 0px 0px 0px 0px;"> 		
				<div class="container fill" style="margin: 0px; padding-top:0%; padding-left:0%; padding-right:0%; width:100%;">
					<div class="row" style="margin:0px; padding: 0px;">
						
						<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 text-center">
							<div class="titleSection"><?=MStranslate('lavora_con_noi')?></div>
							<div style="background-color: #ffffff; padding: 20px 20px 20px 20px;">
								<form id="submitFormLavoraConNoi" class="form-horizontal" role="form">

									<div class="form-group">
										<label for="txtNomeLavoraConNoi" class="col-sm-3 control-label"><?=MStranslate('nome')?></label>
										<div class="col-sm-9">
											<input type="text" class="form-control" id="txtNomeLavoraConNoi">
										</div>
									</div>

									<div class="form-group">
										<label for="txtCognomeLavoraConNoi" class="col-sm-3 control-label"><?=MStranslate('cognome')?></label>
										<div class="col-sm-9">
											<input type="text" class="form-control" id="txtCognomeLavoraConNoi">
										</div>
									</div>

									<div class="form-group">
										<label for="txtEmailLavoraConNoi" class="col-sm-3 control-label">EMAIL</label>
										<div class="col-sm-9">
											<input type="email" class="form-control" id="txtEmailLavoraConNoi">
										</div>
									</div>

									<div class="form-group">
										<label for="txtMessaggioLavoraConNoi" class="col-sm-3 control-label"><?=MStranslate('messaggio')?></label>
										<div class="col-sm-9">
											<textarea class="form-control" id="txtMessaggioLavoraConNoi" rows="5" style="resize:none;"></textarea>
										</div>
									</div>

								  <div class="form-group">
									<div class="col-sm-offset-2 col-sm-10 text-left">
									  <div class="checkbox">
										<label>
										  <input type="checkbox" id="chkPrivacyLavoraConNoi">
										</label>
										<a href="#" id="disclaimerHrefLavora"><?=MStranslate('messaggio_condizioni_privacy')?></a>
									  </div>
									</div>
								  </div>

								  <div class="form-group">
									<div class="col-sm-offset-2 col-sm-10">
  									  <p style="margin-top:10px;"><a id="submitButtonLavoraConNoi" class="pulsanteReadMore"><?=MStranslate('invia')?></a></p>
									  <!-- <button id="submitButtonLavoraConNoi" type="button" class="btn btn-default"><?=MStranslate('invia')?></button> -->
									</div>
								  </div>
								</form>

							</div>
						</div>

						
						<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 text-center;">
							<div class="titleSection" style="text-align:center" ><?=MStranslate('posizioni_aperte')?></div>
							<div id="posizioni_aperte" style="padding: 20px 20px 20px 20px;">
								<?php
									$indice=0;
									while($positionRow =  mysql_fetch_assoc($OpenPositionsRows))  {
										$indice++;
										?>
										<div>
											<div class="posizioniAperteTitolo"><?=$positionRow['titolo']?></div>
											<div class="posizioniAperteTesto"><?=$positionRow['descrizione_iniziale']?></div>
											<div id="collapse<?=$indice?>" class="collapse">
												<div class="posizioniAperteTesto"><?=$positionRow['descrizione_estesa']?></div>
											</div>
											<p style="margin-top:10px;"><a class="pulsanteReadMore" data-toggle="collapse" data-target="#collapse<?=$indice?>">MORE</a></p>
										</div>
										<?php
									}
									if ($indice==0) //non ci sono posizioni aperte
									{
										?>
										<div>
											<div class="posizioniAperteTesto">Non ci sono posizioni aperte, al momento</div>
										</div>
										<?php
									}
								?>										
							</div>
							
						</div>


					</div>
				</div>

				<!--
				<div class="modal" id="modalDisclaimerLavoraConNoi" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				  <div class="modal-dialog">
					<div class="modal-content">
					  <div class="modal-header" style="border:none;">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					  </div>
					  <div class="modal-body">
							<?//=MStranslate('disclaimer')?>
					  </div>
					</div>
				  </div>
				</div>  -->

				<div class="modal" id="myModalLavoraConNoi" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				  <div class="modal-dialog">
					<div class="modal-content">
					  <div class="modal-header" style="border:none;">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					  </div>
					  <div class="modal-body" id="alertBodyLavoraConNoi"></div>

					</div><!-- /.modal-content -->
				  </div><!-- /.modal-dialog -->
				</div><!-- /.modal -->

				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center" style="padding: 0px; margin: 0px; ">
				<hr style="color: #B1C903; background-color: #B1C903; height: 2px; width:100%">
				</div>
			
			</div> <!-- sezione 3 -->

			<!--
			<div id="section4" class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-left" style="padding: 10px 20px 25px 20px; margin: 0px 0px 0px 0px;"> 		
				<div class="container fill" style="margin: 0px; padding-top:0%; padding-left:0%; padding-right:0%; width:100%;">
					<div class="row" style="margin:0px; padding: 0px;">
						
						<div>
							<div class="titleSection" style="margin-left:8%; margin-right:8%; text-align:center; color:#b1c903;"><?=MStranslate('supporto')?></div>
							<div style="padding: 20px 20px 20px 20px;">
								<form id="submitFormSupport" class="form-horizontal" role="form">

									<div class="form-group">
										<label for="txtNomeSupport" class="col-sm-3 control-label"><?=MStranslate('nome-cognome')?></label>
										<div class="col-sm-6">
											<input type="text" class="form-control" id="txtNomeSupport">
										</div>
									</div>

									<div class="form-group">
										<label for="txtEmailSupport" class="col-sm-3 control-label">EMAIL</label>
										<div class="col-sm-6">
											<input type="email" class="form-control" id="txtEmailSupport">
										</div>
									</div>

									<div class="form-group">
										<label for="txtTelefonoSupport" class="col-sm-3 control-label"><?=MStranslate('telefono')?></label>
										<div class="col-sm-3">
											<input type="text" class="form-control" id="txtTelefonoSupport">
										</div>
									</div>

									<div class="form-group">
										<label for="txtProdottoSupport" class="col-sm-3 control-label"><?=MStranslate('prodotto')?></label>
										<div class="col-sm-6">
											<input type="text" class="form-control" id="txtProdottoSupport">
										</div>
									</div>

									<div class="form-group">
										<label for="txtTitoloSupport" class="col-sm-3 control-label"><?=MStranslate('livello_importanza')?></label>
										<div class="col-sm-6">
											<select name="livello" id="comboLevel" style="height: 34px;padding: 6px 12px;font-size: 14px;line-height: 1.428571429;color: #555555;border: 1px solid #4d4d4d;border-radius: 4px;">
												<option value="low">Bassa</option>
												<option value="medium" selected>Media</option>
												<option value="high">Alta</option>
												<option value="critical">Critica</option>
											</select>
										</div>
									</div>

									<div class="form-group">
										<label for="txtTitoloSupport" class="col-sm-3 control-label"><?=MStranslate('titolo')?></label>
										<div class="col-sm-6">
											<input type="text" class="form-control" id="txtTitoloSupport">
										</div>
									</div>

									<div class="form-group">
										<label for="txtMessaggioSupport" class="col-sm-3 control-label"><?=MStranslate('messaggio')?></label>
										<div class="col-sm-9">
											<textarea class="form-control" id="txtMessaggioSupport" rows="5" style="resize:none;"></textarea>
										</div>
									</div>

								  <div class="form-group">
									<div class="col-sm-offset-2 col-sm-10 text-left">
									  <div class="checkbox">
										<label>
										  <input type="checkbox" id="chkPrivacySupport">
										</label>
										<a href="#" id="disclaimerHrefSupport"><?=MStranslate('messaggio_condizioni_privacy')?></a>
									  </div>
									</div>
								  </div>

								  <div class="form-group">
									<div class="col-sm-offset-2 col-sm-10">
  									  <p style="margin-top:10px;"><a id="submitButtonSupport" class="pulsanteReadMore"><?=MStranslate('open-ticket')?></a></p>
									</div>
								  </div>
								</form>

							</div>
						</div>


					</div>
				</div>

				<div class="modal" id="myModalSupport" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				  <div class="modal-dialog">
					<div class="modal-content">
					  <div class="modal-header" style="border:none;">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					  </div>
					  <div class="modal-body" id="alertBodySupport"></div>

					</div>
				  </div>
				</div>

				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center" style="padding: 0px; margin: 0px; ">
				<hr style="color: #B1C903; background-color: #B1C903; height: 2px; width:100%">
				</div>
			
			</div> -->	   <!-- sezione 4 -->		

			<div id="section5" class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-left" style="padding: 10px 20px 25px 20px; margin: 0px 0px 0px 0px;"> 		
				<div class="container fill" style="margin: 0px; padding-top:0%; padding-left:0%; padding-right:0%; width:100%;">
					<div class="row" style="margin:0px; padding: 0px;">
						
						<div>
							<div class="titleSection" style="margin-left:8%; margin-right:8%; text-align:center; color:#b1c903;"><?=MStranslate('area-stampa')?></div>

							<?php
								$indice=0;
								while($positionRassegnaRow =  mysql_fetch_assoc($rassegnaStampaRows))
								{
									$indice++;
									?>

									<div class="posizioniAperteTitolo" style="padding-top: 0px;"><?=$positionRassegnaRow['titolo']?></div>
									<div id="testo" style="display:block;  margin: 0% 0% 0% 0%; font-size: 15px; overflow-y:auto; text-align: justify; padding-bottom: 5px;"> 
										<div style="margin: 0% 0% 0% 0%;padding-bottom: 5px;width:20%;float: left;"> 
											<div style="float: left; width:95%;padding: 5px 0px 0px 10px;"><img src="images/<?=$positionRassegnaRow['imageURL']?>" alt="" style="max-width:100%;border: solid 2px #b1c903"></div>
										</div>
										
										<?php
										//se il testo � molto lungo, vedo di dividerlo...
										$numChar=300;
										if (strlen($positionRassegnaRow['testo'])>$numChar)
										{
											$parte1=substr($positionRassegnaRow['testo'],0,$numChar);
											$parte2Temp=substr($positionRassegnaRow['testo'],$numChar);
											//dalla parte 2 arrivo al primo spazio.... quello che trovo lo tolgo dalla parte 2 e lo metto alla parte1
											$indexNextSpace = strpos($parte2Temp, ' '); 
											$parte1add = substr($parte2Temp,0,$indexNextSpace);
											$parte2 = substr($parte2Temp,$indexNextSpace);
											$parte1 = $parte1.$parte1add;
									
											?>
											<div class="posizioniAperteTesto"><?=$parte1?></div>
											<div id="collapseRassegna<?=$indice?>" class="collapse">
												<div class="posizioniAperteTesto" style="display:block;  margin: 0% 0% 0% 0%; text-align: justify; padding-bottom: 5px;"><?=$parte2?></div>
											</div>
											
											<?php
											//c'e' un url associato a questa rassegna?
											if ($positionRassegnaRow['link']=="" || $positionRassegnaRow['link']==null)
											{
												?>
												<p style="margin-top:10px;"><a id="readMoreRassegna<?=$indice?>" data-toggle="collapse" data-target="#collapseRassegna<?=$indice?>" class="pulsanteReadMore">MORE</a></p>
												<?php	
											}
											else
											{
												?>
												<p style="margin-top:10px;"><a id="readMoreRassegna<?=$indice?>" data-toggle="collapse" data-target="#collapseRassegna<?=$indice?>" class="pulsanteReadMore" style="margin-right:10px;">MORE</a><a class="pulsanteReadMore" href="<?=$positionRassegnaRow['link']?>"  target="_blank">LINK</a></p>
												<?php	
											}
										}
										else
										{
											//c'e' un url associato a questa rassegna?
											if ($positionRassegnaRow['link']=="" || $positionRassegnaRow['link']==null)
											{
												?>
												<div class="posizioniAperteTesto"><?=$positionRassegnaRow['testo']?></div>
												<?php	
											}
											else
											{
												?>
												<div class="posizioniAperteTesto"><?=$positionRassegnaRow['testo']?><p style="margin-top:10px;"><a class="pulsanteReadMore" href="<?=$positionRassegnaRow['link']?>" target="_blank">LINK</a></p></div>
												<?php	
											}

										}
										?>
									
									</div>



									<?php
								}
							?>										


							<div>
								<div class="posizioniAperteTitolo" >Media Kit</div>
								<span><?=MStranslate('contact_copy_cut')?></span>
								<div style="padding: 5px;">
									<div>
										<span><?=MStranslate('contact_copy_cut1')?></span><br>
										<img style="border: 1px solid #b1c903; background: #888888;padding:10px; margin-top:5px; margin-bottom:5px" alt="" src="images/logo_mediasoft_black.png"><br>
										<textarea class="snippet" rows="3" cols="100" name="textarea">&lt;a title="Mediasoftonline.com" href="http://www.mediasoftonline.com" target="_blank"&gt;&lt;img src="http://www.mediasoftonline.com/images/logo_mediasoft_black.png" alt="MediasoftOnline.com" border="0" /&gt;&lt;/a&gt;</textarea>
									</div>
									<br>
									<div>
										<span><?=MStranslate('contact_copy_cut2')?></span><br>
										<img style="border: 1px solid #b1c903; background: #888888;padding:10px; margin-top:5px; margin-bottom:5px" alt="" src="images/logo_mediasoft_white.png"><br>
										<textarea class="snippet" rows="3" cols="100" name="textarea">&lt;a title="Mediasoftonline.com" href="http://www.mediasoftonline.com" target="_blank"&gt;&lt;img src="http://www.mediasoftonline.com/images/logo_mediasoft_white.png" alt="MediasoftOnline.com" border="0" /&gt;&lt;/a&gt;</textarea>
									</div>
									<br>
									<div>
										<span><?=MStranslate('contact_copy_cut3')?></span><br>
										<img style="border: 1px solid #b1c903; background: #888888;padding:10px; margin-top:5px; margin-bottom:5px" alt="" src="images/logo_mediasoft_green.png"><br>
										<textarea class="snippet" rows="3" cols="100" name="textarea">&lt;a title="Mediasoftonline.com" href="http://www.mediasoftonline.com" target="_blank"&gt;&lt;img src="http://www.mediasoftonline.com/images/logo_mediasoft_green.png" alt="MediasoftOnline.com" border="0" /&gt;&lt;/a&gt;</textarea>
									</div>
									<br>
									<div>
										<span><?=MStranslate('contact_copy_cut4')?></span><br>
										<img style="border: 1px solid #b1c903; background: #888888;padding:10px; margin-top:5px; margin-bottom:5px" alt="" src="images/logo_mediasoft_notext_black.png"><br>
										<textarea class="snippet" rows="3" cols="100" name="textarea">&lt;a title="Mediasoftonline.com" href="http://www.mediasoftonline.com" target="_blank"&gt;&lt;img src="http://www.mediasoftonline.com/images/logo_mediasoft_notext_black.png" alt="MediasoftOnline.com" border="0" /&gt;&lt;/a&gt;</textarea>
									</div>
									<br>
									<div>
										<span><?=MStranslate('contact_copy_cut5')?></span><br>
										<img style="border: 1px solid #b1c903; background: #888888;padding:10px; margin-top:5px; margin-bottom:5px" alt="" src="images/logo_mediasoft_notext_white.png"><br>
										<textarea class="snippet" rows="3" cols="100" name="textarea">&lt;a title="Mediasoftonline.com" href="http://www.mediasoftonline.com" target="_blank"&gt;&lt;img src="http://www.mediasoftonline.com/images/logo_mediasoft_notext_white.png" alt="MediasoftOnline.com" border="0" /&gt;&lt;/a&gt;</textarea>
									</div>
									<br>
								</div>
							</div>
							
							<div>
								<!--<a href="allegati/presentazione_corporate_it.pdf" target="_blank"><p style="font-size: 18px;font-weight: bold;padding-top: 5px;padding-bottom: 5px;color:#58585a"><img src="images/ppt.png" height="20px" style="margin-right:10px;"\>Presentazione (Italiano)</p></a>
								<a href="allegati/presentazione_corporate_en.pdf" target="_blank"><p style="font-size: 18px;font-weight: bold;padding-top: 5px;padding-bottom: 5px;color:#58585a"><img src="images/ppt.png" height="20px" style="margin-right:10px;"\>Presentazione (English)</p></a>-->	
								<a href="allegati/curriculum_corporate_it.pdf" target="_blank"><p style="font-size: 18px;font-weight: bold;padding-top: 5px;padding-bottom: 5px;color:#58585a"><img src="images/ppt.png" height="20px" style="margin-right:10px;"\>Curriculum (Italiano)</p></a>
								<a href="allegati/curriculum_corporate_en.pdf" target="_blank"><p style="font-size: 18px;font-weight: bold;padding-top: 5px;padding-bottom: 5px;color:#58585a"><img src="images/ppt.png" height="20px" style="margin-right:10px;"\>Curriculum (English)</p></a>
							</div>

						</div>


					</div>
				</div>

				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center" style="padding: 0px; margin: 0px; ">
				<hr style="color: #B1C903; background-color: #B1C903; height: 2px; width:100%">
				</div>
			
			</div> <!-- sezione 5 -->			
			
		</div>
	
	</div>



	
	<div id="navbar-laterale" style="position:fixed;width:12%;margin: 10px;  right:15px; top: 180px;">
		<ul id="lateralMenu" class="nav nav-pills nav-stacked">
			<li><a href="#section1"><img src="images/pallino_verde_20.png" style="width:8px;height:8px;margin-right:6px;margin-bottom:2px;"\><?=MStranslate('sidebar_contatti_sedi')?></a></li>
			<li><a href="#section2"><img src="images/pallino_verde_20.png" style="width:8px;height:8px;margin-right:6px;margin-bottom:2px;"\><?=MStranslate('sidebar_contatti_contatti')?></a></li>
			<li><a href="#section3"><img src="images/pallino_verde_20.png" style="width:8px;height:8px;margin-right:6px;margin-bottom:2px;"\><?=MStranslate('sidebar_contatti_lavora')?></a></li>
			<!-- <li><a href="#section4"><img src="images/pallino_verde_20.png" style="width:8px;height:8px;margin-right:6px;margin-bottom:2px;"\>SUPPORTO</a></li> -->
			<li><a href="#section5"><img src="images/pallino_verde_20.png" style="width:8px;height:8px;margin-right:6px;margin-bottom:2px;"\><?=MStranslate('sidebar_contatti_as')?></a></li>
		</ul>
	</div>

	<div id="social" style="position:fixed; bottom:0px; margin: 10px;  right:20px; height:20px; z-index:999999;">
		<a href="https://www.facebook.com/Mediasoftsrl" target="_blank"><img src="images/social-fb.png" alt="FB" height="20px"></a>
		<a href="https://twitter.com/Mediasoftonline" target="_blank"><img src="images/social-tw.png" alt="TW" height="20px"></a>
		<a href="http://www.linkedin.com/company/1194297" target="_blank"><img src="images/social-lin.png" alt="LinkedIn" height="20px"></a>
	</div>

	<script src="jquery-1.10.2.min.js"></script>
	<script src="jquery.jscroll.min.js"></script>
	<script src="bootstrap/js/bootstrap.js"></script>
	<script src="functions.js"></script>
	<script src="ThumbnailGridExpandingPreview/js/modernizr.custom.js"></script>
	<script src="ThumbnailGridExpandingPreview/js/grid.js"></script>

	
	

	<script type="text/javascript">

		
		var pages_config = new Array();

		pages_config[$.urlParam('idPage')] =
		{
			'idPage': $.urlParam('idPage'),
			'backColor': 'white',

			'renderer': function(row_data)
			{
				var idSezione = row_data['idSezione'];
				var type = row_data['type'];
				var url = row_data['url'];
				var titolo = row_data['titolo'];
				var testo = row_data["testo"];
				var media = row_data['subItems'];
				
				//alert("type: " + type);
				//in base al tipo ho una composizione differente
				var html;
				//tipo 10: Dove Siamo (mappa)
				if (type=="10")
				{
					html =	
					'<div id="section'+idSezione+'" class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-left" style="padding: 10px 20px 25px 20px; margin: 0px 0px 0px 0px;">' + 		
						'<div>' +
							'<div class="titleSection" style="margin-left:8%; margin-right:8%; text-align:center; margin-top:0px; margin-bottom:20px;">DOVE SIAMO</div>' +				
							'<div style="margin: 0% 0% 0% 0%;padding-bottom: 20px;"><img src="images/mappa_europa.png" style="max-width:100%;" alt="Smiley face"></div>' +
						'</div>' +
						'<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center" style="padding: 0px; margin: 0px; ">' +
						'<hr style="color: #B1C903; background-color: #B1C903; height: 2px; width:100%">' +
						'</div>' +
					'</div>' 
				}
				else if (type=="11") //vieni a trovarci e scrivici
				{
					html =	
					'<div id="section'+idSezione+'" class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-left" style="padding: 10px 20px 25px 20px; margin: 0px 0px 0px 0px;">' + 		
						'<div class="container fill" style="margin: 0px; padding-top:0%; padding-left:0%; padding-right:0%; width:100%;">' +
							'<div class="row" style="margin:0px; padding: 0px;">' +
								'<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 text-center">' +
									'<div class="titleSection">VIENI A TROVARCI</div>' +
									'<div>' +
										'<iframe class="map-frame" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.it/maps?f=q&amp;source=s_q&amp;hl=it&amp;geocode=&amp;q=Via+Sonzini,+25,+Galatina,+LE&amp;aq=0&amp;oq=via+sonzini+25&amp;sll=41.008099,16.727239&amp;sspn=3.150242,4.954834&amp;t=m&amp;ie=UTF8&amp;hq=&amp;hnear=Via+Sonzini,+25,+Galatina,+Lecce,+Puglia&amp;z=14&amp;ll=40.17726,18.169935&amp;output=embed&iwloc=near"></iframe>' +

										'<div style="text-align:left; margin-bottom: 3%;">' +
											'<h5>GALATINA</h5>' +
											'<h6>Via Sonzini, 25 73013 Galatina (Le) &#8226; Tel 0836564849 &#8226; Fax 0836843111</h6>' +
										'</div>' +

										'<iframe class="map-frame" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.it/maps?f=q&amp;source=s_q&amp;hl=it&amp;geocode=&amp;q=Piazza+del+Popolo,+18+00187+Roma&amp;aq=&amp;sll=40.177004,18.171773&amp;sspn=0.012968,0.023046&amp;ie=UTF8&amp;hq=&amp;hnear=Piazza+del+Popolo,+18,+00187+Roma,+Lazio&amp;t=m&amp;z=14&amp;ll=41.910246,12.477104&amp;output=embed&iwloc=near"></iframe>' +

										'<div style="text-align:left; margin-bottom: 3%;">' +
											'<h5>ROMA</h5>' +
											'<h6>Piazza del Popolo, 18 00187 Roma &#8226; Tel 0636712397 &#8226; Fax 0636712400</h6>' +
										'</div>' +
									'</div>' +
								'</div>' +

								'<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 text-center">' +
									'<div class="titleSection">OPPURE SCRIVICI</div>' +
									'<div style="background-color: #ffffff; padding: 20px 20px 20px 20px;">' +
										'<form id="submitForm" class="form-horizontal" role="form">' +

											'<div class="form-group">' +
												'<label for="txtNome" class="col-sm-3 control-label">NOME</label>' +
												'<div class="col-sm-9">' +
													'<input type="text" class="form-control" id="txtNomeScrivici">' +
												'</div>' +
											'</div>' +

											'<div class="form-group">' +
												'<label for="txtCognome" class="col-sm-3 control-label">COGNOME</label>' +
												'<div class="col-sm-9">' +
													'<input type="text" class="form-control" id="txtCognomeScrivici">' +
												'</div>' +
											'</div>' +

											'<div class="form-group">' +
												'<label for="txtEmail" class="col-sm-3 control-label">EMAIL</label>' +
												'<div class="col-sm-9">' +
													'<input type="email" class="form-control" id="txtEmailScrivici">' +
												'</div>' +
											'</div>' +

											'<div class="form-group">' +
												'<label for="txtMessaggio" class="col-sm-3 control-label">MESSAGGIO</label>' +
												'<div class="col-sm-9">' +
													'<textarea class="form-control" id="txtMessaggioScrivici" rows="5" style="resize:none;"></textarea>' +
												'</div>' +
											'</div>' +

										  '<div class="form-group">' +
											'<div class="col-sm-offset-2 col-sm-10 text-left">' +
											  '<div class="checkbox">' +
												'<label>' +
												  '<input type="checkbox" id="chkPrivacyScrivici">' +
												'</label>' +
												'<a href="#" id="disclaimerHref">Ho letto ed accettato le condizioni di privacy</a>' +
											  '</div>' +
											'</div>' +
										  '</div>' +

										  '<div class="form-group">' +
											'<div class="col-sm-offset-2 col-sm-10">' +
											  '<button id="submitButtonScrivici" type="button" class="btn btn-default">INVIA</button>' +
											'</div>' +
										  '</div>' +
										'</form>' +

									'</div>' +
								'</div>' +
							'</div>' +
						'</div>' +

						'<div class="modal" id="modalDisclaimer" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">' +
						  '<div class="modal-dialog">' +
							'<div class="modal-content">' +
							  '<div class="modal-header" style="border:none;">' +
								'<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>' +
							  '</div>' +
							  '<div class="modal-body">DISCLAIMER</div>' +
							'</div>' +
						  '</div>' +
						'</div>' +

						'<div class="modal" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">' +
						  '<div class="modal-dialog">' +
							'<div class="modal-content">' +
							  '<div class="modal-header" style="border:none;">' +
								'<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>' +
							  '</div>' +
							  '<div class="modal-body" id="alertBody"></div>' +

							'</div>' +
						  '</div>' +
						'</div>' +

						'<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center" style="padding: 0px; margin: 0px; ">' +
						'<hr style="color: #B1C903; background-color: #B1C903; height: 2px; width:100%">' +
						'</div>' +
					'</div>' 
				}
				return html;
			},
			'renderer2': function(row_data)
			{
				var idSezione = row_data['idSezione'];
				var titolo = row_data['titolo_menu_lat'];
				
				var html =	'<li><a href="#section'+idSezione+'">'+titolo+'</a></li>'
				return html;
			}
		}
		
	
	
		$(window).load(function() {

			//$.buildTopMenuNew($.urlParam('idPage'));
			$.tweakIpadLandscape();
			
		});

		$(document).ready(function() {

			$.setupNavbar();			
			//$.alignLeftAndRightMenuItems();
			$.centerNavbarByLogo();
			$.setThemeBlack();

			
			var mainPageConfig = pages_config[$.urlParam('idPage')];
			var next_content = mainPageConfig.next_content;
			var index = 0;
			
			//$.setDivContent2( mainPageConfig, true, false);


			$('a[href*=#]:not([href=#])').click(function()
			{
				
				if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
				  var target = $(this.hash);
				  
				  target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
				  
				  var name=($(target).attr('id'));
				  //alert(name.charAt(0));
				  
				  if (name.charAt(0)=="c") return false; //collapse
				  
				  if (target.length)
				  {
					$('html,body').animate(
					{
					  scrollTop: target.offset().top-110
					}, 500);
					return false;
				  }
				}
			});	

			
			$('body').scrollspy({ target: '#navbar-laterale' })
	
			var largh=Math.round(($('#section1').width()*0.95)/2);
			//alert(largh);
			//largh=Math.round(largh);
			//alert(largh);
			
			$('.fb-like-box').attr('data-width',largh);
			//massima larghezza twitter = 520
			if (largh>520) largh=520;
			$('.twitter-timeline').attr('width',largh);
			
			$('.fb-like-box').attr('data-height',470);
			$('.twitter-timeline').attr('height',400);
			
			
	
			$('[id^=readMoreRassegna]').click(function(event)
			{
				var $this = $(this);
				var testo = $this.text();
				if (testo=="MORE") $this.text("LESS"); 
				else $this.text("MORE");
			});

	
			$('#submitButtonScrivici').click(function() {
				$.onSubmitScrivici();
			});
			
			$('#submitButtonLavoraConNoi').click(function() {
				$.onSubmitLavoraConNoi();
			});

			$('#submitButtonSupport').click(function() {
				$.onSubmitSupport();
			});
			
			
			$('#disclaimerHref').click(function() {
				$('#modalDisclaimer').modal();
			});

			$('#disclaimerHrefLavora').click(function() {
				$('#modalDisclaimer').modal();
			});

			$('#disclaimerHrefSupport').click(function() {
				$('#modalDisclaimer').modal();
			});

			$.onSubmitScrivici = function() {
				if($('#chkPrivacyScrivici').is(':checked') == false) {

					$('#alertBodyScrivici').text("<?=MStranslate('errore_privacy_non_selezionata')?>");
					$('#myModalScrivici').modal();
					return false;
				}
				if($('#txtNomeScrivici').val() == "") {

					$('#alertBodyScrivici').text("<?=MStranslate('errore_inserire_il_nome')?>");
					$('#myModalScrivici').modal();
					return false;
				}
				if($('#txtCognomeScrivici').val() == "") {

					$('#alertBodyScrivici').text("<?=MStranslate('errore_inserire_il_cognome')?>");
					$('#myModalScrivici').modal();
					return false;
				}
				if( $.validateEmail($('#txtEmailScrivici').val()) == false) {

					$('#alertBodyScrivici').text("<?=MStranslate('errore_mail')?>");
					$('#myModalScrivici').modal();
					return false;
				}
				if($('#txtMessaggioScrivici').val() == "") {

					$('#alertBodyScrivici').text("<?=MStranslate('errore_messaggio')?>");
					$('#myModalScrivici').modal();
					return false;
				}
				$.ajax({

					/*dataType: 'json',*/
					type: 'POST',
					url: "mailsend.php",			  	
					data: {
						'txtNome': $('#txtNomeScrivici').val(),
						'txtCognome': $('#txtCognomeScrivici').val(),
						'txtEmail': $('#txtEmailScrivici').val(),
						'txtMessaggio': $('#txtMessaggioScrivici').val()
					}
					
				}).done(function(data) {

					//alert(data);
					$('#alertBodyScrivici').text("<?=MStranslate('messaggio_inviato')?>");
					$('#myModalScrivici').modal();
					$("#submitFormScrivici").trigger('reset'); 

				}).fail(function(jqXHR, textStatus, errorThrown ) {
					console.log(textStatus);
					console.log(errorThrown);
					alert( "error" );
				});

				
			}
			
			$.onSubmitLavoraConNoi = function() {
				if($('#chkPrivacyLavoraConNoi').is(':checked') == false) {

					$('#alertBodyLavoraConNoi').text("<?=MStranslate('errore_privacy_non_selezionata')?>");
					$('#myModalLavoraConNoi').modal();
					return false;
				}
				if($('#txtNomeLavoraConNoi').val() == "") {

					$('#alertBodyLavoraConNoi').text("<?=MStranslate('errore_inserire_il_nome')?>");
					$('#myModalLavoraConNoi').modal();
					return false;
				}
				if($('#txtCognomeLavoraConNoi').val() == "") {

					$('#alertBodyLavoraConNoi').text("<?=MStranslate('errore_inserire_il_cognome')?>");
					$('#myModalLavoraConNoi').modal();
					return false;
				}
				if( $.validateEmail($('#txtEmailLavoraConNoi').val()) == false) {

					$('#alertBodyLavoraConNoi').text("<?=MStranslate('errore_mail')?>");
					$('#myModalLavoraConNoi').modal();
					return false;
				}
				if($('#txtMessaggioLavoraConNoi').val() == "") {

					$('#alertBodyLavoraConNoi').text("<?=MStranslate('errore_messaggio')?>");
					$('#myModalLavoraConNoi').modal();
					return false;
				}
				$.ajax({

					/*dataType: 'json',*/
					type: 'POST',
					url: "mailsendLavoraConNoi.php",			  	
					data: {
						'txtNome': $('#txtNomeLavoraConNoi').val(),
						'txtCognome': $('#txtCognomeLavoraConNoi').val(),
						'txtEmail': $('#txtEmailLavoraConNoi').val(),
						'txtMessaggio': $('#txtMessaggioLavoraConNoi').val()
					}
					
				}).done(function(data) {

					$('#alertBodyLavoraConNoi').text("<?=MStranslate('messaggio_inviato')?>");
					$('#myModalLavoraConNoi').modal();
					$("#submitFormLavoraConNoi").trigger('reset'); 

				}).fail(function(jqXHR, textStatus, errorThrown ) {
					console.log(textStatus);
					console.log(errorThrown);
					alert( "error" );
				});
			}
			
			$.onSubmitSupport = function() {
				if($('#chkPrivacySupport').is(':checked') == false) {

					$('#alertBodySupport').text("<?=MStranslate('errore_privacy_non_selezionata')?>");
					$('#myModalSupport').modal();
					return false;
				}
				if($('#txtNomeSupport').val() == "") {

					$('#alertBodySupport').text("<?=MStranslate('errore_inserire_il_nome')?>");
					$('#myModalSupport').modal();
					return false;
				}
				if( $.validateEmail($('#txtEmailSupport').val()) == false) {

					$('#alertBodySupport').text("<?=MStranslate('errore_mail')?>");
					$('#myModalSupport').modal();
					return false;
				}
				if($('#txtTelefonoSupport').val() == "") {

					$('#alertBodySupport').text("<?=MStranslate('errore_inserire_il_telefono')?>");
					$('#myModalSupport').modal();
					return false;
				}
				if($('#txtProdottoSupport').val() == "") {

					$('#alertBodySupport').text("<?=MStranslate('errore_inserire_il_prodotto')?>");
					$('#myModalSupport').modal();
					return false;
				}
				if($('#txtTitoloSupport').val() == "") {

					$('#alertBodySupport').text("<?=MStranslate('errore_inserire_il_titolo')?>");
					$('#myModalSupport').modal();
					return false;
				}
				if($('#txtMessaggioSupport').val() == "") {

					$('#alertBodySupport').text("<?=MStranslate('errore_messaggio')?>");
					$('#myModalSupport').modal();
					return false;
				}
				$.ajax({

					/*dataType: 'json',*/
					type: 'POST',
					url: "mailsendSupport.php",			  	
					data: {
						'txtNome': $('#txtNomeSupport').val(),
						'txtEmail': $('#txtEmailSupport').val(),
						'txtProdotto': $('#txtProdottoSupport').val(),
						'txtTelefono': $('#txtTelefonoSupport').val(),
						'txtLevel': $('#comboLevel').val(),
						'txtTitolo': $('#txtTitoloSupport').val(),
						'txtMessaggio': $('#txtMessaggioSupport').val()
					}
					
				}).done(function(data) {

					$('#alertBodySupport').text("<?=MStranslate('messaggio_inviato')?>");
					$('#myModalSupport').modal();
					$("#submitFormSupport").trigger('reset'); 

				}).fail(function(jqXHR, textStatus, errorThrown ) {
					console.log(textStatus);
					console.log(errorThrown);
					alert( "error" );
				});

				
			}

			
		});






	</script>

  </body>

</html>
