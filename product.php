<?php

include 'lang.php';
include "./backoffice/DBAccess.php";

$dbInst = new DBAccess();
$dbInst->connOpen();

$lang = (isset($_GET["lang"]) ? $_GET["lang"] : "IT");
$idCollection = $_GET["idCollection"];

$res = $dbInst->getIdPageAssociataByRisorsa($idCollection);
$row = mysql_fetch_assoc($res);
$idPage = $row['pagina_idPagina'];

$extD = $dbInst->getDetailLangPagina($idPage, $lang, "Title_Posizionamento");
$row = mysql_fetch_assoc($extD);
$title = $row['valore'];

$extD = $dbInst->getDetailLangPagina($idPage,$lang,"Description");  
$row = mysql_fetch_assoc($extD);
$description = $row['valore'];

$extD = $dbInst->getDetailLangPagina($idPage,$lang,"Keywords");  
$row = mysql_fetch_assoc($extD);
$keywords = $row['valore'];

$dbInst->connClose();

?>


<!DOCTYPE html>
<html>
  <head>
    <title><?=$title?></title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="HandheldFriendly" content="true" />
    <meta name="description" content="<?=$description?>"/>
    <meta name="keywords" content="<?=$keywords?>"/>

    <link href='https://fonts.googleapis.com/css?family=Maven+Pro' rel='stylesheet' type='text/css'>
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
    <link href="navbar.customization.css" rel="stylesheet" media="screen">
    <link href="carousel.customization.css" rel="stylesheet" media="screen">
    <link href="content.customization.css" rel="stylesheet" media="screen">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

    <style>

	    body {
	    	padding: 0px;
	    	margin: 0px;
	    	font-family: 'Maven Pro';
	    	font-size: 13px;
	    	font-weight:500;
	    }

	    h2 {
	    	font-size: 55px;
	    	line-height: 55px;	 
	    	margin:0px;	    	
	    	color: #b1c903;
	    }

	    h3 {
	    	font-size: 27px;
	    	line-height: 27px;	    	
	    	margin:0px;
	    	padding: 0px;
	    	color: #b1c903;
	    }

	    #mainContentDiv h4 {
	    	font-size: 16px;
	    	line-height: 16px;
	    	color: #b1c903;
	    }

	    #mainContentDiv h5 {
	    	text-align: justify;
	    }

	    a:link, a:visited, a:hover {
	    	color:black;
	    	text-decoration: none;
	    }

    </style>
  </head>

  <body>

  	<div class="navbar-wrapper">
		<div class="container" id="navContainer" style="width:100%;">

			<nav class="navbar navbar-default transparent_navbar" role="navigation">

			  <!-- Brand and toggle get grouped for better mobile display -->
			  <div class="navbar-header">
			    <button type="button" class="navbar-toggle" data-toggle="collapse" id="buttonMobile" data-target="#bs-example-navbar-collapse-1">
			      <span class="sr-only">Toggle navigation</span>
			       <img id="logoButtonMobile" src="images/logo_mediasoft_notext_white.png">
			    </button>
			  </div>

			  <!-- Collect the nav links, forms, and other content for toggling -->
			  <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			    <ul class="nav navbar-nav" id="navBarMainUl">
			    </ul>

			  </div><!-- /.navbar-collapse -->
			</nav>

		</div>
	</div>


	<div id="mainContainerDiv" class="container fill" style="width:100%; margin: 0px; padding: 0px;">

		<div id="white-spacer-div"></div>

		<div id="image-from-page" style="background-image:url('images/prodotto.jpg'); background-repeat:no-repeat; background-position:center; background-size: cover; height:250px;"></div>
		
	</div>

	<script src="jquery-1.10.2.min.js"></script>
	<script src="jquery.jscroll.min.js"></script>
	<script src="bootstrap/js/bootstrap.min.js"></script>
	<script src="functions.js"></script>
	<script src="modernizr.custom.js"></script>



	<script type="text/javascript">


		$.setProductContent = function(idCollection) {

			$.ajax({

				dataType: 'json',
			  	url: "DBService.php",
			  	async:false,
			  	data: {
			  		'action': 'getCollectionItemInfo',
			  		'lang': $.getLanguage(),
			  		'idCollection': idCollection
			  	}
			  	
			}).done(function(jsonResponse) {
				
				var html =  
					'<div style="padding-left:8%; padding-right:8%; text-align:center; background-color: white;">' +
						'<div id="titlePage" style="padding: 30px 0px 30px 0px; color:#b1c903;">' + jsonResponse['titolo'] + '</div>' + 
						'<h5 style="text-align:justify; padding: 0px 0px 30px 0px;">' +  $('<div/>').html(jsonResponse['descrizione_estesa']).text() + '</h5>' +
					'</div>' +

					'<div style="padding-left:8%; padding-right: 8%; min-height: 100px; padding-top:15px;" style="padding-bottom:20px;">' +
						'<div class="row" id="rowCollection" style="padding: 0px; margin: 0px;">' + 
							
							'<div class="col-xs-12 col-sm-12 col-md-6 col-lg-5 text-center">' +

							'</div>' +
							
							'<div id="sezione-allegati" class="col-xs-12 col-sm-12 col-md-3 col-lg-3 text-center" style="padding-bottom:20px;">' + 
								'<h3 style="padding-bottom: 10px;"><?=translate('allegati')?></h3>' +
							'</div>' +

							'<div id="sezione-correlati" class="col-xs-12 col-sm-12 col-md-3 col-lg-4 text-center" style="padding-bottom:20px;">' + 
								'<h3 style="padding-bottom: 10px;"><?=translate('correlati')?></h3>' +
							'</div>' +

						'</div>' +
					'</div>';

				$('#mainContainerDiv').append( html );


				for(i=0; i<jsonResponse['allegati'].length; i++) {
					
					$('#sezione-allegati').append('<p><a target="_blank" href="backoffice/Offerte/Repository/' + jsonResponse['allegati'][i]['Path'] + '" download>' + jsonResponse['allegati'][i]['Nome'] + '</a></p>');

				};



			});

		}

		$(window).load(function() {

			$.buildTopMenu();
			
			$.tweakIpadLandscape();
			
			$.setThemeBlack();

		});


		$(document).ready(function() {

			$.setProductContent($.urlParam('idCollection'));

		});


	</script>

  </body>

</html>
