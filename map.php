<!DOCTYPE html>
<html>
  <head>
    <title>Mediasoft Srl</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="HandheldFriendly" content="true" />


    <link href='https://fonts.googleapis.com/css?family=Maven+Pro' rel='stylesheet' type='text/css'>
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
    <link href="navbar.customization.css" rel="stylesheet" media="screen">
    <link href="carousel.customization.css" rel="stylesheet" media="screen">
    <link href="content.customization.css" rel="stylesheet" media="screen">
    
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

    <style>

	    body {
	    	padding: 0px;
	    	margin: 0px;
	    	font-family: 'Maven Pro';
	    	font-size: 13px;
	    	font-weight:500;
	    }

    </style>
  </head>

  <body style="background-image:url('images/mappa_europa.png'); background-size: cover; background-position: center; filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='./images/mappa_europa.png', sizingMethod='scale'); -ms-filter progid:DXImageTransform.Microsoft.AlphaImageLoader(src='images/mappa_europa.png', sizingMethod='scale');">

  	<div class="navbar-wrapper">
		<div class="container" id="navContainer" style="width:100%;">

			<nav class="navbar navbar-default transparent_navbar" role="navigation">

			  <!-- Brand and toggle get grouped for better mobile display -->
			  <div class="navbar-header">
			    <button type="button" class="navbar-toggle" data-toggle="collapse" id="buttonMobile" data-target="#bs-example-navbar-collapse-1">
			      <span class="sr-only">Toggle navigation</span>
			       <img id="logoButtonMobile" src="images/logo_mediasoft_notext_white.png">
			    </button>
			  </div>

			  <!-- Collect the nav links, forms, and other content for toggling -->
			  <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			    <ul class="nav navbar-nav" id="navBarMainUl"></ul>
			  </div><!-- /.navbar-collapse -->

			</nav>

		</div>
	</div>

	<script src="jquery-1.10.2.min.js"></script>
	<script src="bootstrap/js/bootstrap.min.js"></script>
	<script src="functions.js"></script>
	<script src="modernizr.custom.js"></script>

	<script type="text/javascript">

		$(window).load(function() {

			$.buildTopMenu();

			$.tweakIpadLandscape();

			$.setThemeWhite();	
					
		});


	</script>

  </body>

</html>
