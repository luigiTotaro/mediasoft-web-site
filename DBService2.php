<?php
include "./backoffice/DBAccess2.php";
require "./backoffice/JSON.php"; 
require "./backoffice/ClassObject/PaginaFlag.php"; 

function jsonfyAndEcho($ext)
{
	$rows = array();
	$countTotal = mysql_num_rows($ext);
	while($row =  mysql_fetch_assoc($ext)) {
			$rows[] = $row;
		}		
	echo json_encode($rows);
}

function cleanString($string){
	//$string = str_replace("/", "_", $string);
	$string = str_replace("%", "", $string);
	$string = str_replace("&", "", $string);
	$string = str_replace("$", "", $string);
	$string = str_replace("£", "", $string);
	$string = str_replace("?", "", $string);
	$string = str_replace(")", "", $string);
	$string = str_replace("(", "", $string);
	return $string;
}


function getSubMenu($dbInst, &$father,  $lang) {


	// menu di secondo livello
	$ext = $dbInst->getVociMenuTopByPadre($lang, $father['idVoceMenu']);

	while($row =  mysql_fetch_assoc($ext)) {

		//$resultset = $dbInst->getDetailLangPagina($row['Pagina_idPagina'], $lang, 'Url_Pagina');
		//$singleRow = mysql_fetch_assoc($resultset);		
		//$row['url'] = $singleRow['valore'];

		$subMenuItem = array(

			'idVoceMenu'	 	=>	$row['id'],
			'url' 				=>	$row['url'],
			'valore'			=>	$row['valore'],
			'subItems'			=>	array()

		);

		getSubMenu($dbInst, $subMenuItem, $lang);

		array_push($father['subItems'], $subMenuItem);

	}
	// submenu
}

function getMediaBySezione($dbInst, &$father,  $lang) {

	$ext = $dbInst->getMediaBySezione($lang, $father['idSezione']);

	while($row =  mysql_fetch_assoc($ext)) {

		$mediaItem = array(

			'idMedia'		 	=>	$row['id'],
			'tipo'				=>	$row['tipo'],
			'url' 				=>	$row['URL'],
			'url_link' 			=>	$row['URL_link'],
			'titolo'			=>	$row['titolo'],
			'descrizione'		=>	$row['descrizione'],
			'posizione'			=>	$row['posizione'],
			'sotto_sezione'		=>	$row['sotto_sezione']
		);

		array_push($father['subItems'], $mediaItem);

	}
}

function getIconBySezione($dbInst, &$father,  $lang) {
	$ext = $dbInst->getIconBySezione($lang, $father['idSezione']);
	while($row =  mysql_fetch_assoc($ext)) {

		$iconItem = array(
			'idIcon'		 	=>	$row['id'],
			'path'				=>	$row['path']
		);

		array_push($father['iconItems'], $iconItem);
	}
}


function getAllegatiBySezione($dbInst, &$father,  $lang) {

	$ext = $dbInst->getAllegatiBySezione($lang, $father['idSezione']);

	while($row =  mysql_fetch_assoc($ext)) {

		$allegatoItem = array(

			'id'			 	=>	$row['id'],
			'tipo'				=>	$row['tipo'],
			'icona'				=>	$row['icona'],
			'url' 				=>	$row['link'],
			'testo'				=>	$row['testo'],
			'isUrl'				=>	$row['isUrl']
		);

		array_push($father['subItemsAllegati'], $allegatoItem);

	}
}

function getFullMenu() {

	$message = "Entro in DBService2.getFullMenu";
	print_r($message,true);

	$lang = $_GET["lang"];
	$idPage = $_GET["idPage"];
	$dbInst = new DBAccess2();
	$dbInst->connOpen();

	$ext = $dbInst->getVociMenuTop($lang,$idPage);
	$menuItemList = array();

	while($row =  mysql_fetch_assoc($ext))  {

		//$resultset = $dbInst->getDetailLangPagina($row['Pagina_idPagina'], $lang, 'Url_Pagina');
		//$singleRow = mysql_fetch_assoc($resultset);		
		//$row['url'] = $singleRow['valore'];

		$menuItem = array(

				'idVoceMenu'		=>	$row['id'],
				'url' 				=>	$row['url'],
				'ordering'			=>	$row['ordering'],
				'SxDx'				=>	$row['SxDx'],
				'valore'			=>	$row['valore'],
				'colore'			=>	$row['colore'],
				'subItems'			=>	array()

		);

		getSubMenu($dbInst, $menuItem, $lang);
	
		array_push($menuItemList, $menuItem);
		
	}
/*
	echo "<pre>";
	print_r($menuItemList);
	echo "</pre>";
*/
	
	$dbInst->connClose();	
	echo json_encode($menuItemList);
}




function getVociMenuTop() {

	$lang = $_GET["lang"];
	$dbInst = new DBAccess();
	$dbInst->connOpen();

	$ext = $dbInst->getVociMenuTop($lang);
	$result = array();

	while($row =  mysql_fetch_assoc($ext))  {

		$resultset = $dbInst->getDetailLangPagina($row['Pagina_idPagina'], $lang, 'Url_Pagina');
		$singleRow = mysql_fetch_assoc($resultset);		
		$row['url'] = $singleRow['valore'];

		array_push($result, $row);
		
	}

	$dbInst->connClose();	
	echo json_encode($result);
}

function getVociMenuTopByPadre() {

	$lang = $_GET["lang"];
	$idPadre= $_GET["idPadre"];
	$dbInst = new DBAccess();
	$dbInst->connOpen();
		
	$result = array();

	$ext = $dbInst->getVociMenuTopByPadre($lang, $idPadre);

	while($row =  mysql_fetch_assoc($ext)) {

		$resultset = $dbInst->getDetailLangPagina($row['Pagina_idPagina'], $lang, 'Url_Pagina');
		$singleRow = mysql_fetch_assoc($resultset);		
		$row['url'] = $singleRow['valore'];

		array_push($result, $row);

	}

	$dbInst->connClose();	
	//jsonfyAndEcho($ext);
	echo json_encode($result);

}

function getVociMenuFooter() {

	$lang = $_GET["lang"];
	$idPage = $_GET["idPage"];
	$dbInst = new DBAccess2();
	$dbInst->connOpen();
	$ext = $dbInst->getVociMenuFooter($lang, $idPage);
	$rows = array();

	while($row =  mysql_fetch_assoc($ext))  {
		$rows[] = $row;
	}

	echo json_encode($rows);
	$dbInst->connClose();	
}

function getTimelineBylang($dbInst, &$father,  $lang) {

	$ext = $dbInst->getTimelineByLang($lang);

	while($row =  mysql_fetch_assoc($ext)) {

		$mediaItemTimeline = array(
			'id'		 	=>	$row['id'],
			'anno'				=>	$row['anno'],
			'progressivo' 				=>	$row['progressivo'],
			'titolo' 			=>	$row['titolo'],
			'descrizione'			=>	$row['descrizione'],
			'immagine'			=>	$row['immagine'],
			'link'		=>	$row['link']
		);

		array_push($father['subItemsTimeline'], $mediaItemTimeline);

	}
}

function getConfigTimeline($dbInst, &$father, $lang) {

	$ext = $dbInst->getConfigTimeline($lang);

	$countByAnnoTimeline = array();
	$annoTimeline = array();

	while($row =  mysql_fetch_assoc($ext)) {

		array_push($countByAnnoTimeline,$row['total']);
		array_push($annoTimeline,$row['anno']);

	}

	array_push($father['subTotalAnnoTimeline'], $countByAnnoTimeline);
	array_push($father['subAnnoTimeline'], $annoTimeline);
}


















function getClientiTestuali($dbInst, &$father) {
	$ext = $dbInst->getClientiTestuali();
	while($row =  mysql_fetch_assoc($ext)) {
		$colonne = array(
			'testo_colonna'		 	=>	$row['testo_colonna']
		);
		array_push($father['clientiTxt'], $colonne);
	}
}

function getClientiImg($dbInst, &$father) {
	$ext = $dbInst->getClientiImg();
	while($row =  mysql_fetch_assoc($ext)) {
		$mediaItemCliente = array(
			'nome'		 	=>	$row['nome'],
			'path'				=>	$row['path']
		);	
		if($row['sezione'] == 1){
			array_push($father['clientiImg1'], $mediaItemCliente);
		}else if($row['sezione'] == 2){
			array_push($father['clientiImg2'], $mediaItemCliente);
		}else if($row['sezione'] == 3){
			array_push($father['clientiImg3'], $mediaItemCliente);
		}	
	}
}

// FUNZIONE R&D
function getRDBylang($dbInst, &$father,  $lang) {

	//$ext = $dbInst->getTimelineByLang($lang);
	$ext = $dbInst->getRDByLang($lang);

	while($row =  mysql_fetch_assoc($ext)) {

		$mediaItemRD = array(
			'id'		 	=>	$row['id'],
			'titolo'				=>	$row['titolo'],
			'autore' 				=>	$row['autore'],
			'testo' 			=>	$row['testo'],
			'link'			=>	$row['link']
		);

		array_push($father['subItemsRD'], $mediaItemRD);

	}
}
// FINE FUNZIONE R&D

function getPaginaByLingua() {

	$lang = $_GET["lang"];
	$idPage = $_GET["idPage"];
	$dbInst = new DBAccess2();
	$dbInst->connOpen();

	$ext = $dbInst->getSezioniByPage($lang,$idPage);
	$PageItemList = array();

	while($row =  mysql_fetch_assoc($ext))  {

		$pageItem = array(

				'idSezione'			=>	$row['id'],
				'type' 				=>	$row['tipo'],
				'url'				=>	$row['URL'],
				'titolo'			=>	$row['titolo'],
				'testo'				=>	$row['testo'],
				'titolo_menu_lat'	=>	$row['titolo_menu_laterale'],
				'subItems'			=>	array(),
				'subItemsAllegati'	=>	array(),
				'subItemsTimeline'  =>	array(),
				'subTotalAnnoTimeline'  =>	array(),
				'subAnnoTimeline'  =>	array(),
				'clientiTxt'  =>	array(),
				'clientiImg1'  =>	array(),
				'clientiImg2'  =>	array(),
				'clientiImg3'  =>	array(),

				'iconItems'			=>	array(),
				'subItemsRD'			=>	array()
		);

		if($row['id'] == 6){
			getTimelineBylang($dbInst, $pageItem, $lang);
			getConfigTimeline($dbInst, $pageItem, $lang);
		}

		if($row['tipo'] == 7){
			getClientiTestuali($dbInst, $pageItem);
			getClientiImg($dbInst, $pageItem);
		}

		//TIPO R&D
		if($row['tipo'] == 16){
			getRDBylang($dbInst, $pageItem, $lang);
		}

		getMediaBySezione($dbInst, $pageItem, $lang);

		getIconBySezione($dbInst, $pageItem, $lang);
	
		getAllegatiBySezione($dbInst, $pageItem, $lang);
	
		array_push($PageItemList, $pageItem);
	}


/*


	$ext =  $dbInst->getPaginaByLingua($idPage, $lang, '');

	$rows = array();
	
	$result =  mysql_fetch_assoc($ext);
	
	$resFlag = $dbInst->getFlagPagina($idPage);
	
	$idFlag="";

	while($rowPag =  mysql_fetch_array($resFlag)) {
		$idFlag = $rowPag[1];
	}


	// recuper le immagini slideshow data un adeterminata pagina 
	$res = $dbInst->getRisorseFlagVetrinaGallery($idFlag, '0', $lang);
	
	$resources = array();

	while($rowRes =  mysql_fetch_array($res)) {

		if ($rowRes['campo']=='File' && $rowRes['valore']) {

			array_push($resources, array('path' => $rowRes['valore']));

		}

	}

	$result['resources'] = $resources;
	// fine 


	$res = $dbInst->getRisorseFlagVetrinaGallery($idFlag, '3', $lang);
	
	$collection = array();
	
	while($rowRes = mysql_fetch_assoc($res)) {

		if(!array_key_exists($rowRes['Risorsa_idRisorsa'], $collection)) {

			$collection[$rowRes['Risorsa_idRisorsa']] = array();
			$collection[$rowRes['Risorsa_idRisorsa']]['idRisorsa'] = $rowRes['Risorsa_idRisorsa'];
			$collection[$rowRes['Risorsa_idRisorsa']]['Data'] = $rowRes['inizioValidita'];



		}

		$collection[$rowRes['Risorsa_idRisorsa']][$rowRes['campo']] = $rowRes['valore'];
	}

	$result['collection'] = array_values($collection);
*/

	$dbInst->connClose();

	echo json_encode($PageItemList);

}



function getHighLights() {
	
	$lang = $_GET["lang"];
	$idPage = $_GET["idPage"];
	$dbInst = new DBAccess2();
	$dbInst->connOpen();
	$ext =  $dbInst->getHighLights($idPage, $lang);
	$dbInst->connClose();	
	jsonfyAndEcho($ext);

}

function getCollectionItemInfo() {

	$lang = $_GET["lang"];
	$idCollectionItem = $_GET["idCollection"];
	$dbInst = new DBAccess();
	$dbInst->connOpen();

	$res = $dbInst->getOffertaGirosalentoDettaglio($idCollectionItem);

	$detailsInfo = mysql_fetch_assoc($res[0]);



	$collectionItem = array(
		'idRisorsa' => $idCollectionItem,
		'titolo' => $detailsInfo['titolo' . $lang],
		'descrizione_breve' => $detailsInfo['pres' . $lang],
		'descrizione_estesa' => $detailsInfo['descr' . $lang],
		'foto' => array(),
		'video' => array(),
		'allegati' => array()
	);


	// recupero le foto associate ad una risorsa
	$resRisorseCollectionItem = $dbInst->getContenutoByRisorsaLingua($idCollectionItem, $lang, "FOTO");
	
	while($rowFoto = mysql_fetch_assoc($resRisorseCollectionItem)) 
		array_push(
			$collectionItem['foto'], 
			array(
				"Path" => $rowFoto['path'],
				"Nome" => $rowFoto['nome'],
				"Descrizione" => $rowFoto['descrizione']));

	// recupero gli allegati associati ad una risorsa
	$resRisorseCollectionItem = $dbInst->getContenutoByRisorsaLingua($idCollectionItem, $lang, "ALLEGATO");

	while($rowAllegato = mysql_fetch_assoc($resRisorseCollectionItem)) 
		array_push(
			$collectionItem['allegati'], 
			array(
				"Path" => $rowAllegato['path'],
				"Nome" => $rowAllegato['nome'],
				"Descrizione" => $rowAllegato['descrizione']));


	// recupero i link video associati ad una risorsa
	$resRisorseCollectionItem = $dbInst->getContenutoByRisorsaLingua($idCollectionItem, $lang, "VIDEO");

	while($rowVideo = mysql_fetch_assoc($resRisorseCollectionItem)) 
		array_push(
			$collectionItem['video'], 
			array(
				"Path" => $rowVideo['path'],
				"Nome" => $rowVideo['nome'],
				"Descrizione" => $rowVideo['descrizione']));

	$dbInst->connClose();

	echo json_encode(($collectionItem));
			
}





//retrieve dell'azione da effettuare
$param = $_GET["action"];
switch ($param ) {

	case "getFullMenu":
		getFullMenu();
	break;

	case "getVociMenuTop":
		getVociMenuTop();
	break;
	case "getVociMenuTopByPadre":
		getVociMenuTopByPadre();
	break;
	case "getVociMenuCarosel":
		getVociMenuFooter();
	break;
	case "getHighLights":
		getHighLights();
	break;

	case "getPaginaByLingua":
		getPaginaByLingua();
	break;

	case "getCollectionItemInfo":
	getCollectionItemInfo();
	break;
}


?>