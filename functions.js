/* preloading delle immagini per chrome */
var preload_images = new Array();
preload_images[0] = document.createElement('img');
preload_images[0].src = "images/logo_mediasoft_white.png";

preload_images[1] = document.createElement('img');
preload_images[1].src = "images/logo_mediasoft_black.png";

preload_images[2] = document.createElement('img');
preload_images[2].src = "images/logo_mediasoft_notext_white.png";

preload_images[3] = document.createElement('img');
preload_images[3].src = "images/logo_mediasoft_notext_black.png";

var carousel_style_by_index = new Array();

var month_translate = new Array("IT", "EN");
month_translate["IT"] = new Array();
month_translate["EN"] = new Array()

month_translate["IT"]["01"] = "GENNAIO";
month_translate["IT"]["02"] = "FEBBRAIO";
month_translate["IT"]["03"] = "MARZO";
month_translate["IT"]["04"] = "APRILE";
month_translate["IT"]["05"] = "MAGGIO";
month_translate["IT"]["06"] = "GIUGNO";
month_translate["IT"]["07"] = "LUGLIO";
month_translate["IT"]["08"] = "AGOSTO";
month_translate["IT"]["09"] = "SETTEMBRE";
month_translate["IT"]["10"] = "OTTOBRE";
month_translate["IT"]["11"] = "NOVEMBRE";
month_translate["IT"]["12"] = "DICEMBRE";

month_translate["EN"]["01"] = "JANUARY";
month_translate["EN"]["02"] = "FEBRUARY";
month_translate["EN"]["03"] = "MARCH";
month_translate["EN"]["04"] = "APRIL";
month_translate["EN"]["05"] = "MAY";
month_translate["EN"]["06"] = "JUNE";
month_translate["EN"]["07"] = "JULY";
month_translate["EN"]["08"] = "AUGUST";
month_translate["EN"]["09"] = "SEPTEMBER";
month_translate["EN"]["10"] = "OCTOBER";
month_translate["EN"]["11"] = "NOVEMBER";
month_translate["EN"]["12"] = "DECEMBER";


$.detectMobile = function() {

	//alert(navigator.userAgent);
	var check = false;

	if( navigator.userAgent.match(/Android/i)
 		|| navigator.userAgent.match(/webOS/i)
		|| navigator.userAgent.match(/iPhone/i)
		|| navigator.userAgent.match(/iPad/i)
		|| navigator.userAgent.match(/iPod/i)
		|| navigator.userAgent.match(/BlackBerry/i)
		|| navigator.userAgent.match(/Windows Phone/i)
		|| navigator.platform.match(/iPad/i) ){
		    check = true;
	} else {
	
			check = false;
		  
	}

	if(window.innerWidth <= 768) {
		check = true;
    }

	return check;

}

$.detectTablet = function() {

	var check = false;

	if ((navigator.userAgent.match(/Android/i)) && (window.innerWidth >= 1024)) check = true;
	if (navigator.userAgent.match(/iPad/i)) check = true;
	if (navigator.platform.match(/iPad/i)) check = true;

	return check;

}


$.tweakIpadLandscape = function() {

	// tweak per ipad su risoluzione landscape SOLO PER SAFARI...Chrome non ne ha bisgono
	//alert(navigator.userAgent.match(/iPad/i));
	//alert(navigator.userAgent.match(/Safari/i));
	/*
	
	if(navigator.userAgent.match(/iPad/i) != null && navigator.userAgent.match(/Safari/i) != null) {

		if (Math.abs(window.orientation) === 90) {

	        $("body").css("height","97.5%");
	        window.scrollTo(0,0);
	        $.setupNavbar();
	        $.alignLeftAndRightMenuItems();
			$.centerNavbarByLogo();

	    }

		window.onorientationchange = function() {

			if (Math.abs(window.orientation) === 90) {

		        $("body").css("height","97.5%");
		        window.scrollTo(0,0);
		        $.setupNavbar();
		        $.alignLeftAndRightMenuItems();
				$.centerNavbarByLogo();

		    } else {

		    	$("body").css("height","100%");

		    }

		};


		document.ontouchmove = function(event) {
		  event.preventDefault();
		}
	}
	
	*/
			
}


$.buildTopMenu = function() {

	var main_navbar_ul = $('#navBarMainUl');
	
	$.ajax({

		dataType: 'json',
	  	url: "DBService.php",
	  	async:false,
	  	data: {
	  		'action': 'getFullMenu',
	  		'lang': $.getLanguage()
	  	}
	  	
	}).done(function(data) {

		// aggiungo il menu home solo per i dispositivi mobili
		main_navbar_ul.append('<li id="linkHomeMobile"><a href="index.php?lang=' + $.getLanguage() + '">HOME</a></li>');

		$.each(data, function(key, val) {

			var idPagina = val["Pagina_idPagina"];
			var Valore = val["valore"];	
			var SxOrDx = (val["SxDx"] == 1 ? "left" : "right");
			var url = val["url"];
			var secondLevelItems = val['subItems'];

			if(Valore != "LOGO_SEGNAPOSTO") {

				if(secondLevelItems.length == 0) {

					main_navbar_ul.append('<li menu-position="' + SxOrDx + '" class="menu-' + SxOrDx + '-item"><a href="' + url + '">' + Valore + '</a><div class="underliner" style="top:20px;"/></li>');

				} else {

					var elem = 
					'<li menu-position="' + SxOrDx + '" class="dropdown menu-' + SxOrDx + '-item" id="father_internet">' +
						'<a href="#" ' + ($.detectMobile() ? '' : 'onclick="window.location.href=\'' + url + '\'"' ) + ' class="dropdown-toggle" data-toggle="dropdown">' + Valore + '</a>' +
						'<div class="underliner" style="top:20px;"/>' + 
						'<ul id="submenu_' + idPagina + '" class="dropdown-menu second-level-submenu-custom">' +
							'<ul class="list-inline">';
							
					for(i=0; i<secondLevelItems.length; i++) {
						
						var Valore = secondLevelItems[i]["valore"];						
						var url = secondLevelItems[i]["url"];
						elem += '<li><a href="' + url + '" id="link' + Valore.replace(" ","_") + '" menu-level="secondo">' + Valore + '</a><div class="underliner" style="top:35px;"/></li>';

					}

					elem +=
							'</ul>' +
						'</ul>' +
					'</li>';

					main_navbar_ul.append(elem);


					for(i=0; i<secondLevelItems.length; i++) {

						var nome_secondo_livello = secondLevelItems[i]["valore"].replace(" ","_");
						var thirdLevelItems = secondLevelItems[i]["subItems"];

						if(thirdLevelItems.length > 0) {

							var containing_ul = $('#submenu_' + idPagina);
							// per evitare 'spostamenti' del tag <ul> con classe .list-inline quando avviene un hover
							// devo settare, sul suddetto elemento, un float:left; Tale regola non vale quando i menù di secondo
							// livello sono privi di terzo livello
							containing_ul.children(".list-inline").css("float", "left");

							var colNumber = 0;

							if(thirdLevelItems.length == 1) colNumber = "12";
							else if(thirdLevelItems.length == 2) colNumber = "6";
							else colNumber = "4";

							var elem = 

							'<div id="' + nome_secondo_livello + 'Child" father-element="link' + nome_secondo_livello + '" menu-level="terzo" class="third-level-menu">' +
								'<div class="row">';

							for(j=0; j<thirdLevelItems.length; j++) {						

								elem += '<div class="col-sm-' + colNumber + ' col-md-' + colNumber + ' col-lg-' + colNumber + '">' + 
										'<a href="' + thirdLevelItems[j]["url"] + '">' + thirdLevelItems[j]["valore"] + '</a></div>';

							}


							elem += 
									'</div>' +
								'</div>';

							containing_ul.append(elem);


						}
						
					}

				}
				
			} else {

				// quando Valore == 'LOGO_SEGNAPOSTO' carico l'immagine centrale
				main_navbar_ul.append('<li id="navLogo"><a href="index.php?lang=' + $.getLanguage() + '"><img id="imageLogo" src="images/logo_mediasoft_black.png"></a></li>');

			}

		});

	});

	
	/*
	*	A fine caricamento dati per la generazione del menù cerco di centrare il menù
	*	rispetto allo schermo. Includere i metodi di centramento in un timeout (seppur di 0 millisecondo) 
	*	è un tweak per ie8 e ie7 nel caricamento dei dati in locale
	*/
	setTimeout(function() {

		$.setupNavbar();			
		$.alignLeftAndRightMenuItems();
		$.centerNavbarByLogo();

	}, 0);


}


$.buildTopMenuNew = function(idPage) {

	//alert("entro functions.js.buildTopMenuNew" + idPage);
	var main_navbar_ul = $('#navBarMainUl');
	var menu_color;
	
	$.ajax({

		dataType: 'json',
	  	url: "DBService2.php",
	  	async:false,
	  	data: {
	  		'action': 'getFullMenu',
	  		'lang': $.getLanguage(),
			'idPage': idPage
	  	}
	  	
	}).done(function(data) {

		//alert("arrivati i dati functions.js.buildTopMenuNew");
		// aggiungo il menu home solo per i dispositivi mobili
		main_navbar_ul.append('<li id="linkHomeMobile"><a href="index.php?lang=' + $.getLanguage() + '">HOME</a></li>');
		//alert(data);
		$.each(data, function(key, val) {
			
			var idVoceMenu = val["idVoceMenu"];
			var Valore = val["valore"];	
			var SxOrDx = (val["SxDx"] == 1 ? "left" : "right");
			var url = val["url"];
			var secondLevelItems = val['subItems'];
			var colore = val['colore'];
			menu_color = colore;
			
			if(Valore != "LOGO_SEGNAPOSTO") {

				if(secondLevelItems.length == 0)
				{
					var n = url.indexOf("idPage"); 
					var trovato=false;
					if (url.substr(n)==("idPage=" + idPage))  trovato = true;
					//alert(idPage + " - " + url + " - " + trovato);
					
					//alert(Valore + " - No SubItems");
					if (trovato) main_navbar_ul.append('<li menu-position="' + SxOrDx + '" class="menu-' + SxOrDx + '-item"><a href="' + url + '&lang=' + $.getLanguage() + '"><b>' + Valore + '</b></a><div class="underliner" style="top:20px;"/></li>');
					else main_navbar_ul.append('<li menu-position="' + SxOrDx + '" class="menu-' + SxOrDx + '-item"><a href="' + url + '&lang=' + $.getLanguage() + '">' + Valore + '</a><div class="underliner" style="top:20px;"/></li>');
				}
				else
				{
					//alert(Valore + " - " + secondLevelItems.length + " SubItems");

					var elem = 
					'<li menu-position="' + SxOrDx + '" class="dropdown menu-' + SxOrDx + '-item" id="father_internet">' +
						'<a href="#" ' + ($.detectMobile() ? '' : 'onclick="window.location.href=\'' + url + '\'"' ) + ' class="dropdown-toggle" data-toggle="dropdown">' + Valore + '</a>' +
						'<div class="underliner" style="top:20px;"/>' + 
						'<ul id="submenu_' + idVoceMenu + '" class="dropdown-menu second-level-submenu-custom">' +
							'<ul class="list-inline">';
							
					for(i=0; i<secondLevelItems.length; i++) {
						
						var Valore = secondLevelItems[i]["valore"];						
						var url = secondLevelItems[i]["url"];
						elem += '<li><a href="' + url + '" id="link' + Valore.replace(" ","_") + '" menu-level="secondo">' + Valore + '</a><div class="underliner" style="top:35px;"/></li>';

					}

					elem +=
							'</ul>' +
						'</ul>' +
					'</li>';

					main_navbar_ul.append(elem);


					for(i=0; i<secondLevelItems.length; i++) {

						var nome_secondo_livello = secondLevelItems[i]["valore"].replace(" ","_");
						var thirdLevelItems = secondLevelItems[i]["subItems"];

						if(thirdLevelItems.length > 0) {

							var containing_ul = $('#submenu_' + idPagina);
							// per evitare 'spostamenti' del tag <ul> con classe .list-inline quando avviene un hover
							// devo settare, sul suddetto elemento, un float:left; Tale regola non vale quando i menù di secondo
							// livello sono privi di terzo livello
							containing_ul.children(".list-inline").css("float", "left");

							var colNumber = 0;

							if(thirdLevelItems.length == 1) colNumber = "12";
							else if(thirdLevelItems.length == 2) colNumber = "6";
							else colNumber = "4";

							var elem = 

							'<div id="' + nome_secondo_livello + 'Child" father-element="link' + nome_secondo_livello + '" menu-level="terzo" class="third-level-menu">' +
								'<div class="row">';

							for(j=0; j<thirdLevelItems.length; j++) {						

								elem += '<div class="col-sm-' + colNumber + ' col-md-' + colNumber + ' col-lg-' + colNumber + '">' + 
										'<a href="' + thirdLevelItems[j]["url"] + '">' + thirdLevelItems[j]["valore"] + '</a></div>';

							}


							elem += 
									'</div>' +
								'</div>';

							containing_ul.append(elem);


						}
						
					}

				}
				
			} else {

				// quando Valore == 'LOGO_SEGNAPOSTO' carico l'immagine centrale
				main_navbar_ul.append('<li id="navLogo"><a href="index.php?lang=' + $.getLanguage() + '"><img id="imageLogo" src="images/logo_mediasoft_black.png"></a></li>');

			}

		});

	});

	
	/*
	*	A fine caricamento dati per la generazione del menù cerco di centrare il menù
	*	rispetto allo schermo. Includere i metodi di centramento in un timeout (seppur di 0 millisecondo) 
	*	è un tweak per ie8 e ie7 nel caricamento dei dati in locale
	*/
	setTimeout(function() {

		$.setupNavbar();			
		$.alignLeftAndRightMenuItems();
		$.centerNavbarByLogo();
		if (menu_color=="1") $.setThemeBlack();//nero
		else $.setThemeWhite(); 

	}, 0);


}

function loadCarousel(idPage) {
	//alert("chiedo i dati");
	$.ajax({

		dataType: 'json',
	  	url: "DBService2.php",
	  	async: false,
	  	data: {
	  		'action': 'getVociMenuCarosel',
	  		'lang': $.getLanguage(),
			'idPage': idPage
	  	}
	  	
	}).done(function(data) {
		//alert("arrivati i dati");

		var carouselIndex = 0;
		
		$.each( data, function( key, val ) {

			var img = val["URL"];
			var titolo_sezione = val["titolo"];
			var sottotitolo_sezione = val["sottotitolo"];
			if (!sottotitolo_sezione || sottotitolo_sezione.length === 0) sottotitolo_sezione="";
			var idPagina = val["URL_link"];
			var highlight_link = val["highlight_link"]; // se = 1 è un link da mostrare come highlight, altrimenti è una nuova pagina
			var buttonId = "btn" + val["id"];
			var descrizione = val["descrizione"];
			var bw = "0";
			
			carousel_style_by_index[carouselIndex] = bw;

			$("#carouselIndicators").append(
				'<li data-target="#mainCarousel" data-slide-to="' + carouselIndex + '" ' + (carouselIndex == 0 ? 'class="active"' : "") + '></li>');

			$("#carouselInnerContainer").append(
				'<div style="width:100%;" ' + (carouselIndex==0 ? 'class="active item"' : 'class="item"') + '>' + 
					'<div class="fill" ' + 
					' style="background-image:url(\'backoffice/Gallery/Repository/' + img + '\');' + 
					' filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src=\'./backoffice/Gallery/Repository/' + img + '\', sizingMethod=\'scale\');' +
					' -ms-filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src=\'backoffice/Gallery/Repository/' + img + '\', sizingMethod=\'scale\');' +
					'">' + 

						'<div class="container" name="carousel-content">' + 
							'<div class="carousel-caption">' + 
								'<h1 style="text-shadow:0px 0px 8px rgba(0, 0, 0, 1);">' + titolo_sezione + '</h1>' + 
								'<h3 style="text-shadow:0px 0px 6px rgba(0, 0, 0, 1);">' + sottotitolo_sezione + '</h3>' + 
								'<button type="button" id="' + buttonId +'" linked-page="' + idPagina + '" class="mediasoft-btn" style="text-shadow:0px 0px 5px rgba(0, 0, 0, 1);box-shadow:0px 0px 5px rgba(0, 0, 0, 1);">' + descrizione + '</button>' + 
							'</div>' + 
						'</div>' + 
					'</div>' + 
				'</div>');

			// lego un evento onclick sul bottone all'interno del 'carouselInnerContainer'
			$('#' + buttonId).click(function() {

				if (highlight_link==1)
				{
					var linkedPage = $(this).attr("linked-page");				
					manageClickOnCarouselButton(linkedPage, descrizione, highlight_link);
				}
				else window.open (idPagina,'_self',false);

			});

 
			carouselIndex++;

		});

	});

}


function manageClickOnCarouselButton(linkedPage, titolo_sezione, highlight_link) {
	//interrompo il carousel
	$('.carousel').carousel('pause');
	
	$.ajax({
		
		dataType: 'json',
	  	url: "DBService2.php",
	  	data: {
	  		'action': 'getHighLights',
	  		'lang': $.getLanguage(),
	  		'idPage': linkedPage
	  	}
	  	
	}).done(function(data) {

		//alert("arrivati highlight con idpagina=" + linkedPage);
		if(data.length == 0) {

			/* l'oggetto json restituito non ha elementi quindi si tratta di un agina verso cui fare redirect ...*/

		} else {

			/* l'oggetto json continene elementi quindi renderizzo il contenuto in un pannello modale all'interno della home */

			$("#modalContentWindowRow").empty();

			$.each( data, function( key, val ) {

				var titolo = val["titolo"];
				var descrizione = val["descrizione"];
				var media_div = "";
				var path = val["URL"];
				var link = val["URL_link"];

				

				if(val["tipo"] == "1") //immagine 
				{
					media_div = '<div class="modal-image-container" style="background-image:url(\'images/' + path + '\');background-size:contain;"></div>';
				}
				else if (val["tipo"] == "allegato")
				{
					media_div = '<div class="modal-image-container" style="background-image:url(\'backoffice/Menu/Repository/File.png\');"></div>';
				}
				else if (val["tipo"] == "5") //video
				{
					media_div = '<div class="modal-image-container"><iframe width="100%" height="100%" src="' + path + '?showinfo=0&controls=0&feature=player_embedded&rel=0" frameborder="0"></iframe></div>';
				}


				$("#modalContentWindowTitle").html(titolo_sezione);

				$("#modalContentWindowRow").append(

					'<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-full-height">' + 
						'<div class="thumbnail">' + 
							
							(link !="" ? '<a href="' + link + '" target="_blank">' + media_div + '</a>' : media_div)  +
						
							
							'<div class="caption">' +
								'<h5>' + titolo + '</h5>' +
								'<h6 style="overflow-y:auto;padding-right:4px;">' + descrizione + '</h6>' +
								(link !="" ? '<h7><a href="' + link + '" target="_blank">Read more</a></h7>' : '')  +
							'</div>' +

						'</div>' +
					'</div>'

				);

			});

			

			$("#carousel" + linkedPage).hide();

			$('#modalContentWindow').modal({

				'backdrop' : false,
				'keyboard': true

			});

		}				 	
		

	});

} 









$.setDivContent = function(params, isMainView, withScrollTo) {
	
	$.ajax({

		dataType: 'json',
	  	url: "DBService2.php",
	  	async:false,
	  	data: {
	  		'action': 'getPaginaByLingua',
	  		'lang': $.getLanguage(),
	  		'idPage': params.idPage
	  	}
	  	
	}).done(function(jsonResponse) {
		
		var containerDiv = $("[role='dynamic-div']").last();
		
		var lateralMenuDiv = $('#lateralMenu');

		containerDiv.css( 'background-color', params.backColor);

		//containerDiv.append('<a id="anchor' + params.idPage + '"></a>');
	
		//$.setPageContent(jsonResponse, containerDiv, params, isMainView);
		
		$.setPageCollectionContent(jsonResponse, containerDiv, params, isMainView);

		$.setLateralMenuContent(jsonResponse, lateralMenuDiv, params, isMainView);
		
		if(isMainView == true) {
			/*
			if(jsonResponse['BW'] == 0) {

				$.setThemeWhite();

			} else {

				$.setThemeBlack();

			}
			*/
			containerDiv.show();
		
		} else {

			containerDiv.hide().fadeIn(2000);

		}

	});

}

$.findIconByType = function(type) {
	
	var icona="";
	if (type==1) icona="man_utente.png";
	else if (type==2) icona="ppt.png";
	else if (type==3) icona="articolo.png";
	else if (type==4) icona="brochure.png";
	else if (type==5) icona="image.png";
	
	return icona;
}										

$.setDivContent2 = function(params, isMainView) {
	//alert("setDivContent2");
	$.ajax({

		dataType: 'json',
	  	url: "DBService2.php",
	  	async:false,
	  	data: {
	  		'action': 'getPaginaByLingua',
	  		//'lang': $.getLanguage(),
	  		'lang': $("#langHidden").val(),
	  		'idPage': params.idPage
	  	}
	  	
	}).done(function(jsonResponse) {

		//alert("risposta arrivata");
		var containerDiv = $("#mainContainerDiv");
		
		var lastSectionDiv = $("#sectionCompetenze");
		
		var lateralMenuDiv = $('#lateralMenu');

		containerDiv.css( 'background-color', params.backColor);

		//containerDiv.append('<a id="anchor' + params.idPage + '"></a>');
	
		//$.setPageContent(jsonResponse, containerDiv, params, isMainView);
		
		$.setPageCollectionContent(jsonResponse, containerDiv, params, isMainView);
		//$.setPageCollectionContent(jsonResponse, lastSectionDiv, params, isMainView);

		$.setLateralMenuContent(jsonResponse, lateralMenuDiv, params, isMainView);
		
		if(isMainView == true) {
			/*
			if(jsonResponse['BW'] == 0) {

				$.setThemeWhite();

			} else {

				$.setThemeBlack();

			}
			*/
			containerDiv.show();
		
		} else {

			containerDiv.hide().fadeIn(2000);

		}

	});

}




$.setPageContent = function(data, containerDiv, params, isMainView) {

	var decoded_body = $('<div/>').html(data["valore"]).text();
	var page_title = data["title"];
	
	//alert(data['resources'].length);
	
	// se sono presenti immagini diventa un carousel altrimenti un div vuoto di altezza inferiore
	if(data['resources'].length > 0) {

		// aggiunta del blocco carousel impostato sul 50% di altezza della pagina

		containerDiv.append(
			'<div id="page-carousel-div">' +
				'<div id="mainCarousel" class="carousel slide">' +
					'<div class="carousel-inner" id="carouselInnerContainer"></div>' +
				'</div>' +
			'</div>' 
		);

		for(i=0; i<data['resources'].length; i++) {

			var img = data['resources'][i]['path'];
		
			
			var toRender = '<div ' + (i == 0 ? 'class="active item"' : 'class="item"') + '>' + 
				'<div class="fill" ' + 
				' style="background-image:url(\'backoffice/Gallery/Repository/' + img + '\'); ' + 
				' filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src=\'./backoffice/Gallery/Repository/' + img + '\', sizingMethod=\'scale\'); ' +
				' -ms-filter progid:DXImageTransform.Microsoft.AlphaImageLoader(src=\'backoffice/Gallery/Repository/' + img + '\', sizingMethod=\'scale\');' +
				'"></div>' + 
			'</div>';
			
//			alert(toRender);
			
			
			$("#carouselInnerContainer").append(
			'<div ' + (i == 0 ? 'class="active item"' : 'class="item"') + '>' + 
				'<div class="fill" ' + 
				' style="background-image:url(\'backoffice/Gallery/Repository/' + img + '\'); ' + 
				' filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src=\'./backoffice/Gallery/Repository/' + img + '\', sizingMethod=\'scale\'); ' +
				' -ms-filter progid:DXImageTransform.Microsoft.AlphaImageLoader(src=\'backoffice/Gallery/Repository/' + img + '\', sizingMethod=\'scale\');' +
				'"></div>' + 
			'</div>');

		}	

	} else {
		
		// nessun carousel disponibile
		if(isMainView) {		
			// per evitare effetti di 'stacco grafico' fra una pagina dinamica e l'altra annullo questo div
			containerDiv.append(
				'<div id="white-spacer-div"></div>'
			);
		}

	}

	containerDiv.append('<div id="titlePage" style="margin-left:8%; margin-right:8%; text-align:center; margin-top:20px; margin-bottom:20px; color:#b1c903;">' + page_title + '</div>');		
	containerDiv.append('<div style="margin: 0% 8% 0% 8%; font-size: 15px; overflow-y:auto; text-align: justify; padding-bottom: 20px;" id="divContent">' + decoded_body + '</div>');
	
}



$.setPageCollectionContent = function(data, containerDiv, params) {

	//var collection = data['collection'];
	var collection = data;

	//alert(collection.length);
	
	if(collection.length > 0) {

		containerDiv.append(
			'<div style="margin-left:8%; margin-right:8%;">' +
				'<div class="row" id="rowCollection" role="dynamic-row-collection"></div>' +
			'</div>'
		);

		for(i=0; i<collection.length; i++) { 

			//alert(collection[i].idSezione + " - " + collection[i].type + " - " + collection[i].url + " - " + collection[i].titolo + " - " + collection[i].testo);
			//alert(collection[i].subItems.length);
			//alert(collection[i].subItemsAllegati.length);
			
			$('[role="dynamic-row-collection"]').last().append(

				params.renderer(collection[i])

			);
		
		}

	}
}

$.setLateralMenuContent = function(data, containerDiv, params) {

	var collection = data;
	//alert("setLateralMenuContent");

	if(collection.length > 0)
	{
		for(i=0; i<collection.length; i++)
		{ 
			//alert("Appendo in mezzo");
			containerDiv.append(params.renderer2(collection[i]));
		}
		//containerDiv.children[0].addClass('active');
	}
}






$.urlParam = function(name) {
    var results = new RegExp('[\\?&amp;]' + name + '=([^&amp;#]*)').exec(window.location.href);

    return (results == null ? "" : results[1] || 0);
}

$.getLanguage = function() {
	var lang = $('#langHidden').val();
	//var lang = $.urlParam('lang');
	if ((lang!="IT") && (lang!="EN")) lang="EN";
	return lang;
	//return (lang=="" ? "IT" : lang);
}


$.getPercWidth = function(width_container, width_element) {

	// width_container : 100 = width_element : x

	return ((width_element * 100) / width_container);

}


var prevMobileSize = ($(window).width() < 768);

$(window).resize(function() {

//	$.alignLeftAndRightMenuItems();

	// tweak necessario per reimpostare correttamente i sottomenù
	//quando si esce dalla modalità mobile a quella desktop
	if( ($(window).width() >= 768)  && prevMobileSize == true) {

		prevMobileSize = false;

		setTimeout(function() {

			$.setupNavbar();			
			$.alignLeftAndRightMenuItems();
			$.centerNavbarByLogo();

		}, 0);
    
	} 

	prevMobileSize = ($(window).width() < 768);
	
});



/* riposizione la navbar usando il logo 
come centro schermo */
$.centerNavbarByLogo = function() { 

	/* centro la navbar rispetto al centro dello schermo */
	
	var larghezzaLogo = $('#navLogo').innerWidth();

	var larghezzaSchermo = $( window ).width();

	var centroSchermoAsseX = larghezzaSchermo / 2;
	
	var centroImmagineAsseX = $( "#navLogo" ).offset().left + (larghezzaLogo / 2);

	var navBarRelativePositionX = centroSchermoAsseX - centroImmagineAsseX;

	if(navBarRelativePositionX <=0) {

		$('#navBarMainUl').css({

			'position' : 'relative',
			'left': navBarRelativePositionX + 'px'

		});

	} 
	
}


$.alignLeftAndRightMenuItems = function() {

	var larghezzaLogo = $('#navLogo').outerWidth();

	var larghezzaSchermo = $( window ).width();
	var centroSchermoAsseX = larghezzaSchermo / 2;

	var leftElements = $("#navBarMainUl > [menu-position='left']");
	var rightElements = $("#navBarMainUl > [menu-position='right']");
	
	var RxLxMargin = larghezzaSchermo * 0.09;
	var SpazioUtileRxLx = centroSchermoAsseX - RxLxMargin - (larghezzaLogo / 2);

	var totLxLength = 0;
	leftElements.each(function() {
		totLxLength += $(this).width();
	});

	var totRxLength = 0;
	rightElements.each(function() {
		totRxLength += $(this).width();
	});

	var spazioResiduoLx = SpazioUtileRxLx - totLxLength;
	var spazioResiduoRx = SpazioUtileRxLx - totRxLength;

	var spazioResiduoLxPerElemento = (spazioResiduoLx / leftElements.length);
	var spazioResiduoRxPerElemento = (spazioResiduoRx / rightElements.length);

	$('.menu-left-item').css('margin-right', spazioResiduoLxPerElemento + 'px');
	$('.menu-right-item').css('margin-left', spazioResiduoRxPerElemento + 'px');
	
	$('.menu-left-item').css('text-shadow', '0px 0px 6px rgba(0, 0, 0, 1)');
	//$('.menu-right-item').css('margin-left', spazioResiduoRxPerElemento + 'px');

 
}


/*
*
*
*/
$.clearAllNavbarSubSelection = function() {

	// quando si clicca su un menù padre TUTTI i menu figlio aperti devono essere chiusi
	$("[menu-level='terzo']").each(function(index){     
	     $(this).hide();
	});

	// quando si clicca su un menù di secondo livello TUTTI per tutti i menu secondo livello va rimossa la classe css 
	$("[menu-level='secondo']").each(function(index){     
	     $(this).removeClass('secondo-livello-selezionato');
	});

}


/*
Calcolo attributo left relativo di un padre rispetto ad un figlio
*/
$.getRelativeLeft = function(father, son) {
	return (father.outerWidth() / 2) - (son.outerWidth() / 2);
}

$.setupNavbar = function() {

	/*
	"Hovernav" navbar dropdown on hover
	Delete this segment if you don't want it, and delete the corresponding CSS in bst.css
	Uses jQuery Media Query - see http://www.sitepoint.com/javascript-media-queries/
	*/

	if (($(window).width() >= 768) && ($.detectMobile()==false)) {

		$('ul.navbar-nav li').addClass('hovernav');

	} else {

		$('ul.navbar-nav li').removeClass('hovernav');

	}

	$(window).resize(function() {

		if( $(window).width() >= 768 ) {

			$('ul.navbar-nav li').addClass('hovernav');

		} else {

			$('ul.navbar-nav li').removeClass('hovernav');

		}
		
	});

	/* partendo dall'elemento #navBarMainUI navigo tutti i 'li' di primo livello all'interno dei quali cerco gli 'ul' di 
	primo livello. se ne trovo uno allora è un menù di secondo livello che deve essere centrato rispetto al primo */
	$("#navBarMainUl > li > ul").each(function() {

		var son = $(this);
		var father = $(this).parent();

		son.css({
			'left': $.getRelativeLeft(father, son) + 'px'
		});

	});



	$("#navBarMainUl > li").each(function(index) {

		// quando si sposta il mouse fuori da un menù primo livello comunque TUTTI
		// i menu di terzo livello vanno chiusi
		$(this).hover(

			function(){},
			
			function() {

				$.clearAllNavbarSubSelection();
			
			}
		);

	});



	/* setup terzo livello */
	$("[menu-level='secondo']").each(function(index) {    
				
		$(this).hover(
	
	    	function() {

	    		// Attivo un hover sul secondo livello solo se 
	    		// lo schermo è più grande di 768 pixel.
				if($(window).width() >= 768) {
				 
		    		$.clearAllNavbarSubSelection();

					$(this).addClass('secondo-livello-selezionato');

					// un sotto-menu deve essere collegato al padre tramite l'attributo father-element
					// che deve coincidere con l'id dell'elemento padre stesso
					var search_list = $("[father-element='" + $(this).attr('id') + "']");

					// per i menu di secondo livello devo forzare la larghezza del div di sottolineatura sottostante
					$(this).parent().children("div").width( $(this).outerWidth(true) );

					// se viene rilevato un menu figlio questo deve essere apribile con un 'hover'
					if(search_list.length == 1) {
					
						// un menu di primo livello possiede menu di secondo livello (cioè un 'son')
						var Son =  $("#" + search_list[0].id);

						Son.css({ 
							"position" : "relative",				
							"left": $.getRelativeLeft($(this), Son) + "px"/*,									
							'display' :'block',
							'-webkit-animation': 'fadeIn 0.15s ease-in',
        					'animation': 'fadeIn 0.15s ease-in'*/	
						});

						Son.fadeIn(150);
					 
					}		

				} 

	    	}, function() {

	    	}
	 	);

	});
	
	// rimozione dell'evento onclick su tutti i link di primo livello (della navbar)
	// questo permette il solo funzionamento dell' hover
	// $('.menu-left-item, .menu-right-item').dropdown('toggle');

}


$.setThemeBlack = function() {

	//$("imageLogo").attr("src", "images/logo_mediasoft_black.png");
	document.getElementById("imageLogo").src = "images/logo_mediasoft_green.png";
	document.getElementById("logoButtonMobile").src = "images/logo_mediasoft_notext_black.png";
	$('#navBarMainUl a').css({ 'color': '#58585a' });
	$('#footer-custom-content').css({ 'color': '#58585a', 'border-color': '#58585a' });
	$('#footer-custom-content a').css({ 'color': '#58585a' });
	$('.underliner').css({"background-image": "url(images/line_black_v2.png)" });
	$('#modalContentWindowTitle').css({'color' : '#58585a'});
	$('body').css('background-color', '#ffffff');
	//$('body').css('background-color', '#ededed');

	

}

$.setThemeWhite = function() {

	//$("imageLogo").attr("src", "images/logo_mediasoft_white.png");
	document.getElementById("imageLogo").src = "images/logo_mediasoft_white_v2.png";
	document.getElementById("logoButtonMobile").src = "images/logo_mediasoft_notext_white.png";
	$('#navBarMainUl a').css({ 'color': 'white' });
	$('#footer-custom-content').css({ 'color': 'white', 'border-color': 'white' });
	$('#footer-custom-content a').css({ 'color': 'white' });
	$('.underliner').css({"background-image": "url(images/line_white_v3.png)" });
	$('#modalContentWindowTitle').css({'color' : 'white'});
	$('body').css('background-color', '#494949');
	
}

$.setCarouselColor = function(slideIndex, color) {

	if(Modernizr.borderradius) {

		$('.carousel-indicators li').css({'background-color': 'transparent'});
		$('[data-slide-to=' + slideIndex + ']').css({'background-color': color});
		$('.carousel-indicators li').css({'border-color': color});

	} else {

		$('#carouselIndicators li').css({
			'border': 'none',
			'background': 'url(images/blank.png) no-repeat center center',
			'filter': 'progid:DXImageTransform.Microsoft.AlphaImageLoader(src=\'./images/' + color + '_empty_circle.png\', sizingMethod=\'scale\')',
			'-ms-filter': 'progid:DXImageTransform.Microsoft.AlphaImageLoader(src=\'images/' + color + '_empty_circle.png\', sizingMethod=\'scale\')'
		});

		$('[data-slide-to=' + slideIndex + ']').css({
			'border': 'none',
			'background': 'url(images/blank.png) no-repeat center center',
			'filter': 'progid:DXImageTransform.Microsoft.AlphaImageLoader(src=\'./images/' + color + '_full_circle.png\', sizingMethod=\'scale\')',
			'-ms-filter': 'progid:DXImageTransform.Microsoft.AlphaImageLoader(src=\'images/' + color + '_full_circle.png\', sizingMethod=\'scale\')'
		});
	}
	
	
	$('.carousel-caption').css({'color': color});
	$('.mediasoft-btn').css({'color': color, 'border-color': color});
	
}


$.validateEmail = function(email){
	
	var emailReg = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
	var valid = emailReg.test(email);

	if(!valid) {
        return false;
    } else {
    	return true;
    }

}

$.getDay = function(data) {

	var parts = data.split("/");
	return parts[0];

}

$.getMonth = function(data) {

	var parts = data.split("/");
	var month = parts[1];
	var lang = $.getLanguage();

	return month_translate[lang][month];

}

$dividiTesto = function(text, numChar) {

	var ret = new Array();
	//devo arrivare allo spazio successivo al numero di caratteri impostato... non voglio dividere una parola a metà
	var parte2;
	var parte1=text.substr(0,numChar);
	var parte2Temp=text.substr(numChar);
	//console.log(parte1);
	//console.log(parte2Temp);
	//dalla parte 2 arrivo al primo spazio.... quello che trovo lo tolgo dalla parte 2 e lo metto alla parte1
	var indexNextSpace = parte2Temp.indexOf(" "); 
	if (indexNextSpace==-1) //non ci sono spazi da qui alla fine... metto tutto nella prima parte
	{
		parte1=text;
		parte2="";
	}
	else
	{
		var parte1add = parte2Temp.substr(0,indexNextSpace);
		parte2 = parte2Temp.substr(indexNextSpace);
		parte1+=parte1add;
	}
	ret.push(parte1);
	ret.push(parte2);

	return ret;

}