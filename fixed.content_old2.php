<?php

include 'lang.php';
//include "./backoffice/DBAccess.php";
include "./backoffice/DBAccess2.php";

$dbInst = new DBAccess2();
$dbInst->connOpen();

$lang = (isset($_GET["lang"]) ? $_GET["lang"] : "IT");
if (($lang!="IT") && ($lang!="EN")) $lang="IT";
$idPage = $_GET["idPage"];

$extD = $dbInst->getDetailLangPagina($idPage, $lang);
$row = mysql_fetch_array($extD);
$title = $row['TITLE'];
$description = $row['DESCRIPTION'];
$keywords = $row['KEYWORDS'];

$menuRows = $dbInst->getVociMenuTop($lang,$idPage);
//per ogni menu' devo vedere se ci sono sottomenu
	//$subMenuItem = $dbInst->getVociMenuTopByPadre($lang,$positionRow['id']);	
	//$subMenuRows[$positionRow['id']] = $dbInst->getVociMenuTopByPadre($lang,$positionRow['id']);
 
$dbInst->connClose();

?>


<!DOCTYPE html>
<html>
  <head>
    <title><?=$title?></title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="HandheldFriendly" content="true" />

    <meta name="description" content="<?=$description?>"/>
    <meta name="keywords" content="<?=$keywords?>"/>

    <link href='http://fonts.googleapis.com/css?family=Maven+Pro:900,500' rel='stylesheet' type='text/css'>
    <link href="bootstrap/css/bootstrap.css" rel="stylesheet" media="screen">
    <link href="bootstrap.customization.css" rel="stylesheet" media="screen">
    <link href="navbar.customization.css" rel="stylesheet" media="screen">
    <link href="carousel.customization.css" rel="stylesheet" media="screen">
    <link href="content.customization.css" rel="stylesheet" media="screen">
	<link href="ThumbnailGridExpandingPreview/css/default.css" rel="stylesheet" type="text/css"  />
	<link href="ThumbnailGridExpandingPreview/css/component.css" rel="stylesheet" type="text/css"  />
	<!-- <link href="jQueryTimeliner/css/style.css" rel="stylesheet"  media="screen" /> -->

	<link href="timeline/css/flat.css" rel="stylesheet" type="text/css" />
	<link href="timeline/css/style.css" rel="stylesheet" type="text/css" />
	<link href="timeline/css/lightbox.css" rel="stylesheet" type="text/css" />
	<link href="timeline/css/jquery.mCustomScrollbar.css" rel="stylesheet" type="text/css" />

	<!-- <link rel="stylesheet" href="arbor-v0.92/docs/sample-project/style.css" type="text/css"> -->
	
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
    
    <style>

	    body {
	    	padding: 0px;
	    	margin: 0px;
	    	font-family: 'Maven Pro';
	    	font-size: 13px;
	    	font-weight:500;
	    }

		q{
	    	font-size: 18px;
			font-weight:500;
	    	line-height: 30px;	 
		}	
		
	    #rowCollection h2 {
	    	font-size: 55px;
	    	line-height: 55px;	 
	    	margin:0px;
	    	padding: 0px;   	
	    	color: #b1c903;
	    }

	    #rowCollection h3 {
	    	font-size: 18px;
	    	line-height: 18px;	    	
	    	margin:0px;
	    	padding: 0px;
	    }

	    #rowCollection h4 {
	    	font-size: 17px;
	    	line-height: 17px;
	    	color: #b1c903;
	    }

	    #rowCollection h5 {
	    	text-align: justify;
	    }

	    a:link, a:hover, a:visited {
	    	text-decoration: none;
	    	color: #58585a;
			<!--color: #b1c903;-->
			
	    }
		
		b {
			font-weight:900;
		}		

    </style>
  </head>

  <body>

  
  
  	<div class="navbar-wrapper" style="position:fixed;background-color: #FFFFFF">
		<div class="container" id="navContainer" style="width:100%;">

			<nav class="navbar navbar-default transparent_navbar" role="navigation">

			  <!-- Brand and toggle get grouped for better mobile display -->
			  <div class="navbar-header">
			    <button type="button" class="navbar-toggle" data-toggle="collapse" id="buttonMobile" data-target="#bs-example-navbar-collapse-1">
			      <span class="sr-only">Toggle navigation</span>
			       <img id="logoButtonMobile" src="images/logo_mediasoft_notext_white.png">
			    </button>
			  </div>

			  <!-- Collect the nav links, forms, and other content for toggling -->
			  <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			    <ul class="nav navbar-nav" id="navBarMainUl" style="width:100%">
					<li id="linkHomeMobile"><a href="index.php?lang='<?=$lang?>'">HOME</a></li>
					<?php
						$indice=0;
						$dbInst = new DBAccess2();
						$dbInst->connOpen();
						
						while($positionRow =  mysql_fetch_assoc($menuRows))
						{
							$indice++;
							$url = $positionRow['url'];
							$SxOrDx = "";
							if ($positionRow['SxDx'] == 1) $SxOrDx="left";
							else $SxOrDx="right";
							if($positionRow['valore'] != "LOGO_SEGNAPOSTO")
							{
								//vedo se ci sono sottomenu
								$subMenuItems = $dbInst->getVociMenuTopByPadre($lang,$positionRow['id']);	
								$countTotal = mysql_num_rows($subMenuItems);
								
								
								if($countTotal == 0)
								//if(0 == 0)
								{
									$subString = strstr($url, 'idPage'); 
									$trovato=false;
									if ($subString==("idPage=".$idPage))  $trovato = true;
									//alert(idPage + " - " + url + " - " + trovato);
									
									//alert(Valore + " - No SubItems");
									if ($trovato)
									{
									?>
									<li menu-position="<?=$SxOrDx?>" class="menu-<?=$SxOrDx?>-item" style="width:14.2%;"><a href="<?=$positionRow['url']?>&lang=<?=$lang?>"><b><?=$positionRow['valore']?></b></a><div class="underliner" style="top:20px;"/></li>
									<?php
									}
									else
									{
									?>
									<li menu-position="<?=$SxOrDx?>" class="menu-<?=$SxOrDx?>-item" style="width:14.2%;"><a href="<?=$positionRow['url']?>&lang=<?=$lang?>"><?=$positionRow['valore']?></a><div class="underliner" style="top:20px;"/></li>
									<?php
									}
								}
								else
								{
									$subString = strstr($url, 'idPage'); 
									$trovato=false;
									if ($subString==("idPage=".$idPage))  $trovato = true;

									if ($trovato)
									{
									?>
									<li menu-position="<?=$SxOrDx?>" class="dropdown menu-<?=$SxOrDx?>-item" style="width:14.2%;" id="father_internet"><a href="<?=$positionRow['url']?>&lang=<?=$lang?>"><b><?=$positionRow['valore']?></b></a><div class="underliner" style="top:20px;"></div> 
									<?php
									}
									else
									{
									?>
									<li menu-position="<?=$SxOrDx?>" class="dropdown menu-<?=$SxOrDx?>-item" style="width:14.2%;" id="father_internet"><a href="<?=$positionRow['url']?>&lang=<?=$lang?>"><?=$positionRow['valore']?></a><div class="underliner" style="top:20px;"></div> 
									<?php
									}
									?>
										<ul id="submenu_<?=$positionRow['id']?>" class="dropdown-menu second-level-submenu-custom">
											<ul class="list-inline">
												<?php		
												while($subMenuRow =  mysql_fetch_assoc($subMenuItems))
												{
													?>
													<li><a href="<?=$subMenuRow['url']?>" id="link<?=$subMenuRow['id']?>" menu-level="secondo"><?=$subMenuRow['valore']?></a><div class="underliner" style="top:35px;"/></li>
													<?php
												}
												?>
											</ul>
										</ul>
									</li>
									<?php
								
								}
							
							}
							else
							{
								// quando Valore == 'LOGO_SEGNAPOSTO' carico l'immagine centrale
								?>
								<li id="navLogo" style="width:14.2%;"><a href="index.php?lang='<?=$lang?>'"><img id="imageLogo" src="images/logo_mediasoft_green.png"></a></li>
								<?php
							}
						}
						$dbInst->connClose();
					?>										


					
				</ul>

			  </div><!-- /.navbar-collapse -->
			</nav>

		</div>
		<hr style="margin-left:6%;margin-right:6%;color: #B1C903; background-color: #B1C903; height: 2px;margin-top: 0px;margin-bottom: 0px;">
	</div>
	

	<div id="mainContainerDiv" class="container fill" style="width:90%; margin: 0px; padding: 0px;float:left;position:absolute;top:100px;">
	<!--
	<div id="mainContainerDiv" class="container fill" style="width:80%; margin: 0px; padding: 0px;float:left;">
	-->


	</div>


	
	
	<div id="navbar-laterale" style="position:fixed;width:14%;margin-top: 10px;  right:-5px; top: 120px;">
		<ul id="lateralMenu" class="nav nav-pills nav-stacked" style="list-style-type: circle;">
		</ul>
	</div>

	<div id="social" style="position:fixed; bottom:0px; margin: 10px;  right:20px; height:20px; z-index:999999;">
		<a href="https://www.facebook.com/SeoPosizionamentoMotoriRicerca?ref=hl" target="_blank"><img src="images/social-fb.png" alt="FB" height="20px"></a>
		<a href="https://twitter.com/Mediasoftonline" target="_blank"><img src="images/social-tw.png" alt="TW" height="20px"></a>
		<a href="http://www.linkedin.com/company/1194297" target="_blank"><img src="images/social-lin.png" alt="LinkedIn" height="20px"></a>
	</div>



	<script src="jquery-1.10.2.min.js"></script>
	<script src="jquery.jscroll.min.js"></script>
	<script src="bootstrap/js/bootstrap.js"></script>
	<script src="functions.js"></script>
	
<!--	<script src="js/jquery.timelinr-0.9.54.js"></script> -->
	<script src="timeline/js/jquery.mCustomScrollbar.js" type="text/javascript"></script>
	<script src="timeline/js/jquery.easing.1.3.js" type="text/javascript"></script>
	<script src="timeline/js/jquery.timeline.js" type="text/javascript"></script>
	<script src="freewall-master/freewall.js"></script>
	<script src="ThumbnailGridExpandingPreview/js/modernizr.custom.js"></script>
	<script src="ThumbnailGridExpandingPreview/js/grid.js"></script>
	<script src="arbor-v0.92/lib/arbor.js"></script> 
	
<!--	<script src="http://d3js.org/d3.v3.min.js" charset="utf-8"></script> 
		
	<script type="text/javascript" src="http://mbostock.github.com/d3/d3.js?2.6.0"></script>
	<script type="text/javascript" src="http://mbostock.github.com/d3/d3.layout.js?2.6.0"></script>
	<script type="text/javascript" src="http://mbostock.github.com/d3/d3.geom.js?2.6.0"></script>
-->	
	

	<script type="text/javascript">

		var pages_config = new Array();

		pages_config[$.urlParam('idPage')] =
		{
			'idPage': $.urlParam('idPage'),
			'backColor': 'white',

			'renderer': function(row_data)
			{
				var idSezione = row_data['idSezione'];
				var type = row_data['type'];
				var url = row_data['url'];
				var titolo = row_data['titolo'];
				var testo = row_data["testo"];
				var media = row_data['subItems'];
				var allegati = row_data['subItemsAllegati'];
				
				//alert("type: " + type);
				//in base al tipo ho una composizione differente
				var html;
				//tipo 1: Titolo, Immagine e descrizione
				if (type=="1")
				{
					//mi aspetto un solo subitem, una immagine
					var img=media[0]['url'];
					html =	
					'<div id="section'+idSezione+'" class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-left" style="padding: 0px 10px 0px 10px; margin: 0px 0px 0px 0px;">' + 		
						'<div>' +
							'<div class="titleSection" style="margin-left:8%; margin-right:8%; text-align:center; color:#b1c903;">' + titolo + '</div>'	+				
							'<div style="margin: 0% 0% 0% 0%;padding-bottom: 5px;"><img src="images/' + img + '" alt="" style="width:100%;" ></div>' + 
							'<div style="margin: 0% 0% 0% 0%; font-size: 15px; overflow-y:auto; text-align: justify; padding-bottom: 20px;">' + testo + '</div>' +
						'</div>' +						
						'<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center" style="padding: 0px; margin: 0px;">' +
						'<hr style="color: #B1C903; background-color: #B1C903; height: 2px; width:100%">' +
						'</div>' +
					'</div>' 
				}
				//tipo 2: Immagine, Titolo e descrizione
				else if (type=="2")
				{
					
					//mi aspetto un solo subitem, una immagine (o anche no.. solo il titolo...)
					var img="";
					if (media.length>0) img=media[0]['url'];
					html =	
					'<div id="section'+idSezione+'" class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-left" style="padding: 0px 10px 0px 10px; margin: 0px 0px 0px 0px;">' + 		
						'<div>' 
							if (img!="") html += '<div style="margin: 0% 0% 0% 0%;padding-bottom: 5px;padding-top: 40px"><img src="images/' + img + '" alt="" style="width:100%;"></div>' 
							if (titolo!="")	html += '<div class="titleSection" style="margin-left:0%; margin-right:0%; text-align:center;  color:#b1c903;">' + titolo + '</div>'			
							html += '<div style="margin: 0% 0% 0% 0%; font-size: 15px; overflow-y:auto; text-align: justify; padding-bottom: 20px;font-weight: 500">' + testo + '</div>' +

							'<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center" style="padding: 0px; margin: 0px;">' +
							'<hr style="color: #B1C903; background-color: #B1C903; height: 2px; width:100%">' +
							'</div>' +
						'</div>' +						
					'</div>' 
				}
				//tipo 3: Titolo e lista item testuali cliccabili (in 2 colonne)... tipo Allegati e Prodotti correlati
				else if (type=="3")
				{
					//alert("media length: " + media.length);
					html = '<div id="section'+idSezione+'" class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-left" style="padding: 0px 10px 0px 10px; margin: 0px 0px 0px 0px;">' 		
						//devo dividere la sezione in 2 parti (piu' la parte centrale dove metto la linea verticale)
						html += '<table style="width:100%"><tr>';		
						for(sez=1; sez<=2; sez++)
						{ 
							//html += '<div style="width=100%;float:left;">'
							html += '<td style="width:50%">'
							//per ogni item nell'array dei subitem, vedo vedere il tipo e il link
							for(i=0; i<media.length; i++)
							{ 
								if (media[i]['sotto_sezione']==sez)
								{
									//divido tra titolo e testi linkabili
									if (media[i]['tipo']=="2") //Titolo
									{
										html += '<div class="titleSection" style="margin-left:8%; margin-right:8%; text-align:center;  color:#b1c903;">' + media[i]['titolo'] + '</div>'				
									}
									else if (media[i]['tipo']=="3") //Testo con Link
									{
										html += '<div class="allegati_correlati" style="margin-left:8%; margin-right:8%; text-align:center; margin-top:5px; margin-bottom:5px;"><a href="'+ media[i]['url_link'] +'">' + media[i]['titolo'] + '</a></div>'				
									}
								}
							}
							html += '</td>';
							//html += '</div>'
							if (sez!=2) //metto la riga di separazione verticale, ma solo se non è l'ultima sotto sezione
							{
								html +='<td style="width:2px;background-color:#B1C903"></td>'
							}
						}
						html += '</td></table>';
					html += '</div>' + 
					'<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center" style="padding: 0px; margin: 0px;">' +
					'<hr style="color: #B1C903; background-color: #B1C903; height: 2px; width:100%">' +
					'</div>'
				}
				//tipo 5: Titolo e n media (foto, titolo e testo), tipo Media Blog
				else if (type=="5")
				{
					//alert("media length: " + media.length);
					html = '<div id="section'+idSezione+'" class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-left" style="padding: 0px 10px 0px 10px; margin: 0px 0px 0px 0px;">' +	
					'<div>' +
						'<div class="titleSection" style="margin-left:8%; margin-right:8%; text-align:center; color:#b1c903;">' + titolo + '</div>'	+				
						'<div>'
							//quanto media abbiamo?
							if (media.length==3) // li metto in linea
							{
								for(type5Index=0; type5Index<media.length; type5Index++)
								{ 
									//alert(media[i]['url']);
									//alert(media[i]['titolo']);
									//alert(media[i]['descrizione']);
									var testoArticolo = media[type5Index]['descrizione'];
									//alert(testoArticolo);
									//alert(testoArticolo.length);
									html += '<div style="float: left; width:33%;padding: 5px 10px 5px 10px;">' + 
										'<div style="margin: 0% 0% 0% 0%;padding-bottom: 0px;"><img src="images/' + media[type5Index]['url'] + '" alt="" style="width:100%;border: solid 1px #b1c903;"></div>' + 
										'<div class="mediaBlogTitle" style="margin-left:0%; margin-right:0%; color:#b1c903;">' + media[type5Index]['titolo'] + '</div>'				
										//se è molto lungo, vedo di dividerlo...
										//se il testo è molto lungo, vedo di dividerlo...
										if (testoArticolo.length>750)
										{
											//prendo i primi 750 caratteri
											var arrTestoDiviso = $dividiTesto(testoArticolo,750);
											var testoArticoloParte1=arrTestoDiviso[0];
											var testoArticoloParte2=arrTestoDiviso[1];
											
											
											html += '<div style="margin: 0% 0% 0% 0%; font-size: 15px; overflow-y:auto; text-align: justify; padding-bottom: 20px;">' + testoArticoloParte1 + 
											'<div id="collapseSection'+idSezione+''+type5Index+'" class="collapse">' +
												'<div style="margin: 0% 0% 0% 0%; font-size: 15px; overflow-y:auto; text-align: justify; padding-bottom: 20px;">' + testoArticoloParte2 + '</div>' +
											'</div>' +
											'<p style="margin-top:10px;"><a id="readMoreSection'+idSezione+'" data-toggle="collapse" data-target="#collapseSection'+idSezione+''+type5Index+'" class="pulsanteReadMore">MORE</a></p></div>'
										}
										else
										{
											html += '<div style="margin: 0% 0% 0% 0%; font-size: 15px; overflow-y:auto; text-align: justify; padding-bottom: 20px;">' + media[type5Index]['descrizione'] + '</div>'
										}
									html += '</div>'
								}
							}
							else if (media.length==4) // 2 sopra e 2 sotto
							{
								html += '<div style="width:100%;">'
								for(type5Index=0; type5Index<2; type5Index++)
								{ 
									//alert(media[i]['url']);
									//alert(media[i]['titolo']);
									//alert(media[i]['descrizione']);
									html += '<div style="float: left; width:50%;padding: 5px 10px 5px 10px;">' + 
										'<div style="margin: 0% 0% 0% 0%;padding-bottom: 0px;"><img src="images/' + media[type5Index]['url'] + '" alt="" style="width:100%;border: solid 1px #b1c903;"></div>' + 
										'<div class="mediaBlogTitle" style="margin-left:0%; margin-right:0%; color:#b1c903;">' + media[type5Index]['titolo'] + '</div>'	+				
										'<div style="margin: 0% 0% 0% 0%; font-size: 15px; overflow-y:auto; text-align: justify; padding-bottom: 20px;">' + media[type5Index]['descrizione'] + '</div>' +
									'</div>'
								}
								html += '</div><br clear="all"/><div style="width:100%;">'
								for(type5Index=2; type5Index<4; type5Index++)
								{ 
									//alert(media[i]['url']);
									//alert(media[i]['titolo']);
									//alert(media[i]['descrizione']);
									html += '<div style="float: left; width:50%;padding: 15px 15px 15px 15px;">' + 
										'<div style="margin: 0% 0% 0% 0%;padding-bottom: 0px;"><img src="images/' + media[type5Index]['url'] + '" alt="" style="width:100%;border: solid 1px #b1c903;"></div>' + 
										'<div class="mediaBlogTitle" style="margin-left:0%; margin-right:0%; color:#b1c903;">' + media[type5Index]['titolo'] + '</div>'	+				
										'<div style="margin: 0% 0% 0% 0%; font-size: 15px; overflow-y:auto; text-align: justify; padding-bottom: 20px;">' + media[type5Index]['descrizione'] + '</div>' +
									'</div>'
								}
								html += '</div><br clear="all"/>'
							}
						html += '</div>' + 
						'<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center" style="padding: 0px; margin: 0px;">' +
						'<hr style="color: #B1C903; background-color: #B1C903; height: 2px; width:100%">' +
						'</div>' +
					'</div>'
				}
				//tipo 6: Timeline
				else if (type=="6")
				{
					var language=$.urlParam('lang');
					//alert(language);

					//Per adesso metto tutto hard coded
					var dataIdTimeline = new Array();
					var titoloTimeline = new Array();
					var descrizioneTimeline = new Array();
					var immagineTimeline = new Array();
					var link = new Array();
					
					dataIdTimeline[1]="1/2007";
					titoloTimeline[1]="Nasce Mediasoft";
					descrizioneTimeline[1]="L\'azienda, operante nel settore dell\'informatica e dell\'Information and Communications Technology, nasce (nel 2007) dalle idee e dalle competenze professionali dell\'ingegnere  Stefano Santo Sabato, divenendo una tra le piu\' importanti del settore.";
					immagineTimeline[1]="timeline/01.jpg";
					link[1]="";
					
					dataIdTimeline[2]="2/2007";
					titoloTimeline[2]="One Click Support";
					descrizioneTimeline[2]="MediaSoft parte con la creazione di One Click Support, un sistema innovativo che si configura come una piattaforma per la gestione dei servizi di customer care in rete, attraverso canali testuali, audio e videoconferenza, a beneficio di imprese dislocate in tutto il mondo.";
					immagineTimeline[2]="timeline/02.jpg";
					link[2]="9";

					dataIdTimeline[3]="1/2008";
					titoloTimeline[3]="One Click House e Ubiwork";
					descrizioneTimeline[3]="L\'azienda lancia sul mercato due nuovi prodotti rivolti rispettivamente alle agenzie immobiliari ed alle agenzie assicurative: One Click House e Ubiwork. Il primo progetto rappresenta un sistema di gestione delle attività più comuni all\'interno di un\'agenzia immobiliare. Il secondo, un sistema ibrido desktop e mobile, che consente il monitoraggio, la gestione e la documentazione multimediale dei lavori di manutenzione e controllo effettuati da squadre di intervento.";
					immagineTimeline[3]="timeline/03.jpg";
					link[3]="19";

					dataIdTimeline[4]="2/2008";
					titoloTimeline[4]="MyTeam";
					descrizioneTimeline[4]="Nel 2008 nasce MyTeam, prodotto pensato da MediaSoft per rivoluzionare, per mezzo di strumenti tecnologici, l\'organizzazione e la gestione dei piccoli, medi e grandi uffici. Il prodotto si sviluppa successivamente in due versioni: una per la pubblica amministrazione, MyTeam PA, e l\'altra per gli studi legali, MyTeam Lex.";
					immagineTimeline[4]="timeline/04.jpg";
					link[4]="21";

					dataIdTimeline[5]="3/2008";
					titoloTimeline[5]="Cercaturismo.it";
					descrizioneTimeline[5]="MediaSoft crea Cercaturismo.it, un network turistico focalizzato sia sull\'offerta di servizi alle imprese operanti nel settore (dalle strutture ricettive ai tour operator), che sull\'informazione dei turisti interessati a scoprire le meraviglie dell\'Italia.";
					immagineTimeline[5]="timeline/05.jpg";
					link[5]="22";

					dataIdTimeline[6]="1/2009";
					titoloTimeline[6]="Cercaturismo.it premiato al “Buy Tourism Online”";
					descrizioneTimeline[6]="Cercaturismo.it viene invitato presso la fiera turistica nazionale \"Buy Tourism Online\" di Firenze per essere premiato tra i 10 portali turistici online maggiormente rappresentativi dello scenario turistico legato al Web.";
					immagineTimeline[6]="timeline/06.jpg";
					link[6]="http://www.buytourismonline.com";

					dataIdTimeline[7]="2/2009";
					titoloTimeline[7]="Apertura della nuova sede romana";
					descrizioneTimeline[7]="Nel 2009 viene aperta una nuova sede operativa a Roma, nella prestigiosa cornice di Piazza del Popolo, in modo da favorire i contatti e gli scambi con enti ed aziende di portata nazionale.";
					immagineTimeline[7]="timeline/07.jpg";
					link[7]="";

					dataIdTimeline[8]="1/2010";
					titoloTimeline[8]="Accordo tra Cercaturismo.it e Google AdWords";
					descrizioneTimeline[8]="Nel 2010 Cercaturismo.it sigla un accordo di co-branding con Google per la campagna AdWords. Un regalo questo che permette ai clienti del portale di avere diritto ad un credito omaggio di ben 50 euro per pubblicizzare la propria attività su Google AdWords.";
					immagineTimeline[8]="timeline/08.jpg";
					link[8]="http://www.google.it/adwords";

					dataIdTimeline[9]="2/2010";
					titoloTimeline[9]="MediaSoft vince il Premio Innovazione ICT 2010 SMAU";
					descrizioneTimeline[9]="Importante traguardo raggiunto dalla MediaSoft che si aggiudica assieme al suo cliente Alba Service Spa il primo premio dello SMAU ICT di Bari per le \"applicazioni Mobile&Wireless\" grazie ad un prodotto di nuovissima concezione: Ubiwork, una sintesi tra software e hardware in grado di gestire e monitorare squadre di lavoro soggette a spostamenti di qualunque entità.";
					immagineTimeline[9]="timeline/09.jpg";
					link[9]="http://www.smau.it";

					dataIdTimeline[10]="1/2011";
					titoloTimeline[10]="Bedandbreakfast-online.it";
					descrizioneTimeline[10]="Nasce Bedandbreakfast-online.it, il nuovo portale turistico di MediaSoft, dedicato a chi desidera organizzare una vacanza in base ai propri gusti. Un tramite tra i turisti e le migliori strutture ricettive presenti in Italia, che diventa presto una vetrina turistica tra le più note.";
					immagineTimeline[10]="timeline/10.jpg";
					link[10]="20";

					dataIdTimeline[11]="2/2011";
					titoloTimeline[11]="Studio “GISC”";
					descrizioneTimeline[11]="Prende il via il progetto sullo studio GISC \"Gestione Integrata dello Scompenso Cardiaco\", un sistema sviluppato da MediaSoft per l\'analisi e l\'elaborazione di statistiche sui dati relativi ai pazienti rilevati da medici di medicina generale, cardiologi specialisti e  ospedali.";
					immagineTimeline[11]="timeline/14.jpg";
					link[11]="";

					dataIdTimeline[12]="1/2012";
					titoloTimeline[12]="MyHotel";
					descrizioneTimeline[12]="MediaSoft lancia un nuovo importante prodotto: MyHotel. MyHotel è un innovativo Software Gestionale pensato per gestire e soddisfare le esigenze di tipologie di strutture ricettive eterogenee, assicurandone un controllo globale semplificato ed una riduzione dei tempi di gestione";
					immagineTimeline[12]="timeline/11.jpg";
					link[12]="1";

					dataIdTimeline[13]="2/2012";
					titoloTimeline[13]="Manutengo";
					descrizioneTimeline[13]="Un nuovo prodotto da Mediasoft: Manutengo. Manutengo è una piattaforma web per la gestione delle manutenzioni di impianti di diversa natura (ascensori, caldaie, ecc.), che permette l\'amministrazione di manutenzioni programmate ordinarie e straordinarie e quella dei tecnici e delle richieste di intervento che pervengono dai clienti.";
					immagineTimeline[13]="timeline/11_bis.jpg";
					link[13]="";

					dataIdTimeline[14]="3/2012";
					titoloTimeline[14]="Certificazione di “Internet Marketing” presso la Web Ceo University";
					descrizioneTimeline[14]="MediaSoft raggiunge un altro importante traguardo nel campo del Marketing in internet e del SEO (Search Engine Optimization). L\'azienda acquisisce con successo la Certificazione di \"Internet Marketing\" presso la Web Ceo University ottenendo, così, le competenze relative al Search Engine Marketing, Social Media Marketing, Web Analytics, Email Marketing e Servizi di Marketing di Affiliazione.";
					immagineTimeline[14]="timeline/12.jpg";
					link[14]="http://www.seo-training-course.com";

					dataIdTimeline[15]="4/2012";
					titoloTimeline[15]="Concerto di “Salvatore Russo Gypsy Jazz Project”";
					descrizioneTimeline[15]="Mediasoft dà il via alla serie di eventi sul tema \"Musica e Innovazione Tecnologica\". Il primo appuntamento è con il concerto del trio \"Salvatore Russo Gypsy Jazz Project\" nella stupenda cornice del Castello de Monti di Corigliano D\'Otranto. Sul palco tre artisti, tre smisurati talenti: i chitarristi Salvatore Russo, Tony Miolla e Giuseppe Venezia al contrabbasso.";
					immagineTimeline[15]="timeline/13.jpg";
					link[15]="http://www.salvatorerusso.com";


					dataIdTimeline[16]="1/2013";
					titoloTimeline[16]="Opera teatrale “La Traviata”";
					descrizioneTimeline[16]="Il secondo appuntamento sul tema “Musica e Innovazione Tecnologica” è con l\'opera teatrale “La Traviata”, spettacolo organizzato da MediaSoft in collaborazione con l\'Orchestra Giovanile Pergolesi, presso il Teatro comunale di Novoli, al quale partecipano 12 orchestrali e 36 attori professionisti.";
					immagineTimeline[16]="timeline/15.jpg";
					link[16]="";

					dataIdTimeline[17]="2/2013";
					titoloTimeline[17]="Nuovo Logo Mediasoft";
					descrizioneTimeline[17]="Grazie alla collaborazione con l’agenzia di comunicazione Rebel prende vita il nuovo logo di Mediasoft, che manifesta lo spirito creativo dell’azienda e ne definisce il carattere innovativo e ancora più attrattivo, del quale desidera essere portavoce.";
					immagineTimeline[17]="timeline/15_bis.jpg";
					link[17]="";
					
					dataIdTimeline[18]="3/2013";
					titoloTimeline[18]="Mediasoft e il progetto KISS-Health";
					descrizioneTimeline[18]="Nasce il partenariato tra Mediasoft e il progetto KISS-Health (Knowledge Intensive Social Services for Health) che promuove un modello integrato di ricerca, formazione e servizi, realizzato per il Laboratorio di Biomeccanica Posturale di Mesagne, con lo scopo di dare una risposta alle problematiche generate da alterazioni nella postura del corpo.";
					immagineTimeline[18]="timeline/16.jpg";
					link[18]="http://www.kisshealth.it";

					dataIdTimeline[19]="1/2014";
					titoloTimeline[19]="Mediasoft e Kiss Health partecipano allo SMAU 2014";
					descrizioneTimeline[19]="Mediasoft partecipa allo SMAU come partner tecnologico del progetto Kiss Health promosso dal laboratorio di Biomedica Posturale di Mesagne. L’evento dedicato all\'innovazione e giunto ormai alla sesta edizione, si tiene a Bari presso il Nuovo Padiglione della Fiera del Levante.";
					immagineTimeline[19]="timeline/17.jpg";
					link[19]="http://www.kisshealth.it";

					dataIdTimeline[20]="2/2014";
					titoloTimeline[20]="Mediasoft diventa partner tecnologico di Mubo project";
					descrizioneTimeline[20]="Mediasoft diventa partner tecnologico di Mubo project, casa editrice che si occupa di editoria digitale per ragazzi. I due progetti frutto di questa collaborazione sono: un libro digitale basato sul romanzo di “20mila Leghe Sotto i Mari” e Santa Croce Kids, un’applicazione mobile per la visita della Basilica di Santa Croce di Firenze.";
					immagineTimeline[20]="timeline/18.jpg";
					link[20]="http://www.muboproject.it";

					dataIdTimeline[21]="3/2014";
					titoloTimeline[21]="Fiera del Libro di Bologna 2014";
					descrizioneTimeline[21]="Alla Fiera del Libro di Bologna 2014 viene presentata la demo del libro interattivo digitale basato sul romanzo di Jules Verne “20mila Leghe Sotto i Mari”, progetto sviluppato da Mediasoft per Mubo Project.";
					immagineTimeline[21]="timeline/19.jpg";
					link[21]="http://www.bolognachildrensbookfair.com";

					dataIdTimeline[22]="4/2014";
					titoloTimeline[22]="Il nuovo Sito Web di Mediasoft è online";
					descrizioneTimeline[22]="Il nuovo sito Mediasoft segna un nuovo inizio per l’azienda,  sempre più attenta alle esigenze dei clienti;  pronta ad instaurare con gli stessi rapporti di fiducia e collaborazione; capace di intrigare gli utenti con nuove dinamiche aziendali.";
					immagineTimeline[22]="timeline/20.jpg";
					link[22]="";

					html =	
					'<div id="section'+idSezione+'" class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-left" style="padding: 0px 10px 0px 10px; margin: 0px 0px 0px 0px;">' + 		
						'<div>' +
							'<div class="titleSection" style="margin-left:0%; margin-right:0%; text-align:center; color:#b1c903;">' + titolo + '</div>'	+				

							'<div class="timelineLoader">' +
								'<img src="timeline/images/timeline/loadingAnimation.gif" />' +
							'</div>' +					 
							
							'<div class="timelineFlat tl1">'
								for(timelineIndex=1; timelineIndex<dataIdTimeline.length; timelineIndex++)
								{ 
									html +='<div class="item" data-id="'+dataIdTimeline[timelineIndex]+'" data-description="'+titoloTimeline[timelineIndex]+'">'
									if (link[timelineIndex]!="")
									{
										//è un link verso la pagina prodotti o verso altre pagine?
										if (link[timelineIndex].length<=4)
										{
											html +='<h2><a style="color:#ffffff" href="fixed.content.php?idPage=1&lang='+language+'&prod='+link[timelineIndex]+'">'+titoloTimeline[timelineIndex]+'</a></h2>'
										}
										else
										{
											html +='<h2><a target="_blank" style="color:#ffffff" href="'+link[timelineIndex]+'">'+titoloTimeline[timelineIndex]+'</a></h2>'
										}
									}
									else
									{
										html +='<h2>'+titoloTimeline[timelineIndex]+'</h2>'
									}
										
									html += '<img src="images/'+immagineTimeline[timelineIndex]+'" alt="" style="max-width:100%;border: solid 1px #b1c903;"/>' +
									'<span>'+descrizioneTimeline[timelineIndex]+'</span>' +
									'</div>'
								}

							html += '</div>' +

						
						'</div>' +						
						'<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center" style="padding: 15px 0px 0px 0px; margin: 0px;">' +
						'<hr style="color: #B1C903; background-color: #B1C903; height: 2px; width:100%">' +
						'</div>' +
						
					'</div>' 
				}
				//tipo 7: Titolo, Testo e griglia di item (immagini, immagini con caption, ecc
				else if (type=="7")
				{
					//verranno disposti 8 elementi per riga.. vediamo quante righe...
					var righe=3;
					var element_width = ($('#mainContainerDiv').width()*0.80)/8;
					var altezza_sezione = (element_width*righe*0.82)+90;
					//element_width=140;
					//alert(altezza_sezione);
					html =	
					'<div id="section'+idSezione+'" class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-left" style="padding: 0px 10px 0px 10px; margin: 0px 0px 0px 0px">' + 		
						'<div id="test">' +
							'<div id="titolo" class="titleSection" style="margin-left:0%; margin-right:0%; text-align:center; color:#b1c903;">' + titolo + '</div>'	+				
							'<div id="testo" style="margin: 0% 0% 0% 0%; font-size: 15px; overflow-y:auto; text-align: justify; padding-bottom: 20px;">' + testo + '</div>' +

							'<div id="wall_container">' +
								'<div class="wall_item" style="background: #b1c903; width:'+element_width+'px; height:'+element_width+'px;"><img src="images/clienti/salus.png" alt="" style="max-width:100%"></div>' +
								'<div class="wall_item" style="background: #b1c903; width:'+element_width+'px; height:'+element_width+'px;"><img src="images/clienti/SSI.png" alt="" style="max-width:100%"></div>' +
								'<div class="wall_item" style="background: #b1c903; width:'+element_width+'px; height:'+element_width+'px;"><img src="images/clienti/unionKey.png" alt="" style="max-width:100%"></div>' +
								'<div class="wall_item" style="background: #b1c903; width:'+element_width+'px; height:'+element_width+'px;"><img src="images/clienti/memar.png" alt="" style="max-width:100%"></div>' +
								'<div class="wall_item" style="background: #b1c903; width:'+element_width+'px; height:'+element_width+'px;"><img src="images/clienti/albaService.png" alt="" style="max-width:100%"></div>' +
								'<div class="wall_item" style="background: #b1c903; width:'+element_width+'px; height:'+element_width+'px;"><img src="images/clienti/meltinPot.png" alt="" style="max-width:100%"></div>' +
								'<div class="wall_item" style="background: #b1c903; width:'+element_width+'px; height:'+element_width+'px;"><img src="images/clienti/chiriattiTour.png" alt="" style="max-width:100%"></div>' +
								'<div class="wall_item" style="background: #b1c903; width:'+element_width+'px; height:'+element_width+'px;"><img src="images/clienti/claimExpert.png" alt="" style="max-width:100%"></div>' +
								'<div class="wall_item" style="background: #b1c903; width:'+element_width+'px; height:'+element_width+'px;"><img src="images/clienti/comuneDiLecce.png" alt="" style="max-width:100%"></div>' +
								'<div class="wall_item" style="background: #b1c903; width:'+element_width+'px; height:'+element_width+'px;"><img src="images/clienti/genovaMusei.png" alt="" style="max-width:100%"></div>' +
								'<div class="wall_item" style="background: #b1c903; width:'+element_width+'px; height:'+element_width+'px;"><img src="images/clienti/giroSalento.png" alt="" style="max-width:100%"></div>' +
								'<div class="wall_item" style="background: #b1c903; width:'+element_width+'px; height:'+element_width+'px;"><img src="images/clienti/hotelBelvedere.png" alt="" style="max-width:100%"></div>' +
								'<div class="wall_item" style="background: #b1c903; width:'+element_width+'px; height:'+element_width+'px;"><img src="images/clienti/multiserviceEco.png" alt="" style="max-width:100%"></div>' +
								'<div class="wall_item" style="background: #b1c903; width:'+element_width+'px; height:'+element_width+'px;"><img src="images/clienti/pallavoloGalatina.png" alt="" style="max-width:100%"></div>' +
								'<div class="wall_item" style="background: #b1c903; width:'+element_width+'px; height:'+element_width+'px;"><img src="images/clienti/provinciaDiLecce.png" alt="" style="max-width:100%"></div>' +
								'<div class="wall_item" style="background: #b1c903; width:'+element_width+'px; height:'+element_width+'px;"><img src="images/clienti/tecnomed.png" alt="" style="max-width:100%"></div>' +
								'<div class="wall_item" style="background: #b1c903; width:'+element_width+'px; height:'+element_width+'px;"><img src="images/clienti/parcoNazionaleDelCirceo.png" alt="" style="max-width:100%"></div>' +
								'<div class="wall_item" style="background: #b1c903; width:'+element_width+'px; height:'+element_width+'px;"><img src="images/clienti/teatroSanCarlo.png" alt="" style="max-width:100%"></div>' +
								'<div class="wall_item" style="background: #b1c903; width:'+element_width+'px; height:'+element_width+'px;"><img src="images/clienti/teatroAllaScala.png" alt="" style="max-width:100%"></div>' +
								'<div class="wall_item" style="background: #b1c903; width:'+element_width+'px; height:'+element_width+'px;"><img src="images/clienti/parcoNazionaleDelle5Terre.png" alt="" style="max-width:100%"></div>' +
								'<div class="wall_item" style="background: #b1c903; width:'+element_width+'px; height:'+element_width+'px;"><img src="images/clienti/parcoNazionaleAbruzzo.png" alt="" style="max-width:100%"></div>' +
								'<div class="wall_item" style="background: #b1c903; width:'+element_width+'px; height:'+element_width+'px;"><img src="images/clienti/piccoloMilano.png" alt="" style="max-width:100%"></div>' +
								'<div class="wall_item" style="background: #b1c903; width:'+element_width+'px; height:'+element_width+'px;"><img src="images/clienti/museoNicolis.png" alt="" style="max-width:100%"></div>' +
								'<div class="wall_item" style="background: #b1c903; width:'+element_width+'px; height:'+element_width+'px;"><img src="images/clienti/mart.png" alt="" style="max-width:100%"></div>' +
								'<div class="wall_item" style="background: #b1c903; width:'+element_width+'px; height:'+element_width+'px;"><img src="images/clienti/parcoNazionaleAspromonte.png" alt="" style="max-width:100%"></div>' +
								'<div class="wall_item" style="background: #b1c903; width:'+element_width+'px; height:'+element_width+'px;"><img src="images/clienti/motorShow.png" alt="" style="max-width:100%"></div>' +
								'<div class="wall_item" style="background: #b1c903; width:'+element_width+'px; height:'+element_width+'px;"><img src="images/clienti/teatroLaFenice.png" alt="" style="max-width:100%"></div>' +
								'<div class="wall_item" style="background: #b1c903; width:'+element_width+'px; height:'+element_width+'px;"><img src="images/clienti/regionePuglia.png" alt="" style="max-width:100%"></div>' +
								'<div class="wall_item" style="background: #b1c903; width:'+element_width+'px; height:'+element_width+'px;"><img src="images/clienti/teatroFestivalItalia.png" alt="" style="max-width:100%"></div>' +
								'<div class="wall_item" style="background: #b1c903; width:'+element_width+'px; height:'+element_width+'px;"><img src="images/clienti/parcoNazionaleMajella.png" alt="" style="max-width:100%"></div>' +
								'<div class="wall_item" style="background: #b1c903; width:'+element_width+'px; height:'+element_width+'px;"><img src="images/clienti/gtsVacanze.png" alt="" style="max-width:100%"></div>' +
								'<div class="wall_item" style="background: #b1c903; width:'+element_width+'px; height:'+element_width+'px;"><img src="images/clienti/mubo.png" alt="" style="max-width:100%"></div>' +
								'<div class="wall_item" style="background: #b1c903; width:'+element_width+'px; height:'+element_width+'px;"><img src="images/clienti/ciesseti.png" alt="" style="max-width:100%"></div>' +
							'</div>' +
							'<div style="float: left; width:33%;padding: 5px 10px 5px 10px;font-size: 16px;line-height: 20px;">' + 
								'Cartotecnica Vitellio<br>Villa Conca Marco<br>Residence Le Palme<br>Tenuta Flora Maria<br>Residenza Gemma<br>Agenzia Viaggi Sasinae<br>Hotel Aloisi<br>I Teatri -Reggio Emilia<br>Area Marina Protetta - Porto Cesareo<br>Parco dei Nebrodi<br>Area Marina Protetta - Capo Rizzuto<br>Parco Nazionale del Pollino<br>Parco delle Orobie<br>Parco Oglio Sud<br>Parco Naturale Veglia<br>Fiera Bolzano<br>Area Marina Protetta Portofino<br>Parco Regionale Abbazia di Monte Veglio<br>Parco La Mandria<br>Parco Regionale dei Colli di Bergamo'+
							'</div>' +
							'<div style="float: left; width:33%;padding: 5px 10px 5px 10px;font-size: 16px;line-height: 20px;">' + 
								'Area Marina Protetta Punta Capannella<br>ScopriMiniera<br>www.informazionecampania.it<br>Il Piccolo d&apos;Abruzzo<br>Gran Fondo della Pace<br>Fiera del Cioccolato Artigianale<br>Bike Expo<br>Blu Nautilus<br>AmaTour World Cup 2008<br>KunStart08<br>Tutti Sapori<br>Parco Regionale di Montemarcello-Magra<br>Riserva Naturale dello Zingaro<br>Parco Regionale delle Orobie Valtellinesi<br>Parco Nazionale dell&apos;Alta Murgia<br>Parco Regionale dei Nebrodi<br>Area Marina Protetta Capo Rizzuto<br>Case ad Oriente<br>Area Marina Protetta di Porto Cesareo'+
							'</div>' +
							'<div style="float: left; width:33%;padding: 5px 10px 5px 10px;font-size: 16px;line-height: 20px;">' + 
								'Parco Fluviale dell&apos;Alcantara<br>Area Marina Protetta di Porto Cesareo<br>Parco Nazionale Appennino Tosco-Emiliano<br>Parco del Po Cuneese<br>Parco Regionale del Partenio<br>Area Marina Protetta Penisola del Sinis - Isola di Mal di Ventre<br>Parco della Valle del Lambro<br>Parco Regionale Migliarino San Rossore Massaciuccoli<br>Parco Fluviale Oglio Sud<br>Parco Regionale La Mandria<br>Parco Regionale dell&apos;Abbazia di Monteveglio<br>Parco Naturale dell&apos;Alta Valsesia<br>Parco Regionale di Veglia - Devero<br>Parco Regionale Sasso Simone e Simoncello<br>Parco Fluviale Regionale del Taro<br>'+
							'</div>' +
							
						'</div>' +						
					'</div>' +
					'<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center" style="padding: 0px; margin: 0px;">' +
					'<hr style="color: #B1C903; background-color: #B1C903; height: 2px; width:100%">' +
					'</div>'
					
				}
				//tipo 8: Titolo, Immagine + 3 thumb, Testo di fianco
				else if (type=="8")
				{
					//alert("media length: " + media.length);
					//mi aspetto 4 immagini, la prima la metto nel box centrale e le altre in quelli laterali
					var img1=media[0]['url'];
					var img2=media[1]['url'];
					var img3=media[2]['url'];
					var img4=media[3]['url'];
					
					html =	
					'<div id="section'+idSezione+'" class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-left" style="padding: 0px 10px 0px 10px; margin: 0px 0px 0px 0px;">' + 		
						'<div id="test">' +
							'<div id="titolo" class="titleSection" style="margin-left:0%; margin-right:0%; text-align:center;  color:#b1c903;">' + titolo + '</div>'	+				
							'<div id="testo" style="display:block;  margin: 0% 0% 0% 0%; font-size: 15px; overflow-y:auto; text-align: justify; padding-bottom: 20px;">' + 
								'<div style="margin: 0% 0% 0% 0%;padding-bottom: 5px;width:45%;float: left;">' + 
									'<div style="float: left; width:76%;padding: 10px 0px 10px 10px;"><img id="myGridImg_section'+idSezione+'" src="images/'+ img1 +'" alt="" style="max-width:100%;border: solid 2px #b1c903"></div>' +
									'<div style="float: left; width:24%;display:block;">' + 
										'<div style="padding: 10px 10px 10px 10px;"><img id="myGridThumb1_section'+idSezione+'" src="images/'+ img2 +'" alt="" style="max-width:100%;border: solid 2px #b1c903;"></div>' +
										'<div style="padding: 5px 10px 5px 10px;"><img id="myGridThumb2_section'+idSezione+'" src="images/'+ img3 +'" alt="" style="max-width:100%;border: solid 2px #b1c903;"></div>' +
										'<div style="padding: 10px 10px 10px 10px;"><img id="myGridThumb3_section'+idSezione+'" src="images/'+ img4 +'" alt="" style="max-width:100%;border: solid 2px #b1c903;"></div>' +
									'</div>' +
								'</div>'
							
								//se il testo è molto lungo, vedo di dividerlo...
								if (testo.length>400)
								{
									var arrTestoDiviso = $dividiTesto(testo,400);
									//prendo i primi 100 caratteri
									var testoArticoloParte1=arrTestoDiviso[0];
									var testoArticoloParte2=arrTestoDiviso[1];
									html += testoArticoloParte1 + 
									'<div id="collapseSection'+idSezione+'" class="collapse">' +
										'<div style="display:block;  margin: 0% 0% 0% 0%; font-size: 15px; text-align: justify; padding-bottom: 20px;">' + testoArticoloParte2 + '</div>' +
									'</div>'
									//c'e' un url associato a questa sezione?
									if (url=="" || url==null)
									{
										html +='<p style="margin-top:10px;"><a id="readMoreSection'+idSezione+'" data-toggle="collapse" data-target="#collapseSection'+idSezione+'" class="pulsanteReadMore">MORE</a>'
										'</p></div>'
									}
									else
									{
										html +='<p style="margin-top:10px;"><a id="readMoreSection'+idSezione+'" data-toggle="collapse" data-target="#collapseSection'+idSezione+'" class="pulsanteReadMore">MORE</a><a class="pulsanteReadMore" href="'+url+'" style="margin-left:10px;" target="_blank">WEBSITE</a>'
										'</p></div>'
									}
								}
								else
								{
									html += testo
								}
								
								//testo +
						
							
							html += '</div>'
							
							
							
							//eventuali allegati
							if (allegati.length>0)
							{
								// ci sono allegati... metto inizialmente una riga di separazione
								html +='<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center" style="padding: 0px; margin: 0px;">' +
								'<hr style="color: #B1C903; background-color: #B1C903; height: 1px; width:90%;margin-left:5%">' +
								'</div>'
								
								var allegatoTipo1 = new Array();
								var allegatoTipo2 = new Array();
								var allegatoTipo3 = new Array();
								//alert("trovati allegati: " + allegati.length);
								//li devo dividere per tipo
								for(allegatiIndex=0; allegatiIndex<allegati.length; allegatiIndex++)
								{ 
									if (allegati[allegatiIndex]['tipo']==1) //allegato
									{
										allegatoTipo1.push(allegati[allegatiIndex]);
									}
									else if (allegati[allegatiIndex]['tipo']==2) //collegamento
									{
										allegatoTipo2.push(allegati[allegatiIndex]);
									}
									else if (allegati[allegatiIndex]['tipo']==3) //rassegna stampa
									{
										allegatoTipo3.push(allegati[allegatiIndex]);
									}
								}								
								//alert("Allegato tipo 1: " + allegatoTipo1.length);
								//alert("Allegato tipo 2: " + allegatoTipo2.length);
								//alert("Allegato tipo 3: " + allegatoTipo3.length);
								var numSezioni=0;
								if (allegatoTipo1.length>0) numSezioni++;
								if (allegatoTipo2.length>0) numSezioni++;
								if (allegatoTipo3.length>0) numSezioni++;
								var larghezzaSez = 100/numSezioni;
								
								var icona="";
								
								html += '<div style="display: table;padding: 5px 10px 5px 10px;width:100%"><div style="display: table-row;padding: 5px 10px 5px 10px;">'

								if (allegatoTipo1.length>0)
								{
									html += '<div style="display: table-cell;width:'+larghezzaSez+'%;">' + 
										'<div class="titleAllegati" style="margin-left:8%; margin-right:8%; text-align:center;  color:#b1c903;">ALLEGATI</div>'
									for(allegatiIndex=0; allegatiIndex<allegatoTipo1.length; allegatiIndex++)
									{
										//icona
										icona = $.findIconByType(allegatoTipo1[allegatiIndex]['icona']);
										var link="";
										if (allegatoTipo1[allegatiIndex]['isUrl']==0) link="allegati/" + allegatoTipo1[allegatiIndex]['url'];
										else link=allegatoTipo1[allegatiIndex]['url'];
										html += '<div class="itemAllegati" style="margin-left:8%; margin-right:8%; text-align:left;  color:#58585a;"><a target="_blank" href="'+ link +'"><img src="images/'+icona+'" height="20px" style="margin-right:10px;"\>'+allegatoTipo1[allegatiIndex]['testo']+'</a></div>'
									}
									html += '</div>'								
								}
								if (allegatoTipo2.length>0)
								{
									//ci sono allegati di tipo 1(per mettere una riga verticale)
									if (allegatoTipo1.length>0) //metto un'altra cella con una riga verticale
									{
										html += '<div style="display: table-cell;width:2px;background-color:#B1C903"></div>'
									}
									html += '<div style="display: table-cell;width:'+larghezzaSez+'%;">' + 
										'<div class="titleAllegati" style="margin-left:8%; margin-right:8%; text-align:center;  color:#b1c903;">COLLEGAMENTI</div>'
									for(allegatiIndex=0; allegatiIndex<allegatoTipo2.length; allegatiIndex++)
									{
										//icona
										icona = $.findIconByType(allegatoTipo2[allegatiIndex]['icona']);
										var link="";
										if (allegatoTipo2[allegatiIndex]['isUrl']==0) link="allegati/" + allegatoTipo2[allegatiIndex]['url'];
										else link=allegatoTipo2[allegatiIndex]['url'];
										html += '<div class="itemAllegati" style="margin-left:8%; margin-right:8%; text-align:left;  color:#58585a;"><a target="_blank" href="'+ link +'"><img src="images/'+icona+'" height="20px" style="margin-right:10px;"\>'+allegatoTipo2[allegatiIndex]['testo']+'</a></div>'
									}									
									html += '</div>'								
								}
								if (allegatoTipo3.length>0)
								{
									//ci sono allegati di tipo 1 o 2(per mettere una riga verticale)
									if ((allegatoTipo1.length>0) || (allegatoTipo2.length>0))
									{
										html += '<div style="display: table-cell;width:1px;background-color:#B1C903"></div>'
									}
									html += '<div style="display: table-cell;width:'+larghezzaSez+'%;">' + 
										'<div class="titleAllegati" style="margin-left:8%; margin-right:8%; text-align:center;  color:#b1c903;">RASSEGNA STAMPA</div>'
									for(allegatiIndex=0; allegatiIndex<allegatoTipo3.length; allegatiIndex++)
									{
										//icona
										icona = $.findIconByType(allegatoTipo3[allegatiIndex]['icona']);
										var link="";
										if (allegatoTipo3[allegatiIndex]['isUrl']==0) link="allegati/" + allegatoTipo3[allegatiIndex]['url'];
										else link=allegatoTipo3[allegatiIndex]['url'];
										html += '<div class="itemAllegati" style="margin-left:8%; margin-right:8%; text-align:left;  color:#58585a;"><a target="_blank" href="'+ link +'"><img src="images/'+icona+'" height="20px" style="margin-right:10px;"\>'+allegatoTipo3[allegatiIndex]['testo']+'</a></div>'
									}									
									html += '</div>'								
								}
							}
						
						html += '</div></div>' +						
						'<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center" style="padding: 0px; margin: 0px;">' +
						'<hr style="color: #B1C903; background-color: #B1C903; height: 2px; width:100%">' +
						'</div>' +
					'</div>' 
				}
				//tipo 10: Titolo, Immagine + 3 thumb, Testo sotto
				else if (type=="10")
				{
					//alert("media length: " + media.length);
					//mi aspetto 4 immagini, la prima la metto nel box centrale e le altre in quelli laterali
					var img1=media[0]['url'];
					var img2=media[1]['url'];
					var img3=media[2]['url'];
					var img4=media[3]['url'];
					
					html =	
					'<div id="section'+idSezione+'" class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-left" style="padding: 10px 10px 0px 10px; margin: 0px 0px 0px 0px;">' + 		
						'<div id="test">' +
							'<div id="titolo" class="titleSection" style="margin-left:0%; margin-right:0%; text-align:center;  color:#b1c903;">' + titolo + '</div>'	+				
							'<div style="margin: 0% 0% 0% 0%;padding-bottom: 20px;">' + 
								'<div style="float: left; width:76%;padding: 10px 10px 10px 10px;"><img id="myGridImg_section'+idSezione+'" src="images/'+ img1 +'" alt="" style="max-width:100%;border: solid 2px #b1c903"></div>' +
								'<div style="float: left; width:24%;display:block;">' + 
									'<div style="padding: 10px 10px 10px 10px;"><img id="myGridThumb1_section'+idSezione+'" src="images/'+ img2 +'" alt="" style="max-width:100%;border: solid 2px #b1c903;"></div>' +
									'<div style="padding: 9px 10px 9px 10px;"><img id="myGridThumb2_section'+idSezione+'" src="images/'+ img3 +'" alt="" style="max-width:100%;border: solid 2px #b1c903;"></div>' +
									'<div style="padding: 10px 10px 10px 10px;"><img id="myGridThumb3_section'+idSezione+'" src="images/'+ img4 +'" alt="" style="max-width:100%;border: solid 2px #b1c903;"></div>' +
								'</div>' +
							'</div><br clear="all"/>' +
							'<div id="testo" style="display:block;  margin: 0% 0% 0% 0%; font-size: 15px; overflow-y:auto; text-align: justify; padding-bottom: 20px;">' + testo + '</div>'

							//eventuali allegati
							if (allegati.length>0)
							{
								// ci sono allegati... metto inizialmente una riga di separazione
								html +='<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center" style="padding: 0px; margin: 0px;">' +
								'<hr style="color: #B1C903; background-color: #B1C903; height: 1px; width:90%;margin-left:5%">' +
								'</div>'
								
								var allegatoTipo1 = new Array();
								var allegatoTipo2 = new Array();
								var allegatoTipo3 = new Array();
								//alert("trovati allegati: " + allegati.length);
								//li devo dividere per tipo
								for(allegatiIndex=0; allegatiIndex<allegati.length; allegatiIndex++)
								{ 
									if (allegati[allegatiIndex]['tipo']==1) //allegato
									{
										allegatoTipo1.push(allegati[allegatiIndex]);
									}
									else if (allegati[allegatiIndex]['tipo']==2) //collegamento
									{
										allegatoTipo2.push(allegati[allegatiIndex]);
									}
									else if (allegati[allegatiIndex]['tipo']==3) //rassegna stampa
									{
										allegatoTipo3.push(allegati[allegatiIndex]);
									}
								}								
								//alert("Allegato tipo 1: " + allegatoTipo1.length);
								//alert("Allegato tipo 2: " + allegatoTipo2.length);
								//alert("Allegato tipo 3: " + allegatoTipo3.length);
								var numSezioni=0;
								if (allegatoTipo1.length>0) numSezioni++;
								if (allegatoTipo2.length>0) numSezioni++;
								if (allegatoTipo3.length>0) numSezioni++;
								var larghezzaSez = 100/numSezioni;
								
								var icona="";
								
								html += '<div style="display: table;padding: 5px 10px 5px 10px;width:100%"><div style="display: table-row;padding: 5px 10px 5px 10px;">'
								
								if (allegatoTipo1.length>0)
								{
									html += '<div style="display: table-cell;width:'+larghezzaSez+'%;">' + 
										'<div class="titleAllegati" style="margin-left:8%; margin-right:8%; text-align:center;  color:#b1c903;">ALLEGATI</div>'
									for(allegatiIndex=0; allegatiIndex<allegatoTipo1.length; allegatiIndex++)
									{
										//icona
										icona = $.findIconByType(allegatoTipo1[allegatiIndex]['icona']);
										var link="";
										if (allegatoTipo1[allegatiIndex]['isUrl']==0) link="allegati/" + allegatoTipo1[allegatiIndex]['url'];
										else link=allegatoTipo1[allegatiIndex]['url'];
										html += '<div class="itemAllegati" style="margin-left:8%; margin-right:8%; text-align:left;  color:#58585a;"><a target="_blank" href="'+ link +'"><img src="images/'+icona+'" height="20px" style="margin-right:10px;"\>'+allegatoTipo1[allegatiIndex]['testo']+'</a></div>'
									}
									html += '</div>'								
								}
								if (allegatoTipo2.length>0)
								{
									//ci sono allegati di tipo 1(per mettere una riga verticale)
									if (allegatoTipo1.length>0) //metto un'altra cella con una riga verticale
									{
										html += '<div style="display: table-cell;width:2px;background-color:#B1C903"></div>'
									}
									html += '<div style="display: table-cell;width:'+larghezzaSez+'%;">' + 
										'<div class="titleAllegati" style="margin-left:8%; margin-right:8%; text-align:center;  color:#b1c903;">COLLEGAMENTI</div>'
									for(allegatiIndex=0; allegatiIndex<allegatoTipo2.length; allegatiIndex++)
									{
										//icona
										icona = $.findIconByType(allegatoTipo2[allegatiIndex]['icona']);
										var link="";
										if (allegatoTipo2[allegatiIndex]['isUrl']==0) link="allegati/" + allegatoTipo2[allegatiIndex]['url'];
										else link=allegatoTipo2[allegatiIndex]['url'];
										html += '<div class="itemAllegati" style="margin-left:8%; margin-right:8%; text-align:left;  color:#58585a;"><a target="_blank" href="'+ link +'"><img src="images/'+icona+'" height="20px" style="margin-right:10px;"\>'+allegatoTipo2[allegatiIndex]['testo']+'</a></div>'
									}									
									html += '</div>'								
								}
								if (allegatoTipo3.length>0)
								{
									//ci sono allegati di tipo 1 o 2(per mettere una riga verticale)
									if ((allegatoTipo1.length>0) || (allegatoTipo2.length>0))
									{
										html += '<div style="display: table-cell;width:1px;background-color:#B1C903"></div>'
									}
									html += '<div style="display: table-cell;width:'+larghezzaSez+'%;">' + 
										'<div class="titleAllegati" style="margin-left:8%; margin-right:8%; text-align:center;  color:#b1c903;">RASSEGNA STAMPA</div>'
									for(allegatiIndex=0; allegatiIndex<allegatoTipo3.length; allegatiIndex++)
									{
										//icona
										icona = $.findIconByType(allegatoTipo3[allegatiIndex]['icona']);
										var link="";
										if (allegatoTipo3[allegatiIndex]['isUrl']==0) link="allegati/" + allegatoTipo3[allegatiIndex]['url'];
										else link=allegatoTipo3[allegatiIndex]['url'];
										html += '<div class="itemAllegati" style="margin-left:8%; margin-right:8%; text-align:left;  color:#58585a;"><a target="_blank" href="'+ link +'"><img src="images/'+icona+'" height="20px" style="margin-right:10px;"\>'+allegatoTipo3[allegatiIndex]['testo']+'</a></div>'
									}									
									html += '</div>'								
								}
							}
							
							
							
						html += '</div>' +						
						'<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center" style="padding: 0px; margin: 0px;">' +
						'<hr style="color: #B1C903; background-color: #B1C903; height: 2px; width:100%">' +
						'</div>' +
					'</div>' 
				}
				//tipo 9: Titolo, descrizione e griglia di immagini (competenze)
				else if (type=="9")
				{
					//creo la sezione
					html =	
					'<div id="section'+idSezione+'" class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-left" style="padding: 10px 10px 0px 10px; margin: 0px 0px 0px 0px;">' + 		
						'<div id="competenzeTitle" class="titleSection" style="margin-left:8%; margin-right:8%; text-align:center; color:#b1c903;">' + titolo + '</div>'	+				
						'<div style="margin: 0% 0% 0% 0%; font-size: 15px; overflow-y:auto; text-align: justify; padding-bottom: 20px;">' + testo + '</div>' +

						'<div id="competenzeDivTest1" style="padding-left:5%;padding-right:5%">' +
							'<div style="float: left; width:20%;padding: 5px 5px 5px 5px;"><img id="competenzeItem1" src="images/vision-applicazioniWeb.png" alt="" style="max-width:100%;"></div>' +
							'<div style="float: left; width:20%;padding: 5px 5px 5px 5px;"><img id="competenzeItem2" src="images/vision-appMobileMulticanale.png" alt="" style="max-width:100%;"></div>' +
							'<div style="float: left; width:20%;padding: 5px 5px 5px 5px;"><img id="competenzeItem3" src="images/vision-webMarketing.png" alt="" style="max-width:100%;"></div>' +
							'<div style="float: left; width:20%;padding: 5px 5px 5px 5px;"><img id="competenzeItem4" src="images/vision-realtaAumentata.png" alt="" style="max-width:100%;"></div>' +
							'<div style="float: left; width:20%;padding: 5px 5px 5px 5px;"><img id="competenzeItem5" src="images/vision-app3D.png" alt="" style="max-width:100%;"></div>' +
							'<div id="collapseCompetenze1" class="collapse" style="padding-left:20px;padding-right:20px;clear:both;">' +
								'<div style="font-size:1px;">&nbsp;</div>' +
								'<div style="background-color:#dddddd;">' +
									'<div style="font-weight: 900; color: #58585a; font-size: 52px;padding: 0px 0 10px;margin-bottom: 10px;padding-top: 15px; line-height: 40px;text-align: center;" id="collapseTitoloCompetenze1">ciao ciao</div>' +
									'<div style="padding-left:8%;padding-right:8%">' +
										'<div style="display:table-row">' +
											'<div style="display:table-cell;width:10%">' +
												'<img src="images/virgolette-inizio.png" width="60px" style="margin-right:5px;"/>' +
											'</div>' +
											'<p id="collapseVirgolettatoCompetenze1" style="font-style:italic;text-align: justify;display:table-cell;width:80%;padding-top:20px;padding-bottom:20px;font-weight: 400;font-size: 16px;line-height: 22px;color: #999;"></p>' +
											'<div style="display:table-cell;width:10%;height:100%;position:relative;">' +
												'<img src="images/virgolette-fine.png" width="60px" style="margin-left:5px;position: absolute;bottom: 0px;"/>' +
											'</div>' +
											'<br clear="all"/>' +
										'</div>' +
									'</div>' +
									'<div style="font-weight: 400;font-size: 16px;line-height: 22px;color: #999;padding-left:3%;padding-right:3%" id="collapseTestoCompetenze1">ciao ciao</div>' +
									'<div style="padding:20px;text-align: center;">' +
										'<a id="collapseLinkCompetenze1" class="pulsanteReadMore" href="" style="font-weight: 900;font-size: 14px;background-color: transparent;margin-left:10px;padding:10px">SCOPRI DI PIU</a>' +
									'</div>' +
								'</div>' +
							'</div>' +
						'</div>' + 
						'<div id="competenzeDivTest2" style="padding-left:5%;padding-right:5%">' +
							'<div style="float: left; width:20%;padding: 5px 5px 5px 5px;"><img id="competenzeItem6" src="images/vision-IOT.png" alt="" style="max-width:100%;"></div>' +
							'<div style="float: left; width:20%;padding: 5px 5px 5px 5px;"><img id="competenzeItem7" src="images/vision-tecnologieEmergenti.png" alt="" style="max-width:100%;"></div>' +
							'<div style="float: left; width:20%;padding: 5px 5px 5px 5px;"><img id="competenzeItem8" src="images/vision-sistemiCollaborazioneAziendale.png" alt="" style="max-width:100%;"></div>' +
							'<div style="float: left; width:20%;padding: 5px 5px 5px 5px;"><img id="competenzeItem9" src="images/vision-identitaDigitale.png" alt="" style="max-width:100%;"></div>' +
							'<div style="float: left; width:20%;padding: 5px 5px 5px 5px;"><img id="competenzeItem10" src="images/vision-gestioneInformazioni.png" alt="" style="max-width:100%;"></div>' +
							'<div id="collapseCompetenze2" class="collapse" style="padding-left:20px;padding-right:20px;clear:both;">' +
									'<div style="font-size:1px;">&nbsp;</div>' +
									'<div style="background-color:#dddddd;">' +
										'<div style="font-weight: 900; color: #58585a; font-size: 52px;padding: 0px 0 10px;margin-bottom: 10px;padding-top: 15px; line-height: 40px;text-align: center;" id="collapseTitoloCompetenze2">ciao ciao</div>' +
										'<div style="padding-left:8%;padding-right:8%">' +
											'<div style="display:table-row">' +
												'<div style="display:table-cell;width:10%">' +
													'<img src="images/virgolette-inizio.png" width="60px" style="margin-right:5px;"/>' +
												'</div>' +
												'<p id="collapseVirgolettatoCompetenze2" style="font-style:italic;text-align: justify;display:table-cell;width:80%;padding-top:20px;padding-bottom:20px;font-weight: 400;font-size: 16px;line-height: 22px;color: #999;"></p>' +
												'<div style="display:table-cell;width:10%;height:100%;position:relative;">' +
													'<img src="images/virgolette-fine.png" width="60px" style="margin-left:5px;position: absolute;bottom: 0px;"/>' +
												'</div>' +
												'<br clear="all"/>' +
											'</div>' +
										'</div>' +
										'<div style="font-weight: 400;font-size: 16px;line-height: 22px;color: #999;padding-left:3%;padding-right:3%" id="collapseTestoCompetenze2">ciao ciao</div>' +
										'<div style="padding:20px;text-align: center;"><a id="collapseLinkCompetenze2" class="pulsanteReadMore" href="" style="font-weight: 900;font-size: 14px;background-color: transparent;margin-left:10px;padding:10px">SCOPRI DI PIU</a></div>' +
									'</div>' +
							'</div>' +
						'</div>' + 
					'</div>' + 
					
					'<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center" style="padding-top: 25px; margin: 0px;">' +
					'<hr style="color: #B1C903; background-color: #B1C903; height: 2px; width:100%">' +
					'</div>'
				}
				//tipo 11: grafico competenze
				else if (type=="11")
				{
					//alert("arrivato");
					html =	
					'<div id="sectionCompetenze" class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-left" style="padding: 10px 10px 0px 10px; margin: 0px 0px 0px 0px;">' +
					'<canvas id="viewport" width="800" height="600" ></canvas></div>'				
				}
				//tipo 12: Titolo, Immagine, Testo di fianco
				else if (type=="12")
				{
					//alert("media length: " + media.length);
					//mi aspetto 1 immagine
					var img1=media[0]['url'];
					
					html =	
					'<div id="section'+idSezione+'" class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-left" style="padding: 0px 10px 0px 10px; margin: 0px 0px 0px 0px;">' + 		
						'<div id="test">' +
							'<div id="titolo" class="titleSection" style="margin-left:0%; margin-right:0%; text-align:center;  color:#b1c903;">' + titolo + '</div>'	+				
							'<div id="testo" style="display:block;  margin: 0% 0% 0% 0%; font-size: 15px; overflow-y:auto; text-align: justify; padding-bottom: 20px;">' + 
								'<div style="margin: 0% 0% 0% 0%;padding-bottom: 5px;width:45%;float: left;">' + 
									'<div style="float: left; width:95%;padding: 10px 0px 10px 10px;"><img src="images/'+ img1 +'" alt="" style="max-width:100%;border: solid 2px #b1c903"></div>' +
								'</div>'
							
								//se il testo è molto lungo, vedo di dividerlo...
								if (testo.length>400)
								{
									var arrTestoDiviso = $dividiTesto(testo,400);
									//prendo i primi 100 caratteri
									var testoArticoloParte1=arrTestoDiviso[0];
									var testoArticoloParte2=arrTestoDiviso[1];
									html += testoArticoloParte1 + 
									'<div id="collapseSection'+idSezione+'" class="collapse">' +
										'<div style="display:block;  margin: 0% 0% 0% 0%; font-size: 15px; text-align: justify; padding-bottom: 20px;">' + testoArticoloParte2 + '</div>' +
									'</div>'
									//c'e' un url associato a questa sezione?
									if (url=="" || url==null)
									{
										html +='<p style="margin-top:10px;"><a id="readMoreSection'+idSezione+'" data-toggle="collapse" data-target="#collapseSection'+idSezione+'" class="pulsanteReadMore">MORE</a>'
										'</p></div>'
									}
									else
									{
										html +='<p style="margin-top:10px;"><a id="readMoreSection'+idSezione+'" data-toggle="collapse" data-target="#collapseSection'+idSezione+'" class="pulsanteReadMore">MORE</a><a class="pulsanteReadMore" href="'+url+'" style="margin-right:10px;" target="_blank">WEBSITE</a>'
										'</p></div>'
									}
								}
								else
								{
									html += testo
								}
								
								//testo +
						
							
							html += '</div>'
							
							
							
							//eventuali allegati
							if (allegati.length>0)
							{
								// ci sono allegati... metto inizialmente una riga di separazione
								html +='<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center" style="padding: 0px; margin: 0px;">' +
								'<hr style="color: #B1C903; background-color: #B1C903; height: 1px; width:90%;margin-left:5%">' +
								'</div>'
								
								var allegatoTipo1 = new Array();
								var allegatoTipo2 = new Array();
								var allegatoTipo3 = new Array();
								//alert("trovati allegati: " + allegati.length);
								//li devo dividere per tipo
								for(allegatiIndex=0; allegatiIndex<allegati.length; allegatiIndex++)
								{ 
									if (allegati[allegatiIndex]['tipo']==1) //allegato
									{
										allegatoTipo1.push(allegati[allegatiIndex]);
									}
									else if (allegati[allegatiIndex]['tipo']==2) //collegamento
									{
										allegatoTipo2.push(allegati[allegatiIndex]);
									}
									else if (allegati[allegatiIndex]['tipo']==3) //rassegna stampa
									{
										allegatoTipo3.push(allegati[allegatiIndex]);
									}
								}								
								//alert("Allegato tipo 1: " + allegatoTipo1.length);
								//alert("Allegato tipo 2: " + allegatoTipo2.length);
								//alert("Allegato tipo 3: " + allegatoTipo3.length);
								var numSezioni=0;
								if (allegatoTipo1.length>0) numSezioni++;
								if (allegatoTipo2.length>0) numSezioni++;
								if (allegatoTipo3.length>0) numSezioni++;
								var larghezzaSez = 100/numSezioni;
								
								var icona="";
								
								html += '<div style="display: table;padding: 5px 10px 5px 10px;width:100%"><div style="display: table-row;padding: 5px 10px 5px 10px;">'
								
								if (allegatoTipo1.length>0)
								{
									html += '<div style="display: table-cell;width:'+larghezzaSez+'%;">' + 
										'<div class="titleAllegati" style="margin-left:8%; margin-right:8%; text-align:center;  color:#b1c903;">ALLEGATI</div>'
									for(allegatiIndex=0; allegatiIndex<allegatoTipo1.length; allegatiIndex++)
									{
										//icona
										icona = $.findIconByType(allegatoTipo1[allegatiIndex]['icona']);
										var link="";
										if (allegatoTipo1[allegatiIndex]['isUrl']==0) link="allegati/" + allegatoTipo1[allegatiIndex]['url'];
										else link=allegatoTipo1[allegatiIndex]['url'];
										html += '<div class="itemAllegati" style="margin-left:8%; margin-right:8%; text-align:left;  color:#58585a;"><a target="_blank" href="'+ link +'"><img src="images/'+icona+'" height="20px" style="margin-right:10px;"\>'+allegatoTipo1[allegatiIndex]['testo']+'</a></div>'
									}
									html += '</div>'								
								}
								if (allegatoTipo2.length>0)
								{
									//ci sono allegati di tipo 1(per mettere una riga verticale)
									if (allegatoTipo1.length>0) //metto un'altra cella con una riga verticale
									{
										html += '<div style="display: table-cell;width:2px;background-color:#B1C903"></div>'
									}
									html += '<div style="display: table-cell;width:'+larghezzaSez+'%;">' + 
										'<div class="titleAllegati" style="margin-left:8%; margin-right:8%; text-align:center;  color:#b1c903;">COLLEGAMENTI</div>'
									for(allegatiIndex=0; allegatiIndex<allegatoTipo2.length; allegatiIndex++)
									{
										//icona
										icona = $.findIconByType(allegatoTipo2[allegatiIndex]['icona']);
										var link="";
										if (allegatoTipo2[allegatiIndex]['isUrl']==0) link="allegati/" + allegatoTipo2[allegatiIndex]['url'];
										else link=allegatoTipo2[allegatiIndex]['url'];
										html += '<div class="itemAllegati" style="margin-left:8%; margin-right:8%; text-align:left;  color:#58585a;"><a target="_blank" href="'+ link +'"><img src="images/'+icona+'" height="20px" style="margin-right:10px;"\>'+allegatoTipo2[allegatiIndex]['testo']+'</a></div>'
									}									
									html += '</div>'								
								}
								if (allegatoTipo3.length>0)
								{
									//ci sono allegati di tipo 1 o 2(per mettere una riga verticale)
									if ((allegatoTipo1.length>0) || (allegatoTipo2.length>0))
									{
										html += '<div style="display: table-cell;width:1px;background-color:#B1C903"></div>'
									}
									html += '<div style="display: table-cell;width:'+larghezzaSez+'%;">' + 
										'<div class="titleAllegati" style="margin-left:8%; margin-right:8%; text-align:center;  color:#b1c903;">RASSEGNA STAMPA</div>'
									for(allegatiIndex=0; allegatiIndex<allegatoTipo3.length; allegatiIndex++)
									{
										//icona
										icona = $.findIconByType(allegatoTipo3[allegatiIndex]['icona']);
										var link="";
										if (allegatoTipo3[allegatiIndex]['isUrl']==0) link="allegati/" + allegatoTipo3[allegatiIndex]['url'];
										else link=allegatoTipo3[allegatiIndex]['url'];
										html += '<div class="itemAllegati" style="margin-left:8%; margin-right:8%; text-align:left;  color:#58585a;"><a target="_blank" href="'+ link +'"><img src="images/'+icona+'" height="20px" style="margin-right:10px;"\>'+allegatoTipo3[allegatiIndex]['testo']+'</a></div>'
									}									
									html += '</div>'								
								}
							}
						
						html += '</div></div>' +						
						'<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center" style="padding: 0px; margin: 0px;">' +
						'<hr style="color: #B1C903; background-color: #B1C903; height: 2px; width:100%">' +
						'</div>' +
					'</div>' 
				}
				//tipo 13: footer
				else if (type=="13")
				{
					html =	
					'<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-left" style="padding: 10px 10px 0px 10px; margin: 0px 0px 0px 0px;">' + 		
						'<div style="margin: 0% 0% 0% 0%; font-size: 15px; overflow-y:auto; text-align: center; padding-bottom: 20px;">&copy; <?=date("Y", time());?>  MediaSoft srl</div>' +
					'</div>' 
				}
				//tipo 14: social feed
				else if (type=="14")
				{
					//alert("arrivato");
					html =	
					'<div id="sectionSocialFeed" class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-left" style="padding: 10px 10px 0px 10px; margin: 0px 0px 0px 0px;">' + 		
						'<div style="display:table;width:100%">' +
							'<div style="display:table-row">' +
								'<div style="padding:5px;display:table-cell;margin: auto;text-align: center;"><img src="images/logo_facebook.png" width="30%"></div>' +
								'<div style="display:table-cell"></div>' +
								'<div style="padding:5px;display:table-cell;margin: auto;text-align: center;"><img src="images/logo_twitter.png" width="30%"></div>' +
							'</div>' +
							'<div style="display:table-row">' +
								'<div class="container-like" style="overflow: hidden;padding:5px;display:table-cell">' +
									'<div class="fb-like-box" style="margin-top:-70px;border:none!important" data-href="https://www.facebook.com/SeoPosizionamentoMotoriRicerca"  data-colorscheme="light" data-show-faces="false" data-header="false" data-stream="true" data-show-border="false"></div>' +
								'</div>' +

								'<div style="display:table-cell;width=3px;background-color:#B1C903"></div>' +
								
								'<div class="container-like2" style="overflow: hidden;padding:5px;display:table-cell">' +
									'<a class="twitter-timeline"  href="https://twitter.com/Mediasoftonline"  data-chrome="nofooter noheader"  data-widget-id="461507894010060800">Tweets di @Mediasoftonline</a>' +
								'</div>' +
							'</div>' +
						'</div>' +
					'</div>'
				}
				return html;
			},
			'renderer2': function(row_data)
			{
				var idSezione = row_data['idSezione'];
				var titolo = row_data['titolo_menu_lat'];
				
				if (titolo!=null) var html = '<li><a href="#section'+idSezione+'"><img src="images/pallino_verde_20.png" style="width:8px;height:8px;margin-right:6px;margin-bottom:2px;"\>'+titolo+'</a></li>'
				return html;
			}
		}
		

		$(window).load(function() {

			//$.buildTopMenuNew($.urlParam('idPage'));
			//$.tweakIpadLandscape();
			if($.detectMobile() == true) {
				//tolgo il menu' a destra e metto il resto al 100%
				$('#mainContainerDiv').css("width","100%");
				$('#navbar-laterale').css("visibility","hidden");
			}
			

			
		});


		$(document).ready(function()
		{

			$.setupNavbar();			
			//$.alignLeftAndRightMenuItems();
			$.centerNavbarByLogo();
			$.setThemeBlack();
			//if (menu_color=="1") $.setThemeBlack();//nero
			//else $.setThemeWhite(); 


			setTimeout(function()
			{

				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/it_IT/all.js#xfbml=1&appId=528759423900786";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));  

				!function(d,s,id){
					var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';
					if(!d.getElementById(id)){
						js=d.createElement(s);js.id=id;
						js.src=p+"://platform.twitter.com/widgets.js";
						fjs.parentNode.insertBefore(js,fjs);
						}
				}(document,"script","twitter-wjs");
			
			
				var mainPageConfig = pages_config[$.urlParam('idPage')];
				var next_content = mainPageConfig.next_content;
				var index = 0;
				
				$.setDivContent2( mainPageConfig, true, false);

				
				$('a[href*=#]:not([href=#])').click(function()
				{
					
					if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
					  var target = $(this.hash);
					  
					  target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
					  
					  var offsetTimeline=0;
					  
					  var name=($(target).attr('id'));
					  //alert(name.charAt(0));
					  //alert(name);
					  
					  if (name.charAt(0)=="t") offsetTimeline=120; //timeline
					  else offsetTimeline=0;

					  if (target.length) {
						$('html,body').animate({
						  scrollTop: target.offset().top-100-offsetTimeline
						}, 500);
						return false;
					  }
					}
				});	
				


				/*
				//esiste la sezione competenze?
				if ($('#competenzeTitle').length != 0)
				{
					$(function() {
						//prima di far partire il js della griglia, ridimensiono le immagini
						var element_width = ($('#mainContainerDiv').width()*0.70)/5;
						//alert(element_width);
						$('#competenze1').width(element_width);
						$('#competenze2').width(element_width);
						$('#competenze3').width(element_width);
						$('#competenze4').width(element_width);
						$('#competenze5').width(element_width);
						$('#competenze6').width(element_width);
						$('#competenze7').width(element_width);
						$('#competenze8').width(element_width);
						$('#competenze9').width(element_width);
						$('#competenze10').width(element_width);
						//var $grid = $( '#og-grid' );
						//alert($grid.attr('id'));
						//alert($( '#TitoloCompetenze' ).attr('id'));
						//alert($( '#TitoloCompetenze' ).text());
						//sposto la griglia nel div appropriato
						$("#og-grid").appendTo('#competenzeDiv');
						//rimetto la griglia a visibile
						$( '#og-grid').css("visibility", "visible");
						Grid.init();
					});
				}
				else
				{
					//elimino la og-grid che avevo messo statica
					$( '#og-grid').remove();
				}
				*/

				$('img').click(function(event)
				{
					//è un thumb della categoria myGrid, quello che sto cliccando?
					var name=($(event.target).attr('id'));
					
					if (name.substr(0,11)=="myGridThumb")
					{
						//di che sezione fa parte?
						var sezione = name.substr(12);
						//alert(sezione);
						//devo fare lo swap delle 2 immagini
						var src_thumb=($(event.target).attr('src'));
						var src_image=($("#myGridImg"+sezione).attr('src'));
						//faccio lo swap
						$("#myGridImg"+sezione).fadeOut(function(){
						  $(this).attr("src",src_thumb).fadeIn();
						});					  

	//					$("#myGridImg"+sezione).attr('src',src_thumb);
						
						$(event.target).attr('src',src_image);
					
					}
				});

				
				$('[id^=readMoreSection]').click(function(event)
				{
					var $this = $(this);
					var testo = $this.text();
					if (testo=="MORE") $this.text("LESS"); 
					else $this.text("MORE");
				});
				
				
				$('[id^=competenzeItem]').click(function(event)
				{
					var $this = $(this);
					//alert($this.attr('id'));
					var titolo="";
					var virgolettato="";
					var descrizione="";
					var link="";
					var collapseSection=0;
					if ($this.attr('id')=="competenzeItem1")
					{
						collapseSection=1;
						titolo="APPLICAZIONI WEB";
						virgolettato="Sviluppiamo le vostre soluzioni sotto forma di applicazioni web quali sistemi di monitoraggio, gestionali per il web, CRM, gestione progetti, automazione della rete vendita, sistemi di prenotazione alberghiera ed altre ancora.";
						descrizione="Ogni qualvolta si usi un social network come facebook, si guardi un video su Youtube, o si legga la propria casella di posta elettronica on-line, si sta utilizzando un'applicazione web, ovvero un software che non risiede fisicamente sul proprio computer ma sul web, e che diviene quindi raggiungibile da qualsiasi terminale connesso ad internet. Mediasoft ha da tempo sposato la filosofia del SaaS, ovvero del Software as Service, sviluppando le proprie soluzioni (dai sistemi gestionali arrivando addirittura a software di diagnostica clinica) sotto forma di applicazioni web";
						link="fixed.content.php?idPage=9";
					}
					else if ($this.attr('id')=="competenzeItem2")
					{
						collapseSection=1;
						titolo="APP MOBILE e MULTICANALE";
						virgolettato="Attiviamo diversi canali tra cui app o portali mobile per dispositivi quali palmari e smartphone, portali vocali fruibili tramite riconoscimento vocale o tastiera telefonica, newsletter tramite mail, invio di SMS, RSS e podcasting dei contenuti multimediali.";
						descrizione="Le applicazioni multicanale permettono di distribuire contenuti formattati in maniera adeguata, a seconda della tipologia di dispositivo impiegato dall’utente finale per la visualizzazione. Mediasoft è in grado di attivare molteplici canali tra cui app (iOS o Android) o portali mobile per dispositivi quali palmari e smartphone, portali vocali fruibili tramite riconoscimento vocale o tastiera telefonica, totem multimediali, Web TV, newsletter tramite mail, invio di SMS, RSS e podcasting dei contenuti multimediali.";
						link="fixed.content.php?idPage=10";
					}
					else if ($this.attr('id')=="competenzeItem3")
					{
						collapseSection=1;
						titolo="WEB MARKETING";
						virgolettato="Annoveriamo certificazioni SEO & SEM in grado di garantire professionalità e capacità riconosciute, indirizzate verso l'ottenimento di una migliore rilevazione, analisi di web marketing e lettura dei siti web da parte dei motori di ricerca.";
						descrizione="Per aggirare l’ostacolo del costante incremento di informazioni di qualità presenti in rete, l'azienda deve usufruire di alcuni accorgimenti e strumenti di web marketing per ottenere una visibilità soddisfacente del proprio sito.  E’ fondamentale strutturare un sito e le sue pagine affinché possano essere facilmente raggiungibili dagli spider dei motori di ricerca e ottimizzate sulle parole chiave per le quali si vuole essere trovati. Il personale Mediasoft vanta certificazioni SEO & SEM che garantiscono professionalità e capacità comprovate, finalizzate ad ottenere la migliore rilevazione, analisi e lettura del sito web da parte dei motori di ricerca.";
						link="fixed.content.php?idPage=11";
					}
					else if ($this.attr('id')=="competenzeItem4")
					{
						collapseSection=1;
						titolo="REALTA' AUMENTATA";
						virgolettato="Utilizziamo la Realtà Aumentata, la sovrapposizione di livelli informativi (immagini, video, oggetti 3D, animazioni) all’ambiente reale; alla normale realtà percepita attraverso i propri sensi, vengono fatte coincidere informazioni sensoriali artificiali/virtuali.";
						descrizione="Gli elementi si aggiungono alla realtà tramite dispositivi dotati di fotocamera, telecamera, webcam o particolari visori, i quali riprendono l’ambiente circostante. Il software rielabora il flusso video in tempo reale, aggiungendo contenuti multimediali in grado di integrarsi al contesto ed arricchirlo tramite geolocalizzazione e tracciamento. Tra gli esempi di Augmented Reality: la confezione di un prodotto che fornisce informazioni aggiuntive sul contenuto; gli oggetti di un catalogo si mostrano in un display video tridimensionalmente; un supporto cartaceo attiva un concorso a premi o un divertente gioco.";
						link="fixed.content.php?idPage=8";
					}
					else if ($this.attr('id')=="competenzeItem5")
					{
						collapseSection=1;
						titolo="APPLICAZIONI 3D";
						virgolettato="Sviluppiamo applicazioni 3d di diverse tipologie (fondate sull’utilizzo di motori grafici 3D) a seconda delle esigenze richieste dai propri clienti, spaziando da semplici visualizzazioni a simulazioni articolate fino ad applicazioni di realtà aumentata.";
						descrizione="Fondate sull’utilizzo di motori grafici 3D, consentono di navigare interattivamente in un ambiente virtuale o di visualizzare un oggetto mutandone in tempo reale la configurazione, le finiture, i materiali e le forme, di riprodurne le cinematiche, e di programmare logiche complesse a fini simulativi. Le applicazioni sviluppate possono essere di diverso tipo a seconda delle necessità, spaziando da semplici visualizzazioni a simulazioni articolate fino ad applicazioni di realtà aumentata.";
						link="fixed.content.php?idPage=12";
					}
					else if ($this.attr('id')=="competenzeItem6")
					{
						collapseSection=2;
						titolo="INTERNET DELLE COSE";
						virgolettato="Ci accostiamo al mondo dell’Internet delle Cose, che integra il mondo fisico con quello virtuale della rete attraverso tecnologie incorporate agli oggetti quotidiani che rendono le informazioni su questi ultimi, e sul loro ambiente, accessibili e gestibili nel mondo digitale.";
						descrizione="Le piante avvisano l'innaffiatoio quando è il momento di essere innaffiate, le sveglie suonano in anticipo in caso di traffico, le tapparelle si regolano in automatico a seconda del livello di luce presente all’esterno, le scarpe da ginnastica trasmettono tempi, distanza e velocità per gareggiare in tempo reale con persone dall'altra parte del mondo. Fantascienza? Assolutamente no! E’ la nuova rivoluzione dell’Internet delle Cose che consente agli oggetti di potersi conquistare un ruolo attivo attraverso il collegamento alla Rete. Gli oggetti si rendono identificabili e acquisiscono intelligenza grazie alla nuova capacità di poter comunicare dati su se stessi e accedere a informazioni aggregate da parte di altri.";
						link="fixed.content.php?idPage=7";
					}
					else if ($this.attr('id')=="competenzeItem7")
					{
						collapseSection=2;
						titolo="TECNOLOGIE EMERGENTI";
						virgolettato="Esaminiamo le Tecnologie Emergenti degli strumenti abilitanti per Creatività e Innovazione ed esegue su di esse attività di scouting e test. Tale approccio consente all’azienda di realizzare prototipi sperimentali su casi reali anticipando in tal modo le necessità del mercato.";
						descrizione="Le Tecnologie Emergenti rappresentano l’innovazione per eccellenza presente nel mondo della tecnologia e della convergenza tecnologica pertinente a numerosi campi di ricerca. Quelle che attualmente vengono considerate emergenti sono, ad esempio:<br>Natural User Interfaces: la possibilità di dominare sistemi informativi articolati con il solo movimento corporeo consente nuove modalità di impiego di sistemi informativi complessi.<br>Wearable Devices: esistono molteplici sperimentazioni di dispositivi indossabili, ognuna con peculiarità e caratteristiche interessanti dal punto di vista di un’ipotetica collocazione sul mercato.";
						link="fixed.content.php?idPage=13";
					}
					else if ($this.attr('id')=="competenzeItem8")
					{
						collapseSection=2;
						titolo="SISTEMI DI COLLABORAZIONE AZIENDALE";
						virgolettato="Garantiamo strumenti e soluzioni di collaborazione efficienti, in grado di creare l'equivalente virtuale della presenza di tutti i partecipanti in una stessa stanza, alla stessa ora, per esaminare gli stessi dati.";
						descrizione="In un ambiente in cui i team di prodotto sono sempre più distribuiti e i partner e i fornitori sono dislocati in tutto il mondo, una collaborazione efficiente necessita sia della memorizzazione centralizzata dei dati di prodotto sia di mezzi per la condivisione plurilaterale. Le funzionalità di social networking e Web 2.0 custodiscono molteplici potenzialità di collaborazione per gli utenti aziendali, oltre ad avere già trasformato le modalità di interazione dei consumatori.";
						link="fixed.content.php?idPage=14";
					}
					else if ($this.attr('id')=="competenzeItem9")
					{
						collapseSection=2;
						titolo="IDENTITA' DIGITALE";
						virgolettato="Creiamo e integriamo sistemi per la gestione del ciclo di vita delle identità digitali, sviluppando processi di assegnazione delle stesse che possono riguardare l'accesso all’intranet aziendale o l'account di posta.";
						descrizione="Ogni singola persona, che sia il dipendente di un’azienda o un semplice utente esterno di un servizio, è oggetto di un processo di assegnazione di numerose identità digitali che possono riguardare l'accesso all’intranet aziendale o l'account di posta, in maniera coerente con il suo particolare ruolo aziendale. Risulta quindi indispensabile essere in grado di progettare e realizzare o integrare sistemi per la gestione del ciclo di vita delle identità digitali.";
						link="fixed.content.php?idPage=15";
					}
					else if ($this.attr('id')=="competenzeItem10")
					{
						collapseSection=2;
						titolo="GESTIONE DELLE INFORMAZIONI E DELLA CONOSCENZA";
						virgolettato="Sviluppiamo e gestiamo siti web fondati su sistemi di content e knowledge management. Attraverso l'impiego di soluzioni fondate sulle ontologie si ha la possibilità di visualizzare percorsi logici di navigazione e usufruire di motori di ricerca concettuali.";
						descrizione="Mediasoft ha sviluppato negli anni tecniche e metodi per il reperimento, la gestione e l'analisi dei dati, dell'informazione e della conoscenza, posando l'attenzione sull’esigenza di gestione di ampi flussi informativi tipici di organizzazioni complesse, e dell'evoluzione verso una società dell'informazione. Attraverso l'utilizzo di soluzioni fondate sulle ontologie diviene possibile visualizzare percorsi logici di navigazione e usufruire di motori di ricerca concettuali.";
						link="fixed.content.php?idPage=16";
					}
					if (collapseSection==1)
					{
						//ho cliccato lo stesso?
						if ($('#collapseTitoloCompetenze1').text()==titolo)
						{
							//è lo stesso... lo chiudo
							$('#collapseTitoloCompetenze1').text("");
							$('#collapseCompetenze1').collapse('hide');
						}
						else
						{
							//è un altro... lo apro
							$('#collapseTitoloCompetenze1').text(titolo);
							$('#collapseVirgolettatoCompetenze1').text(virgolettato);
							$('#collapseTestoCompetenze1').text(descrizione);
							$('#collapseLinkCompetenze1').attr('href',link);
							$('#collapseCompetenze1').collapse('show');
						}
						//il collapse 2 è aperto?
						if ($('#collapseCompetenze2').hasClass( "in" )) $('#collapseCompetenze2').collapse('hide');
					}
					else if (collapseSection==2)
					{
						//ho cliccato lo stesso?
						if ($('#collapseTitoloCompetenze2').text()==titolo)
						{
							//è lo stesso... lo chiudo
							$('#collapseTitoloCompetenze2').text("");
							$('#collapseCompetenze2').collapse('hide');
						}
						else
						{
							//è un altro... lo apro
							$('#collapseTitoloCompetenze2').text(titolo);
							$('#collapseVirgolettatoCompetenze2').text(virgolettato);
							$('#collapseTestoCompetenze2').text(descrizione);
							$('#collapseLinkCompetenze2').attr('href',link);
							$('#collapseCompetenze2').collapse('show');
						}
						//il collapse 1 è aperto?
						if ($('#collapseCompetenze1').hasClass( "in" )) $('#collapseCompetenze1').collapse('hide');
					}
					
				});


				
				
				//$('body').scrollspy({ target: '#navbar-laterale' })

				//esiste una timeline?
				if ($('.tl1').length != 0)
				{
					$('.tl1').timeline({
						openTriggerClass : '.read_more',
						startItem : '2/2014',
						closeText : 'x',
						categories : ['2007', '2008', '2009', '2010', '2011', '2012', '2013', '2014'],
						nuberOfSegments        : [3, 4, 3, 3, 3, 5, 4, 5], // number of elements per category (number of days)
						yearsOn                : false           // show years (can be any number you use in data-id (elementNumber/category/yearOrSomeOtherNumber))
						});
					$('.tl1').on('ajaxLoaded.timeline', function(e){
						var height = e.element.height()-60-e.element.find('h2').height();
						e.element.find('.timeline_open_content span').css('max-height', height).mCustomScrollbar({
							autoHideScrollbar:true,
							theme:"light-thin"
						});	
					});
				}

				//esiste la sezione socialFeed (siamo quindi nella pagina dei feed social)?
				if ($('#sectionSocialFeed').length != 0)
				{
					//tolgo il menu a dx e metto a tutta larghezza
					$('#mainContainerDiv').css("width","100%");
					$('#navbar-laterale').css("visibility","hidden");
					
					var largh=Math.round(($('#sectionSocialFeed').width()*0.95)/2);
					//alert(largh);
					$('.fb-like-box').attr('data-width',largh);
					//massima larghezza twitter = 520
					if (largh>520) largh=520;
					$('.twitter-timeline').attr('width',largh);
					
					$('.fb-like-box').attr('data-height',470);
					$('.twitter-timeline').attr('height',400);
				}
				
				$(function() {
					var element_width = ($('#mainContainerDiv').width()*0.80)/8;
					var width=$('#mainContainerDiv').width()*0.80;
					var height=$('#mainContainerDiv').width()*0.20;

					var wall = new freewall("#wall_container");
					//wall.fitWidth();
					//wall.fitHeigth();
					wall.reset({
						selector: '.wall_item',
						animate: true,
						cellW: element_width,
						cellH: element_width,
						onResize: function() {
							wall.refresh();
						}
					});
					wall.fitWidth();
					// for scroll bar appear;
					//$(window).trigger("resize");
					//rimetto a posto l'altezza del div
					//var wallHeight = $('#wall_container').height();
					//var titleHeight = $('#wall_container').parent().children('titolo').height();
					//var testoHeight = $('#wall_container').parent().children('testo').height();
					//alert(wallHeight + " - " + titleHeight + " - " + testoHeight);
					//var oldHeight = $('#wall_container').parent().parent().height();
					//alert(oldHeight);
					//$('#wall_container').parent().parent().height(oldHeight*1.3);
				});

				//esiste la sezione dove mettere il grafico a pallini?
				if ($('#sectionCompetenze').length != 0)
				{
					var w = $(window).width()*0.95, h = $(window).height()*0.70
					//devo fare la larghezza in proporzione all'altezza
					w=h*2.2;
					var marginLeft = -(w/2)*0.8;
					$('#viewport').attr('width', w);
					$('#viewport').attr('height', h);

					//riposiziono il canvas al centro
					$('#viewport').css("position","absolute");
					$('#viewport').css("position","absolute");
					$('#viewport').css("top","10px");
					$('#viewport').css("left","50%");
					$('#viewport').css("margin-left",marginLeft);
					
					$.getScript( "arbor-v0.92/competenze_grigio.js" );
					
					/*
					//alert("eccolo");
					setTimeout(function() {
						var w = $('#sectionCompetenze').width()*0.95, h = 500;

						//alert(w);
						//alert(h);
						var labelDistance = 0;

						//alert("ciao");
						
		//				var vis = d3.select(".container").append("svg:svg").attr("width", w).attr("height", h).style("position", "absolute").style("top", "80px"); 
						var vis = d3.select("#sectionCompetenze").append("svg:svg").attr("width", w).attr("height", h).attr("id", "grafico").style("position", "absolute").style("top", "0px").style("background", "transparent").style("display", "none");
						
						//alert(vis);
						
						var nodes = [];
						var labelAnchors = [];
						var labelAnchorLinks = [];
						var links = [];
						var labelText="";
						
						for(var i = 0; i < 28; i++) {
							
							if (i==2) labelText="APPLICAZIONI WEB";
							else if (i==6) labelText="APP MOBILE E MULTICANALE";
							else if (i==10) labelText="WEB MARKETING";
							else if (i==14) labelText="REALTA' AUMENTATA";
							else if (i==16) labelText="APPLICAZIONI 3D";
							else if (i==18) labelText="INTERNET DELLE COSE";
							else if (i==20) labelText="TECNOLOGIE EMERGENTI";
							else if (i==21) labelText="SISTEMI DI COLLABORAZIONE AZIENDALE";
							else if (i==25) labelText="IDENTITA' DIGITALE";
							else if (i==27) labelText="GESTIONE DELLE INFORMAZIONI";
							else labelText="";
							
							
							var node = {label : labelText};
							nodes.push(node);
							labelAnchors.push({	node : node	});
							labelAnchors.push({	node : node	});
						
						};

						var linkWeight=0.6;

						links.push({source : 25,	target : 13,	weight : linkWeight});
						links.push({source : 1,	target : 26,	weight : linkWeight});
						links.push({source : 19,	target : 27,	weight : linkWeight});
						links.push({source : 16,	target : 0,	weight : linkWeight});
						
						links.push({source : 17,	target : 24,	weight : linkWeight});
						//links.push({source : 19,	target : 25,	weight : linkWeight}); --
						links.push({source : 21,	target : 26,	weight : linkWeight});
						//links.push({source : 23,	target : 27,	weight : linkWeight}); --


						//links.push({source : 0,	target : 16,	weight : linkWeight});
						links.push({source : 2,	target : 17,	weight : linkWeight});
						links.push({source : 4,	target : 18,	weight : linkWeight});
						links.push({source : 6,	target : 19,	weight : linkWeight});
						links.push({source : 8,	target : 20,	weight : linkWeight});
						links.push({source : 10,	target : 21,	weight : linkWeight});
						links.push({source : 12,	target : 22,	weight : linkWeight});
						links.push({source : 14,	target : 23,	weight : linkWeight});

						//links.push({source : 24,	target : 25,	weight : linkWeight});
						//links.push({source : 25,	target : 26,	weight : linkWeight});
						//links.push({source : 26,	target : 27,	weight : linkWeight});
						links.push({source : 27,	target : 24,	weight : linkWeight});

						//links.push({source : 16,	target : 17,	weight : linkWeight});
						links.push({source : 17,	target : 18,	weight : linkWeight});
						links.push({source : 18,	target : 19,	weight : linkWeight});
						links.push({source : 19,	target : 20,	weight : linkWeight});
						links.push({source : 20,	target : 21,	weight : linkWeight});
						links.push({source : 21,	target : 22,	weight : linkWeight});
						links.push({source : 22,	target : 23,	weight : linkWeight});
						links.push({source : 23,	target : 16,	weight : linkWeight});

						links.push({source : 0,	target : 1,	weight : linkWeight});
						//links.push({source : 1,	target : 2,	weight : linkWeight});
						links.push({source : 2,	target : 3,	weight : linkWeight});
						links.push({source : 3,	target : 4,	weight : linkWeight});
						links.push({source : 4,	target : 5,	weight : linkWeight});
						links.push({source : 5,	target : 6,	weight : linkWeight});
						links.push({source : 6,	target : 7,	weight : linkWeight});
						links.push({source : 7,	target : 8,	weight : linkWeight});
						links.push({source : 8,	target : 9,	weight : linkWeight});
						links.push({source : 9,	target : 10,	weight : linkWeight});
						links.push({source : 10,	target : 11,	weight : linkWeight});
						links.push({source : 11,	target : 12,	weight : linkWeight});
						links.push({source : 12,	target : 13,	weight : linkWeight});
						links.push({source : 13,	target : 14,	weight : linkWeight});
						links.push({source : 14,	target : 15,	weight : linkWeight});
						links.push({source : 15,	target : 0,	weight : linkWeight});

						
						for(var i = 0; i < nodes.length; i++)
						{
							labelAnchorLinks.push({	source : i * 2,
													target : i * 2 + 1,
													weight : 0.5
													});
						};

						var force = d3.layout.force().size([w, h]).nodes(nodes).links(links).gravity(1).linkDistance(10).charge(-2000).linkStrength(function(x) {
							return x.weight * 10
						});


						force.start();

						
						$("#grafico" ).fadeIn(3000);
						
						var force2 = d3.layout.force().nodes(labelAnchors).links(labelAnchorLinks).gravity(0).linkDistance(0).linkStrength(8).charge(-100).size([w, h]);
						force2.start();

						
		//				for (var i=1;i<10;i++)
		//				{
							setInterval(function() {	force.alpha(0.03);}, 3000);
		//				}
						
						var link = vis.selectAll("line.link").data(links).enter().append("svg:line").attr("class", "link").style("stroke", "#58585a").style("stroke-width", "2px").style("stroke-opacity", "0.5");

						var node = vis.selectAll("g.node").data(force.nodes()).enter().append("svg:g").attr("class", "node").on("click", function(d,i)
						{
							//alert(d.label);
							if (d.label=="WEB MARKETING") window.location.href="fixed.content.php?idPage=11";
							else if (d.label=="APP MOBILE E MULTICANALE") window.location.href="fixed.content.php?idPage=10";
							else if (d.label=="APPLICAZIONI WEB") window.location.href="fixed.content.php?idPage=9";
							else if (d.label=="REALTA' AUMENTATA") window.location.href="fixed.content.php?idPage=8";
							else if (d.label=="APPLICAZIONI 3D") window.location.href="fixed.content.php?idPage=12";
							else if (d.label=="INTERNET DELLE COSE") window.location.href="fixed.content.php?idPage=7";
							else if (d.label=="TECNOLOGIE EMERGENTI") window.location.href="fixed.content.php?idPage=13";
							else if (d.label=="SISTEMI DI COLLABORAZIONE AZIENDALE") window.location.href="fixed.content.php?idPage=14";
							else if (d.label=="IDENTITA' DIGITALE") window.location.href="fixed.content.php?idPage=15";
							else if (d.label=="GESTIONE DELLE INFORMAZIONI") window.location.href="fixed.content.php?idPage=16";
							
						}).on("mouseover", function ()
						{
							document.body.style.cursor = "pointer";
						}).on("mouseout", function ()
						{
							document.body.style.cursor = "auto";
						});
			//			node.append("svg:circle").attr("r", 10).style("fill", "#EEE").style("stroke", "#F00").style("stroke-width", 1);
						node.append("svg:circle").attr("r", function(d,i)	{return (d.label=="") ? 4 : 6}).style("fill", function(d,i)	{return (d.label=="") ? "#58585a" : "#b1c903"}).style("stroke", "#58585a").style("stroke-width", function(d,i)	{return (d.label=="") ? 0 : 0}).style("fill-opacity", function(d,i)	{return (d.label=="") ? "0.3" : "1.0"});
						node.call(force.drag);
						

						var anchorLink = vis.selectAll("line.anchorLink").data(labelAnchorLinks)//.enter().append("svg:line").attr("class", "anchorLink").style("stroke", "#999");

						var anchorNode = vis.selectAll("g.anchorNode").data(force2.nodes()).enter().append("svg:g").attr("class", "anchorNode");
						anchorNode.append("svg:circle").attr("r", 0).style("fill", "#FFF");
						anchorNode.append("svg:text").text(function(d, i)
						{
							return i % 2 == 0 ? "" : d.node.label
						}).style("fill", "#58585a").style("font-family", "Maven Pro").style("font-size", 16).on("mouseover", function ()
						{
							document.body.style.cursor = "pointer";
						}).on("mouseout", function ()
						{
							document.body.style.cursor = "auto";
						}).on("click", function(d,i)
						{
							//alert(d.node.label);
							if (d.node.label=="WEB MARKETING") window.location.href="fixed.content.php?idPage=11";
							else if (d.node.label=="APP MOBILE E MULTICANALE") window.location.href="fixed.content.php?idPage=10";
							else if (d.node.label=="APPLICAZIONI WEB") window.location.href="fixed.content.php?idPage=9";
							else if (d.node.label=="REALTA' AUMENTATA") window.location.href="fixed.content.php?idPage=8";
							else if (d.node.label=="APPLICAZIONI 3D") window.location.href="fixed.content.php?idPage=12";
							else if (d.node.label=="INTERNET DELLE COSE") window.location.href="fixed.content.php?idPage=7";
							else if (d.node.label=="TECNOLOGIE EMERGENTI") window.location.href="fixed.content.php?idPage=13";
							else if (d.node.label=="SISTEMI DI COLLABORAZIONE AZIENDALE") window.location.href="fixed.content.php?idPage=14";
							else if (d.node.label=="IDENTITA' DIGITALE") window.location.href="fixed.content.php?idPage=15";
							else if (d.node.label=="GESTIONE DELLE INFORMAZIONI") window.location.href="fixed.content.php?idPage=16";
							
						});

						var updateLink = function() {
							this.attr("x1", function(d) {
								return d.source.x;
							}).attr("y1", function(d) {
								return d.source.y;
							}).attr("x2", function(d) {
								return d.target.x;
							}).attr("y2", function(d) {
								return d.target.y;
							});

						}

						var updateNode = function() {
							this.attr("transform", function(d) {
								return "translate(" + d.x + "," + d.y + ")";
							});

						}


						force.on("tick", function() {

							
							force2.start();

							node.call(updateNode);

							anchorNode.each(function(d, i) {
								if(i % 2 == 0) {
									d.x = d.node.x;
									d.y = d.node.y;
								} else {
									var b = this.childNodes[1].getBBox();

									var diffX = d.x - d.node.x;
									var diffY = d.y - d.node.y;

									var dist = Math.sqrt(diffX * diffX + diffY * diffY);

									var shiftX = b.width * (diffX - dist) / (dist * 2);
									shiftX = Math.max(-b.width, Math.min(0, shiftX));
									var shiftY = 5;
									this.childNodes[1].setAttribute("transform", "translate(" + shiftX + "," + shiftY + ")");
								}
							});


							anchorNode.call(updateNode);
							link.call(updateLink);
							anchorLink.call(updateLink);

						});
					}, 10);
					*/
				}
				
				
				setTimeout(function()
				{
					$('body').scrollspy({ target: '#navbar-laterale' })

					//nella url ci sta un parametro col nome del prodotto su cui andare?
					var prodotto=$.urlParam('prod');
					//alert(prodotto);
					if (prodotto!="")
					{
						 var target = $('#section'+prodotto);
							var name=(target.attr('id'));
						  //alert(name.charAt(0));
						  //alert(name);
						  
						  if (name.charAt(0)=="t") offsetTimeline=120; //timeline
						  else offsetTimeline=0;

						  if (target.length) {
							$('html,body').animate({
							  scrollTop: target.offset().top-100-offsetTimeline
							}, 500);
							return false;
						  }

					}
				}, 1100);
			
			}, 10);

		});





	</script>

  </body>

</html>
