Certificate Download and Installation

The included certificates and installation instructions are for Microsoft version Windows 2008 - IIS 7.0 & 7.5.

BEFORE YOU BEGIN
You must have admin access to the server where you will install the certificate. If you use a hosted web service or do not have access to your server, forward this ZIP file to your host admin or technical assistant who will install your Rapidssl RSA certificate.
- SSLAssistant

There are two options to install your certificate. You can use the SSL Assistant to install your certificate, or install your certificate manually.

(OPTIONAL) TO USE SSL ASSISTANT TO INSTALL YOUR CERTIFICATE ON WINDOWS IIS
The SSL Assistant installs your primary and intermediate CA certificates.
1. Move the SSL Assistant folder onto the server where you plan to install the certificate.
2. In the SSL Assistant folder, run sslassistant_installcert.exe and follow the on-screen instructions.
Note: SSL Assistant does not install anything on your server and does not run as a background process. SSL Assistant does not gather or send any information from your server.

TO INSTALL YOUR CERTIFICATE
*************************
PKCS#7(.p7b) file includes intermediate certificates that are automatically installed with your certificate.It is not necessary to separately install intermediate certificates when using PKCS#7(.p7b) file.
*************************

To get detailed installation instructions for your server, go to:
https://knowledge.rapidssl.com/support/ssl-certificate-support/index?page=content&id=SO10517

CHECK YOUR CERTIFICATE INSTALLATION
To test your newly installed certificate with the SSL Toolbox, go to:
https://cryptoreport.rapidssl.com/checker/views/certCheck.jsp

INSTALL RAPIDSSL SECURED SEAL
Take advantage of the trust mark that gives customers confidence put the RapidSSL Secured Site Seal on your site today! RapidSSL Secured Seal is included with your certificate purchase.
To customize and install the seal on your web site, go to:
https://knowledge.rapidssl.com/support/ssl-certificate-support/index?page=content&id=SO14424

FOR MORE ASSISTANCE
Visit our customer technical support site:
https://knowledge.rapidssl.com/support/ssl-certificate-support/index.html