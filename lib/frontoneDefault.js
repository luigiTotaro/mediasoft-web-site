var slider = {
    width: '100%',  //fullscreen set 100% - layer not responsive
    height: '100%', //fullscreen set 100% - layer not responsive
    options : {
      	//layout properties
      	responsive: true,
      	responsiveUnder: 0,
      	layersContainer: 0,
      	hideOnMobile: false,
      	hideUnder: 0,
      	hideOver:100000,

      	//Slideshow properties
      	autoStart:true,
      	startInViewport:true,
      	pauseOnHover:false,
      	firstSlide:1,
      	animateFirstSlide:true,
      	sliderFadeInDuration:350,
      	loops:0,
      	forceLoopNum:true,
      	twoWaySlideshow:false,
      	randomSlideshow:false,
		
		//Appearance properties
		skin: 'v5', //v5,borderlesslight,carousel,fullwidthdark,borderlesslight3d,borderlessdark3d..
		skinsPath: 'layerslider/skins/',
		globalBGColor: 'transparent',
		globalBGImage: false,

		//Navigation properties
		navPrevNext: false,
		navStartStop: false,
		navButtons: false,
		hoverPrevNext: false,
		hoverBottomNav: false,
		keybNav: false,
		touchNav: false,
		showBarTimer: false,
		showCircleTimer: false,
		thumbnailNavigation: 'hover',
		tnContainerWidth: '60%',
		tnWidth: 100,
		tnHeight: 60,
		tnActiveOpacity: 35,
		tnInactiveOpacity: 100,

		//Video properties
		autoPlayVideos: false,
		autoPauseSlideshow: 'auto',
		youtubePreview: '',

		//Image preload properties
		imgPreload: true,
		lazyLoad: true,

		//YourLogo properties
		yourLogo: '',
		yourLogoLink: '',
		yourLogoStyle: '',
		yourLogoTarget: ''
	},
	slides: [
		{
			slidedelay: 4000,
			transitionType: 'transition2d', //2d o 3d
			transitionNumber: 'all', //1 a 113 per il 2d, 1 a 100 per il 3d oppure all (si possono scegliere   effetti concatenati con la virgola es. 1,4,7,33....)
			timeshift: 0,
			href: '',
			target: '_blank',
			layers:[
				{
					tag: 'img', //div,p,h1,h2,h3,h4 ecc..
					class: 'ls-bg', //ls-l per i layer oppure ls-bg per il background
					style: '',
					text: '',
					multimedia: '',
					path: 'frontone/14736797205254.jpg',
					dataLs:{
						offsetxin: 80,
						offsetxout: -80,
						offsetyin: 0,
						offsetyout: 0,
						delayin: 0,
						showuntil: 0,
						durationin: 1000,
						durationout: 1000,
						easingin: 'easeInOutQuint',
						easingout: 'easeInOutQuint',
						fadein: true,
						fadeout: true,
						rotatexin: 0,
						rotatexout: 0,
						rotateyin: 0,
						rotateyout: 0,
						scalexin: 1,
						scalexout: 1,
						scaleyin: 1,
						scaleyout: 1,
						skewxin: 0,
						skewxout: 0,
						skewyin: 0,
						skewyout: 0,
						transformoriginin: '50% 50% 0', 
						transformoriginout: '50% 50% 0'
					}
				}
			]
		},
		{
			slidedelay: 4000,
			transitionType: 'transition2d', //2d o 3d
			transitionNumber: 'all', //1 a 113 per il 2d, 1 a 100 per il 3d oppure all 
			timeshift: 0,
			href: '',
			target: '_blank',
			layers:[
				{
					tag: 'img', //div,p,h1,h2,h3,h4 ecc..
					class: 'ls-bg', //ls-l per i layer oppure ls-bg per il background
					style: '',
					text: '',
					multimedia: '',
					path: 'frontone/14736893794457.jpg',
					dataLs:{
						offsetxin: 80,
						offsetxout: -80,
						offsetyin: 0,
						offsetyout: 0,
						delayin: 0,
						showuntil: 0,
						durationin: 1000,
						durationout: 1000,
						easingin: 'easeInOutQuint',
						easingout: 'easeInOutQuint',
						fadein: true,
						fadeout: true,
						rotatexin: 0,
						rotatexout: 0,
						rotateyin: 0,
						rotateyout: 0,
						scalexin: 1,
						scalexout: 1,
						scaleyin: 1,
						scaleyout: 1,
						skewxin: 0,
						skewxout: 0,
						skewyin: 0,
						skewyout: 0,
						transformoriginin: '50% 50% 0', 
						transformoriginout: '50% 50% 0'
					}
				}
			]
		}
	]
};