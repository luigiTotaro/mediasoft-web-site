<?php

	require_once("includes/beans/LoggedUser.php");
	require_once("includes/beans/Funzione.php");
		
	error_reporting(E_ERROR | E_PARSE);	
	//error_reporting(E_ALL);	
	ini_set('display_errors', 1);
	

	session_start();
	//se non loggato ritorno a login
	if (!isset($_SESSION['loggedUser'])) {

		header("location: login.php?msg=error");//redirect
	}

	$loggedUser = $_SESSION['loggedUser'];
	
?>

<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
<meta charset="utf-8"/>
<title>Sito Mediasoft</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<meta content="" name="description"/>
<meta content="" name="author"/>
<meta name="apple-mobile-web-app-capable" content="yes" />
<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent" />
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
<link href="./assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
<link href="./assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css"/>
<link href="./assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="./assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
<link href="./assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css"/>
<link href="./css/jquery-ui.css" rel="stylesheet"/>
<link href="./css/jquery-ui.theme.min.css" rel="stylesheet"/>
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN PAGE LEVEL PLUGIN STYLES -->
<link href="./assets/global/plugins/icheck/skins/all.css" rel="stylesheet"/>
<!--<link rel="stylesheet" type="text/css" href="./assets/global/plugins/bootstrap-select/bootstrap-select.min.css"/> -->
<link rel="stylesheet" type="text/css" href="./assets/global/plugins/select2/select2.css"/>
<link rel="stylesheet" type="text/css" href="./assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
<link rel="stylesheet" type="text/css" href="./assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css"/>
<!--<link rel="stylesheet" type="text/css" href="./assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css"/>-->
<link rel="stylesheet" type="text/css" href="./assets/global/plugins/image-picker/image-picker.css"/>
<link href="./assets/global/plugins/jquery-timepicker/jquery-ui-timepicker-addon.min.css" rel="stylesheet"/>
<link href="./assets/admin/pages/css/profile.css" rel="stylesheet" type="text/css"/>

<!-- <link rel="stylesheet" type="text/css" href="./assets/global/plugins/jquery-multiselect/css/multiple-select.css"/> -->
<!--<link rel="stylesheet" type="text/css" href="./assets/global/plugins/bootstrap-multiselect-plugin/css/bootstrap-multiselect.css"/>-->
<!--<link rel="stylesheet" type="text/css" href="./assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css"/>-->

<!--<link rel="stylesheet" type="text/css" href="./assets/global/plugins/jquery-multi-select/css/multi-select.css"/>-->
<!--<link rel="stylesheet" type="text/css" href="./assets/global/plugins/SliderNav-master/slidernav.css" media="screen, projection"/>-->
<link rel="stylesheet" type="text/css" href="./assets/global/plugins/file-input/fileinput.css"/>


<!-- END PAGE LEVEL PLUGIN STYLES -->
<!-- BEGIN PAGE STYLES -->
<link href="./assets/admin/pages/css/tasks.css" rel="stylesheet" type="text/css"/>
<!-- END PAGE STYLES -->
<!-- BEGIN THEME STYLES -->
<!-- DOC: To use 'rounded corners' style just load 'components-rounded.css' stylesheet instead of 'components.css' in the below style tag -->
<link href="./assets/global/css/components-rounded.css" id="style_components" rel="stylesheet" type="text/css"/>
<link href="./assets/global/css/plugins.css" rel="stylesheet" type="text/css"/>
<link href="./assets/admin/layout/css/layout.css" rel="stylesheet" type="text/css"/>
<link href="./assets/admin/layout/css/themes/darkblue.css" rel="stylesheet" type="text/css" id="style_color"/>

<link href="./assets/global/css/colorpicker.css" rel="stylesheet" type="text/css"/>
<link href="./css/custom.css" rel="stylesheet" type="text/css"/>
 <link href="./css/reveal.css" rel="stylesheet" />
 <link rel="stylesheet" href="./css/theme/moon.css" id="theme">

 <!-- Code syntax highlighting -->
<link rel="stylesheet" href="./lib/css/zenburn.css">

<!-- END THEME STYLES -->
<link rel="apple-touch-icon" href="apple-touch-icon.png" />
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="page-header-fixed page-sidebar-closed-hide-logo page-sidebar-closed-hide-logo ">
<!-- BEGIN HEADER -->
<div class="page-header navbar navbar-fixed-top">
	<!-- BEGIN HEADER INNER -->
	<div class="page-header-inner">
		<div class="page-logo">
			
			<div class="menu-toggler sidebar-toggler">
				<!-- DOC: Remove the above "hide" to enable the sidebar toggler button on header -->
			</div>
		</div>
		<!-- END LOGO -->
		<!-- BEGIN RESPONSIVE MENU TOGGLER -->
		<a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
		</a>
		<!-- END RESPONSIVE MENU TOGGLER -->
		<!-- BEGIN PAGE TOP -->
		<div class="page-top">
			
			<!-- BEGIN TOP NAVIGATION MENU -->
			<div class="top-menu">
				<ul class="nav navbar-nav pull-right">
					<li class="separator hide">
					</li>
					<!-- BEGIN NOTIFICATION DROPDOWN -->
					<!-- END NOTIFICATION DROPDOWN -->
					
					<!-- BEGIN INBOX DROPDOWN -->
					<!-- END INBOX DROPDOWN -->
					
					<!-- BEGIN TODO DROPDOWN -->
					<!-- END TODO DROPDOWN -->
					<!-- BEGIN USER LOGIN DROPDOWN -->
					<!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
					<li class="dropdown dropdown-user dropdown-dark">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
						<span class="username username-hide-on-mobile"><?=$loggedUser->getNome() . ' '. $loggedUser->getCognome() ?></span>
						<!-- DOC: Do not remove below empty space(&nbsp;) as its purposely used -->

						<!--<img alt="" class="img-circle profiloImg" src="./images/images.jpg"/>-->
						</a>
						<ul class="dropdown-menu dropdown-menu-default">
							<li>
								<a href="/" data-link-home="">
								<i class="fa fa-home"></i> Home </a>
							</li>
							
							<li class="divider">
							</li>	
							<li>
								<a href="logout.php">
								<i class="icon-key"></i> Log Out </a>
							</li>
						</ul>
					</li>
					<!-- END USER LOGIN DROPDOWN -->
				</ul>
			</div>
			<!-- END TOP NAVIGATION MENU -->
		</div>
		<!-- END PAGE TOP -->
		
	</div>
	<!-- END HEADER INNER -->
</div>
<!-- END HEADER -->
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
	<div class="page-sidebar-wrapper">
		<div class="page-sidebar navbar-collapse collapse">
			<!-- BEGIN SIDEBAR MENU -->
			
			<ul class="page-sidebar-menu" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
				<?php
					$listaFunzioni = $loggedUser->getFunzioni();

					/*print_r("<PRE>");
					print_r ($listaFunzioni);
					print_r("</PRE>");*/

					for($i = 0; $i <count($listaFunzioni); $i++){

						$item = $listaFunzioni[$i];

						echo "<li id='li_$item->getNome()' class='elencoModuli'> ";

							//echo "<a href='javascript:;''>";
							echo "<a href='#' data-link-type='" . $item-> getClassName() . "'  >"; 
								echo "<i class='" . $item->getIcon() ."'></i>";
								echo "<span class='title'>" .$item->getNome() . "</span>";
								echo "<span class='arrow'></span>";
							echo "</a>";

							if($item->getId() == 3){
								echo '<ul class="sub-menu" data-link-type="moduloFrontoneProdotti" style="display: none;">';
								echo 	'<li>';
								echo 		'<a href="#" data-link-type="moduloFrontoneProdotti">';
								echo 		'Frontone Prodotti</a>';
								echo 	'</li>';
								echo '</ul>';
							}
						
						echo "</li>";
					}
				?>
			</ul>
			<!-- END SIDEBAR MENU -->
		</div>
	</div>
	<!-- END SIDEBAR -->
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			
			<!-- BEGIN PAGE CONTENT INNER -->
			
			<div class="row">
				<!--<div class="col-md-12 col-sm-12">-->
					<!-- BEGIN PORTLET-->
					<div class="portlet light " id="applicationContainer">
					</div>                    
					<!-- END PORTLET-->
				<!--</div>-->
				
			</div>
			
			<!-- END PAGE CONTENT INNER -->
		</div>
	</div>
	<!-- END CONTENT -->
	
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<div class="page-footer">
	<div class="page-footer-inner">
		 2016 &copy; Mediasoftonline.com.
	</div>
	<div class="scroll-to-top">
		<i class="icon-arrow-up"></i>
	</div>
</div>
<!-- END FOOTER -->
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="./assets/global/plugins/respond.min.js"></script>
<script src="./assets/global/plugins/excanvas.min.js"></script> 
<![endif]-->
<script src="./assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="./assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>

<script src="./backbone/underscore.js" type="text/javascript"></script>
<script src="./backbone/backbone.js" type="text/javascript"></script>

<!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="./assets/global/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
<script src="./assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="./assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="./assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="./assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="./assets/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
<script src="./assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<script src="./assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<script src="./assets/global/plugins/bootbox/bootbox.min.js" type="text/javascript"></script>

<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<!--<script type="text/javascript" src="./assets/global/plugins/bootstrap-select/bootstrap-select.min.js"></script>
<script type="text/javascript" src="./assets/global/plugins/jquery-multi-select/js/jquery.multi-select.js"></script>-->
<script type="text/javascript" src="./assets/global/plugins/fuelux/js/spinner.min.js"></script>
<script type="text/javascript" src="./assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" ></script>
<script type="text/javascript" src="./assets/global/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="./assets/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="./assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
<!--<script type="text/javascript" src="./assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js"></script>
<script type="text/javascript" src="./assets/global/plugins/bootstrap-multiselect-plugin/js/bootstrap-multiselect.js"></script>
<script src="./assets/global/plugins/SliderNav-master/slidernav-min.js"></script>
<script src="./assets/global/plugins/jquery-timepicker/jquery-ui-timepicker-addon.js"></script>
<script src="./assets/global/plugins/jquery-timepicker/jquery-ui-sliderAccess.js"></script>
<script type="text/javascript" src="./assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js"></script> -->
<script src="./assets/admin/pages/scripts/components-pickers.js"></script>

<script src="./assets/global/plugins/colorpicker/colorpicker.js" type="text/javascript"></script>
<script src="./assets/global/plugins/file-input/fileinput.js" type="text/javascript"></script>

<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="./assets/global/scripts/App.js" type="text/javascript"></script>
<script src="./assets/admin/layout/scripts/layout.js" type="text/javascript"></script>
<script src="./assets/admin/pages/scripts/index.js" type="text/javascript"></script>
<script src="./js/jquery.form.min.js" type="text/javascript"></script>
<script src="./assets/global/plugins/moment.min.js"></script>
<script src="./assets/global/plugins/icheck/icheck.js"></script>

<script src="./js/tinymce/tinymce.min.js" type="text/javascript"></script>

<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->

<script src="./js/Common.js" type="text/javascript"></script>
<script src="./lib/js/head.min.js"></script>
<script src="./lib/js/classList.js" type="text/javascript"></script>
<script src="./js/reveal.js" type="text/javascript"></script>

<!-- END PAGE LEVEL SCRIPTS -->
<!-- Controller-->
<script src="./js/controller/HightlightsController.js" type="text/javascript"></script>
<script src="./js/controller/TimelineController.js" type="text/javascript"></script>
<script src="./js/controller/ProdottiController.js" type="text/javascript"></script>
<script src="./js/controller/ClientiController.js" type="text/javascript"></script>
<script src="./js/controller/CompetenzeController.js" type="text/javascript"></script>
<script src="./js/controller/RDController.js" type="text/javascript"></script>

<script src="./js/controller/ProdottiFrontoneController.js" type="text/javascript"></script>
<!-- Controller -->

<!-- Interfacce di sistema -->
<script src="./js/views/moduloHightlights.js" type="text/javascript"></script>
<script src="./js/views/moduloHightlightsDetail.js" type="text/javascript"></script>
<script src="./js/views/moduloTimeline.js" type="text/javascript"></script>
<script src="./js/views/moduloTimelineDetail.js" type="text/javascript"></script>
<script src="./js/views/moduloProdotti.js" type="text/javascript"></script>
<script src="./js/views/moduloProdottiDetail.js" type="text/javascript"></script>
<script src="./js/views/moduloClienti.js" type="text/javascript"></script>
<script src="./js/views/moduloClientiDetail.js" type="text/javascript"></script>
<script src="./js/views/moduloCompetenze.js" type="text/javascript"></script>
<script src="./js/views/moduloCompetenzeDetail.js" type="text/javascript"></script>
<script src="./js/views/moduloRD.js" type="text/javascript"></script>
<script src="./js/views/moduloRDDetail.js" type="text/javascript"></script>

<script src="./js/views/moduloFrontoneProdotti.js" type="text/javascript"></script>
<!-- Interfacce di sistema -->

<script>


window.loggedUser = <?=$_SESSION['loggedUser']->toJson()?>;

jQuery(document).ready(function() {    

   App.init(); // init metronic core componets
   Layout.init(); // init layout
   Index.init();  
	
   $("[data-link-type]").click(function(event) {
  
   		var dataLinkType = $(event.currentTarget).attr("data-link-type");

   		console.log(dataLinkType)

   		$('.elencoModuli').removeClass('start active');
   		$(event.currentTarget).parent('li').addClass('start active');
   		//var permissinWrite = $(event.currentTarget).attr("data-permission-write");
		var modulo = 'new ' + dataLinkType  + "({ canWrite: true})";
		eval(modulo);
		
   });
  
});
</script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>