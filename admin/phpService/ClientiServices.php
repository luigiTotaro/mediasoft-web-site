<?php

error_reporting(E_ERROR);
ini_set('display_errors', 1);

require_once("../DAO/ClientiDao.php");
include "../includes/SimpleImage.php";

$_IMAGE_PATH = "../../images/clienti/";
$_FINAL_IMAGE_PATH = "../../images/clienti/";

ini_set("memory_limit", "256M");

$action = $_REQUEST['action'];
 

switch ($action) {
		
    case 'getClientiList':
        $res = ClientiDao::getClientiList();
        echo json_encode( $res );
    break;

    case 'getCliente':
        $res = ClientiDao::getCliente($_REQUEST["idCliente"]);
        echo json_encode( $res );
    break;

    case 'deleteCliente':
        $res = ClientiDao::deleteCliente($_REQUEST['idCliente']);
        echo json_encode( "ok" );
    break;

    case 'insertCliente':
        $res = ClientiDao::insertCliente($_REQUEST['path'],$_REQUEST['nome'],$_REQUEST['sezione'],$_REQUEST['posizione']);
        echo json_encode( $res );
    break;

    case 'updateCliente':
        $res = ClientiDao::updateCliente($_REQUEST["idCliente"],$_REQUEST['path'],$_REQUEST['nome'],$_REQUEST['sezione'],$_REQUEST['posizione']);
        echo json_encode( $res );
    break;

    case 'updateColonneTestuali':
        $res = ClientiDao::updateColonneTestuali($_REQUEST["colonna1"],$_REQUEST['colonna2'],$_REQUEST['colonna3']);
        echo json_encode( $res );
    break;

    case 'uploadImage':
                    
        $pathFile = "";
           
        if($_FILES['selectedFile'] && $_FILES['selectedFile']['size'] > 0) { //se è stato passato un file
        
            if (!file_exists($_IMAGE_PATH)) { // se non esiste la cartella la creo
                mkdir($_IMAGE_PATH, 0777, true);
                 
            }

            //se esiste già un file devo prenderne il nome e sostituirlo
            if($_REQUEST["oldFile"]!=''){

                //fwrite($myfile, "scambio");
                $oldFile = $_REQUEST["oldFile"];

                if (file_exists($oldFile)) {
                    unlink($oldFile);
                }

                $file = $_FILES['selectedFile'];
                //@move_uploaded_file($file['tmp_name'], ".".$oldFile);


                if(@move_uploaded_file($file['tmp_name'],$oldFile)){                 
                    list($width, $height, $type, $attr) = getimagesize($oldFile);
                    $thumb = imagecreatetruecolor(200, 200);
                    $path_parts = pathinfo($oldFile);          
                    if($path_parts['extension'] == "png"){
                        imagealphablending($thumb, false); 
                        imagesavealpha($thumb, true);
                        $source = imagecreatefrompng($oldFile);
                        imagealphablending($source, true);
                        imagecopyresampled($thumb, $source, 0, 0, 0, 0, 200, 200, $width, $height);
                        imagepng($thumb,$oldFile);
                    }else{
                        $source = imagecreatefromjpeg($oldFile);
                        imagecopyresized($thumb, $source, 0, 0, 0, 0, 200, 200, $width, $height);
                        imagejpeg($thumb,$oldFile, 75);
                    }
                }

                //fwrite($myfile, $pathFile);
                $pathFile = $oldFile;

            }else{

                $pathinfo = pathinfo($_FILES['selectedFile']["name"]);   
                $extension = $pathinfo['extension'];
                  
                $nuovoFile = time() . "." . $extension;
                
                $pathFile =  $_FINAL_IMAGE_PATH . $nuovoFile;
                $file = $_FILES['selectedFile'];
               
                //fwrite($myfile, print_r($_FILES['selectedFile'],true));
                //fwrite($myfile, $pathFile);

               // @move_uploaded_file($file['tmp_name'], $_IMAGE_PATH . $nuovoFile);

                if(@move_uploaded_file($file['tmp_name'], $_IMAGE_PATH . $nuovoFile)){                 
                    list($width, $height, $type, $attr) = getimagesize($_IMAGE_PATH . $nuovoFile);
                    $thumb = imagecreatetruecolor(200, 200);
                    $path_parts = pathinfo($nuovoFile);          
                    if($path_parts['extension'] == "png"){
                        imagealphablending($thumb, false); 
                        imagesavealpha($thumb, true);
                        $source = imagecreatefrompng($_IMAGE_PATH . $nuovoFile);
                        imagealphablending($source, true);
                        imagecopyresampled($thumb, $source, 0, 0, 0, 0, 200, 200, $width, $height);
                        imagepng($thumb,$_IMAGE_PATH . $nuovoFile);
                    }else{
                        $source = imagecreatefromjpeg($_IMAGE_PATH . $nuovoFile);
                        imagecopyresized($thumb, $source, 0, 0, 0, 0, 200, 200, $width, $height);
                        imagejpeg($thumb,$_IMAGE_PATH . $nuovoFile, 75);
                    }
                }
            }

        }
               
        echo json_encode(
            array(
                "nuovoFile" => $pathFile
            )
        );     
          
    break;


}
?>