<?php

error_reporting(E_ERROR);
ini_set('display_errors', 1);

require_once("../DAO/TimelineDao.php");
include "../includes/SimpleImage.php";

$_IMAGE_PATH = "../../images/timeline/";
$_FINAL_IMAGE_PATH = "../../images/timeline/";

ini_set("memory_limit", "256M");

$action = $_REQUEST['action'];
 

switch ($action) {
		
    case 'getTimelineList':
        $res = TimelineDao::getTimelineList();
        echo json_encode( $res );
    break;

    case 'getTimeline':
        $res = TimelineDao::getTimeline($_REQUEST["idTimeline"]);
        echo json_encode( $res );
    break;

    case 'deleteTimeline':
        $res = TimelineDao::deleteTimeline($_REQUEST['idTimeline']);
        echo json_encode( "ok" );
    break;

    case 'insertTimeline':
        $res = TimelineDao::insertTimeline($_REQUEST['path'],$_REQUEST['link'],$_REQUEST['titoloIt'],$_REQUEST['titoloEn'],$_REQUEST['descrizioneIt'],$_REQUEST['descrizioneEn'],$_REQUEST['progressivo'],$_REQUEST['anno']);
        echo json_encode( $res );
    break;

    case 'updateTimeline':
        $res = TimelineDao::updateTimeline($_REQUEST["idTimeline"],$_REQUEST['path'],$_REQUEST['link'],$_REQUEST['titoloIt'],$_REQUEST['titoloEn'],$_REQUEST['descrizioneIt'],$_REQUEST['descrizioneEn'],$_REQUEST['progressivo'],$_REQUEST['anno']);
        echo json_encode( $res );
    break;

    case 'uploadImage':
                    
        $pathFile = "";
           
        if($_FILES['selectedFile'] && $_FILES['selectedFile']['size'] > 0) { //se è stato passato un file
        
            if (!file_exists($_IMAGE_PATH)) { // se non esiste la cartella la creo
                mkdir($_IMAGE_PATH, 0777, true);
                 
            }

            //se esiste già un file devo prenderne il nome e sostituirlo
            if($_REQUEST["oldFile"]!=''){

                //fwrite($myfile, "scambio");
                $oldFile = $_REQUEST["oldFile"];

                if (file_exists($oldFile)) {
                    unlink($oldFile);
                }

                $file = $_FILES['selectedFile'];
                //@move_uploaded_file($file['tmp_name'], ".".$oldFile);


                if(@move_uploaded_file($file['tmp_name'],$oldFile)){                 
                    list($width, $height, $type, $attr) = getimagesize($oldFile);
                    $thumb = imagecreatetruecolor(410, 230);
                    $path_parts = pathinfo($oldFile);          
                    if($path_parts['extension'] == "png"){
                        imagealphablending($thumb, false); 
                        imagesavealpha($thumb, true);
                        $source = imagecreatefrompng($oldFile);
                        imagealphablending($source, true);
                        imagecopyresampled($thumb, $source, 0, 0, 0, 0, 410, 230, $width, $height);
                        imagepng($thumb,$oldFile);
                    }else{
                        $source = imagecreatefromjpeg($oldFile);
                        imagecopyresized($thumb, $source, 0, 0, 0, 0, 410, 230, $width, $height);
                        imagejpeg($thumb,$oldFile, 75);
                    }
                }

                //fwrite($myfile, $pathFile);
                $pathFile = $oldFile;

            }else{

                $pathinfo = pathinfo($_FILES['selectedFile']["name"]);   
                $extension = $pathinfo['extension'];
                  
                $nuovoFile = time() . "." . $extension;
                
                $pathFile =  $_FINAL_IMAGE_PATH . $nuovoFile;
                $file = $_FILES['selectedFile'];
               

                //fwrite($myfile, print_r($_FILES['selectedFile'],true));
                //fwrite($myfile, $pathFile);

               // @move_uploaded_file($file['tmp_name'], $_IMAGE_PATH . $nuovoFile);

                if(@move_uploaded_file($file['tmp_name'], $_IMAGE_PATH . $nuovoFile)){                 
                    list($width, $height, $type, $attr) = getimagesize($_IMAGE_PATH . $nuovoFile);
                    $thumb = imagecreatetruecolor(410, 230);
                    $path_parts = pathinfo($nuovoFile);          
                    if($path_parts['extension'] == "png"){
                        imagealphablending($thumb, false); 
                        imagesavealpha($thumb, true);
                        $source = imagecreatefrompng($_IMAGE_PATH . $nuovoFile);
                        imagealphablending($source, true);
                        imagecopyresampled($thumb, $source, 0, 0, 0, 0, 410, 230, $width, $height);
                        imagepng($thumb,$_IMAGE_PATH . $nuovoFile);
                    }else{
                        $source = imagecreatefromjpeg($_IMAGE_PATH . $nuovoFile);
                        imagecopyresized($thumb, $source, 0, 0, 0, 0, 410, 230, $width, $height);
                        imagejpeg($thumb,$_IMAGE_PATH . $nuovoFile, 75);
                    }
                }
            }

        }
               
        echo json_encode(
            array(
                "nuovoFile" => $pathFile
            )
        );     
          
    break;


}
?>