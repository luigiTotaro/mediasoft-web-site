<?php

error_reporting(E_ERROR);
ini_set('display_errors', 1);

require_once("../DAO/ProdottiFrontoneDao.php");
include "../includes/SimpleImage.php";

$_IMAGE_PATH = "../../frontone/";
$_FINAL_IMAGE_PATH = "../../frontone/";

ini_set("memory_limit", "256M");

$action = $_REQUEST['action'];

switch ($action) {
		
    case 'getMediaByIdProdotto':
        $res = ProdottiFrontoneDao::getMediaByIdProdotto($_REQUEST["idProdotto"]);
        echo json_encode( $res );
    break;

    //Frontone
    case 'insertFrontone':
        $res = ProdottiFrontoneDao::insertFrontone($_REQUEST["idProdotto"],$_REQUEST['path'],$_REQUEST['titoloIt'],$_REQUEST['titoloEn']);
        echo json_encode( $res );
    break;

    case 'updateFrontone':
        $res = ProdottiFrontoneDao::updateFrontone($_REQUEST["idFrontone"],$_REQUEST["idProdotto"],$_REQUEST['path'],$_REQUEST['titoloIt'],$_REQUEST['titoloEn']);
        echo json_encode( $res );
    break;

    case 'deleteFrontone':

        $path = $_FINAL_IMAGE_PATH.$_REQUEST['path'];

        //elimino fisicamente il file
        $res = unlink($path);
        
        //cancello file in db
        if ($res==true)
        {
            $res = ProdottiFrontoneDao::deleteFrontone($_REQUEST['idFrontone']);
            echo json_encode( "ok" );
        }
        
    break;

    case 'uploadFrontone':
                    
        $pathFile = "";
           
        if($_FILES['selectedFileFrontone'] && $_FILES['selectedFileFrontone']['size'] > 0) { //se è stato passato un file
        
            if (!file_exists($_IMAGE_PATH)) { // se non esiste la cartella la creo
                mkdir($_IMAGE_PATH, 0777, true);
                 
            }

            //se esiste già un file devo prenderne il nome e sostituirlo
            if($_REQUEST["oldFileFrontone"]!=''){

                //fwrite($myfile, "scambio");
                $oldFile = $_REQUEST["oldFileAllegato"];

                if (file_exists($oldFile)) {
                    unlink($oldFile);
                }

                $file = $_FILES['selectedFileFrontone'];
                @move_uploaded_file($file['tmp_name'], $oldFile);
                //fwrite($myfile, $pathFile);
                $pathFile = $oldFile;

            }else{

                $pathinfo = pathinfo($_FILES['selectedFileFrontone']["name"]);   
                $extension = $pathinfo['extension'];
                  
                $nuovoFile = time() . rand(1, 10000) . "." . $extension;
                
                $pathFile =  $_IMAGE_PATH . $nuovoFile;
                $file = $_FILES['selectedFileFrontone'];
 
                @move_uploaded_file($file['tmp_name'], $_IMAGE_PATH . $nuovoFile);
            }

        }
               
        echo json_encode(
            array(
                "nuovoFile" => $pathFile
            )
        );     
          
    break;



}
?>