<?php

//error_reporting(E_ERROR);
//ini_set('display_errors', 1);

require_once("../DAO/ProdottiDao.php");
include "../includes/SimpleImage.php";

$_IMAGE_PATH = "../../images/";
$_FINAL_IMAGE_PATH = "../../images/";

$_IMAGE_PATH_PRODUCT = "../../allegati/";

ini_set("memory_limit", "256M");

$action = $_REQUEST['action'];

switch ($action) {
		
    case 'getProdottiList':
        $res = ProdottiDao::getProdottiList();
        echo json_encode( $res );
    break;

    case 'getProdotto':
        $res = ProdottiDao::getProdotto($_REQUEST["idProdotto"]);
        echo json_encode( $res );
    break;

    case 'getMediaByIdProdotto':
        $res = ProdottiDao::getMediaByIdProdotto($_REQUEST["idProdotto"]);
        echo json_encode( $res );
    break;

    case 'getAllegatiByIdProdotto':
        $res = ProdottiDao::getAllegatiByIdProdotto($_REQUEST["idProdotto"]);
        echo json_encode( $res );
    break;

    case 'deleteProdotto':
        $res = ProdottiDao::deleteProdotto($_REQUEST['idProdotto']);
        echo json_encode( "ok" );
    break;

    case 'insertProdotto':
        $res = ProdottiDao::insertProdotto($_REQUEST['path1'],$_REQUEST['path2'],$_REQUEST['path3'],$_REQUEST['path4'],
            $_REQUEST['nome'],$_REQUEST['link'],$_REQUEST['titoloIt'],$_REQUEST['titoloEn'],$_REQUEST['descrizioneIt'],$_REQUEST['descrizioneEn'],
            $_REQUEST['menuIt'],$_REQUEST['menuEn'],$_REQUEST['tipoLayout'],$_REQUEST['posizione']);
        echo json_encode( $res );
    break;

    case 'updateProdotto':
        $res = ProdottiDao::updateProdotto($_REQUEST["idProdotto"],$_REQUEST['path1'],$_REQUEST['path2'],$_REQUEST['path3'],$_REQUEST['path4'],
            $_REQUEST['nome'],$_REQUEST['link'],$_REQUEST['titoloIt'],$_REQUEST['titoloEn'],$_REQUEST['descrizioneIt'],$_REQUEST['descrizioneEn'],
            $_REQUEST['menuIt'],$_REQUEST['menuEn'],$_REQUEST['tipoLayout'],$_REQUEST['posizione']);
        echo json_encode( $res );
    break;

    case 'insertAllegato':
        $res = ProdottiDao::insertAllegato($_REQUEST["idProdotto"],$_REQUEST['path'],$_REQUEST['titoloIt'],$_REQUEST['titoloEn'],$_REQUEST['tipoAllegato'],$_REQUEST['tipoIcona'],$_REQUEST['posizione']);
        echo json_encode( $res );
    break;

    case 'updateAllegato':
        $res = ProdottiDao::updateAllegato($_REQUEST["idAllegato"],$_REQUEST["idProdotto"],$_REQUEST['path'],$_REQUEST['titoloIt'],$_REQUEST['titoloEn'],$_REQUEST['tipoAllegato'],$_REQUEST['tipoIcona'],$_REQUEST['posizione']);
        echo json_encode( $res );
    break;

    case 'deleteAllegato':
        $res = ProdottiDao::deleteAllegato($_REQUEST['idAllegato']);
        echo json_encode( "ok" );
    break;

    //Screenshot
    case 'insertScreenshot':
        $res = ProdottiDao::insertScreenshot($_REQUEST["idProdotto"],$_REQUEST['path'],$_REQUEST['titoloIt'],$_REQUEST['titoloEn']);
        echo json_encode( $res );
    break;

    case 'updateScreenshot':
        $res = ProdottiDao::updateScreenshot($_REQUEST["idScreenshot"],$_REQUEST["idProdotto"],$_REQUEST['path'],$_REQUEST['titoloIt'],$_REQUEST['titoloEn']);
        echo json_encode( $res );
    break;

    case 'deleteScreenshot':
        $res = ProdottiDao::deleteScreenshot($_REQUEST['idScreenshot']);
        echo json_encode( "ok" );
    break;

    case 'getIconeListByProdotto':
        $res = ProdottiDao::getIconeListByProdotto($_REQUEST["idProdotto"]);
        echo json_encode( $res );
    break;

    case 'insertIcon':
        $res = ProdottiDao::insertIcon($_REQUEST["idProdotto"],$_REQUEST["idIcona"]);
        echo json_encode( $res );
    break;

    case 'removeIcon':
        $res = ProdottiDao::removeIcon($_REQUEST["idProdotto"],$_REQUEST["idIcona"]);
        echo json_encode( $res );
    break;
    //fine Screenshot

    case 'uploadImage1':
                    
        $pathFile = "";
           
        if($_FILES['selectedFile1'] && $_FILES['selectedFile1']['size'] > 0) { //se è stato passato un file
        
            if (!file_exists($_IMAGE_PATH)) { // se non esiste la cartella la creo
                mkdir($_IMAGE_PATH, 0777, true);
                 
            }

            //se esiste già un file devo prenderne il nome e sostituirlo
            /*if($_REQUEST["oldFile1"]!=''){

                //fwrite($myfile, "scambio");
                $oldFile = $_REQUEST["oldFile1"];

                if (file_exists($oldFile)) {
                    unlink($oldFile);
                }

                $file = $_FILES['selectedFile1'];
                @move_uploaded_file($file['tmp_name'], $oldFile);
                //fwrite($myfile, $pathFile);
                $pathFile = $oldFile;

            }else{*/

                $pathinfo = pathinfo($_FILES['selectedFile1']["name"]);   
                $extension = $pathinfo['extension'];
                  
                $nuovoFile = time() . rand(1, 10000) . "." . $extension;
                
                $pathFile =  $_FINAL_IMAGE_PATH . $nuovoFile;
                $file = $_FILES['selectedFile1'];

                @move_uploaded_file($file['tmp_name'], $_IMAGE_PATH . $nuovoFile);
            //}

        }
               
        echo json_encode(
            array(
                "nuovoFile" => $pathFile
            )
        );     
          
    break;

    case 'uploadImage2':
                    
        $pathFile = "";
           
        if($_FILES['selectedFile2'] && $_FILES['selectedFile2']['size'] > 0) { //se è stato passato un file
        
            if (!file_exists($_IMAGE_PATH)) { // se non esiste la cartella la creo
                mkdir($_IMAGE_PATH, 0777, true);
                 
            }

            //se esiste già un file devo prenderne il nome e sostituirlo
            if($_REQUEST["oldFile2"]!=''){

                //fwrite($myfile, "scambio");
                $oldFile = $_REQUEST["oldFile2"];

                if (file_exists($oldFile)) {
                    unlink($oldFile);
                }

                $file = $_FILES['selectedFile2'];
                @move_uploaded_file($file['tmp_name'], $oldFile);
                //fwrite($myfile, $pathFile);
                $pathFile = $oldFile;

            }else{

                $pathinfo = pathinfo($_FILES['selectedFile2']["name"]);   
                $extension = $pathinfo['extension'];
                  
                $nuovoFile = time() . rand(1, 10000) . "." . $extension;
                
                $pathFile =  $_FINAL_IMAGE_PATH . $nuovoFile;
                $file = $_FILES['selectedFile2'];

                @move_uploaded_file($file['tmp_name'], $_IMAGE_PATH . $nuovoFile);
            }

        }
               
        echo json_encode(
            array(
                "nuovoFile" => $pathFile
            )
        );     
          
    break;

    case 'uploadImage3':
                    
        $pathFile = "";
           
        if($_FILES['selectedFile3'] && $_FILES['selectedFile3']['size'] > 0) { //se è stato passato un file
        
            if (!file_exists($_IMAGE_PATH)) { // se non esiste la cartella la creo
                mkdir($_IMAGE_PATH, 0777, true);
                 
            }

            //se esiste già un file devo prenderne il nome e sostituirlo
            if($_REQUEST["oldFile3"]!=''){

                //fwrite($myfile, "scambio");
                $oldFile = $_REQUEST["oldFile3"];

                if (file_exists($oldFile)) {
                    unlink($oldFile);
                }

                $file = $_FILES['selectedFile3'];
                @move_uploaded_file($file['tmp_name'], $oldFile);
                //fwrite($myfile, $pathFile);
                $pathFile = $oldFile;

            }else{

                $pathinfo = pathinfo($_FILES['selectedFile3']["name"]);   
                $extension = $pathinfo['extension'];
                  
                $nuovoFile = time() . rand(1, 10000) . "." . $extension;
                
                $pathFile =  $_FINAL_IMAGE_PATH . $nuovoFile;
                $file = $_FILES['selectedFile3'];

                @move_uploaded_file($file['tmp_name'], $_IMAGE_PATH . $nuovoFile);
            }

        }
               
        echo json_encode(
            array(
                "nuovoFile" => $pathFile
            )
        );     
          
    break;

    case 'uploadImage4':
                    
        $pathFile = "";
           
        if($_FILES['selectedFile4'] && $_FILES['selectedFile4']['size'] > 0) { //se è stato passato un file
        
            if (!file_exists($_IMAGE_PATH)) { // se non esiste la cartella la creo
                mkdir($_IMAGE_PATH, 0777, true);
                 
            }

            //se esiste già un file devo prenderne il nome e sostituirlo
            if($_REQUEST["oldFile4"]!=''){

                //fwrite($myfile, "scambio");
                $oldFile = $_REQUEST["oldFile4"];

                if (file_exists($oldFile)) {
                    unlink($oldFile);
                }

                $file = $_FILES['selectedFile4'];
                @move_uploaded_file($file['tmp_name'], $oldFile);
                //fwrite($myfile, $pathFile);
                $pathFile = $oldFile;

            }else{

                $pathinfo = pathinfo($_FILES['selectedFile4']["name"]);   
                $extension = $pathinfo['extension'];
                  
                $nuovoFile = time() . rand(1, 10000) . "." . $extension;
                
                $pathFile =  $_FINAL_IMAGE_PATH . $nuovoFile;
                $file = $_FILES['selectedFile4'];
 
                @move_uploaded_file($file['tmp_name'], $_IMAGE_PATH . $nuovoFile);
            }

        }
               
        echo json_encode(
            array(
                "nuovoFile" => $pathFile
            )
        );     
          
    break;

    case 'uploadAllegato':
                    
        $pathFile = "";
           
        if($_FILES['selectedFileAllegato'] && $_FILES['selectedFileAllegato']['size'] > 0) { //se è stato passato un file
        
            if (!file_exists($_IMAGE_PATH_PRODUCT)) { // se non esiste la cartella la creo
                mkdir($_IMAGE_PATH_PRODUCT, 0777, true);
                 
            }

            //se esiste già un file devo prenderne il nome e sostituirlo
            if($_REQUEST["oldFileAllegato"]!=''){

                //fwrite($myfile, "scambio");
                $oldFile = $_REQUEST["oldFileAllegato"];

                if (file_exists($oldFile)) {
                    unlink($oldFile);
                }

                $file = $_FILES['selectedFileAllegato'];
                @move_uploaded_file($file['tmp_name'], $oldFile);
                //fwrite($myfile, $pathFile);
                $pathFile = $oldFile;

            }else{

                $pathinfo = pathinfo($_FILES['selectedFileAllegato']["name"]);   
                $extension = $pathinfo['extension'];
                  
                $nuovoFile = time() . rand(1, 10000) . "." . $extension;
                
                $pathFile =  $_IMAGE_PATH_PRODUCT . $nuovoFile;
                $file = $_FILES['selectedFileAllegato'];
 
                @move_uploaded_file($file['tmp_name'], $_IMAGE_PATH_PRODUCT . $nuovoFile);
            }

        }
               
        echo json_encode(
            array(
                "nuovoFile" => $pathFile
            )
        );     
          
    break;

    case 'uploadScreenshot':
                    
        $pathFile = "";
           
        if($_FILES['selectedFileScreenshot'] && $_FILES['selectedFileScreenshot']['size'] > 0) { //se è stato passato un file
        
            if (!file_exists($_IMAGE_PATH)) { // se non esiste la cartella la creo
                mkdir($_IMAGE_PATH, 0777, true);
                 
            }

            //se esiste già un file devo prenderne il nome e sostituirlo
            if($_REQUEST["oldFileScreenshot"]!=''){

                //fwrite($myfile, "scambio");
                $oldFile = $_REQUEST["oldFileAllegato"];

                if (file_exists($oldFile)) {
                    unlink($oldFile);
                }

                $file = $_FILES['selectedFileScreenshot'];
                @move_uploaded_file($file['tmp_name'], $oldFile);
                //fwrite($myfile, $pathFile);
                $pathFile = $oldFile;

            }else{

                $pathinfo = pathinfo($_FILES['selectedFileScreenshot']["name"]);   
                $extension = $pathinfo['extension'];
                  
                $nuovoFile = time() . rand(1, 10000) . "." . $extension;
                
                $pathFile =  $_IMAGE_PATH . $nuovoFile;
                $file = $_FILES['selectedFileScreenshot'];
 
                @move_uploaded_file($file['tmp_name'], $_IMAGE_PATH . $nuovoFile);
            }

        }
               
        echo json_encode(
            array(
                "nuovoFile" => $pathFile
            )
        );     
          
    break;



}
?>