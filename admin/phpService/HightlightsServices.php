<?php

error_reporting(E_ERROR);
ini_set('display_errors', 1);

require_once("../DAO/HightlightsDao.php");
include "../includes/SimpleImage.php";

$_IMAGE_PATH = "../../images/";
$_FINAL_IMAGE_PATH = "../../images/";

ini_set("memory_limit", "256M");

$action = $_REQUEST['action'];
 

switch ($action) {
		
    case 'getHightlightsList':
        $res = HightlightsDao::getHightlightsList();
        echo json_encode( $res );
    break;

    case 'getHightlight':
        $res = HightlightsDao::getHightlight($_REQUEST["idHightlight"]);
        echo json_encode( $res );
    break;

    case 'deleteHightlight':
        $res = HightlightsDao::deleteHightlight($_REQUEST['idHightlight']);
        echo json_encode( "ok" );
    break;

    case 'insertHightlight':
        $res = HightlightsDao::insertHightlight($_REQUEST['path'],$_REQUEST['nome'],$_REQUEST['link'],$_REQUEST['titoloIt'],$_REQUEST['titoloEn'],$_REQUEST['descrizioneIt'],$_REQUEST['descrizioneEn'],$_REQUEST['posizione'],$_REQUEST['abilitata']);
        echo json_encode( $res );
    break;

    case 'updateHightlight':
        $res = HightlightsDao::updateHightlight($_REQUEST["idHightlight"],$_REQUEST['path'],$_REQUEST['nome'],$_REQUEST['link'],$_REQUEST['titoloIt'],$_REQUEST['titoloEn'],$_REQUEST['descrizioneIt'],$_REQUEST['descrizioneEn'],$_REQUEST['posizione'],$_REQUEST['abilitata']);
        echo json_encode( $res );
    break;

    case 'uploadImage':
                    
        $pathFile = "";
           
        if($_FILES['selectedFile'] && $_FILES['selectedFile']['size'] > 0) { //se è stato passato un file
        
            if (!file_exists($_IMAGE_PATH)) { // se non esiste la cartella la creo
                mkdir($_IMAGE_PATH, 0777, true);
                 
            }

            //se esiste già un file devo prenderne il nome e sostituirlo
            if($_REQUEST["oldFile"]!=''){

                //fwrite($myfile, "scambio");
                $oldFile = $_REQUEST["oldFile"];

                if (file_exists($oldFile)) {
                    unlink($oldFile);
                }

                $file = $_FILES['selectedFile'];
                @move_uploaded_file($file['tmp_name'], $oldFile);
                //fwrite($myfile, $pathFile);
                $pathFile = $oldFile;

            }else{

                $pathinfo = pathinfo($_FILES['selectedFile']["name"]);   
                $extension = $pathinfo['extension'];
                  
                $nuovoFile = time() . "." . $extension;
                
                $pathFile =  $_FINAL_IMAGE_PATH . $nuovoFile;
                $file = $_FILES['selectedFile'];
               

                //fwrite($myfile, print_r($_FILES['selectedFile'],true));
                //fwrite($myfile, $pathFile);

                @move_uploaded_file($file['tmp_name'], $_IMAGE_PATH . $nuovoFile);
            }

        }
               
        echo json_encode(
            array(
                "nuovoFile" => $pathFile
            )
        );     
          
    break;


}
?>