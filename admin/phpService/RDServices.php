<?php

error_reporting(E_ERROR);
ini_set('display_errors', 1);

require_once("../DAO/RDDao.php");

ini_set("memory_limit", "256M");

$action = $_REQUEST['action'];

switch ($action) {
		
    case 'getRDList':
        $res = RDDao::getRDList();
        echo json_encode( $res );
    break;

    case 'getRD':
        $res = RDDao::getRD($_REQUEST["idRD"]);
        echo json_encode( $res );
    break;

    case 'deleteRD':
        $res = RDDao::deleteRD($_REQUEST['idRD']);
        echo json_encode( "ok" );
    break;

    case 'insertRD':
        $res = RDDao::insertRD($_REQUEST['link'],$_REQUEST['titoloIt'],$_REQUEST['titoloEn'],
            $_REQUEST['autoreIt'],$_REQUEST['autoreEn'],
            $_REQUEST['testoIt'],$_REQUEST['testoEn'],$_REQUEST['progressivo']);
        echo json_encode( $res );
    break;

    case 'updateRD':
        $res = RDDao::updateRD($_REQUEST["idRD"],$_REQUEST['link'],$_REQUEST['titoloIt'],$_REQUEST['titoloEn'],
            $_REQUEST['autoreIt'],$_REQUEST['autoreEn'],
            $_REQUEST['testoIt'],$_REQUEST['testoEn'],$_REQUEST['progressivo']);
        echo json_encode( $res );
    break;
    
}
?>