var moduloTimelineDetail = Backbone.View.extend({

    tagName : "div",
    className: '',
    update: null,
    currentIdTimeline: 0,
    canWrite: null,
    pathImage: '',
    tipoFile: '2',
    modalBootbox: null,
    attributes: {
        'id': 'div_moduloTimelineDetail'
    },

    events: {
       
    },

    initialize: function(options) {
     
        this.update = options.update;  
        this.canWrite = options.canWrite;

        var me=this;  

        if(this.update==true){
            this.currentIdTimeline = options.idTimeline;
            TimelineController.getTimeline({
                idTimeline:  options.idTimeline,
                callback: function(jsonData) { 
                    me.render(jsonData);
                } 
            });
        }else
            this.render();
            
    },


    render : function(jsonData) {

           var me = this;

           var today = new Date();
           var yyyy = today.getFullYear();

           //console.log(jsonData)

           if(jsonData!=null){
                me.pathImage = jsonData["path"];
           }else{
                me.pathImage = "";
           }
        
           var html = '<form method="post" class="horizontal-form" id="formNuovoTimeline >'+
                        '<div class="form-body">'+  

                            '<div class="row">'+
                                '<div class="col-md-12">'+
                                    '<div class="form-group">'+
                                        '<input id="selectedFile" name="selectedFile" class="file" type="file" style="display:none;">'+
                                        '<span style="font-size:80%">Sono supportati solo file di tipo "jpg, png, gif". Dimensione max '+(this.tipoFile==2 ?  "1 MB." : "5 MB.")+'</span><br>'+
                                        '<span style="font-size:80%;color: #C11B17;">Attenzione! Per mantenere la visualizzazione corretta dell\'elemento, il sistema ridimensiona in automatico con misure 410px per 230px. Si consiglia di caricare un\'immagine di questa proporzione.</span><br>'+
                                        '<span class="text-danger error" id="imgError" ></span>'+
                                    '</div>'+
                                '</div>'+    
                            '</div>'+    

                            '<div class="row">'+
                                '<div class="col-md-2">'+
                                    '<label class="control-label">Link </label>'+
                                '</div>'+
                                '<div class="col-md-10">'+
                                   '<div class="form-group">'+
                                        '<input type="text" id="txtLink" name="txtLink" placeholder="Link ..." value="' + (jsonData!=null ? jsonData["link"] : "") + '" class="form-control">'+
                                        '<span class="text-danger error" id="linkError" ></span>'+
                                    '</div>'+
                                '</div>'+                                            
                            '</div>'+  

                            '<div class="row">'+
                                '<div class="col-md-2">'+
                                    '<label class="control-label">Progressivo:</label>'+
                                '</div>'+
                                '<div class="col-md-4">'+
                                    '<div id="txtProgressivo">'+ 
                                        '<div class="input-group" style="width:150px;margin-bottom:10px;">'+ 
                                            '<input type="text" id="txtProgressivoVal" value="' + (jsonData!=null ? jsonData["progressivo"] : "1") + '" class="spinner-input form-control" maxlength="3" >'+ 
                                            '<div class="spinner-buttons input-group-btn">'+ 
                                                '<button type="button" class="btn spinner-up default">'+ 
                                                '<i class="fa fa-angle-up"></i>'+ 
                                                '</button>'+ 
                                                '<button type="button" class="btn spinner-down default">'+ 
                                                '<i class="fa fa-angle-down"></i>'+ 
                                                '</button>'+ 
                                            '</div>'+ 
                                        '</div>'+ 
                                    '</div>'+ 
                                '</div>'+  

                                '<div class="col-md-2">'+
                                    '<label class="control-label">Anno: </label>'+
                                '</div>'+
                                '<div class="col-md-4">'+
                                    '<div id="txtAnno">'+ 
                                        '<div class="input-group" style="width:150px;margin-bottom:10px;">'+ 
                                            '<input type="text" id="txtAnnoVal" value="' + (jsonData!=null ? jsonData["anno"] : yyyy) + '" class="spinner-input form-control" maxlength="4" >'+ 
                                            '<div class="spinner-buttons input-group-btn">'+ 
                                                '<button type="button" class="btn spinner-up default">'+ 
                                                '<i class="fa fa-angle-up"></i>'+ 
                                                '</button>'+ 
                                                '<button type="button" class="btn spinner-down default">'+ 
                                                '<i class="fa fa-angle-down"></i>'+ 
                                                '</button>'+ 
                                            '</div>'+ 
                                        '</div>'+ 
                                    '</div>'+ 
                                '</div>'+ 

                            '</div>'+  
         
                            '<div class="row">'+
                                '<div class="col-md-2">'+
                                    '<label class="control-label">Titolo IT </label>'+
                                '</div>'+
                                '<div class="col-md-10">'+
                                   '<div class="form-group">'+
                                        '<input type="text" id="txtTitoloIt"  name="txtTitoloIt"  placeholder="Titolo IT..."  value="' + (jsonData!=null ? jsonData["titoloIT"] : "") + '" class="form-control">'+
                                         '<span class="text-danger error" id="titoloItError" ></span>'+
                                 '</div>'+
                                '</div>'+  
                            '</div>'+

                            '<div class="row">'+
                                '<div class="col-md-2">'+
                                    '<label class="control-label">Titolo EN </label>'+
                                '</div>'+
                                '<div class="col-md-10">'+
                                   '<div class="form-group">'+
                                        '<input type="text" id="txtTitoloEn" name="txtTitoloEn" placeholder="Titolo EN..." value="' + (jsonData!=null ? jsonData["titoloEN"] : "") + '" class="form-control">'+
                                        '<span class="text-danger error" id="titoloEnError" ></span>'+
                                    '</div>'+
                                '</div>'+                                            
                            '</div>'+  

                            '<div class="row">'+
                                '<div class="col-md-2">'+
                                    '<label class="control-label">Descrizione IT </label>'+
                                '</div>'+
                                '<div class="col-md-10">'+
                                    '<div class="form-group">'+
                                        '<textarea id="txtDescrizioneIt" maxlength="383" name="txtDescrizioneIt"  class="form-control"  rows="3" style="resize: none;"  placeholder="descrizione it...">'+
                                        (jsonData!=null ? jsonData["descrizioneIT"] : "")
                                        +'</textarea>'+
                                        '<span class="text-danger error" id="" >Max 400 caretteri</span>'+
                                    '</div>'+
                                '</div>'+    
                            '</div>'+  

                            '<div class="row">'+
                                '<div class="col-md-2">'+
                                    '<label class="control-label">Descrizione EN </label>'+
                                '</div>'+
                                '<div class="col-md-10">'+
                                    '<div class="form-group">'+
                                        '<textarea id="txtDescrizioneEn" maxlength="383" name="txtDescrizioneEn"  class="form-control"  rows="3" style="resize: none;"  placeholder="descrizione en...">'+
                                       (jsonData!=null ? jsonData["descrizioneEN"] : "")
                                        +'</textarea>'+
                                        '<span class="text-danger error" id="" >Max 400 caretteri</span>'+
                                    '</div>'+
                                '</div>'+    
                            '</div>'+  
                            
                        '</div>' +
                   ' </form>';

            me.modalBootbox = bootbox.dialog({
                closeButton: false,
                className: "modal70",
                message: html,
                title: (me.update==true ?  "Modifica elemento Timeline " : "Inserimento Nuovo Elemento Timeline"),
                buttons: {
                    annulla: {
                      label: "Annulla",
                      className: "btn-success",
                      callback: function() {
                        $('#btNuovoTlItem').removeClass('disabled');
                        $('.actionButton').removeClass('disabled');
                      }
                    },                      
                    conferma: {
                      label: "Conferma",
                      className: "btn-primary",
                      callback: function() {

                            var nomeFile = $('#selectedFile').val();

                            /*if(jsonData!=null){
                                me.pathImage = jsonData["path"];
                            }else{
                                me.pathImage = "";
                            }*/

                            if(nomeFile==""){
                                //vuol dire che sto modificando solo il contenuto
                                me.insertOrUpdate();
                            }else{  
                                //effettuo l'upload prima di salvare
                                me.innerUpload();
                            }
        
                            return false;

                       }//chiude callback
                    }//chiude conferma
                }//chiude button
            });//chiude dialog

            $('#txtProgressivo').spinner({value:(jsonData!=null ? jsonData["progressivo"] : 1), min: 1});
            $('#txtAnno').spinner({value:(jsonData!=null ? jsonData["anno"] : yyyy), min: 1, max: 2100});    

            var _initialPreview = [];
            var _allowedFileExtensions = [];
            var _maxFileSize="";
          
            if(this.tipoFile=='2'){

                if(me.update==true){
                    _initialPreview = [ "<img src='../images/"+(jsonData!=null ? jsonData["path"] : "")+"?time="+(new Date().getTime())+"' class='file-preview-image' >" ];
                }

                //_allowedFileTypes : ['image'];
                _allowedFileExtensions = ['jpg', 'png','gif'];
                _maxFileSize= 1000;
               
            }else{
                if(me.update==true){
                    _initialPreview = ["<video controls class='file-preview-other' style='height:160px;'>" + 
                                         "<source width='200px' height='200px' src='" + (jsonData!=null ? jsonData["path"] : "") + "' type='video/mp4'></video>"];
                } 
                _allowedFileExtensions =['mp4'];
                _maxFileSize= 5000;  
            }

            $("#selectedFile").fileinput({
                allowedFileExtensions : _allowedFileExtensions,
                uploadAsync: true,
                minFileCount: 0,
                maxFileCount: 1,
                maxFileSize: _maxFileSize,
                removeLabel: 'Rimuovi',
                removeTitle: 'Rimuovi file selezionato',
                cancelLabel: 'Annulla',
                cancelTitle: 'Annulla upload',
                uploadLabel: 'Upload',
                uploadTitle: 'Upload file selezionato',
                browseLabel: 'Seleziona',
                msgSelected: '{n} files selezionato',
                dropZoneEnabled: false,
                msgInvalidFileType: 'Formato del file "{name}" non supportato. Sono supportati solo file di tipo "{types}".',
                msgInvalidFileExtension: 'Estensione del file "{name}" non supportata. Sono supportati solo file di tipo "{extensions}".',
                msgSizeTooLarge:'La dimesione del file "{name}" (<b>{size} KB</b>) eccede il limite imposto <b>{maxSize} KB</b>.',
                uploadUrl: './phpService/TimelineServices.php?action=uploadImage&oldFile='+(jsonData!=null ? "../../images/"+jsonData["path"] : ""),
                initialPreview: _initialPreview,
                layoutTemplates: {
                
                    main1: '{preview}\n' +
                            '<div class="kv-upload-progress hide"></div>\n' +
                            '<div class="input-group {class}">\n' +
                            '   {caption}\n' +
                            '   <div class="input-group-btn">\n' +
                            '       <a id="btnEmptyFileManager" href="#" class="btn btn-default"><i class="fa fa-trash-o"></i> Rimuovi</a>\n' +
                            '       <a id="btnAddFileManager" href="#" class="btn btn-default"><i class="fa fa-upload"></i> Seleziona un file</a>\n' +
                            '   </div>\n' +
                            '</div>',
                            
                    
                    preview: 
                            '<div class="file-preview {class}"  style="height:220px;">\n' +              
                            '    <div class="{dropClass}" style="border:none; padding: 3px; margin: 0px;">\n' +
                            '    <div class="file-preview-thumbnails">\n' +
                            '    </div>\n' +
                            '    <div class="clearfix"></div>' +
                            '    <div class="kv-fileinput-error"></div>\n' +
                            '    </div>\n' +
                            '</div>',
                            
                    icon: '<span class="glyphicon glyphicon-picture kv-caption-icon"></span>',
                    
                    actions: '',
                    footer: ''
                }
                
            }).on('fileuploaded', function(event, data, previewId, index) {
                    
                $("#selectedFile").attr('data-name', data.response.nuovoFile);

                var newFile = data.response.nuovoFile;
                me.pathImage = newFile.replace("../../images/", "");

                me.insertOrUpdate();

                //me.outerCallback();
                            
            }).on('filebatchuploadsuccess', function(event, data, previewId, index) {

                $("#selectedFile").attr('data-name', data.response.nuovoFile);

                var newFile = data.response.nuovoFile;
                me.pathImage = newFile.replace("../../images/", "");

                me.insertOrUpdate();

                //me.outerCallback();
                
            }).on('filecleared', function(event) {

                //se rimuovo una immagine esistente non faccio upload ma devo rimuovere il file e aggiornare la path
                //console.log("rimuovo")
                me.pathImage = "";
                
            });
           
            $('#btnEmptyFileManager').click(function(evt) {
                $("#selectedFile").fileinput('clear');
            });

            $('#btnAddFileManager').click(function(evt) {
                $("#selectedFile").click();
            });

    },
    innerUpload: function(){
        //this.outerCallback = outCb;
        $("#selectedFile").fileinput('upload');
    },
    insertOrUpdate: function(){

        var me = this;
        var path = me.pathImage;
        var link = $('#txtLink').val();
        var titoloIt = $('#txtTitoloIt').val();
        var titoloEn = $('#txtTitoloEn').val();
        var descrizioneIt = $('#txtDescrizioneIt').val();
        var descrizioneEn = $('#txtDescrizioneEn').val();

        var today = new Date();
        var yyyy = today.getFullYear();

        var progressivo = ($('#txtProgressivoVal').val()!='' ? $('#txtProgressivoVal').val() : 1);
        var anno = ($('#txtAnnoVal').val()!='' ? $('#txtAnnoVal').val() : yyyy);

        $('.error').empty();
        var errori = false;
       
        if(path == "") {
            $('#imgError').append('<em>(Inserire un immagine per l\'elemento)</em>');
            errori = true;
        }

        if(errori)
            return false;


        TimelineController.insertOrUpdateTimeline({
            path: path,
            link: link,
            titoloIt: titoloIt,
            titoloEn: titoloEn,
            descrizioneIt: descrizioneIt,
            descrizioneEn: descrizioneEn,
            progressivo: progressivo,
            anno: anno,
            update: me.update,
            idTimeline: me.currentIdTimeline,
            callback: function(utente_id){

                $('#btNuovoTlItem').removeClass('disabled');
                $('.actionButton').removeClass('disabled');
                me.modalBootbox.modal("hide");

                new moduloTimeline({
                    canWrite: true
                });

            }

        });
                                  
        return false;
    }
});

