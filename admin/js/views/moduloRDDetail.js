var moduloRDDetail = Backbone.View.extend({

    tagName : "div",
    className: '',
    update: null,
    currentIdRD: 0,
    canWrite: null,
    pathImage: '',
    tipoFile: '2',
    modalBootbox: null,
    attributes: {
        'id': 'div_moduloRDDetail'
    },

    events: {
       
    },

    initialize: function(options) {
     
        this.update = options.update;  
        this.canWrite = options.canWrite;

        var me=this;  

        if(this.update==true){
            this.currentIdRD = options.idRD;
            RDController.getRD({
                idRD:  options.idRD,
                callback: function(jsonData) { 
                    me.render(jsonData);
                } 
            });
        }else
            this.render();
            
    },


    render : function(jsonData) {

           var me = this;

           //console.log(jsonData)
        
           var html = '<form method="post" class="horizontal-form" id="formNuovoRD >'+
                        '<div class="form-body">'+  

                            '<div class="row">'+

                                '<div class="col-md-2">'+
                                    '<label class="control-label">Progressivo:</label>'+
                                '</div>'+
                                '<div class="col-md-2">'+
                                    '<div id="txtProgressivo">'+ 
                                        '<div class="input-group" style="width:100%;margin-bottom:10px;">'+ 
                                            '<input type="text" id="txtProgressivoVal" value="' + (jsonData!=null ? jsonData["progressivo"] : "1") + '" class="spinner-input form-control" maxlength="3" >'+ 
                                            '<div class="spinner-buttons input-group-btn">'+ 
                                                '<button type="button" class="btn spinner-up default">'+ 
                                                '<i class="fa fa-angle-up"></i>'+ 
                                                '</button>'+ 
                                                '<button type="button" class="btn spinner-down default">'+ 
                                                '<i class="fa fa-angle-down"></i>'+ 
                                                '</button>'+ 
                                            '</div>'+ 
                                        '</div>'+ 
                                    '</div>'+ 
                                '</div>'+ 

                                '<div class="col-md-1">'+
                                    '<label class="control-label">Link </label>'+
                                '</div>'+
                                '<div class="col-md-7">'+
                                   '<div class="form-group">'+
                                        '<input type="text" id="txtLink" name="txtLink" placeholder="Link ..." value="' + (jsonData!=null ? jsonData["link"] : "") + '" class="form-control">'+
                                        '<span class="text-danger error" id="linkError" ></span>'+
                                    '</div>'+
                                '</div>'+                                            

                            '</div>'+  
         
                            '<div class="row">'+
                                '<div class="col-md-2">'+
                                    '<label class="control-label">Titolo IT (*) </label>'+
                                '</div>'+
                                '<div class="col-md-10">'+
                                   '<div class="form-group">'+
                                        '<input type="text" id="txtTitoloIt"  name="txtTitoloIt"  placeholder="Titolo IT..."  value="' + (jsonData!=null ? jsonData["titoloIT"] : "") + '" class="form-control">'+
                                         '<span class="text-danger error" id="titoloItError" ></span>'+
                                 '</div>'+
                                '</div>'+  
                            '</div>'+

                            '<div class="row">'+
                                '<div class="col-md-2">'+
                                    '<label class="control-label">Titolo EN </label>'+
                                '</div>'+
                                '<div class="col-md-10">'+
                                   '<div class="form-group">'+
                                        '<input type="text" id="txtTitoloEn" name="txtTitoloEn" placeholder="Titolo EN..." value="' + (jsonData!=null ? jsonData["titoloEN"] : "") + '" class="form-control">'+
                                        '<span class="text-danger error" id="titoloEnError" ></span>'+
                                    '</div>'+
                                '</div>'+                                            
                            '</div>'+  

                            '<div class="row">'+
                                '<div class="col-md-2">'+
                                    '<label class="control-label">Autore IT </label>'+
                                '</div>'+
                                '<div class="col-md-10">'+
                                    '<div class="form-group">'+
                                        '<textarea id="txtAutoreIt" maxlength="" name="txtAutoreIt"  class="form-control"  rows="3" style="resize: none;"  placeholder="autore it...">'+
                                        (jsonData!=null ? jsonData["autoreIT"] : "")
                                        +'</textarea>'+
                                    '</div>'+
                                '</div>'+    
                            '</div>'+  

                            '<div class="row">'+
                                '<div class="col-md-2">'+
                                    '<label class="control-label">Autore EN </label>'+
                                '</div>'+
                                '<div class="col-md-10">'+
                                    '<div class="form-group">'+
                                        '<textarea id="txtAutoreEn" maxlength="" name="txtAutoreEn"  class="form-control"  rows="3" style="resize: none;"  placeholder="autore en...">'+
                                       (jsonData!=null ? jsonData["autoreEN"] : "")
                                        +'</textarea>'+
                                    '</div>'+
                                '</div>'+    
                            '</div>'+  

                            '<div class="row">'+
                                '<div class="col-md-2">'+
                                    '<label class="control-label">Testo IT </label>'+
                                '</div>'+
                                '<div class="col-md-10">'+
                                    '<div class="form-group">'+
                                        '<textarea id="txtTestoIt" maxlength="" name="txtTestoIt"  class="form-control"  rows="3" style="resize: none;"  placeholder="testo it...">'+
                                        (jsonData!=null ? jsonData["testoIT"] : "")
                                        +'</textarea>'+
                                    '</div>'+
                                '</div>'+    
                            '</div>'+  

                            '<div class="row">'+
                                '<div class="col-md-2">'+
                                    '<label class="control-label">Testo EN </label>'+
                                '</div>'+
                                '<div class="col-md-10">'+
                                    '<div class="form-group">'+
                                        '<textarea id="txtTestoEn" maxlength="" name="txtTestoEn"  class="form-control"  rows="3" style="resize: none;"  placeholder="testo en...">'+
                                       (jsonData!=null ? jsonData["testoEN"] : "")
                                        +'</textarea>'+
                                    '</div>'+
                                '</div>'+    
                            '</div>'+  
                            
                        '</div>' +
                   ' </form>';

            me.modalBootbox = bootbox.dialog({
                closeButton: false,
                className: "modal70",
                message: html,
                title: (me.update==true ?  "Modifica elemento R&D " : "Inserimento Nuovo Elemento R&D"),
                buttons: {
                    annulla: {
                      label: "Annulla",
                      className: "btn-success",
                      callback: function() {
                        $('#btNuovoRDItem').removeClass('disabled');
                        $('.actionButton').removeClass('disabled');
                      }
                    },                      
                    conferma: {
                      label: "Conferma",
                      className: "btn-primary",
                      callback: function() {

                            
                            me.insertOrUpdate();
                            
                            return false;

                       }//chiude callback
                    }//chiude conferma
                }//chiude button
            });//chiude dialog

            $('#txtProgressivo').spinner({value:(jsonData!=null ? jsonData["progressivo"] : 1), min: 1});
         
    },
   
    insertOrUpdate: function(){

        var me = this;
        var link = $('#txtLink').val();
        var titoloIt = $('#txtTitoloIt').val();
        var titoloEn = $('#txtTitoloEn').val();
        var autoreIt = $('#txtAutoreIt').val();
        var autoreEn = $('#txtAutoreEn').val();
        var testoIt = $('#txtTestoIt').val();
        var testoEn = $('#txtTestoEn').val();

        var progressivo = ($('#txtProgressivoVal').val()!='' ? $('#txtProgressivoVal').val() : 1);

        $('.error').empty();
        var errori = false;
       
        if(titoloIt == "" ) {
            $('#titoloItError').append('<em>(Titolo obbligatorio.)</em>');
            errori = true;
        }

        if(errori)
            return false;

        RDController.insertOrUpdateRD({
            link: link,
            titoloIt: titoloIt,
            titoloEn: titoloEn,
            autoreIt: autoreIt,
            autoreEn: autoreEn,
            testoIt: testoIt,
            testoEn: testoEn,
            progressivo: progressivo,
            update: me.update,
            idRD: me.currentIdRD,
            callback: function(utente_id){

                $('#btNuovoTlItem').removeClass('disabled');
                $('.actionButton').removeClass('disabled');
                me.modalBootbox.modal("hide");

                new moduloRD({
                    canWrite: true
                });

            }

        });
                                  
        return false;
    }
});

