var moduloCompetenzeDetail = Backbone.View.extend({

    tagName : "div",
    className: '',
    update: null,
    updateAllegato: false,
    currentIdCompetenza: 0,
    currentIdPage: 0,
    canWrite: null,
    pathImage1: '',
    pathImage2: '',
    pathImage3: '',
    pathImage4: '',
    pathImageAllegato: '',
    tipoFile: '2',
    modalBootbox: null,
    attributes: {
        'id': 'div_moduloCompetenzaDetail'
    },

    events: {
       //'click #saveAllegato': 'insertOrUpdateAllegato',
       //'click [data-role-edit-allegato-id]' : 'doEditAllegato',
       'click [data-role-delete-allegato-id]' : 'doDeleteAllegato'
    },

    initialize: function(options) {

        $( ".file-preview-image" ).css( "height", "initial" );
     
        this.update = options.update;  
        this.canWrite = options.canWrite;
        this.currentIdPage = options.idPage;

        console.log("idPagina: "+this.currentIdPage);

        var me=this;  

        if(this.update==true){
            this.currentIdCompetenza = options.idCompetenza;
            CompetenzeController.getCompetenza({
                idCompetenza:  options.idCompetenza,
                idPage:  options.idPage,
                callback: function(jsonData) { 
                    me.render(jsonData);
                } 
            });
        }else
            this.render();
            
    },


    render : function(jsonData) {

           var me = this;

           console.log("-----------dettaglio-------");
           console.log(jsonData);

           var html = '<form method="post" class="horizontal-form" id="formNuovoCompetenza >'+
                        '<div class="form-body">'+  

                            '<div class="row">'+
                                '<div class="col-md-2">'+
                                    '<label class="control-label">Layout Competenza</label>'+
                                '</div>'+
                                '<div class="col-md-4">'+
                                    '<div class="form-group">'+
                                       '<select class="form-control select2me" data-placeholder="Seleziona Tipo Layout..." id="selTipo" name="selTipo">'+
                                             '<option value="2">Immagine - Titolo - Testo (intro)</option>'+
                                             '<option value="5">Elementi Multimediali Affiancati (media blog)</option>'+
                                             '<option value="10">Titolo - Immagine + 3 thumb - testo sotto</option>'+
                                             '<option ' + (jsonData==null ? "selected" : "") + ' value="12">Titolo - Immagine - Testo sulla destra</option>'+
                                        '</select>'+
                                       '<span class="text-danger error" id="errorTipo"></span>'+
                                    '</div>'+
                                '</div>'+

                                '<div class="col-md-2">'+
                                    '<label class="control-label">Posizione:</label>'+
                                '</div>'+
                                '<div class="col-md-4">'+
                                    '<div id="txtPosizione">'+ 
                                        '<div class="input-group" style="width:150px;margin-bottom:10px;">'+ 
                                            '<input type="text" id="txtPosizioneVal" value="' + (jsonData!=null ? (parseInt(jsonData["posizione"])) : "1") + '" class="spinner-input form-control" maxlength="3" >'+ 
                                            '<div class="spinner-buttons input-group-btn">'+ 
                                                '<button type="button" class="btn spinner-up default">'+ 
                                                '<i class="fa fa-angle-up"></i>'+ 
                                                '</button>'+ 
                                                '<button type="button" class="btn spinner-down default">'+ 
                                                '<i class="fa fa-angle-down"></i>'+ 
                                                '</button>'+ 
                                            '</div>'+ 
                                        '</div>'+ 
                                        '<span class="text-danger error" id="errorPosizione"></span>'+
                                    '</div>'+ 
                                '</div>'+  
                            '</div>'+ 

                            '<div class="row">'+
                                '<div class="col-md-2">'+
                                    '<label class="control-label">Nome (*)</label>'+
                                '</div>'+
                                '<div class="col-md-4">'+
                                   '<div class="form-group">'+
                                        '<input type="text" id="txtNome" name="txtNome" placeholder="Nome ..." value="' + (jsonData!=null ? jsonData["nome"] : "") + '" class="form-control">'+
                                        '<span class="text-danger error" id="nomeError" ></span>'+
                                    '</div>'+
                                '</div>'+                                            
                                '<div class="col-md-2">'+
                                    '<label class="control-label">Link </label>'+
                                '</div>'+
                                '<div class="col-md-4">'+
                                   '<div class="form-group">'+
                                        '<input type="text" id="txtLink" name="txtLink" placeholder="Link ..." value="' + (jsonData!=null ? jsonData["link"] : "") + '" class="form-control">'+
                                        '<span class="text-danger error" id="linkError" ></span>'+
                                    '</div>'+
                                '</div>'+                                            
                            '</div>'+  

                            '<div class="row">'+
                                '<div class="col-md-2">'+
                                    '<label class="control-label">Menu Laterale IT </label>'+
                                '</div>'+
                                '<div class="col-md-4">'+
                                   '<div class="form-group">'+
                                        '<input type="text" id="txtMenuIt"  name="txtMenuIt"  placeholder="Menu Laterale IT..."  value="' + (jsonData!=null ? jsonData["titolo_menu_laterale_it"] : "") + '" class="form-control">'+
                                         '<span class="text-danger error" id="titoloItError" ></span>'+
                                 '</div>'+
                                '</div>'+  
                     
                                '<div class="col-md-2">'+
                                    '<label class="control-label">Menu Laterale EN </label>'+
                                '</div>'+
                                '<div class="col-md-4">'+
                                   '<div class="form-group">'+
                                        '<input type="text" id="txtMenuEn"  name="txtMenuEn"  placeholder="Menu Laterale EN..."  value="' + (jsonData!=null ? jsonData["titolo_menu_laterale_en"] : "") + '" class="form-control">'+
                                         '<span class="text-danger error" id="titoloItError" ></span>'+
                                 '</div>'+
                                '</div>'+  
                            '</div>'+

                            '<div class="row">'+
                                '<div class="col-md-2">'+
                                    '<label class="control-label">Titolo IT </label>'+
                                '</div>'+
                                '<div class="col-md-4">'+
                                   '<div class="form-group">'+
                                        '<input type="text" id="txtTitoloIt"  name="txtTitoloIt"  placeholder="Titolo IT..."  value="' + (jsonData!=null ? jsonData["titoloIT"] : "") + '" class="form-control">'+
                                         '<span class="text-danger error" id="titoloItError" ></span>'+
                                 '</div>'+
                                '</div>'+  
                         
                                '<div class="col-md-2">'+
                                    '<label class="control-label">Titolo EN </label>'+
                                '</div>'+
                                '<div class="col-md-4">'+
                                   '<div class="form-group">'+
                                        '<input type="text" id="txtTitoloEn" name="txtTitoloEn" placeholder="Titolo EN..." value="' + (jsonData!=null ? jsonData["titoloEN"] : "") + '" class="form-control">'+
                                        '<span class="text-danger error" id="titoloEnError" ></span>'+
                                    '</div>'+
                                '</div>'+                                            
                            '</div>'+  

                            '<div class="row" id="descrizioni">'+
                                '<div class="col-md-2">'+
                                    '<label class="control-label">Descrizione IT </label>'+
                                '</div>'+
                                '<div class="col-md-4">'+
                                    '<div class="form-group">'+
                                        '<textarea id="txtDescrizioneIt" maxlength="" name="txtDescrizioneIt"  class="form-control"  rows="5" style="resize: none;"  placeholder="descrizione it...">'+
                                        (jsonData!=null ? jsonData["testoIT"] : "")
                                        +'</textarea>'+
                                    '</div>'+
                                '</div>'+    
                                '<div class="col-md-2">'+
                                    '<label class="control-label">Descrizione EN </label>'+
                                '</div>'+
                                '<div class="col-md-4">'+
                                    '<div class="form-group">'+
                                        '<textarea id="txtDescrizioneEn" maxlength="" name="txtDescrizioneEn"  class="form-control"  rows="5" style="resize: none;"  placeholder="descrizione en...">'+
                                       (jsonData!=null ? jsonData["testoEN"] : "")
                                        +'</textarea>'+
                                    '</div>'+
                                '</div>'+    
                            '</div>'+  

                            '<hr>'+ 

                            '<div class="row">'+
                                '<div class="col-md-3" id="file1">'+
                                    '<label class="control-label">Immagine 1(*)</label>'+
                                    '<div class="form-group">'+
                                        '<input id="selectedFile1" name="selectedFile1" class="file" type="file" style="display:none;">'+
                                        '<span style="font-size:80%">Sono supportati solo file di tipo "jpg, png, gif". Dimensione max '+(this.tipoFile==2 ?  "1 MB." : "5 MB.")+'</span><br>'+
                                        '<span class="text-danger error" id="imgError1" ></span>'+
                                    '</div>'+

                                    '<div id="descrizioniMedia1">'+

                                        '<label class="control-label">Titolo IT </label>'+
                                        '<div class="form-group">'+
                                            '<input type="text" id="txtTitoloMediaIt1"  name="txtTitoloMediaIt1"  placeholder="Titolo IT..."  class="form-control">'+
                                        '</div>'+
 
                                        '<label class="control-label">Descrizione IT</label>'+
                                        '<div class="form-group">'+
                                            '<textarea id="txtDescrizioneMediaIT1" maxlength="" name="txtDescrizioneMediaIT1"  class="form-control"  rows="5" style="resize: none;"  placeholder="descrizione it...">'+
                                            '</textarea>'+
                                        '</div>'+

                                        '<label class="control-label">Titolo EN </label>'+
                                        '<div class="form-group">'+
                                            '<input type="text" id="txtTitoloMediaEn1"  name="txtTitoloMediaEn1"  placeholder="Titolo EN..."  class="form-control">'+
                                        '</div>'+

                                        '<label class="control-label">Descrizione EN</label>'+
                                        '<div class="form-group">'+
                                            '<textarea id="txtDescrizioneMediaEN1" maxlength="" name="txtDescrizioneMediaEN1"  class="form-control"  rows="5" style="resize: none;"  placeholder="descrizione en...">'+
                                            '</textarea>'+
                                        '</div>'+

                                    '</div>'+
                                       
                                '</div>'+    
                                '<div class="col-md-3" id="file2">'+
                                    '<label class="control-label">Immagine 2(*)</label>'+
                                    '<div class="form-group">'+
                                        '<input id="selectedFile2" name="selectedFile2" class="file" type="file" style="display:none;">'+
                                        '<span style="font-size:80%">Sono supportati solo file di tipo "jpg, png, gif". Dimensione max '+(this.tipoFile==2 ?  "1 MB." : "5 MB.")+'</span><br>'+
                                        '<span class="text-danger error" id="imgError2" ></span>'+
                                    '</div>'+

                                    '<div id="descrizioniMedia2">'+

                                        '<label class="control-label">Titolo IT </label>'+
                                        '<div class="form-group">'+
                                            '<input type="text" id="txtTitoloMediaIt2"  name="txtTitoloMediaIt2"  placeholder="Titolo IT..."  class="form-control">'+
                                        '</div>'+

                                        '<label class="control-label">Descrizione IT</label>'+
                                        '<div class="form-group">'+
                                            '<textarea id="txtDescrizioneMediaIT2" maxlength="" name="txtDescrizioneMediaIT2"  class="form-control"  rows="5" style="resize: none;"  placeholder="descrizione it...">'+
                                            '</textarea>'+
                                        '</div>'+

                                        '<label class="control-label">Titolo EN </label>'+
                                        '<div class="form-group">'+
                                            '<input type="text" id="txtTitoloMediaEn2"  name="txtTitoloMediaEn2"  placeholder="Titolo EN..."  class="form-control">'+
                                        '</div>'+

                                        '<label class="control-label">Descrizione EN</label>'+
                                        '<div class="form-group">'+
                                            '<textarea id="txtDescrizioneMediaEN2" maxlength="" name="txtDescrizioneMediaEN2"  class="form-control"  rows="5" style="resize: none;"  placeholder="descrizione en...">'+
                                            '</textarea>'+
                                        '</div>'+
                                    '</div>'+

                                '</div>'+  
                                '<div class="col-md-3" id="file3">'+
                                    '<label class="control-label">Immagine 3(*)</label>'+
                                    '<div class="form-group">'+
                                        '<input id="selectedFile3" name="selectedFile3" class="file" type="file" style="display:none;">'+
                                        '<span style="font-size:80%">Sono supportati solo file di tipo "jpg, png, gif". Dimensione max '+(this.tipoFile==2 ?  "1 MB." : "5 MB.")+'</span><br>'+
                                        '<span class="text-danger error" id="imgError3" ></span>'+
                                    '</div>'+

                                    '<div id="descrizioniMedia3">'+

                                        '<label class="control-label">Titolo IT </label>'+
                                        '<div class="form-group">'+
                                            '<input type="text" id="txtTitoloMediaIt3"  name="txtTitoloMediaIt3"  placeholder="Titolo IT..."  class="form-control">'+
                                        '</div>'+

                                        '<label class="control-label">Descrizione IT</label>'+
                                        '<div class="form-group">'+
                                            '<textarea id="txtDescrizioneMediaIT3" maxlength="" name="txtDescrizioneMediaIT3"  class="form-control"  rows="5" style="resize: none;"  placeholder="descrizione it...">'+
                                            '</textarea>'+
                                        '</div>'+

                                        '<label class="control-label">Titolo EN </label>'+
                                        '<div class="form-group">'+
                                            '<input type="text" id="txtTitoloMediaEn3"  name="txtTitoloMediaEn3"  placeholder="Titolo EN..."  class="form-control">'+
                                        '</div>'+

                                        '<label class="control-label">Descrizione EN</label>'+
                                        '<div class="form-group">'+
                                            '<textarea id="txtDescrizioneMediaEN3" maxlength="" name="txtDescrizioneMediaEN3"  class="form-control"  rows="5" style="resize: none;"  placeholder="descrizione en...">'+
                                            '</textarea>'+
                                        '</div>'+
                                    '</div>'+

                                '</div>'+    
                                '<div class="col-md-3" id="file4">'+
                                    '<label class="control-label">Immagine 4(*)</label>'+
                                    '<div class="form-group">'+
                                        '<input id="selectedFile4" name="selectedFile4" class="file" type="file" style="display:none;">'+
                                        '<span style="font-size:80%">Sono supportati solo file di tipo "jpg, png, gif". Dimensione max '+(this.tipoFile==2 ?  "1 MB." : "5 MB.")+'</span><br>'+
                                        '<span class="text-danger error" id="imgError4" ></span>'+
                                    '</div>'+

                                    '<div id="descrizioniMedia4" style="display:none;">'+

                                        '<label class="control-label">Titolo IT </label>'+
                                        '<div class="form-group">'+
                                            '<input type="text" id="txtTitoloMediaIt4"  name="txtTitoloMediaIt4"  placeholder="Titolo IT..."  class="form-control">'+
                                        '</div>'+

                                        '<label class="control-label">Descrizione IT</label>'+
                                        '<div class="form-group">'+
                                            '<textarea id="txtDescrizioneMediaIT4" maxlength="" name="txtDescrizioneMediaIT4"  class="form-control"  rows="5" style="display:none;resize: none;"  placeholder="descrizione it...">'+
                                            '</textarea>'+
                                        '</div>'+


                                        '<label class="control-label">Titolo EN </label>'+
                                        '<div class="form-group">'+
                                            '<input type="text" id="txtTitoloMediaEn4"  name="txtTitoloMediaEn4"  placeholder="Titolo EN..."  class="form-control">'+
                                        '</div>'+

                                        '<label class="control-label">Descrizione EN</label>'+
                                        '<div class="form-group">'+
                                            '<textarea id="txtDescrizioneMediaEN4" maxlength="" name="txtDescrizioneMediaEN4"  class="form-control"  rows="5" style="display:none;resize: none;"  placeholder="descrizione en...">'+
                                            '</textarea>'+
                                        '</div>'+
                                    '</div>'+

                                '</div>'+    
                            '</div>'+  
                           
                            '<div id="portletAllegati"  class="portlet box blue" '+ (me.update!=true ? "style=\"display:none\"" : "") +'>'+
                                '<div class="portlet-title">'+
                                    '<div class="caption">'+
                                        '<i class="fa fa-file-pdf-o"></i>Allegati'+
                                    '</div>'+
                                    '<div class="tools">'+
                                        '<a href="javascript:;" class="expand" data-original-title="" title=""></a>'+
                                    '</div>'+
                                '</div>'+
                                '<div class="portlet-body" style="display: none;">'+

                                     '<div class="row">'+
                                        '<div class="col-md-2">'+
                                            '<label class="control-label">Tipo Allegato (*)</label>'+
                                        '</div>'+
                                        '<div class="col-md-2">'+
                                            '<div class="form-group">'+
                                               '<select class="form-control select2me" data-placeholder="Seleziona Tipo Allegato..." id="selTipoAllegato" name="selTipo">'+
                                                 '<option value="1">Allegato</option>'+
                                                 '<option value="2">Collegamento</option>'+
                                                 '<option value="3">Rassegna Stampa</option>'+
                                                '</select>'+
                                               '<span class="text-danger error" id="errorTipoAllegato"></span>'+
                                            '</div>'+
                                        '</div>'+ 
                                        '<div class="col-md-2">'+
                                            '<label class="control-label">Tipo Icona (*)</label>'+
                                        '</div>'+
                                        '<div class="col-md-2">'+
                                            '<div class="form-group">'+
                                               '<select class="form-control select2me" data-placeholder="Seleziona Tipo Icona..." id="selTipoIcona" name="selTipo">'+
                                                 '<option value="1">Manuale Utente</option>'+
                                                 '<option value="2">Presentazione</option>'+
                                                 '<option value="3">Articolo</option>'+
                                                 '<option value="4">Brochure</option>'+
                                                '</select>'+
                                               '<span class="text-danger error" id="errorTipoIcona"></span>'+
                                            '</div>'+
                                        '</div>'+

                                        '<div class="col-md-2">'+
                                            '<label class="control-label">Posizione (*)</label>'+
                                        '</div>'+
                                        '<div class="col-md-2">'+
                                            '<div id="txtPosizioneAllegato">'+ 
                                                '<div class="input-group" style="width:150px;margin-bottom:10px;">'+ 
                                                    '<input type="text" id="txtPosizioneAllegatoVal" value="" class="spinner-input form-control" maxlength="3" >'+ 
                                                    '<div class="spinner-buttons input-group-btn">'+ 
                                                        '<button type="button" class="btn spinner-up default">'+ 
                                                        '<i class="fa fa-angle-up"></i>'+ 
                                                        '</button>'+ 
                                                        '<button type="button" class="btn spinner-down default">'+ 
                                                        '<i class="fa fa-angle-down"></i>'+ 
                                                        '</button>'+ 
                                                    '</div>'+ 
                                                '</div>'+ 
                                                '<span class="text-danger error" id="errorPosizioneAllegato"></span>'+
                                            '</div>'+ 
                                        '</div>'+  
                                    '</div>'+

                                    '<div class="row">'+
                                        '<div class="col-md-2">'+
                                            '<label class="control-label">Testo IT (*)</label>'+
                                        '</div>'+
                                        '<div class="col-md-4">'+
                                           '<div class="form-group">'+
                                                '<input type="text" id="txtAllegatoTestoIT" name="txtAllegatoTestoIT" placeholder="Titolo it ..." value="" class="form-control">'+
                                                '<span class="text-danger error" id="itAllegatoError" ></span>'+
                                            '</div>'+
                                        '</div>'+                                            
                                        '<div class="col-md-2">'+
                                            '<label class="control-label">Testo EN (*)</label>'+
                                        '</div>'+
                                        '<div class="col-md-4">'+
                                           '<div class="form-group">'+
                                                '<input type="text" id="txtAllegatoTestoEN" name="txtAllegatoTestoEN" placeholder="Titolo en ..." value="" class="form-control">'+
                                                '<span class="text-danger error" id="enAllegatoError" ></span>'+
                                            '</div>'+
                                        '</div>'+                                            
                                    '</div>'+ 

                                    '<div class="row">'+
                                        '<div class="col-md-2">'+
                                            '<label class="control-label">File (*)</label>'+
                                        '</div>'+
                                        '<div class="col-md-4">'+
                                            '<div class="form-group">'+
                                                '<input id="selectedFileAllegato" name="selectedFileAllegato" class="file" type="file" style="display:none;">'+
                                                '<span style="font-size:80%">Dimensione max '+(this.tipoFile==2 ?  "2 MB." : "5 MB.")+'</span><br>'+
                                                '<span class="text-danger error" id="imgErrorAllegato" ></span>'+
                                            '</div>'+
                                        '</div>'+   
                                        '<div class="col-md-6">'+
                                            '<button type="button" id="saveAllegato" class="btn btn-primary">Salva Nuovo Allegato</button>'+
                                        '</div>'+                                 
                                    '</div>'+ 

                                    '<div class="row">'+
                                        '<div class="col-md-12">'+
                                        '<table class="table table-striped table-bordered table-hover" id="tableAllegati" >'+
                                            '<div id="divFiltraAllegati"></div>'+
                                            '<thead>'+
                                            '<tr>'+
                                                '<th>Path</th>'+
                                                '<th>Titolo IT</th>'+
                                                '<th>Posizione</th>'+
                                                '<th></th>'+
                                            '</tr>'+
                                            '</thead>'+
                                            '<tbody id="tableDataAllegati">'+
                                            '</tbody>'+
                                        '</table>'+
                                        '</div>'+
                                    '</div>'+
                             
                                '</div>'+
                            '</div>'+

                        '</div>' +
                   ' </form>';

            me.modalBootbox = bootbox.dialog({
                closeButton: false,
                className: "modal90",
                message: html,
                title: (me.update==true ?  "Modifica Sezione " : "Inserimento Nuova Sezione"),
                buttons: {
                    annulla: {
                      label: "Annulla",
                      className: "btn-success",
                      callback: function() {
                        $('#btNuovaSezione').removeClass('disabled');
                        $('.actionButton').removeClass('disabled');
                      }
                    },                      
                    conferma: {
                      label: "Conferma",
                      className: "btn-primary",
                      callback: function() {

                            var nomeFile1 = $('#selectedFile1').val();
                            var nomeFile2 = $('#selectedFile2').val();
                            var nomeFile3 = $('#selectedFile3').val();
                            var nomeFile4 = $('#selectedFile4').val();

                            if(nomeFile1=="" && nomeFile2=="" && nomeFile3=="" && nomeFile4==""){
                                //vuol dire che sto modificando solo il contenuto
                                me.insertOrUpdate();
                            }else{  
                                //effettuo l'upload prima di salvare
                               if(nomeFile1 != ''){
                                   me.innerUpload(1);
                               }
                               if(nomeFile2 != ''){
                                   me.innerUpload(2);
                               }
                               if(nomeFile3 != ''){
                                   me.innerUpload(3);
                               }
                               if(nomeFile4 != ''){
                                   me.innerUpload(4);
                               }
                                //me.insertOrUpdate();
                            }
        
                            return false;

                       }//chiude callback
                    }//chiude conferma
                }//chiude button
            });//chiude dialog

            $('#txtPosizione').spinner({value:(jsonData!=null ? (parseInt(jsonData["posizione"])) : 1), min: 1});
            $('#txtPosizioneAllegato').spinner({value:1, min: 1});

            $('#selTipo').select2({
                allowClear:true
            }).on("select2-selecting", function(e) { 

            }).on('change', function(e) {
                console.log("value change = " + $(this).val().toString());
                me.viewLayoutSezione($('#selTipo').val());
            });

            if(jsonData!=null){ 
                $('#selTipo').select2('val', jsonData["layout"]);
                me.viewLayoutSezione(jsonData["layout"]);
            }else{
                me.viewLayoutSezione(12);
            }

            $('#selTipoAllegato').select2();
            $('#selTipoIcona').select2();

            me.showMedia();

            if(me.update==true){
                me.showAllegati();
            }

            $('#saveAllegato').click(function(event){
               me.innerUploadAllegato();
            });

            //ALLEGATI
            var _initialPreview = [];
            var _allowedFileExtensions = [];
            var _maxFileSize="";

            _allowedFileExtensions = '';
            //_allowedFileExtensions = ['jpg', 'png','gif'];
            _maxFileSize= 2000;      

            $("#selectedFileAllegato").fileinput({
                allowedFileExtensions : _allowedFileExtensions,
                uploadAsync: true,
                minFileCount: 0,
                maxFileCount: 1,
                maxFileSize: _maxFileSize,
                removeLabel: 'Rimuovi',
                removeTitle: 'Rimuovi file selezionato',
                cancelLabel: 'Annulla',
                cancelTitle: 'Annulla upload',
                uploadLabel: 'Upload',
                uploadTitle: 'Upload file selezionato',
                browseLabel: 'Seleziona',
                msgSelected: '{n} files selezionato',
                dropZoneEnabled: false,
                msgInvalidFileType: 'Formato del file "{name}" non supportato. Sono supportati solo file di tipo "{types}".',
                msgInvalidFileExtension: 'Estensione del file "{name}" non supportata. Sono supportati solo file di tipo "{extensions}".',
                msgSizeTooLarge:'La dimesione del file "{name}" (<b>{size} KB</b>) eccede il limite imposto <b>{maxSize} KB</b>.',
                uploadUrl: './phpService/CompetenzeServices.php?action=uploadAllegato',
                initialPreview: _initialPreview,
                layoutTemplates: {
                
                    main1: '' +
                            '<div class="kv-upload-progress hide"></div>\n' +
                            '<div class="input-group {class}">\n' +
                            '   {caption}\n' +
                            '   <div class="input-group-btn">\n' +
                            '       <a id="btnEmptyFileManagerAllegato" href="#" class="btn btn-default"><i class="fa fa-trash-o"></i> Rimuovi</a>\n' +
                            '       <a id="btnAddFileManagerAllegato" href="#" class="btn btn-default"><i class="fa fa-upload"></i> Seleziona un file</a>\n' +
                            '   </div>\n' +
                            '</div>',                     
                            
                    icon: '<span class="glyphicon glyphicon-picture kv-caption-icon"></span>',
                    
                    actions: '',
                    footer: ''
                }
                
            }).on('fileuploaded', function(event, data, previewId, index) {
                    
                $("#selectedFileAllegato").attr('data-name', data.response.nuovoFile);
                var newFile = data.response.nuovoFile;
                me.pathImageAllegato = newFile.replace("../../allegati/", "");
                me.insertOrUpdateAllegato();

            }).on('filebatchuploadsuccess', function(event, data, previewId, index) {

                $("#selectedFileAllegato").attr('data-name', data.response.nuovoFile);
                var newFile = data.response.nuovoFile;
                me.pathImageAllegato = newFile.replace("../../allegati/", "");
                me.insertOrUpdateAllegato();
                
            }).on('filecleared', function(event) {
                //se rimuovo una immagine esistente non faccio upload ma devo rimuovere il file e aggiornare la path
                me.pathImageAllegato = "";    
            });
           
            $('#btnEmptyFileManagerAllegato').click(function(evt) {
                $("#selectedFileAllegato").fileinput('clear');
            });

            $('#btnAddFileManagerAllegato').click(function(evt) {
                $("#selectedFileAllegato").click();
            });

    },

    viewLayoutSezione: function(idTipoSezione){

        var me = this;

        console.log("--------mylayout------"+idTipoSezione);

        //immagine titolo testo (sezione - intro)
        if(idTipoSezione == 2){

            $("#descrizioni").show();

            $("#descrizioniMedia1").hide();
            $("#descrizioniMedia2").hide();
            $("#descrizioniMedia3").hide();
        
            $("#file1").show();
            $("#file2").hide();
            $("#file3").hide();
            $("#file4").hide();

            $("#file1").removeClass("col-md-4").addClass("col-md-3");
            $("#file2").removeClass("col-md-4").addClass("col-md-3");
            $("#file3").removeClass("col-md-4").addClass("col-md-3");
            $("#file4").removeClass("col-md-4").addClass("col-md-3");

            $("#portletAllegati").hide();

        //elementi multimediali affiancati (sezione - media blog)
        }else if(idTipoSezione == 5){

            $("#descrizioni").hide();

            $("#descrizioniMedia1").show();
            $("#descrizioniMedia2").show();
            $("#descrizioniMedia3").show();

            $("#file1").show();
            $("#file2").show();
            $("#file3").show();
            $("#file4").hide();

            $("#file1").removeClass("col-md-3").addClass("col-md-4");
            $("#file2").removeClass("col-md-3").addClass("col-md-4");
            $("#file3").removeClass("col-md-3").addClass("col-md-4");
            $("#file4").removeClass("col-md-3").addClass("col-md-4");

            $("#portletAllegati").hide();

        //titolo immagine + 3 thumb (sezione)
        }else if(idTipoSezione == 10){

            $("#descrizioni").show();

            $("#descrizioniMedia1").hide();
            $("#descrizioniMedia2").hide();
            $("#descrizioniMedia3").hide();

            $("#file1").removeClass("col-md-4").addClass("col-md-3");
            $("#file2").removeClass("col-md-4").addClass("col-md-3");
            $("#file3").removeClass("col-md-4").addClass("col-md-3");
            $("#file4").removeClass("col-md-4").addClass("col-md-3");

            $("#file1").show();
            $("#file2").show();
            $("#file3").show();
            $("#file4").show();

            if(me.update!=true){
                $("#portletAllegati").hide();
            }else{
                $("#portletAllegati").show();
            }

            
        //Titolo - Immagine - Testo sulla destra (sezione)
        }else if(idTipoSezione == 12){

            $("#descrizioni").show();

            $("#descrizioniMedia1").hide();
            $("#descrizioniMedia2").hide();
            $("#descrizioniMedia3").hide();

            $("#file1").removeClass("col-md-4").addClass("col-md-3");
            $("#file2").removeClass("col-md-4").addClass("col-md-3");
            $("#file3").removeClass("col-md-4").addClass("col-md-3");
            $("#file4").removeClass("col-md-4").addClass("col-md-3");

            $("#file1").show();
            $("#file2").hide();
            $("#file3").hide();
            $("#file4").hide();

            if(me.update!=true){
                $("#portletAllegati").hide();
            }else{
                $("#portletAllegati").show();
            }
            
        }
    },

    showMedia: function(){
        var me = this;
        CompetenzeController.getMediaByIdCompetenza({
            idCompetenza:  me.currentIdCompetenza,
            callback: function(jsonData) { 
                console.log("------media------------------------");
                for(i=0;i<4;i++){
                    me.renderMedia(jsonData[i],i+1);
                }
            } 
        });
    },

    renderMedia : function(jsonData,count) {

        var me = this;

        if(jsonData!=null){
            if(count == 1){
                me.pathImage1  = jsonData["path"];
            }else if(count == 2){
                me.pathImage2  = jsonData["path"];
            }else if(count == 3){
                me.pathImage3  = jsonData["path"];
            }else if(count == 4){
                me.pathImage4  = jsonData["path"];
            }

            console.log(jsonData);
            //+ (jsonData!=null ? jsonData["titoloIT"] : "") + 'txtDescrizioneMediaIT1

            $('#txtTitoloMediaIt'+count).val(jsonData["titoloIT"]);
            $('#txtTitoloMediaEn'+count).val(jsonData["titoloEN"]);

            $('#txtDescrizioneMediaIT'+count).val(jsonData["descrizioneIT"]);
            $('#txtDescrizioneMediaEN'+count).val(jsonData["descrizioneEN"]);

        }

        var _initialPreview = [];
        var _allowedFileExtensions = [];
        var _maxFileSize="";
      
        if(me.update==true && jsonData!=null){
            _initialPreview = [ "<img style='width:100%' src='../images/"+(jsonData!=null ? jsonData["path"] : "")+"?time="+(new Date().getTime())+"' class='file-preview-image' >" ];
        }

        //_allowedFileTypes : ['image'];
        _allowedFileExtensions = ['jpg', 'png','gif'];
        _maxFileSize= 1000;
       
        $("#selectedFile"+count).fileinput({
            allowedFileExtensions : _allowedFileExtensions,
            uploadAsync: true,
            minFileCount: 0,
            maxFileCount: 1,
            maxFileSize: _maxFileSize,
            showUpload: false,
            removeLabel: 'Rimuovi',
            removeTitle: 'Rimuovi file selezionato',
            cancelLabel: 'Annulla',
            cancelTitle: 'Annulla upload',
            uploadLabel: 'Upload',
            uploadTitle: 'Upload file selezionato',
            browseLabel: 'Seleziona',
            msgSelected: '{n} files selezionato',
            dropZoneEnabled: false,
            msgInvalidFileType: 'Formato del file "{name}" non supportato. Sono supportati solo file di tipo "{types}".',
            msgInvalidFileExtension: 'Estensione del file "{name}" non supportata. Sono supportati solo file di tipo "{extensions}".',
            msgSizeTooLarge:'La dimesione del file "{name}" (<b>{size} KB</b>) eccede il limite imposto <b>{maxSize} KB</b>.',
            uploadUrl: './phpService/CompetenzeServices.php?action=uploadImage'+count+'&oldFile'+count+'='+(jsonData!=null ? "../../images/"+jsonData["path"] : ""),
            initialPreview: _initialPreview,
            //initialPreview: '',
            layoutTemplates: {
            
                main1: '{preview}\n' +
                        '<div class="kv-upload-progress hide"></div>\n' +
                        '<div class="input-group {class}">\n' +
                        '   {caption}\n' +
                        '   <div class="input-group-btn">\n' +
                        '       <a id="btnEmptyFileManager'+count+'" href="#" class="btn btn-default"><i class="fa fa-trash-o"></i> Rimuovi</a>\n' +
                        '       <a id="btnAddFileManager'+count+'" href="#" class="btn btn-default"><i class="fa fa-upload"></i> Seleziona un file</a>\n' +
                        '   </div>\n' +
                        '</div>',
                        
                preview: 
                        '<div class="file-preview {class}"  style="/*height:220px;*/">\n' +              
                        '    <div class="{dropClass}" style="border:none; padding: 3px; margin: 0px;">\n' +
                        '    <div class="file-preview-thumbnails">\n' +
                        '    </div>\n' +
                        '    <div class="clearfix"></div>' +
                        '    <div class="kv-fileinput-error"></div>\n' +
                        '    </div>\n' +
                        '</div>',
                        
                icon: '<span class="glyphicon glyphicon-picture kv-caption-icon"></span>',
                
                actions: '',
                footer: ''
            }
            
        }).on('fileuploaded', function(event, data, previewId, index) {
                
            $("#selectedFile"+count).attr('data-name', data.response.nuovoFile);

            var newFile = data.response.nuovoFile;

            if(count == 1){
                me.pathImage1 = newFile.replace("../../images/", "");
            }else if(count == 2){
                me.pathImage2 = newFile.replace("../../images/", "");
            }else if(count == 3){
                me.pathImage3 = newFile.replace("../../images/", "");
            }else if(count == 4){
                me.pathImage4 = newFile.replace("../../images/", "");
            }

            me.pathImage = newFile.replace("../../images/", "");

            me.insertOrUpdate();
                        
        }).on('filebatchuploadsuccess', function(event, data, previewId, index) {

            $("#selectedFile"+count).attr('data-name', data.response.nuovoFile);

            var newFile = data.response.nuovoFile;

            if(count == 1){
                me.pathImage1 = newFile.replace("../../images/", "");
            }else if(count == 2){
                me.pathImage2 = newFile.replace("../../images/", "");
            }else if(count == 3){
                me.pathImage3 = newFile.replace("../../images/", "");
            }else if(count == 4){
                me.pathImage4 = newFile.replace("../../images/", "");
            }

            me.insertOrUpdate();
            
        }).on('filecleared', function(event) {

            //se rimuovo una immagine esistente non faccio upload ma devo rimuovere il file e aggiornare la path

            if(count == 1){
                me.pathImage1  = "";
            }else if(count == 2){
                me.pathImage2  = "";
            }else if(count == 3){
                me.pathImage3  = "";
            }else if(count == 4){
                me.pathImage4  = "";
            }

        });
       
        $('#btnEmptyFileManager'+count).click(function(evt) {
            $("#selectedFile"+count).fileinput('clear');
        });

        $('#btnAddFileManager'+count).click(function(evt) {
            $("#selectedFile"+count).click();
        });

    },

    showAllegati: function(){
        var me = this;
        CompetenzeController.getAllegatiByIdCompetenza({
            idCompetenza:  me.currentIdCompetenza,
            callback: function(jsonData) { 

                $('#tableDataAllegati').empty();
                for(var i in jsonData) {

                    $('#tableDataAllegati').append(
                    '<tr class="odd gradeX" id="rigaAllegato_'+jsonData[i]['id']+'">'+
                        '<td><a href="../allegati/'+ jsonData[i]['path'] +'" target="_blank">' + jsonData[i]['path'] + '</a></td>'+
                        '<td>' + jsonData[i]['testoIT'] + '</td>'+
                        '<td>' + jsonData[i]['posizione'] + '</td>'+
                        '<td style="width: 20px;">' + 
                            //'<span class="actionButton" style="padding: 0 10px 0 0; !important"><a href="#" data-role-edit-allegato-id="' + jsonData[i]['id'] + '"><i class="icon-pencil"></i></a></span>' + 
                            '<span class="actionButton"><a href="#" data-role-delete-allegato-id="' + jsonData[i]['id'] + '" >'+
                            '<i class="fa fa-trash"></i></a></span>'+ 
                        '</td>'+
                    '</tr>');
                }

                if(!$('#tableAllegati').hasClass('initialized')){ 
                    //Prima carico i dati e dopo inizializzo la tabella altrimenti non funziona la ricerca
                    var columnSort = [];
                    columnSort.push({"orderable" : false}),
                    columnSort.push({"orderable" : false}),
                    columnSort.push({"orderable" : false}),
                    columnSort.push({"orderable" : false}),
                    manageDatatable2($('#tableAllegati'),$('#tableAllegati'), columnSort);
                    $('#tableAllegati').not('.initialized').addClass('initialized');
                }

                $('[data-role-delete-allegato-id]').click(function(event){
                   me.doDeleteAllegato($(this).attr("data-role-delete-allegato-id"));
                });

            } 
        });
    },

    innerUpload: function(count){
        $("#selectedFile"+count).fileinput('upload');
    },

    innerUploadAllegato: function(){
        var path = $('#selectedFileAllegato').val();
        var titoloIt = $('#txtAllegatoTestoIT').val();
        var titoloEn = $('#txtAllegatoTestoEN').val();

        $('.error').empty();
        var errori = false;

        if(titoloIt == "") {
            $('#itAllegatoError').append('<em>(Titolo IT obbligatorio)</em>');
            errori = true;
        }

        if(titoloEn == "") {
            $('#enAllegatoError').append('<em>(Titolo EN obbligatorio)</em>');
            errori = true;
        }

        if(errori)
            return false;


        if(path == ''){
            alert("Selezionare un file.");
        }else{
            $("#selectedFileAllegato").fileinput('upload');
        } 
    },

    insertOrUpdateAllegato: function(){

        var me = this;
        var path = me.pathImageAllegato;
        var titoloIt = $('#txtAllegatoTestoIT').val();
        var titoloEn = $('#txtAllegatoTestoEN').val();
        var tipoAllegato = $('#selTipoAllegato').val();
        var tipoIcona = $('#selTipoIcona').val();
        var posizione = ($('#txtPosizioneAllegatoVal').val()!='' ? $('#txtPosizioneAllegatoVal').val() : 1);

        $('.error').empty();
        var errori = false;
       
        if(path == "") {
            $('#imgErrorAllegato').append('<em>(Immagine obbligatoria)</em>');
            errori = true;
        }

        if(titoloIt == "") {
            $('#itAllegatoError').append('<em>(Titolo IT obbligatorio)</em>');
            errori = true;
        }

        if(titoloEn == "") {
            $('#enAllegatoError').append('<em>(Titolo EN obbligatorio)</em>');
            errori = true;
        }

        if(posizione < 1 ) {
            $('#errorPosizioneAllegato').append('<em>(Attenzione! la posizione deve essere maggiore di 1)</em>');
            errori = true;
        }

        if(errori)
            return false;

        CompetenzeController.insertOrUpdateAllegato({
            path: path,
            titoloIt: titoloIt,
            titoloEn: titoloEn,
            tipoAllegato: tipoAllegato,
            tipoIcona: tipoIcona,
            posizione: posizione,
            updateAllegato: me.updateAllegato,
            idCompetenza: me.currentIdCompetenza,
            idAllegato: '',
            callback: function(utente_id){

               // alert("ho salvato allegato");
                me.showAllegati();
                me.resetCampiAllegato();

            }

        });
    },

    resetCampiAllegato: function(){
        var me = this;
        me.updateAllegato = false;
        me.pathImageAllegato = "";
        $("#selectedFileAllegato").fileinput('clear');
        $('#txtAllegatoTestoIT').val("");
        $('#txtAllegatoTestoEN').val("");
        $('#selTipoAllegato').select2('val', 1);
        $('#selTipoIcona').select2('val', 1);
        $('#txtPosizioneAllegatoVal').val(1);
    },

    doDeleteAllegato: function(id){

        var me = this;
        var currentAllegatoId = id;
        var messaggio = "Si desidera eliminare l\'allegato selezionato?" 
       
        var confirmDelete = bootbox.dialog({
            message: messaggio,
            title: "Attenzione!",
            backdrop: true,
            closeButton: true,
            animate: false,
            buttons: {
                conferma: {
                  label: "Conferma",
                  className: "btn-primary",
                  callback: function() {         
                    CompetenzeController.deleteAllegato({
                        idAllegato: currentAllegatoId,
                        callback: function() {
                             confirmDelete.modal("hide");
                             $('#rigaAllegato_'+currentAllegatoId).remove();
                        }
                    });
                    return false;
                  }
                   
                },
                cancel: {
                  label: "Annulla",
                  className: "btn-success",
                  callback: function() {
                  }
                }
            }
        });
    },

    insertOrUpdate: function(){

        var me = this;
        var path1 = me.pathImage1;
        var path2 = me.pathImage2;
        var path3 = me.pathImage3;
        var path4 = me.pathImage4;
        var nome = $('#txtNome').val();
        var link = $('#txtLink').val();
        var titoloIt = $('#txtTitoloIt').val();
        var titoloEn = $('#txtTitoloEn').val();
        var menuIt = $('#txtMenuIt').val();
        var menuEn = $('#txtMenuEn').val();
        var descrizioneIt = $('#txtDescrizioneIt').val();
        var descrizioneEn = $('#txtDescrizioneEn').val();
        var tipoLayout = $('#selTipo').val();

        var posizione = ($('#txtPosizioneVal').val()!='' ? $('#txtPosizioneVal').val() : 1);
        posizione = parseInt(posizione);

        var descrizioneMediaIt1 = $('#txtDescrizioneMediaIT1').val();
        var descrizioneMediaEn1 = $('#txtDescrizioneMediaEN1').val();
        var descrizioneMediaIt2 = $('#txtDescrizioneMediaIT2').val();
        var descrizioneMediaEn2 = $('#txtDescrizioneMediaEN2').val();
        var descrizioneMediaIt3 = $('#txtDescrizioneMediaIT3').val();
        var descrizioneMediaEn3 = $('#txtDescrizioneMediaEN3').val();

        var titoloMediaIt1 = $('#txtTitoloMediaIt1').val();
        var titoloMediaEn1 = $('#txtTitoloMediaEn1').val();
        var titoloMediaIt2 = $('#txtTitoloMediaIt2').val();
        var titoloMediaEn2 = $('#txtTitoloMediaEn2').val();
        var titoloMediaIt3 = $('#txtTitoloMediaIt3').val();
        var titoloMediaEn3 = $('#txtTitoloMediaEn3').val();

        $('.error').empty();
        var errori = false;
       
        if(nome == "") {
            $('#nomeError').append('<em>(Inserire il nome per la competenza)</em>');
            errori = true;
        }

        if(path1 == "") {
            $('#imgError1').append('<em>(Immagine obbligatoria)</em>');
            errori = true;
        }

        if(posizione < 1 ) {
            $('#errorPosizione').append('<em>(Attenzione! la posizione deve essere maggiore di 0)</em>');
            errori = true;
        }

        if(errori)
            return false;

        CompetenzeController.insertOrUpdateCompetenza({
            path1: path1,
            path2: path2,
            path3: path3,
            path4: path4,
            nome: nome,
            link: link,
            titoloIt: titoloIt,
            titoloEn: titoloEn,
            menuIt: menuIt,
            menuEn: menuEn,
            descrizioneIt: descrizioneIt,
            descrizioneEn: descrizioneEn,
            tipoLayout: tipoLayout,
            posizione: posizione,
            titoloMediaIt1: titoloMediaIt1,
            titoloMediaEn1: titoloMediaEn1,
            titoloMediaIt2: titoloMediaIt2,
            titoloMediaEn2: titoloMediaEn2,
            titoloMediaIt3: titoloMediaIt3,
            titoloMediaEn3: titoloMediaEn3,
            titoloMediaIt4: "",
            titoloMediaEn4: "",
            descrizioneMediaIt1: descrizioneMediaIt1,
            descrizioneMediaEn1: descrizioneMediaEn1,
            descrizioneMediaIt2: descrizioneMediaIt2,
            descrizioneMediaEn2: descrizioneMediaEn2,
            descrizioneMediaIt3: descrizioneMediaIt3,
            descrizioneMediaEn3: descrizioneMediaEn3,
            descrizioneMediaIt4: "",
            descrizioneMediaEn4: "",
            update: me.update,
            idCompetenza: me.currentIdCompetenza,
            idPage: me.currentIdPage,
            callback: function(utente_id){

                $('#btNuovaSezione').removeClass('disabled');
                $('.actionButton').removeClass('disabled');
                me.modalBootbox.modal("hide");

                //alert("ho salvato");

                new moduloCompetenze({
                    canWrite: true,
                    updateCallback: me.currentIdPage
                });

            }

        });
                                  
        return false;
    }
});

