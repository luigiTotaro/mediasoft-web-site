var moduloHightlights = Backbone.View.extend({

    tagName : "div",
    className: '',
    canWrite: null,
   
    // attributi applicabili all'oggetto DOM root della vista (in questo caso il div contenitore)
    attributes: {
        'id': 'div_moduloHightlight'
    },

    events: {
        'click #btNuovoHightlight': 'doCreateHightlight',
        'click [data-role-edit-hightlight-id]' : 'doEditHightlight',
        'click [data-role-delete-hightlight-id]' : 'doDeleteHightlight'
    },

    initialize: function(options) {
        this.canWrite = options.canWrite;
        this.render();
    },

    render : function(jsonData) {
       
        var header='<div class="page-head">'+
                        '<div class="page-title">'+
                          '<h1>Gestione Highlights <small> in questa sezione &egrave; possibile inserire, modificare o eliminare Highlights della homepage</small></h1>'+
                        '</div>'+
                '<div>';

       var html = '<div class="portlet box red-sunglo">' +
                        '<div class="portlet-title">' +
                            '<div class="caption">' +
                                '<i class="fa fa-play"></i> Highlights'+
                            '</div>' +

                            '<div class="actions" id="strumentAction">' +  
                                '<div id="actionCreate" class="btn-group">'+                        
                                  '<a href="#" class="btn btn-default btn-sm" id="btNuovoHightlight">' +
                                  '<i class="fa fa-pencil"></i> Nuovo Highlight</a>' +
                                '</div>'+
                            '</div>' +
                          
                        '</div>'+
                        '<div class="portlet-body">'+
                            '<table class="table table-striped table-bordered table-hover" id="tableHL" >'+
                            '<div id="divFiltra"></div>'+
                            '<thead>'+
                            '<tr>'+
                                '<th>Immagine</th>'+
                                '<th>Titolo IT</th>'+
                                '<th>Link</th>'+
                                '<th></th>'+
                            '</tr>'+
                            '</thead>'+
                            '<tbody id="tableData">'+
                            '</tbody>'+

                            '</table>'+
                            
                       ' </div>'+
                   ' </div>';
        this.$el.empty();
        $('#applicationContainer').empty();
        this.$el.append(header);
        this.$el.append(html);
        $('#applicationContainer').append(this.$el);
           
        this.showData();

    },

    showData: function(filtro){
        var me = this;

        HightlightsController.getHightlightsList({

            callback: function(jsonData){

                $('#tableData').empty();
                for(var i in jsonData) {

                    var myStringLink = jsonData[i]['link'];
                    if(myStringLink.length > 50){
                        var myTruncatedString = myStringLink.substring(0,50)+"...";
                    }else{
                        var myTruncatedString = jsonData[i]['link'];
                    }

                    $('#tableData').append(
                                '<tr class="odd gradeX" id="riga_'+jsonData[i]['id']+'">'+
                                    '<td><a href="../images/'+ jsonData[i]['path'] +'" target="_blank">' + jsonData[i]['path'] + '</a></td>'+
                                    '<td>' + jsonData[i]['titolo'] + '</td>'+
                                    '<td>' + myTruncatedString + '</td>'+
                                    '<td style="width: 50px;">' + 
                                        '<span class="actionButton" style="padding: 0 10px 0 0; !important"><a href="#" data-role-edit-hightlight-id="' + jsonData[i]['id'] + '"><i class="icon-pencil"></i></a></span>' + 
                                        '<span class="actionButton"><a href="#" data-role-delete-hightlight-id="' + jsonData[i]['id'] + '" >'+
                                        '<i class="fa fa-trash"></i></a></span>'+ 
                                    '</td>'+
                                '</tr>');
                }


                if(!$('#tableHL').hasClass('initialized')){ 
                    //Prima carico i dati e dopo inizializzo la tabella altrimenti non funziona la ricerca
                    var columnSort = [];
                    columnSort.push({"orderable" : false}),
                    columnSort.push({"orderable" : true}),
                    columnSort.push({"orderable" : true}),
                    columnSort.push({"orderable" : false}),
                    manageDatatable($('#tableHL'),$('#tableHL'), columnSort);
                    $('#tableHL').not('.initialized').addClass('initialized');
                }
            }

        });

    },

    doCreateHightlight: function(){

        console.log("sono qui")

        event.stopImmediatePropagation();
        
        if (!$('#btNuovoHightlight').hasClass('disabled')){

            $('#btNuovoHightlight').addClass('disabled');
            
            new moduloHightlightsDetail({
                update: false,
                canWrite: true
            });
        }
       
    },
    
    doEditHightlight: function(event){
        
        event.stopImmediatePropagation();

        if (!$('.actionButton').hasClass('disabled')){

            var idHightlight= $(event.currentTarget).attr('data-role-edit-hightlight-id');

            $('#btNuovoHightlight').addClass('disabled');
            $('.actionButton').addClass('disabled');

            new moduloHightlightsDetail({
                update: true,
                idHightlight: idHightlight,
                canWrite: true
            });

        }
    },

    doDeleteHightlight: function(event){

        var me = this;
        var currentHightlightId = $(event.currentTarget).attr('data-role-delete-hightlight-id');
        var messaggio = "Si desidera eliminare l'hightlight selezionato?" 
       
        var confirmDelete = bootbox.dialog({
            message: messaggio,
            title: "Attenzione!",
            backdrop: true,
            closeButton: true,
            animate: false,
            buttons: {
                conferma: {
                  label: "Conferma",
                  className: "btn-primary",
                  callback: function() {
                            
                    HightlightsController.deleteHightlight({
                        idHightlight: currentHightlightId,
                        
                        callback: function() {
                             confirmDelete.modal("hide");
                             //$('#riga_'+currentHightlightId).remove();

                            new moduloHightlights({
                                canWrite: true
                            });

                        }
                    });
                    return false;
                  }
                   
                },
                cancel: {
                  label: "Annulla",
                  className: "btn-success",
                  callback: function() {
                  }
                }
            }
        });
    }

});

