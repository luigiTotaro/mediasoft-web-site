var moduloTimeline = Backbone.View.extend({

    tagName : "div",
    className: '',
    canWrite: null,
   
    // attributi applicabili all'oggetto DOM root della vista (in questo caso il div contenitore)
    attributes: {
        'id': 'div_moduloTimeline'
    },

    events: {
        'click #btNuovoTlItem': 'doCreateTlItem',
        'click [data-role-edit-timeline-id]' : 'doEditTimeline',
        'click [data-role-delete-timeline-id]' : 'doDeleteTimeline'
    },

    initialize: function(options) {
        this.canWrite = options.canWrite;
        this.render();
    },

    render : function(jsonData) {
       
        var header='<div class="page-head">'+
                        '<div class="page-title">'+
                          '<h1>Gestione Timeline <small> in questa sezione &egrave; possibile inserire, modificare o eliminare elementi della timeline dalla pagina "About Us"</small></h1>'+
                        '</div>'+
                '<div>';

       var html = '<div class="portlet box red-sunglo">' +
                        '<div class="portlet-title">' +
                            '<div class="caption">' +
                                '<i class="fa fa-history"></i> Timeline'+
                            '</div>' +

                            '<div class="actions" id="strumentAction">' +  
                                '<div id="actionCreate" class="btn-group">'+                        
                                  '<a href="#" class="btn btn-default btn-sm" id="btNuovoTlItem">' +
                                  '<i class="fa fa-pencil"></i> Nuovo Elemento</a>' +
                                '</div>'+
                            '</div>' +
                          
                        '</div>'+
                        '<div class="portlet-body">'+
                            '<table class="table table-striped table-bordered table-hover" id="tableTL" >'+
                            '<div id="divFiltra"></div>'+
                            '<thead>'+
                            '<tr>'+
                                '<th>Immagine</th>'+
                                '<th>Titolo IT</th>'+
                                '<th>Anno</th>'+
                                '<th>Progressivo</th>'+
                                '<th></th>'+
                            '</tr>'+
                            '</thead>'+
                            '<tbody id="tableData">'+
                            '</tbody>'+

                            '</table>'+
                            
                       ' </div>'+
                   ' </div>';
        this.$el.empty();
        $('#applicationContainer').empty();
        this.$el.append(header);
        this.$el.append(html);
        $('#applicationContainer').append(this.$el);
           
        this.showData();

    },

    showData: function(filtro){
        var me = this;

        TimelineController.getTimelineList({

            callback: function(jsonData){

                $('#tableData').empty();
                for(var i in jsonData) {

                    $('#tableData').append(
                                '<tr class="odd gradeX" id="riga_'+jsonData[i]['id']+'">'+
                                    '<td><a href="../images/'+ jsonData[i]['path'] +'" target="_blank">' + jsonData[i]['path'] + '</a></td>'+
                                    '<td>' + jsonData[i]['titolo'] + '</td>'+
                                    '<td>' + jsonData[i]['anno'] + '</td>'+
                                    '<td>' + jsonData[i]['progressivo'] + '</td>'+
                                    '<td style="width: 50px;">' + 
                                        '<span class="actionButton" style="padding: 0 10px 0 0; !important"><a href="#" data-role-edit-timeline-id="' + jsonData[i]['id'] + '"><i class="icon-pencil"></i></a></span>' + 
                                        '<span class="actionButton"><a href="#" data-role-delete-timeline-id="' + jsonData[i]['id'] + '" >'+
                                        '<i class="fa fa-trash"></i></a></span>'+ 
                                    '</td>'+
                                '</tr>');
                }


                if(!$('#tableTL').hasClass('initialized')){ 
                    //Prima carico i dati e dopo inizializzo la tabella altrimenti non funziona la ricerca
                    var columnSort = [];
                    columnSort.push({"orderable" : false}),
                    columnSort.push({"orderable" : true}),
                    columnSort.push({"orderable" : true}),
                    columnSort.push({"orderable" : true}),
                    columnSort.push({"orderable" : false}),
                    manageDatatable($('#tableTL'),$('#tableTL'), columnSort);
                    $('#tableTL').not('.initialized').addClass('initialized');
                }
            }

        });

    },

    doCreateTlItem: function(){
        event.stopImmediatePropagation();
        
        if (!$('#btNuovoTlItem').hasClass('disabled')){

            $('#btNuovoTlItem').addClass('disabled');
            
            new moduloTimelineDetail({
                update: false,
                canWrite: true
            });
        }
       
    },
    
    doEditTimeline: function(event){
        
        event.stopImmediatePropagation();

        if (!$('.actionButton').hasClass('disabled')){

            var idTimeline= $(event.currentTarget).attr('data-role-edit-timeline-id');

            $('#btNuovoTlItem').addClass('disabled');
            $('.actionButton').addClass('disabled');

            new moduloTimelineDetail({
                update: true,
                idTimeline: idTimeline,
                canWrite: true
            });

        }
    },

    doDeleteTimeline: function(event){

        var me = this;
        var currentTimelineId = $(event.currentTarget).attr('data-role-delete-timeline-id');
        var messaggio = "Si desidera eliminare l'elemento selezionato?" 
       
        var confirmDelete = bootbox.dialog({
            message: messaggio,
            title: "Attenzione!",
            backdrop: true,
            closeButton: true,
            animate: false,
            buttons: {
                conferma: {
                  label: "Conferma",
                  className: "btn-primary",
                  callback: function() {
                            
                    TimelineController.deleteTimeline({
                        idTimeline: currentTimelineId,
                        
                        callback: function() {
                            confirmDelete.modal("hide");
                             //$('#riga_'+currentHightlightId).remove();

                            new moduloTimeline({
                                canWrite: true
                            });

                        }
                    });
                    return false;
                  }
                   
                },
                cancel: {
                  label: "Annulla",
                  className: "btn-success",
                  callback: function() {
                  }
                }
            }
        });
    }

});

