function writeToFile(DATA){
    var url = "phpService/write.php";
    var XML = "../../lib/frontone.js";
    if (window.XMLHttpRequest)
    {// code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    }
    else
    {// code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }

    xmlhttp.open("POST",url,true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send("D="+DATA+"&F="+XML);
    //xmlhttp.send("D="+DATA+'&F='+XML);
}

var moduloFrontoneProdotti = Backbone.View.extend({

    tagName : "div",
    className: '',
    canWrite: null,
    updateFrontone: false,
    currentIdProdotto: 2,
    pathImageFrontone: '',
    tipoFile: '2',
    modalBootbox: null,
   
    // attributi applicabili all'oggetto DOM root della vista (in questo caso il div contenitore)
    attributes: {
        'id': 'div_moduloProdotto'
    },

    events: {
       
    },

    initialize: function(options) {
        this.canWrite = options.canWrite;
        this.render();
    },

    render : function(jsonData) {

        var me = this;
       
        var header= '<div class="page-head">'+
                        '<div class="page-title">'+
                          '<h1>Frontone Prodotti <small> in questa sezione &egrave; possibile gestire il Frontone della pagina "Products"</small></h1>'+
                        '</div>'+
                    '<div>';

       var html = '<div class="portlet box red-sunglo">' +
                        '<div class="portlet-title">' +
                            '<div class="caption">' +
                                '<i class="fa fa-cubes"></i> Frontone Pagina Prodotti'+
                            '</div>' +
                        '</div>'+

                        '<div class="portlet-body">'+

                            '<div class="row">'+
                                '<div class="col-md-2">'+
                                    '<label class="control-label">Testo IT (*)</label>'+
                                '</div>'+
                                '<div class="col-md-4">'+
                                   '<div class="form-group">'+
                                        '<input type="text" id="txtFrontoneTestoIT" name="txtFrontoneTestoIT" placeholder="Titolo it ..." value="" class="form-control">'+
                                        '<span class="text-danger error" id="itScreenshotError" ></span>'+
                                    '</div>'+
                                '</div>'+                                            
                                '<div class="col-md-2">'+
                                    '<label class="control-label">Testo EN (*)</label>'+
                                '</div>'+
                                '<div class="col-md-4">'+
                                   '<div class="form-group">'+
                                        '<input type="text" id="txtFrontoneTestoEN" name="txtFrontoneTestoEN" placeholder="Titolo en ..." value="" class="form-control">'+
                                        '<span class="text-danger error" id="enScreenshotError" ></span>'+
                                    '</div>'+
                                '</div>'+                                            
                            '</div>'+ 

                            '<div class="row">'+
                                '<div class="col-md-2">'+
                                    '<label class="control-label">File (*)</label>'+
                                '</div>'+
                                '<div class="col-md-4">'+
                                    '<div class="form-group">'+
                                        '<input id="selectedFileFrontone" name="selectedFileFrontone" class="file" type="file" style="display:none;">'+
                                        '<span style="font-size:80%">Dimensione max '+(this.tipoFile==2 ?  "2 MB." : "5 MB.")+'</span><br>'+
                                        '<span style="font-size:80%">Per una visualizzazione ottimale caricare foto di dimensione 1300px X 455px.</span><br>'+
                                        '<span class="text-danger error" id="imgErrorFrontone" ></span>'+
                                    '</div>'+
                                '</div>'+   
                                '<div class="col-md-6">'+
                                    '<button type="button" id="saveFrontone" class="btn btn-primary">Upload Nuovo Frontone</button>'+
                                '</div>'+                                 
                            '</div>'+ 

                            '<div class="row">'+
                                '<div class="col-md-12">'+
                                    '<label class="control-label">La funzione di upload permette di caricare sul server le immagini nella cartella "/frontone/" di root.<br>'+
                                    'Per modificare lo slide di frontone, editare il json sottostante gestendo gli elementi dell\'array "slides" e impostando come path "frontone/{nomefile}".<br>'+
                                    '</label>'+
                                '</div>'+
                            '</div>'+

                            '<div class="row">'+
                                '<div class="col-md-12">'+
                                '<table class="table table-striped table-bordered table-hover" id="tableFrontone" >'+
                                    '<div id="divFiltraFrontone"></div>'+
                                    '<thead>'+
                                    '<tr>'+
                                        '<th>Path</th>'+
                                        '<th>Titolo IT</th>'+
                                        '<th></th>'+
                                    '</tr>'+
                                    '</thead>'+
                                    '<tbody id="tableDataFrontone">'+
                                    '</tbody>'+
                                '</table>'+
                                '</div>'+
                            '</div>'+

                            '<div class="row">'+
                                '<div class="col-md-12">'+
                                    '<form>'+
                                      '<textarea id="textArea" style="width:100%;height:500px"></textarea>'+
                                      '<input type="button" style="margin-right:10px" id="restoreFile" class="btn green btn-primary" value="Restore" />'+
                                      '<input type="button" id="saveFile" class="btn btn-primary" value="Save" />'+
                                    '</form>'+
                                '</div>'+
                            '</div>'+
                            
                       ' </div>'+
                   ' </div>';
        this.$el.empty();
        $('#applicationContainer').empty();
        this.$el.append(header);
        this.$el.append(html);
        $('#applicationContainer').append(this.$el);

        //edit json
        $.ajax({
           url : "../lib/frontone.js",
           dataType: "text",
           success : function (data) {
               $("#textArea").text(data);
               $("#textArea").val(data);
           }
        });
        
        var buttonReset = document.querySelector("#restoreFile");
        buttonReset.onclick = function(e) {
            console.log("sono quidddddd")
            $.ajax({
               url : "../lib/frontoneDefault.js",
               dataType: "text",
               success : function (data) {

                    $("#textArea").text(data);
                    $("#textArea").val(data);

               }
            });
        }

        var text = document.querySelector("#textArea");
        var button = document.querySelector("#saveFile");
        button.onclick = function(e) {
            writeToFile(text.value);
            bootbox.alert("Dati salvati con successo");
        }

        var _initialPreview = [];
        var _allowedFileExtensions = [];
        var _maxFileSize="";

        //_allowedFileExtensions = '';
        _allowedFileExtensions = ['jpg', 'png','gif'];
        _maxFileSize= 2000;      

        $("#selectedFileFrontone").fileinput({
            allowedFileExtensions : _allowedFileExtensions,
            uploadAsync: true,
            minFileCount: 0,
            maxFileCount: 1,
            maxFileSize: _maxFileSize,
            removeLabel: 'Rimuovi',
            removeTitle: 'Rimuovi file selezionato',
            cancelLabel: 'Annulla',
            cancelTitle: 'Annulla upload',
            uploadLabel: 'Upload',
            uploadTitle: 'Upload file selezionato',
            browseLabel: 'Seleziona',
            msgSelected: '{n} files selezionato',
            dropZoneEnabled: false,
            msgInvalidFileType: 'Formato del file "{name}" non supportato. Sono supportati solo file di tipo "{types}".',
            msgInvalidFileExtension: 'Estensione del file "{name}" non supportata. Sono supportati solo file di tipo "{extensions}".',
            msgSizeTooLarge:'La dimesione del file "{name}" (<b>{size} KB</b>) eccede il limite imposto <b>{maxSize} KB</b>.',
            uploadUrl: './phpService/ProdottiFrontoneServices.php?action=uploadFrontone',
            initialPreview: _initialPreview,
            layoutTemplates: {
                main1: '' +
                        '<div class="kv-upload-progress hide"></div>\n' +
                        '<div class="input-group {class}">\n' +
                        '   {caption}\n' +
                        '   <div class="input-group-btn">\n' +
                        '       <a id="btnEmptyFileManagerFrontone" href="#" class="btn btn-default"><i class="fa fa-trash-o"></i> Rimuovi</a>\n' +
                        '       <a id="btnAddFileManagerFrontone" href="#" class="btn btn-default"><i class="fa fa-upload"></i> Seleziona un file</a>\n' +
                        '   </div>\n' +
                        '</div>',                     
                icon: '<span class="glyphicon glyphicon-picture kv-caption-icon"></span>',
                actions: '',
                footer: ''
            }       
        }).on('fileuploaded', function(event, data, previewId, index) {
                
            $("#selectedFileFrontone").attr('data-name', data.response.nuovoFile);
            var newFile = data.response.nuovoFile;
            me.pathImageFrontone = newFile.replace("../../frontone/", "");
            me.insertOrUpdateFrontone();

        }).on('filebatchuploadsuccess', function(event, data, previewId, index) {

            $("#selectedFileFrontone").attr('data-name', data.response.nuovoFile);
            var newFile = data.response.nuovoFile;
            me.pathImageFrontone = newFile.replace("../../frontone/", "");
            me.insertOrUpdateFrontone();
            
        }).on('filecleared', function(event) {
            //se rimuovo una immagine esistente non faccio upload ma devo rimuovere il file e aggiornare la path
            me.pathImageFrontone = "";    
        });
       
        $('#btnEmptyFileManagerFrontone').click(function(evt) {
            $("#selectedFileFrontone").fileinput('clear');
        });

        $('#btnAddFileManagerFrontone').click(function(evt) {
            $("#selectedFileFrontone").click();
        });

        this.showFrontone();

        $('#saveFrontone').click(function(event){
           me.innerUploadFrontone();
        });

    },

    //Frontone
    showFrontone: function(){
        var me = this;
        ProdottiFrontoneController.getMediaByIdProdotto({
            idProdotto:  me.currentIdProdotto,
            callback: function(jsonData) { 

                $('#tableDataFrontone').empty();
                for(var i in jsonData) {

                    if(jsonData[i]['path'] == "evoluzione.jpg"){
                        continue;
                    }else{
                        $('#tableDataFrontone').append(
                        '<tr class="odd gradeX" id="rigaFrontone_'+jsonData[i]['id']+'">'+
                            '<td><a href="../frontone/'+ jsonData[i]['path'] +'" target="_blank">' + jsonData[i]['path'] + '</a></td>'+
                            '<td>' + jsonData[i]['titoloIT'] + '</td>'+
                            '<td style="width: 20px;">' + 
                                '<span class="actionButton"><a href="#" data-role-delete-Frontone-name="'+jsonData[i]['path']+'" data-role-delete-Frontone-id="' + jsonData[i]['id'] + '" >'+
                                '<i class="fa fa-trash"></i></a></span>'+ 
                            '</td>'+
                        '</tr>');
                    }
                    
                }

                if(!$('#tableFrontone').hasClass('initialized')){ 
                    //Prima carico i dati e dopo inizializzo la tabella altrimenti non funziona la ricerca
                    var columnSort = [];
                    columnSort.push({"orderable" : false}),
                    columnSort.push({"orderable" : false}),
                    columnSort.push({"orderable" : false}),
                    manageDatatable2($('#tableFrontone'),$('#tableFrontone'), columnSort);
                    $('#tableFrontone').not('.initialized').addClass('initialized');
                }

                $('[data-role-delete-Frontone-id]').click(function(event){
                   me.doDeleteFrontone($(this).attr("data-role-delete-Frontone-id"),$(this).attr("data-role-delete-Frontone-name"));
                });
 
            } 
        });
    },

    innerUploadFrontone: function(){
        var path = $('#selectedFileFrontone').val();
        if(path == ''){
            alert("Selezionare un file.");
        }else{
            $("#selectedFileFrontone").fileinput('upload');
        } 
    },

    resetCampiFrontone: function(){
        var me = this;
        me.updateFrontone = false;
        me.pathImageFrontone = "";
        $("#selectedFileFrontone").fileinput('clear');
        $('#txtFrontoneTestoIT').val("");
        $('#txtFrontoneTestoEN').val("");
    },

    insertOrUpdateFrontone: function(){

        var me = this;
        var path = me.pathImageFrontone;
        var titoloIt = $('#txtFrontoneTestoIT').val();
        var titoloEn = $('#txtFrontoneTestoEN').val();

        $('.error').empty();
        var errori = false;
       
        if(path == "") {
            $('#imgErrorFrontone').append('<em>(Immagine obbligatoria)</em>');
            errori = true;
        }

        if(errori)
            return false;

        ProdottiFrontoneController.insertOrUpdateFrontone({
            path: path,
            titoloIt: titoloIt,
            titoloEn: titoloEn,
            updateFrontone: me.updateFrontone,
            idProdotto: me.currentIdProdotto,
            idFrontone: '',
            callback: function(utente_id){
                me.showFrontone();
                me.resetCampiFrontone();
            }
        });
    },

    doDeleteFrontone: function(id,path){

        var me = this;
        var currentFrontoneId = id;
        var messaggio = "Si desidera eliminare lo Frontone selezionato?" 
       
        var confirmDelete = bootbox.dialog({
            message: messaggio,
            title: "Attenzione!",
            backdrop: true,
            closeButton: true,
            animate: false,
            buttons: {
                conferma: {
                  label: "Conferma",
                  className: "btn-primary",
                  callback: function() {         
                    ProdottiFrontoneController.deleteFrontone({
                        idFrontone: currentFrontoneId,
                        path: path,
                        callback: function() {
                             confirmDelete.modal("hide");
                             $('#rigaFrontone_'+currentFrontoneId).remove();
                        }
                    });
                    return false;
                  }
                },
                cancel: {
                  label: "Annulla",
                  className: "btn-success",
                  callback: function() {
                  }
                }
            }
        });
    },


});

