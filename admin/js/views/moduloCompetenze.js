var moduloCompetenze = Backbone.View.extend({

    tagName : "div",
    className: '',
    canWrite: null,
    currentIdCompetenza: 0,
    updateCallback: null,
   
    // attributi applicabili all'oggetto DOM root della vista (in questo caso il div contenitore)
    attributes: {
        'id': 'div_moduloCompetenza'
    },

    events: {
        'click #goBackCompetenze': 'goBackCompetenze',
        'click [data-role-edit-competenza-id]' : 'doEditCompetenza',
        'click #btNuovaSezione' : 'doCreateSezione',
        'click [data-role-edit-sezione-id]' : 'doEditSezione',
        'click [data-role-delete-sezione-id]' : 'doDeleteSezione'
    },

    initialize: function(options) {
        this.canWrite = options.canWrite;
        this.updateCallback = options.updateCallback;
        this.render();
    },

    render : function(jsonData) {
       
        var header='<div class="page-head">'+
                        '<div class="page-title">'+
                          '<h1>Gestione Competenze <small> in questa sezione &egrave; possibile modificare il contenuto delle pagine "Skill/Competenze"</small></h1>'+
                        '</div>'+
                    '<div>';

        var html = '<div id="portletCompetenze" class="portlet box red-sunglo">' +
                        '<div class="portlet-title">' +
                            '<div class="caption">' +
                                '<i class="fa fa-cubes"></i>Seleziona una Pagina'+
                            '</div>' +
                        '</div>'+

                        //pagine competenze 
                        '<div class="portlet-body">'+
                            '<table class="table table-striped table-bordered table-hover" id="tableHL" >'+
                            '<div id="divFiltra"></div>'+
                            '<thead>'+
                            '<tr>'+
                                '<th>Nome</th>'+
                                '<th></th>'+
                            '</tr>'+
                            '</thead>'+
                            '<tbody id="tableData">'+
                            '</tbody>'+
                            '</table>'+
                       ' </div>'+

                    '</div>'+

                    '<div id="portletSezioni" class="portlet box red-sunglo" style="display:none">' +
                        '<div class="portlet-title">' +
                            '<div class="caption" id="portletSezioniTitle">' +
                                //'<i class="fa fa-cubes" ></i>'+
                            '</div>' +

                            '<div class="actions" id="strumentAction">' +  
                                '<div id="actionCreate" class="btn-group">'+                        
                                  '<a href="#" class="btn btn-default btn-sm" id="btNuovaSezione">' +
                                  '<i class="fa fa-pencil"></i> Nuova Sezione</a>' +
                                '</div>&nbsp;'+
                                '<div id="actionCreate2" style="margin-left:10px" class="btn-group">'+                        
                                  '<a href="#" class="btn btn-default btn-sm" id="goBackCompetenze">' +
                                  '<i class="fa fa-pencil"></i> Torna a lista competenze</a>' +
                                '</div>'+
                            '</div>' +
                          
                        '</div>'+

                       //sezione di pagina 
                       '<div class="portlet-body">'+
                            '<table class="table table-striped table-bordered table-hover" id="tableHL2" >'+
                            '<div id="divFiltra2"></div>'+
                            '<thead>'+
                            '<tr>'+
                                '<th>Nome</th>'+
                                '<th></th>'+
                            '</tr>'+
                            '</thead>'+
                            '<tbody id="tableData2">'+
                            '</tbody>'+
                            '</table>'+
                       ' </div>'+

                   ' </div>';

        this.$el.empty();
        $('#applicationContainer').empty();
        this.$el.append(header);
        this.$el.append(html);
        $('#applicationContainer').append(this.$el);
           
        this.showPage();

    },

    showPage: function(filtro){
        var me = this;

        CompetenzeController.getCompetenzeList({

            callback: function(jsonData){

                $('#tableData').empty();
                for(var i in jsonData) {

                    $('#tableData').append(
                                '<tr class="odd gradeX" id="riga_'+jsonData[i]['id']+'">'+
                                    '<td>' + jsonData[i]['titolo'] + '</td>'+
                                    '<td style="width: 20px;">' + 
                                        '<span class="actionButton" style="padding: 0 10px 0 0; !important"><a href="#" data-role-name-competenza="' + jsonData[i]['titolo'] + '" data-role-edit-competenza-id="' + jsonData[i]['id'] + '"><i class="icon-pencil"></i></a></span>' + 
                                        //'<span class="actionButton"><a href="#" data-role-delete-Competenza-id="' + jsonData[i]['id'] + '" >'+
                                        //'<i class="fa fa-trash"></i></a></span>'+ 
                                    '</td>'+
                                '</tr>');
                }


                if(!$('#tableHL').hasClass('initialized')){ 
                    //Prima carico i dati e dopo inizializzo la tabella altrimenti non funziona la ricerca
                    var columnSort = [];
                    columnSort.push({"orderable" : true}),
                    columnSort.push({"orderable" : false}),
                    manageDatatable2($('#tableHL'),$('#tableHL'), columnSort);
                    $('#tableHL').not('.initialized').addClass('initialized');
                }

                //$('a[data-role-edit-competenza-id="10"]').trigger('click');

                    console.log("me.updateCallback");
                    console.log(me.updateCallback);

                    if(me.updateCallback !=null){

                        console.log("sono in callback click");
                        //$('a[data-role-edit-competenza-id="'+me.updateCallback+'"]').click();
                        $('a[data-role-edit-competenza-id="'+me.updateCallback+'"]').trigger('click');

                    }

            }

        });   

    },

    showSezioni: function(idPage){
        var me = this;

        CompetenzeController.getSezioniByPage({
            idPage: idPage,
            callback: function(jsonData){

                console.log(jsonData)

                $('#tableData2').empty();
                for(var i in jsonData) {

                    $('#tableData2').append(
                                '<tr class="odd gradeX" id="riga2_'+jsonData[i]['id']+'">'+
                                    '<td>' + jsonData[i]['titolo'] + '</td>'+
                                    '<td style="width: 50px;">' + 
                                        '<span class="actionButton" style="padding: 0 10px 0 0; !important"><a href="#" data-role-edit-sezione-id="' + jsonData[i]['id'] + '"><i class="icon-pencil"></i></a></span>' + 
                                        '<span class="actionButton"><a href="#" data-role-delete-sezione-id="' + jsonData[i]['id'] + '" >'+
                                        '<i class="fa fa-trash"></i></a></span>'+ 
                                    '</td>'+
                                '</tr>');
                }


                if(!$('#tableHL2').hasClass('initialized')){ 
                    //Prima carico i dati e dopo inizializzo la tabella altrimenti non funziona la ricerca
                    var columnSort = [];
                    columnSort.push({"orderable" : true}),
                    columnSort.push({"orderable" : false}),
                    manageDatatable2($('#tableHL2'),$('#tableHL2'), columnSort);
                    $('#tableHL2').not('.initialized').addClass('initialized');
                }
            }

        });

    },

    goBackCompetenze: function(event){
        var me = this;
        me.currentIdCompetenza = 0;

        $("#portletCompetenze").show();
        $("#portletSezioni").hide(); 
    },
    
    doEditCompetenza: function(event){

        console.log(event);

        var me = this;
        var idCompetenza= $(event.currentTarget).attr('data-role-edit-competenza-id');
        var titoloCompetenza= $(event.currentTarget).attr('data-role-name-competenza');

        me.currentIdCompetenza = idCompetenza;

        $("#portletCompetenze").hide();
        $("#portletSezioni").show();

        $("#portletSezioniTitle").html("Gestione sezioni della pagina: "+titoloCompetenza);

        me.showSezioni(idCompetenza);
    },

    doCreateSezione: function(){
        var me = this;

        event.stopImmediatePropagation();
        
        if (!$('#btNuovaSezione').hasClass('disabled')){

            $('#btNuovaSezione').addClass('disabled');
            
            new moduloCompetenzeDetail({
                update: false,
                canWrite: true,
                idPage: me.currentIdCompetenza
            });
        }
       
    },

    doEditSezione: function(event){

        var me = this;
        
        event.stopImmediatePropagation();

        if (!$('.actionButton').hasClass('disabled')){

            var idCompetenza= $(event.currentTarget).attr('data-role-edit-sezione-id'); //sezione di competenza

            $('#btNuovaSezione').addClass('disabled');
            $('.actionButton').addClass('disabled');

            new moduloCompetenzeDetail({
                update: true,
                idCompetenza: idCompetenza,
                idPage: me.currentIdCompetenza,
                canWrite: true
            });

        }
    },

    doDeleteSezione: function(event){

        var me = this;
        var currentCompetenzaId = $(event.currentTarget).attr('data-role-delete-sezione-id');
        var messaggio = "Si desidera eliminare la sezione selezionata?" 
       
        var confirmDelete = bootbox.dialog({
            message: messaggio,
            title: "Attenzione!",
            backdrop: true,
            closeButton: true,
            animate: false,
            buttons: {
                conferma: {
                  label: "Conferma",
                  className: "btn-primary",
                  callback: function() {
                            
                    CompetenzeController.deleteCompetenza({
                        idCompetenza: currentCompetenzaId,
                        
                        callback: function() {
                             confirmDelete.modal("hide");
                             $('#riga2_'+currentCompetenzaId).remove();

                            //new moduloCompetenze({
                            //    canWrite: true
                            //});

                        }
                    });
                    return false;
                  }
                   
                },
                cancel: {
                  label: "Annulla",
                  className: "btn-success",
                  callback: function() {
                  }
                }
            }
        });
    }

});

