var moduloRD = Backbone.View.extend({

    tagName : "div",
    className: '',
    canWrite: null,
   
    // attributi applicabili all'oggetto DOM root della vista (in questo caso il div contenitore)
    attributes: {
        'id': 'div_moduloRD'
    },

    events: {
        'click #btNuovoRDItem': 'doCreateRDItem',
        'click [data-role-edit-RD-id]' : 'doEditRD',
        'click [data-role-delete-RD-id]' : 'doDeleteRD'
    },

    initialize: function(options) {
        this.canWrite = options.canWrite;
        this.render();
    },

    render : function(jsonData) {
       
        var header='<div class="page-head">'+
                        '<div class="page-title">'+
                          '<h1>Gestione R&D <small> in questa sezione &egrave; possibile inserire, modificare o eliminare elementi dalla pagina "R&D"</small></h1>'+
                        '</div>'+
                    '<div>';

       var html = '<div class="portlet box red-sunglo">' +
                        '<div class="portlet-title">' +
                            '<div class="caption">' +
                                '<i class="fa fa-history"></i> RD'+
                            '</div>' +

                            '<div class="actions" id="strumentAction">' +  
                                '<div id="actionCreate" class="btn-group">'+                        
                                  '<a href="#" class="btn btn-default btn-sm" id="btNuovoRDItem">' +
                                  '<i class="fa fa-pencil"></i> Nuovo Elemento</a>' +
                                '</div>'+
                            '</div>' +
                          
                        '</div>'+
                        '<div class="portlet-body">'+
                            '<table class="table table-striped table-bordered table-hover" id="tableRD" >'+
                            '<div id="divFiltra"></div>'+
                            '<thead>'+
                            '<tr>'+
                                '<th>Titolo IT</th>'+
                                '<th>Link</th>'+
                                '<th>Progressivo</th>'+
                                '<th></th>'+
                            '</tr>'+
                            '</thead>'+
                            '<tbody id="tableData">'+
                            '</tbody>'+

                            '</table>'+
                            
                       ' </div>'+
                   ' </div>';
        this.$el.empty();
        $('#applicationContainer').empty();
        this.$el.append(header);
        this.$el.append(html);
        $('#applicationContainer').append(this.$el);
           
        this.showData();

    },

    showData: function(filtro){
        var me = this;

        RDController.getRDList({

            callback: function(jsonData){

                $('#tableData').empty();
                for(var i in jsonData) {

                    var myStringLink = jsonData[i]['link'];
                    if(myStringLink.length > 50){
                        var myTruncatedString = myStringLink.substring(0,50)+"...";
                    }else{
                        var myTruncatedString = jsonData[i]['link'];
                    }

                    $('#tableData').append(
                                '<tr class="odd gradeX" id="riga_'+jsonData[i]['id']+'">'+
                                    '<td>' + jsonData[i]['titolo'] + '</td>'+
                                    '<td>' + myTruncatedString + '</td>'+
                                    '<td>' + jsonData[i]['progressivo'] + '</td>'+
                                    '<td style="width: 60px;">' + 
                                        '<span class="actionButton" style="padding: 0 10px 0 0; !important"><a href="#" data-role-edit-RD-id="' + jsonData[i]['id'] + '"><i class="icon-pencil"></i></a></span>' + 
                                        '<span class="actionButton"><a href="#" data-role-delete-RD-id="' + jsonData[i]['id'] + '" >'+
                                        '<i class="fa fa-trash"></i></a></span>'+ 
                                    '</td>'+
                                '</tr>');
                }


                if(!$('#tableRD').hasClass('initialized')){ 
                    //Prima carico i dati e dopo inizializzo la tabella altrimenti non funziona la ricerca
                    var columnSort = [];
                    columnSort.push({"orderable" : true}),
                    columnSort.push({"orderable" : true}),
                    columnSort.push({"orderable" : true}),
                    columnSort.push({"orderable" : false}),
                    manageDatatable($('#tableRD'),$('#tableRD'), columnSort);
                    $('#tableRD').not('.initialized').addClass('initialized');
                }
            }

        });

    },

    doCreateRDItem: function(){
        event.stopImmediatePropagation();
        
        if (!$('#btNuovoRDItem').hasClass('disabled')){

            $('#btNuovoRDItem').addClass('disabled');
            
            new moduloRDDetail({
                update: false,
                canWrite: true
            });
        }
       
    },
    
    doEditRD: function(event){
        
        event.stopImmediatePropagation();

        if (!$('.actionButton').hasClass('disabled')){

            var idRD= $(event.currentTarget).attr('data-role-edit-RD-id');

            $('#btNuovoRDItem').addClass('disabled');
            $('.actionButton').addClass('disabled');

            new moduloRDDetail({
                update: true,
                idRD: idRD,
                canWrite: true
            });

        }
    },

    doDeleteRD: function(event){

        var me = this;
        var currentRDId = $(event.currentTarget).attr('data-role-delete-RD-id');
        var messaggio = "Si desidera eliminare l'elemento selezionato?" 
       
        var confirmDelete = bootbox.dialog({
            message: messaggio,
            title: "Attenzione!",
            backdrop: true,
            closeButton: true,
            animate: false,
            buttons: {
                conferma: {
                  label: "Conferma",
                  className: "btn-primary",
                  callback: function() {
                            
                    RDController.deleteRD({
                        idRD: currentRDId,
                        
                        callback: function() {
                            confirmDelete.modal("hide");
                             //$('#riga_'+currentHightlightId).remove();

                            new moduloRD({
                                canWrite: true
                            });

                        }
                    });
                    return false;
                  }
                   
                },
                cancel: {
                  label: "Annulla",
                  className: "btn-success",
                  callback: function() {
                  }
                }
            }
        });
    }

});

