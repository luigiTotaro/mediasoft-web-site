var moduloProdottiDetail = Backbone.View.extend({

    tagName : "div",
    className: '',
    update: null,
    updateAllegato: false,
    updateScreenshot: false,
    currentIdProdotto: 0,
    canWrite: null,
    pathImage1: '',
    pathImage2: '',
    pathImage3: '',
    pathImage4: '',
    pathImageAllegato: '',
    pathImageScreenshot: '',
    tipoFile: '2',
    modalBootbox: null,
    attributes: {
        'id': 'div_moduloProdottoDetail'
    },

    events: {
       //'click #saveAllegato': 'insertOrUpdateAllegato',
       //'click [data-role-edit-allegato-id]' : 'doEditAllegato',
       'click [data-role-delete-allegato-id]' : 'doDeleteAllegato'
    },

    initialize: function(options) {
     
        this.update = options.update;  
        this.canWrite = options.canWrite;

        var me=this;  

        if(this.update==true){
            this.currentIdProdotto = options.idProdotto;
            ProdottiController.getProdotto({
                idProdotto:  options.idProdotto,
                callback: function(jsonData) { 
                    me.render(jsonData);
                } 
            });
        }else
            this.render();
            
    },


    render : function(jsonData) {

           var me = this;

           console.log(jsonData)

           var html = '<form method="post" class="horizontal-form" id="formNuovoProdotto >'+
                        '<div class="form-body">'+  

                            '<div class="row">'+
                                '<div class="col-md-3">'+
                                    '<label class="control-label">Immagine 1(*)</label>'+
                                    '<div class="form-group">'+
                                        '<input id="selectedFile1" name="selectedFile1" class="file" type="file" style="display:none;">'+
                                        '<span style="font-size:80%">Sono supportati solo file di tipo "jpg, png, gif". Dimensione max '+(this.tipoFile==2 ?  "1 MB." : "5 MB.")+'</span><br>'+
                                        '<span class="text-danger error" id="imgError1" ></span>'+
                                    '</div>'+
                                '</div>'+    
                                '<div class="col-md-3" style="display:none">'+
                                    '<label class="control-label">Immagine 2(*)</label>'+
                                    '<div class="form-group">'+
                                        '<input id="selectedFile2" name="selectedFile2" class="file" type="file" style="display:none;">'+
                                        '<span style="font-size:80%">Sono supportati solo file di tipo "jpg, png, gif". Dimensione max '+(this.tipoFile==2 ?  "1 MB." : "5 MB.")+'</span><br>'+
                                        '<span class="text-danger error" id="imgError2" ></span>'+
                                    '</div>'+
                                '</div>'+  
                                '<div class="col-md-3" style="display:none">'+
                                    '<label class="control-label">Immagine 3(*)</label>'+
                                    '<div class="form-group">'+
                                        '<input id="selectedFile3" name="selectedFile3" class="file" type="file" style="display:none;">'+
                                        '<span style="font-size:80%">Sono supportati solo file di tipo "jpg, png, gif". Dimensione max '+(this.tipoFile==2 ?  "1 MB." : "5 MB.")+'</span><br>'+
                                        '<span class="text-danger error" id="imgError3" ></span>'+
                                    '</div>'+
                                '</div>'+    
                                '<div class="col-md-3" style="display:none">'+
                                    '<label class="control-label">Immagine 4(*)</label>'+
                                    '<div class="form-group">'+
                                        '<input id="selectedFile4" name="selectedFile4" class="file" type="file" style="display:none;">'+
                                        '<span style="font-size:80%">Sono supportati solo file di tipo "jpg, png, gif". Dimensione max '+(this.tipoFile==2 ?  "1 MB." : "5 MB.")+'</span><br>'+
                                        '<span class="text-danger error" id="imgError4" ></span>'+
                                    '</div>'+
                                '</div>'+    
                            '</div>'+  

                            '<div class="row">'+
                                '<div class="col-md-2">'+
                                    '<label class="control-label">Nome (*)</label>'+
                                '</div>'+
                                '<div class="col-md-4">'+
                                   '<div class="form-group">'+
                                        '<input type="text" id="txtNome" name="txtNome" placeholder="Nome ..." value="' + (jsonData!=null ? jsonData["nome"] : "") + '" class="form-control">'+
                                        '<span class="text-danger error" id="nomeError" ></span>'+
                                    '</div>'+
                                '</div>'+                                            
                                '<div class="col-md-2">'+
                                    '<label class="control-label">Link </label>'+
                                '</div>'+
                                '<div class="col-md-4">'+
                                   '<div class="form-group">'+
                                        '<input type="text" id="txtLink" name="txtLink" placeholder="Link ..." value="' + (jsonData!=null ? jsonData["link"] : "") + '" class="form-control">'+
                                        '<span class="text-danger error" id="linkError" ></span>'+
                                    '</div>'+
                                '</div>'+                                            
                            '</div>'+  

                            '<div class="row">'+
                                '<div class="col-md-2">'+
                                    '<label class="control-label">Menu Laterale IT </label>'+
                                '</div>'+
                                '<div class="col-md-4">'+
                                   '<div class="form-group">'+
                                        '<input type="text" id="txtMenuIt"  name="txtMenuIt"  placeholder="Menu Laterale IT..."  value="' + (jsonData!=null ? jsonData["titolo_menu_laterale_it"] : "") + '" class="form-control">'+
                                         '<span class="text-danger error" id="titoloItError" ></span>'+
                                 '</div>'+
                                '</div>'+  
                     
                                '<div class="col-md-2">'+
                                    '<label class="control-label">Menu Laterale EN </label>'+
                                '</div>'+
                                '<div class="col-md-4">'+
                                   '<div class="form-group">'+
                                        '<input type="text" id="txtMenuEn"  name="txtMenuEn"  placeholder="Menu Laterale EN..."  value="' + (jsonData!=null ? jsonData["titolo_menu_laterale_en"] : "") + '" class="form-control">'+
                                         '<span class="text-danger error" id="titoloItError" ></span>'+
                                 '</div>'+
                                '</div>'+  
                            '</div>'+

                            '<div class="row">'+
                                '<div class="col-md-2">'+
                                    '<label class="control-label">Titolo IT </label>'+
                                '</div>'+
                                '<div class="col-md-4">'+
                                   '<div class="form-group">'+
                                        '<input type="text" id="txtTitoloIt"  name="txtTitoloIt"  placeholder="Titolo IT..."  value="' + (jsonData!=null ? jsonData["titoloIT"] : "") + '" class="form-control">'+
                                         '<span class="text-danger error" id="titoloItError" ></span>'+
                                 '</div>'+
                                '</div>'+  
                         
                                '<div class="col-md-2">'+
                                    '<label class="control-label">Titolo EN </label>'+
                                '</div>'+
                                '<div class="col-md-4">'+
                                   '<div class="form-group">'+
                                        '<input type="text" id="txtTitoloEn" name="txtTitoloEn" placeholder="Titolo EN..." value="' + (jsonData!=null ? jsonData["titoloEN"] : "") + '" class="form-control">'+
                                        '<span class="text-danger error" id="titoloEnError" ></span>'+
                                    '</div>'+
                                '</div>'+                                            
                            '</div>'+  

                            '<div class="row">'+
                                '<div class="col-md-2">'+
                                    '<label class="control-label">Descrizione IT </label>'+
                                '</div>'+
                                '<div class="col-md-4">'+
                                    '<div class="form-group">'+
                                        '<textarea id="txtDescrizioneIt" maxlength="" name="txtDescrizioneIt"  class="form-control"  rows="5" style="resize: none;"  placeholder="descrizione it...">'+
                                        (jsonData!=null ? jsonData["testoIT"] : "")
                                        +'</textarea>'+
                                    '</div>'+
                                '</div>'+    
                         
                                '<div class="col-md-2">'+
                                    '<label class="control-label">Descrizione EN </label>'+
                                '</div>'+
                                '<div class="col-md-4">'+
                                    '<div class="form-group">'+
                                        '<textarea id="txtDescrizioneEn" maxlength="" name="txtDescrizioneEn"  class="form-control"  rows="5" style="resize: none;"  placeholder="descrizione en...">'+
                                       (jsonData!=null ? jsonData["testoEN"] : "")
                                        +'</textarea>'+
                                    '</div>'+
                                '</div>'+    
                            '</div>'+  

                            '<div class="row">'+
                                '<div class="col-md-2">'+
                                    '<label class="control-label">Layout Prodotto</label>'+
                                '</div>'+
                                '<div class="col-md-4">'+
                                    '<div class="form-group">'+
                                       '<select class="form-control select2me" readonly data-placeholder="Seleziona Tipo Layout..." id="selTipo" name="selTipo">'+
                                         '<option value="12">Titolo - Immagine - Testo sulla destra</option>'+
                                         '<option value="8">Titolo - Immagine + 3 thumb - Testo sulla destra</option>'+
                                        '</select>'+
                                       '<span class="text-danger error" id="errorTipo"></span>'+
                                    '</div>'+
                                '</div>'+

                                '<div class="col-md-2" id="divPos1" ' + ((jsonData!=null && jsonData["posizione"] == -1) ? "style='display:none'" : "") + '>'+
                                    '<label class="control-label">Posizione:</label>'+
                                '</div>'+
                                '<div class="col-md-2" id="divPos2" ' + ((jsonData!=null && jsonData["posizione"] == -1) ? "style='display:none'" : "") + '>'+
                                    '<div id="txtPosizione">'+ 
                                        '<div class="input-group" style="width:150px;margin-bottom:10px;">'+ 
                                            '<input type="text" id="txtPosizioneVal" value="' + ((jsonData!=null && jsonData["posizione"] != -1)  ? (parseInt(jsonData["posizione"])-1) : "1") + '" class="spinner-input form-control" maxlength="3" >'+ 
                                            '<div class="spinner-buttons input-group-btn">'+ 
                                                '<button type="button" class="btn spinner-up default">'+ 
                                                '<i class="fa fa-angle-up"></i>'+ 
                                                '</button>'+ 
                                                '<button type="button" class="btn spinner-down default">'+ 
                                                '<i class="fa fa-angle-down"></i>'+ 
                                                '</button>'+ 
                                            '</div>'+ 
                                        '</div>'+ 
                                        '<span class="text-danger error" id="errorPosizione"></span>'+
                                    '</div>'+ 
                                '</div>'+ 
                                '<div class="col-md-2">'+
                                    '<label class="control-label">Disabilitato:</label>&nbsp;&nbsp;&nbsp;'+
                                    '<input type="checkbox" class="make-switch" name="abilitato" ' + ((jsonData!=null && jsonData["posizione"] == -1) ? "checked" : "") + ' id="txtAbilitato" data-size="normal">'+
                                '</div>'+ 

                            '</div>'+  

                            //ICONE
                            '<div class="portlet box blue" '+ (me.update!=true ? "style=\"display:none\"" : "") +'>'+
                                '<div class="portlet-title">'+
                                    '<div class="caption">'+
                                        '<i class="fa fa-info-circle"></i>Icone di prodotto'+
                                    '</div>'+
                                    '<div class="tools">'+
                                        '<a href="javascript:;" class="expand" data-original-title="" title=""></a>'+
                                    '</div>'+
                                '</div>'+
                                '<div class="portlet-body" style="display: none;">'+
                                    '<div class="row">'+

                                        '<div id="tableDataIcon"></div>'+
                                                                  
                                    '</div>'+                           
                                '</div>'+
                            '</div>'+

                            //Screenshot
                            '<div class="portlet box blue" '+ (me.update!=true ? "style=\"display:none\"" : "") +'>'+
                                '<div class="portlet-title">'+
                                    '<div class="caption">'+
                                        '<i class="fa fa-picture-o"></i>Screenshot'+
                                    '</div>'+
                                    '<div class="tools">'+
                                        '<a href="javascript:;" class="expand" data-original-title="" title=""></a>'+
                                    '</div>'+
                                '</div>'+
                                '<div class="portlet-body" style="display: none;">'+

                                    '<div class="row">'+
                                        '<div class="col-md-2">'+
                                            '<label class="control-label">Testo IT (*)</label>'+
                                        '</div>'+
                                        '<div class="col-md-4">'+
                                           '<div class="form-group">'+
                                                '<input type="text" id="txtScreenshotTestoIT" name="txtScreenshotTestoIT" placeholder="Titolo it ..." value="" class="form-control">'+
                                                '<span class="text-danger error" id="itScreenshotError" ></span>'+
                                            '</div>'+
                                        '</div>'+                                            
                                        '<div class="col-md-2">'+
                                            '<label class="control-label">Testo EN (*)</label>'+
                                        '</div>'+
                                        '<div class="col-md-4">'+
                                           '<div class="form-group">'+
                                                '<input type="text" id="txtScreenshotTestoEN" name="txtScreenshotTestoEN" placeholder="Titolo en ..." value="" class="form-control">'+
                                                '<span class="text-danger error" id="enScreenshotError" ></span>'+
                                            '</div>'+
                                        '</div>'+                                            
                                    '</div>'+ 

                                    '<div class="row">'+
                                        '<div class="col-md-2">'+
                                            '<label class="control-label">File (*)</label>'+
                                        '</div>'+
                                        '<div class="col-md-4">'+
                                            '<div class="form-group">'+
                                                '<input id="selectedFileScreenshot" name="selectedFileScreenshot" class="file" type="file" style="display:none;">'+
                                                '<span style="font-size:80%">Dimensione max '+(this.tipoFile==2 ?  "2 MB." : "5 MB.")+'</span><br>'+
                                                '<span class="text-danger error" id="imgErrorScreenshot" ></span>'+
                                            '</div>'+
                                        '</div>'+   
                                        '<div class="col-md-6">'+
                                            '<button type="button" id="saveScreenshot" class="btn btn-primary">Salva Nuovo Screenshot</button>'+
                                        '</div>'+                                 
                                    '</div>'+ 

                                    '<div class="row">'+
                                        '<div class="col-md-12">'+
                                        '<table class="table table-striped table-bordered table-hover" id="tableScreenshot" >'+
                                            '<div id="divFiltraScreenshot"></div>'+
                                            '<thead>'+
                                            '<tr>'+
                                                '<th>Path</th>'+
                                                '<th>Titolo IT</th>'+
                                                '<th></th>'+
                                            '</tr>'+
                                            '</thead>'+
                                            '<tbody id="tableDataScreenshot">'+
                                            '</tbody>'+
                                        '</table>'+
                                        '</div>'+
                                    '</div>'+
                             
                                '</div>'+
                            '</div>'+

                            //ALLEGATI
                            '<div class="portlet box blue" '+ (me.update!=true ? "style=\"display:none\"" : "") +'>'+
                                '<div class="portlet-title">'+
                                    '<div class="caption">'+
                                        '<i class="fa fa-file-pdf-o"></i>Allegati'+
                                    '</div>'+
                                    '<div class="tools">'+
                                        '<a href="javascript:;" class="expand" data-original-title="" title=""></a>'+
                                    '</div>'+
                                '</div>'+
                                '<div class="portlet-body" style="display: none;">'+

                                     '<div class="row">'+
                                        '<div class="col-md-2">'+
                                            '<label class="control-label">Tipo Allegato (*)</label>'+
                                        '</div>'+
                                        '<div class="col-md-2">'+
                                            '<div class="form-group">'+
                                               '<select class="form-control select2me" data-placeholder="Seleziona Tipo Allegato..." id="selTipoAllegato" name="selTipo">'+
                                                 '<option value="1">Allegato</option>'+
                                                 '<option value="2">Collegamento</option>'+
                                                 '<option value="3">Rassegna Stampa</option>'+
                                                '</select>'+
                                               '<span class="text-danger error" id="errorTipoAllegato"></span>'+
                                            '</div>'+
                                        '</div>'+ 
                                        '<div class="col-md-2">'+
                                            '<label class="control-label">Tipo Icona (*)</label>'+
                                        '</div>'+
                                        '<div class="col-md-2">'+
                                            '<div class="form-group">'+
                                               '<select class="form-control select2me"  data-placeholder="Seleziona Tipo Icona..." id="selTipoIcona" name="selTipo">'+
                                                 '<option value="1">Manuale Utente</option>'+
                                                 '<option value="2">Presentazione</option>'+
                                                 '<option value="3">Articolo</option>'+
                                                 '<option value="4">Brochure</option>'+
                                                '</select>'+
                                               '<span class="text-danger error" id="errorTipoIcona"></span>'+
                                            '</div>'+
                                        '</div>'+

                                        '<div class="col-md-2">'+
                                            '<label class="control-label">Posizione (*)</label>'+
                                        '</div>'+
                                        '<div class="col-md-2">'+
                                            '<div id="txtPosizioneAllegato">'+ 
                                                '<div class="input-group" style="width:150px;margin-bottom:10px;">'+ 
                                                    '<input type="text" id="txtPosizioneAllegatoVal" value="" class="spinner-input form-control" maxlength="3" >'+ 
                                                    '<div class="spinner-buttons input-group-btn">'+ 
                                                        '<button type="button" class="btn spinner-up default">'+ 
                                                        '<i class="fa fa-angle-up"></i>'+ 
                                                        '</button>'+ 
                                                        '<button type="button" class="btn spinner-down default">'+ 
                                                        '<i class="fa fa-angle-down"></i>'+ 
                                                        '</button>'+ 
                                                    '</div>'+ 
                                                '</div>'+ 
                                                '<span class="text-danger error" id="errorPosizioneAllegato"></span>'+
                                            '</div>'+ 
                                        '</div>'+  
                                    '</div>'+

                                    '<div class="row">'+
                                        '<div class="col-md-2">'+
                                            '<label class="control-label">Testo IT (*)</label>'+
                                        '</div>'+
                                        '<div class="col-md-4">'+
                                           '<div class="form-group">'+
                                                '<input type="text" id="txtAllegatoTestoIT" name="txtAllegatoTestoIT" placeholder="Titolo it ..." value="" class="form-control">'+
                                                '<span class="text-danger error" id="itAllegatoError" ></span>'+
                                            '</div>'+
                                        '</div>'+                                            
                                        '<div class="col-md-2">'+
                                            '<label class="control-label">Testo EN (*)</label>'+
                                        '</div>'+
                                        '<div class="col-md-4">'+
                                           '<div class="form-group">'+
                                                '<input type="text" id="txtAllegatoTestoEN" name="txtAllegatoTestoEN" placeholder="Titolo en ..." value="" class="form-control">'+
                                                '<span class="text-danger error" id="enAllegatoError" ></span>'+
                                            '</div>'+
                                        '</div>'+                                            
                                    '</div>'+ 

                                    '<div class="row">'+
                                        '<div class="col-md-2">'+
                                            '<label class="control-label">File (*)</label>'+
                                        '</div>'+
                                        '<div class="col-md-4">'+
                                            '<div class="form-group">'+
                                                '<input id="selectedFileAllegato" name="selectedFileAllegato" class="file" type="file" style="display:none;">'+
                                                '<span style="font-size:80%">Dimensione max '+(this.tipoFile==2 ?  "2 MB." : "5 MB.")+'</span><br>'+
                                                '<span class="text-danger error" id="imgErrorAllegato" ></span>'+
                                            '</div>'+
                                        '</div>'+   
                                        '<div class="col-md-6">'+
                                            '<button type="button" id="saveAllegato" class="btn btn-primary">Salva Nuovo Allegato</button>'+
                                        '</div>'+                                 
                                    '</div>'+ 

                                    '<div class="row">'+
                                        '<div class="col-md-12">'+
                                        '<table class="table table-striped table-bordered table-hover" id="tableAllegati" >'+
                                            '<div id="divFiltraAllegati"></div>'+
                                            '<thead>'+
                                            '<tr>'+
                                                '<th>Path</th>'+
                                                '<th>Titolo IT</th>'+
                                                '<th>Posizione</th>'+
                                                '<th></th>'+
                                            '</tr>'+
                                            '</thead>'+
                                            '<tbody id="tableDataAllegati">'+
                                            '</tbody>'+
                                        '</table>'+
                                        '</div>'+
                                    '</div>'+
                             
                                '</div>'+
                            '</div>'+

                        '</div>' +
                   ' </form>';

            me.modalBootbox = bootbox.dialog({
                closeButton: false,
                className: "modal90",
                message: html,
                title: (me.update==true ?  "Modifica Prodotto " : "Inserimento Nuovo Prodotto"),
                buttons: {
                    annulla: {
                      label: "Annulla",
                      className: "btn-success",
                      callback: function() {
                        $('#btNuovoProdotto').removeClass('disabled');
                        $('.actionButton').removeClass('disabled');
                      }
                    },                      
                    conferma: {
                      label: "Conferma",
                      className: "btn-primary",
                      callback: function() {

                            var nomeFile1 = $('#selectedFile1').val();
                            var nomeFile2 = $('#selectedFile2').val();
                            var nomeFile3 = $('#selectedFile3').val();
                            var nomeFile4 = $('#selectedFile4').val();

                            if(nomeFile1=="" && nomeFile2=="" && nomeFile3=="" && nomeFile4==""){
                                //vuol dire che sto modificando solo il contenuto
                                me.insertOrUpdate();
                            }else{  
                                //effettuo l'upload prima di salvare
                               if(nomeFile1 != ''){
                                   me.innerUpload(1);
                               }
                               if(nomeFile2 != ''){
                                   me.innerUpload(2);
                               }
                               if(nomeFile3 != ''){
                                   me.innerUpload(3);
                               }
                               if(nomeFile4 != ''){
                                   me.innerUpload(4);
                               }
                                //me.insertOrUpdate();
                            }
        
                            return false;

                       }//chiude callback
                    }//chiude conferma
                }//chiude button
            });//chiude dialog

            //check abilitato
            $('#txtAbilitato').change(function(event) {
                var checkbox = event.target;
                if (checkbox.checked) {
                     $('#divPos1').hide();
                     $('#divPos2').hide();
                } else {
                     $('#divPos1').show();
                     $('#divPos2').show();
                }
            });

            $('#txtPosizione').spinner({value:((jsonData!=null && jsonData["posizione"] != -1) ? (parseInt(jsonData["posizione"])-1) : 1), min: 1});
            $('#txtPosizioneAllegato').spinner({value:1, min: 1});

            $('#selTipo').select2({
                allowClear:true,
                disabled: true
            });

            $( '#selTipo' ).prop( 'disabled', true );

            if(jsonData!=null){ 
                $('#selTipo').select2('val', jsonData["layout"]);
            }

            $('#selTipoAllegato').select2();
            $('#selTipoIcona').select2();

            me.showMedia();

            if(me.update==true){
                me.showAllegati();
                me.showScreenshot();
                me.getIconeListByProdotto();
            }

            $('#saveAllegato').click(function(event){
               me.innerUploadAllegato();
            });
            $('#saveScreenshot').click(function(event){
               me.innerUploadScreenshot();
            });

            //ALLEGATI
            var _initialPreview = [];
            var _allowedFileExtensions = [];
            var _maxFileSize="";

            _allowedFileExtensions = '';
            //_allowedFileExtensions = ['jpg', 'png','gif'];
            _maxFileSize= 2000;      

            $("#selectedFileAllegato").fileinput({
                allowedFileExtensions : _allowedFileExtensions,
                uploadAsync: true,
                minFileCount: 0,
                maxFileCount: 1,
                maxFileSize: _maxFileSize,
                removeLabel: 'Rimuovi',
                removeTitle: 'Rimuovi file selezionato',
                cancelLabel: 'Annulla',
                cancelTitle: 'Annulla upload',
                uploadLabel: 'Upload',
                uploadTitle: 'Upload file selezionato',
                browseLabel: 'Seleziona',
                msgSelected: '{n} files selezionato',
                dropZoneEnabled: false,
                msgInvalidFileType: 'Formato del file "{name}" non supportato. Sono supportati solo file di tipo "{types}".',
                msgInvalidFileExtension: 'Estensione del file "{name}" non supportata. Sono supportati solo file di tipo "{extensions}".',
                msgSizeTooLarge:'La dimesione del file "{name}" (<b>{size} KB</b>) eccede il limite imposto <b>{maxSize} KB</b>.',
                uploadUrl: './phpService/ProdottiServices.php?action=uploadAllegato',
                initialPreview: _initialPreview,
                layoutTemplates: {
                
                    main1: '' +
                            '<div class="kv-upload-progress hide"></div>\n' +
                            '<div class="input-group {class}">\n' +
                            '   {caption}\n' +
                            '   <div class="input-group-btn">\n' +
                            '       <a id="btnEmptyFileManagerAllegato" href="#" class="btn btn-default"><i class="fa fa-trash-o"></i> Rimuovi</a>\n' +
                            '       <a id="btnAddFileManagerAllegato" href="#" class="btn btn-default"><i class="fa fa-upload"></i> Seleziona un file</a>\n' +
                            '   </div>\n' +
                            '</div>',                     
                            
                    icon: '<span class="glyphicon glyphicon-picture kv-caption-icon"></span>',
                    
                    actions: '',
                    footer: ''
                }
                
            }).on('fileuploaded', function(event, data, previewId, index) {
                    
                $("#selectedFileAllegato").attr('data-name', data.response.nuovoFile);
                var newFile = data.response.nuovoFile;
                me.pathImageAllegato = newFile.replace("../../allegati/", "");
                me.insertOrUpdateAllegato();

            }).on('filebatchuploadsuccess', function(event, data, previewId, index) {

                $("#selectedFileAllegato").attr('data-name', data.response.nuovoFile);
                var newFile = data.response.nuovoFile;
                me.pathImageAllegato = newFile.replace("../../allegati/", "");
                me.insertOrUpdateAllegato();
                
            }).on('filecleared', function(event) {
                //se rimuovo una immagine esistente non faccio upload ma devo rimuovere il file e aggiornare la path
                me.pathImageAllegato = "";    
            });
           
            $('#btnEmptyFileManagerAllegato').click(function(evt) {
                $("#selectedFileAllegato").fileinput('clear');
            });

            $('#btnAddFileManagerAllegato').click(function(evt) {
                $("#selectedFileAllegato").click();
            });

            //SCREENSHOT
            var _initialPreview = [];
            var _allowedFileExtensions = [];
            var _maxFileSize="";

            //_allowedFileExtensions = '';
            _allowedFileExtensions = ['jpg', 'png','gif'];
            _maxFileSize= 2000;      

            $("#selectedFileScreenshot").fileinput({
                allowedFileExtensions : _allowedFileExtensions,
                uploadAsync: true,
                minFileCount: 0,
                maxFileCount: 1,
                maxFileSize: _maxFileSize,
                removeLabel: 'Rimuovi',
                removeTitle: 'Rimuovi file selezionato',
                cancelLabel: 'Annulla',
                cancelTitle: 'Annulla upload',
                uploadLabel: 'Upload',
                uploadTitle: 'Upload file selezionato',
                browseLabel: 'Seleziona',
                msgSelected: '{n} files selezionato',
                dropZoneEnabled: false,
                msgInvalidFileType: 'Formato del file "{name}" non supportato. Sono supportati solo file di tipo "{types}".',
                msgInvalidFileExtension: 'Estensione del file "{name}" non supportata. Sono supportati solo file di tipo "{extensions}".',
                msgSizeTooLarge:'La dimesione del file "{name}" (<b>{size} KB</b>) eccede il limite imposto <b>{maxSize} KB</b>.',
                uploadUrl: './phpService/ProdottiServices.php?action=uploadScreenshot',
                initialPreview: _initialPreview,
                layoutTemplates: {
                    main1: '' +
                            '<div class="kv-upload-progress hide"></div>\n' +
                            '<div class="input-group {class}">\n' +
                            '   {caption}\n' +
                            '   <div class="input-group-btn">\n' +
                            '       <a id="btnEmptyFileManagerScreenshot" href="#" class="btn btn-default"><i class="fa fa-trash-o"></i> Rimuovi</a>\n' +
                            '       <a id="btnAddFileManagerScreenshot" href="#" class="btn btn-default"><i class="fa fa-upload"></i> Seleziona un file</a>\n' +
                            '   </div>\n' +
                            '</div>',                     
                    icon: '<span class="glyphicon glyphicon-picture kv-caption-icon"></span>',
                    actions: '',
                    footer: ''
                }       
            }).on('fileuploaded', function(event, data, previewId, index) {
                    
                $("#selectedFileScreenshot").attr('data-name', data.response.nuovoFile);
                var newFile = data.response.nuovoFile;
                me.pathImageScreenshot = newFile.replace("../../images/", "");
                me.insertOrUpdateScreenshot();

            }).on('filebatchuploadsuccess', function(event, data, previewId, index) {

                $("#selectedFileScreenshot").attr('data-name', data.response.nuovoFile);
                var newFile = data.response.nuovoFile;
                me.pathImageScreenshot = newFile.replace("../../images/", "");
                me.insertOrUpdateScreenshot();
                
            }).on('filecleared', function(event) {
                //se rimuovo una immagine esistente non faccio upload ma devo rimuovere il file e aggiornare la path
                me.pathImageScreenshot = "";    
            });
           
            $('#btnEmptyFileManagerScreenshot').click(function(evt) {
                $("#selectedFileScreenshot").fileinput('clear');
            });

            $('#btnAddFileManagerScreenshot').click(function(evt) {
                $("#selectedFileScreenshot").click();
            });

    },

    showMedia: function(){
        var me = this;
        ProdottiController.getMediaByIdProdotto({
            idProdotto:  me.currentIdProdotto,
            callback: function(jsonData) { 
                console.log("------media------------------------");
                me.renderMedia(jsonData[0],1);
                /*for(i=0;i<4;i++){
                    me.renderMedia(jsonData[i],i+1);
                }*/
            } 
        });
    },

    renderMedia : function(jsonData,count) {

        var me = this;

        if(jsonData!=null){
            if(count == 1){
                me.pathImage1  = jsonData["path"];
            }else if(count == 2){
                me.pathImage2  = jsonData["path"];
            }else if(count == 3){
                me.pathImage3  = jsonData["path"];
            }else if(count == 4){
                me.pathImage4  = jsonData["path"];
            }
        }

        var _initialPreview = [];
        var _allowedFileExtensions = [];
        var _maxFileSize="";
      
        if(me.update==true && jsonData!=null){
            _initialPreview = [ "<img style='' src='../images/"+(jsonData!=null ? jsonData["path"] : "")+"?time="+(new Date().getTime())+"' class='file-preview-image' >" ];
        }

        //_allowedFileTypes : ['image'];
        _allowedFileExtensions = ['jpg', 'png','gif'];
        _maxFileSize= 1000;
       
        $("#selectedFile"+count).fileinput({
            allowedFileExtensions : _allowedFileExtensions,
            uploadAsync: true,
            minFileCount: 0,
            maxFileCount: 1,
            maxFileSize: _maxFileSize,
            showUpload: false,
            removeLabel: 'Rimuovi',
            removeTitle: 'Rimuovi file selezionato',
            cancelLabel: 'Annulla',
            cancelTitle: 'Annulla upload',
            uploadLabel: 'Upload',
            uploadTitle: 'Upload file selezionato',
            browseLabel: 'Seleziona',
            msgSelected: '{n} files selezionato',
            dropZoneEnabled: false,
            msgInvalidFileType: 'Formato del file "{name}" non supportato. Sono supportati solo file di tipo "{types}".',
            msgInvalidFileExtension: 'Estensione del file "{name}" non supportata. Sono supportati solo file di tipo "{extensions}".',
            msgSizeTooLarge:'La dimesione del file "{name}" (<b>{size} KB</b>) eccede il limite imposto <b>{maxSize} KB</b>.',
            uploadUrl: './phpService/ProdottiServices.php?action=uploadImage'+count+'&oldFile'+count+'='+(jsonData!=null ? "../../images/"+jsonData["path"] : ""),
            initialPreview: _initialPreview,
            //initialPreview: '',
            layoutTemplates: {
            
                main1: '{preview}\n' +
                        '<div class="kv-upload-progress hide"></div>\n' +
                        '<div class="input-group {class}">\n' +
                        '   {caption}\n' +
                        '   <div class="input-group-btn">\n' +
                        '       <a id="btnEmptyFileManager'+count+'" href="#" class="btn btn-default"><i class="fa fa-trash-o"></i> Rimuovi</a>\n' +
                        '       <a id="btnAddFileManager'+count+'" href="#" class="btn btn-default"><i class="fa fa-upload"></i> Seleziona un file</a>\n' +
                        '   </div>\n' +
                        '</div>',
                        
                preview: 
                        '<div class="file-preview {class}"  style="height:220px;">\n' +              
                        '    <div class="{dropClass}" style="border:none; padding: 3px; margin: 0px;">\n' +
                        '    <div class="file-preview-thumbnails">\n' +
                        '    </div>\n' +
                        '    <div class="clearfix"></div>' +
                        '    <div class="kv-fileinput-error"></div>\n' +
                        '    </div>\n' +
                        '</div>',
                        
                icon: '<span class="glyphicon glyphicon-picture kv-caption-icon"></span>',
                
                actions: '',
                footer: ''
            }
            
        }).on('fileuploaded', function(event, data, previewId, index) {
                
            $("#selectedFile"+count).attr('data-name', data.response.nuovoFile);

            var newFile = data.response.nuovoFile;

            if(count == 1){
                me.pathImage1 = newFile.replace("../../images/", "");
            }else if(count == 2){
                me.pathImage2 = newFile.replace("../../images/", "");
            }else if(count == 3){
                me.pathImage3 = newFile.replace("../../images/", "");
            }else if(count == 4){
                me.pathImage4 = newFile.replace("../../images/", "");
            }

            me.pathImage = newFile.replace("../../images/", "");

            me.insertOrUpdate();
                        
        }).on('filebatchuploadsuccess', function(event, data, previewId, index) {

            $("#selectedFile"+count).attr('data-name', data.response.nuovoFile);

            var newFile = data.response.nuovoFile;

            if(count == 1){
                me.pathImage1 = newFile.replace("../../images/", "");
            }else if(count == 2){
                me.pathImage2 = newFile.replace("../../images/", "");
            }else if(count == 3){
                me.pathImage3 = newFile.replace("../../images/", "");
            }else if(count == 4){
                me.pathImage4 = newFile.replace("../../images/", "");
            }

            me.insertOrUpdate();
            
        }).on('filecleared', function(event) {

            //se rimuovo una immagine esistente non faccio upload ma devo rimuovere il file e aggiornare la path

            if(count == 1){
                me.pathImage1  = "";
            }else if(count == 2){
                me.pathImage2  = "";
            }else if(count == 3){
                me.pathImage3  = "";
            }else if(count == 4){
                me.pathImage4  = "";
            }

        });
       
        $('#btnEmptyFileManager'+count).click(function(evt) {
            $("#selectedFile"+count).fileinput('clear');
        });

        $('#btnAddFileManager'+count).click(function(evt) {
            $("#selectedFile"+count).click();
        });

    },

    showAllegati: function(){
        var me = this;
        ProdottiController.getAllegatiByIdProdotto({
            idProdotto:  me.currentIdProdotto,
            callback: function(jsonData) { 

                $('#tableDataAllegati').empty();
                for(var i in jsonData) {

                    $('#tableDataAllegati').append(
                    '<tr class="odd gradeX" id="rigaAllegato_'+jsonData[i]['id']+'">'+
                        '<td><a href="../allegati/'+ jsonData[i]['path'] +'" target="_blank">' + jsonData[i]['path'] + '</a></td>'+
                        '<td>' + jsonData[i]['testoIT'] + '</td>'+
                        '<td>' + jsonData[i]['posizione'] + '</td>'+
                        '<td style="width: 20px;">' + 
                            //'<span class="actionButton" style="padding: 0 10px 0 0; !important"><a href="#" data-role-edit-allegato-id="' + jsonData[i]['id'] + '"><i class="icon-pencil"></i></a></span>' + 
                            '<span class="actionButton"><a href="#" data-role-delete-allegato-id="' + jsonData[i]['id'] + '" >'+
                            '<i class="fa fa-trash"></i></a></span>'+ 
                        '</td>'+
                    '</tr>');
                }

                if(!$('#tableAllegati').hasClass('initialized')){ 
                    //Prima carico i dati e dopo inizializzo la tabella altrimenti non funziona la ricerca
                    var columnSort = [];
                    columnSort.push({"orderable" : false}),
                    columnSort.push({"orderable" : false}),
                    columnSort.push({"orderable" : false}),
                    columnSort.push({"orderable" : false}),
                    manageDatatable2($('#tableAllegati'),$('#tableAllegati'), columnSort);
                    $('#tableAllegati').not('.initialized').addClass('initialized');
                }

                $('[data-role-delete-allegato-id]').click(function(event){
                   me.doDeleteAllegato($(this).attr("data-role-delete-allegato-id"));
                });

 
            } 
        });
    },

    innerUpload: function(count){
        $("#selectedFile"+count).fileinput('upload');
    },

    innerUploadAllegato: function(){
        var path = $('#selectedFileAllegato').val();
        var titoloIt = $('#txtAllegatoTestoIT').val();
        var titoloEn = $('#txtAllegatoTestoEN').val();

        $('.error').empty();
        var errori = false;

        if(titoloIt == "") {
            $('#itAllegatoError').append('<em>(Titolo IT obbligatorio)</em>');
            errori = true;
        }

        if(titoloEn == "") {
            $('#enAllegatoError').append('<em>(Titolo EN obbligatorio)</em>');
            errori = true;
        }

        if(errori)
            return false;


        if(path == ''){
            alert("Selezionare un file.");
        }else{
            $("#selectedFileAllegato").fileinput('upload');
        } 
    },

    insertOrUpdateAllegato: function(){

        var me = this;
        var path = me.pathImageAllegato;
        var titoloIt = $('#txtAllegatoTestoIT').val();
        var titoloEn = $('#txtAllegatoTestoEN').val();
        var tipoAllegato = $('#selTipoAllegato').val();
        var tipoIcona = $('#selTipoIcona').val();
        var posizione = ($('#txtPosizioneAllegatoVal').val()!='' ? $('#txtPosizioneAllegatoVal').val() : 1);

        $('.error').empty();
        var errori = false;
       
        if(path == "") {
            $('#imgErrorAllegato').append('<em>(Immagine obbligatoria)</em>');
            errori = true;
        }

        if(titoloIt == "") {
            $('#itAllegatoError').append('<em>(Titolo IT obbligatorio)</em>');
            errori = true;
        }

        if(titoloEn == "") {
            $('#enAllegatoError').append('<em>(Titolo EN obbligatorio)</em>');
            errori = true;
        }

        if(posizione < 1 ) {
            $('#errorPosizioneAllegato').append('<em>(Attenzione! la posizione deve essere maggiore di 1)</em>');
            errori = true;
        }

        if(errori)
            return false;

        ProdottiController.insertOrUpdateAllegato({
            path: path,
            titoloIt: titoloIt,
            titoloEn: titoloEn,
            tipoAllegato: tipoAllegato,
            tipoIcona: tipoIcona,
            posizione: posizione,
            updateAllegato: me.updateAllegato,
            idProdotto: me.currentIdProdotto,
            idAllegato: '',
            callback: function(utente_id){

               // alert("ho salvato allegato");
                me.showAllegati();
                me.resetCampiAllegato();

            }

        });
    },

    resetCampiAllegato: function(){
        var me = this;
        me.updateAllegato = false;
        me.pathImageAllegato = "";
        $("#selectedFileAllegato").fileinput('clear');
        $('#txtAllegatoTestoIT').val("");
        $('#txtAllegatoTestoEN').val("");
        $('#selTipoAllegato').select2('val', 1);
        $('#selTipoIcona').select2('val', 1);
        $('#txtPosizioneAllegatoVal').val(1);
    },

    doDeleteAllegato: function(id){

        var me = this;
        var currentAllegatoId = id;
        var messaggio = "Si desidera eliminare l\'allegato selezionato?" 
       
        var confirmDelete = bootbox.dialog({
            message: messaggio,
            title: "Attenzione!",
            backdrop: true,
            closeButton: true,
            animate: false,
            buttons: {
                conferma: {
                  label: "Conferma",
                  className: "btn-primary",
                  callback: function() {         
                    ProdottiController.deleteAllegato({
                        idAllegato: currentAllegatoId,
                        callback: function() {
                             confirmDelete.modal("hide");
                             $('#rigaAllegato_'+currentAllegatoId).remove();
                        }
                    });
                    return false;
                  }
                   
                },
                cancel: {
                  label: "Annulla",
                  className: "btn-success",
                  callback: function() {
                  }
                }
            }
        });
    },

    insertOrUpdate: function(){

        var me = this;
        var path1 = me.pathImage1;
        var path2 = me.pathImage2;
        var path3 = me.pathImage3;
        var path4 = me.pathImage4;
        var nome = $('#txtNome').val();
        var link = $('#txtLink').val();
        var titoloIt = $('#txtTitoloIt').val();
        var titoloEn = $('#txtTitoloEn').val();
        var menuIt = $('#txtMenuIt').val();
        var menuEn = $('#txtMenuEn').val();
        var descrizioneIt = $('#txtDescrizioneIt').val();
        var descrizioneEn = $('#txtDescrizioneEn').val();
        var tipoLayout = $('#selTipo').val();
        var abilitato = $('#txtAbilitato').is(':checked');

        if(link != ""){
            var pattern = /^((http|https|ftp):\/\/)/;
            if(!pattern.test(link)) {
                link = "http://" + link;
            }
        }

        var posizione = ($('#txtPosizioneVal').val()!='' ? $('#txtPosizioneVal').val() : 1);

        posizione = parseInt(posizione)+1

        $('.error').empty();
        var errori = false;
       
        if(nome == "") {
            $('#nomeError').append('<em>(Inserire il nome per il Prodotto)</em>');
            errori = true;
        }

        if(path1 == "") {
            $('#imgError1').append('<em>(Immagine obbligatoria)</em>');
            errori = true;
        }

        /*if(path2 == "") {
            $('#imgError2').append('<em>(Immagine obbligatoria)</em>');
            errori = true;
        }

        if(path3 == "") {
            $('#imgError3').append('<em>(Immagine obbligatoria)</em>');
            errori = true;
        }

        if(path4 == "") {
            $('#imgError4').append('<em>(Immagine obbligatoria)</em>');
            errori = true;
        }*/

        console.log(posizione)
        console.log(abilitato)
        if(abilitato != true){
            if(posizione < 1 ) {
                $('#errorPosizione').append('<em>(Attenzione! la posizione deve essere maggiore di 1)</em>');
                errori = true;
            }
        } else {
            posizione = -1;
        } 

        if(errori)
            return false;

        ProdottiController.insertOrUpdateProdotto({
            path1: path1,
            path2: path2,
            path3: path3,
            path4: path4,
            nome: nome,
            link: link,
            titoloIt: titoloIt,
            titoloEn: titoloEn,
            menuIt: menuIt,
            menuEn: menuEn,
            descrizioneIt: descrizioneIt,
            descrizioneEn: descrizioneEn,
            tipoLayout: tipoLayout,
            posizione: posizione,
            update: me.update,
            idProdotto: me.currentIdProdotto,
            callback: function(utente_id){

                $('#btNuovoProdotto').removeClass('disabled');
                $('.actionButton').removeClass('disabled');
                me.modalBootbox.modal("hide");

                //alert("ho salvato");

                new moduloProdotti({
                    canWrite: true
                });

            }

        });
                                  
        return false;
    },

















    //SCREENSHOT
    showScreenshot: function(){
        var me = this;
        ProdottiController.getMediaByIdProdotto({
            idProdotto:  me.currentIdProdotto,
            callback: function(jsonData) { 

                $('#tableDataScreenshot').empty();
                for(var i in jsonData) {

                    if(i == 0){
                        continue;
                    }

                    $('#tableDataScreenshot').append(
                    '<tr class="odd gradeX" id="rigaScreenshot_'+jsonData[i]['id']+'">'+
                        '<td><a href="../images/'+ jsonData[i]['path'] +'" target="_blank">' + jsonData[i]['path'] + '</a></td>'+
                        '<td>' + jsonData[i]['titoloIT'] + '</td>'+
                        '<td style="width: 20px;">' + 
                            '<span class="actionButton"><a href="#" data-role-delete-screenshot-id="' + jsonData[i]['id'] + '" >'+
                            '<i class="fa fa-trash"></i></a></span>'+ 
                        '</td>'+
                    '</tr>');
                }

                if(!$('#tableScreenshot').hasClass('initialized')){ 
                    //Prima carico i dati e dopo inizializzo la tabella altrimenti non funziona la ricerca
                    var columnSort = [];
                    columnSort.push({"orderable" : false}),
                    columnSort.push({"orderable" : false}),
                    columnSort.push({"orderable" : false}),
                    manageDatatable2($('#tableScreenshot'),$('#tableScreenshot'), columnSort);
                    $('#tableScreenshot').not('.initialized').addClass('initialized');
                }

                $('[data-role-delete-screenshot-id]').click(function(event){
                   me.doDeleteScreenshot($(this).attr("data-role-delete-screenshot-id"));
                });

 
            } 
        });
    },

    innerUploadScreenshot: function(){
        var path = $('#selectedFileScreenshot').val();
        if(path == ''){
            alert("Selezionare un file.");
        }else{
            $("#selectedFileScreenshot").fileinput('upload');
        } 
    },

    resetCampiScreenshot: function(){
        var me = this;
        me.updateScreenshot = false;
        me.pathImageScreenshot = "";
        $("#selectedFileScreenshot").fileinput('clear');
        $('#txtScreenshotTestoIT').val("");
        $('#txtScreenshotTestoEN').val("");
    },

    insertOrUpdateScreenshot: function(){

        var me = this;
        var path = me.pathImageScreenshot;
        var titoloIt = $('#txtScreenshotTestoIT').val();
        var titoloEn = $('#txtScreenshotTestoEN').val();

        $('.error').empty();
        var errori = false;
       
        if(path == "") {
            $('#imgErrorScreenshot').append('<em>(Immagine obbligatoria)</em>');
            errori = true;
        }

        if(errori)
            return false;

        ProdottiController.insertOrUpdateScreenshot({
            path: path,
            titoloIt: titoloIt,
            titoloEn: titoloEn,
            updateScreenshot: me.updateScreenshot,
            idProdotto: me.currentIdProdotto,
            idScreenshot: '',
            callback: function(utente_id){
                me.showScreenshot();
                me.resetCampiScreenshot();
            }
        });
    },

    doDeleteScreenshot: function(id){

        var me = this;
        var currentScreenshotId = id;
        var messaggio = "Si desidera eliminare lo Screenshot selezionato?" 
       
        var confirmDelete = bootbox.dialog({
            message: messaggio,
            title: "Attenzione!",
            backdrop: true,
            closeButton: true,
            animate: false,
            buttons: {
                conferma: {
                  label: "Conferma",
                  className: "btn-primary",
                  callback: function() {         
                    ProdottiController.deleteScreenshot({
                        idScreenshot: currentScreenshotId,
                        callback: function() {
                             confirmDelete.modal("hide");
                             $('#rigaScreenshot_'+currentScreenshotId).remove();
                        }
                    });
                    return false;
                  }
                },
                cancel: {
                  label: "Annulla",
                  className: "btn-success",
                  callback: function() {
                  }
                }
            }
        });
    },

    //ICONE
    getIconeListByProdotto: function(){
        var me = this;
        ProdottiController.getIconeListByProdotto({
            idProdotto:  me.currentIdProdotto,
            callback: function(jsonData) { 

                $('#tableDataIcon').empty();
                for(var i in jsonData) {

                    var ckd = "";
                    if(jsonData[i].enable == '1'){
                        ckd = "checked";
                    }

                    $('#tableDataIcon').append(
                    '<div class="col-md-4">'+    
                        '<img src="../icon/'+jsonData[i].path_it+'" style="width:100%" />'+
                        '<input id="" type="checkbox" data-role-icon-id="'+jsonData[i].id+'"  '+ckd+' class="input-small">'+  
                    '</div>');
                }

                $('[data-role-icon-id]').click(function(event){
                    var idProdotto = me.currentIdProdotto;
                    var idIcona = $(this).attr("data-role-icon-id");
                    var enable = $(this).is(":checked");
                    me.saveOrRemoveIcon(idProdotto,idIcona,enable);
                   //me.doDeleteScreenshot($(this).attr("data-role-delete-screenshot-id"));
                });

            } 
        });
    },

    saveOrRemoveIcon: function(idProdotto,idIcona,enable){
        var me = this;
        ProdottiController.saveOrRemoveIcon({
            idProdotto: idProdotto,
            idIcona: idIcona,
            enable: enable,
            callback: function(){
                console.log("ok");
            }
        });
    },

});

