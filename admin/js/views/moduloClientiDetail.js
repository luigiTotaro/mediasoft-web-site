var moduloClientiDetail = Backbone.View.extend({

    tagName : "div",
    className: '',
    update: null,
    currentidCliente: 0,
    canWrite: null,
    pathImage: '',
    tipoFile: '2',
    modalBootbox: null,
    attributes: {
        'id': 'div_moduloClientiDetail'
    },

    events: {
       
    },

    initialize: function(options) {
     
        this.update = options.update;  
        this.canWrite = options.canWrite;

        var me=this;  

        if(this.update==true){
            this.currentidCliente = options.idCliente;
            ClientiController.getCliente({
                idCliente:  options.idCliente,
                callback: function(jsonData) { 
                    me.render(jsonData);
                } 
            });
        }else
            this.render();
            
    },


    render : function(jsonData) {

           var me = this;

           //console.log(jsonData)

           if(jsonData!=null){
                me.pathImage = jsonData["path"];
           }else{
                me.pathImage = "";
           }
        
           var html = '<form method="post" class="horizontal-form" id="formNuovoCliente >'+
                        '<div class="form-body">'+  

                            '<div class="row">'+
                                '<div class="col-md-12">'+
                                    '<div class="form-group">'+
                                        '<input id="selectedFile" name="selectedFile" class="file" type="file" style="display:none;">'+
                                        '<span style="font-size:80%">Sono supportati solo file di tipo "jpg, png, gif". Dimensione max '+(this.tipoFile==2 ?  "1 MB." : "5 MB.")+'</span><br>'+
                                        '<span style="font-size:80%;color: #C11B17;">Attenzione! Per mantenere la visualizzazione corretta dell\'elemento, il sistema ridimensiona in automatico con misure 200px per 200px. Si consiglia di caricare un\'immagine di questa proporzione.</span><br>'+
                                        '<span class="text-danger error" id="imgError" ></span>'+
                                    '</div>'+
                                '</div>'+    
                            '</div>'+    

                            '<div class="row">'+
                                '<div class="col-md-2">'+
                                    '<label class="control-label">Nome: </label>'+
                                '</div>'+
                                '<div class="col-md-10">'+
                                   '<div class="form-group">'+
                                        '<input type="text" id="txtNome" name="txtNome" placeholder="Nome ..." value="' + (jsonData!=null ? jsonData["nome"] : "") + '" class="form-control">'+
                                        '<span class="text-danger error" id="nomeError" ></span>'+
                                    '</div>'+
                                '</div>'+                                            
                            '</div>'+  

                            '<div class="row">'+

                                '<div class="col-md-2" style="display:none">'+
                                    '<div class="form-group">'+
                                       '<select class="form-control select2me" data-placeholder="Seleziona Tipo Layout..." id="selTipo" name="selTipo">'+
                                         '<option value="1">Immagine</option>'+
                                         '<option value="2">Testuale</option>'+
                                        '</select>'+
                                        '<span class="text-danger error" id="errorTipo"></span>'+
                                    '</div>'+
                                '</div>'+

                                '<div class="col-md-2">'+
                                    '<label class="control-label">Sezione:</label><br/>'+
                                    //'<span class="" id="" style="font-size:8px">identifica su quale riga verrà visualizzato il cliente.</span>'+
                                '</div>'+
                                '<div class="col-md-2">'+
                                    '<div class="form-group">'+
                                       '<select class="form-control select2me" data-placeholder="Seleziona Tipo Layout..." id="selSezione" name="selTipo">'+
                                         '<option value="1">Sezione 1</option>'+
                                         '<option value="2">Sezione 2</option>'+
                                         '<option value="3">Sezione 3</option>'+
                                        '</select>'+
                                        '<span class="text-danger error" id="errorSezione"></span>'+
                                    '</div>'+
                                '</div>'+

                                '<div class="col-md-2">'+
                                    '<label class="control-label">Posizione:</label>'+
                                '</div>'+
                                '<div class="col-md-2">'+
                                    '<div id="txtPosizione">'+ 
                                        '<div class="input-group" style="width:150px;margin-bottom:10px;">'+ 
                                            '<input type="text" id="txtPosizioneVal" value="' + (jsonData!=null ? jsonData["posizione"] : "1") + '" class="spinner-input form-control" maxlength="3" >'+ 
                                            '<div class="spinner-buttons input-group-btn">'+ 
                                                '<button type="button" class="btn spinner-up default">'+ 
                                                '<i class="fa fa-angle-up"></i>'+ 
                                                '</button>'+ 
                                                '<button type="button" class="btn spinner-down default">'+ 
                                                '<i class="fa fa-angle-down"></i>'+ 
                                                '</button>'+ 
                                            '</div>'+ 
                                        '</div>'+ 
                                    '</div>'+ 
                                '</div>'+  

                            '</div>'+  
                            
                        '</div>' +
                   ' </form>';

            me.modalBootbox = bootbox.dialog({
                closeButton: false,
                className: "modal70",
                message: html,
                title: (me.update==true ?  "Modifica Cliente" : "Inserimento Nuovo Cliente"),
                buttons: {
                    annulla: {
                      label: "Annulla",
                      className: "btn-success",
                      callback: function() {
                        $('#btNuovoCliente').removeClass('disabled');
                        $('.actionButton').removeClass('disabled');
                      }
                    },                      
                    conferma: {
                      label: "Conferma",
                      className: "btn-primary",
                      callback: function() {

                            var nomeFile = $('#selectedFile').val();

                            /*if(jsonData!=null){
                                me.pathImage = jsonData["path"];
                            }else{
                                me.pathImage = "";
                            }*/

                            if(nomeFile==""){
                                //vuol dire che sto modificando solo il contenuto
                                me.insertOrUpdate();
                            }else{  
                                //effettuo l'upload prima di salvare
                                me.innerUpload();
                            }
        
                            return false;

                       }//chiude callback
                    }//chiude conferma
                }//chiude button
            });//chiude dialog

            
            $('#selTipo').select2();
            $('#selSezione').select2();

            if(jsonData!=null){ 
                $('#selTipo').select2('val', jsonData["tipo"]);
                $('#selSezione').select2('val', jsonData["sezione"]);
            }

            $('#txtPosizione').spinner({value:(jsonData!=null ? jsonData["posizione"] : 1), min: 1});

            var _initialPreview = [];
            var _allowedFileExtensions = [];
            var _maxFileSize="";

            if(this.tipoFile=='2'){
                if(me.update==true){
                    _initialPreview = [ "<img src='../images/clienti/"+(jsonData!=null ? jsonData["path"] : "")+"?time="+(new Date().getTime())+"' class='file-preview-image' >" ];
                }
            }

            _allowedFileExtensions = ['jpg', 'png','gif'];
            _maxFileSize= 1000;
               
            $("#selectedFile").fileinput({
                allowedFileExtensions : _allowedFileExtensions,
                uploadAsync: true,
                minFileCount: 0,
                maxFileCount: 1,
                maxFileSize: _maxFileSize,
                removeLabel: 'Rimuovi',
                removeTitle: 'Rimuovi file selezionato',
                cancelLabel: 'Annulla',
                cancelTitle: 'Annulla upload',
                uploadLabel: 'Upload',
                uploadTitle: 'Upload file selezionato',
                browseLabel: 'Seleziona',
                msgSelected: '{n} files selezionato',
                dropZoneEnabled: false,
                msgInvalidFileType: 'Formato del file "{name}" non supportato. Sono supportati solo file di tipo "{types}".',
                msgInvalidFileExtension: 'Estensione del file "{name}" non supportata. Sono supportati solo file di tipo "{extensions}".',
                msgSizeTooLarge:'La dimesione del file "{name}" (<b>{size} KB</b>) eccede il limite imposto <b>{maxSize} KB</b>.',
                uploadUrl: './phpService/ClientiServices.php?action=uploadImage&oldFile='+(jsonData!=null ? "../../images/clienti/"+jsonData["path"] : ""),
                initialPreview: _initialPreview,
                layoutTemplates: {
                
                    main1: '{preview}\n' +
                            '<div class="kv-upload-progress hide"></div>\n' +
                            '<div class="input-group {class}">\n' +
                            '   {caption}\n' +
                            '   <div class="input-group-btn">\n' +
                            '       <a id="btnEmptyFileManager" href="#" class="btn btn-default"><i class="fa fa-trash-o"></i> Rimuovi</a>\n' +
                            '       <a id="btnAddFileManager" href="#" class="btn btn-default"><i class="fa fa-upload"></i> Seleziona un file</a>\n' +
                            '   </div>\n' +
                            '</div>',
                            
                    
                    preview: 
                            '<div class="file-preview {class}"  style="height:220px;">\n' +              
                            '    <div class="{dropClass}" style="border:none; padding: 3px; margin: 0px;">\n' +
                            '    <div class="file-preview-thumbnails">\n' +
                            '    </div>\n' +
                            '    <div class="clearfix"></div>' +
                            '    <div class="kv-fileinput-error"></div>\n' +
                            '    </div>\n' +
                            '</div>',
                            
                    icon: '<span class="glyphicon glyphicon-picture kv-caption-icon"></span>',
                    
                    actions: '',
                    footer: ''
                }
                
            }).on('fileuploaded', function(event, data, previewId, index) {
                    
                $("#selectedFile").attr('data-name', data.response.nuovoFile);

                var newFile = data.response.nuovoFile;
                me.pathImage = newFile.replace("../../images/clienti/", "");

                me.insertOrUpdate();

                //me.outerCallback();
                            
            }).on('filebatchuploadsuccess', function(event, data, previewId, index) {

                $("#selectedFile").attr('data-name', data.response.nuovoFile);

                var newFile = data.response.nuovoFile;
                me.pathImage = newFile.replace("../../images/clienti/", "");

                me.insertOrUpdate();

                //me.outerCallback();
                
            }).on('filecleared', function(event) {

                //se rimuovo una immagine esistente non faccio upload ma devo rimuovere il file e aggiornare la path
                //console.log("rimuovo")
                me.pathImage = "";
                
            });
           
            $('#btnEmptyFileManager').click(function(evt) {
                $("#selectedFile").fileinput('clear');
            });

            $('#btnAddFileManager').click(function(evt) {
                $("#selectedFile").click();
            });

    },
    innerUpload: function(){
        //this.outerCallback = outCb;
        $("#selectedFile").fileinput('upload');
    },
    insertOrUpdate: function(){

        var me = this;
        var path = me.pathImage;
        var nome = $('#txtNome').val();
        var sezione = $('#selSezione').val();
        var posizione = ($('#txtPosizioneVal').val()!='' ? $('#txtPosizioneVal').val() : 1);


        console.log(posizione)

        $('.error').empty();
        var errori = false;
       
        if(path == "") {
            $('#imgError').append('<em>(Inserire un immagine per il cliente)</em>');
            errori = true;
        }

        if(errori)
            return false;

        ClientiController.insertOrUpdateCliente({
            path: path,
            nome: nome,
            sezione:sezione,
            posizione: posizione,
            update: me.update,
            idCliente: me.currentidCliente,
            callback: function(utente_id){

                $('#btNuovoCliente').removeClass('disabled');
                $('.actionButton').removeClass('disabled');
                me.modalBootbox.modal("hide");

                new moduloClienti({
                    canWrite: true
                });

            }

        });
                                  
        return false;
    }
});

