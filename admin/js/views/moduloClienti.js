var moduloClienti = Backbone.View.extend({

    tagName : "div",
    className: '',
    canWrite: null,
   
    // attributi applicabili all'oggetto DOM root della vista (in questo caso il div contenitore)
    attributes: {
        'id': 'div_moduloClienti'
    },

    events: {
        'click #btNuovoCliente': 'doCreateCliente',
        'click #updateClientiTxt': 'doUpdateClientiTxt',
        'click [data-role-edit-cliente-id]' : 'doEditCliente',
        'click [data-role-delete-cliente-id]' : 'doDeleteCliente'
    },

    initialize: function(options) {
        this.canWrite = options.canWrite;
        this.render();
    },

    render : function(jsonData) {
       
        var header='<div class="page-head">'+
                        '<div class="page-title">'+
                          '<h1>Gestione Clienti <small> in questa sezione &egrave; possibile inserire, modificare o eliminare Clienti dalla pagina "About Us"</small></h1>'+
                        '</div>'+
                    '<div>';

         var html = '<div class="portlet box red-sunglo">' +
                        '<div class="portlet-title">' +
                            '<div class="caption">' +
                                '<i class="fa fa-users"></i> Clienti Sezione Testuale'+
                            '</div>' +
                        '</div>'+
                        '<div class="portlet-body">'+

                            '<div class="row">'+
                                '<div class="col-md-4">'+
                                    '<label class="control-label">Colonna 1</label>'+
                                    '<div class="form-group">'+
                                        '<textarea id="txtColonna1" maxlength="" name="txtColonna1"  class="form-control"  rows="8" style="resize: none;"  placeholder="Colonna 1...">'+
                                        '</textarea>'+
                                    '</div>'+
                                '</div>'+    
                    
                                '<div class="col-md-4">'+
                                    '<label class="control-label">Colonna 2</label>'+
                                    '<div class="form-group">'+
                                        '<textarea id="txtColonna2" maxlength="" name="txtColonna2"  class="form-control"  rows="8" style="resize: none;"  placeholder="Colonna 2...">'+
                                        '</textarea>'+
                                    '</div>'+
                                '</div>'+    

                                '<div class="col-md-4">'+
                                    '<label class="control-label">Colonna 3</label>'+
                                    '<div class="form-group">'+
                                        '<textarea id="txtColonna3" maxlength="" name="txtColonna3"  class="form-control"  rows="8" style="resize: none;"  placeholder="Colonna 3...">'+
                                        '</textarea>'+
                                    '</div>'+
                                '</div>'+ 

                                '<div class="col-md-12">'+
                                    '<button type="button" id="updateClientiTxt" class="btn btn-primary" style="float:right">Salva</button>'+
                                '</div>'+    
                            '</div>'+  
 
                       ' </div>'+
                    '</div>'+        

                    '<div class="portlet box red-sunglo">' +
                        '<div class="portlet-title">' +
                            '<div class="caption">' +
                                '<i class="fa fa-users"></i> Clienti Sezione Immagini'+
                            '</div>' +

                            '<div class="actions" id="strumentAction">' +  
                                '<div id="actionCreate" class="btn-group">'+                        
                                  '<a href="#" class="btn btn-default btn-sm" id="btNuovoCliente">' +
                                  '<i class="fa fa-pencil"></i> Nuovo Cliente</a>' +
                                '</div>'+
                            '</div>' +
                          
                        '</div>'+
                        '<div class="portlet-body">'+
                            '<table class="table table-striped table-bordered table-hover" id="tableClienti" >'+
                            '<div id="divFiltra"></div>'+
                            '<thead>'+
                            '<tr>'+
                                '<th>Immagine</th>'+
                                '<th>Nome</th>'+
                                '<th>Posizione</th>'+
                                '<th></th>'+
                            '</tr>'+
                            '</thead>'+
                            '<tbody id="tableData">'+
                            '</tbody>'+

                            '</table>'+
                            
                       ' </div>'+
                   ' </div>';

        this.$el.empty();
        $('#applicationContainer').empty();
        this.$el.append(header);
        this.$el.append(html);
        $('#applicationContainer').append(this.$el);
           
        this.showData();

    },

    showData: function(filtro){
        var me = this;

        ClientiController.getClientiList({

            callback: function(jsonData){

                $('#tableData').empty();
                for(var i in jsonData) {

                    $('#tableData').append(
                                '<tr class="odd gradeX" id="riga_'+jsonData[i]['id']+'">'+
                                    '<td><a href="../images/clienti/'+ jsonData[i]['path'] +'" target="_blank">' + jsonData[i]['path'] + '</a></td>'+
                                    '<td>' + jsonData[i]['nome'] + '</td>'+
                                    '<td>' + jsonData[i]['posizione'] + '</td>'+
                                    '<td style="width: 50px;">' + 
                                        '<span class="actionButton" style="padding: 0 10px 0 0; !important"><a href="#" data-role-edit-cliente-id="' + jsonData[i]['id'] + '"><i class="icon-pencil"></i></a></span>' + 
                                        '<span class="actionButton"><a href="#" data-role-delete-cliente-id="' + jsonData[i]['id'] + '" >'+
                                        '<i class="fa fa-trash"></i></a></span>'+ 
                                    '</td>'+
                                '</tr>');

                    $('#txtColonna1').val(jsonData[i]['colonna1']);
                    $('#txtColonna2').val(jsonData[i]['colonna2']);
                    $('#txtColonna3').val(jsonData[i]['colonna3']);
                }

                if(!$('#tableClienti').hasClass('initialized')){ 
                    //Prima carico i dati e dopo inizializzo la tabella altrimenti non funziona la ricerca
                    var columnSort = [];
                    columnSort.push({"orderable" : false}),
                    columnSort.push({"orderable" : true}),
                    columnSort.push({"orderable" : true}),
                    columnSort.push({"orderable" : false}),
                    manageDatatable($('#tableClienti'),$('#tableClienti'), columnSort);
                    $('#tableClienti').not('.initialized').addClass('initialized');
                }
            }

        });

    },

    doCreateCliente: function(){
        event.stopImmediatePropagation();
        
        if (!$('#btNuovoCliente').hasClass('disabled')){

            $('#btNuovoCliente').addClass('disabled');
            
            new moduloClientiDetail({
                update: false,
                canWrite: true
            });
        }
       
    },
    
    doEditCliente: function(event){
        
        event.stopImmediatePropagation();

        if (!$('.actionButton').hasClass('disabled')){

            var idCliente= $(event.currentTarget).attr('data-role-edit-cliente-id');

            $('#btNuovoCliente').addClass('disabled');
            $('.actionButton').addClass('disabled');

            new moduloClientiDetail({
                update: true,
                idCliente: idCliente,
                canWrite: true
            });

        }
    },

    doDeleteCliente: function(event){

        var me = this;
        var currentClientiId = $(event.currentTarget).attr('data-role-delete-cliente-id');
        var messaggio = "Si desidera eliminare l'elemento selezionato?" 
       
        var confirmDelete = bootbox.dialog({
            message: messaggio,
            title: "Attenzione!",
            backdrop: true,
            closeButton: true,
            animate: false,
            buttons: {
                conferma: {
                  label: "Conferma",
                  className: "btn-primary",
                  callback: function() {
                            
                    ClientiController.deleteCliente({
                        idCliente: currentClientiId,
                        
                        callback: function() {
                            confirmDelete.modal("hide");
                             //$('#riga_'+currentHightlightId).remove();

                            new moduloClienti({
                                canWrite: true
                            });

                        }
                    });
                    return false;
                  }
                   
                },
                cancel: {
                  label: "Annulla",
                  className: "btn-success",
                  callback: function() {
                  }
                }
            }
        });
    },

    doUpdateClientiTxt: function(){

        var me = this;

        var colonna1 = $('#txtColonna1').val();
        var colonna2 = $('#txtColonna2').val();
        var colonna3 = $('#txtColonna3').val();

        ClientiController.updateColonneTestuali({
            colonna1: colonna1,
            colonna2: colonna2,
            colonna3: colonna3,
            callback: function(utente_id){
                bootbox.alert("Dati salvati con successo");
            }
        });
                                  
        return false;
    }

});

