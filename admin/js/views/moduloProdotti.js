var moduloProdotti = Backbone.View.extend({

    tagName : "div",
    className: '',
    canWrite: null,
   
    // attributi applicabili all'oggetto DOM root della vista (in questo caso il div contenitore)
    attributes: {
        'id': 'div_moduloProdotto'
    },

    events: {
        'click #btNuovoProdotto': 'doCreateProdotto',
        'click [data-role-edit-prodotto-id]' : 'doEditProdotto',
        'click [data-role-delete-prodotto-id]' : 'doDeleteProdotto'
    },

    initialize: function(options) {
        this.canWrite = options.canWrite;
        this.render();
    },

    render : function(jsonData) {
       
        var header= '<div class="page-head">'+
                        '<div class="page-title">'+
                          '<h1>Gestione Prodotti <small> in questa sezione &egrave; possibile inserire, modificare o eliminare prodotti della pagina "Products"</small></h1>'+
                        '</div>'+
                    '<div>';

       var html = '<div class="portlet box red-sunglo">' +
                        '<div class="portlet-title">' +
                            '<div class="caption">' +
                                '<i class="fa fa-cubes"></i> Prodotti'+
                            '</div>' +

                            '<div class="actions" id="strumentAction">' +  
                                '<div id="actionCreate" class="btn-group">'+                        
                                  '<a href="#" class="btn btn-default btn-sm" id="btNuovoProdotto">' +
                                  '<i class="fa fa-pencil"></i> Nuovo Prodotto</a>' +
                                '</div>'+
                            '</div>' +
                          
                        '</div>'+
                        '<div class="portlet-body">'+
                            '<table class="table table-striped table-bordered table-hover" id="tableHL" >'+
                            '<div id="divFiltra"></div>'+
                            '<thead>'+
                            '<tr>'+
                                '<th>Nome</th>'+
                                '<th>Link</th>'+
                                '<th>Posizione</th>'+
                                '<th></th>'+
                            '</tr>'+
                            '</thead>'+
                            '<tbody id="tableData">'+
                            '</tbody>'+

                            '</table>'+
                            
                       ' </div>'+
                   ' </div>';
        this.$el.empty();
        $('#applicationContainer').empty();
        this.$el.append(header);
        this.$el.append(html);
        $('#applicationContainer').append(this.$el);
           
        this.showData();

    },

    showData: function(filtro){
        var me = this;

        ProdottiController.getProdottiList({

            callback: function(jsonData){

                $('#tableData').empty();
                for(var i in jsonData) {

                    var myPos = jsonData[i]['posizione']-1;
                    if(jsonData[i]['posizione'] == -1){
                        var myPos = "disabilitata";
                    }

                    $('#tableData').append(
                                '<tr class="odd gradeX" id="riga_'+jsonData[i]['id']+'">'+
                                    '<td>' + jsonData[i]['titolo'].replace("Prodotti - ", "") + '</td>'+
                                    '<td>' + jsonData[i]['link'] + '</td>'+
                                    '<td>' + myPos + '</td>'+
                                    '<td style="width: 50px;">' + 
                                        '<span class="actionButton" style="padding: 0 10px 0 0; !important"><a href="#" data-role-edit-prodotto-id="' + jsonData[i]['id'] + '"><i class="icon-pencil"></i></a></span>' + 
                                        '<span class="actionButton"><a href="#" data-role-delete-prodotto-id="' + jsonData[i]['id'] + '" >'+
                                        '<i class="fa fa-trash"></i></a></span>'+ 
                                    '</td>'+
                                '</tr>');
                }


                if(!$('#tableHL').hasClass('initialized')){ 
                    //Prima carico i dati e dopo inizializzo la tabella altrimenti non funziona la ricerca
                    var columnSort = [];
                    columnSort.push({"orderable" : true}),
                    columnSort.push({"orderable" : true}),
                    columnSort.push({"orderable" : true}),
                    columnSort.push({"orderable" : false}),
                    manageDatatable($('#tableHL'),$('#tableHL'), columnSort);
                    $('#tableHL').not('.initialized').addClass('initialized');
                }
            }

        });

    },

    doCreateProdotto: function(){

        event.stopImmediatePropagation();
        
        if (!$('#btNuovoProdotto').hasClass('disabled')){

            $('#btNuovoProdotto').addClass('disabled');
            
            new moduloProdottiDetail({
                update: false,
                canWrite: true
            });
        }
       
    },
    
    doEditProdotto: function(event){
        
        event.stopImmediatePropagation();

        if (!$('.actionButton').hasClass('disabled')){

            var idProdotto= $(event.currentTarget).attr('data-role-edit-prodotto-id');

            $('#btNuovoProdotto').addClass('disabled');
            $('.actionButton').addClass('disabled');

            new moduloProdottiDetail({
                update: true,
                idProdotto: idProdotto,
                canWrite: true
            });

        }
    },

    doDeleteProdotto: function(event){

        var me = this;
        var currentProdottoId = $(event.currentTarget).attr('data-role-delete-prodotto-id');
        var messaggio = "Si desidera eliminare il prodotto selezionato?" 
       
        var confirmDelete = bootbox.dialog({
            message: messaggio,
            title: "Attenzione!",
            backdrop: true,
            closeButton: true,
            animate: false,
            buttons: {
                conferma: {
                  label: "Conferma",
                  className: "btn-primary",
                  callback: function() {
                            
                    ProdottiController.deleteProdotto({
                        idProdotto: currentProdottoId,
                        
                        callback: function() {
                             confirmDelete.modal("hide");
                             //$('#riga_'+currentProdottoId).remove();

                            new moduloProdotti({
                                canWrite: true
                            });

                        }
                    });
                    return false;
                  }
                   
                },
                cancel: {
                  label: "Annulla",
                  className: "btn-success",
                  callback: function() {
                  }
                }
            }
        });
    }

});

