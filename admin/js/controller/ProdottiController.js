var ProdottiController =  Backbone.Model.extend({}, {
	
  	getProdottiList: function(options) {
  		  
         	$.ajax({

              dataType: 'json',
              url: 'phpService/ProdottiServices.php?action=getProdottiList',
              async: true,

              data: {

              },

              success: function(data) {
                  options.callback(data);
              },

              error: function(xhr) {
                  alert("Si è verificato un errore di connessione ad internet: [" + xhr.responseText + "]");
              }

          });
    },

    getProdotto: function(options) {

        $.ajax({

            dataType: 'json',
            url: 'phpService/ProdottiServices.php?action=getProdotto',
            async: true,

            data: {
              idProdotto: options.idProdotto 
            },

            success: function(data) {
                options.callback(data[0]);
            },

            error: function(xhr) {
                alert("Si è verificato un errore di connessione ad internet: [" + xhr.responseText + "]");
            }

        });

    },

    getMediaByIdProdotto: function(options) {

        $.ajax({

            dataType: 'json',
            url: 'phpService/ProdottiServices.php?action=getMediaByIdProdotto',
            async: true,

            data: {
              idProdotto: options.idProdotto 
            },

            success: function(data) {
                options.callback(data);
            },

            error: function(xhr) {
                alert("Si è verificato un errore di connessione ad internet: [" + xhr.responseText + "]");
            }

        });

    },

    getAllegatiByIdProdotto: function(options) {

        $.ajax({

            dataType: 'json',
            url: 'phpService/ProdottiServices.php?action=getAllegatiByIdProdotto',
            async: true,

            data: {
              idProdotto: options.idProdotto 
            },

            success: function(data) {
                options.callback(data);
            },

            error: function(xhr) {
                alert("Si è verificato un errore di connessione ad internet: [" + xhr.responseText + "]");
            }

        });

    },

    deleteProdotto: function(options){

        $.ajax({

            url: 'phpService/ProdottiServices.php?action=deleteProdotto',
            type: 'POST',

            data: {
                idProdotto: options.idProdotto
            },

            content: 'application/json',

            success: function(data) {
                options.callback();
            },

            error: function(xhr) {
                alert("Si è verificato un errore di connessione ad internet: [" + xhr.responseText + "]");
            }

        });

    },

    deleteAllegato: function(options){

        $.ajax({

            url: 'phpService/ProdottiServices.php?action=deleteAllegato',
            type: 'POST',

            data: {
                idAllegato: options.idAllegato
            },

            content: 'application/json',

            success: function(data) {
                options.callback();
            },

            error: function(xhr) {
                alert("Si è verificato un errore di connessione ad internet: [" + xhr.responseText + "]");
            }

        });

    },

    insertOrUpdateProdotto: function(options){

        console.log(options.descrizioneIt)

        $.ajax({

            url: 'phpService/ProdottiServices.php?action=' + (options.update==true ? 'updateProdotto' : 'insertProdotto'),
            type: 'POST',

            data: {
                path1: options.path1,
                path2: options.path2,
                path3: options.path3,
                path4: options.path4,
                nome: options.nome,
                link: options.link,
                titoloIt: options.titoloIt,
                titoloEn: options.titoloEn,
                menuIt: options.menuIt,
                menuEn: options.menuEn,
                descrizioneIt: options.descrizioneIt,
                descrizioneEn: options.descrizioneEn,
                tipoLayout: options.tipoLayout,
                posizione: options.posizione,
                idProdotto: options.idProdotto
            },

            content: 'application/json',

            success: function(data) {
               
                options.callback(data);
            },

            error: function(xhr) {
                alert("Si è verificato un errore di connessione ad internet: [" + xhr.responseText + "]");
            }

        });

    },

    insertOrUpdateAllegato: function(options){

        $.ajax({

            url: 'phpService/ProdottiServices.php?action=' + (options.updateAllegato==true ? 'updateAllegato' : 'insertAllegato'),
            type: 'POST',

            data: {
                path: options.path,
                titoloIt: options.titoloIt,
                titoloEn: options.titoloEn,
                tipoAllegato: options.tipoAllegato,
                tipoIcona: options.tipoIcona,
                posizione: options.posizione,
                idProdotto: options.idProdotto,
                idAllegato: options.idAllegato
            },

            content: 'application/json',

            success: function(data) {
               
                options.callback(data);
            },

            error: function(xhr) {
                alert("Si è verificato un errore di connessione ad internet: [" + xhr.responseText + "]");
            }

        });

    },

    //Screenshot
    insertOrUpdateScreenshot: function(options){

        $.ajax({

            url: 'phpService/ProdottiServices.php?action=' + (options.updateScreenshot==true ? 'updateScreenshot' : 'insertScreenshot'),
            type: 'POST',

            data: {
                path: options.path,
                titoloIt: options.titoloIt,
                titoloEn: options.titoloEn,
                idProdotto: options.idProdotto,
                idScreenshot: options.idScreenshot
            },

            content: 'application/json',

            success: function(data) {
               
                options.callback(data);
            },

            error: function(xhr) {
                alert("Si è verificato un errore di connessione ad internet: [" + xhr.responseText + "]");
            }

        });

    },

    deleteScreenshot: function(options){

        $.ajax({

            url: 'phpService/ProdottiServices.php?action=deleteScreenshot',
            type: 'POST',

            data: {
                idScreenshot: options.idScreenshot
            },

            content: 'application/json',

            success: function(data) {
                options.callback();
            },

            error: function(xhr) {
                alert("Si è verificato un errore di connessione ad internet: [" + xhr.responseText + "]");
            }

        });

    },

    getIconeListByProdotto: function(options) {
          
            $.ajax({

              dataType: 'json',
              url: 'phpService/ProdottiServices.php?action=getIconeListByProdotto',
              async: true,

              data: {
                idProdotto: options.idProdotto 
              },

              success: function(data) {
                  options.callback(data);
              },

              error: function(xhr) {
                  alert("Si è verificato un errore di connessione ad internet: [" + xhr.responseText + "]");
              }

          });
    },

    saveOrRemoveIcon: function(options){

        console.log(options)

        $.ajax({

            url: 'phpService/ProdottiServices.php?action=' + (options.enable==true ? 'insertIcon' : 'removeIcon'),
            type: 'POST',

            data: {
                idProdotto: options.idProdotto,
                idIcona: options.idIcona
            },

            content: 'application/json',

            success: function(data) {
                options.callback(data);
            },

            error: function(xhr) {
                alert("Si è verificato un errore di connessione ad internet: [" + xhr.responseText + "]");
            }

        });

    },

    
});