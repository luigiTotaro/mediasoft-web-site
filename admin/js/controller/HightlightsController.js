var HightlightsController =  Backbone.Model.extend({}, {
	
  	getHightlightsList: function(options) {
  		  
         	$.ajax({

              dataType: 'json',
              url: 'phpService/HightlightsServices.php?action=getHightlightsList',
              async: true,

              data: {

              },

              success: function(data) {
                  options.callback(data);
              },

              error: function(xhr) {
                  alert("Si è verificato un errore di connessione ad internet: [" + xhr.responseText + "]");
              }

          });
    },

    getHightlight: function(options) {

        $.ajax({

            dataType: 'json',
            url: 'phpService/HightlightsServices.php?action=getHightlight',
            async: true,

            data: {
              idHightlight: options.idHightlight 
            },

            success: function(data) {
                options.callback(data[0]);
            },

            error: function(xhr) {
                alert("Si è verificato un errore di connessione ad internet: [" + xhr.responseText + "]");
            }

        });

    },

    deleteHightlight: function(options){

        $.ajax({

            url: 'phpService/HightlightsServices.php?action=deleteHightlight',
            type: 'POST',

            data: {
                idHightlight: options.idHightlight
            },

            content: 'application/json',

            success: function(data) {
                options.callback();
            },

            error: function(xhr) {
                alert("Si è verificato un errore di connessione ad internet: [" + xhr.responseText + "]");
            }

        });

    },

    insertOrUpdateHightlight: function(options){

        $.ajax({

            url: 'phpService/HightlightsServices.php?action=' + (options.update==true ? 'updateHightlight' : 'insertHightlight'),
            type: 'POST',

            data: {
                path: options.path,
                nome: options.nome,
                link: options.link,
                titoloIt: options.titoloIt,
                titoloEn: options.titoloEn,
                descrizioneIt: options.descrizioneIt,
                descrizioneEn: options.descrizioneEn,
                posizione: options.posizione,
                abilitata: options.abilitata,
                idHightlight: options.idHightlight
            },

            content: 'application/json',

            success: function(data) {
               
                options.callback(data);
            },

            error: function(xhr) {
                alert("Si è verificato un errore di connessione ad internet: [" + xhr.responseText + "]");
            }

        });

    }
    
});