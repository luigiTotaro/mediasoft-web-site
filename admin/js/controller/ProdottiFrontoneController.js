var ProdottiFrontoneController =  Backbone.Model.extend({}, {
	
    getMediaByIdProdotto: function(options) {

        $.ajax({

            dataType: 'json',
            url: 'phpService/ProdottiFrontoneServices.php?action=getMediaByIdProdotto',
            async: true,

            data: {
              idProdotto: options.idProdotto 
            },

            success: function(data) {
                options.callback(data);
            },

            error: function(xhr) {
                alert("Si è verificato un errore di connessione ad internet: [" + xhr.responseText + "]");
            }

        });

    },

    //Frontone
    insertOrUpdateFrontone: function(options){

        $.ajax({

            url: 'phpService/ProdottiFrontoneServices.php?action=' + (options.updateFrontone==true ? 'updateFrontone' : 'insertFrontone'),
            type: 'POST',

            data: {
                path: options.path,
                titoloIt: options.titoloIt,
                titoloEn: options.titoloEn,
                idProdotto: options.idProdotto,
                idFrontone: options.idFrontone
            },

            content: 'application/json',

            success: function(data) {
               
                options.callback(data);
            },

            error: function(xhr) {
                alert("Si è verificato un errore di connessione ad internet: [" + xhr.responseText + "]");
            }

        });

    },

    deleteFrontone: function(options){

        $.ajax({

            url: 'phpService/ProdottiFrontoneServices.php?action=deleteFrontone',
            type: 'POST',

            data: {
                idFrontone: options.idFrontone,
                path: options.path
            },

            content: 'application/json',

            success: function(data) {
                options.callback();
            },

            error: function(xhr) {
                alert("Si è verificato un errore di connessione ad internet: [" + xhr.responseText + "]");
            }

        });

    },
    
});