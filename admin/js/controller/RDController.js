var RDController =  Backbone.Model.extend({}, {
	
  	getRDList: function(options) {
  		  
         	$.ajax({

              dataType: 'json',
              url: 'phpService/RDServices.php?action=getRDList',
              async: true,

              data: {

              },

              success: function(data) {
                  options.callback(data);
              },

              error: function(xhr) {
                  alert("Si è verificato un errore di connessione ad internet: [" + xhr.responseText + "]");
              }

          });
    },

    getRD: function(options) {

        $.ajax({

            dataType: 'json',
            url: 'phpService/RDServices.php?action=getRD',
            async: true,

            data: {
              idRD: options.idRD 
            },

            success: function(data) {
                options.callback(data[0]);
            },

            error: function(xhr) {
                alert("Si è verificato un errore di connessione ad internet: [" + xhr.responseText + "]");
            }

        });

    },

    deleteRD: function(options){

        $.ajax({

            url: 'phpService/RDServices.php?action=deleteRD',
            type: 'POST',

            data: {
                idRD: options.idRD
            },

            content: 'application/json',

            success: function(data) {
                options.callback();
            },

            error: function(xhr) {
                alert("Si è verificato un errore di connessione ad internet: [" + xhr.responseText + "]");
            }

        });

    },

    insertOrUpdateRD: function(options){

        $.ajax({

            url: 'phpService/RDServices.php?action=' + (options.update==true ? 'updateRD' : 'insertRD'),
            type: 'POST',

            data: {
                link: options.link,
                titoloIt: options.titoloIt,
                titoloEn: options.titoloEn,
                autoreIt: options.autoreIt,
                autoreEn: options.autoreEn,
                testoIt: options.testoIt,
                testoEn: options.testoEn,
                progressivo: options.progressivo,
                idRD: options.idRD
            },

            content: 'application/json',

            success: function(data) {
               
                options.callback(data);
            },

            error: function(xhr) {
                alert("Si è verificato un errore di connessione ad internet: [" + xhr.responseText + "]");
            }

        });

    }
    
});