var TimelineController =  Backbone.Model.extend({}, {
	
  	getTimelineList: function(options) {
  		  
         	$.ajax({

              dataType: 'json',
              url: 'phpService/TimelineServices.php?action=getTimelineList',
              async: true,

              data: {

              },

              success: function(data) {
                  options.callback(data);
              },

              error: function(xhr) {
                  alert("Si è verificato un errore di connessione ad internet: [" + xhr.responseText + "]");
              }

          });
    },

    getTimeline: function(options) {

        $.ajax({

            dataType: 'json',
            url: 'phpService/TimelineServices.php?action=getTimeline',
            async: true,

            data: {
              idTimeline: options.idTimeline 
            },

            success: function(data) {
                options.callback(data[0]);
            },

            error: function(xhr) {
                alert("Si è verificato un errore di connessione ad internet: [" + xhr.responseText + "]");
            }

        });

    },

    deleteTimeline: function(options){

        console.log(options.idTimeline)

        $.ajax({

            url: 'phpService/TimelineServices.php?action=deleteTimeline',
            type: 'POST',

            data: {
                idTimeline: options.idTimeline
            },

            content: 'application/json',

            success: function(data) {
                options.callback();
            },

            error: function(xhr) {
                alert("Si è verificato un errore di connessione ad internet: [" + xhr.responseText + "]");
            }

        });

    },

    insertOrUpdateTimeline: function(options){

        $.ajax({

            url: 'phpService/TimelineServices.php?action=' + (options.update==true ? 'updateTimeline' : 'insertTimeline'),
            type: 'POST',

            data: {
                path: options.path,
                link: options.link,
                titoloIt: options.titoloIt,
                titoloEn: options.titoloEn,
                descrizioneIt: options.descrizioneIt,
                descrizioneEn: options.descrizioneEn,
                progressivo: options.progressivo,
                anno: options.anno,
                idTimeline: options.idTimeline
            },

            content: 'application/json',

            success: function(data) {
               
                options.callback(data);
            },

            error: function(xhr) {
                alert("Si è verificato un errore di connessione ad internet: [" + xhr.responseText + "]");
            }

        });

    }
    
});