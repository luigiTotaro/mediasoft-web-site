var ClientiController =  Backbone.Model.extend({}, {
	
  	getClientiList: function(options) {
  		  
         	$.ajax({

              dataType: 'json',
              url: 'phpService/ClientiServices.php?action=getClientiList',
              async: true,

              data: {

              },

              success: function(data) {
                  options.callback(data);
              },

              error: function(xhr) {
                  alert("Si è verificato un errore di connessione ad internet: [" + xhr.responseText + "]");
              }

          });
    },

    getCliente: function(options) {

        $.ajax({

            dataType: 'json',
            url: 'phpService/ClientiServices.php?action=getCliente',
            async: true,

            data: {
              idCliente: options.idCliente 
            },

            success: function(data) {
                options.callback(data[0]);
            },

            error: function(xhr) {
                alert("Si è verificato un errore di connessione ad internet: [" + xhr.responseText + "]");
            }

        });

    },

    deleteCliente: function(options){

        console.log(options.idCliente)

        $.ajax({

            url: 'phpService/ClientiServices.php?action=deleteCliente',
            type: 'POST',

            data: {
                idCliente: options.idCliente
            },

            content: 'application/json',

            success: function(data) {
                options.callback();
            },

            error: function(xhr) {
                alert("Si è verificato un errore di connessione ad internet: [" + xhr.responseText + "]");
            }

        });

    },

    insertOrUpdateCliente: function(options){

        $.ajax({

            url: 'phpService/ClientiServices.php?action=' + (options.update==true ? 'updateCliente' : 'insertCliente'),
            type: 'POST',

            data: {
                path: options.path,
                nome: options.nome,
                sezione: options.sezione,
                posizione: options.posizione,
                idCliente: options.idCliente
            },

            content: 'application/json',

            success: function(data) {
               
                options.callback(data);
            },

            error: function(xhr) {
                alert("Si è verificato un errore di connessione ad internet: [" + xhr.responseText + "]");
            }

        });

    },

    updateColonneTestuali: function(options){

        $.ajax({

            url: 'phpService/ClientiServices.php?action=updateColonneTestuali',
            type: 'POST',

            data: {
                colonna1: options.colonna1,
                colonna2: options.colonna2,
                colonna3: options.colonna3
            },

            content: 'application/json',

            success: function(data) {
               
                options.callback(data);
            },

            error: function(xhr) {
                alert("Si è verificato un errore di connessione ad internet: [" + xhr.responseText + "]");
            }

        });

    }
    
});