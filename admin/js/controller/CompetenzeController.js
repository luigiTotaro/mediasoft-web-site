var CompetenzeController =  Backbone.Model.extend({}, {
	
  	getCompetenzeList: function(options) {
  		  
         	$.ajax({

              dataType: 'json',
              url: 'phpService/CompetenzeServices.php?action=getCompetenzeList',
              async: true,

              data: {

              },

              success: function(data) {
                  options.callback(data);
              },

              error: function(xhr) {
                  alert("Si è verificato un errore di connessione ad internet: [" + xhr.responseText + "]");
              }

          });
    },


    getSezioniByPage: function(options) {

        $.ajax({

              dataType: 'json',
              url: 'phpService/CompetenzeServices.php?action=getSezioniByPage',
              async: true,

              data: {
                idPage: options.idPage 
              },

              success: function(data) {
                  options.callback(data);
              },

              error: function(xhr) {
                  alert("Si è verificato un errore di connessione ad internet: [" + xhr.responseText + "]");
              }

          });
    },

    getCompetenza: function(options) {

        $.ajax({

            dataType: 'json',
            url: 'phpService/CompetenzeServices.php?action=getCompetenza',
            async: true,

            data: {
              idCompetenza: options.idCompetenza,
              idPage: options.idPage 
            },

            success: function(data) {
                options.callback(data[0]);
            },

            error: function(xhr) {
                alert("Si è verificato un errore di connessione ad internet: [" + xhr.responseText + "]");
            }

        });

    },

    getMediaByIdCompetenza: function(options) {

        $.ajax({

            dataType: 'json',
            url: 'phpService/CompetenzeServices.php?action=getMediaByIdCompetenza',
            async: true,

            data: {
              idCompetenza: options.idCompetenza 
            },

            success: function(data) {
                options.callback(data);
            },

            error: function(xhr) {
                alert("Si è verificato un errore di connessione ad internet: [" + xhr.responseText + "]");
            }

        });

    },

    getAllegatiByIdCompetenza: function(options) {

        $.ajax({

            dataType: 'json',
            url: 'phpService/CompetenzeServices.php?action=getAllegatiByIdCompetenza',
            async: true,

            data: {
              idCompetenza: options.idCompetenza 
            },

            success: function(data) {
                options.callback(data);
            },

            error: function(xhr) {
                alert("Si è verificato un errore di connessione ad internet: [" + xhr.responseText + "]");
            }

        });

    },

    deleteCompetenza: function(options){

        $.ajax({

            url: 'phpService/CompetenzeServices.php?action=deleteCompetenza',
            type: 'POST',

            data: {
                idCompetenza: options.idCompetenza
            },

            content: 'application/json',

            success: function(data) {
                options.callback();
            },

            error: function(xhr) {
                alert("Si è verificato un errore di connessione ad internet: [" + xhr.responseText + "]");
            }

        });

    },

    deleteAllegato: function(options){

        $.ajax({

            url: 'phpService/CompetenzeServices.php?action=deleteAllegato',
            type: 'POST',

            data: {
                idAllegato: options.idAllegato
            },

            content: 'application/json',

            success: function(data) {
                options.callback();
            },

            error: function(xhr) {
                alert("Si è verificato un errore di connessione ad internet: [" + xhr.responseText + "]");
            }

        });

    },

    insertOrUpdateCompetenza: function(options){

        $.ajax({

            url: 'phpService/CompetenzeServices.php?action=' + (options.update==true ? 'updateCompetenza' : 'insertCompetenza'),
            type: 'POST',

            data: {
                path1: options.path1,
                path2: options.path2,
                path3: options.path3,
                path4: options.path4,
                nome: options.nome,
                link: options.link,
                titoloIt: options.titoloIt,
                titoloEn: options.titoloEn,
                menuIt: options.menuIt,
                menuEn: options.menuEn,
                descrizioneIt: options.descrizioneIt,
                descrizioneEn: options.descrizioneEn,
                tipoLayout: options.tipoLayout,
                posizione: options.posizione,

                titoloMediaIt1: options.titoloMediaIt1,
                titoloMediaEn1: options.titoloMediaEn1,
                titoloMediaIt2: options.titoloMediaIt2,
                titoloMediaEn2: options.titoloMediaEn2,
                titoloMediaIt3: options.titoloMediaIt3,
                titoloMediaEn3: options.titoloMediaEn3,
                titoloMediaIt4: options.titoloMediaIt4,
                titoloMediaEn4: options.titoloMediaEn4,
                descrizioneMediaIt1: options.descrizioneMediaIt1,
                descrizioneMediaEn1: options.descrizioneMediaEn1,
                descrizioneMediaIt2: options.descrizioneMediaIt2,
                descrizioneMediaEn2: options.descrizioneMediaEn2,
                descrizioneMediaIt3: options.descrizioneMediaIt3,
                descrizioneMediaEn3: options.descrizioneMediaEn3,
                descrizioneMediaIt4: options.descrizioneMediaIt4,
                descrizioneMediaEn4: options.descrizioneMediaEn4,

                idCompetenza: options.idCompetenza,
                idPage: options.idPage
            },

            content: 'application/json',

            success: function(data) {
               
                options.callback(data);
            },

            error: function(xhr) {
                alert("Si è verificato un errore di connessione ad internet: [" + xhr.responseText + "]");
            }

        });

    },

    insertOrUpdateAllegato: function(options){

        $.ajax({

            url: 'phpService/CompetenzeServices.php?action=' + (options.updateAllegato==true ? 'updateAllegato' : 'insertAllegato'),
            type: 'POST',

            data: {
                path: options.path,
                titoloIt: options.titoloIt,
                titoloEn: options.titoloEn,
                tipoAllegato: options.tipoAllegato,
                tipoIcona: options.tipoIcona,
                posizione: options.posizione,
                idCompetenza: options.idCompetenza,
                idAllegato: options.idAllegato
            },

            content: 'application/json',

            success: function(data) {
               
                options.callback(data);
            },

            error: function(xhr) {
                alert("Si è verificato un errore di connessione ad internet: [" + xhr.responseText + "]");
            }

        });

    }
    
});