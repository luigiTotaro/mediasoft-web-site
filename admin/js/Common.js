/*function hasExtension(exts) {
    return (new RegExp('(' + exts.join('|').replace(/\./g, '\\.') + ')$')).test($(this).val());
}
*/




function postDataToPrint(path, params, method) {
    method = method || "post"; // Set method to post by default, if not specified.
    var form = document.createElement("form");
    form.setAttribute("method", method);
    form.setAttribute("action", path);
    form.setAttribute("target", "_blank");
    
    for(var key in params)
    {
        if(params.hasOwnProperty(key))
        {
            var hiddenField = document.createElement("input");
            hiddenField.setAttribute("type", "hidden");
            hiddenField.setAttribute("name", key);
            hiddenField.setAttribute("value", params[key]);
            form.appendChild(hiddenField);
        }
    }
    document.body.appendChild(form);
    form.submit();
}

function hasExtension(fileName, exts) {

    return (new RegExp('(' + exts.join('|').replace(/\./g, '\\.') + ')$')).test(fileName);
}




function manageDatatable(nomeTabella, nomeTabellaWrapper, columnSort){


     var table = eval(nomeTabella);

        table.dataTable({

            "language": {
                "aria": {
                    "sortAscending": ": activate to sort column ascending",
                    "sortDescending": ": activate to sort column descending"
                },
                "emptyTable": "Nessun elemento trovato",
                "info": "Visualizzati da _START_ a _END_ di _TOTAL_ elementi",
                "infoEmpty": "Visualizzati da 0 a 0 di 0 elementi",
                "infoFiltered": "(filtered1 from _MAX_ total entries)",
                "lengthMenu": "Visualizza _MENU_ elementi per pagina",
                "paginate": {
                    "previous":"Prev",
                    "next": "Next",
                    "last": "Last",
                    "first": "First"
                },
                "search": "Ricerca:",
                "zeroRecords": "Nessun elemento trovato"
            },

            "bStateSave": true, // save datatable state(pagination, sort, etc) in cookie.
            "columns": columnSort,
            "lengthMenu": [
                [5, 15, 20, -1],
                [5, 15, 20, "Tutti"] // change per page values here
            ],
            // set the initial value
           // "pageLength": 5,    
            "iDisplayLength" : -1,        
            "pagingType": "bootstrap_full_number",
            /*"language": {
                "search": "Ricerca: ",
                "info": "Visualizzati da _START_ a _END_ di _TOTAL_ elementi",
                "infoEmpty": "Visualizzati da 0 a 0 di 0 elementi",
                "emptyTable": "Nessun elemento trovato",
                "lengthMenu": "  Visualizza _MENU_ elementi per pagina",
                "paginate": {
                    "previous":"Prev",
                    "next": "Next",
                    "last": "Last",
                    "first": "First"
                }
            },*/
            "columnDefs": [{  // set default column settings
                'orderable': false,
                'targets': [1]
            }, {
                "searchable": true,
                "targets": [0]
            }],
            "order": [
                [1, "asc"]
            ] // set first column as a default sort by asc
        });

        var tableWrapper = jQuery(eval(nomeTabellaWrapper));

        table.find('.group-checkable').change(function () {
            var set = jQuery(this).attr("data-set");
            var checked = jQuery(this).is(":checked");
            jQuery(set).each(function () {
                if (checked) {
                    $(this).attr("checked", true);
                } else {
                    $(this).attr("checked", false);
                }
            });
            jQuery.uniform.update(set);
        });

        tableWrapper.find('.dataTables_length select').select2(); // initialize select2 dropdown
  
}


function manageDatatable2(nomeTabella, nomeTabellaWrapper, columnSort){


     var table = eval(nomeTabella);

        table.dataTable({

            "language": {
                "aria": {
                    "sortAscending": ": activate to sort column ascending",
                    "sortDescending": ": activate to sort column descending"
                },
                "emptyTable": "Nessun elemento trovato",
                "info": "Visualizzati da _START_ a _END_ di _TOTAL_ elementi",
                "infoEmpty": "Visualizzati da 0 a 0 di 0 elementi",
                "infoFiltered": "(filtered1 from _MAX_ total entries)",
                "lengthMenu": "Visualizza _MENU_ elementi per pagina",
                "paginate": {
                    "previous":"Prev",
                    "next": "Next",
                    "last": "Last",
                    "first": "First"
                },
                "search": "Ricerca:",
                "zeroRecords": "Nessun elemento trovato"
            },
            "info": false,
            "paging": false,
            "searching": false,
            "ordering":  false,
            "bStateSave": true, // save datatable state(pagination, sort, etc) in cookie.
            "columns": columnSort,
            "lengthMenu": [
                [5, 15, 20, -1],
                [5, 15, 20, "Tutti"] // change per page values here
            ],
            // set the initial value
           // "pageLength": 5,    
            "iDisplayLength" : -1,        
            "pagingType": "bootstrap_full_number",
            /*"language": {
                "search": "Ricerca: ",
                "info": "Visualizzati da _START_ a _END_ di _TOTAL_ elementi",
                "infoEmpty": "Visualizzati da 0 a 0 di 0 elementi",
                "emptyTable": "Nessun elemento trovato",
                "lengthMenu": "  Visualizza _MENU_ elementi per pagina",
                "paginate": {
                    "previous":"Prev",
                    "next": "Next",
                    "last": "Last",
                    "first": "First"
                }
            },*/
            "columnDefs": [{  // set default column settings
                'orderable': false,
                'targets': [1]
            }, {
                "searchable": true,
                "targets": [0]
            }],
            "order": [
                [1, "asc"]
            ] // set first column as a default sort by asc
        });

        var tableWrapper = jQuery(eval(nomeTabellaWrapper));

        table.find('.group-checkable').change(function () {
            var set = jQuery(this).attr("data-set");
            var checked = jQuery(this).is(":checked");
            jQuery(set).each(function () {
                if (checked) {
                    $(this).attr("checked", true);
                } else {
                    $(this).attr("checked", false);
                }
            });
            jQuery.uniform.update(set);
        });

        tableWrapper.find('.dataTables_length select').select2(); // initialize select2 dropdown
  
}
