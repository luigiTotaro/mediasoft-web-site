<?php
/**
 * Created by PhpStorm.
 * User: federicopici
 * Date: 12/02/14
 * Time: 11:35
 */


require("mail/PHPMailerAutoload.php");

class SmtpWrapper {

    private $smtp_host;
    private $smtp_username;
    private $smtp_password;
    private $smtp_port;

    function __construct($smtp_host, $smtp_username, $smtp_password, $smtp_port) {

        $this->smtp_host = $smtp_host;
        $this->smtp_username = $smtp_username;
        $this->smtp_password = $smtp_password;
        $this->smtp_port = $smtp_port;

    }

    function sendMail($from_name, $from_email, $subject, $to, $body) {

        //Create a new PHPMailer instance
        $mail = new PHPMailer();

        $mail->CharSet = 'UTF-8';

        //Tell PHPMailer to use SMTP
        $mail->isSMTP();

        //Enable SMTP debugging
        // 0 = off (for production use)
        // 1 = client messages
        // 2 = client and server messages
        $mail->SMTPDebug = 0;

        //Ask for HTML-friendly debug output
        $mail->Debugoutput = 'html';

        //Set the hostname of the mail server
        $mail->Host = $this->smtp_host;

        //Set the SMTP port number - likely to be 25, 465 or 587
        $mail->Port = $this->smtp_port;

        //Whether to use SMTP authentication
        if($this->smtp_username || $this->smtp_password) {

            $mail->SMTPAuth = true;
            //Username to use for SMTP authentication
            $mail->Username = $this->smtp_username;
            //Password to use for SMTP authentication
            $mail->Password = $this->smtp_password;

        } else {

            $mail->SMTPAuth = true;

        }

        //Set who the message is to be sent from
        $mail->setFrom($from_email, $from_name);

        //Set an alternative reply-to address
        $mail->addReplyTo($from_email, $from_name);

        //Set who the message is to be sent to
        $mail->addAddress($to);

        //Set the subject line
        $mail->Subject = $subject;

        //Read an HTML message body from an external file, convert referenced images to embedded,
        //convert HTML into a basic plain-text alternative body
        $mail->msgHTML(nl2br($body));

        //Replace the plain text body with one created manually
        $mail->AltBody = $body;

        //send the message, check for errors
        if (!$mail->send()) {

            return $mail->ErrorInfo;

        } else {

            return "";

        }

    }
} 