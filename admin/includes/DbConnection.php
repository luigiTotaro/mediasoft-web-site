<?php

	class DbConnection {
/*
		private $host = "localhost";
		private $db = "sitomediasoft";
		private $username = "root";
		private $password = "root";
*/		
		
		private $host = "46.37.4.164";
		private $db = "sitomediasoft_new";
		private $username = "sitomediasoft";
		private $password = "sitomediasoft";

		function openConnection() {

			$db = null;

			try {

				//$db = new PDO('mysql:host=' . $this->host . ';port=' . $this->$port .';dbname='. $this->db, $this->username, $this->password);
				//$db = new PDO('mysql:host=' . $this->host . ';dbname='. $this->db, $this->username, $this->password);
				$db = new PDO('mysql:host=' . $this->host . ';dbname='. $this->db, $this->username, $this->password, array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
			
			} catch(PDOException $e) {
			 
			  echo 'Attenzione: '.$e->getMessage();
			
			}

			return $db;

		}


		function executeQuery($query, $withTotal=false) {

			$db = $this->openConnection();
			$sql = $db->prepare($query);
			$sql->execute();
            $results = $sql->fetchAll(PDO::FETCH_ASSOC);

            if($withTotal) {

                $sql = $db->prepare("SELECT FOUND_ROWS();");
                $sql->execute();
                $total = $sql->fetch(PDO::FETCH_COLUMN);

                return array(
                    'results'   =>  $results,
                    'total'     =>  $total
                );

            } else {

                return $results;

            }


		}


        function insertQuery($insert) {

            $db = $this->openConnection();
            $sql = $db->prepare($insert);
            $sql->execute();
            return $db->lastInsertId();

        }


        function updateQuery($update) {

            $db = $this->openConnection();
            $sql = $db->prepare($update);
            $sql->execute();

        }


	}

?>
