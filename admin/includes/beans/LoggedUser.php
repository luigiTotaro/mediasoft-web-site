<?php

class LoggedUser {

	private $id;
	private $username;
	private $nome;
	private $cognome;
	private $email;
	private $telefono;
	private $funzioniUtente =array();

	function getId() {
		return $this->id;
	}
	function setId($id) {
		$this->id = $id;
	}

	function getUsername() {
		return $this->username;
	}
	function setUsername($username) {
		$this->username = $username;
	}

	function getNome() {
		return $this->nome;
	}
	function setNome($nome) {
		$this->nome = $nome;
	}

	function getCognome() {
		return $this->cognome;
	}
	function setCognome($cognome) {
		$this->cognome = $cognome;
	}
	
	function getMail() {
		return $this->email;
	}
	function setMail($email) {
		$this->email = $email;
	}

	function getTelefono() {
		return $this->telefono;
	}
	function setTelefono($telefono) {
		$this->telefono = $telefono;
	}

	function getFunzioni(){
		return $this->funzioniUtente;
	}

	function addFunzione($funzioneUtente) {
		array_push($this->funzioniUtente, $funzioneUtente);
	}

	function toArray() {

		$funzioni_array = array();

		foreach($this->funzioniUtente as $funzioneUtente) {
			array_push($funzioni_array, $funzioneUtente->toArray());
		}


		return 
			array(
				"Id"		=>  $this->id,
				"Nome"		=>	$this->nome,
				"Cognome"	=>	$this->cognome,
				"Username"	=>	$this->username,
				"Email"		=>	$this->email,
				"Telefono"	=>	$this->telefono,
			    "Funzioni"	=>  $funzioni_array
			);
			

	}
	
	function toJson() {

		echo json_encode($this->toArray());
		 
	}
}


?>