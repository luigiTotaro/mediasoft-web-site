<?php

	class Funzione {
		
		private $id;
		private $nome;
		private $class_name;
		private $icon;
		private $ordine;
		private $abilitata;

		function getId() {
			return $this->id;
		}		
		function setId($id) {
			$this->id = $id;
		}	
		function getNome(){
			return $this->nome;
		}		
		function setNome($nome){
			$this->nome = $nome;
		}	

		function getClassName(){
			return $this->class_name;
		}		
		function setClassName($class_name){
			$this->class_name = $class_name;
		}	

		function getIcon(){
			return $this->icon;
		}		
		function setIcon($icon){
			$this->icon = $icon;
		}		
		
		function isAbilitata(){
			return $this->abilitata;
		}
		function setAbilitata($abilitata){
			$this->abilitata = $abilitata;
		}
		
		function getOrdine(){
			return $this->ordine;
		}
		function setOrdine($ordine){
			$this->ordine=$ordine;
		}

		function toArray() {
			return 
				array(
					"Id"		=>  $this->id,	
					"Nome"		=>  $this->nome,
					"ClassName"	=>  $this->class_name,
					"Icon"		=>  $this->icon
				);
		}


	}
?>