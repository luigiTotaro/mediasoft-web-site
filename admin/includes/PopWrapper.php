<?php
/**
 * Created by PhpStorm.
 * User: federicopici
 * Date: 11/02/14
 * Time: 10:17
 */


error_reporting(E_ALL);
ini_set('display_errors', '1');

require("mail/mime_parser.php");
require("mail/rfc822_addresses.php");
require("mail/pop3.php");


stream_wrapper_register('mlpop3', 'pop3_stream');  /* Register the pop3 stream handler class */


class PopWrapper {

    private $pop3 = null;
    private $hostname = null;
    private $username = null;
    private $password = null;
    private $apop = 0;

    function __construct($host, $username, $password, $port) {

        $this->pop3 = new pop3_class();
        $this->pop3->hostname = $host;                      /* POP 3 server host name                      */
        $this->pop3->port = $port;                          /* POP 3 server port                           */
        $this->pop3->tls = 0;                               /* Establish secure connections using TLS      */
        $this->username = $username;                        /* Authentication user name                    */
        $this->password = $password;                        /* Authentication password                     */
        $this->pop3->realm = "";                            /* Authentication realm or domain              */
        $this->pop3->workstation = "";                      /* Workstation for NTLM authentication         */
        $this->apop=0;                                      /* Use APOP authentication                     */
        $this->pop3->authentication_mechanism = "USER";     /* SASL authentication mechanism               */
        $this->pop3->debug = 0;                             /* Output debug information                    */
        $this->pop3->html_debug = 0;                        /* Debug information is in HTML                */
        $this->pop3->join_continuation_header_lines = 1;    /* Concatenate headers split in multiple lines */

    }

    /**
     * Apertura della connessione al pop3 e login
     */
    function OpenConnection() {

        $error = $this->pop3->Open();

        if($error) {
            echo $error;
            return;
        }

        $error = $this->pop3->Login($this->username, $this->password, $this->apop);

        if($error) {
            echo $error;
            return;
        }

    }


    /**
     * Chiusura della connessione al pop3
     */
    function CloseConnection() {

        $error = $this->pop3->Close();

        if($error) {
            echo $error;
            return;
        }

    }

    /**
     * restituzione lista message_id
     */
    function getEmailCount() {

        $error = $this->pop3->Statistics($msgCount, $size);

        if($error) {
            echo $error;
            return;
        }

        return $msgCount;

    }

    /**
     * restituzione lista message_id
     */
    function getMessageIdList() {

        $result = $this->pop3->ListMessages("", 1);

        if(GetType($result) != "array") {
            echo $result;
            return;
        }

        return $result;

    }




    function getHeaders($total, $array_messageid) {

        $header_list = array();

        for($i=0; $i<count($array_messageid); $i++) {

            $json_message = $this->getMessage($array_messageid[$i]["index"], $array_messageid[$i]["message_id"], true);
            array_push($header_list, $json_message);

        }
        
        return $header_list;

    }


    function getMessage($message_index, $message_id, $only_headers) {

        $this->pop3->GetConnectionName($connection_name);
        $message_file = 'mlpop3://' . $connection_name . '/' . $message_index;
        $mime = new mime_parser_class();

        $mime->decode_bodies = 1;

        $parameters = array(
            'File'=>$message_file,

            /* Read a message from a string instead of a file */
            /* 'Data'=>'My message data string',              */

            /* Save the message body parts to a directory     */
            /* 'SaveBody'=>'/tmp',                            */

            /* Do not retrieve or save message body parts     */
            'SkipBody' => $only_headers ? 1 : 0,
        );

        $success = $mime->Decode($parameters, $decoded);

        $mime->Analyze($decoded[0], $results,true);

/*
        if(!$only_headers) {

            $doc_file_handler = fopen("file" . $message_index . ".txt", 'w') or die("can't open file");

            $body = print_r($results, true);

            $body .= isset($results['Alternative'][0]['Data']) ? utf8_encode($results['Alternative'][0]['Data']) : "";

            $body .= isset($results['Alternative'][0]['Data']) ? "ORIG -- ". $results['Alternative'][0]['Data'] : "";
            fwrite($doc_file_handler, $body);
            fclose($doc_file_handler);

        }
*/


        $message = new MailMessage($message_index, $message_id, $results);

        return $message->toArray();

    }

    function deleteMessage($message_index) {

        $this->pop3->DeleteMessage($message_index);

    }


}




class MailMessage {

    private $subject;
    private $date;
    private $fromMail;
    private $fromName;
    private $messageId;
    private $index;
    private $body = "";

    function __construct($index, $messageId, $rawdata) {

        $this->index = $index;
        $this->messageId = $messageId;
        $this->date = $rawdata['Date'];
        $this->subject = $rawdata['Subject'];
        $this->fromMail = $rawdata['From'][0]['address'];
        $this->fromName = $rawdata['From'][0]['name'];
        $this->body = isset($rawdata['Alternative'][0]['Data']) ? $rawdata['Alternative'][0]['Data'] : "";

    }

    function toArray() {

        return array(
            "index"         =>  $this->index,
            "message_id"    =>  $this->messageId,
            "date"          =>  $this->date,
            "subject"       =>  utf8_encode( trim($this->subject) ),
            "from_name"     =>  $this->fromName,
            "from_email"    =>  $this->fromMail,
            "body"          =>  utf8_encode( trim($this->body))
        );
    }

}