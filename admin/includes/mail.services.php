<?php


include("PopWrapper.php");
include("SmtpWrapper.php");


$action = $_REQUEST['action'];


switch ($action) {

    case 'getEmailCount':
        {

            $popwrapper = new PopWrapper($_REQUEST['pop3_host'], $_REQUEST['pop3_username'], $_REQUEST['pop3_password'], 110);
            $popwrapper->OpenConnection();
            $count = $popwrapper->getEmailCount();
            $popwrapper->CloseConnection();
            echo json_encode($count);

        }
        break;



    case 'getMessageIdList':
        {
            $message_id_list = array();
            $popwrapper = new PopWrapper($_REQUEST['pop3_host'], $_REQUEST['pop3_username'], $_REQUEST['pop3_password'], 110);
            $popwrapper->OpenConnection();
            $mail_list = $popwrapper->getMessageIdList();
            $popwrapper->CloseConnection();

            for($i=count($mail_list); $i>0; $i--) {
                array_push($message_id_list, array(
                    "index" => $i,
                    "message_id" => $mail_list[$i]
                ));
            }

            echo json_encode($message_id_list);

        }
        break;



    case 'getHeaders':
        {
            
            $popwrapper = new PopWrapper($_REQUEST['pop3_host'], $_REQUEST['pop3_username'], $_REQUEST['pop3_password'], 110);
            $popwrapper->OpenConnection();
            $headers = $popwrapper->getHeaders(0, json_decode(stripslashes($_REQUEST['message_id_list']), true)) ;
            $popwrapper->CloseConnection();

            echo json_encode($headers);

        }
        break;

    case 'getFullEmail':
        {

            $popwrapper = new PopWrapper($_REQUEST['pop3_host'], $_REQUEST['pop3_username'], $_REQUEST['pop3_password'], 110);
            $popwrapper->OpenConnection();
            $message = $popwrapper->getMessage($_REQUEST['message_index'], '', false);
            $popwrapper->CloseConnection();

            echo json_encode($message);

        }
        break;

    case 'sendMail':
        {
            $smptwrapper = new SmtpWrapper($_REQUEST['smtp_host'], $_REQUEST['smtp_username'], $_REQUEST['smtp_password'], 25);
            $response = $smptwrapper->sendMail($_REQUEST['fromName'], $_REQUEST['fromEmail'], $_REQUEST['subject'], $_REQUEST['destaddress'], $_REQUEST['message']);
            return json_encode(response);
        }
        break;

    case 'deleteMessage':
        {
            $popwrapper = new PopWrapper($_REQUEST['pop3_host'], $_REQUEST['pop3_username'], $_REQUEST['pop3_password'], 110);
            $popwrapper->OpenConnection();
            $message = $popwrapper->deleteMessage($_REQUEST['message_index']);
            $popwrapper->CloseConnection();
        }
        break;
}
?>