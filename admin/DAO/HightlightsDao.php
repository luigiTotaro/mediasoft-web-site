<?php	

	require_once("../includes/DbConnection.php");	
	
	error_reporting(E_ERROR);
	ini_set('display_errors', 1);
	
	 class HightlightsDao {
		

	 	static function getHightlightsList(){

			$dbConnection = new DbConnection();

			$query = "select m.id,m.URL as path,m.URL_link as link,mit.titolo as titolo  
							from pagina_sezione ps, sezione_media sm, media m,media_it mit
							where 
							sm.media_id = m.id
							and mit.media_id = m.id
							and sm.sezione_id = ps.sezione_id
							and ps.pagina_id = 4 order by m.id";
						
			/* $myfile = fopen("mylog.txt", "w") or die("Unable to open file!");
			 fwrite($myfile, $query);
			 fclose($myfile);	*/
					
            $res = $dbConnection->executeQuery($query);

           	$dbConnection = null;

           	return $res;
		}

		static function getHightlight($idHightlight){
			
			$dbConnection = new DbConnection();

			$query ="select m.id id,m.URL path,m.nome nome,m.URL_link link,mit.titolo titoloIT,mit.descrizione descrizioneIT,men.titolo titoloEN,men.descrizione descrizioneEN,sm.posizione posizione,sm.abilitata abilitata
				from media m
				inner join media_it mit on mit.media_id = m.id
				inner join media_en men on men.media_id = m.id
				inner join sezione_media sm on sm.media_id = m.id
				where m.id = " . $idHightlight;
						
			//$myfile = fopen("getHL.txt", "w") or die("Unable to open file!");
			//fwrite($myfile, $query);
			//fwrite($myfile, print_r($res,true));
            //fclose($myfile);	
					
            $res = $dbConnection->executeQuery($query);
            
           	$dbConnection = null;

           	return $res;
		}

		static function deleteHightlight($idHightlight){

        	$dbConnection = new DbConnection();

            $query = "DELETE FROM media WHERE id = " . $idHightlight ." ";

            //$myfile = fopen("myDelete.txt", "w") or die("Unable to open file!");
			// fwrite($myfile, $query);
			// fclose($myfile);


            $dbConnection->updateQuery($query);

            $query = "DELETE FROM sezione_media WHERE media_id = " . $idHightlight ." ";
            $dbConnection->updateQuery($query);

            $query = "DELETE FROM media_it WHERE media_id = " . $idHightlight ." ";
            $dbConnection->updateQuery($query);

            $query = "DELETE FROM media_en WHERE media_id = " . $idHightlight ." ";
            $dbConnection->updateQuery($query);
          
            $dbConnection = null;

        }

        static function insertHightlight($path,$nome,$link,$titoloIt,$titoloEn,$descrizioneIt,$descrizioneEn,$posizione,$abilitata){

           		$dbConnection = new DbConnection();

           		//ottengo il max+1 id in quanto non è autoincrementante
           		$query="select max(id)+1 as max from media";
				$max = $dbConnection->executeQuery($query);

           		$query ="INSERT INTO media  (id,tipo, URL, nome, URL_link) " .
								"VALUES (".print_r($max[0]["max"],true).",1,'" . mysql_escape_string($path) . "','" . mysql_escape_string($nome) . "'," . "'" . mysql_escape_string($link) . "')";
			    $idHightlight = $dbConnection->insertQuery($query);

	            $query = "insert into sezione_media (sezione_id,media_id,posizione,abilitata) values (5,".print_r($max[0]["max"],true).",".$posizione.",".$abilitata.")";
	            $dbConnection->insertQuery($query);

	            $query = "insert into media_it (titolo, descrizione,media_id) values ('" . mysql_escape_string($titoloIt) . "','" . mysql_escape_string($descrizioneIt) . "',".print_r($max[0]["max"],true).")";
	            $dbConnection->insertQuery($query);

	            $query = "insert into media_en (titolo, descrizione,media_id) values ('" . mysql_escape_string($titoloEn) . "','" . mysql_escape_string($descrizioneEn) . "',".print_r($max[0]["max"],true).")";
	            $dbConnection->insertQuery($query);

	            $dbConnection = null;

			    return print_r($max[0]["max"],true);
		}

		static function updateHightlight($idHightlight,$path,$nome,$link,$titoloIt,$titoloEn,$descrizioneIt,$descrizioneEn,$posizione,$abilitata){

			$dbConnection = new DbConnection();

			$query = "UPDATE media SET nome = '" . mysql_escape_string($nome) . "', URL = '". mysql_escape_string($path) . "', " .
					 "URL_link = '".mysql_escape_string($link) . "' where id = " . $idHightlight ;
            $dbConnection->updateQuery($query);

            $query = "update sezione_media set posizione = ". $posizione.",abilitata = ". $abilitata." where media_id= ".$idHightlight;
            $dbConnection->updateQuery($query);

            $query = "update media_it set titolo = '". mysql_escape_string($titoloIt) . "',descrizione = '". mysql_escape_string($descrizioneIt) . "' where media_id= ".$idHightlight;
            $dbConnection->updateQuery($query);

            $query = "update media_en set titolo = '". mysql_escape_string($titoloEn) . "',descrizione = '". mysql_escape_string($descrizioneEn) . "' where media_id= ".$idHightlight;
            $dbConnection->updateQuery($query);


            $dbConnection = null;
            return $idHightlight;
		}

}
?>