<?php	

require_once("../includes/DbConnection.php");	
	
error_reporting(E_ERROR);
ini_set('display_errors', 1);
	
class ClientiDao {
		
 	static function getClientiList(){

		$dbConnection = new DbConnection();

		$query = "select id id,ifnull(nome,'') nome,ifnull(path,'') path,tipo tipo,sezione sezione,posizione posizione,
		(select testo_colonna from cliente where id=39) colonna1,
		(select testo_colonna from cliente where id=40) colonna2,
		(select testo_colonna from cliente where id=41) colonna3
		FROM cliente where id not in (39,40,41)  order by nome desc,posizione desc";
					
		/* $myfile = fopen("mylog.txt", "w") or die("Unable to open file!");
		 fwrite($myfile, $query);
		 fclose($myfile);	*/
				
        $res = $dbConnection->executeQuery($query);

       	$dbConnection = null;

       	return $res;
	}

	static function getCliente($idCliente){
		
		$dbConnection = new DbConnection();

		$query ="select id id,ifnull(nome,'') nome,ifnull(path,'') path,tipo tipo,sezione sezione,posizione posizione
		 from cliente where id = " . $idCliente;

		/*$myfile = fopen("getClienti.txt", "w") or die("Unable to open file!");
		 fwrite($myfile, $query);
		 fclose($myfile);*/
					
		//$myfile = fopen("getHL.txt", "w") or die("Unable to open file!");
		//fwrite($myfile, $query);
		//fwrite($myfile, print_r($res,true));
        //fclose($myfile);	
				
        $res = $dbConnection->executeQuery($query);
        
       	$dbConnection = null;

       	return $res;
	}

	static function deleteCliente($idCliente){

    	$dbConnection = new DbConnection();

        $query = "DELETE FROM cliente WHERE id = " . $idCliente ." ";
        $dbConnection->updateQuery($query);
      
        $dbConnection = null;

    }

    static function insertCliente($path,$nome,$sezione,$posizione){

   		$dbConnection = new DbConnection();

        $query = "insert into cliente (nome,path,tipo,sezione,posizione) values ('" . mysql_escape_string($nome) . "','" . mysql_escape_string($path) . "',1,".$sezione.",".$posizione.")";
        $dbConnection->insertQuery($query);

        /*$myfile = fopen("insertClienti.txt", "w") or die("Unable to open file!");
		 fwrite($myfile, $query);
		 fclose($myfile);*/

        $dbConnection = null;
	    return $idCliente;
	}

	static function updateCliente($idCliente,$path,$nome,$sezione,$posizione){

		$dbConnection = new DbConnection();

        $query = "update cliente set nome = '". mysql_escape_string($nome) . "',path = '". mysql_escape_string($path) . "',sezione = ".$sezione. ",posizione = ".$posizione. " where id= ".$idCliente;
        $dbConnection->updateQuery($query);

        $dbConnection = null;
        return $idCliente;
	}

	static function updateColonneTestuali($colonna1,$colonna2,$colonna3){

		$dbConnection = new DbConnection();

        $query = "update cliente set testo_colonna = '". mysql_escape_string($colonna1) . "' where id= 39";
        $dbConnection->updateQuery($query);

        $query = "update cliente set testo_colonna = '". mysql_escape_string($colonna2) . "' where id= 40";
        $dbConnection->updateQuery($query);

        $query = "update cliente set testo_colonna = '". mysql_escape_string($colonna3) . "' where id= 41";
        $dbConnection->updateQuery($query);

        $dbConnection = null;
        return $idCliente;
	}

}
?>