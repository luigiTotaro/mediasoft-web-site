<?php
	
	require_once("./includes/DbConnection.php");	
	require_once("./includes/beans/LoggedUser.php");
	require_once("./includes/beans/Funzione.php");
	
	error_reporting(E_ERROR);
	ini_set('display_errors', 1);
	
	 class LoggedUserDao {
		
		 static function getAuthenticatedUser($username, $pwd) {

			$loggedUser = null;
			$user = array();
			
			$dbConnection = new DbConnection();
			
			$query =
			"select u.id idUtente, u.nome nomeUtente, u.cognome cognomeUtente,u.email email,u.telefono telefono,
			 f.id idFunzione ,f.nome nomeFunzione, f.ordine ordineFunzione, f.class_name, f.icon 
			
			from utente u
			inner join rel_utente_funzione rel1 on rel1.fk_utente = u.id
			inner join funzione f on rel1.fk_funzione = f.id

			where u.username='$username' and u.password='$pwd' and u.abilitato = 1 and f.abilitata = 1
				
			order by f.ordine";
			
			//$myfile = fopen("getAuthenticatedUser.txt", "w") or die("Unable to open file!");
			//fwrite($myfile, $query);
			
			$res = $dbConnection->executeQuery($query);
			
			if(count($res) > 0) {
	

			    $loggedUser = LoggedUserDao::createBaseLoggedUser($res[0]);
			    //fwrite($myfile, print_r($loggedUser,true));

			    foreach($res as $item) {

					$funzione = LoggedUserDao::createFunction($item);

					if( count($loggedUser->getFunzioni()) == 0 || end($loggedUser->getFunzioni())->getId() != $funzione->getId() ) {
						 $loggedUser->addFunzione($funzione);
					}

				}

			} 
			////fwrite($myfile, print_r($loggedUser,true));
            //fclose($myfile);
			$dbConnection = null;
			
			return $loggedUser;
		}

		static function createBaseLoggedUser($row){
				$user = new loggedUser();
				$user->setId($row["idUtente"]);
				$user->setNome($row["nomeUtente"]);
				$user->setCognome($row["cognomeUtente"]);
				$user->setUsername($row["username"]);
				$user->setMail($row["email"]);
				$user->setTelefono($row["telefono"]);
				return $user;
		}
	
		static function createFunction($row){
			$funzione = new Funzione();
			$funzione->setId($row["idFunzione"]);
			$funzione->setNome($row["nomeFunzione"]);
			$funzione->setClassName($row["class_name"]);
			$funzione->setIcon($row["icon"]);
			$funzione->setOrdine($row["ordineFunzione"]);
			return $funzione;
		}
		
	}
?>