<?php	

require_once("../includes/DbConnection.php");	

error_reporting(E_ERROR);
ini_set('display_errors', 1);

class ProdottiDao {
		
 	static function getProdottiList(){

		$dbConnection = new DbConnection();

		$query = "select  sezione.id, sezione.nome titolo, ifnull(sezione.URL,'') link, pagina_sezione.position posizione 
					FROM pagina_sezione, sezione
					left join sezione_it ON sezione.id = sezione_it.sezione_id
					where sezione.id=pagina_sezione.sezione_id
					and pagina_sezione.pagina_id=1
					and (sezione.tipo = 8 or sezione.tipo = 12)
					order by pagina_sezione.position";
					
		/* $myfile = fopen("mylog.txt", "w") or die("Unable to open file!");
		 fwrite($myfile, $query);
		 fclose($myfile);	*/
				
        $res = $dbConnection->executeQuery($query);

       	$dbConnection = null;

       	return $res;
	}

	static function getProdotto($idProdotto){
		
		$dbConnection = new DbConnection();

		$query ="select  sezione.id,sezione.tipo layout, sezione.nome nome, ifnull(sezione.URL,'') link,
					sezione_it.titolo titoloIT,sezione_it.testo testoIT,sezione_it.titolo_menu_laterale titolo_menu_laterale_it,
					sezione_en.titolo titoloEN,sezione_en.testo testoEN,sezione_en.titolo_menu_laterale titolo_menu_laterale_en, pagina_sezione.position posizione
											FROM pagina_sezione, sezione
											left join sezione_it ON sezione.id = sezione_it.sezione_id
											left join sezione_en ON sezione.id = sezione_en.sezione_id
											where sezione.id=pagina_sezione.sezione_id
											and pagina_sezione.pagina_id=1
											and (sezione.tipo = 8 or sezione.tipo = 12)
											and sezione.id = ".$idProdotto."
											order by pagina_sezione.position";
					
		//$myfile = fopen("getHL.txt", "w") or die("Unable to open file!");
		//fwrite($myfile, $query);
		//fwrite($myfile, print_r($res,true));
        //fclose($myfile);	
				
        $res = $dbConnection->executeQuery($query);
        
       	$dbConnection = null;

       	return $res;
	}

	static function getMediaByIdProdotto($idProdotto){
		
		$dbConnection = new DbConnection();

		$query ="select media.id, media.URL path,  sezione_media.posizione , COALESCE(media_it.titolo,'') as titoloIT
						FROM sezione_media, media
						left join media_it ON media.id = media_it.media_id
						where sezione_media.media_id = media.id
						and sezione_media.sezione_id = ".$idProdotto."
						order by sezione_media.sotto_sezione, sezione_media.posizione";
									
        $res = $dbConnection->executeQuery($query);
        
       	$dbConnection = null;

       	return $res;
	}

	static function getAllegatiByIdProdotto($idProdotto){
		
		$dbConnection = new DbConnection();

		$query ="select allegato.id, allegato.tipo, allegato.tipo_icona icona, allegato.link path, allegato.testoIT testoIT,allegato.testoEN testoEN, posizione posizione
					FROM allegato
					WHERE allegato.sezione_id = ".$idProdotto."
					order by tipo,posizione";	
				
        $res = $dbConnection->executeQuery($query);
        
       	$dbConnection = null;

       	return $res;
	}

	static function deleteProdotto($idProdotto){

    	$dbConnection = new DbConnection();

        $query = "DELETE FROM allegato WHERE sezione_id = " . $idProdotto ." ";
        $dbConnection->updateQuery($query);

        $query = "DELETE FROM sezione_media WHERE sezione_id = " . $idProdotto ." ";
        $dbConnection->updateQuery($query);

        $query = "DELETE FROM pagina_sezione WHERE sezione_id = " . $idProdotto ." ";
        $dbConnection->updateQuery($query);

        $query = "DELETE FROM sezione_it WHERE sezione_id = " . $idProdotto ." ";
        $dbConnection->updateQuery($query);

        $query = "DELETE FROM sezione_en WHERE sezione_id = " . $idProdotto ." ";
        $dbConnection->updateQuery($query);

        $query = "DELETE FROM sezione WHERE id = " . $idProdotto ." ";
        $dbConnection->updateQuery($query);
      
        $dbConnection = null;

    }

    static function insertProdotto($path1,$path2,$path3,$path4,$nome,$link,$titoloIt,$titoloEn,$descrizioneIt,$descrizioneEn,$menuIt,$menuEn,$tipoLayout,$posizione){

   		$dbConnection = new DbConnection();     

   		$query ="INSERT INTO sezione  (tipo,nome,URL) " .
						"VALUES (".$tipoLayout.",'" . mysql_escape_string($nome) . "'," . "'" . mysql_escape_string($link) . "')";
	    $idProdotto = $dbConnection->insertQuery($query);


	    $query = "insert into sezione_it (titolo, testo, sezione_id, titolo_menu_laterale) values ('" . mysql_escape_string($titoloIt) . "','" . $descrizioneIt . "',".$idProdotto.",'" . mysql_escape_string($menuIt) . "')";
        $dbConnection->insertQuery($query);

        $query = "insert into sezione_en (titolo, testo, sezione_id, titolo_menu_laterale) values ('" . mysql_escape_string($titoloEn) . "','" . $descrizioneEn . "',".$idProdotto.",'" . mysql_escape_string($menuEn) . "')";
        $dbConnection->insertQuery($query);

        $query = "insert into pagina_sezione (pagina_id, sezione_id, position) values ('1'," . $idProdotto . ",'" . mysql_escape_string($posizione) . "')";
        $dbConnection->insertQuery($query);


        //cancello e riscrivo media
        $query = "DELETE FROM sezione_media WHERE sezione_id = " . $idProdotto ." and posizione = 1 ";
    	$dbConnection->updateQuery($query);

        //media1
        $query ="insert into media  (tipo, URL) values (1,'" . mysql_escape_string($path1) . "')";
	    $idMedia1 = $dbConnection->insertQuery($query);

        $query = "insert into sezione_media (sezione_id,media_id,posizione,abilitata) values (" . $idProdotto . ",".$idMedia1.",1,1)";
        $dbConnection->insertQuery($query);

        /*media2
        if($path2 != ''){
        	$query ="insert into media  (tipo, URL) values (1,'" . mysql_escape_string($path2) . "')";
		    $idMedia2 = $dbConnection->insertQuery($query);

            $query = "insert into sezione_media (sezione_id,media_id,posizione,abilitata) values (" . $idProdotto . ",".$idMedia2.",2,1)";
            $dbConnection->insertQuery($query);
        }
        
        //media3
        if($path3 != ''){
        	$query ="insert into media  (tipo, URL) values (1,'" . mysql_escape_string($path3) . "')";
	    	$idMedia3 = $dbConnection->insertQuery($query);

        	$query = "insert into sezione_media (sezione_id,media_id,posizione,abilitata) values (" . $idProdotto . ",".$idMedia3.",3,1)";
        	$dbConnection->insertQuery($query);
        }

        //media4
        if($path4 != ''){
        	$query ="insert into media  (tipo, URL) values (1,'" . mysql_escape_string($path4) . "')";
	    	$idMedia4 = $dbConnection->insertQuery($query);

        	$query = "insert into sezione_media (sezione_id,media_id,posizione,abilitata) values (" . $idProdotto . ",".$idMedia4.",4,1)";
        	$dbConnection->insertQuery($query);
        }*/

        $dbConnection = null;

	    return $idProdotto;
	}

	static function updateProdotto($idProdotto,$path1,$path2,$path3,$path4,$nome,$link,$titoloIt,$titoloEn,$descrizioneIt,$descrizioneEn,$menuIt,$menuEn,$tipoLayout,$posizione){

		$dbConnection = new DbConnection();

        $query = "UPDATE sezione SET tipo = ".$tipoLayout.", nome = '" . mysql_escape_string($nome) . "', URL = '". mysql_escape_string($link) . "' where id = " . $idProdotto ;
        $dbConnection->updateQuery($query);

        $query = "update sezione_it set titolo = '". mysql_escape_string($titoloIt) . "',testo = '". $descrizioneIt . "',titolo_menu_laterale = '". mysql_escape_string($menuIt) . "' where sezione_id= ".$idProdotto;
        $dbConnection->updateQuery($query);

        $query = "update sezione_en set titolo = '". mysql_escape_string($titoloEn) . "',testo = '". $descrizioneEn . "',titolo_menu_laterale = '". mysql_escape_string($menuEn) . "' where sezione_id= ".$idProdotto;
        $dbConnection->updateQuery($query);

        $query = "update pagina_sezione set position = '". mysql_escape_string($posizione) . "' where sezione_id= ".$idProdotto;
        $dbConnection->updateQuery($query);


        //cancello e riscrivo media
        $query = "DELETE FROM sezione_media WHERE sezione_id = " . $idProdotto ." and posizione = 1  ";
        $dbConnection->updateQuery($query);

        //media1
        $query ="insert into media  (tipo, URL) values (1,'" . mysql_escape_string($path1) . "')";
	    $idMedia1 = $dbConnection->insertQuery($query);

        $query = "insert into sezione_media (sezione_id,media_id,posizione,abilitata) values (" . $idProdotto . ",".$idMedia1.",1,1)";
        $dbConnection->insertQuery($query);

        /*media2
        if($path2 != ''){
        	$query ="insert into media  (tipo, URL) values (1,'" . mysql_escape_string($path2) . "')";
		    $idMedia2 = $dbConnection->insertQuery($query);

            $query = "insert into sezione_media (sezione_id,media_id,posizione,abilitata) values (" . $idProdotto . ",".$idMedia2.",2,1)";
            $dbConnection->insertQuery($query);
        }
        
        //media3
        if($path3 != ''){
        	$query ="insert into media  (tipo, URL) values (1,'" . mysql_escape_string($path3) . "')";
	    	$idMedia3 = $dbConnection->insertQuery($query);

        	$query = "insert into sezione_media (sezione_id,media_id,posizione,abilitata) values (" . $idProdotto . ",".$idMedia3.",3,1)";
        	$dbConnection->insertQuery($query);
        }

        //media4
        if($path4 != ''){
        	$query ="insert into media  (tipo, URL) values (1,'" . mysql_escape_string($path4) . "')";
	    	$idMedia4 = $dbConnection->insertQuery($query);

        	$query = "insert into sezione_media (sezione_id,media_id,posizione,abilitata) values (" . $idProdotto . ",".$idMedia4.",4,1)";
        	$dbConnection->insertQuery($query);
        }*/

        $dbConnection = null;
        return $idProdotto;
	}

    static function insertAllegato($idProdotto,$path,$titoloIt,$titoloEn,$tipoAllegato,$tipoIcona,$posizione){

        $isUrl = 0;
        //if($tipoAllegato == 2){
        //    $isUrl = 1;
        //}

        $dbConnection = new DbConnection();     

        $query ="insert into allegato (sezione_id,link,descrizione,testoIT,testoEN,tipo,tipo_icona,posizione,isUrl) VALUES (".$idProdotto.",'" . mysql_escape_string($path) . "',
            " . "'" . mysql_escape_string($titoloIt) . "'," . "'" . mysql_escape_string($titoloIt) . "'," . "'" . mysql_escape_string($titoloEn) . "',
            ".$tipoAllegato.",".$tipoIcona.",".$posizione.",".$isUrl.")";
        $idAllegato = $dbConnection->insertQuery($query);

        $dbConnection = null;

        return $idAllegato;
    }

    static function deleteAllegato($idAllegato){

        $dbConnection = new DbConnection();

        $query = "DELETE FROM allegato WHERE id = " . $idAllegato ." ";
        $dbConnection->updateQuery($query);

        $dbConnection = null;

    }



    //Screenshot
    static function insertScreenshot($idProdotto,$path,$titoloIt,$titoloEn){

        $dbConnection = new DbConnection();     

        //media
        $query ="insert into media  (tipo, URL) values (1,'" . mysql_escape_string($path) . "')";
        $idMedia = $dbConnection->insertQuery($query);

        $query = "insert into sezione_media (sezione_id,media_id,posizione,abilitata) values (" . $idProdotto . ",".$idMedia.",2,1)";
        $dbConnection->insertQuery($query);

        $query = "insert into media_it (media_id,titolo) values (" . $idMedia . ",'".mysql_escape_string($titoloIt)."')";
        $dbConnection->insertQuery($query);

        $query = "insert into media_en (media_id,titolo) values (" . $idMedia . ",'".mysql_escape_string($titoloEn)."')";
        $dbConnection->insertQuery($query);

        $dbConnection = null;

        return $idAllegato;
    }

    static function deleteScreenshot($idScreenshot){

        $dbConnection = new DbConnection();

        $query = "DELETE FROM media_en WHERE media_id = " . $idScreenshot ." ";
        $dbConnection->updateQuery($query);

        $query = "DELETE FROM media_it WHERE media_id = " . $idScreenshot ." ";
        $dbConnection->updateQuery($query);

        $query = "DELETE FROM sezione_media WHERE media_id = " . $idScreenshot ." ";
        $dbConnection->updateQuery($query);

        $query = "DELETE FROM media WHERE id = " . $idScreenshot ." ";
        $dbConnection->updateQuery($query);

        $dbConnection = null;

    }

    //ICONE 
    static function getIconeListByProdotto($idProdotto){

        $dbConnection = new DbConnection();
        $query = "Select  i.id, i.path_it, (select count(id) from rel_icona_prodotto where prodotto_id = ".$idProdotto." and icona_id = i.id) as enable
                    FROM icona_prodotto as i";               
        $res = $dbConnection->executeQuery($query);
        $dbConnection = null;

        return $res;
    }

    static function insertIcon($idProdotto,$idIcona){

        $dbConnection = new DbConnection();     

        $query ="insert into rel_icona_prodotto  (prodotto_id, icona_id) values (".$idProdotto.",".$idIcona.")";
        $idMedia = $dbConnection->insertQuery($query);

        $dbConnection = null;

        return $idAllegato;
    }

    static function removeIcon($idProdotto,$idIcona){

        $dbConnection = new DbConnection();

        $query = "DELETE FROM rel_icona_prodotto WHERE prodotto_id = " . $idProdotto ." and icona_id = ".$idIcona. " ";
        $dbConnection->updateQuery($query);

        $dbConnection = null;

    }
}
?>