<?php	

require_once("../includes/DbConnection.php");	

error_reporting(E_ERROR);
ini_set('display_errors', 1);

class CompetenzeDao {
		
 	static function getCompetenzeList(){

		$dbConnection = new DbConnection();

		$query = "select  pagina.id, pagina.nome titolo
					FROM pagina
					where id > 6 and id < 17
					order by pagina.nome";
					
		/* $myfile = fopen("mylog.txt", "w") or die("Unable to open file!");
		 fwrite($myfile, $query);
		 fclose($myfile);	*/
				
        $res = $dbConnection->executeQuery($query);

       	$dbConnection = null;

       	return $res;
	}


    static function getSezioniByPage($idPage){

        $dbConnection = new DbConnection();

        $query = "select  sezione.id, sezione.nome titolo, ifnull(sezione.URL,'') link 
                    FROM pagina_sezione, sezione
                    left join sezione_it ON sezione.id = sezione_it.sezione_id
                    where sezione.id=pagina_sezione.sezione_id
                    and pagina_sezione.pagina_id=".$idPage."
                    and sezione.tipo != 13
                    order by pagina_sezione.position";   //and (sezione.tipo = 8 or sezione.tipo = 12)
                    
        /* $myfile = fopen("mylog.txt", "w") or die("Unable to open file!");
         fwrite($myfile, $query);
         fclose($myfile);   */
                
        $res = $dbConnection->executeQuery($query);

        $dbConnection = null;

        return $res;
    }

	static function getCompetenza($idCompetenza,$idPage){
		
		$dbConnection = new DbConnection();

		$query ="select  sezione.id,sezione.tipo layout, sezione.nome nome, ifnull(sezione.URL,'') link,
					ifnull(sezione_it.titolo,'') titoloIT,ifnull(sezione_it.testo,'') testoIT,sezione_it.titolo_menu_laterale titolo_menu_laterale_it,
					ifnull(sezione_en.titolo,'') titoloEN,ifnull(sezione_en.testo,'') testoEN,sezione_en.titolo_menu_laterale titolo_menu_laterale_en, pagina_sezione.position posizione
											FROM pagina_sezione, sezione
											left join sezione_it ON sezione.id = sezione_it.sezione_id
											left join sezione_en ON sezione.id = sezione_en.sezione_id
											where sezione.id=pagina_sezione.sezione_id
											and pagina_sezione.pagina_id=".$idPage."
											and sezione.id = ".$idCompetenza."
											order by pagina_sezione.position";
					
		//$myfile = fopen("getHL.txt", "w") or die("Unable to open file!");
		//fwrite($myfile, $query);
		//fwrite($myfile, print_r($res,true));
        //fclose($myfile);	
        //and (sezione.tipo = 8 or sezione.tipo = 12)
				
        $res = $dbConnection->executeQuery($query);
        
       	$dbConnection = null;

       	return $res;
	}

	static function getMediaByIdCompetenza($idCompetenza){
		
		$dbConnection = new DbConnection();

		$query ="select media.id, media.URL path, 
                        ifnull(media_it.descrizione,'') descrizioneIT, ifnull(media_en.descrizione,'') descrizioneEN, ifnull(media_it.titolo,'') titoloIT, ifnull(media_en.titolo,'') titoloEN,   
                        sezione_media.posizione
						FROM sezione_media, media
						left join media_it ON media.id = media_it.media_id
                        left join media_en ON media.id = media_en.media_id
						where sezione_media.media_id = media.id
						and sezione_media.sezione_id = ".$idCompetenza."
						order by sezione_media.sotto_sezione, sezione_media.posizione";
									
        $res = $dbConnection->executeQuery($query);
        
       	$dbConnection = null;

       	return $res;
	}

	static function getAllegatiByIdCompetenza($idCompetenza){
		
		$dbConnection = new DbConnection();

		$query ="select allegato.id, allegato.tipo, allegato.tipo_icona icona, allegato.link path, allegato.testoIT testoIT,allegato.testoEN testoEN, posizione posizione
					FROM allegato
					WHERE allegato.sezione_id = ".$idCompetenza."
					order by tipo,posizione";	
				
        $res = $dbConnection->executeQuery($query);
        
       	$dbConnection = null;

       	return $res;
	}

	static function deleteCompetenza($idCompetenza){

    	$dbConnection = new DbConnection();

        $query = "DELETE FROM allegato WHERE sezione_id = " . $idCompetenza ." ";
        $dbConnection->updateQuery($query);

        $query = "DELETE FROM sezione_media WHERE sezione_id = " . $idCompetenza ." ";
        $dbConnection->updateQuery($query);

        $query = "DELETE FROM pagina_sezione WHERE sezione_id = " . $idCompetenza ." ";
        $dbConnection->updateQuery($query);

        $query = "DELETE FROM sezione_it WHERE sezione_id = " . $idCompetenza ." ";
        $dbConnection->updateQuery($query);

        $query = "DELETE FROM sezione_en WHERE sezione_id = " . $idCompetenza ." ";
        $dbConnection->updateQuery($query);

        $query = "DELETE FROM sezione WHERE id = " . $idCompetenza ." ";
        $dbConnection->updateQuery($query);
      
        $dbConnection = null;

    }

    static function insertCompetenza($path1,$path2,$path3,$path4,$nome,$link,$titoloIt,$titoloEn,$descrizioneIt,$descrizioneEn,$menuIt,$menuEn,$tipoLayout,$posizione,$idPage,
        $titoloMediaIt1,$titoloMediaEn1,$titoloMediaIt2,$titoloMediaEn2,
        $titoloMediaIt3,$titoloMediaEn3,$titoloMediaIt4,$titoloMediaEn4,
        $descrizioneMediaIt1,$descrizioneMediaEn1,$descrizioneMediaIt2,$descrizioneMediaEn2,
        $descrizioneMediaIt3,$descrizioneMediaEn3,$descrizioneMediaIt4,$descrizioneMediaEn4){

   		$dbConnection = new DbConnection();     

   		$query ="INSERT INTO sezione  (tipo,nome,URL) " .
						"VALUES (".$tipoLayout.",'" . mysql_escape_string($nome) . "'," . "'" . mysql_escape_string($link) . "')";
	    $idCompetenza = $dbConnection->insertQuery($query);


	    $query = "insert into sezione_it (titolo, testo, sezione_id, titolo_menu_laterale) values ('" . mysql_escape_string($titoloIt) . "','" . mysql_escape_string($descrizioneIt) . "',".$idCompetenza.",'" . mysql_escape_string($menuIt) . "')";
        $dbConnection->insertQuery($query);

        $query = "insert into sezione_en (titolo, testo, sezione_id, titolo_menu_laterale) values ('" . mysql_escape_string($titoloEn) . "','" . mysql_escape_string($descrizioneEn) . "',".$idCompetenza.",'" . mysql_escape_string($menuEn) . "')";
        $dbConnection->insertQuery($query);

        $query = "insert into pagina_sezione (pagina_id, sezione_id, position) values (" . $idPage . "," . $idCompetenza . ",'" . mysql_escape_string($posizione) . "')";
        $dbConnection->insertQuery($query);


        //cancello e riscrivo media
        $query = "DELETE FROM sezione_media WHERE sezione_id = " . $idCompetenza ." ";
    	$dbConnection->updateQuery($query);

        //media1
        $query ="insert into media  (tipo, URL) values (1,'" . mysql_escape_string($path1) . "')";
	    $idMedia1 = $dbConnection->insertQuery($query);

        $query = "insert into sezione_media (sezione_id,media_id,posizione,abilitata) values (" . $idCompetenza . ",".$idMedia1.",1,1)";
        $dbConnection->insertQuery($query);

        $query = "insert into media_it (titolo,descrizione,media_id) values ('" . mysql_escape_string($titoloMediaIt1) . "','" . mysql_escape_string($descrizioneMediaIt1) . "',".$idMedia1.")";
        $dbConnection->insertQuery($query);

        $query = "insert into media_en (titolo,descrizione,media_id) values ('" . mysql_escape_string($titoloMediaEn1) . "','" . mysql_escape_string($descrizioneMediaEn1) . "',".$idMedia1.")";
        $dbConnection->insertQuery($query);

        //media2
        if($path2 != ''){
        	$query ="insert into media  (tipo, URL) values (1,'" . mysql_escape_string($path2) . "')";
		    $idMedia2 = $dbConnection->insertQuery($query);

            $query = "insert into sezione_media (sezione_id,media_id,posizione,abilitata) values (" . $idCompetenza . ",".$idMedia2.",2,1)";
            $dbConnection->insertQuery($query);

            $query = "insert into media_it (titolo,descrizione,media_id) values ('" . mysql_escape_string($titoloMediaIt2) . "','" . mysql_escape_string($descrizioneMediaIt2) . "',".$idMedia2.")";
            $dbConnection->insertQuery($query);

            $query = "insert into media_en (titolo,descrizione,media_id) values ('" . mysql_escape_string($titoloMediaEn2) . "','" . mysql_escape_string($descrizioneMediaEn2) . "',".$idMedia2.")";
            $dbConnection->insertQuery($query);
        }
        
        //media3
        if($path3 != ''){
        	$query ="insert into media  (tipo, URL) values (1,'" . mysql_escape_string($path3) . "')";
	    	$idMedia3 = $dbConnection->insertQuery($query);

        	$query = "insert into sezione_media (sezione_id,media_id,posizione,abilitata) values (" . $idCompetenza . ",".$idMedia3.",3,1)";
        	$dbConnection->insertQuery($query);

            $query = "insert into media_it (titolo,descrizione,media_id) values ('" . mysql_escape_string($titoloMediaIt3) . "','" . mysql_escape_string($descrizioneMediaIt3) . "',".$idMedia3.")";
            $dbConnection->insertQuery($query);
            
            $query = "insert into media_en (titolo,descrizione,media_id) values ('" . mysql_escape_string($titoloMediaEn3) . "','" . mysql_escape_string($descrizioneMediaEn3) . "',".$idMedia3.")";
            $dbConnection->insertQuery($query);
        }

        //media4
        if($path4 != ''){
        	$query ="insert into media  (tipo, URL) values (1,'" . mysql_escape_string($path4) . "')";
	    	$idMedia4 = $dbConnection->insertQuery($query);

        	$query = "insert into sezione_media (sezione_id,media_id,posizione,abilitata) values (" . $idCompetenza . ",".$idMedia4.",4,1)";
        	$dbConnection->insertQuery($query);

            $query = "insert into media_it (titolo,descrizione,media_id) values ('" . mysql_escape_string($titoloMediaIt4) . "','" . mysql_escape_string($descrizioneMediaIt4) . "',".$idMedia4.")";
            $dbConnection->insertQuery($query);

            $query = "insert into media_en (titolo,descrizione,media_id) values ('" . mysql_escape_string($titoloMediaEn4) . "','" . mysql_escape_string($descrizioneMediaEn4) . "',".$idMedia4.")";
            $dbConnection->insertQuery($query);
        }

        $dbConnection = null;

	    return $idCompetenza;
	}

	static function updateCompetenza($idCompetenza,$path1,$path2,$path3,$path4,$nome,$link,$titoloIt,$titoloEn,$descrizioneIt,$descrizioneEn,$menuIt,$menuEn,$tipoLayout,$posizione,
        $titoloMediaIt1,$titoloMediaEn1,$titoloMediaIt2,$titoloMediaEn2,
        $titoloMediaIt3,$titoloMediaEn3,$titoloMediaIt4,$titoloMediaEn4,
        $descrizioneMediaIt1,$descrizioneMediaEn1,$descrizioneMediaIt2,$descrizioneMediaEn2,
        $descrizioneMediaIt3,$descrizioneMediaEn3,$descrizioneMediaIt4,$descrizioneMediaEn4){

		$dbConnection = new DbConnection();

        $query = "UPDATE sezione SET tipo = ".$tipoLayout.", nome = '" . mysql_escape_string($nome) . "', URL = '". mysql_escape_string($link) . "' where id = " . $idCompetenza ;
        $dbConnection->updateQuery($query);

        $query = "update sezione_it set titolo = '". mysql_escape_string($titoloIt) . "',testo = '". mysql_escape_string($descrizioneIt) . "',titolo_menu_laterale = '". mysql_escape_string($menuIt) . "' where sezione_id= ".$idCompetenza;
        $dbConnection->updateQuery($query);

        $query = "update sezione_en set titolo = '". mysql_escape_string($titoloEn) . "',testo = '". mysql_escape_string($descrizioneEn) . "',titolo_menu_laterale = '". mysql_escape_string($menuEn) . "' where sezione_id= ".$idCompetenza;
        $dbConnection->updateQuery($query);

        $query = "update pagina_sezione set position = '". mysql_escape_string($posizione) . "' where sezione_id= ".$idCompetenza;
        $dbConnection->updateQuery($query);


        //cancello e riscrivo media
        $query = "DELETE FROM sezione_media WHERE sezione_id = " . $idCompetenza ." ";
        $dbConnection->updateQuery($query);

        //media1
        $query ="insert into media  (tipo, URL) values (1,'" . mysql_escape_string($path1) . "')";
	    $idMedia1 = $dbConnection->insertQuery($query);

        $query = "insert into sezione_media (sezione_id,media_id,posizione,abilitata) values (" . $idCompetenza . ",".$idMedia1.",1,1)";
        $dbConnection->insertQuery($query);

        $query = "insert into media_it (titolo,descrizione,media_id) values ('" . mysql_escape_string($titoloMediaIt1) . "','" . mysql_escape_string($descrizioneMediaIt1) . "',".$idMedia1.")";
        $dbConnection->insertQuery($query);

        $query = "insert into media_en (titolo,descrizione,media_id) values ('" . mysql_escape_string($titoloMediaEn1) . "','" . mysql_escape_string($descrizioneMediaEn1) . "',".$idMedia1.")";
        $dbConnection->insertQuery($query);

        //media2
        if($path2 != ''){
        	$query ="insert into media  (tipo, URL) values (1,'" . mysql_escape_string($path2) . "')";
		    $idMedia2 = $dbConnection->insertQuery($query);

            $query = "insert into sezione_media (sezione_id,media_id,posizione,abilitata) values (" . $idCompetenza . ",".$idMedia2.",2,1)";
            $dbConnection->insertQuery($query);

            $query = "insert into media_it (titolo,descrizione,media_id) values ('" . mysql_escape_string($titoloMediaIt2) . "','" . mysql_escape_string($descrizioneMediaIt2) . "',".$idMedia2.")";
            $dbConnection->insertQuery($query);

            $query = "insert into media_en (titolo,descrizione,media_id) values ('" . mysql_escape_string($titoloMediaEn2) . "','" . mysql_escape_string($descrizioneMediaEn2) . "',".$idMedia2.")";
            $dbConnection->insertQuery($query);
        }
        
        //media3
        if($path3 != ''){
        	$query ="insert into media  (tipo, URL) values (1,'" . mysql_escape_string($path3) . "')";
	    	$idMedia3 = $dbConnection->insertQuery($query);

        	$query = "insert into sezione_media (sezione_id,media_id,posizione,abilitata) values (" . $idCompetenza . ",".$idMedia3.",3,1)";
        	$dbConnection->insertQuery($query);

            $query = "insert into media_it (titolo,descrizione,media_id) values ('" . mysql_escape_string($titoloMediaIt3) . "','" . mysql_escape_string($descrizioneMediaIt3) . "',".$idMedia3.")";
            $dbConnection->insertQuery($query);

            $query = "insert into media_en (titolo,descrizione,media_id) values ('" . mysql_escape_string($titoloMediaEn3) . "','" . mysql_escape_string($descrizioneMediaEn3) . "',".$idMedia3.")";
            $dbConnection->insertQuery($query);
        }

        //media4
        if($path4 != ''){
        	$query ="insert into media  (tipo, URL) values (1,'" . mysql_escape_string($path4) . "')";
	    	$idMedia4 = $dbConnection->insertQuery($query);

        	$query = "insert into sezione_media (sezione_id,media_id,posizione,abilitata) values (" . $idCompetenza . ",".$idMedia4.",4,1)";
        	$dbConnection->insertQuery($query);

            $query = "insert into media_it (titolo,descrizione,media_id) values ('" . mysql_escape_string($titoloMediaIt4) . "','" . mysql_escape_string($descrizioneMediaIt4) . "',".$idMedia4.")";
            $dbConnection->insertQuery($query);

            $query = "insert into media_en (titolo,descrizione,media_id) values ('" . mysql_escape_string($titoloMediaEn4) . "','" . mysql_escape_string($descrizioneMediaEn4) . "',".$idMedia4.")";
            $dbConnection->insertQuery($query);
        }

        $dbConnection = null;
        return $idCompetenza;
	}

    static function insertAllegato($idCompetenza,$path,$titoloIt,$titoloEn,$tipoAllegato,$tipoIcona,$posizione){

        $isUrl = 0;
        //if($tipoAllegato == 2){
        //    $isUrl = 1;
        //}

        $dbConnection = new DbConnection();     

        $query ="insert into allegato (sezione_id,link,descrizione,testoIT,testoEN,tipo,tipo_icona,posizione,isUrl) VALUES (".$idCompetenza.",'" . mysql_escape_string($path) . "',
            " . "'" . mysql_escape_string($titoloIt) . "'," . "'" . mysql_escape_string($titoloIt) . "'," . "'" . mysql_escape_string($titoloEn) . "',
            ".$tipoAllegato.",".$tipoIcona.",".$posizione.",".$isUrl.")";
        $idAllegato = $dbConnection->insertQuery($query);

        $dbConnection = null;

        return $idAllegato;
    }

    static function deleteAllegato($idAllegato){

        $dbConnection = new DbConnection();

        $query = "DELETE FROM allegato WHERE id = " . $idAllegato ." ";
        $dbConnection->updateQuery($query);

        $dbConnection = null;

    }

}
?>