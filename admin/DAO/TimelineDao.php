<?php	

require_once("../includes/DbConnection.php");	
	
error_reporting(E_ERROR);
ini_set('display_errors', 1);
	
class TimelineDao {
		
 	static function getTimelineList(){

		$dbConnection = new DbConnection();

		$query = "select id,anno,progressivo,titolo,immagine path
		FROM timeline_it 
		order by anno desc,progressivo desc";
					
		/* $myfile = fopen("mylog.txt", "w") or die("Unable to open file!");
		 fwrite($myfile, $query);
		 fclose($myfile);	*/
				
        $res = $dbConnection->executeQuery($query);

       	$dbConnection = null;

       	return $res;
	}

	static function getTimeline($idTimeline){
		
		$dbConnection = new DbConnection();

		$query ="select tit.id id,tit.anno anno,tit.progressivo progressivo,tit.link link,tit.immagine path,tit.titolo titoloIT,tit.descrizione descrizioneIT,ten.titolo titoloEN,ten.descrizione descrizioneEN
			from timeline_it tit
			inner join timeline_en ten on tit.id = ten.id
			where tit.id = " . $idTimeline;

		/*$myfile = fopen("getTimeline.txt", "w") or die("Unable to open file!");
		 fwrite($myfile, $query);
		 fclose($myfile);*/
					
		//$myfile = fopen("getHL.txt", "w") or die("Unable to open file!");
		//fwrite($myfile, $query);
		//fwrite($myfile, print_r($res,true));
        //fclose($myfile);	
				
        $res = $dbConnection->executeQuery($query);
        
       	$dbConnection = null;

       	return $res;
	}

	static function deleteTimeline($idTimeline){

    	$dbConnection = new DbConnection();

        $query = "DELETE FROM timeline_it WHERE id = " . $idTimeline ." ";
        $dbConnection->updateQuery($query);

        $query = "DELETE FROM timeline_en WHERE id = " . $idTimeline ." ";
        $dbConnection->updateQuery($query);
      
        $dbConnection = null;

    }

    static function insertTimeline($path,$link,$titoloIt,$titoloEn,$descrizioneIt,$descrizioneEn,$progressivo,$anno){

   		$dbConnection = new DbConnection();

        $query = "insert into timeline_it (titolo, descrizione,immagine,link,progressivo,anno) values ('" . mysql_escape_string($titoloIt) . "','" . mysql_escape_string($descrizioneIt) . "','" . mysql_escape_string($path) . "','" . mysql_escape_string($link) . "',".$progressivo.",".$anno.")";
        $dbConnection->insertQuery($query);

        $query = "insert into timeline_en (titolo, descrizione,immagine,link,progressivo,anno) values ('" . mysql_escape_string($titoloEn) . "','" . mysql_escape_string($descrizioneEn) . "','" . mysql_escape_string($path) . "','" . mysql_escape_string($link) . "',".$progressivo.",".$anno.")";
        $dbConnection->insertQuery($query);

        $dbConnection = null;
	    return $idTimeline;
	}

	static function updateTimeline($idTimeline,$path,$link,$titoloIt,$titoloEn,$descrizioneIt,$descrizioneEn,$progressivo,$anno){

		$dbConnection = new DbConnection();

        $query = "update timeline_it set titolo = '". mysql_escape_string($titoloIt) . "',descrizione = '". mysql_escape_string($descrizioneIt) . "',immagine = '". mysql_escape_string($path) . "',link = '". mysql_escape_string($link) . "',progressivo = ".$progressivo.",anno = ".$anno." where id= ".$idTimeline;
        $dbConnection->updateQuery($query);

        $query = "update timeline_en set titolo = '". mysql_escape_string($titoloEn) . "',descrizione = '". mysql_escape_string($descrizioneEn) . "',immagine = '". mysql_escape_string($path) . "',link = '". mysql_escape_string($link) . "',progressivo = ".$progressivo.",anno = ".$anno." where id= ".$idTimeline;
        $dbConnection->updateQuery($query);

        $dbConnection = null;
        return $idTimeline;
	}

}
?>