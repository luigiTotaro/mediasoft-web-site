<?php	

require_once("../includes/DbConnection.php");	
	
error_reporting(E_ERROR);
ini_set('display_errors', 1);
	
class RDDao {
		
 	static function getRDList(){

		$dbConnection = new DbConnection();

		$query = "select id,progressivo,titolo,link 
		FROM pubblicazioni_rd_it 
		order by progressivo desc";
					
		/* $myfile = fopen("mylog.txt", "w") or die("Unable to open file!");
		 fwrite($myfile, $query);
		 fclose($myfile);	*/
				
        $res = $dbConnection->executeQuery($query);

       	$dbConnection = null;

       	return $res;
	}

	static function getRD($idRD){
		
		$dbConnection = new DbConnection();

		$query ="select tit.id id,tit.progressivo progressivo,tit.link link,tit.titolo titoloIT,tit.autore autoreIT,ten.autore autoreEN,tit.testo testoIT,ten.titolo titoloEN,ten.testo testoEN
			from pubblicazioni_rd_it tit
			inner join pubblicazioni_rd_en ten on tit.id = ten.id
			where tit.id = " . $idRD;

		/*$myfile = fopen("getRD.txt", "w") or die("Unable to open file!");
		 fwrite($myfile, $query);
		 fclose($myfile);*/
					
		//$myfile = fopen("getHL.txt", "w") or die("Unable to open file!");
		//fwrite($myfile, $query);
		//fwrite($myfile, print_r($res,true));
        //fclose($myfile);	
				
        $res = $dbConnection->executeQuery($query);
        
       	$dbConnection = null;

       	return $res;
	}

	static function deleteRD($idRD){

    	$dbConnection = new DbConnection();

        $query = "DELETE FROM pubblicazioni_rd_it WHERE id = " . $idRD ." ";
        $dbConnection->updateQuery($query);

        $query = "DELETE FROM pubblicazioni_rd_en WHERE id = " . $idRD ." ";
        $dbConnection->updateQuery($query);
      
        $dbConnection = null;

    }

    static function insertRD($link,$titoloIt,$titoloEn,$autoreIt,$autoreEn,$testoIt,$testoEn,$progressivo){

   		$dbConnection = new DbConnection();

        $query = "insert into pubblicazioni_rd_it (titolo,autore,testo,link,progressivo) 
        values ('" . mysql_escape_string($titoloIt) . "','" . mysql_escape_string($autoreIt) . "','" . mysql_escape_string($testoIt) . "','" . mysql_escape_string($link) . "',".$progressivo.")";
        $dbConnection->insertQuery($query);

        $query = "insert into pubblicazioni_rd_en (titolo,autore,testo,link,progressivo) 
        values ('" . mysql_escape_string($titoloEn) . "','" . mysql_escape_string($autoreEn) . "','" . mysql_escape_string($testoEn) . "','" . mysql_escape_string($link) . "',".$progressivo.")";
        $dbConnection->insertQuery($query);

        $dbConnection = null;
	    return $idRD;
	}

	static function updateRD($idRD,$link,$titoloIt,$titoloEn,$autoreIt,$autoreEn,$testoIt,$testoEn,$progressivo){

		$dbConnection = new DbConnection();

        $query = "update pubblicazioni_rd_it set titolo = '". mysql_escape_string($titoloIt) . "',autore = '". mysql_escape_string($autoreIt) . "',
        testo = '". mysql_escape_string($testoIt) . "',link = '". mysql_escape_string($link) . "',progressivo = ".$progressivo." where id= ".$idRD;
        $dbConnection->updateQuery($query);

        $query = "update pubblicazioni_rd_en set titolo = '". mysql_escape_string($titoloEn) . "',autore = '". mysql_escape_string($autoreEn) . "',
        testo = '". mysql_escape_string($testoEn) . "',link = '". mysql_escape_string($link) . "',progressivo = ".$progressivo." where id= ".$idRD;
        $dbConnection->updateQuery($query);

        $dbConnection = null;
        return $idRD;
	}

}
?>