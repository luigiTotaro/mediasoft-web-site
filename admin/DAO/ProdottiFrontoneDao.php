<?php	

require_once("../includes/DbConnection.php");	

error_reporting(E_ERROR);
ini_set('display_errors', 1);

class ProdottiFrontoneDao {	

	static function getMediaByIdProdotto($idProdotto){
		
		$dbConnection = new DbConnection();

		$query ="select media.id, media.URL path,  sezione_media.posizione , COALESCE(media_it.titolo,'') as titoloIT
						FROM sezione_media, media
						left join media_it ON media.id = media_it.media_id
						where sezione_media.media_id = media.id
						and sezione_media.sezione_id = ".$idProdotto."
						order by sezione_media.sotto_sezione, sezione_media.posizione";
									
        $res = $dbConnection->executeQuery($query);
        
       	$dbConnection = null;

       	return $res;
	}


    //Frontone
    static function insertFrontone($idProdotto,$path,$titoloIt,$titoloEn){

        $dbConnection = new DbConnection();     

        //media
        $query ="insert into media  (tipo, URL) values (1,'" . mysql_escape_string($path) . "')";
        $idMedia = $dbConnection->insertQuery($query);

        $query = "insert into sezione_media (sezione_id,media_id,posizione,abilitata) values (" . $idProdotto . ",".$idMedia.",2,1)";
        $dbConnection->insertQuery($query);

        $query = "insert into media_it (media_id,titolo) values (" . $idMedia . ",'".mysql_escape_string($titoloIt)."')";
        $dbConnection->insertQuery($query);

        $query = "insert into media_en (media_id,titolo) values (" . $idMedia . ",'".mysql_escape_string($titoloEn)."')";
        $dbConnection->insertQuery($query);


        $dbConnection = null;

        return $idAllegato;
    }

    static function deleteFrontone($idFrontone){

        $dbConnection = new DbConnection();

        $query = "DELETE FROM media_en WHERE media_id = " . $idFrontone ." ";
        $dbConnection->updateQuery($query);

        $query = "DELETE FROM media_it WHERE media_id = " . $idFrontone ." ";
        $dbConnection->updateQuery($query);

        $query = "DELETE FROM sezione_media WHERE media_id = " . $idFrontone ." ";
        $dbConnection->updateQuery($query);

        $query = "DELETE FROM media WHERE id = " . $idFrontone ." ";
        $dbConnection->updateQuery($query);


        $dbConnection = null;

    }

}
?>