<?php

include 'lang.php';
include "./backoffice/DBAccess.php";

$dbInst = new DBAccess();
$dbInst->connOpen();

$lang = (isset($_GET["lang"]) ? $_GET["lang"] : "IT");
$idPage = $_GET["idPage"];

$extD = $dbInst->getDetailLangPagina($idPage, $lang, "Title_Posizionamento");
$row = mysql_fetch_array($extD);
$title = $row['valore'];

$extD = $dbInst->getDetailLangPagina($idPage,$lang,"Description");  
$row = mysql_fetch_array($extD);
$description = $row['valore'];

$extD = $dbInst->getDetailLangPagina($idPage,$lang,"Keywords");  
$row = mysql_fetch_array($extD);
$keywords = $row['valore'];
 
$dbInst->connClose();

?>


<!DOCTYPE html>
<html>
  <head>
    <title><?=$title?></title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="HandheldFriendly" content="true" />

    <meta name="description" content="<?=$description?>"/>
    <meta name="keywords" content="<?=$keywords?>"/>

    <link href='http://fonts.googleapis.com/css?family=Maven+Pro' rel='stylesheet' type='text/css'>
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
    <link href="bootstrap.customization.css" rel="stylesheet" media="screen">
    <link href="navbar.customization.css" rel="stylesheet" media="screen">
    <link href="carousel.customization.css" rel="stylesheet" media="screen">
    <link href="content.customization.css" rel="stylesheet" media="screen">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
    
    <style>

	    body {
	    	padding: 0px;
	    	margin: 0px;
	    	font-family: 'Maven Pro';
	    	font-size: 13px;
	    	font-weight:500;
	    }

	    #rowCollection h2 {
	    	font-size: 55px;
	    	line-height: 55px;	 
	    	margin:0px;
	    	padding: 0px;   	
	    	color: #b1c903;
	    }

	    #rowCollection h3 {
	    	font-size: 18px;
	    	line-height: 18px;	    	
	    	margin:0px;
	    	padding: 0px;
	    }

	    #rowCollection h4 {
	    	font-size: 17px;
	    	line-height: 17px;
	    	color: #b1c903;
	    }

	    #rowCollection h5 {
	    	text-align: justify;
	    }

	    a:link, a:hover, a:visited {
	    	text-decoration: none;
	    	color: #b1c903;
	    }

    </style>
  </head>

  <body>

  	<div class="navbar-wrapper" style="position:fixed;background-color: #FFFFFF">
		<div class="container" id="navContainer" style="width:100%;">

			<nav class="navbar navbar-default transparent_navbar" role="navigation">

			  <!-- Brand and toggle get grouped for better mobile display -->
			  <div class="navbar-header">
			    <button type="button" class="navbar-toggle" data-toggle="collapse" id="buttonMobile" data-target="#bs-example-navbar-collapse-1">
			      <span class="sr-only">Toggle navigation</span>
			       <img id="logoButtonMobile" src="images/logo_mediasoft_notext_white.png">
			    </button>
			  </div>

			  <!-- Collect the nav links, forms, and other content for toggling -->
			  <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			    <ul class="nav navbar-nav" id="navBarMainUl">
			    </ul>

			  </div><!-- /.navbar-collapse -->
			</nav>

		</div>
		<hr>
	</div>
	<hr>
<!--
	<div id="mainContainerDiv" class="container fill" style="width:80%; margin: 0px; padding: 0px;float:left;position:absolute;top:200px;border:solid;">
	-->
	<div id="mainContainerDiv" class="container fill" style="width:80%; margin: 0px; padding: 0px;float:left;">
	
	</div>
	
	<div id="navbar-laterale" style="position:fixed;width:10%;margin: 10px;  right:15px; top: 180px;">
		<ul id="lateralMenu" class="nav nav-pills">
		</ul>
	</div>

	<script src="jquery-1.10.2.min.js"></script>
	<script src="jquery.jscroll.min.js"></script>
	<script src="bootstrap/js/bootstrap.js"></script>
	<script src="functions.js"></script>
	<script src="modernizr.custom.js"></script>

	
	

	<script type="text/javascript">

		var pages_config = new Array();

		// pagina internet delle cose
		pages_config[27] = {

			'idPage': 27,
			'backColor': 'white',
			'next_content': [40]

		}

		// pagina about
		pages_config[29] = {

			'idPage': 29,
			'backColor': 'white',
			'next_content': [58, 59, 60]

		}

		// pagina prodotti
		pages_config[40] = {

			'idPage': 40,
			'backColor': '#ededee',

			'renderer': function(row_data) {

				var idRisorsa = row_data['idRisorsa'];
				var data = row_data['Data'];
				var titolo = row_data['Titolo'];
				var descrizione_breve = row_data['Presentazione'];
				var descrizione_estesa = $('<div/>').html(row_data["Descrizione"]).text();
				var file = row_data['File'];

				var html =	
					'<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-left" style="padding: 10px 20px 25px 20px; margin: 0px 0px 0px 0px;">' + 		
						'<div>' +
												
							'<div style="float:left; border:solid 1px gray; margin-right:20px; text-align:center; width: 200px; height: 100px; ' + 
								' background-image:url(\'backoffice/Offerte/Repository/' + file + '\'); ' +
								' background-position:center; background-repeat:no-repeat; background-size: cover; ' + 
								' filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src=\'./backoffice/Offerte/Repository/' + file + '\', sizingMethod=\'scale\'); ' +
								' -ms-filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src=\'backoffice/Offerte/Repository/' + file + '\', sizingMethod=\'scale\')' +
								'">' +
							'</div>' + 

							'<p><h4>' + titolo + '</h4></p>' + 
							'<p style="color:#b1c903;"><h5>' + descrizione_breve + '</h5></p>' +	
							'<p><a href="product.php?idCollection=' + idRisorsa + '"><?=translate('dettagli')?></a></p>' + 										
						
						'</div>' +						
						
					'</div>' + 

					'<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center" style="padding: 0px; margin: 0px; background-image:url(\'images/line_gray.png\'); background-repeat: no-repeat; background-position:center bottom; background-size: 96%;"><div>';

				return html;

			}
		}

		// pagina staff
		pages_config[58] = {

			'idPage': 58,
			'backColor': '#ededee',

			'renderer': function(row_data) {

				var data = row_data['Data'];
				var titolo = row_data['Titolo'];
				var descrizione_breve = row_data['Presentazione'];
				var descrizione_estesa = $('<div/>').html(row_data["Descrizione"]).text();
				var file = row_data['File'];

				var html =	
					'<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 text-center" style="padding: 0px 0px 0px 0px; margin: 0px 0px 30px 0px; vertical-align:middle; display:inline-block;">' + 						
						'<img src="backoffice/Offerte/Repository/' + file + '" />' +
						'<p>' + titolo + '</p>' + 
						'<p style="color: #b1c903;">' + descrizione_breve + '</p>' +											
					'</div>';

				return html;

			}
		}

		// pagina clienti
		pages_config[59] = {

			'idPage': 59,
			'backColor': 'white',

			'renderer': function(row_data) {

				var data = row_data['Data'];
				var titolo = row_data['Titolo'];
				var descrizione_breve = row_data['Presentazione'];
				var descrizione_estesa = $('<div/>').html(row_data["Descrizione"]).text();
				var file = row_data['File'];

				var html =	
					'<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 text-center" style="padding: 0px 0px 0px 0px; margin: 0px 0px 0px 0px; height:200px; vertical-align:middle; display:inline-block;">' + 						
						'<img src="backoffice/Offerte/Repository/' + file + '" />' +										
					'</div>';

				return html;

			}			
		}

		// pagina cosa c'è di nuovo
		pages_config[60] = {

			'idPage': 60,
			'backColor': '#ededee',

			'renderer': function(row_data) {

				var data = row_data['Data'];
				var titolo = row_data['Titolo'];
				var descrizione_breve = row_data['Presentazione'];
				var descrizione_estesa = $('<div/>').html(row_data["Descrizione"]).text();
				var file = row_data['File'];

				var html =	
					'<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-left" style="padding: 10px 0px 25px 0px; margin: 0px 0px 0px 0px;">' + 		
						'<div>' +
												
							'<div style="float:left; margin-right:20px; width: 100px; height:100px; text-align:center; background-color: white;">' +
								'<p><h3>' + $.getMonth(data) +'</h3></p>' + 
								'<p><h2>' + $.getDay(data) + '</h2></p>' + 
							'</div>' + 

							'<p><h4>' + titolo + '</h4></p>' + 
							'<p style="color:#b1c903;"><h5>' + descrizione_estesa + '</h5></p>' +											
						
						'</div>' +
					'</div>' +

					'<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center" style="padding: 0px; margin: 0px; background-image:url(\'images/line_gray.png\'); backgroud-repeat: no-repeat; background-position:center bottom; background-size: 93%;"><div>';

				return html;

			}
			
		}


		if(!pages_config[$.urlParam('idPage')]) {

			pages_config[$.urlParam('idPage')] = {
				'idPage': $.urlParam('idPage'),
				'backColor': 'white',

			'renderer': function(row_data) {

				var idRisorsa = row_data['idRisorsa'];
				var data = row_data['Data'];
				var titolo = row_data['Titolo'];
				var descrizione_breve = row_data['Presentazione'];
				var descrizione_estesa = $('<div/>').html(row_data["Descrizione"]).text();
				var file = row_data['File'];

				var html =	
					'<div id="section'+idRisorsa+'" class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-left" style="padding: 10px 20px 25px 20px; margin: 0px 0px 0px 0px;">' + 		
						'<div>' +
												
							'<div style="float:left; border:solid 1px gray; margin-right:20px; text-align:center; width: 200px; height: 100px; ' + 
								' background-image:url(\'backoffice/Offerte/Repository/' + file + '\'); ' +
								' background-position:center; background-repeat:no-repeat; background-size: cover; ' + 
								' filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src=\'./backoffice/Offerte/Repository/' + file + '\', sizingMethod=\'scale\'); ' +
								' -ms-filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src=\'backoffice/Offerte/Repository/' + file + '\', sizingMethod=\'scale\')' +
								'">' +
							'</div>' + 

							'<p><h4>' + titolo + '</h4></p>' + 
							'<p style="color:#b1c903;"><h5>' + descrizione_breve + '</h5></p>' +	
							'<p><a href="product.php?idCollection=' + idRisorsa + '"><?=translate('dettagli')?></a></p>' + 										
						
						'</div>' +						
						
					'</div>' + 

					'<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center" style="padding: 0px; margin: 0px; background-image:url(\'images/line_gray.png\'); background-repeat: no-repeat; background-position:center bottom; background-size: 96%;"><div>';
				return html;
			},
			'renderer2': function(row_data) {

				var idRisorsa = row_data['idRisorsa'];
				var data = row_data['Data'];
				var titolo = row_data['Titolo'];
				var descrizione_breve = row_data['Presentazione'];
				var descrizione_estesa = $('<div/>').html(row_data["Descrizione"]).text();
				var file = row_data['File'];

				var html =	'<li><a href="#section'+idRisorsa+'">'+titolo+'</a></li>'
				return html;

			}
			
			}

		}


		$(window).load(function() {

			$.buildTopMenu();
			$.tweakIpadLandscape();
			
		});


		$(document).ready(function() {

			var mainPageConfig = pages_config[$.urlParam('idPage')];
			var next_content = mainPageConfig.next_content;
			var index = 0;

			
			$('#mainContainerDiv').load(
				"container.php",
				"",
				function() {

					$.setDivContent( mainPageConfig, true, false);

					$('a[href*=#]:not([href=#])').click(function()
					{
						if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
						  var target = $(this.hash);
						  
						  target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
						  if (target.length) {
							$('html,body').animate({
							  scrollTop: target.offset().top-180
							}, 1000);
							return false;
						  }
						}
					});	
					
					$('body').scrollspy({ target: '#navbar-laterale' })
				}			
			);
			

			
		});






	</script>

  </body>

</html>
